@echo off
setlocal EnableExtensions
setlocal EnableDelayedExpansion 

set srvs=patcas02,patcas12,teledb01,teledb02,teledb03,teledb04,teledb13,teledb16,patcdb02,patcdb03,patcdb04,patcdb05
set f=info.bat
set o=output.txt

rem @echo %srvs%

rem if exists c:\%f% del %f%

for %%a in (%srvs%) do (
@echo wmic /node:%%a ComputerSystem get Name ^>^> ^%o% >> %f%
@echo wmic /node:%%a ComputerSystem get TotalPhysicalMemory ^>^> ^%o% >> %f%
rem @echo. >> %f%
@echo wmic /node:%%a logicaldisk get Name, Size, FreeSpace ^>^> ^%o% >> %f%
rem @echo. >> %f%
)

rem wmic /node:patcas55 bios get serialnumber
rem wmic /node:patcas02 logicaldisk get Name, Size, FreeSpace
rem wmic /node:patcas02 ComputerSystem get TotalPhysicalMemory