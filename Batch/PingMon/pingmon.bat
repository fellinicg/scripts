@echo off
setlocal EnableDelayedExpansion 

rem ===================================================================
rem Variables
rem ===================================================================

set ct=21600
set ct=3600
set wait=1
set host=teleav82

rem ===================================================================
rem Create TimeStamp
rem ===================================================================

for /f "tokens=2-4 delims=/ " %%a in ('date /T') do set ts=%%c%%a%%b

rem ===================================================================
rem Add timestamp to log name
rem ===================================================================

set log=%ts%-%host%.log

rem ===================================================================
rem Ping
rem ===================================================================

for /l %%g in (1,1,%ct%) do (
ping %host% -n 1 -w 5 |for /f "tokens=*" %%a in ('findstr /I "reply request"') do @echo %%time%%: %%a >> %log%
if /i %%g lss %ct% choice /d y /t %wait% > nul
)

:end