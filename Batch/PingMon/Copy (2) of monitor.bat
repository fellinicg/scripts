@echo off

rem ===================================================================
rem Variables
rem ===================================================================

set dur=1800
rem set ct=3600
set ct=10
set wait=1
set log=ping.log
set host=teleav05

rem ===================================================================
rem Create TimeStamp
rem ===================================================================

for /f "tokens=2-4 delims=/ " %%a in ('date /T') do set ts=%%c%%a%%b

rem ===================================================================
rem Add timestamp to log name
rem ===================================================================

set log=%ts%-%log%

del %log%

rem ===================================================================
rem Ping
rem ===================================================================
set line=

for /l %%g in (1,1,%ct%) do (
for /f "tokens=*" %%a in ('ping %host% -n 1 -w 900') do (set line=%line% %%a
)
if /i %%g lss %ct% choice /d y /t %wait% > nul
)

:end
@echo %line% >> %log%

rem set line=%time%: %%a