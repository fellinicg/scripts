@echo off

@echo LT-ADB-AC2M
wmic /node:159.102.54.120 service where caption='LS License Server' get state

@echo HT-ADB-AC2M
wmic /node:159.102.54.186 service where caption='LS License Server' get state

@echo GWB-ADB-AC2M
wmic /node:159.102.61.135 service where caption='LS License Server' get state

pause
