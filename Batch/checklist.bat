@echo off
set path=S:\Documentation Library\Services\Server Support\Server Admin Procedures\Daily Environment Checks\
set file=Daily Checks Checklist.docx
for /f "tokens=2-4 delims=/ " %%a in ('date /T') do (
 set ts=%%c%%a%%b
 set year=%%c
 set yearmonth=%%c%%a
)
start "" "%path%%year%\%yearmonth%\%ts%-%file%"