@echo off
setlocal EnableExtensions
setlocal EnableDelayedExpansion 

@echo START.BAT >> script.log

rem ===================================================================
rem Local variables
rem ===================================================================

set dns_entry=TSDCOMRWIS
set pri_srv=TELEAV82
set pri_ip=159.102.242.22
set sec_srv=PATCAV86
set sec_ip=159.102.47.163
set ip=

rem ===================================================================
rem Remove DNS entry from all DNS servers
rem ===================================================================

@echo dnscmd teleis29 /recorddelete pacorp.panynj.gov %dns_entry% A %pri_ip% /f
@echo dnscmd teleis29 /recorddelete pacorp.panynj.gov %dns_entry% A %sec_ip% /f
@echo dnscmd teleis30 /recorddelete pacorp.panynj.gov %dns_entry% A %pri_ip% /f
@echo dnscmd teleis30 /recorddelete pacorp.panynj.gov %dns_entry% A %sec_ip% /f
@echo dnscmd patcis22 /recorddelete pacorp.panynj.gov %dns_entry% A %pri_ip% /f
@echo dnscmd patcis22 /recorddelete pacorp.panynj.gov %dns_entry% A %sec_ip% /f
@echo dnscmd patcis23 /recorddelete pacorp.panynj.gov %dns_entry% A %pri_ip% /f
@echo dnscmd patcis23 /recorddelete pacorp.panynj.gov %dns_entry% A %sec_ip% /f

rem ===================================================================
rem Check computer name and register appropriate IP address
rem ===================================================================

if /i "%computername%"=="%pri_srv%" (
set ip=%pri_ip%
@echo Should be TELEAV82 >> script.log
@echo %computername% >> script.log
) else (
set ip=%sec_ip%
@echo Should be PATCAV86 >> script.log
@echo %computername% >> script.log
)

@echo dnscmd teleis29 /recordadd pacorp.panynj.gov %dns_entry% A %ip%
@echo dnscmd teleis30 /recordadd pacorp.panynj.gov %dns_entry% A %ip%
@echo dnscmd patcis22 /recordadd pacorp.panynj.gov %dns_entry% A %ip%
@echo dnscmd patcis23 /recordadd pacorp.panynj.gov %dns_entry% A %ip%
