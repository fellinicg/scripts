@echo off
setlocal EnableExtensions
setlocal EnableDelayedExpansion 

rem ===================================================================
rem Create local file with dns servers
rem ===================================================================

@echo TELEIS29 > dns.txt
@echo PATCIS22 >> dns.txt
@echo TELEIS30 >> dns.txt
@echo PATCIS23 >> dns.txt


rem ===================================================================
rem Local variables
rem ===================================================================

set dnscmd="C:\Program Files\Support Tools\dnscmd"
set dns_entry=TSD-COM-RWIS

rem ===================================================================
rem Delete DNS A records
rem ===================================================================

for /f "tokens=2 delims=:" %%a in ('ipconfig ^| find /i "ip address"') do (
 for /F %%b in (dns.txt) do (
   @echo %dnscmd% %%b /recordadd pacorp.panynj.gov %dns_entry% A%%a
 )
)

goto exit

for /f "tokens=2 delims=:" %%a in ('ipconfig ^| find /i "ip address"') do (
 for /f "tokens=1" %%i in ("%%a") do (
  for /F %%b in (dns.txt) do (
   @echo %dnscmd% %%b /recordadd pacorp.panynj.gov %dns_entry% A %%i
  )
 )
)


:exit