@echo off
setlocal EnableExtensions
setlocal EnableDelayedExpansion 

rem ===================================================================
rem Local variables
rem ===================================================================

set dnsfile=dns.txt
set dnssrvs=TELEIS29,PATCIS22,TELEIS30,PATCIS23
set dnsentry=TSD-COM-RWIS
set dnscmd="C:\Program Files\Support Tools\dnscmd"
set dnszone=pacorp.panynj.gov
set logfile=start.log
set ts=%date% %time%
set pri_srv=TELEAV82
set pri_ip=159.102.242.22
set sec_srv=PATCAV86
set sec_ip=159.102.47.163
set iplst=ip.txt
set ip=

rem ===================================================================
rem Check computer name and assign appropriate IP address
rem ===================================================================

if /i "%computername%"=="%pri_srv%" (set ip=%pri_ip%) else (set ip=%sec_ip%)

rem ===================================================================
rem Start log file
rem ===================================================================

@echo *** %ts% - Begin *** >> %logfile%
@echo. >> %logfile%

rem ===================================================================
rem Delete DNS A records
rem ===================================================================

type nul > %iplst%
for %%a in (%dnssrvs%) do (
 for /f "skip=1 tokens=2 delims=:" %%b in ('nslookup %dnsentry% %%a ^| find /i "address"') do (
  for %%c in (%%b) do (
   for /f %%d in ('type %iplst% ^| find /c "%%c"') do (
    if %%d equ 0 (
     for %%e in (%dnssrvs%) do (
      @echo %dnscmd% %%e /recorddelete %dnszone% %dnsentry% A %%c /f >> %logfile%
      rem %dnscmd% %%e /recorddelete %dnszone% %dnsentry% A %%c /f >> %logfile%
      @echo. >> %logfile%
     )
     @echo %%c >> %iplst%
    )
   )
  )
 )
)

rem ===================================================================
rem Add DNS A records
rem ===================================================================

for %%a in (%dnssrvs%) do (
 @echo %dnscmd% %%a /recordadd %dnszone% %dnsentry% A %ip% >> %logfile%
 rem %dnscmd% %%a /recordadd %dnszone% %dnsentry% A %ip% >> %logfile%
 @echo. >> %logfile%
)

rem ===================================================================
rem Finish log file
rem ===================================================================

@echo *** %ts% - End *** >> %logfile%
@echo. >> %logfile%

:exit