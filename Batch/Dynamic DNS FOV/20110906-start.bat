@echo off
setlocal EnableExtensions
setlocal EnableDelayedExpansion 

rem ===================================================================
rem Local variables
rem ===================================================================

set dnsfile=dns.txt
set dnssrvs=TELEIS29,PATCIS22,TELEIS30,PATCIS23
set dnsentry=TSD-COM-RWIS
set dnscmd="C:\Program Files\Support Tools\dnscmd"
set dnszone=pacorp.panynj.gov
set logfile=start.log
set ts=%date% %time%
set pri_srv=TELEAV82
set pri_ip=159.102.242.22
set sec_srv=PATCAV86
set sec_ip=159.102.47.163
set iplst=ip.txt
set ip=
set myvar=

rem ===================================================================
rem Check computer name and assign appropriate IP address
rem ===================================================================

if /i "%computername%"=="%pri_srv%" (set ip=%pri_ip%) else (set ip=%sec_ip%)

rem ===================================================================
rem Use nslookup to get a list of ip address to delete from DNS
rem ===================================================================

type nul > %iplst%
for %%a in (%dnssrvs%) do (
 for /f "skip=1 tokens=2 delims=:" %%b in ('nslookup %dnsentry% %%a ^| find /i "address"') do (
  for %%c in (%%b) do (set tmf=&call :concat %%c&@echo %myvar%
rem @echo %myvar&^|find /i "%c"

   rem @echo %%c >> %iplst%
   rem findstr "%%c" ip.txt
 rem @echo %errorlevel% >> %iplst%
   rem if %errorlevel% neq 0 @echo %%c >> %iplst%
  )
 )
)

:concat
set myvar=%myvar% %1;
goto :eof

goto exit

sort %iplst% /O %iplst%


rem ===================================================================
rem Add DNS A records
rem ===================================================================

@echo *** %ts% - Begin *** >> %logfile%
@echo. >> %logfile%
for %%a in (%dnssrvs%) do (
 @echo %dnscmd% %%a /recordadd %dnszone% %dnsentry% A %ip% >> %logfile%
 rem %dnscmd% %%a /recordadd %dnszone% %dnsentry% A %ip% >> %logfile%
 @echo. >> %logfile%
)
@echo *** %ts% - End *** >> %logfile%
@echo. >> %logfile%

:exit