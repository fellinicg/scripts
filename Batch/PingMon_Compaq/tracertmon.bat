@echo off
setlocal EnableDelayedExpansion 

rem ===================================================================
rem Variables
rem ===================================================================

set ct=1440
set ct=2
set wait=15
set host=teledb04

rem ===================================================================
rem Create TimeStamp
rem ===================================================================

for /f "tokens=2-4 delims=/ " %%a in ('date /T') do set ts=%%c%%a%%b

rem ===================================================================
rem Add timestamp to log name
rem ===================================================================

set log=%ts%-%host%-trcrt.log

rem ===================================================================
rem Tracert
rem ===================================================================

for /l %%g in (1,1,%ct%) do (
@echo !time! >> %log%
tracert %host% -w 1000 >> %log%
@echo. >> %log%
if /i %%g lss %ct% choice /d y /t %wait% > nul
)

:end