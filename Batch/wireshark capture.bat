@echo off

rem ===================================================================
rem Create TimeStamp
rem ===================================================================

for /f "tokens=2-4 delims=/ " %%a in ('date /T') do set ts=%%c%%a%%b

rem ===================================================================
rem Get interfaces and save in file
rem ===================================================================

"c:\program files\wireshark\tshark.exe" -D > interfaces.txt

rem ===================================================================
rem Loop over interface file and start tshark in a separate cmd window
rem ===================================================================
for /F "tokens=1 delims=." %%i in (interfaces.txt) do start cmd /C "c:\program files\wireshark\tshark.exe" -i %%i -a duration:10 -t ad ^> %ts%-%%i.txt

:end