@echo off
rem setlocal EnableDelayedExpansion 

rem ===================================================================
rem Retrieve Computer Name
rem ===================================================================

for /f "tokens=2 delims=:" %%a in ('ipconfig /all ^| findstr /i "host name"') do set myvar=%%a

rem ===================================================================
rem Remove first character
rem ===================================================================

set myvar=%myvar:~1%

rem ===================================================================
rem Remove spaces
rem ===================================================================

set myvar=%myvar: =%

@echo %myvar%
