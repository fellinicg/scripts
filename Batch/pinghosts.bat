ping -a -w 2 -n 1 %1 |findstr "Reply from" >> ping.log
echo %ERRORLEVEL%
if errorlevel==1 goto notgood
if errorlevel==0 goto good
:notgood
echo %1 is not pingable >> ping.log
goto end
:good
echo %1 is good >> ping.log
goto end
:end
echo ---------------------- >> ping.log