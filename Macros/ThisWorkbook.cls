VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ThisWorkbook"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
Option Explicit
 
Private Sub Workbook_Activate()
    
    ' Disable copy/cut/paste
    '
    Call ToggleCutCopyAndPaste(False)
    
End Sub
 
Private Sub Workbook_BeforeClose(Cancel As Boolean)
    
    ' Declarations
    '
    'Dim ws As Worksheet
    
    ' Enable copy/cut/paste
    '
    Call ToggleCutCopyAndPaste(True)
    
    ' Loop through all worksheets and hide
    '
    'For Each ws In ThisWorkbook.Worksheets
    '    If ws.Name <> "Sheet1" Then
    '        ws.Visible = xlVeryHidden
    '    End If
    'Next ws
    
    ' Prevent save dialog box
    '
    Me.Saved = True

End Sub
 
Private Sub Workbook_BeforeSave(ByVal SaveAsUI As Boolean, Cancel As Boolean)
    
    ' Cancel any request to save the file
    '
    Cancel = True
    
End Sub

Private Sub Workbook_Deactivate()
    
    ' Enable copy/cut/paste
    '
    Call ToggleCutCopyAndPaste(True)

End Sub
 
Private Sub Workbook_Open()

    ' Declarations
    '
    Dim ws As Worksheet
     
    ' Unprotect workbook
    '
    UnprotectWorkbook
    
    ' Loop through all worksheets and unhide
    '
    For Each ws In ThisWorkbook.Worksheets
        ws.Visible = xlSheetVisible
    Next ws
    
    ' Hide Sheet1
    '
    Sheets("Sheet1").Visible = xlHidden
    
    ' Protect workbook
    '
    ProtectWorkbook
    
    ' Check access level
    '
    If Authenticate = "X" Then
        Application.ScreenUpdating = False
        ClearSheets
        Application.ScreenUpdating = True
        MsgBox "Access Denied"
        Exit Sub
    End If
    
    ' Disable copy/cut/paste
    '
    Call ToggleCutCopyAndPaste(False)

    ' Load server data
    '
    LoadServers
    LoadServerIPs
    
    ' Select first tab
    '
    Sheets("Servers").Select

End Sub
