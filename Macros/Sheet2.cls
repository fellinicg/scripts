VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Sheet2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
Private Sub Worksheet_BeforeDoubleClick(ByVal Target As Range, Cancel As Boolean)

    Call Main.WorksheetBeforeDoubleClick(Target, Cancel)

End Sub

Private Sub Worksheet_Change(ByVal Target As Range)
    
    Call Main.WorksheetChange(Target)

End Sub

Private Sub Worksheet_SelectionChange(ByVal Target As Range)

    If Not IsLoading And Target.Cells.CountLarge = 1 Then CurrVal = Target.Value

End Sub

