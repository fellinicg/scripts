Attribute VB_Name = "Main"
Option Explicit

Public Const ConnStr As String = "Provider=SQLOLEDB;Data Source=patcdb21\alloy_1;Initial Catalog=Refresh;Persist Security Info=False;Integrated Security=SSPI;Trusted_Connection=Yes"
Public IsLoading As Boolean
Public CurrVal As String

Function Authenticate() As String

    On Error GoTo Handler
    
    ' Declarations
    '
    Dim Conn As New ADODB.Connection
    Dim Cmd As New ADODB.Command
    
    ' Open connection
    '
    Conn.Open (ConnStr)
    
    ' Stored Procedure
    '
    Cmd.CommandText = "upAuthenticate"
    Cmd.CommandType = adCmdStoredProc
    Cmd.ActiveConnection = Conn

    ' ServerID
    '
    Cmd.Parameters.Append Cmd.CreateParameter("@Rights", adChar, adParamOutput, 1)
    
    ' Execute the Stored Procedure
    '
    Cmd.Execute

    ' Assign access level to global variable
    '
    Authenticate = Cmd.Parameters("@Rights")
    
    ' Close the connection
    '
    Conn.Close

    Exit Function
    
Handler:
    Authenticate = "X"

End Function

Public Sub LoadServers()

    ' Turn off screen update
    '
    Application.ScreenUpdating = False
    
    ' Set loading flag
    '
    IsLoading = True
    
    ' Unprotect Workbook
    '
    'UnprotectWorkbook
    
    ' Select Servers sheet
    '
    Sheets("Servers").Select
    
    ' Unprotect Sheet
    '
    UnprotectSheet
    
    ' Clear old information
    '
    Cells.Select
    Selection.ClearContents

    ' Refresh button
    '
    Range("B1").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlCenter
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
        .Value = "Refresh"
    End With
    With Selection.Font
        .Name = "Calibri"
        .FontStyle = "Bold"
        .Size = 11
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
        .ThemeFont = xlThemeFontMinor
    End With
    With Selection.Interior
        .Pattern = xlPatternLinearGradient
        .Gradient.Degree = 90
        .Gradient.ColorStops.Clear
    End With
    With Selection.Interior.Gradient.ColorStops.Add(0)
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
    End With
    With Selection.Interior.Gradient.ColorStops.Add(0.5)
        .ThemeColor = xlThemeColorAccent1
        .TintAndShade = 0
    End With
    With Selection.Interior.Gradient.ColorStops.Add(1)
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
    End With
    
    ' Populate data
    '
    With ActiveSheet.ListObjects.Add(SourceType:=0, Source:=Array(Array( _
        "ODBC;DRIVER=SQL Server;SERVER=PATCDB21\ALLOY_1;Trusted_Connection=Yes;DAT" _
        ), Array("ABASE=Refresh")), Destination:=Range("$A$2")).QueryTable
        .CommandText = Array("{CALL Refresh.dbo.upGetServers}")
        .RowNumbers = False
        .FillAdjacentFormulas = False
        .PreserveFormatting = True
        .BackgroundQuery = True
        .RefreshStyle = xlInsertDeleteCells
        .SavePassword = False
        .SaveData = True
'        .RefreshPeriod = 5
        .PreserveColumnInfo = True
        .ListObject.DisplayName = "Servers"
        'If Application.Version <= 12 Then
        .Refresh BackgroundQuery:=False
    End With
    
    ' Format CreateDT
    '
    Columns(Rows("2").Find(What:="CreateDT", LookIn:=xlFormulas, LookAt:=xlWhole, MatchCase:=False).Column).Select
    Selection.NumberFormat = "m/d/yyyy h:mm AM/PM"
    
    ' Autofit
    '
    Cells.Select
    Cells.EntireColumn.AutoFit
    
    ' Lock Column header cells
    '
'    Rows("1:1").Select
'    Selection.Locked = True
    
    ' Hide ServerID column
    '
    Columns("A:A").Select
    Selection.EntireColumn.Hidden = True
    
    ' Unlock cells for managers only
    '
'    Columns("D:J").Select
    If (Authenticate = "M" And ActiveWorkbook.ReadOnly = False) Then
        Range("D:I,K:M").Select
        Selection.Locked = False
        Selection.FormulaHidden = False
    End If
    
    ' Freeze column headers
    '
    With ActiveWindow
        .SplitColumn = 0
        .SplitRow = 2
    End With
    ActiveWindow.FreezePanes = True
    
    ' Protect Sheet
    '
    ProtectSheet
    
    ' Protect Workbook
    '
    ProtectWorkbook
    
    ' Select first cell
    '
    Range("B3").Select

    ' Set loading flag
    '
    IsLoading = False

    ' Turn on screen update
    '
    Application.ScreenUpdating = True

End Sub

Public Sub LoadServerIPs()

    ' Turn off screen update
    '
    Application.ScreenUpdating = False
    
    ' Set loading flag
    '
    IsLoading = True
    
    ' Unprotect Workbook
    '
    'UnprotectWorkbook
    
    ' Select Servers sheet
    '
    Sheets("IPs").Select
    
    ' Unprotect Sheet
    '
    UnprotectSheet
    
    ' Clear old information
    '
    Cells.Select
    Selection.ClearContents

    ' Refresh button
    '
    Range("B1").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlCenter
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
        .Value = "Refresh"
    End With
    With Selection.Font
        .Name = "Calibri"
        .FontStyle = "Bold"
        .Size = 11
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
        .ThemeFont = xlThemeFontMinor
    End With
    With Selection.Interior
        .Pattern = xlPatternLinearGradient
        .Gradient.Degree = 90
        .Gradient.ColorStops.Clear
    End With
    With Selection.Interior.Gradient.ColorStops.Add(0)
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
    End With
    With Selection.Interior.Gradient.ColorStops.Add(0.5)
        .ThemeColor = xlThemeColorAccent1
        .TintAndShade = 0
    End With
    With Selection.Interior.Gradient.ColorStops.Add(1)
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
    End With
    
    ' Populate data
    '
    With ActiveSheet.ListObjects.Add(SourceType:=0, Source:=Array(Array( _
        "ODBC;DRIVER=SQL Server;SERVER=PATCDB21\ALLOY_1;Trusted_Connection=Yes;DAT" _
        ), Array("ABASE=Refresh")), Destination:=Range("$A$2")).QueryTable
        .CommandText = Array("{CALL Refresh.dbo.upGetServerIPs}")
        .RowNumbers = False
        .FillAdjacentFormulas = False
        .PreserveFormatting = True
        .BackgroundQuery = True
        .RefreshStyle = xlInsertDeleteCells
        .SavePassword = False
        .SaveData = True
'        .RefreshPeriod = 5
        .PreserveColumnInfo = True
        .ListObject.DisplayName = "IPs"
        .Refresh BackgroundQuery:=False
    End With
    
    ' Autofit
    '
    Cells.Select
    Cells.EntireColumn.AutoFit
    
    ' Lock Column header cells
    '
'    Rows("1:1").Select
'    Selection.Locked = True
    
    ' Hide ServerID column
    '
    Columns("A:A").Select
    Selection.EntireColumn.Hidden = True
    
    ' Unlock cells for managers only
    '
'    Columns("D:J").Select
    If (Authenticate = "M" And ActiveWorkbook.ReadOnly = False) Then
        Range("D3:E1000").Select
        Selection.Locked = False
        Selection.FormulaHidden = False
    End If
    
    ' Freeze column headers
    '
    With ActiveWindow
        .SplitColumn = 0
        .SplitRow = 2
    End With
    ActiveWindow.FreezePanes = True
    
    ' Protect Sheet
    '
    ProtectSheet
    
    ' Protect Workbook
    '
    ProtectWorkbook
    
    ' Select first cell
    '
    Range("B3").Select

    ' Set loading flag
    '
    IsLoading = False

    ' Turn on screen update
    '
    Application.ScreenUpdating = True

End Sub

Sub ToggleCutCopyAndPaste(Allow As Boolean)

    ' Activate/deactivate cut, copy, paste and pastespecial menu items
    '
    Call EnableMenuItem(21, Allow) ' cut
    Call EnableMenuItem(19, Allow) ' copy
    Call EnableMenuItem(22, Allow) ' paste
    Call EnableMenuItem(755, Allow) ' pastespecial
     
    ' Activate/deactivate drag and drop ability
    '
    Application.CellDragAndDrop = Allow
     
    ' Activate/deactivate cut, copy, paste and pastespecial shortcut keys
    '
    With Application
        Select Case Allow
        Case Is = False
            .OnKey "^c", "CutCopyPasteDisabled"
            .OnKey "^v", "CutCopyPasteDisabled"
            .OnKey "^x", "CutCopyPasteDisabled"
            .OnKey "+{DEL}", "CutCopyPasteDisabled"
            .OnKey "^{INSERT}", "CutCopyPasteDisabled"
        Case Is = True
            .OnKey "^c"
            .OnKey "^v"
            .OnKey "^x"
            .OnKey "+{DEL}"
            .OnKey "^{INSERT}"
        End Select
    End With
    
End Sub
 
Sub EnableMenuItem(ctlId As Integer, Enabled As Boolean)

    ' Activate/Deactivate specific menu item
    '
    Dim cBar As CommandBar
    Dim cBarCtrl As CommandBarControl
    For Each cBar In Application.CommandBars
        If cBar.Name <> "Clipboard" Then
            Set cBarCtrl = cBar.FindControl(ID:=ctlId, recursive:=True)
            If Not cBarCtrl Is Nothing Then cBarCtrl.Enabled = Enabled
        End If
    Next
    
End Sub

Sub CutCopyPasteDisabled()
     
    'Inform user that the functions have been disabled
    '
    MsgBox "Sorry!  Cutting, copying and pasting have been disabled in this workbook!"
    
End Sub

Sub test()
    ' Declarations
    '
   ' Dim ws As Worksheet
    
    ' Enable copy/cut/paste
    '
    'Call ToggleCutCopyAndPaste(True)
    
    ' Unhide Sheet1
    '
'    Sheets("Sheet1").Visible = xlSheetVisible

    ' Clear Sheets
    '
    'ClearSheets
    
    ' Loop through all worksheets and hide
    '
'    For Each ws In ActiveWorkbook.Worksheets
'        MsgBox ws.Name
        
'        If ws.Name <> "Sheet1" Then
'            ws.Visible = xlVeryHidden
'        End If
'    Next ws
    
End Sub

Sub ProtectSheet()
    
    ' Protect Sheet
    '
    ActiveSheet.Protect Password:="donottouch", DrawingObjects:=True, Contents:=True, Scenarios:=True _
        , AllowFormattingCells:=True, AllowFormattingColumns:=True, _
        AllowFormattingRows:=True, AllowSorting:=True, AllowFiltering:=True

End Sub

Sub ProtectWorkbook()
    
    ' Protect Workbook
    '
    ActiveWorkbook.Protect Structure:=True, Windows:=False, Password:="donottouch"
    
End Sub

Sub UnprotectWorkbook()

    ' Unprotect Workbook
    '
    ActiveWorkbook.Unprotect Password:="donottouch"

End Sub

Sub UnprotectSheet()

    ' Unprotect Sheet
    '
    ActiveSheet.Unprotect "donottouch"

End Sub

Sub ClearSheets()
    Dim i As Integer
    
    For i = 1 To ActiveWorkbook.Sheets.Count
        Sheets(i).Select
        UnprotectSheet
        Cells.Select
        Selection.Clear
        With ActiveWindow
            .SplitColumn = 0
            .SplitRow = 0
        End With
        Range("A1").Select
        ProtectSheet
    Next i
    
End Sub

Sub ClearSheet()

    ' Clear sheet
    '
    Cells.Select
    Range("B1").Activate
    Selection.Delete Shift:=xlUp
    With ActiveWindow
        .SplitColumn = 0
        .SplitRow = 0
    End With
    Range("B1").Select
    
End Sub

Private Sub UpdateDB(ByVal Cmd As ADODB.Command)

    ' Declarations
    '
    Dim Conn As New ADODB.Connection
    
    ' Open connection
    '
    Conn.Open (ConnStr)
    
    ' Stored Procedure
    '
    Cmd.CommandType = adCmdStoredProc
    Cmd.ActiveConnection = Conn

    ' Execute the Stored Procedure
    '
    Cmd.Execute

    ' Close the connection
    '
    Conn.Close

End Sub

Private Sub UpdOriginalRack(ByVal Target As Range)

    ' Declarations
    '
    Dim Cmd As New ADODB.Command
    
    ' Stored Procedure
    '
    Cmd.CommandText = "upUpdateOriginalRack"

    ' ServerID
    '
    Cmd.Parameters.Append Cmd.CreateParameter("ServerID", adInteger, adParamInput, 4, ActiveSheet.Cells(Target.Row, 1))
    
    ' New Value
    '
    Cmd.Parameters.Append Cmd.CreateParameter("OriginalRack", adVarChar, adParamInput, 20, IIf(Len(Target.Value) > 0, Target.Value, Null))
    
    ' Update database
    '
    UpdateDB Cmd
    
End Sub

Private Sub UpdLocation(ByVal Target As Range)

    ' Declarations
    '
    Dim Cmd As New ADODB.Command
    
    ' Stored Procedure
    '
    Cmd.CommandText = "upUpdateLocation"

    ' ServerID
    '
    Cmd.Parameters.Append Cmd.CreateParameter("ServerID", adInteger, adParamInput, 4, ActiveSheet.Cells(Target.Row, 1))
    
    ' New Value
    '
    Cmd.Parameters.Append Cmd.CreateParameter("Location", adVarChar, adParamInput, 50, IIf(Len(Target.Value) > 0, Target.Value, Null))
    
    ' Update database
    '
    UpdateDB Cmd

End Sub

Private Sub UpdApplication(ByVal Target As Range)

    ' Declarations
    '
    Dim Cmd As New ADODB.Command
    
    ' Stored Procedure
    '
    Cmd.CommandText = "upUpdateApplication"

    ' ServerID
    '
    Cmd.Parameters.Append Cmd.CreateParameter("ServerID", adInteger, adParamInput, 4, ActiveSheet.Cells(Target.Row, 1))
    
    ' New Value
    '
    Cmd.Parameters.Append Cmd.CreateParameter("Application", adVarChar, adParamInput, 100, IIf(Len(Target.Value) > 0, Target.Value, Null))
    
    ' Update database
    '
    UpdateDB Cmd

End Sub

Private Sub UpdApplicationOwner(ByVal Target As Range)

    ' Declarations
    '
    Dim Cmd As New ADODB.Command
    
    ' Stored Procedure
    '
    Cmd.CommandText = "upUpdateApplicationOwner"

    ' ServerID
    '
    Cmd.Parameters.Append Cmd.CreateParameter("ServerID", adInteger, adParamInput, 4, ActiveSheet.Cells(Target.Row, 1))
    
    ' New Value
    '
    Cmd.Parameters.Append Cmd.CreateParameter("ApplicationOwner", adVarChar, adParamInput, 50, IIf(Len(Target.Value) > 0, Target.Value, Null))
    
    ' Update database
    '
    UpdateDB Cmd

End Sub

Private Sub UpdNewName(ByVal Target As Range)

    ' Declarations
    '
    Dim Cmd As New ADODB.Command
    
    ' Stored Procedure
    '
    Cmd.CommandText = "upUpdateNewName"

    ' ServerID
    '
    Cmd.Parameters.Append Cmd.CreateParameter("ServerID", adInteger, adParamInput, 4, ActiveSheet.Cells(Target.Row, 1))
    
    ' New Value
    '
    Cmd.Parameters.Append Cmd.CreateParameter("NewName", adVarChar, adParamInput, 20, IIf(Len(Target.Value) > 0, Target.Value, Null))
    
    ' Update database
    '
    UpdateDB Cmd

End Sub

Private Sub UpdNewSerialNumber(ByVal Target As Range)

    ' Declarations
    '
    Dim Cmd As New ADODB.Command
    
    ' Stored Procedure
    '
    Cmd.CommandText = "upUpdateNewSerialNumber"

    ' ServerID
    '
    Cmd.Parameters.Append Cmd.CreateParameter("ServerID", adInteger, adParamInput, 4, ActiveSheet.Cells(Target.Row, 1))
    
    ' New Value
    '
    Cmd.Parameters.Append Cmd.CreateParameter("NewSerialNumber", adVarChar, adParamInput, 15, IIf(Len(Target.Value) > 0, Target.Value, Null))
    
    ' Update database
    '
    UpdateDB Cmd

End Sub

Private Sub UpdNewIP(ByVal Target As Range)

    ' Declarations
    '
    Dim Cmd As New ADODB.Command
    
    ' Stored Procedure
    '
    Cmd.CommandText = "upUpdateNewIP"

    ' ServerID
    '
    Cmd.Parameters.Append Cmd.CreateParameter("IpAddressID", adInteger, adParamInput, 4, ActiveSheet.Cells(Target.Row, 1))
    
    ' New Value
    '
    Cmd.Parameters.Append Cmd.CreateParameter("NewIP", adVarChar, adParamInput, 15, IIf(Len(Target.Value) > 0, Target.Value, Null))
    
    ' Update database
    '
    UpdateDB Cmd

End Sub

Private Sub UpdAdapter(ByVal Target As Range)

    ' Declarations
    '
    Dim Cmd As New ADODB.Command
    
    ' Stored Procedure
    '
    Cmd.CommandText = "upUpdateAdapter"

    ' ServerID
    '
    Cmd.Parameters.Append Cmd.CreateParameter("IpAddressID", adInteger, adParamInput, 4, ActiveSheet.Cells(Target.Row, 1))
    
    ' New Value
    '
    Cmd.Parameters.Append Cmd.CreateParameter("Adapter", adVarChar, adParamInput, 50, IIf(Len(Target.Value) > 0, Target.Value, Null))
    
    ' Update database
    '
    UpdateDB Cmd

End Sub

Private Sub UpdNewRack(ByVal Target As Range)

    ' Declarations
    '
    Dim Cmd As New ADODB.Command
    
    ' Stored Procedure
    '
    Cmd.CommandText = "upUpdateNewRack"

    ' ServerID
    '
    Cmd.Parameters.Append Cmd.CreateParameter("ServerID", adInteger, adParamInput, 4, ActiveSheet.Cells(Target.Row, 1))
    
    ' New Value
    '
    Cmd.Parameters.Append Cmd.CreateParameter("NewRack", adVarChar, adParamInput, 20, IIf(Len(Target.Value) > 0, Target.Value, Null))
    
    ' Update database
    '
    UpdateDB Cmd

End Sub

Private Sub AddNote(ByVal Target As Range)

    ' Declarations
    '
    Dim Cmd As New ADODB.Command
    
    ' Stored Procedure
    '
    Cmd.CommandText = "upAddNote"

    ' ServerID
    '
    Cmd.Parameters.Append Cmd.CreateParameter("ServerID", adInteger, adParamInput, 4, ActiveSheet.Cells(Target.Row, 1))
    
    ' New Value
    '
    Cmd.Parameters.Append Cmd.CreateParameter("Note", adVarChar, adParamInput, 1000, Target.Value)
    
    ' Update database
    '
    UpdateDB Cmd

End Sub

Private Sub UpdStatus(ByVal Target As Range)

    ' Declarations
    '
    Dim Cmd As New ADODB.Command
    
    ' Stored Procedure
    '
    Cmd.CommandText = "upUpdateStatus"

    ' ServerID
    '
    Cmd.Parameters.Append Cmd.CreateParameter("ServerID", adInteger, adParamInput, 4, ActiveSheet.Cells(Target.Row, 1))
    
    ' New Value
    '
    Cmd.Parameters.Append Cmd.CreateParameter("Status", adChar, adParamInput, 1, Target.Value)
    
    ' Update database
    '
    UpdateDB Cmd

End Sub


Sub WorksheetChange(ByVal Target As Range)

    ' Do not process data is loading
    '
    If IsLoading Then Exit Sub
    If Target.Cells.CountLarge > 1 Then Exit Sub
    If Target.Value = CurrVal Then Exit Sub

    ' Take appropriate action
    '
    Select Case LCase(ActiveSheet.Cells(2, Target.Column))
        Case "adapter"
            UpdAdapter Target
        Case "application"
            UpdApplication Target
        Case "applicationowner"
            UpdApplicationOwner Target
        Case "location"
            UpdLocation Target
        Case "newip"
            If (Len(Target.Value) = 0 Or IsValidIPAddress(Target.Value)) Then
                UpdNewIP Target
            Else
                MsgBox "Please enter a valid IP!"
            End If
        Case "newname"
            UpdNewName Target
        Case "newrack"
            UpdNewRack Target
        Case "newserialnumber"
            UpdNewSerialNumber Target
        Case "note"
            If (Len(Target.Value) > 0 And Len(Target.Value) <= 1000) Then
                Application.ScreenUpdating = False
                UnprotectSheet
                ActiveSheet.Cells(Target.Row, Target.Column + 1).Value = Now
                ActiveSheet.Cells(Target.Row, Target.Column + 2).Value = "PANYNJ\" & Environ("UserName")
                ProtectSheet
                Application.ScreenUpdating = True
                AddNote Target
            Else
                MsgBox "Note must not be blank or greater than 1000 characters long."
            End If
        Case "originalrack"
            UpdOriginalRack Target
        Case "status"
            Select Case UCase(Target.Value)
                Case "U", "B", "Q", "R", "D", "E"
                    UpdStatus Target
                Case Else
                    MsgBox "Valid statuses are:" & vbCrLf & _
                    "U for Unbuilt" & vbCrLf & _
                    "B for Built" & vbCrLf & _
                    "Q for QA'd" & vbCrLf & _
                    "R for Racked" & vbCrLf & _
                    "D for Delivered" & vbCrLf & _
                    "E for Excluded" & vbCrLf
            End Select
    End Select
    
End Sub

Sub WorksheetBeforeDoubleClick(ByVal Target As Range, Cancel As Boolean)

    ' Prevent locked cell popup message
    '
    If Target.Locked Then Cancel = True
    
    ' Cell clicks
    '
    Select Case Target.Address
        ' Refresh button
        '
        Case "$B$1"
            Select Case LCase(ActiveSheet.Name)
                Case "servers"
                    LoadServers
                Case "ips"
                    LoadServerIPs
            End Select
    End Select

End Sub

Public Function IsValidIPAddress(ByVal strIPAddress As String) As Boolean
    On Error GoTo Handler
    Dim varAddress As Variant, n As Long, lCount As Long
    varAddress = Split(strIPAddress, ".", , vbTextCompare)
    '//
    If IsArray(varAddress) Then
        For n = LBound(varAddress) To UBound(varAddress)
            lCount = lCount + 1
            varAddress(n) = CByte(varAddress(n))
        Next
        '//
        IsValidIPAddress = (lCount = 4)
    End If
    '//
Handler:
End Function
