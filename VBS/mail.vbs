    Set objConf = CreateObject("CDO.Configuration")
        
    With objConf.fields
      .item("http://schemas.microsoft.com/cdo/configuration/sendusing") = cdoSendUsingPort
      .item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = sMailServerName
      .item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = cdoAnonymous 'cdoBasic
      '.item("http://schemas.microsoft.com/cdo/configuration/sendusername") = "your-username"
      'item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = "your-password"
      .Update
    End With
    
    Set objMsg = CreateObject("CDO.Message")
    objMsg.Configuration = objConf
 
 
    With objMsg
 
      
      '.to = "test@qwerty.com"
      .From = "mail@zxcvbn.com.au"
      .Subject = Space(2) & Trim(tcSubject)
      .TextBody = "what"
       'use .HTMLBody to send HTML email.
  
           .AddAttachment lcAttName
      
      .fields("urn:schemas:mailheader:disposition-notification-to") = "mail@zxcvbn.com.au"
      .fields("urn:schemas:mailheader:return-receipt-to") = "mail@zxcvbn.com.au"
 
      .DSNOptions = cdoDSNSuccessFailOrDelay
      .fields.Update
      .Send
    End With
 
    Set objMsg = Nothing
    NewMail = True
