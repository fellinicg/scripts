    on error resume next
    Set objMessage = CreateObject("CDO.Message")
    with objMessage
        .From = "tfellini@panynj.gov"
        .To = "tfellini@panynj.gov"
        .Subject = "Low Disk Space Update"
        .TextBody = "Server disk space is low"
        .Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
        .Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "mds3aex0m.USIPANYNJ.com"
        .Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        .Configuration.Fields.Update
        .Send
    end with
    Set objMessage = Nothing
