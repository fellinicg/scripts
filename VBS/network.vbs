strComputer = "." 
Set objWMIService = GetObject("winmgmts:\\" _
    & strComputer & "\root\CIMV2") 
Set colItems = objWMIService.ExecQuery( _
    "SELECT * FROM Win32_NetworkAdapterConfiguration",,48) 
For Each objItem in colItems 
    Wscript.Echo "-----------------------------------"
    Wscript.Echo "Win32_NetworkAdapterConfiguration instance"
    Wscript.Echo "-----------------------------------"
    Wscript.Echo "ArpAlwaysSourceRoute: " _
        & objItem.ArpAlwaysSourceRoute
    Wscript.Echo "ArpUseEtherSNAP: " & objItem.ArpUseEtherSNAP
    Wscript.Echo "DatabasePath: " & objItem.DatabasePath
    Wscript.Echo "DeadGWDetectEnabled: " _
        & objItem.DeadGWDetectEnabled
    Wscript.Echo "DefaultIPGateway: " & strDefaultIPGateway
    Wscript.Echo "DefaultTOS: " & objItem.DefaultTOS
    Wscript.Echo "DefaultTTL: " & objItem.DefaultTTL
    Wscript.Echo "Description: " & objItem.Description
    Wscript.Echo "DHCPEnabled: " & objItem.DHCPEnabled
    Wscript.Echo "DHCPLeaseExpires: " & objItem.DHCPLeaseExpires
    Wscript.Echo "DHCPLeaseObtained: " & objItem.DHCPLeaseObtained
    Wscript.Echo "DHCPServer: " & objItem.DHCPServer
    Wscript.Echo "DNSDomain: " & objItem.DNSDomain
    Wscript.Echo "DNSDomainSuffixSearchOrder: " _
        & strDNSDomainSuffixSearchOrder
    Wscript.Echo "DNSEnabledForWINSResolution: " _
        & objItem.DNSEnabledForWINSResolution
    Wscript.Echo "DNSHostName: " & objItem.DNSHostName
    Wscript.Echo "DNSServerSearchOrder: " _
        & strDNSServerSearchOrder
    Wscript.Echo "DomainDNSRegistrationEnabled: " _
        & objItem.DomainDNSRegistrationEnabled
    Wscript.Echo "ForwardBufferMemory: " _
        & objItem.ForwardBufferMemory
    Wscript.Echo "FullDNSRegistrationEnabled: " _
        & objItem.FullDNSRegistrationEnabled
    Wscript.Echo "GatewayCostMetric: " & strGatewayCostMetric
    Wscript.Echo "IGMPLevel: " & objItem.IGMPLevel
    Wscript.Echo "Index: " & objItem.Index
    Wscript.Echo "IPAddress: " & strIPAddress
    Wscript.Echo "IPConnectionMetric: " _
        & objItem.IPConnectionMetric
    Wscript.Echo "IPEnabled: " & objItem.IPEnabled
    Wscript.Echo "IPFilterSecurityEnabled: " _
        & objItem.IPFilterSecurityEnabled
    Wscript.Echo "IPPortSecurityEnabled: " _
        & objItem.IPPortSecurityEnabled
    Wscript.Echo "IPSecPermitIPProtocols: " _
        & strIPSecPermitIPProtocols
    Wscript.Echo "IPSecPermitTCPPorts: " _
        & strIPSecPermitTCPPorts
    Wscript.Echo "IPSecPermitUDPPorts: " _
        & strIPSecPermitUDPPorts
    Wscript.Echo "IPSubnet: " & strIPSubnet
    Wscript.Echo "IPUseZeroBroadcast: " _
        & objItem.IPUseZeroBroadcast
    Wscript.Echo "IPXAddress: " & objItem.IPXAddress
    Wscript.Echo "IPXEnabled: " & objItem.IPXEnabled
    Wscript.Echo "IPXFrameType: " & strIPXFrameType
    Wscript.Echo "IPXNetworkNumber: " & strIPXNetworkNumber
    Wscript.Echo "IPXVirtualNetNumber: " _
        & objItem.IPXVirtualNetNumber
    Wscript.Echo "KeepAliveInterval: " _
        & objItem.KeepAliveInterval
    Wscript.Echo "KeepAliveTime: " & objItem.KeepAliveTime
    Wscript.Echo "MACAddress: " & objItem.MACAddress
    Wscript.Echo "MTU: " & objItem.MTU
    Wscript.Echo "NumForwardPackets: " _
        & objItem.NumForwardPackets
    Wscript.Echo "PMTUBHDetectEnabled: " _
        & objItem.PMTUBHDetectEnabled
    Wscript.Echo "PMTUDiscoveryEnabled: " _
        & objItem.PMTUDiscoveryEnabled
    Wscript.Echo "ServiceName: " & objItem.ServiceName
    Wscript.Echo "SettingID: " & objItem.SettingID
    Wscript.Echo "TcpipNetbiosOptions: " _
        & objItem.TcpipNetbiosOptions
    Wscript.Echo "TcpMaxConnectRetransmissions: " _
        & objItem.TcpMaxConnectRetransmissions
    Wscript.Echo "TcpMaxDataRetransmissions: " _
        & objItem.TcpMaxDataRetransmissions
    Wscript.Echo "TcpNumConnections: " _
        & objItem.TcpNumConnections
    Wscript.Echo "TcpUseRFC1122UrgentPointer: " _
        & objItem.TcpUseRFC1122UrgentPointer
    Wscript.Echo "TcpWindowSize: " & objItem.TcpWindowSize
    Wscript.Echo "WINSEnableLMHostsLookup: " _
        & objItem.WINSEnableLMHostsLookup
    Wscript.Echo "WINSHostLookupFile: " _
        & objItem.WINSHostLookupFile
    Wscript.Echo "WINSPrimaryServer: " _
        & objItem.WINSPrimaryServer
    Wscript.Echo "WINSScopeID: " & objItem.WINSScopeID
    Wscript.Echo "WINSSecondaryServer: " _
        & objItem.WINSSecondaryServer
Next