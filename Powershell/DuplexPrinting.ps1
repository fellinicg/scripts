$host.Runspace.ThreadOptions = "ReuseThread"

try {Add-Type -AssemblyName System.Printing}
catch  {Write-Warning ".Net feature not installed."} 

# Permissions
#
$permissions = [System.Printing.PrintSystemDesiredAccess]::AdministrateServer
$queueperms = [System.Printing.PrintSystemDesiredAccess]::AdministratePrinter


# Print server
#
$server = new-object System.Printing.PrintServer -argumentList $permissions

# Print Queues
#
$queues = $server.GetPrintQueues(@([System.Printing.EnumeratedPrintQueueTypes]::Shared))
#$lanier = $queues | where {$_.ShareName -Ilike "*LANIER*"}


# Loop through print servers
#
foreach ($q in $queues) {
     # Get edit Permissions on the Queue
     #
     $q2 = new-object System.Printing.PrintQueue -argumentList $server,$q.Name,1,$queueperms        
     
     # Set DefaultPrintTicket for Laniers that are duplex capable
     #
     #if ($q2.GetPrintCapabilities().DuplexingCapability.Contains([System.Printing.Duplexing]::TwoSidedLongEdge) -and $q2.QueueDriver.Name.contains("PCL6 Driver for Universal Print")) {
     if ($q2.GetPrintCapabilities().DuplexingCapability.Contains([System.Printing.Duplexing]::TwoSidedLongEdge)) {
         $q2.DefaultPrintTicket.Duplexing = [System.Printing.Duplexing]::TwoSidedLongEdge		
         $q2.commit()	
     }
}

