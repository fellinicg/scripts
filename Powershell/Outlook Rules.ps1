$outlook = New-Object -ComObject outlook.application

$outlook.session.DefaultStore.GetRules() | where {$_.Name.StartsWith("*") -and $_.IsLocalRule -eq $FALSE}