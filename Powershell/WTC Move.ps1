import-module activedirectory
$thedate = (get-date).year.tostring() + (get-date -format MM).tostring() + (get-date -format dd).tostring()

$users = get-content "\\teledsrv1\data\TSD\HOME\rcannon\Scripts\ActiveDirectory\4WTC\inout\$thedate-Users.txt"
$comps = get-content "\\teledsrv1\data\TSD\HOME\rcannon\Scripts\ActiveDirectory\4WTC\inout\$thedate-Computers.txt"

$OutFile = "C:\users.csv"
foreach($user in $users)
{	
    $object = get-aduser $user -properties CanonicalName
    Add-Content -Value ($object.GivenName + "," + $object.Surname  + "," + $object.CanonicalName) -Path $OutFile
}

$OutFile = "C:\computers.csv"
foreach($comp in $comps)
{	
    $object = get-adcomputer $comp.trim() -properties CanonicalName
    Add-Content -Value ($object.Name + "," + $object.CanonicalName) -Path $OutFile
}
    