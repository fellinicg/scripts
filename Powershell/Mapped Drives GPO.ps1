Import-Module -Name grouppolicy

$gpos = get-gpo -all | where {$_.displayname -like "*INF.U.OSEC.PARK AVE S.MAPPED DRIVES*"} | select displayname 

foreach ($gpo in $gpos) {
    $gpodet = [xml](get-gporeport -name $gpo.displayname -reporttype xml)
    foreach ($drive in $gpodet.GPO.User.extensiondata.Extension.DriveMapSettings.drive) {
        write-host $gpo.displayname, $drive.properties.letter, $drive.properties.label
    }
}

#foreach { [xml](get-gporeport -name $_.displayname -reporttype xml) } | foreach {$_.GPO.User.extensiondata.Extension.DriveMapSettings.drive.properties}
#foreach {$_.drive.properties.path}

#[xml]$gpo = get-gporeport -name "INF.U.PCP.115BWAY.MAPPED DRIVES" -reporttype xml