﻿# Declarations
#
$workingpath = "C:\Users\tfellini\Documents\Timesheet\"
$xltemplate = "template.xlsx"
$person = "Todd Fellini"
$targetpath = "\\teledsrv1\data\tsd\share\PA Orders\TEC Time Tracking\" + $person + "\" + (get-date -format 'yyyyMM') + "\"
$task = @( "Meeting","Team Management","Cyber Security Discussion","Call","Client Management","Client Issue","Team Issue","Paperwork" )
$statuscall = @( "Monday","Wednesday","Friday" )

# Create target path, if necessary
#
if (!(test-path $targetpath)) { new-item $targetpath -type directory }

# Open template
#
$xl = (new-object -comobject excel.application).workbooks.open($workingpath + $xltemplate)

# Insert name
#
$xl.worksheets.item(1).cells.item(1,2).formulalocal = $person

# Insert today's date
#
$xl.worksheets.item(1).cells.item(2,2).formulalocal = get-date -format 'MM/dd/yy'

# Loop over cells that need to be populated (edit template.xlsx)
#
for ($i=9; $i -le 15; $i++) {
    switch ($i) {
        13 { $xl.worksheets.item(1).cells.item($i,2).formulalocal = "Lunch" }
        15 { $xl.worksheets.item(1).cells.item($i,2).formulalocal = $(if ($statuscall -contains ((get-date).dayofweek)) { "Status Call" } else { get-random $task }) }
        default { $xl.worksheets.item(1).cells.item($i,2).formulalocal = get-random $task }
    }
}

# Sleep for a random number of minutes
#
start-sleep -s ((get-random -minimum 1 -maximum 5) * 60)

# Save the workbook
#
$xl.saveas($targetpath + (get-date -format 'yyyyMMdd') + " - " + $person + ".xlsx" )

# Close Excel
#
$xl.parent.quit()

# Cleanup
#
([System.Runtime.InteropServices.Marshal]::ReleaseComObject([System.__ComObject]$xl) -gt 0)