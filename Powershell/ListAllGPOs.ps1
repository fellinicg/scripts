param(
      $domain = $env:userDNSdomain,
      [switch]$query,
      [switch]$verbose,
      [switch]$help,
      [switch]$examples,
      [switch]$min,
      [switch]$full
      ) #end param

# Begin Functions

function Get-HelpTopic()
{
 $descriptionText= `
@"
 NAME: ListAllGPOs.ps1
 DESCRIPTION:
 Returns detailed information about all GPOs in 
 the selected domain
 
 PARAMETERS: 
 -domain domain to return GPO information
 -query perform the query. By default returns name
  and guid of GPO
 -verbose returns extended GPO information
 -help prints help description and parameters file
 -examples prints only help examples of syntax
 -full prints complete help information
 -min prints minimal help. Modifies -help

"@ #end descriptionText

$examplesText= `
@"

 SYNTAX:
 ListAllGPOs.ps1 

 Displays an error missing query, and calls help

 ListAllGPOs.ps1  -query

 Lists name and guid of the GPOs in current domain 

 ListAllGPOs.ps1 -query -domain "nwtraders.com" -verbose
 Lists detailed GPO information about the GPOs in
 nwtraders.com domain 
 
 ListAllGPOs.ps1 -help

 Prints the help topic for the script

 ListAllGPOs.ps1 -help -full

 Prints full help topic for the script

 ListAllGPOs.ps1 -help -examples

 Prints only the examples for the script

 ListAllGPOs.ps1 -examples

 Prints only the examples for the script
"@ #end examplesText

$remarks = `
"
REMARKS
     For more information, type: $($MyInvocation.ScriptName) -help -full
" #end remarks

  if($examples) { $examplesText ; $remarks ; exit }
  if($full)     { $descriptionText; $examplesText ; exit } 
  if($min)      { $descriptionText ; exit }
  $descriptionText; $remarks 
  exit
} #end Get-HelpTopic function

function New-Line (
                  $strIN,
                  $char = "=",
                  $sColor = "Yellow",
                  $uColor = "darkYellow",
                  [switch]$help
                 )
{
 if($help)
  {
    $local:helpText = `
@"
     New-Line accepts inputs: -strIN for input string and -char for seperator
     -sColor for the string color, and -uColor for the underline color. Only 
     the -strIn is required. The others have the following default values:
     -char: =, -sColor: Yellow, -uColor: darkYellow
     Example:
     New-Line -strIN "Hello world"
     New-Line -strIn "Morgen welt" -char "-" -sColor "blue" -uColor "yellow"
     New-Line -help
"@
   $local:helpText
   break
  } #end New-Line help
  
 $strLine= $char * $strIn.length
 Write-Host -ForegroundColor $sColor $strIN 
 Write-Host -ForegroundColor $uColor $strLine
} #end New-Line function

Function Get-GPO()
{
 New-Line("Listing GPOs from $domain")
 $gpm=New-Object -ComObject gpmgmt.gpm
 $constants = $gpm.GetConstants()
 $gpmDomain = $gpm.GetDomain($domain,$null,$constants.useanydc)
 $gpmSearchCriteria = $gpm.CreateSearchCriteria()
 $gpo=$gpmdomain.SearchGPOs($gpmSearchCriteria)
 if($verbose)
  { 
   $gpo 
  }
 ELSE
  {
   foreach($ogpo in $gpo)
    {
     $hash += @{ $ogpo.ID = $ogpo.DisplayName }
    }
     format-table -inputobject $hash -autosize
  } #end else
 exit
} #end Get-GPO

# Entry Point

if($help)      { Get-HelpTopic }
if($examples)  { Get-HelpTopic }
if($full)      { Get-HelpTopic }
if($query)     { Get-GPO }
if(!$query)    { "Missing query" ; Get-HelpTopic }