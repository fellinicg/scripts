$host.Runspace.ThreadOptions = "ReuseThread"

Import-module ActiveDirectory

# Declarations
#
$LogFile = "disabledusers_" + (Get-Date -Format yyyyMMdd_HHMMss) + ".csv"
$DisabledUsersGroup = get-adgroup "DisabledUsers"
$DisabledUsersGroupID = (get-adgroup $DisabledUsersGroup -properties primarygrouptoken).primarygrouptoken
$Output = @()

# Retrieve all disabled users that have not already been processed
#
$Output = get-aduser -filter {(enabled -eq 'false' -and primarygroupid -ne $DisabledUsersGroupID)} -property * | select-object GivenName, Surname, name, @{n="MemberOf";e={($_.MemberOf | % {(get-adobject $_).Name}) -join '; '}}, CanonicalName, SID

# Backup to CSV
#
$Output | export-csv $LogFile -notype

# Loop over unprocessed disabled users
#
foreach ($User in $Output) {
    # Get user's primary group
    #
    $WorkingUser = get-aduser $User.SID -properties primarygroup,memberof

    # Add user to DisabledUsers group
    #
    add-adgroupmember $DisabledUsersGroup -member $WorkingUser
    
    # Set user's primary group to DisabledUsers
    #
    set-adobject $WorkingUser -replace @{primarygroupid=(get-adgroup $DisabledUsersGroup -properties primarygrouptoken).primarygrouptoken}
    
    # Remove user from all groups except primary (DisabledUsers)
    #
    #get-adprincipalgroupmembership -identity $WorkingUser | %{if($_.samaccountname -ne $DisabledUsersGroup.samaccountname) {remove-adprincipalgroupmembership -identity $WorkingUser -memberof $_.samaccountname -confirm:$false}}
    #(Get-ADUser $WorkingUser -Properties memberof).memberof | %{$_.split(",")[0].split("=")[1]} | %{if($_ -ne $DisabledUsersGroup.samaccountname) {remove-adprincipalgroupmembership -identity $WorkingUser -memberof ($_.replace("/", "_")) -confirm:$false}}
    (get-aduser $WorkingUser -properties memberof).memberof | %{$_.split(",")[0].split("=")[1]} | %{if($_ -ne $DisabledUsersGroup.samaccountname) {remove-adprincipalgroupmembership -identity $WorkingUser -memberof $_ -confirm:$false}}
    
    # Move user to Disabled Users OU
    #
    move-adobject $WorkingUser -targetpath 'OU=Disabled Users,OU=PANYNJ Users,DC=PACORP,DC=PANYNJ,DC=GOV'
}
#Set-ADObject $user -replace @{PrimaryGroupID=(Get-ADGroup $DisabledUsersGroup -Properties primaryGroupToken).primaryGroupToken}
#$user = Get-ADUser $output[0].name -properties PrimaryGroup
#get-adprincipalgroupmembership -identity $User | %{remove-adprincipalgroupmembership -identity $User -memberof $_.samaccountname -confirm:$false}
#Get-ADObject -filter {samaccountname -like "*admin*"} -properties * | select -first 5 | format-list