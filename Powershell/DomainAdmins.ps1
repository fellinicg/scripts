Import-module ActiveDirectory

$output = @()
#get-adgroupmember "Domain Admins" -recursive  | select-object name, distinguishedName | sort-object name | foreach {
#        $output += Get-ADUser -Filter * -SearchBase $_.distinguishedName -property * | select-object GivenName, Surname, Name, distinguishedName
#}
get-adgroupmember "Domain Admins" -recursive  | select-object name, distinguishedName | sort-object name | foreach {
        $output += Get-ADUser -Filter * -SearchBase $_.distinguishedName -property * | select-object  Surname, GivenName, Name, Description, Enabled
}
$output | Export-Csv domainadms.csv -notype