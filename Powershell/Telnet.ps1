﻿$host.Runspace.ThreadOptions = "ReuseThread"

# Declarations
#
#[String[]]$commands = @("sh access-lists")
[int]$waittime = 1000
[string]$outputpath = "c:\"
[string]$logfile = (get-date -Format yyyyMMdd_HHMMss) + ".log"
[string]$errorlogfile = (get-date -Format yyyyMMdd_HHMMss) + "-Error.log"

# Open dialog box
#
Add-Type -AssemblyName System.Windows.Forms
$openFileDialog = New-Object System.Windows.Forms.OpenFileDialog
$openFileDialog.ShowHelp = $true

# Prompt for file that contains a list of remote hosts
#
$openFileDialog.Title = "Select a Remote Hosts File"
$openFileDialog.ShowDialog() > $null
#if (!test-path $openFileDialog.FileName) {
    [String[]]$remotehosts = get-content $openFileDialog.Filename
#}
#else {
#    msgbox test-path $openFileDialog.FileName + " does not exists.  Please try again!"
#}

# Prompt for port number
#
Add-Type -AssemblyName Microsoft.VisualBasic
[string]$port = [Microsoft.VisualBasic.Interaction]::InputBox("Please enter port number", "Remote Port", "23")

# Prompt for file that contains commands
#
$openFileDialog.Title = "Select a Commands File"
$openFileDialog.ShowDialog() > $null
[String[]]$commands = get-content $openFileDialog.Filename

break

# Get credentials
#
$cred = get-credential -Message "Network Device Credentials"
$commands = @($cred.UserName, $cred.GetNetworkCredential().Password) + $commands


# Loop over hosts
#
foreach ($remotehost in $remotehosts) {

    try {

        # Open socket
        #
        $socket = new-object System.Net.Sockets.TcpClient($remotehost, $port)

        # Test socket
        #
        if ($socket) {
            $stream = $socket.GetStream()
            $writer = new-object System.IO.StreamWriter($stream)
            $buffer = new-object System.Byte[] 1024 
            $encoding = new-object System.Text.AsciiEncoding

            # Issue commands
            #
            foreach ($command in $commands) {
                $writer.WriteLine($command) 
                $writer.Flush()
                start-sleep -Milliseconds $WaitTime
            }

            # Add extra wait time for last command
            #
            Start-Sleep -Milliseconds ($WaitTime * 4)

            # Save results to variable
            #
            while ($stream.DataAvailable) {
                $read = $stream.Read($buffer, 0, 1024) 
                $result += ($encoding.GetString($buffer, 0, $read))
            }
        }
        else {   
            $result += "Unable to connect to host: $($RemoteHost):$Port"
        }

        # Output results
        #
        $result #| out-file ($outputpath + $logfile)
    }
    catch [Exception] {
        $_.Exception.Message | out-file ($outputpath + $errorlogfile)
    }

}