$computername = "MJ94Z41.wks.pacorp.panynj.gov"
$computername = "MJ195HR.wks.pacorp.panynj.gov"
$computername = "127.0.0.1"
(Get-WmiObject -computerName $computername Win32_Service -Filter "Name='RemoteRegistry'").StartService()
$reg = [microsoft.win32.registrykey]::OpenRemoteBaseKey('LocalMachine', $computername) 

$UninstallKey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall"
$regkey = $reg.OpenSubKey($UninstallKey) 
$subkeys = $regkey.GetSubKeyNames() 
for($i=0; $i -lt $subkeys.count; $i++){if (!$subkeys[$i].startswith("SOFTWARE")) {$subkeys[$i] = ($UninstallKey + "\\" + $subkeys[$i])}}

$UninstallKey = "SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall"
$regkey = $reg.OpenSubKey($UninstallKey) 
$subkeys += $regkey.GetSubKeyNames() 
for($i=0; $i -lt $subkeys.count; $i++){if (!$subkeys[$i].startswith("SOFTWARE")) {$subkeys[$i] = ($UninstallKey + "\\" + $subkeys[$i])}}

$array = @()
$array2 = @()

foreach($key in $subkeys){
    $thisKey = $UninstallKey+"\\"+$key 
    $thisSubKey = $reg.OpenSubKey($key)
    $displayname = $thisSubKey.GetValue("DisplayName")

    switch -wildcard ($displayname) {
        "*KB2825644*" { $array += $displayname }
        "*KB2768023*" { $array += $displayname }
        "*2007 Microsoft Office Suite Service Pack 3 (SP3)*" { $array += $displayname }
    }
#    $obj = New-Object PSObject
#    $obj | Add-Member -MemberType NoteProperty -Name "ComputerName" -Value $computername
#    $obj | Add-Member -MemberType NoteProperty -Name "DisplayName" -Value $($thisSubKey.GetValue("DisplayName"))
#    $obj | Add-Member -MemberType NoteProperty -Name "DisplayVersion" -Value $($thisSubKey.GetValue("DisplayVersion"))
#    $obj | Add-Member -MemberType NoteProperty -Name "InstallLocation" -Value $($thisSubKey.GetValue("InstallLocation"))
#    $obj | Add-Member -MemberType NoteProperty -Name "Publisher" -Value $($thisSubKey.GetValue("Publisher"))
#    $obj | Add-Member -MemberType NoteProperty -Name "URLInfoAbout" -Value $($thisSubKey.GetValue("URLInfoAbout"))
#    $array2 += $obj
} 

(Get-WmiObject -computerName $computername Win32_Service -Filter "Name='RemoteRegistry'").StopService()

$array
#$array | Where-Object { $_.DisplayName } | select ComputerName, DisplayName, DisplayVersion, Publisher | ft -auto
