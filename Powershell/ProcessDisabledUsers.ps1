$host.Runspace.ThreadOptions = "ReuseThread"

Import-module ActiveDirectory

# Declarations
#
$LogFile = "disabledusers_" + (Get-Date -Format yyyyMMdd_HHMMss) + ".csv"
$DisabledUsersGroup = get-adgroup "DisabledUsers"
$DisabledUsersGroupID = (get-adgroup $DisabledUsersGroup -properties primarygrouptoken).primarygrouptoken
$PeoplesoftFile = "\\patcdb32\c$\Feeds\AD_LISTING2.TXT"
$Output = @()

# Retrieve all disabled users with an employeeID and that have not already been processed
#
#$Output = get-aduser -filter {(enabled -eq 'false' -and primarygroupid -ne $DisabledUsersGroupID)} -property * | select-object GivenName, Surname, name, @{n="MemberOf";e={($_.MemberOf | % {(get-adobject $_).Name}) -join '; '}}, CanonicalName, SID, employeeID
$Output = get-aduser -filter {(enabled -eq 'false' -and primarygroupid -ne $DisabledUsersGroupID)} -properties * | ? {($_.employeeID -ne $null -and $_.employeeID.trim() -ne "")} | select-object GivenName, Surname, name, @{n="MemberOf";e={($_.MemberOf | % {(get-adobject $_).Name}) -join '; '}}, CanonicalName, SID, employeeID

# Backup to CSV
#
#$Output | export-csv $LogFile -notype

# Import EMPLIDs of terminated employees from Peoplesoft file
#
$EMPLIDs = Import-Csv -Delimiter "," -Path $PeoplesoftFile | Where {$_.TERMDT.trim() -ne ""} | select EMPLID

# Column headers
#
@($Output[0].PSObject.Properties |  select name | ConvertTo-Csv -NoTypeInformation) -join ',' > $LogFile

# Loop over unprocessed disabled users
#
foreach ($User in $Output) {

    # Only process Peoplesoft terminated employees
    #
    if (@($EMPLIDs | where { $_.EMPLID -eq $User.employeeID}).count -eq 1) {
    
        # Write employee information to log
        #
        $User | ConvertTo-Csv -NoTypeInformation | select -skip 1 >> $LogFile

        # Get user's primary group
        #
        $WorkingUser = get-aduser $User.SID -properties primarygroup,memberof

        # Add user to DisabledUsers group
        #
        add-adgroupmember $DisabledUsersGroup -member $WorkingUser
        
        # Set user's primary group to DisabledUsers
        #
        set-adobject $WorkingUser -replace @{primarygroupid=(get-adgroup $DisabledUsersGroup -properties primarygrouptoken).primarygrouptoken}
        
        # Remove user from all groups except primary (DisabledUsers)
        #
        (get-aduser $WorkingUser -properties memberof).memberof | %{$_.split(",")[0].split("=")[1]} | %{if($_ -ne $DisabledUsersGroup.samaccountname) {remove-adprincipalgroupmembership -identity $WorkingUser -memberof $_ -confirm:$false}}
        
        # Move user to Disabled Users OU
        #
        move-adobject $WorkingUser -targetpath 'OU=Disabled Users,OU=PANYNJ Users,DC=PACORP,DC=PANYNJ,DC=GOV'
    }
}