$zone = "pacorp.panynj.gov"
$dnsserver = "teleis29"

# MONTAV
#
$term = "MONTAV"
$records = Get-DnsServerResourceRecord -ZoneName $zone -ComputerName $dnsserver -RRType A | ? TimeStamp -ne $null | ? HostName -like $term* | ? HostName -notlike "*.pacorp.panynj.gov"
foreach ($record in $records) {
    if(Test-Connection -cn $record.HostName -BufferSize 16 -Count 1 -ea 0 -quiet) {
        if (Get-WMIObject -class Win32_NetworkAdapterConfiguration -Filter IPEnabled=True -ComputerName $record.HostName | Where-Object {$_.IPaddress -contains $record.RecordData.IPv4Address.IPAddressToString} | Where-Object {$_.DHCPEnabled -eq $false}) {
            Set-DnsServerResourceRecord -OldInputObject $record -NewInputObject $record -ComputerName $dnsServer -ZoneName $zone
        }
    }
}

# PATCAS
#
$term = "PATCAS"
$records = Get-DnsServerResourceRecord -ZoneName $zone -ComputerName $dnsserver -RRType A | ? TimeStamp -ne $null | ? HostName -like $term* | ? HostName -notlike "*.pacorp.panynj.gov"
foreach ($record in $records) {
    if(Test-Connection -cn $record.HostName -BufferSize 16 -Count 1 -ea 0 -quiet) {
        if (Get-WMIObject -class Win32_NetworkAdapterConfiguration -Filter IPEnabled=True -ComputerName $record.HostName | Where-Object {$_.IPaddress -contains $record.RecordData.IPv4Address.IPAddressToString} | Where-Object {$_.DHCPEnabled -eq $false}) {
            Set-DnsServerResourceRecord -OldInputObject $record -NewInputObject $record -ComputerName $dnsServer -ZoneName $zone
        }
    }
}

# PATCAV
#
$term = "PATCAV"
$records = Get-DnsServerResourceRecord -ZoneName $zone -ComputerName $dnsserver -RRType A | ? TimeStamp -ne $null | ? HostName -like $term* | ? HostName -notlike "*.pacorp.panynj.gov"
foreach ($record in $records) {
    if(Test-Connection -cn $record.HostName -BufferSize 16 -Count 1 -ea 0 -quiet) {
        if (Get-WMIObject -class Win32_NetworkAdapterConfiguration -Filter IPEnabled=True -ComputerName $record.HostName | Where-Object {$_.IPaddress -contains $record.RecordData.IPv4Address.IPAddressToString} | Where-Object {$_.DHCPEnabled -eq $false}) {
            Set-DnsServerResourceRecord -OldInputObject $record -NewInputObject $record -ComputerName $dnsServer -ZoneName $zone
        }
    }
}

# PATCDB
#
$term = "PATCDB"
$records = Get-DnsServerResourceRecord -ZoneName $zone -ComputerName $dnsserver -RRType A | ? TimeStamp -ne $null | ? HostName -like $term* | ? HostName -notlike "*.pacorp.panynj.gov"
foreach ($record in $records) {
    if(Test-Connection -cn $record.HostName -BufferSize 16 -Count 1 -ea 0 -quiet) {
        if (Get-WMIObject -class Win32_NetworkAdapterConfiguration -Filter IPEnabled=True -ComputerName $record.HostName | Where-Object {$_.IPaddress -contains $record.RecordData.IPv4Address.IPAddressToString} | Where-Object {$_.DHCPEnabled -eq $false}) {
            Set-DnsServerResourceRecord -OldInputObject $record -NewInputObject $record -ComputerName $dnsServer -ZoneName $zone
        }
    }
}

# PATCDV
#
$term = "PATCDV"
$records = Get-DnsServerResourceRecord -ZoneName $zone -ComputerName $dnsserver -RRType A | ? TimeStamp -ne $null | ? HostName -like $term* | ? HostName -notlike "*.pacorp.panynj.gov"
foreach ($record in $records) {
    if(Test-Connection -cn $record.HostName -BufferSize 16 -Count 1 -ea 0 -quiet) {
        if (Get-WMIObject -class Win32_NetworkAdapterConfiguration -Filter IPEnabled=True -ComputerName $record.HostName | Where-Object {$_.IPaddress -contains $record.RecordData.IPv4Address.IPAddressToString} | Where-Object {$_.DHCPEnabled -eq $false}) {
            Set-DnsServerResourceRecord -OldInputObject $record -NewInputObject $record -ComputerName $dnsServer -ZoneName $zone
        }
    }
}

# PATCIS
#
$term = "PATCIS"
$records = Get-DnsServerResourceRecord -ZoneName $zone -ComputerName $dnsserver -RRType A | ? TimeStamp -ne $null | ? HostName -like $term* | ? HostName -notlike "*.pacorp.panynj.gov"
foreach ($record in $records) {
    if(Test-Connection -cn $record.HostName -BufferSize 16 -Count 1 -ea 0 -quiet) {
        if (Get-WMIObject -class Win32_NetworkAdapterConfiguration -Filter IPEnabled=True -ComputerName $record.HostName | Where-Object {$_.IPaddress -contains $record.RecordData.IPv4Address.IPAddressToString} | Where-Object {$_.DHCPEnabled -eq $false}) {
            Set-DnsServerResourceRecord -OldInputObject $record -NewInputObject $record -ComputerName $dnsServer -ZoneName $zone
        }
    }
}

# PATCIV
#
$term = "PATCIV"
$records = Get-DnsServerResourceRecord -ZoneName $zone -ComputerName $dnsserver -RRType A | ? TimeStamp -ne $null | ? HostName -like $term* | ? HostName -notlike "*.pacorp.panynj.gov"
foreach ($record in $records) {
    if(Test-Connection -cn $record.HostName -BufferSize 16 -Count 1 -ea 0 -quiet) {
        if (Get-WMIObject -class Win32_NetworkAdapterConfiguration -Filter IPEnabled=True -ComputerName $record.HostName | Where-Object {$_.IPaddress -contains $record.RecordData.IPv4Address.IPAddressToString} | Where-Object {$_.DHCPEnabled -eq $false}) {
            Set-DnsServerResourceRecord -OldInputObject $record -NewInputObject $record -ComputerName $dnsServer -ZoneName $zone
        }
    }
}

# TELEAS
#
$term = "TELEAS"
$records = Get-DnsServerResourceRecord -ZoneName $zone -ComputerName $dnsserver -RRType A | ? TimeStamp -ne $null | ? HostName -like $term* | ? HostName -notlike "*.pacorp.panynj.gov"
foreach ($record in $records) {
    if(Test-Connection -cn $record.HostName -BufferSize 16 -Count 1 -ea 0 -quiet) {
        if (Get-WMIObject -class Win32_NetworkAdapterConfiguration -Filter IPEnabled=True -ComputerName $record.HostName | Where-Object {$_.IPaddress -contains $record.RecordData.IPv4Address.IPAddressToString} | Where-Object {$_.DHCPEnabled -eq $false}) {
            Set-DnsServerResourceRecord -OldInputObject $record -NewInputObject $record -ComputerName $dnsServer -ZoneName $zone
        }
    }
}

# TELEAV
#
$term = "TELEAV"
$records = Get-DnsServerResourceRecord -ZoneName $zone -ComputerName $dnsserver -RRType A | ? TimeStamp -ne $null | ? HostName -like $term* | ? HostName -notlike "*.pacorp.panynj.gov"
foreach ($record in $records) {
    if(Test-Connection -cn $record.HostName -BufferSize 16 -Count 1 -ea 0 -quiet) {
        if (Get-WMIObject -class Win32_NetworkAdapterConfiguration -Filter IPEnabled=True -ComputerName $record.HostName | Where-Object {$_.IPaddress -contains $record.RecordData.IPv4Address.IPAddressToString} | Where-Object {$_.DHCPEnabled -eq $false}) {
            Set-DnsServerResourceRecord -OldInputObject $record -NewInputObject $record -ComputerName $dnsServer -ZoneName $zone
        }
    }
}

# TELEDB
#
$term = "TELEDB"
$records = Get-DnsServerResourceRecord -ZoneName $zone -ComputerName $dnsserver -RRType A | ? TimeStamp -ne $null | ? HostName -like $term* | ? HostName -notlike "*.pacorp.panynj.gov"
foreach ($record in $records) {
    if(Test-Connection -cn $record.HostName -BufferSize 16 -Count 1 -ea 0 -quiet) {
        if (Get-WMIObject -class Win32_NetworkAdapterConfiguration -Filter IPEnabled=True -ComputerName $record.HostName | Where-Object {$_.IPaddress -contains $record.RecordData.IPv4Address.IPAddressToString} | Where-Object {$_.DHCPEnabled -eq $false}) {
            Set-DnsServerResourceRecord -OldInputObject $record -NewInputObject $record -ComputerName $dnsServer -ZoneName $zone
        }
    }
}

# TELEDV
#
$term = "TELEDV"
$records = Get-DnsServerResourceRecord -ZoneName $zone -ComputerName $dnsserver -RRType A | ? TimeStamp -ne $null | ? HostName -like $term* | ? HostName -notlike "*.pacorp.panynj.gov"
foreach ($record in $records) {
    if(Test-Connection -cn $record.HostName -BufferSize 16 -Count 1 -ea 0 -quiet) {
        if (Get-WMIObject -class Win32_NetworkAdapterConfiguration -Filter IPEnabled=True -ComputerName $record.HostName | Where-Object {$_.IPaddress -contains $record.RecordData.IPv4Address.IPAddressToString} | Where-Object {$_.DHCPEnabled -eq $false}) {
            Set-DnsServerResourceRecord -OldInputObject $record -NewInputObject $record -ComputerName $dnsServer -ZoneName $zone
        }
    }
}

# TELEIS
#
$term = "TELEIS"
$records = Get-DnsServerResourceRecord -ZoneName $zone -ComputerName $dnsserver -RRType A | ? TimeStamp -ne $null | ? HostName -like $term* | ? HostName -notlike "*.pacorp.panynj.gov"
foreach ($record in $records) {
    if(Test-Connection -cn $record.HostName -BufferSize 16 -Count 1 -ea 0 -quiet) {
        if (Get-WMIObject -class Win32_NetworkAdapterConfiguration -Filter IPEnabled=True -ComputerName $record.HostName | Where-Object {$_.IPaddress -contains $record.RecordData.IPv4Address.IPAddressToString} | Where-Object {$_.DHCPEnabled -eq $false}) {
            Set-DnsServerResourceRecord -OldInputObject $record -NewInputObject $record -ComputerName $dnsServer -ZoneName $zone
        }
    }
}

# TELEIV
#
$term = "TELEIV"
$records = Get-DnsServerResourceRecord -ZoneName $zone -ComputerName $dnsserver -RRType A | ? TimeStamp -ne $null | ? HostName -like $term* | ? HostName -notlike "*.pacorp.panynj.gov"
foreach ($record in $records) {
    if(Test-Connection -cn $record.HostName -BufferSize 16 -Count 1 -ea 0 -quiet) {
        if (Get-WMIObject -class Win32_NetworkAdapterConfiguration -Filter IPEnabled=True -ComputerName $record.HostName | Where-Object {$_.IPaddress -contains $record.RecordData.IPv4Address.IPAddressToString} | Where-Object {$_.DHCPEnabled -eq $false}) {
            Set-DnsServerResourceRecord -OldInputObject $record -NewInputObject $record -ComputerName $dnsServer -ZoneName $zone
        }
    }
}

# WTC*AS
#
$term = "WTC*AS"
$records = Get-DnsServerResourceRecord -ZoneName $zone -ComputerName $dnsserver -RRType A | ? TimeStamp -ne $null | ? HostName -like $term* | ? HostName -notlike "*.pacorp.panynj.gov"
foreach ($record in $records) {
    if(Test-Connection -cn $record.HostName -BufferSize 16 -Count 1 -ea 0 -quiet) {
        if (Get-WMIObject -class Win32_NetworkAdapterConfiguration -Filter IPEnabled=True -ComputerName $record.HostName | Where-Object {$_.IPaddress -contains $record.RecordData.IPv4Address.IPAddressToString} | Where-Object {$_.DHCPEnabled -eq $false}) {
            Set-DnsServerResourceRecord -OldInputObject $record -NewInputObject $record -ComputerName $dnsServer -ZoneName $zone
        }
    }
}

# WTC*DB
#
$term = "WTC*DB"
$records = Get-DnsServerResourceRecord -ZoneName $zone -ComputerName $dnsserver -RRType A | ? TimeStamp -ne $null | ? HostName -like $term* | ? HostName -notlike "*.pacorp.panynj.gov"
foreach ($record in $records) {
    if(Test-Connection -cn $record.HostName -BufferSize 16 -Count 1 -ea 0 -quiet) {
        if (Get-WMIObject -class Win32_NetworkAdapterConfiguration -Filter IPEnabled=True -ComputerName $record.HostName | Where-Object {$_.IPaddress -contains $record.RecordData.IPv4Address.IPAddressToString} | Where-Object {$_.DHCPEnabled -eq $false}) {
            Set-DnsServerResourceRecord -OldInputObject $record -NewInputObject $record -ComputerName $dnsServer -ZoneName $zone
        }
    }
}
