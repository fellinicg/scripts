$host.Runspace.ThreadOptions = "ReuseThread"

$Delim = ","
$Quoted = [char]34

$OutFile = "C:\Permissions.csv"

if(test-path($OutFile)) {remove-item $OutFile}

out-file -filepath $OutFile -inputobject (`
    $Quoted + "Folder Path" + $Quoted + $Delim `
    + $Quoted + "IdentityReference" + $Quoted + $Delim `
    + $Quoted + "AccessControlType" + $Quoted + $Delim `
    + $Quoted + "FileSystemRights" + $Quoted + $Delim `
    + $Quoted + "IsInherited" + $Quoted + $Delim `
    + $Quoted + "InheritanceFlags" + $Quoted + $Delim `
    + $Quoted + "PropagationFlags" + $Quoted`
    ) -encoding "ASCII"

$RootPath = "\\TELEDSRV2\DATA\LAW\SHARED\LIBRARY\SCAN REQUESTS"

#$Folders = $RootPath
$Folders = dir $RootPath -recurse | where {$_.psiscontainer -eq $true}

foreach ($Folder in $Folders){
	#$ACLs = get-acl $Folder | ForEach-Object { $_.Access  }
	$ACLs = get-acl $Folder.PSPath | ForEach-Object { $_.Access  }
	Foreach ($ACL in $ACLs){
        out-file -filepath $OutFile -inputobject (`
            $Quoted + $Folder.FullName + $Quoted + $Delim `
            + $Quoted + $ACL.IdentityReference + $Quoted + $Delim `
            + $Quoted + $ACL.AccessControlType + $Quoted + $Delim `
            + $Quoted + $ACL.FileSystemRights + $Quoted + $Delim `
            + $Quoted + $ACL.IsInherited + $Quoted + $Delim `
            + $Quoted + $ACL.InheritanceFlags + $Quoted + $Delim `
            + $Quoted + $ACL.PropagationFlags + $Quoted`
            ) -encoding "ASCII" -append
	}}
