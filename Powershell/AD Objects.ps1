Import-module ActiveDirectory

#Get-ADUser -Filter {enabled -eq "true"}  -SearchBase "DC=pacorp,DC=panynj,DC=gov" -property * | select-object CanonicalName, GivenName, EmailAddress, Name, Office, OfficePhone, Surname, telephoneNumber | Export-Csv adusers.txt -notype
#Get-ADUser -Filter * -SearchBase "DC=pacorp,DC=panynj,DC=gov" -property * | select-object -first 3 SID, Name, GivenName, Surname, EmailAddress, Office, OfficePhone, telephoneNumber, CanonicalName | Export-Csv adusers.txt -notype

# Export Users
#
Get-ADUser -Filter * -SearchBase "DC=pacorp,DC=panynj,DC=gov" -property * | select-object SID, Name, GivenName, Surname, EmailAddress, Office, OfficePhone, telephoneNumber, CanonicalName | Export-Csv adusers.txt -notype

# Export Computers
#
Get-ADComputer -Filter * -SearchBase "DC=pacorp,DC=panynj,DC=gov" -property * | select-object SID, Name, CanonicalName | Export-Csv adcomputers.txt -notype