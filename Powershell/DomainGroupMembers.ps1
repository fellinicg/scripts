Get-ADGroup -Filter 

$output = @()
$group = "Board_Comm_CR.TSD.WTCB.WTC.NY.PANYNJ"
$group = "JobWidePhotoAdmin.PS.PATC.NJ.PANYNJ"
#$group = 'Engineering Desktop Admins'
$outfile = $group + ".csv"

get-adgroupmember $group -recursive  | select-object name, distinguishedName | sort-object name | foreach {
        $output += Get-ADUser -Filter * -SearchBase $_.distinguishedName -property * | select-object GivenName, Surname, Name, distinguishedName #, Enabled
}

$output | Export-Csv $outfile -notype