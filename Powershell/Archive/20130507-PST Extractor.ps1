# Declarations
#
$InputDir = "S:\ISD\SAN Failure File Recovery\AT&T Exchange Recovery\01\"
$OutputDir = "S:\ISD\SAN Failure File Recovery\AT&T Exchange Recovery\01\"
$OutputDir = "C:\PST Extract\Output\"
$OutputFile = $OutputDir + "messages.csv"
$Delim = ","
$Quoted = "|"
$Quoted = [char]34
$DirCt = 0
$MsgCt = 0

# Display start time
#
out-file -filepath ($OutputDir + "job.log") -inputobject ((get-date).tostring() + " >> Extraction process started...") -encoding "ASCII" -append

# Write header to $OutputFile
#
out-file -filepath $OutputFile -inputobject (`
    $Quoted + "MsgFileFullPath" + $Quoted + $Delim `
    + $Quoted + "Subject" + $Quoted + $Delim `
    + $Quoted + "SentOn" + $Quoted + $Delim `
    + $Quoted + "ReceivedTime" + $Quoted + $Delim `
    + $Quoted + "Size" + $Quoted + $Delim `
    ) -encoding "ASCII"
    
#out-file -filepath $OutputFile -inputobject (`
#    $Quoted + "MsgFileFullPath" + $Quoted + $Delim `
#    + $Quoted + "MsgFileDirectory" + $Quoted + $Delim `
#    + $Quoted + "Mailbox" + $Quoted + $Delim `
#    + $Quoted + "MsgFile" + $Quoted + $Delim `
#    + $Quoted + "Subject" + $Quoted + $Delim `
#    + $Quoted + "From" + $Quoted + $Delim `
#    + $Quoted + "To" + $Quoted + $Delim `
#    + $Quoted + "SentOn" + $Quoted + $Delim `
#    + $Quoted + "ReceivedTime" + $Quoted + $Delim `
#    + $Quoted + "Size" + $Quoted + $Delim `
#    + $Quoted + "ReceivedBy" + $Quoted `
#    ) -encoding "ASCII"
#    + $Quoted + "CC" + $Quoted + $Delim ` 

# Delete previous error log
#
if(test-path($OutputDir + "error.log")) {remove-item ($OutputDir + "error.log")}

# Retrieve all directories under the $InputDir
#
$UserDirs = get-childitem ($InputDir) | ?{ $_.PSIsContainer }

# Loop over directories (presumably users)
#
foreach($UserDir in $UserDirs) {
    try { 
        # Increment counter
        #
        $DirCt++
        
        # Update log
        #
        out-file -filepath ($OutputDir + "job.log") -inputobject ((get-date).tostring() + " >> Processing " + $UserDir.name + " (" + $DirCt + " of " + $UserDirs.count.tostring() + ")") -encoding "ASCII" -append

        # Retrieve all .MSG files under the $UserDir recursively
        #
        $MsgFiles = get-childitem ($UserDir.fullname) -recurse -filter *.msg

        # Update $MsgCt
        #
        $MsgCt = $MsgCt + $MsgFiles.count
        
        # Update log
        #
        out-file -filepath ($OutputDir + "job.log") -inputobject ((get-date).tostring() + " >> Processing " + $MsgFiles.count.tostring() + " .MSG files in " + $UserDir.name) -encoding "ASCII" -append

        # Open RDO session
        #
        $RDOSession = new-object -com Redemption.RDOsession

        # Loop over files
        #
        foreach($MsgFile in $MsgFiles) {
            try { 
                # Retrieve message from .MSG file
                #
                $Msg = $RDOSession.GetMessageFromMsgFile($MsgFile.fullname)

                # Write message info to $OutputFile
                #
                out-file -filepath $OutputFile -inputobject ( `
                    $Quoted + ($MsgFile.fullname) + $Quoted  + $Delim `
                    + $Quoted + $Msg.subject -replace "`r*`n*" + $Quoted + $Delim `
                    + $Msg.senton + $Delim `
                    + $Msg.receivedtime + $Delim `
                    + $Msg.size + $Delim `
                ) -encoding "ASCII" -append
#                out-file -filepath $OutputFile -inputobject ( `
#                    $Quoted + ($MsgFile.fullname) + $Quoted  + $Delim `
#                    + $Quoted + ($MsgFile.directoryname) + $Quoted  + $Delim `
#                    + $Quoted + ($MsgFile.directory.name) + $Quoted  + $Delim `
#                    + $Quoted + ($MsgFile.name) + $Quoted  + $Delim `
#                    + $Quoted + $Msg.subject + $Quoted + $Delim `
#                    + $Quoted + $Msg.sendername + $Quoted + $Delim `
#                    + $Quoted + $Msg.to + $Quoted + $Delim `
#                    + $Msg.senton + $Delim `
#                    + $Msg.receivedtime + $Delim `
#                    + $Msg.size + $Delim `
#                    + $Quoted + $Msg.receivedbyname + $Quoted
#                ) -encoding "ASCII" -append
        #            + $Quoted + $Msg.cc + $Quoted + $Delim `
            }
            catch {
                out-file -filepath ($OutputDir + "error.log") -inputobject ("Problem with " + ($MsgFile.fullname) + "`r`n`t>> " + $_.Exception.Message) -encoding "ASCII" -append
            }
        }

        # Update log
        #
        out-file -filepath ($OutputDir + "job.log") -inputobject ((get-date).tostring() + " >> " + $MsgFiles.count.tostring() + " .MSG files processed in " + $UserDir.name) -encoding "ASCII" -append
    }
    catch {
        out-file -filepath ($OutputDir + "error.log") -inputobject ("Problem with " + ($UserDir.fullname) + "`r`n`t>> " + $_.Exception.Message) -encoding "ASCII" -append
    }

}

# Update log
#
out-file -filepath ($OutputDir + "job.log") -inputobject ((get-date).tostring() + " >> " + $MsgCt.tostring() + " .MSG files processed.") -encoding "ASCII" -append


# Display finish time
#
out-file -filepath ($OutputDir + "job.log") -inputobject ((get-date).tostring() + " >> Extraction process completed.") -encoding "ASCII" -append
