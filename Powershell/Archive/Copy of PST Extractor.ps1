# Declarations
#
$InputFile = "C:\PST Extract\Input\messages.csv"
$OutputDir = "C:\PST Extract\Output\"
$OutputFile = $OutputDir + "messages.csv"
$Delim = ","
$Quoted = "|"
$Quoted = [char]34
$DirCt = 0
$MsgCt = 0

# Display start time
#
out-file -filepath ($OutputDir + "job.log") -inputobject ((get-date).tostring() + " >> Extraction process started...") -encoding "ASCII" -append

# Write header to $OutputFile
#
out-file -filepath $OutputFile -inputobject (`
    $Quoted + "User" + $Quoted + $Delim `
    + $Quoted + "MsgFileFullPath" + $Quoted + $Delim `
    + $Quoted + "Subject" + $Quoted + $Delim `
    + $Quoted + "SentOn" + $Quoted + $Delim `
    + $Quoted + "ReceivedTime" + $Quoted + $Delim `
    + $Quoted + "Size" + $Quoted + $Delim `
    ) -encoding "ASCII"
    
#out-file -filepath $OutputFile -inputobject (`
#    $Quoted + "MsgFileFullPath" + $Quoted + $Delim `
#    + $Quoted + "MsgFileDirectory" + $Quoted + $Delim `
#    + $Quoted + "Mailbox" + $Quoted + $Delim `
#    + $Quoted + "MsgFile" + $Quoted + $Delim `
#    + $Quoted + "Subject" + $Quoted + $Delim `
#    + $Quoted + "From" + $Quoted + $Delim `
#    + $Quoted + "To" + $Quoted + $Delim `
#    + $Quoted + "SentOn" + $Quoted + $Delim `
#    + $Quoted + "ReceivedTime" + $Quoted + $Delim `
#    + $Quoted + "Size" + $Quoted + $Delim `
#    + $Quoted + "ReceivedBy" + $Quoted `
#    ) -encoding "ASCII"
#    + $Quoted + "CC" + $Quoted + $Delim ` 

# Delete previous error log
#
if(test-path($OutputDir + "error.log")) {remove-item ($OutputDir + "error.log")}

# Loop over file
#
Import-Csv $Inputfile -header "user","file" | foreach {
$_.user
#| convertfrom-csv | format-table
break
}
$reader = [System.IO.File]::OpenText($InputFile)
try {
    for(;;) {
        $line = $reader.ReadLine()

        if ($line -eq $null) { break }

        # Open RDO session
        #
        $RDOSession = new-object -com Redemption.RDOsession

        if ($line.split(",").count -eq 2) {($file = $line.split(",")[1] -replace '"', '')}
        else {continue}

        # Retrieve message from .MSG file
        #
        $Msg = $RDOSession.GetMessageFromMsgFile($file)

        # Write message info to $OutputFile
        #
        out-file -filepath $OutputFile -inputobject ( `
            $Quoted + ($line.split(",")[0]) + $Quoted  + $Delim `
            + $Quoted + ($line.split(",")[1]) + $Quoted  + $Delim `
            + $Quoted + $Msg.subject -replace "`r*`n*" + $Quoted + $Delim `
            + $Msg.senton + $Delim `
            + $Msg.receivedtime + $Delim `
            + $Msg.size + $Delim `
        ) -encoding "ASCII" -append
        
        break
        
    }
}
finally {
    $reader.Close()
}
