# Declarations
#
$InputDir = "S:\ISD\SAN Failure File Recovery\Data File Recovery\Individuals\"
$OutputDir = "C:\PST Extract\Output\"
$OutputFile = $OutputDir + "messages.csv"

# Retrieve all .MSG files under the $InputDir recursively
#
$MsgFiles = get-childitem ($InputDir) -recurse -include *.msg

# Open RDO session
#
$RDOSession = new-object -com Redemption.RDOsession

# Write header to $OutputFile
#
out-file -filepath $OutputFile -inputobject (`
    [char]34 + "MsgFileFullPath" + [char]34 + "," `
    + [char]34 + "MsgFileDirectory" + [char]34 + "," `
    + [char]34 + "Mailbox" + [char]34 + "," `
    + [char]34 + "MsgFile" + [char]34 + "," `
    + [char]34 + "Subject" + [char]34 + "," `
    + [char]34 + "From" + [char]34 + "," `
    + [char]34 + "To" + [char]34 + "," `
    + [char]34 + "CC" + [char]34 + "," `
    + [char]34 + "SentOn" + [char]34 + "," `
    + [char]34 + "ReceivedTime" + [char]34 + "," `
    + [char]34 + "Size" + [char]34`
    ) -encoding "ASCII"
    

# Delete previous error log
#
if(test-path($OutputDir + "error.log")) {remove-item ($OutputDir + "error.log")}

# Display start time
#
get-date

# Loop over files
#
foreach($MsgFile in $MsgFiles) {
    try { 
        # Retrieve message from .MSG file
        #
        $Msg = $RDOSession.GetMessageFromMsgFile($MsgFile.fullname)

        # Write message info to $OutputFile
        #
        out-file -filepath $OutputFile -inputobject ( `
            [char]34 + ($MsgFile.fullname) + [char]34  + "," `
            + [char]34 + ($MsgFile.directoryname) + [char]34  + "," `
            + [char]34 + ($MsgFile.directory.name) + [char]34  + "," `
            + [char]34 + ($MsgFile.name) + [char]34  + "," `
            + [char]34 + $Msg.subject + [char]34 + "," `
            + [char]34 + $Msg.sendername + [char]34 + "," `
            + [char]34 + $Msg.to + [char]34 + "," `
            + [char]34 + $Msg.cc + [char]34 + "," `
            + $Msg.senton + "," `
            + $Msg.receivedtime + "," `
            + $Msg.size + "," `
            + $Msg.receivedbyname
        ) -encoding "ASCII" -append
    }
    catch {
        out-file -filepath ($OutputDir + "error.log") -inputobject ("Problem with " + ($MsgFile.fullname) + "`r`n`t>> " + $_.Exception.Message) -encoding "ASCII" -append
    }
}

# Display start time
#
get-date