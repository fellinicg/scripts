# Declarations
#
$InputDir = "S:\ISD\SAN Failure File Recovery\AT&T Exchange Recovery\01\"
$OutputDir = "S:\ISD\SAN Failure File Recovery\AT&T Exchange Recovery\01\"
$OutputDir = "C:\PST Extract\Output\"
$OutputFile = $OutputDir + "messages.csv"
$Delim = ","
$Quoted = [char]34
$Quoted = "|"

# Display start time
#
out-file -filepath ($OutputDir + "job.log") -inputobject ("Started at " + (get-date)) -encoding "ASCII" -append

# Retrieve all .MSG files under the $InputDir recursively
#
$MsgFiles = get-childitem ($InputDir) -recurse -include *.msg

# Display start time
#
out-file -filepath ($OutputDir + "job.log") -inputobject (($MsgFiles.count.tostring()) + " to be processed...") -encoding "ASCII" -append

# Open RDO session
#
$RDOSession = new-object -com Redemption.RDOsession

# Write header to $OutputFile
#
out-file -filepath $OutputFile -inputobject (`
    $Quoted + "MsgFileFullPath" + $Quoted + $Delim `
    + $Quoted + "MsgFileDirectory" + $Quoted + $Delim `
    + $Quoted + "Mailbox" + $Quoted + $Delim `
    + $Quoted + "MsgFile" + $Quoted + $Delim `
    + $Quoted + "Subject" + $Quoted + $Delim `
    + $Quoted + "From" + $Quoted + $Delim `
    + $Quoted + "To" + $Quoted + $Delim `
    + $Quoted + "SentOn" + $Quoted + $Delim `
    + $Quoted + "ReceivedTime" + $Quoted + $Delim `
    + $Quoted + "Size" + $Quoted + $Delim `
    + $Quoted + "ReceivedBy" + $Quoted `
    ) -encoding "ASCII"
    
#    + $Quoted + "CC" + $Quoted + $Delim ` 

# Delete previous error log
#
if(test-path($OutputDir + "error.log")) {remove-item ($OutputDir + "error.log")}

# Loop over files
#
foreach($MsgFile in $MsgFiles) {
    try { 
        # Retrieve message from .MSG file
        #
        $Msg = $RDOSession.GetMessageFromMsgFile($MsgFile.fullname)

        # Write message info to $OutputFile
        #
        out-file -filepath $OutputFile -inputobject ( `
            $Quoted + ($MsgFile.fullname) + $Quoted  + $Delim `
            + $Quoted + ($MsgFile.directoryname) + $Quoted  + $Delim `
            + $Quoted + ($MsgFile.directory.name) + $Quoted  + $Delim `
            + $Quoted + ($MsgFile.name) + $Quoted  + $Delim `
            + $Quoted + $Msg.subject + $Quoted + $Delim `
            + $Quoted + $Msg.sendername + $Quoted + $Delim `
            + $Quoted + $Msg.to + $Quoted + $Delim `
            + $Msg.senton + $Delim `
            + $Msg.receivedtime + $Delim `
            + $Msg.size + $Delim `
            + $Quoted + $Msg.receivedbyname + $Quoted
        ) -encoding "ASCII" -append
#            + $Quoted + $Msg.cc + $Quoted + $Delim `
    }
    catch {
        out-file -filepath ($OutputDir + "error.log") -inputobject ("Problem with " + ($MsgFile.fullname) + "`r`n`t>> " + $_.Exception.Message) -encoding "ASCII" -append
    }
}

# Display finish time
#
out-file -filepath ($OutputDir + "job.log") -inputobject ("Finished at " + (get-date)) -encoding "ASCII" -append