$FirstFound = $false
$Row = ''
$DnsList = dnscmd teleis30 /ZonePrint /detail wks.pacorp.panynj.gov | Select-String ('Node Name', ' A	', ' TXT		') | foreach {$_ -replace '\tNode Name    = ','Machine='} | foreach {$_ -replace ' A	','IP='} | foreach {$_ -replace ' TXT		','MAC='}
Remove-Item 'c:\dns.txt'
foreach ($Line in $DnsList) {
    switch($true) {
        $Line.StartsWith('Machine=') {
            If($FirstFound) {
                If($Row.EndsWith(',')) {$Row = $Row.ToUpper() -Replace('.$')}
                $Row >> c:\dns.txt
            }
            $Row = $Line.Replace('Machine=', '') + ','
            If(!$Row.StartsWith('@')) {$FirstFound = $true}
        }
        $Line.StartsWith('IP=') {
            $Row += $Line.Replace('IP=', '') + ','
        }
        $Line.StartsWith('MAC=') {
            $Line = $Line.Replace('MAC=', '')
            If($Line.Length -eq 20) {$Line = $Line.SubString(3, 17)}
            $Row += $Line + ','
        }
    }
}