$host.Runspace.ThreadOptions = "ReuseThread"

$OutFile = "C:\Permissions.csv"
$Header = "Folder Path,IdentityReference,AccessControlType,FileSystemRights,IsInherited,InheritanceFlags,PropagationFlags"
Clear-Content $OutFile
Add-Content -Value $Header -Path $OutFile 

$RootPath = "\\patcdsrv1\data\JSTC\PATH\Share\director\capital programs"

$Folders = dir $RootPath -recurse | where {$_.psiscontainer -eq $true}
#$Folders = $RootPath

foreach ($Folder in $Folders){
#	$ACLs = get-acl $Folder | ForEach-Object { $_.Access  }
	$ACLs = get-acl $Folder.PSPath | ForEach-Object { $_.Access  }
	Foreach ($ACL in $ACLs){
	$OutInfo = $Folder.FullName + "," + $ACL.IdentityReference  + "," + $ACL.AccessControlType + ",""" + $ACL.FileSystemRights + """," + $ACL.IsInherited + ",""" + $ACL.InheritanceFlags + """,""" + $ACL.PropagationFlags + """"
	Add-Content -Value $OutInfo -Path $OutFile
	}}
