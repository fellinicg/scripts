$InputDir = "C:\PST Extract\Input\"
$InputDir = "S:\ISD\SAN Failure File Recovery\Data File Recovery\All_User_Recovered_Data\"
$InputDir = "S:\ISD\SAN Failure File Recovery\Data File Recovery\Individuals\"
$OutputDir = "C:\PST Extract\Output\"
$OutputFile = $OutputDir + "messages.csv"
$Global:MessageCt = 0

function EnumPSTFolders($PSTFolder) {
    foreach($Folder in $PSTFolder.folders) {
        "Processing : " + $Folder.name + "(" + $Folder.items.count + ")"
        ProcessItem($Folder)
        if($Folder.folders.count -ne 0) {
            EnumPSTFolders($Folder)
        }
    }
}

function ProcessItem($Folder) {
    foreach($Item in $Folder.items) {
#        "  Subject : " + $Item.subject
#        "  >>  " + $Item.messageclass
        if($Item.messageclass -eq "IPM.Note") { 
            $Global:MessageCt++
            try { 
                out-file -filepath $OutputFile -inputobject ($Item.entryid + "," + [char]34 + $Item.subject + [char]34 + "," + [char]34 + $Item.sendername + [char]34 + "," + [char]34 + $Item.to + [char]34 + "," + [char]34 + $Item.cc + [char]34 + "," + $Item.senton + "," + $Item.receivedtime + "," + $Item.size) -encoding "ASCII" -append
                $Item.SaveAs($OutputDir + "message" + $Global:MessageCt + ".msg") 
            }
            catch {
                out-file -filepath ($OutputDir + "error.log") -inputobject ("Problem with " + $Item.entryid) -encoding "ASCII" -append
            }
        }
    }
}

#$PSTFiles = get-childitem $InputDir -recurse | where-object {!$_.psiscontainer}
#$MsgFiles = get-childitem ($InputDir + "*.msg") -recurse | where-object {!$_.psiscontainer}
$MsgFiles = get-childitem ($InputDir) -recurse -include *.msg #| select-object -expandproperty fullname
$RDOSession = new-object -com Redemption.RDOsession

out-file -filepath $OutputFile -inputobject (`
    [char]34 + "MsgFileFullPath" + [char]34 + "," `
    + [char]34 + "MsgFileDirectory" + [char]34 + "," `
    + [char]34 + "Mailbox" + [char]34 + "," `
    + [char]34 + "MsgFile" + [char]34 + "," `
    + [char]34 + "Subject" + [char]34 + "," `
    + [char]34 + "From" + [char]34 + "," `
    + [char]34 + "To" + [char]34 + "," `
    + [char]34 + "CC" + [char]34 + "," `
    + [char]34 + "SentOn" + [char]34 + "," `
    + [char]34 + "ReceivedTime" + [char]34 + "," `
    + [char]34 + "Size" + [char]34`
    ) -encoding "ASCII"
if(test-path($OutputDir + "error.log")) {remove-item ($OutputDir + "error.log")}

#foreach($PST in $PSTFiles) {
#    $PSTfile = $RDOSession.LogonPSTStore($InputDir + $PST.name, 1)
#    $PSTRoot = $RDOSession.GetFolderFromID($PSTfile.IPMRootFolder.EntryID, $PSTfile.EntryID)
#    EnumPSTFolders($PSTRoot)
#    $RDOSession.Logoff()
#}

get-date

foreach($MsgFile in $MsgFiles) {
    try { 
        #$Msg = $RDOSession.GetMessageFromMsgFile($InputDir + $MsgFile.Name)
        $Msg = $RDOSession.GetMessageFromMsgFile($MsgFile.fullname)
        out-file -filepath $OutputFile -inputobject ( `
            [char]34 + ($MsgFile.fullname) + [char]34  + "," `
            + [char]34 + ($MsgFile.directoryname) + [char]34  + "," `
            + [char]34 + ($MsgFile.directory.name) + [char]34  + "," `
            + [char]34 + ($MsgFile.name) + [char]34  + "," `
            + [char]34 + $Msg.subject + [char]34 + "," `
            + [char]34 + $Msg.sendername + [char]34 + "," `
            + [char]34 + $Msg.to + [char]34 + "," `
            + [char]34 + $Msg.cc + [char]34 + "," `
            + $Msg.senton + "," `
            + $Msg.receivedtime + "," `
            + $Msg.size + "," `
            + $Msg.receivedbyname
        ) -encoding "ASCII" -append
    }
    catch {
        out-file -filepath ($OutputDir + "error.log") -inputobject ("Problem with " + ($MsgFile.fullname) + "`r`n`t>> " + $_.Exception.Message) -encoding "ASCII" -append
    }
}

get-date