$host.Runspace.ThreadOptions = "ReuseThread"

# Declarations
#
$StartDirectory = "C:\Users\tfellini\Documents\Work Orders\2014-981\CAD\cad"

$LogFile = (Get-Date -Format yyyyMMdd_HHMMss) + ".log"
$StartDirectoryArray = $StartDirectory.split("\")
$Directories = @()
$DirectoryAclInfo = import-csv permissions.csv
$RightsMap = @{
    "Traverse Folder/Execute File" = [System.Security.AccessControl.FileSystemRights]::Traverse; 
    "List Folder/Read Data" = [System.Security.AccessControl.FileSystemRights]::ListDirectory; 
    "Read Attributes" = [System.Security.AccessControl.FileSystemRights]::ReadAttributes; 
    "Read Extended Attributes" = [System.Security.AccessControl.FileSystemRights]::ReadExtendedAttributes;
    "Create Files/Write Data" = [System.Security.AccessControl.FileSystemRights]::CreateFiles;
    "Create Folders/Append Data" = [System.Security.AccessControl.FileSystemRights]::AppendData;
    "Write Attributes" = [System.Security.AccessControl.FileSystemRights]::WriteAttributes;
    "Write Extended Attributes" = [System.Security.AccessControl.FileSystemRights]::WriteExtendedAttributes;
    "Delete Subfolders and Files" = [System.Security.AccessControl.FileSystemRights]::DeleteSubdirectoriesAndFiles;
    "Delete" = [System.Security.AccessControl.FileSystemRights]::Delete;
    "Read Permissions" = [System.Security.AccessControl.FileSystemRights]::ReadPermissions;
    "Change Permissions" = [System.Security.AccessControl.FileSystemRights]::ChangePermissions;
    "Take Ownership" = [System.Security.AccessControl.FileSystemRights]::TakeOwnership;
}

# Log entry
#
(Get-Date -Format HH:MM:ss) + ": Begin processing " + $StartDirectory > $LogFile

# Test starting directory
#
switch ("True") {
    # CAD\cad level
    #
    ($StartDirectoryArray[-1] -ceq "cad") {
        $WorkingDirectory = get-childitem $StartDirectory | ?{ $_.PSIsContainer } | select FullName
        foreach ($Directory in $WorkingDirectory) { 
#            $Directories += ,$Directory
            $Directories += (get-childitem $Directory.FullName | ?{ $_.PSIsContainer } | select-object FullName )
        }

        # Log entry
        #
        (Get-Date -Format HH:MM:ss) + ":  Processing at the CAD\cad level" >> $LogFile
    }

    # Facility level
    #
    ($StartDirectoryArray[-2] -ceq "cad") {
#        $Directories = ,$StartDirectory
        $Directories += (get-childitem $StartDirectory | ?{ $_.PSIsContainer } | foreach-object {$_.FullName} )

        # Log entry
        #
        (Get-Date -Format HH:MM:ss) + ":  Processing at the Facility level" >> $LogFile
    }

    # PID level
    #
    ($StartDirectoryArray[-3] -ceq "cad") {
        $Directories = ,$StartDirectory

        # Log entry
        #
        (Get-Date -Format HH:MM:ss) + ":  Processing at the PID level" >> $LogFile
    }
    
    # Bad path
    default {
        # Log entry
        #
        (Get-Date -Format HH:MM:ss) + ":  Bad starting directory" >> $LogFile

        return
    }
}

# Loop over directories
#
foreach ($Directory in $Directories) {

    # Log entry
    #
    (Get-Date -Format HH:MM:ss) + ":   Processing " + $Directory >> $LogFile

    # Loop over information from CSV file
    #
    foreach ($Info in $DirectoryAclInfo) { 

        # Get working directory
        #
        $WorkingDirectory = ((&{if (!$Directory.FullName) {$Directory} else {$Directory.FullName}}) + "\" + $Info.Directory)

        # Check that directory exists
        #
        if ((Test-Path $WorkingDirectory)) {
        
            # Log entry
            #
            (Get-Date -Format HH:MM:ss) + ":    Processing " + $WorkingDirectory >> $LogFile

            # Retrieve current ACL for the working directory
            #
            $Acl = Get-Acl $WorkingDirectory

            # Loop over columns in current row from CSV file
            #
            foreach ($Property in $Info.psobject.properties.GetEnumerator()) {

                # Process only if the column is an access right and not blank
                #
                if ($RightsMap[$Property.Name] -and $Property.Value) {
                
                    # Access right
                    #
                    $Rights = $RightsMap[$Property.Name]

                    # Inheritance and Propogation
                    #
                    switch ($Property.Value.substring(0,1)) {
                        # This folder only
                        #
                        1 {
                            $ApplyTo = "This folder only"
                            $Inheritance = [System.Security.AccessControl.InheritanceFlags]::None
                            $Propogation = [System.Security.AccessControl.PropagationFlags]::None
                        }

                        # This folder and subfolders
                        #
                        2 {
                            $ApplyTo = "This folder and subfolders"
                            $Inheritance = [System.Security.AccessControl.InheritanceFlags]::ContainerInherit
                            $Propogation = [System.Security.AccessControl.PropagationFlags]::None
                        }

                        # This folder, subfolders and files
                        #
                        3 {
                            $ApplyTo = "This folder, subfolders and files"
                            $Inheritance = [System.Security.AccessControl.InheritanceFlags]::ContainerInherit -bor [System.Security.AccessControl.InheritanceFlags]::ObjectInherit
                            $Propogation = [System.Security.AccessControl.PropagationFlags]::None
                        }

                        # Subfolders and files only
                        #
                        4 {
                            $ApplyTo = "Subfolders and files only"
                            $Inheritance = [System.Security.AccessControl.InheritanceFlags]::ContainerInherit -bor [System.Security.AccessControl.InheritanceFlags]::ObjectInherit
                            $Propogation = [System.Security.AccessControl.PropagationFlags]::InheritOnly
                        }

                        # Files only
                        #
                        5 {
                            $ApplyTo = "Files only"
                            $Inheritance = [System.Security.AccessControl.InheritanceFlags]::ObjectInherit
                            $Propogation = [System.Security.AccessControl.PropagationFlags]::InheritOnly
                        }

                        # Unknown
                        #
                        default {
                            # Skip
                            #
                            $ApplyTo = ""
                            break
                        }
                    }

                    # Access
                    #
                    switch ($Property.Value.substring(1,1)) {
                        "a" {$Access = [System.Security.AccessControl.AccessControlType]::Allow}
                        "d" {$Access = [System.Security.AccessControl.AccessControlType]::Deny}
                        default {break}
                    }

                    # Add access rule
                    #
                    $Acl.AddAccessRule((New-Object System.Security.AccessControl.FileSystemAccessRule($Info.Group, $Rights, $Inheritance, $Propogation, $Access)))

                    # Log entry
                    #
                    (Get-Date -Format HH:MM:ss) + ":     " + $Access + " " + ($RightsMap.GetEnumerator() | ?{ $_.Value -eq $Rights }).Name + " to " + $Info.Group + " to " + $ApplyTo >> $LogFile

                }

            }
            
            # Set access rule for the working directory
            #
            set-acl $Acl.Path $Acl

            # Log entry
            #
            (Get-Date -Format HH:MM:ss) + ":    Access rules applied to " + ($Acl.Path -split "::")[1] >> $LogFile
        }
        else {
            # Log entry
            #
            (Get-Date -Format HH:MM:ss) + ":   " + $WorkingDirectory + " doesn't exist" >> $LogFile
        }        
            
    }
}

# Log entry
#
(Get-Date -Format HH:MM:ss) + ": Completed processing " + $StartDirectory >> $LogFile