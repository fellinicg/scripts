# Declarations
#
$InputFile = "C:\PST Extract\Input\messages.csv"
$OutputDir = "C:\PST Extract\Output\"
$OutputFile = $OutputDir + "messages.csv"
$SrcDir = "Z:"
$Delim = [char]9
$Quoted = ""

# Display start time
#
out-file -filepath ($OutputDir + "job.log") -inputobject ((get-date).tostring() + " >> Extraction process started...") -encoding "ASCII" -append

# Write header to $OutputFile
#
out-file -filepath $OutputFile -inputobject (`
    $Quoted + "UserName" + $Quoted + $Delim `
    + $Quoted + "MsgFileFullPath" + $Quoted + $Delim `
    + $Quoted + "Subject" + $Quoted + $Delim `
    + $Quoted + "From" + $Quoted + $Delim `
    + $Quoted + "SenderEmailAddress" + $Quoted + $Delim `
    + $Quoted + "To" + $Quoted + $Delim `
    + $Quoted + "SentOn" + $Quoted + $Delim `
    + $Quoted + "ReceivedTime" + $Quoted + $Delim `
    + $Quoted + "Size" + $Quoted
    ) -encoding "ASCII"
    
# Delete previous error log
#
if(test-path($OutputDir + "error.log")) {remove-item ($OutputDir + "error.log")}

# Loop over file
#
Import-Csv $Inputfile -header "user","file" | foreach {
    try { 
        # Open RDO session
        #
        $RDOSession = new-object -com Redemption.RDOsession

        # Retrieve message from .MSG file
        #
        $Msg = $RDOSession.GetMessageFromMsgFile($SrcDir + $_.file)

        # Write message info to $OutputFile
        #
        out-file -filepath $OutputFile -inputobject ( `
            $Quoted + $_.user + $Quoted  + $Delim `
            + $Quoted + $_.file + $Quoted  + $Delim `
            + $Quoted + (($Msg.subject -replace "`r*`n*") -replace "`t", " ") + $Quoted + $Delim `
            + $Quoted + $Msg.sendername + $Quoted + $Delim `
            + $Quoted + $Msg.senderemailaddress + $Quoted + $Delim `
            + $Quoted + $Msg.to + $Quoted + $Delim `
            + $Msg.senton + $Delim `
            + $Msg.receivedtime + $Delim `
            + $Msg.size
        ) -encoding "ASCII" -append

        # Update log
        #
#        out-file -filepath ($OutputDir + "job.log") -inputobject ((get-date).tostring() + " >> Processed File #" + $FileCt + ": " + $_.file) -encoding "ASCII" -append
    }     
    catch {
        out-file -filepath ($OutputDir + "error.log") -inputobject ("Problem with " + ($MsgFile.fullname) + "`r`n`t>> " + $_.Exception.Message) -encoding "ASCII" -append
    }
    finally {$FileCt++}
}

# Display finish time
#
out-file -filepath ($OutputDir + "job.log") -inputobject ((get-date).tostring() + " >> Extraction process completed.") -encoding "ASCII" -append
