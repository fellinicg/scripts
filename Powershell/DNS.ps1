$path = '\\patcdb21\d$\EventLogOutput\'
$dnsa = $path + 'dns-a.txt'
$dnstxt = $path + 'dns-txt.txt'
$zone = 'wks.pacorp.panynj.gov'
$dnslist = dnscmd teleis29 /EnumRecords $zone '@' /Type A /Continue
if(test-path($dnsa)) {remove-item $dnsa}
get-date
out-file -filepath $dnsa -inputobject 'entry,zone,ip,ts' -encoding "ASCII"
foreach ($line in $dnslist) {
    try {
#        if (!$line.startswith('Returned records') -and !$line.startswith('Command completed')) {$line.split("`t")[0].split(' ')[0] + ',' + $line.split("`t")[1] + ',' + (get-date ((get-date "1/1/1601 00:00:00 AM").addhours($line.split("`t")[0].split(' ')[1].split(':')[1].replace(']', ''))) -format g) >> c:\dns.txt}
        switch($true) {
            $line.startswith('Returned records') {}
            $line.startswith('Command completed') {}
            default {out-file -filepath $dnsa -inputobject ('"' + $line.split("`t")[0].split(' ')[0] + '","' + $zone + '","' + $line.split("`t")[1] + '","' + (get-date ((get-date "1/1/1601 00:00:00 AM").addhours($line.split("`t")[0].split(' ')[1].split(':')[1].replace(']', ''))) -format g) + '"') -encoding "ASCII" -append}
#            default {$zone >> $dnsa}

        }
    }
    catch {}
}
get-date
$dnslist = dnscmd teleis29 /EnumRecords $zone '@' /Type TXT /Continue
if(test-path($dnstxt)) {remove-item $dnstxt}
out-file -filepath $dnstxt -inputobject 'entry,zone,mac,ts' -encoding "ASCII"
get-date
foreach ($line in $dnslist) {
    try {
        switch($true) {
            $line.startswith('Returned records') {}
            $line.startswith('Command completed') {}
            default {out-file -filepath $dnstxt -inputobject ('"' + $line.split("`t")[0].split(' ')[0] + '","' + $zone + '","' + $line.split("`t")[2].substring(3, 17) + '","' + (get-date ((get-date "1/1/1601 00:00:00 AM").addhours($line.split("`t")[0].split(' ')[1].split(':')[1].replace(']', ''))) -format g) + '"') -encoding "ASCII" -append}
        }
    }
    catch {}
}
get-date