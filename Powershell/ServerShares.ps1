$host.Runspace.ThreadOptions = "ReuseThread"

$Delim = ","
$Quoted = [char]34

$OutFile = "C:\shares.csv"

$computer = 'TELEAS50'

$shares = gwmi -Class win32_share -ComputerName $computer | select Name, Description, Path

if(test-path($OutFile)) {remove-item $OutFile}

out-file -filepath $OutFile -inputobject (`
    $Quoted + "Share Name" + $Quoted + $Delim `
    + $Quoted + "Description" + $Quoted + $Delim `
    + $Quoted + "Path" + $Quoted + $Delim `
    + $Quoted + "IdentityReference" + $Quoted + $Delim `
    + $Quoted + "AccessControlType" + $Quoted + $Delim `
    + $Quoted + "FileSystemRights" + $Quoted + $Delim `
    + $Quoted + "IsInherited" + $Quoted + $Delim `
    + $Quoted + "InheritanceFlags" + $Quoted + $Delim `
    + $Quoted + "PropagationFlags" + $Quoted`
    ) -encoding "ASCII"
  
foreach ($share in $shares) {  
#    $acl = $null
    $sharename = $Share.Name.tostring()
    $objShareSec = Get-WMIObject -Class Win32_LogicalShareSecuritySetting -Filter "name='$sharename'"  -ComputerName $computer 
    try {  
        $SD = $objShareSec.GetSecurityDescriptor().Descriptor    
        foreach($ace in $SD.DACL){   
            $UserName = $ace.Trustee.Name      
            If ($ace.Trustee.Domain -ne $Null) {$UserName = "$($ace.Trustee.Domain)\$UserName"}    
            If ($ace.Trustee.Name -eq $Null) {$UserName = $ace.Trustee.SIDString }      
            $ACL = New-Object Security.AccessControl.FileSystemAccessRule($UserName, $ace.AccessMask, $ace.AceType)
            out-file -filepath $OutFile -inputobject (`
                $Quoted + $sharename + $Quoted + $Delim `
                + $Quoted + $share.description + $Quoted + $Delim `
                + $Quoted + $share.path + $Quoted + $Delim `
                + $Quoted + $ACL[0].IdentityReference + $Quoted + $Delim `
                + $Quoted + $ACL[0].AccessControlType + $Quoted + $Delim `
                + $Quoted + $ACL[0].FileSystemRights + $Quoted + $Delim `
                + $Quoted + $ACL[0].IsInherited + $Quoted + $Delim `
                + $Quoted + $ACL[0].InheritanceFlags + $Quoted + $Delim `
                + $Quoted + $ACL[0].PropagationFlags + $Quoted`
                ) -encoding "ASCII" -append
            }          
        } 
    catch  
        { 
        out-file -filepath $OutFile -inputobject (`
            $Quoted + $sharename + $Quoted + $Delim `
            + $Quoted + $share.description + $Quoted + $Delim `
            + $Quoted + $share.path + $Quoted + $Delim `
            + $Quoted + "N/A" + $Quoted + $Delim `
            + $Quoted + "N/A" + $Quoted + $Delim `
            + $Quoted + "N/A" + $Quoted + $Delim `
            + $Quoted + "N/A" + $Quoted + $Delim `
            + $Quoted + "N/A" + $Quoted + $Delim `
            + $Quoted + "N/A" + $Quoted`
            ) -encoding "ASCII" -append
        }  
    }