Import-module ActiveDirectory

$Output = @()
$Application = "VIP"
$Computers = ("TELEDV08").split(",")
$Users = ("jluk").split(",")
$OutPath = "C:\Users\tfellini\Documents\Projects\ACL Remediation\Output\"

foreach($User in $Users) {
    $output += get-aduser $User -properties * | select displayname, telephoneNumber, mail
} 
$output | Export-CSV ($OutPath + $Application + "_Owners.csv") -notype

$Properties = "AdsPath", "Name", "FullName", "AccountDisabled", "Class", "Description"
$Properties = "AdsPath", "FullName", "AccountDisabled", "Class", "Description"

$Select = $Properties | %{Invoke-Expression "@{n='$_';e={ `$_.GetType().InvokeMember('$_', 'GetProperty', `$Null, `$_, `$Null) }}"}

foreach($Computer in $Computers) {
    $Group = [ADSI]("WinNT://$Computer/Administrators")
#    $Group.PsBase.Invoke("Members") | Select-Object ([Array](@{n='ServerName';e={ $Computer }}) + $Select) | Export-CSV ($OutPath + $Application + "_" + $Computer + "_AdminGrp.csv") -notype
    $Group.PsBase.Invoke("Members") | Select-Object ([Array](@{n='ServerName';e={ $Computer }}) + $Select) | select ServerName, @{n='AdsPath';e={ ($_.AdsPath -replace "WinNT://PANYNJ/$Computer", $Computer) -replace "WinNT://", ""}}, FUllName, AccountDisabled, Class, Description | Export-CSV ($OutPath + $Application + "_" + $Computer + "_AdminGrp.csv") -notype

    $Group = [ADSI]("WinNT://$Computer/Remote Desktop Users")
#    $Group.PsBase.Invoke("Members") | Select-Object ([Array](@{n='ServerName';e={ $Computer }}) + $Select) | Export-CSV ($OutPath + $Application + "_" + $Computer + "_RdpGrp.csv") -notype
    $Group.PsBase.Invoke("Members") | Select-Object ([Array](@{n='ServerName';e={ $Computer }}) + $Select) | select ServerName, @{n='AdsPath';e={ ($_.AdsPath -replace "WinNT://PANYNJ/$Computer", $Computer) -replace "WinNT://", ""}}, FUllName, AccountDisabled, Class, Description | Export-CSV ($OutPath + $Application + "_" + $Computer + "_RdpGrp.csv") -notype
    
    Get-WmiObject -Class Win32_Share -computer $Computer | select __SERVER, Name, Caption, Path | Export-CSV ($OutPath + $Application + "_" + $Computer + "_Shares.csv") -notype

    Get-WmiObject -Class Win32_UserAccount -Filter  "LocalAccount='True'" -computer $Computer | select Domain, Name | Export-CSV ($OutPath + $Application + "_" + $Computer + "_LocalUsers.csv") -notype
}