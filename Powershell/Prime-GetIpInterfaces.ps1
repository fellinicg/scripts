﻿param(
    [parameter()]
    [string]$user = [System.Management.Automation.Language.NullString]::Value,
	
    [parameter()]
    [string]$key = [System.Management.Automation.Language.NullString]::Value,

    [parameter()]
    [string]$destpath = ".\Output",

    [parameter()]
    [string]$destfile = "Prime-IpInterfaces.csv",

    [parameter()]
    [string]$copyto = [System.Management.Automation.Language.NullString]::Value
)

# Thread options
#
$host.Runspace.ThreadOptions = "ReuseThread"

# Hack to allow for insecure certificate
#
. .\"InsecurePolicy.ps1"

# Declarations
#
$response = $null
$baseurl = "https://10.146.8.11/webacs/api/v2/data/"
$outfile = $(if ($destpath.EndsWith('"')){$destpath -replace ".$"} else {$destpath}) + "\" + (Get-Date -Format yyyyMMdd) + "-" + $destfile
$output = @("deviceName,deviceType,ipAddress,location,name,interfaceipAddress,adminStatus,operationalStatus")

# Prompt for credentials if we do not already have them
#
if ($user -ne $null -and $key -ne $null) {
    $cred = New-Object System.Management.Automation.PSCredential($user, ($key | ConvertTo-SecureString))
}
elseif ($cred -eq $null) {
    $cred = Get-Credential -Message "Cisco Prime Infrastructure Credentials"
}

# Login
#
$bytes = [System.Text.Encoding]::ASCII.GetBytes(($cred.UserName + ":" + $cred.GetNetworkCredential().Password))
$base64 = [System.Convert]::ToBase64String($bytes)
$basicauthvalue = "Basic $base64"
$headers = @{ Authorization = $basicauthvalue }

# Output header
#out-file -filepath $outfile -inputobject ("deviceName,deviceType,ipAddress,location,name,interfaceipAddress,adminStatus,operationalStatus") -encoding "ASCII"

# Retrieve device list
#
do {
    # Retrieve a maximum of 1000 devices (Cisco API limit)
    #
    $url = $baseurl + "Devices?.full=true&ipAddress=10.160.5.102"
    $url = $baseurl + "Devices?ipAddress=10.160.5.102"
    $url = $baseurl + "Devices?reachability=REACHABLE&.firstResult=" + $(if ($response.queryResponse.last -eq $null) {0} else { [int]$response.queryResponse.last + 1}) + "&.maxResults=1000"
    $response = Invoke-RestMethod $url -Headers $headers

    # Retrieve device details
    #
    foreach ($item in $response.queryResponse.entityId) { 
        $url = $baseurl + "InventoryDetails/" + $item.'#text'
        $item = Invoke-RestMethod $url -Headers $headers
        foreach ($subitem in $item.queryResponse.entity.inventoryDetailsDTO.ipInterfaces.ipInterface) {
#            out-file -filepath $outfile -inputobject (($item.queryResponse.entity.inventoryDetailsDTO.summary | select deviceName,deviceType,ipAddress,location | convertto-csv -NoTypeInformation | select -skip 1)  + "," + ($subitem | select name,ipAddress,adminStatus,operationalStatus | convertto-csv -NoTypeInformation | select -skip 1)) -encoding "ASCII" -append
            $output += (($item.queryResponse.entity.inventoryDetailsDTO.summary | select deviceName,deviceType,ipAddress,location | convertto-csv -NoTypeInformation | select -skip 1)  + "," + ($subitem | select name,ipAddress,adminStatus,operationalStatus | convertto-csv -NoTypeInformation | select -skip 1))
        }
    }
}
until ($response.queryResponse.count -eq [int]$response.queryResponse.last + 1)

# Write to file
#
out-file -filepath $outfile -inputobject $output -encoding "ASCII"

# Copy file
#
if ($copyto -ne $null) {
    if (test-path (([io.fileinfo]$copyto).DirectoryName)) {
        copy $outfile $copyto
    }
}