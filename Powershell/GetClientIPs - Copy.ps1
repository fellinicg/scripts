﻿$host.Runspace.ThreadOptions = "ReuseThread"

# Hack to allow for insecure certificate
#
. "C:\Users\tfellini\OneDrive - The Port Authority of New York & New Jersey\Scripts\Powershell\InsecurePolicy.ps1"

# Declarations
#
$response = $null
$baseurl = "https://10.146.8.11/webacs/api/v2/data/"
#$output = "deviceName,deviceType,vlan,deviceIpAddress,ipAddress`n"
$outfile = ".\Output\" + (Get-Date -Format yyyyMMdd) + "-ClientIPs.csv"

# Prompt for credentials if we do not already have them
#
if ($cred -eq $null) {
    $cred = Get-Credential -Message "Cisco Prime Infrastructure Credentials"
}

# Login
#
$bytes = [System.Text.Encoding]::ASCII.GetBytes(($cred.UserName + ":" + $cred.GetNetworkCredential().Password))
$base64 = [System.Convert]::ToBase64String($bytes)
$basicauthvalue = "Basic $base64"
$headers = @{ Authorization = $basicauthvalue }

#$stopwatch = [system.diagnostics.stopwatch]::startNew()

# Output header
out-file -filepath $outfile -inputobject ("deviceName,deviceType,vlan,deviceIpAddress,ipAddress") -encoding "ASCII"

# Retrieve client list
#
do {
    # Retrieve a maximum of 1000 devices (Cisco API limit)
    #
    $maxresults = "1000"
    $url = $baseurl + "Clients?.full=true&.firstResult=" + $(if ($response.queryResponse.last -eq $null) {0} else { [int]$response.queryResponse.last + 1}) + "&.maxResults=" + $maxresults
    $response = Invoke-RestMethod $url -Headers $headers

    # Store data
    #
    foreach ($item in $response.queryResponse.entity) { 
#        $client = Invoke-RestMethod $entity.url -Headers $headers
#        $output += ($item.clientsDTO | select deviceName, deviceType, vlan | convertto-csv -NoTypeInformation | select -skip 1) + "," + ($item.clientsDTO.deviceIpAddress | ConvertTo-Csv -NoTypeInformation | select -skip 1) + "," +  $(if($item.clientsDTO.ipAddress -eq $null) {'""'} else {$item.clientsDTO.ipAddress | ConvertTo-Csv -NoTypeInformation | select -skip 1}) + "`n"
        out-file -filepath $outfile -inputobject (($item.clientsDTO | select deviceName, deviceType, vlan | convertto-csv -NoTypeInformation | select -skip 1) + "," + ($item.clientsDTO.deviceIpAddress | ConvertTo-Csv -NoTypeInformation | select -skip 1) + "," +  $(if($item.clientsDTO.ipAddress -eq $null) {'""'} else {$item.clientsDTO.ipAddress | ConvertTo-Csv -NoTypeInformation | select -skip 1})) -encoding "ASCII" -append
    }

#    $stopwatch.Elapsed
}
#until ($clients.queryResponse.count -gt [int]$clients.queryResponse.last + 1)
until ($response.queryResponse.count -eq [int]$response.queryResponse.last + 1)

#$stopwatch.stop()

#$stopwatch
