$host.Runspace.ThreadOptions = "ReuseThread"

try {Add-Type -AssemblyName System.Printing}
catch  {Write-Warning ".Net feature not installed."} 

# Declarations
#
$outputdir = "R:\Scripts\Powershell\Output\"
$outputfile = $outputdir + "printqueues.csv"
$delim = "," #[char]9
$quoted = [char]34

# Permissions
#
$permissions = [System.Printing.PrintSystemDesiredAccess]::AdministrateServer
$queueperms = [System.Printing.PrintSystemDesiredAccess]::AdministratePrinter

# Print servers
#
$servers = ("patciv05,patciv06,teleiv07,teleiv08").split(",")

# Write header to $outputfile
#
out-file -filepath $outputfile -inputobject (`
    $quoted + "PrintServer" + $quoted + $delim `
    + $quoted + "Queue" + $quoted + $delim `
    + $quoted + "Driver" + $quoted + $delim `
    + $quoted + "Port" + $quoted + $delim `
    + $quoted + "Duplex" + $quoted + $delim `
    + $quoted + "IPAddress" + $quoted + $delim `
    + $quoted + "Online" + $quoted + $delim `
    ) -encoding "ASCII"
        
# Loop through print servers
#
foreach ($server in $servers) {
    # Get print server
    #
    $printserver = New-Object System.Printing.PrintServer("\\$($server)")

    # Get print server queues
    #
    $queues = $printserver.GetPrintQueues(@([System.Printing.EnumeratedPrintQueueTypes]::Shared))

    # Loop through queues
    #
    foreach($queue in $queues) {
        $portip = (gwmi win32_tcpipprinterport | where {$_.Name -eq $queue.QueuePort.Name}).HostAddress
        $online = Test-Connection -ComputerName $portip -Count 1 -ErrorAction SilentlyContinue
        out-file -filepath $outputfile -inputobject ( `
            $quoted + $server + $quoted  + $delim `
            + $quoted + $queue.Name + $quoted  + $delim `
            + $quoted + $queue.QueueDriver.Name + $quoted  + $delim `
            + $quoted + $queue.QueuePort.Name + $quoted + $delim `
            + $quoted + $queue.DefaultPrintTicket.Duplexing + $quoted + $delim `
            + $quoted + (&{ if($online) {$online.Address} else {""} })  + $quoted + $delim `
            + (&{ if($online) {"True"} else {"False"} }) `
        ) -encoding "ASCII" -append
    }
}