﻿$host.Runspace.ThreadOptions = "ReuseThread"

# Hack to allow for insecure certificate
#
.\"InsecurePolicy.ps1"

# Declarations
#
$response = $null
$baseurl = "http://159.102.12.202:8080/web-services/rest/resource/"
$outfile = ".\Output\" + (Get-Date -Format yyyyMMdd) + "-Clients.csv"

# Prompt for credentials if we do not already have them
#
if ($args.count -ge 3) {
    $cred = New-Object System.Management.Automation.PSCredential($args[1], ($args[2] | ConvertTo-SecureString))
}
elseif ($cred -eq $null) {
    $cred = Get-Credential -Message "Cisco Prime Infrastructure Credentials"
}

# Login
#
$bytes = [System.Text.Encoding]::ASCII.GetBytes(($cred.UserName + ":" + $cred.GetNetworkCredential().Password))
$base64 = [System.Convert]::ToBase64String($bytes)
$basicauthvalue = "Basic $base64"
$headers = @{ Authorization = $basicauthvalue }

# Output header
out-file -filepath $outfile -inputobject ("deviceName,clientInterface,vlan,macAddress,ipAddress") -encoding "ASCII"

# Retrieve client list
#
do {
    # Retrieve a maximum of 1000 devices (Cisco API limit)
    #
    $url = $baseurl + "Lease?clientHostName=SEP64168D51C0CE"
    $url = $baseurl + "Lease"
    $url = $baseurl + "Scope/2MGR-2FL-Lab-384"
    $url = $baseurl + "CCMSubnet/10.10.184.0-24"
    $url = $baseurl + "CCMSubnet"
    $url = $baseurl + "Lease?scopeName=2MGR%2D2FL%2DLab%2D384"

    $response = Invoke-RestMethod $url -Headers $headers
#    $response = Invoke-WebRequest $url -Headers $headers
    
    # Store data
    #
    foreach ($item in $response.queryResponse.entity) { 
        out-file -filepath $outfile -inputobject (($item.clientsDTO | select deviceName, clientInterface, vlan, macAddress | convertto-csv -NoTypeInformation | select -skip 1) + "," +  $(if($item.clientsDTO.ipAddress -eq $null) {'""'} else {$item.clientsDTO.ipAddress | ConvertTo-Csv -NoTypeInformation | select -skip 1})) -encoding "ASCII" -append
    }

}
until ($response.queryResponse.count -eq [int]$response.queryResponse.last + 1)

if ($args.count -ge 4) {
    if (test-path (([io.fileinfo]$args[3]).DirectoryName)) {
        copy $outfile $args[3]
    }
}