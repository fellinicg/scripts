$host.Runspace.ThreadOptions = "ReuseThread"

# Declarations
#
$HomeDirectory = "\\teledsrv1\data\TSD\HomeTest\" + $env:USERNAME
$User = "PANYNJ\" + $env:USERNAME
$HomeDriveLetter = "U:"

# Create Home directory if it does not exist
#
if (!(Test-Path $HomeDirectory)) { New-Item -ItemType directory -Path $HomeDirectory }

# Retrieve ACL
#
$Acl = Get-Acl $HomeDirectory

# Add Modify access rule for user
#
$Acl.AddAccessRule((New-Object System.Security.AccessControl.FileSystemAccessRule("$User", "Modify", "ContainerInherit, ObjectInherit", "None", "Allow")))

# Set access
#
Set-Acl $HomeDirectory $Acl

# Map drive if it isn't already mapped
#
$net = $(New-Object -ComObject WScript.Network)
if (!($net.EnumNetworkDrives() -contains $HomeDriveLetter)) {

    $net.MapNetworkDrive($HomeDriveLetter, $HomeDirectory)
}