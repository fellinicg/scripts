﻿param(
    [parameter()]
    [string]$destpath = "C:\Users\tfellini\OneDrive - The Port Authority of New York & New Jersey\Scripts\Powershell\Output",
#    [string]$destpath = "D:\Feeds\Output",

    [parameter()]
    [string]$destfile = "Registrar-Policies.csv",

    [parameter()]
    [string]$copyto = [System.Management.Automation.Language.NullString]::Value
)

# Credential Manager
#
import-module "C:\Program Files\WindowsPowerShell\Modules\CredentialManager\2.0\CredentialManager.psd1"

# Thread options
#
$host.Runspace.ThreadOptions = "ReuseThread"

# Hack to allow for insecure certificate
#
. .\"InsecurePolicy.ps1"

# Declarations
#
$response = $null
$baseurl = "http://159.102.12.202:8080/web-services/rest/resource/Policy"
$outfile = $(if ($destpath.EndsWith('"')){$destpath -replace ".$"} else {$destpath}) + "\" + (Get-Date -Format yyyyMMdd) + "-" + $destfile

# Retrieve credentials
#
$cred = Get-StoredCredential -Target cisco

# Login
#
$bytes = [System.Text.Encoding]::ASCII.GetBytes(($cred.UserName + ":" + $cred.GetNetworkCredential().Password))
$base64 = [System.Convert]::ToBase64String($bytes)
$basicauthvalue = "Basic $base64"
$headers = @{ Authorization = $basicauthvalue }

# Retrieve Leases
#
$response = ((Invoke-RestMethod $baseurl -Headers $headers).classBases.Policy | select objectOid, name, @{Name="LeaseTime";Expression={($_.optionList.OptionItem | where {$_.number -eq '51'}).value | get-unique}} | convertto-csv -NoTypeInformation)

# Write to file
#
out-file -filepath $outfile -inputobject $response -encoding "ASCII"

# Copy file
#
if ($copyto -ne $null) {
    if (test-path (([io.fileinfo]$copyto).DirectoryName)) {
        copy $outfile $copyto
    }
}