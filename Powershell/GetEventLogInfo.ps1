#Resides on all DCs in C:\Program Files\Alloy and is a scheduled task
$Server = Get-Content Env:ComputerName
$Minutes = -60
$FileStamp = (Get-Date -Format yyyyMMddHH%- (Get-Date).AddHours(-2))
$TimeStamp = (Get-Date -Format M/d/yyyy%" "HH%:00:00 (Get-Date).AddHours(-1))
$UserLog = $Server + '-user.log'
$MachineLog = $Server + '-machine.log'
$Path = '\\patcdb21\d$\EventLogOutput\'
#Get-Date  
#Stop-Service VPatch
If(Test-Path($Path + $UserLog)) {Move-Item ($Path + $Server + '-user.log') ($Path + 'Archive\' + $FileStamp + $UserLog)}
If(Test-Path($Path + $MachineLog)) {Move-Item ($Path +$MachineLog) ($Path + 'Archive\'  + $FileStamp + $MachineLog)}
Get-EventLog Security -Computer $Server -After (Get-Date).AddMinutes($Minutes) | Where-Object {$_.InstanceID -Eq 680 -And $_.EntryType -Eq "SuccessAudit"} | Select -Unique @{Label = "TimeGenerated"; Expression = {$TimeStamp};}, UserName, @{n='Machine';e={$_.ReplacementStrings[2].replace('\\', '')}} | Export-CSV -Path ($Path + $UserLog) -NoTypeInformation
If(!(Get-Content ($Path + $UserLog))) {'"TimeGenerated","UserName","Machine"' > ($Path + $UserLog)}
Get-EventLog Security -Computer $Server -After (Get-Date).AddMinutes($Minutes) | Where-Object {$_.InstanceID -eq 540} | Select -Unique @{Label = "TimeGenerated"; Expression = {$TimeStamp};}, UserName, @{n='IP';e={$_.ReplacementStrings[13]}} | Export-CSV ($Path + $MachineLog) -NoTypeInformation
If(!(Get-Content ($Path + $UserLog))) {'"TimeGenerated","UserName","IP"' > ($Path + $MachineLog)}
#Start-Service VPatch
#Get-Date

#Set-ExecutionPolicy RemoteSigned