﻿$host.Runspace.ThreadOptions = "ReuseThread"

# Declarations
#
[String[]]$commands = @("sh access-lists")
[String[]]$remotehosts = get-content "C:\Users\tfellini\OneDrive - The Port Authority of New York & New Jersey\Scripts\Powershell\20170418-NetworkDevices.csv"
[string]$port = "23"
[int]$waittime = 1000
[string]$outputpath = "c:\"
[string]$logfile = (get-date -Format yyyyMMdd_HHMMss) + ".log"
[string]$errorlogfile = (get-date -Format yyyyMMdd_HHMMss) + "-Error.log"

# Get credentials
#
$cred = get-credential -Message "Network Device Credentials"
$commands = @($cred.UserName, $cred.GetNetworkCredential().Password) + $commands

# Loop over hosts
#
foreach ($remotehost in $remotehosts) {
}
    try {

        # Initialize variable
        #
        $result = "Unable to connect to host: $($remotehost):$port"

        # Open socket
        #
        $socket = new-object System.Net.Sockets.TcpClient($remotehost, $port)

        # Test socket
        #
        if ($socket) {
            $stream = $socket.GetStream()
            $writer = new-object System.IO.StreamWriter($stream)
            $buffer = new-object System.Byte[] 1024 
            $encoding = new-object System.Text.AsciiEncoding

            # Issue commands
            #
            foreach ($command in $commands) {
                $writer.WriteLine($command) 
                $writer.Flush()
                start-sleep -Milliseconds $WaitTime
            }

            # Add extra wait time for last command
            #
            Start-Sleep -Milliseconds ($WaitTime * 4)

            # Save results to variable
            #
            while ($stream.DataAvailable) {
                $read = $stream.Read($buffer, 0, 1024) 
                $result += ($encoding.GetString($buffer, 0, $read))
            }
        }

        # Output results
        #
        $result #| out-file ($outputpath + $logfile)
    }

    catch [Exception] {
        $_.Exception.Message | out-file ($outputpath + $errorlogfile)
    }