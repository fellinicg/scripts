﻿$host.Runspace.ThreadOptions = "ReuseThread"

# Hack to allow for insecure certificate
#
. "C:\Users\tfellini\OneDrive - The Port Authority of New York & New Jersey\Scripts\Powershell\InsecurePolicy.ps1"

# Declarations
#
$devices = $null
$baseurl = "https://10.146.8.11/webacs/api/v2/data/"
$output = "deviceName, deviceIpAddress, clientInterface, connectionType, ipAddress, location, macAddress, vlan`n"

# Prompt for credentials if we do not already have them
#
if ($cred -eq $null) {
    $cred = Get-Credential -Message "Cisco Prime Infrastructure Credentials"
}

# Login
#
$bytes = [System.Text.Encoding]::ASCII.GetBytes(($cred.UserName + ":" + $cred.GetNetworkCredential().Password))
$base64 = [System.Convert]::ToBase64String($bytes)
$basicauthvalue = "Basic $base64"
$headers = @{ Authorization = $basicauthvalue }

$stopwatch = [system.diagnostics.stopwatch]::startNew()

# Retrieve client list
#
do {
    # Retrieve client sessions from Prime
    #
    $url = $baseurl + "ClientSessions?.full=true&.firstResult=" + $(if ($clients.queryResponse.last -eq $null) {0} else { [int]$clients.queryResponse.last + 1}) + "&.maxResults=1000"
    $clients = Invoke-RestMethod $url -Headers $headers

    # Store data 
    #
    foreach ($client in $clients.queryResponse.entity) { 
        $output += ($client.clientSessionsDTO | select deviceName, deviceIpAddress, clientInterface, connectionType, ipAddress, location, macAddress, vlan | ConvertTo-Csv -NoTypeInformation | select -skip 1) + "`n"
    }

    $stopwatch.Elapsed
}
#until ([int]$clients.queryResponse.count -gt [int]$clients.queryResponse.last + 1)
until ([int]$clients.queryResponse.count -eq [int]$clients.queryResponse.last + 1)

$stopwatch.stop()
