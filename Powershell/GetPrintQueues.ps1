$ErrorActionPreference = 'Continue'
$host.Runspace.ThreadOptions = "ReuseThread"

try {Add-Type -AssemblyName System.Printing}
catch  {Write-Warning ".Net feature not installed."} 

# Declarations
#
$outputdir = "\\teledsrv1\data\TSD\HOME\tfellini\Printers\" #"C:\temp\"
$outputfile = $outputdir + '$SERVER$_allprintqueues.csv'
#$delim = [char]9
$delim = ","
$quoted = [char]34

# Permissions
#
$permissions = [System.Printing.PrintSystemDesiredAccess]::AdministrateServer
$queueperms = [System.Printing.PrintSystemDesiredAccess]::AdministratePrinter

# Print server
#
$server = new-object System.Printing.PrintServer

# Get print server queues
#
$queues = $server.GetPrintQueues(@([System.Printing.EnumeratedPrintQueueTypes]::Shared))

# Write header to $outputfile
#
$outputfile = $outputfile.replace('$SERVER$', $server.Name.replace("\\", ""))
out-file -filepath $outputfile -inputobject (`
    $quoted + "PrintServer" + $quoted + $delim `
    + $quoted + "Queue" + $quoted + $delim `
    + $quoted + "Driver" + $quoted + $delim `
    + $quoted + "Port" + $quoted + $delim `
    + $quoted + "Duplex Capable" + $quoted + $delim `
    + $quoted + "Duplex" + $quoted + $delim `
    + $quoted + "IPAddress" + $quoted + $delim `
    + $quoted + "Online" + $quoted`
    ) -encoding "ASCII"
    
# Loop through queues
#
foreach($queue in $queues) {
    try {
#        if ($queue.GetPrintCapabilities().DuplexingCapability.Contains([System.Printing.Duplexing]::TwoSidedLongEdge) -and $queue.QueueDriver.Name.contains("PCL6 Driver for Universal Print")) {
            $portip = (gwmi win32_tcpipprinterport | where {$_.Name -eq $queue.QueuePort.Name}).HostAddress
            $online = Test-Connection -ComputerName $portip -Count 1 -ErrorAction SilentlyContinue
            out-file -filepath $outputfile -inputobject ( `
                $quoted + $server.Name.replace("\\", "") + $quoted  + $delim `
                + $quoted + $queue.Name + $quoted  + $delim `
                + $quoted + $queue.QueueDriver.Name + $quoted  + $delim `
                + $quoted + $queue.QueuePort.Name + $quoted + $delim `
                + (&{ if ($queue.GetPrintCapabilities().DuplexingCapability -and $queue.GetPrintCapabilities().DuplexingCapability.Contains([System.Printing.Duplexing]::TwoSidedLongEdge)) {"Yes"} else {"No"} }) + $delim `
                + $quoted + $queue.DefaultPrintTicket.Duplexing + $quoted + $delim `
                + $quoted + $portip  + $quoted + $delim `
                + (&{ if($online) {"True"} else {"False"} }) `
            ) -encoding "ASCII" -append
#        }
    }
    catch {}
}

