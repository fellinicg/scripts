Import-Module ActiveDirectory
Import-Module GroupPolicy

$UserName = Read-Host -Prompt 'Input username'
$GUIDRegex = "{[a-zA-Z0-9]{8}[-][a-zA-Z0-9]{4}[-][a-zA-Z0-9]{4}[-][a-zA-Z0-9]{4}[-][a-zA-Z0-9]{12}}"
$U = Get-ADUser $UserName -Properties *
$OU = $U.DistinguishedName -Replace ("CN=" + $U.CN + ","), ""

$DriveMappings = @()
ForEach($Level in $OU.Split(",")) {
    If($Level.StartsWith("OU=")) {
        $LinkedGPOs = Get-ADOrganizationalUnit -Filter 'DistinguishedName -eq $OU' | Select -ExpandProperty LinkedGroupPolicyObjects

        ForEach($LinkedGPO in $LinkedGPOs) {            
            $Result = [Regex]::Match($LinkedGPO,$GUIDRegex);            
            If($Result.Success) {            
                $GPOGuid = $Result.Value.TrimStart("{").TrimEnd("}")
                $GPODisplayName = (Get-GPO -Guid $GPOGuid).DisplayName        
                $GPO = [xml](Get-GPOReport -Name $GPODisplayName -ReportType xml)
                If ($GPO.GPO.User.ExtensionData.Extension.DriveMapSettings) {
                    ForEach ($Drive in $GPO.GPO.User.ExtensionData.Extension.DriveMapSettings.Drive) {
                        $DriveMapping = New-Object -TypeName PSObject
#                        $DriveMapping | Add-Member -Type NoteProperty -Name OU -Value ([char]34 + $OU + [char]34)
                        $DriveMapping | Add-Member -Type NoteProperty -Name OU -Value $OU
                        $DriveMapping | Add-Member -Type NoteProperty -Name GPO -Value $GPODisplayName
                        $DriveMapping | Add-Member -Type NoteProperty -Name Order -Value $($Drive.GPOSettingOrder)
                        $DriveMapping | Add-Member -Type NoteProperty -Name "Drive Letter" -Value $($Drive.Properties.Letter)
                        $DriveMapping | Add-Member -Type NoteProperty -Name Path -Value $($Drive.Properties.Path -Replace "%USERNAME%", $UserName)
                        $DriveMapping | Add-Member -Type NoteProperty -Name Filter -Value $($Drive.Filters.FilterGroup.name)
                        $DriveMapping | Add-Member -Type NoteProperty -Name "Filter Boolean" -Value $($Drive.Filters.FilterGroup.bool)
                        $DriveMapping | Add-Member -Type NoteProperty -Name "Filter Not" -Value $($Drive.Filters.FilterGroup.not)

                        $DriveMappings += $DriveMapping
                        #Write-Host "$([char]34 + $OU + [char]34),$GPODisplayName,$($Drive.GPOSettingOrder),$($Drive.Properties.Letter),$($Drive.Properties.Path -Replace "%USERNAME%", $UserName),$($Drive.Filters.FilterGroup.name),$($Drive.Filters.FilterGroup.bool),$($Drive.Filters.FilterGroup.not)"
                    }
                }
            }            
                    
        }
        $OU = $OU -Replace ($Level + ","), ""
    }
}
$Style = "<style>"
$Style += "table {font-family: Arial;border-collapse: collapse;text-align: left;color: #606060;white-space: nowrap;}table td {font-size: 10px;font-weight: normal;vertical-align: middle;padding: 0.5em;border-left: 1px solid #000;border-right: 1px solid #000;}table th {background-color: White;vertical-align: middle;padding: 0.6em;font-size: 0.8em;}table tr th,table tr.separator {padding: 0.5em;background-color: #909090;background: -webkit-gradient(linear, left top, left bottom, from(#909090), to(#ababab));background: -moz-linear-gradient(top, #909090, #ababab);color: #efefef;}table tr:nth-child(odd) {background-color: #fafafa;}table tr:nth-child(even) {background-color: #efefef;}table tr:last-child {border-bottom: solid 1px #404040;}table tr:hover {cursor: pointer;background-color: #cccc99;background: -webkit-gradient(linear, left top, left bottom, from(#909090), to(#ababab));background: -moz-linear-gradient(top, #909090, #ababab);color: White;}"
$Style += "</style>"

$UserTable = $U | ConvertTo-Html @{Expression={$_.CN};Label="User Name"}, @{Expression={$_.GivenName};Label="First Name"}, @{Expression={$_.Surname};Label="Last Name"}, @{Expression={$_.Department};Label="Department"} -Fragment
$MapDriveTable = $DriveMappings | ConvertTo-HTML -Fragment

ConvertTo-HTML -Body ($UserTable + [char]10 + "<br/>" + [char]10 + $MapDriveTable) -Title "Drive Mapping Report" -head $Style | Out-File -FilePath c:\temp\output.html
Invoke-Expression c:\temp\output.html