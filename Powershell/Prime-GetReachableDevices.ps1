﻿param(
    [parameter()]
    [string]$user = [System.Management.Automation.Language.NullString]::Value,
	
    [parameter()]
    [string]$key = [System.Management.Automation.Language.NullString]::Value,

    [parameter()]
    [string]$destpath = ".\Output",

    [parameter()]
    [string]$destfile = "Prime-Devices.csv",

    [parameter()]
    [string]$copyto = [System.Management.Automation.Language.NullString]::Value
)

# Thread options
#
$host.Runspace.ThreadOptions = "ReuseThread"

# Hack to allow for insecure certificate
#
. .\"InsecurePolicy.ps1"

# Declarations
#
$response = $null
$baseurl = "https://10.146.8.11/webacs/api/v2/data/"
$outfile = $(if ($destpath.EndsWith('"')){$destpath -replace ".$"} else {$destpath}) + "\" + (Get-Date -Format yyyyMMdd) + "-" + $destfile
$output = @("deviceId,deviceName,deviceType,ipAddress,productFamily,softwareType,softwareVersion,collectionTime,adminStatus")

# Prompt for credentials if we do not already have them
#
if ($user -ne $null -and $key -ne $null) {
    $cred = New-Object System.Management.Automation.PSCredential($user, ($key | ConvertTo-SecureString))
}
elseif ($cred -eq $null) {
    $cred = Get-Credential -Message "Cisco Prime Infrastructure Credentials"
}

# Login
#
$bytes = [System.Text.Encoding]::ASCII.GetBytes(($cred.UserName + ":" + $cred.GetNetworkCredential().Password))
$base64 = [System.Convert]::ToBase64String($bytes)
$basicauthvalue = "Basic $base64"
$headers = @{ Authorization = $basicauthvalue }

#$stopwatch = [system.diagnostics.stopwatch]::startNew()

# Retrieve device list
#
do {
    # Retrieve devices in batches of 1000 max (Cisco API limit is 1000 per call)
    #
    $maxresults = "1000"
    $url = $baseurl + "Devices?.full=true&reachability=REACHABLE&.firstResult=" + $(if ($response.queryResponse.last -eq $null) {0} else { [int]$response.queryResponse.last + 1}) + "&.maxResults=" + $maxresults
    $response = Invoke-RestMethod $url -Headers $headers

    # Store data
    #
    foreach ($item in $response.queryResponse.entity) { 
        $output += ($item.devicesDTO | select deviceId,deviceName,deviceType,ipAddress,productFamily,softwareType,softwareVersion,collectionTime,adminStatus | convertto-csv -NoTypeInformation | select -skip 1)
#        out-file -filepath $outfile -inputobject ($item.devicesDTO | select deviceId,deviceName,deviceType,ipAddress,productFamily,softwareType,softwareVersion,collectionTime,adminStatus | convertto-csv -NoTypeInformation | select -skip 1) -encoding "ASCII" -append
    }

#    $stopwatch.Elapsed
}
until ($response.queryResponse.count -eq [int]$response.queryResponse.last + 1)

# Write to file
#
out-file -filepath $outfile -inputobject $output -encoding "ASCII"

# Copy file
#
if ($copyto -ne $null) {
    if (test-path (([io.fileinfo]$copyto).DirectoryName)) {
        copy $outfile $copyto
    }
}

#$stopwatch.stop()

#$stopwatch
