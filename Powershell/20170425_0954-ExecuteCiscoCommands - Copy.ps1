﻿$host.Runspace.ThreadOptions = "ReuseThread"

# Declarations
#
[int]$waittime = 1000
[string]$logfile = (get-date -Format yyyyMMdd_HHMMss) + ".log"
[string]$errorlogfile = (get-date -Format yyyyMMdd_HHMMss) + "-Error.log"

# Open dialog box
#
Add-Type -AssemblyName System.Windows.Forms
$openFileDialog = New-Object System.Windows.Forms.OpenFileDialog
$openFileDialog.ShowHelp = $true

# Prompt for file that contains a list of remote hosts
#
$openFileDialog.Title = "Select a Remote Hosts File"
do {
    $button = $openFileDialog.ShowDialog()
}
until ($button -eq "OK")
[String[]]$remotehosts = get-content $openFileDialog.Filename

# Prompt for port number
#
Add-Type -AssemblyName Microsoft.VisualBasic
[string]$port = [Microsoft.VisualBasic.Interaction]::InputBox("Please enter port number", "Remote Port", "23")

# Prompt for file that contains commands
#
$openFileDialog.Title = "Select a Commands File"
do {
    $button = $openFileDialog.ShowDialog()
}
until ($button -eq "OK")
[String[]]$commands = get-content $openFileDialog.Filename

# Prompt for output directory
#
$app = New-Object -ComObject Shell.Application
do {
    [string]$outputpath = $app.BrowseForFolder(0, "Please select an output directory", 512, "c:\").self.path
}
until ($outputpath.length -gt 0)

# Get credentials
#
do {
    $cred = get-credential -Message "Network Device Credentials"
}
until ($cred.UserName.length -gt 0 -and $cred.Password.length -gt 0)
$commands = @($cred.UserName, $cred.GetNetworkCredential().Password) + $commands

break

# Loop over hosts
#
foreach ($remotehost in $remotehosts) {

    try {

        # Open socket
        #
        $socket = new-object System.Net.Sockets.TcpClient($remotehost, $port)

        # Test socket
        #
        if ($socket) {
            $stream = $socket.GetStream()
            $writer = new-object System.IO.StreamWriter($stream)
            $buffer = new-object System.Byte[] 1024 
            $encoding = new-object System.Text.AsciiEncoding

            # Issue commands
            #
            foreach ($command in $commands) {
                $writer.WriteLine($command) 
                $writer.Flush()
                start-sleep -Milliseconds $waittime
            }

            # Add extra wait time for last command
            #
            Start-Sleep -Milliseconds ($waittime * 4)

            # Save results to variable
            #
            while ($stream.DataAvailable) {
                $read = $stream.Read($buffer, 0, 1024) 
                $result += ($encoding.GetString($buffer, 0, $read))
            }
        }
        else {   
            $result += "Unable to connect to host: $($remotehost):$port"
        }

        # Output results
        #
        $result #| out-file ($outputpath + $logfile)
    }
    catch [Exception] {
        $_.Exception.Message | out-file ($outputpath + $errorlogfile)
    }

}