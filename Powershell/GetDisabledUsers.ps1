Import-module ActiveDirectory

Get-ADUser -Filter {(enabled -eq 'false')} -property * | select-object GivenName, Surname, name, @{n="MemberOf";e={($_.MemberOf | % {(Get-ADObject $_).Name}) -join '; '}} | Export-Csv disabledusers.csv -notype