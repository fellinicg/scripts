import win32service
import win32serviceutil
import win32api
import win32con
import win32event
import win32evtlogutil
import winreg
import os, sys, string, time, inspect, datetime, types

class service(win32serviceutil.ServiceFramework):
	
	_svc_name_ = "FCGSvcMon"
	_svc_display_name_ = "FCG Service Monitor"
	_svc_description_ = "Monitor Services"

	def __init__(self, args):
		win32serviceutil.ServiceFramework.__init__(self, args)
		self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)

	def SvcStop(self):
		self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
		win32event.SetEvent(self.hWaitStop)                    

	def SvcDoRun(self):
		import servicemanager
		servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,servicemanager.PYS_SERVICE_STARTED,(self._svc_name_, '')) 

		os.chdir(os.path.dirname(winreg.EnumValue(winreg.OpenKey(winreg.ConnectRegistry(None, winreg.HKEY_LOCAL_MACHINE), str.format('SYSTEM\CurrentControlSet\Services\{}\PythonClass', self._svc_name_)), 0)[1]))

##		if os.path.exists(cfg.log):
##			os.remove(cfg.log)

		cfg.get()

		# This is how long the service will wait to run / refresh itself (see script below)
		# self.timeout = 640000    #640 seconds / 10 minutes (value is in milliseconds)
		self.timeout = int(cfg.poll) * 1000

		while 1:
			# Wait for service stop signal, if I timeout, loop again
			rc = win32event.WaitForSingleObject(self.hWaitStop, self.timeout)
			# Check to see if self.hWaitStop happened
			if rc == win32event.WAIT_OBJECT_0:
				# Stop signal encountered
				servicemanager.LogInfoMsg(self._svc_name_ + " has stopped.")  #For Event Log
				break
			else:
				try:
					if cfg.filemt != os.path.getmtime(cfg.file):
						log.write(cfg.file + ' has been modified.')
						cfg.get()
						self.timeout = int(cfg.poll) * 1000
					for item in monitor.list:
						currstate = winsvc.state(item.service, item.server)
						if currstate != item.svcstate:
							log.write(str.format('{} - {} service went from {} to {} alert sent to {}', item.server, item.service, item.svcstate, currstate, item.alertsto))
							email.send(item.alertsto, str.format('{} {} - {}', item.server, item.service, currstate), '')
							item.svcstate = currstate
							item.lastupd = str(datetime.datetime.now())
				except Exception as err:
					log.error(str(item.server) + ' ' + str(err))

class cfg:
	db = 'svcmon.sqlite'
	emailfrom = 'svcmon@panynj.gov'
	errorlog = 'error.log'
	file = 'svcmon.cfg'
	filemt = None
	log = 'svcmon.log'
	marker = 'running'
	poll = 20

	@classmethod
	def get(self):
		try:
			monitor.clear()
			cfg.filemt = os.path.getmtime(cfg.file)
			f = open(cfg.file, "r").read()
			for section in f.replace('[', '||[').lstrip('||').split('||'):
				if section.split('\n')[0].startswith('[') and section.split('\n')[0].endswith(']'):
					hdr = section.split('\n')[0].lstrip('[').rstrip(']').lower().strip()
					for i,item in enumerate(section.split('\n')):
						if i > 0 and item.strip():
							if hdr == 'settings' and item.find('=') > 0 and hasattr(cfg, item.split('=')[0].lower()):
								setattr(cfg, item.split('=')[0].lower(), item.split('=')[1])
							else:
								ss = srvsvc()
								ss.server = hdr.upper()
								ss.service = item.split('=')[0]
								if item.find('=') > 0:
									ss.alertsto = item.split('=')[1]
								ss.svcstate = str(winsvc.state(ss.service, ss.server))
								ss.lastupd = str(datetime.datetime.now())
								monitor.list.append(ss)
		except Exception as err:
			log.error(err)

class email:

	def send(recp, subj, body, srv = 'TELEDB29'):
		import win32com.client

		try:
			ado = win32com.client.Dispatch('ADODB.Connection')
			conn = 'Provider=SQLOLEDB.1;Data Source=%s;Initial Catalog=msdb;Integrated Security=SSPI;' % srv
			sql = "exec msdb.dbo.sp_send_dbmail @profile_name = 'Service Alerts', @recipients = '%s',	@subject = '%s' ,@body = '%s', @importance = 'High'" % (recp, subj, body)
			ado.Open(conn)
			ado.Execute(sql)
		except Exception as err:
			log.error(err)
		finally:
			if ado.State:
				ado.Close()

class log:
	@classmethod
	def write(cls, txt = '', file = ''):
		try:
			if len(txt.strip()):
				file = cfg.log if not len(file.strip()) else file
				f = open(file, 'a')
				f.write(datetime.datetime.now().strftime("%m/%d/%Y %I:%M %p: ") + str(txt) + '\n')
				f.close()
		except:
			f.close()
			pass

	@classmethod
	def error(cls, txt):
		txt = str(txt) + ' [' + str(inspect.stack()[1][3]) + ']'
		log.write(txt, cfg.errorlog)
	
class monitor:
	list = []

	@classmethod
	def clear(self):
		try:
			self.list = []
		except:
			pass

class srvsvc:
	server = ''
	service = ''
	alertsto = ''
	svcstate = ''
	lastupd = ''

	@classmethod
	def print(self):
		result = ''
		for i in list(self.__dict__):
			if not i.startswith('__') and not callable(getattr(self, i)):
				result += i + ' = ' + getattr(self, i) + '\n'
		return result
	
class winsvc:
	svcstate = {1:'Stopped', 2:'Start Pending', 3:'Stop Pending', 4:'Running'}
	
	def state(svc, machine = None):
		return winsvc.svcstate[win32serviceutil.QueryServiceStatus(svc, machine)[1]]	# scvType, svcState, svcControls, err, svcErr, svcCP, svcWH

	def stop(svc, machine = None):
		result = 'ERR'

		try:
			if winsvc.state(svc, machine).lower() == 'stopped':
				return winsvc.svcstate[1]
			result = svcstate[win32serviceutil.StopService(svc, machine)[1]]
			while result != winsvc.svcstate[1]:
				time.sleep(1)
				result = winsvc.state(svc, machine)
		except Exception as err:
			log.error(err)
		finally:
			return result

	def start(svc, svc_arg = None, machine = None):
		result = 'ERR'

		try:
			if winsvc.state(svc, machine).lower() == 'running':
				return winsvc.svcstate[4]
			if not svc_arg is None:
				if type(svc_arg) in StringTypes:
					# win32service expects a list of string arguments
					svc_arg = [svc_arg]
			win32serviceutil.StartService(svc, svc_arg, machine)
			result = winsvc.state(svc, machine)
			while result == winsvc.svcstate[2]:
				time.sleep(1)
				result = winsvc.state(svc, machine)
		except Exception as err:
			log.error(err)
		finally:
			return result

def ctrlHandler(ctrlType):
 return True
                  
if __name__ == '__main__':   
 win32api.SetConsoleCtrlHandler(ctrlHandler, True)
 win32serviceutil.HandleCommandLine(service)