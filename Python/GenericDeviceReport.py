__author__ = 'Stefan Honeder'
__version__ = '1.0'

# ############################################################################
# Copyright (c) 2017 Stefan Honeder
# All rights reserved.
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
#
# TECHNICAL ASSISTANCE CENTER (TAC) SUPPORT IS NOT AVAILABLE FOR THIS SCRIPT.
# ############################################################################

import traceback
import os
import errno
import jsonpath
import requests
import sys
import logging
import logging.config
import base64
import json
import time
import csv
from requests.packages.urllib3.exceptions import InsecureRequestWarning, InsecurePlatformWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
requests.packages.urllib3.disable_warnings(InsecurePlatformWarning)

# class GenericReport

class GenericReport(object):
    _logger = None
    _inventoryBase = "webacs/api/v1/data/InventoryDetails"
    _inventoryBaseFull = "webacs/api/v1/data/InventoryDetails?.full=true"
    _inventoryPaging="&.firstResult=%s&.maxResults=10"

    _defaultDirectory = "reports/"
    _basicTools = None
    
    _primeIP = None
    _username = None
    _password = None

    def __init__(self, _primeIP, _username, _password):
        self._logger = logging.getLogger()
        self._primeIP = _primeIP
        self._username= _username
        self._password = _password
        if self.connectionTest == False:
            self._logger.error("Problems on getting connection to Prime, please check credentials and IP")
        self.check_directory()
        self._logger.debug("%s v%s initialized"%(type(self),__version__))


    # verify that the destination directory exists, and create if not
    def check_directory(self):
        try:
            if not os.path.exists(self._defaultDirectory):
                os.makedirs(self._defaultDirectory)
        except OSError as exception:
            if exception.errno != errno.EEXIST:
                raise

    # get the inventory and write it to a csv file
    def getDeviceInventory(self):
        self._logger.debug("getDeviceInventory()")
        queryURL = self._inventoryBase

        self._logger.debug("_basicTools.doRestCall(GET,%s)"%queryURL)
        result = self.doRestCall("GET", '%s' % (queryURL))

        if (result== None):
            return

        count = int(jsonpath.jsonpath(result, "$.queryResponse.@count")[0])
        end = -1
        try:
            # open the file to write the result to
            with open(self._defaultDirectory+"deviceInventory"+ time.strftime('%Y%m%d-%H%M%S')+".csv",'w') as outfile:
                outfile.write("deviceId,productFamily,deviceName,ipAddress,modelNr,udiSerialNr\n")
                csvfile = csv.writer(outfile,quoting=csv.QUOTE_NONNUMERIC)
                # page through the inventory in steps of 10 devices so that we don't overload the API
                while (count > end+1):
                    result = self.doRestCall("GET", '%s%s' % (self._inventoryBaseFull, self._inventoryPaging%(end+1)))
                    if result==None: return None
                    end = int(jsonpath.jsonpath(result, "$.queryResponse.@last")[0])
                    self._logger.debug("current paging site = %s"%end)
                    # one device can contain more than one product (modular chassis, stacks,...)
                    for i in range(0,len(result["queryResponse"]["entity"])):
                        # leverage jsonpath to filter/request the required information
                        thisDevice = jsonpath.jsonpath(result,"$.queryResponse.entity["+str(i)+"].inventoryDetailsDTO.summary['deviceId','productFamily','deviceName','ipAddress']")
                        thisParts=jsonpath.jsonpath(result,"$.queryResponse.entity["+str(i)+"].inventoryDetailsDTO.udiDetails.udiDetail[?(@.modelNr && @.udiSerialNr && @.modelNr!='Unspecified')].['modelNr','udiSerialNr']")
                        if thisParts!=False:
                            for j in range (0, (len(thisParts)/2)):
                                fullrow = thisDevice+thisParts[(j*2):(j*2+2)]
                                csvfile.writerow(fullrow)
            return True
        except:
            err = sys.exc_info()[0]
            msg_det = sys.exc_info()[1]
            self._logger.error("Error: %s  Details: %s StackTrace: %s" %
                  (err, msg_det, traceback.format_exc()))

	 # Make the REST call using the defined IP and credentials, command, http url, data for the body (if any)
    def doRestCall(self, command, apiUrl, aData=None):
        response_json = None
        payload = None
        try:

            #if data for the body is passed in put into JSON format for the payload
            if (aData != None):
                payload = json.dumps(aData)


            userAndPass = "%s:%s"%(self._username,self._password)
            base64ticket = base64.b64encode(userAndPass.encode("utf-8")).decode("ascii")

            header = {"Authorization":"Basic %s"%base64ticket,"accept": "application/json",'Connection':'close' }


            if (command == "GET"):
                url = "https://%s/%s" % (self._primeIP,apiUrl)
                self._logger.debug("GET URL %s" %url)
                r = requests.get(url, headers = header, data = payload, verify=False)
            elif (command == "POST"):
                url = "https://%s/%s" % (self._primeIP,apiUrl)
                self._logger.debug("POST URL %s" %url)
                r = requests.post(url,  headers=header,data=payload, verify=False)
            else:
                #if the command is not GET or POST we dont handle it.
                self._logger.error("Unknown command, please use 'get' or 'post'")
                return

            #if no data is returned print a message; otherwise print data to the screen
            if (not r):
                self._logger.error("Error no Data received, Returned status code: %d" % r.status_code)
            else:
                self._logger.info("Returned status code: %d" % r.status_code)
                #put into dictionary format
                response_json = r.json()
            return response_json;
        except requests.exceptions.Timeout as e:
            err = sys.exc_info()[0]
            msg_det = sys.exc_info()[1]
            self._logger.error("Our request timed out. (Error:%s, Details:%s"%(err,msg_det))
        except requests.exceptions.TooManyRedirects:
            err = sys.exc_info()[0]
            msg_det = sys.exc_info()[1]
            self._logger.error("Our request had too many retries. (Error:%s, Details:%s"%(err,msg_det))
        except requests.exceptions.ConnectionError:
            err = sys.exc_info()[0]
            msg_det = sys.exc_info()[1]
            self._logger.error("Our request was refused. (Error:%s, Details:%s"%(err,msg_det))
        except requests.exceptions.RequestException as e:
            err = sys.exc_info()[0]
            msg_det = sys.exc_info()[1]
            self._logger.error("Some unknow exception occured. Please check your input (Error:%s, Details:%s"%(err,msg_det))
        except:
            err = sys.exc_info()[0]
            msg_det = sys.exc_info()[1]
            self._logger.error("Error: %s  Details: %s StackTrace: %s" %
                  (err, msg_det, traceback.format_exc()))

    def connectionTest(self):
        url = "https://" + self._primeIP + "/webacs/api/v1/op/info/version"

        userAndPass = "%s:%s"%(self._username,self._password)
        base64ticket = base64.b64encode(userAndPass.encode("utf-8")).decode("ascii")
        self._logger.debug(base64ticket)

        # important to note, to explicitly add 'Connection':'close' since otherwise
        # too many concurrent API sessions will be opened, and the user get's locked out
        header = {"Authorization":"Basic %s"%base64ticket,"accept": "application/json",'Connection':'close' }

        self._logger.debug("GET URL %s"%url)

        response = requests.get(url, headers=header, verify=False)

        #Check if a response was received. If not, print an error message.
        if (not response):
            self._logger.error("No data returned!")
            return False
        else:
            #Data received.  Get the version and log it
            r_json = response.json()
            ticket = r_json["mgmtResponse"]["versionInfoDTO"]["result"]
            self._logger.info("Prime Version: %s" % ticket)
        self._ticket = ticket
        return True