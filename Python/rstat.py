#!/usr/bin/env python

import httplib
import urllib
import datetime
import inspect
import operator
import os
import re
import json
import sys
import time
import traceback

class log:
	@classmethod
	def write(cls, txt = "", file = "rstat.log"):
		f = None
		try:
			if len(txt.strip()):
				file = (os.getcwd() if len(sys.path[0]) == 0 else sys.path[0]) + "/" + file
				f = open(file, "a")
				f.write(datetime.datetime.now().strftime("%m/%d/%Y %I:%M:%S %p: ") + str(txt) + "\n")
				f.close()
		except:
			if f:
				f.close()
			pass
			
	@classmethod
	def error(cls, txt=None):
		err = "File: " + inspect.trace()[0][1] + ", Function: " + inspect.trace()[0][3] + ", Line: " + str(inspect.trace()[0][2]) + ", Error: " + str(sys.exc_info()[1])
		if txt:
			err += ", Msg: " + txt
		log.write(err, "error.log")

class thermostat:
	_day = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
	_sday = ["mon","tue","wed","thur","fri","sat","sun"]
	_days = {"Monday":"mon","Tuesday":"tue","Wednesday":"wed","Thursday":"thur","Friday":"fri","Saturday":"sat","Sunday":"sun"}
	_dayparts = {0:"Morning",2:"Day",4:"Evening",6:"Night"}
		
	def __init__(self, location=None):
		self.location = location
			
	def away(self):
		ret = True
		url = "/tstat/program/" + self.mode().lower()
		try:
			for item in self._day:
				params = '{"' + str(self._day.index(item)) + '":[300,62,420,62,1080,62,1320,62]}'
				self.post(url, params)
			ret = self.getschedule()
		except:
			ret = False
			log.error()
			pass
		finally:
			return ret
			
	def connect(self, loc=None):
		conn = None
		try:
			loc = self.location if not loc else loc
			conn = httplib.HTTPConnection(loc)
		except:
			conn = None
			log.error()
		finally:
			return conn
		
	def cool(self, temp=None, hold=""):
		url = "/tstat"
		try:
			if not temp:
#				params = '{"tmode":0}'
#				self.post(url, params)
				params = '{"tmode":2}'
			else:
				temp = float(temp)
				if temp >= 50 and temp <= 85:
					hold = ',"hold":1' if hold.lower() == "hold" else ',"hold":0'
					params = '{"tmode":2,"t_cool":' + str(temp) + hold + '}'
			self.post(url, params)
		except:
				log.error()
				pass

	def copyschedule(self, src="", dest=""):
		ret = ""
		if not src: return ret
		days = dest.split(",")
		baseurl = "/tstat/program/" + self.mode().lower()
		try:
			sched = str(self.get(baseurl + "/" + src).items()[0][1])
			if not days[0]:
				days = []
				for day in self._day:
					days.append(self._days[day])
			for idx, day in enumerate(days):
				url = baseurl + "/" + day.lower()
				params = '{"' + str(self._sday.index(day)) + '":' + sched + '}'
				self.post(url, params)
			ret = self.getschedule(",".join(days))
		except:
			ret = None
			log.error()
			pass
		finally:	
			return ret
	
	def fan(self, setting=None):
		state = ["Auto","Circulate","On"]
		url = "/tstat"
		try:
			if not setting: 
				url += "/fmode"
				ret = state[self.get(url)["fmode"]]
			else:
				params = '{"fmode":' + str(state.index(str(setting.capitalize()))) + '}'
				self.post(url, params)
				time.sleep(1)
				ret = self.fan()
		except:
			log.error()
			pass
		finally:
			return ret

	def formattime(self, minutes):
		return time.strftime("%I:%M %p", (datetime.datetime(2000, 1, 1) + datetime.timedelta(minutes=minutes)).timetuple())

	def get(self, url, params=None):
		ret = None
		try:
			conn = self.connect()
			if conn:
				conn.request("GET", url, params)
				ret = json.loads(conn.getresponse().read())
		except:
			ret = None
			log.error()
		finally:
			return ret

	def getname(self):
		self.name = self.get("/sys/name")["name"]
		return self.name

	def getschedule(self, days=""):
		ret = ""
		days = days.split(",")
		baseurl = "/tstat/program/" + self.mode().lower()
		try:
			for day in days:
				url = baseurl + "/" + day.lower() if day else baseurl
				prog = self.get(url)
				prog = sorted(prog.iteritems(), key=operator.itemgetter(0))
				if ret == "":
					ret = "{"
				for item in prog:
					ret += "'" + self._day[int(item[0])] + "'" + ": ["
					for p in item[1]:
						if item[1].index(p) in self._dayparts:
							dp = " [" + self._dayparts[item[1].index(p)] + "]"
							ret += "'" + self.formattime(p) + "', "
						else:
							ret += str(p) + ", "
					ret = ret[:-2] + "], "
		except:
			ret = None
			log.error()
			pass
		finally:	
			return ret[:-2] + "}"

	def gettime(self):
		ret = None
		url = "/tstat/time"
		try:
			currtime = self.get(url)
			AmPm = "AM" if currtime["hour"] < 12 else "PM"
			if currtime["hour"] == 0:
				currtime["hour"] = 12
			elif currtime["hour"] > 12:
				currtime["hour"] = currtime["hour"] - 12
			if currtime["minute"] < 10:
				currtime["minute"] = "0" + str(currtime["minute"])
			ret = self._day[currtime["day"]] + " " + str(currtime["hour"]) + ":" + str(currtime["minute"]) + " " + AmPm
		except:
			ret = None
			log.error()
			pass
		finally:
			return ret

	def heat(self, temp=None, hold=""):
		url = "/tstat"
		try:
			if not temp:
				params = '{"tmode":0}'
				self.post(url, params)
				params = '{"tmode":1}'
			else:
				temp = float(temp)
				if temp >= 50 and temp <= 80:
					hold = ',"hold":1' if hold.lower() == "hold" else ',"hold":0'
					params = '{"tmode":1,"t_heat":' + str(temp) + hold + '}'
			self.post(url, params)
		except:
			log.error()
			pass

	def hold(self, state=""):
		ret = None
		url = "/tstat"
		try:
			if state and (state.lower() == "on" or state.lower() == "off"):
				if state.lower() == "on":
					params = '{"hold":1}'
				else:
					params = '{"hold":0}'
				self.post(url, params)
		except:
			log.error()
			pass
                finally:
			ret = self.get("/tstat/hold")["hold"]
                        return ret

	def isvalid(self, loc=None):
		ret = True
		try:
			loc = self.location if not loc else loc
			self.connect(loc).request("GET", "/")
		except:
			ret = False
		return ret

	def mode(self, type=None):
		state = ["Off","Heat","Cool"]
		url = "/tstat"
		try:
			if not type:
				url += "/tmode"
				ret = state[self.get(url)["tmode"]]
			else:
				params = '{"tmode":' + str(state.index(str(type.capitalize()))) + '}'
				self.post(url, params)
				time.sleep(1)
				ret = self.mode()
		except:
			ret = None
			log.error()
			pass
		finally:
			return ret
		
	def post(self, url, params=None):
		conn = self.connect()
		conn.request("POST", url, params)
		return json.loads(conn.getresponse().read())

	def reset(self):
		self.hold("on")
		self.hold("off")
		return

	def setname(self, name):
		try:
			url = "/sys/name"
			params = '{"name":"' + str(name) + '"}'
			ret = self.post(url, params)
		except:
			ret = None
			log.error()
			pass
		finally:	
			return ret
	
	def setschedule(self, sched="", days=""):
		ret = ""
		sched = sched.split(",")
		if len(sched) != 8:
			return ret
		days = days.split(",")
		baseurl = "/tstat/program/" + self.mode().lower()
		csched = "["
		try:
			for idx, item in enumerate(sched):
				if idx % 2 == 0:
					if len(sched[idx]) != 4: return ""
					csched += str(int(item[0:2])*60 + int(item[2:4])) + ","
				else:
					csched += str(item) + ","
			csched = csched[:-1] + "]"
			if not days[0]:
				days = []
				for day in self._day:
					days.append(self._days[day])
			for idx, day in enumerate(days):
				url = baseurl + "/" + day.lower()
				params = '{"' + str(self._sday.index(day)) + '":' + csched + '}'
				self.post(url, params)
			ret = self.getschedule(",".join(days))
		except:
			ret = None
			log.error()
			pass
		finally:	
			return ret

	def settime(self, day=None, hour=None, minute=None):
		ret = None
		url = "/tstat/time"
		try:
			day = self._day.index(datetime.datetime.now().strftime("%A")) if not day else self._day.index(day.capitalize())
			hour = datetime.datetime.now().strftime("%H") if not hour else hour
			minute = datetime.datetime.now().strftime("%M") if not minute else minute
			params = '{"day":' + str(day) + ',"hour":' + str(hour) + ',"minute":' + str(minute) + '}'
			self.post(url, params)
			ret = self.gettime()
		except:
			ret = None
			log.error()
			pass
		finally:	
			return ret
	
	def temp(self):
		ret = []
		url = "/tstat"
		hold = "off"
		mode = "off"
		fan = "Auto"
		fstate = "Off"
		try:
			resp = self.get(url)
			if resp.has_key("temp"):
				ret.append(str(resp["temp"]))
			else:
				return ret
			if resp.has_key("t_heat"):
				ret.append(str(resp["t_heat"]))
			elif resp.has_key("t_cool"):
				ret.append(str(resp["t_cool"]))
			else:
				ret.append("--")
			if resp.has_key("hold"):
				hold = "off" if resp["hold"] == 0 else "on"
			ret.append(str(hold))
			if resp.has_key("tmode"):
				mode = "off" if resp["tmode"] == 0 else "Heat" if resp["tmode"] == 1 else "Cool"
			ret.append(str(mode))
			if resp.has_key("fmode"):
				fan = "Auto" if resp["fmode"] == 0 else "On" if resp["fmode"] == 2 else "Circulate"
			ret.append(str(fan))
			if resp.has_key("fstate"):
				fstate = "On" if resp["fstate"] == 1 else "Off"
			ret.append(str(fstate))
				
		except:
			ret = None
			log.error()
			pass
		finally:
			return ret