#!/usr/bin/env python

import os
import re
import rstat
import time
import sys
from rstat import log
from googlevoice import Voice
import urllib2
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
import smtplib
import subprocess

def cool(phone, msg):
	try:
		if len(msg.split(" ")) == 2:
			tstat.cool()
		if len(msg.split(" ")) == 3:
			tstat.cool(msg.split(" ")[2])
		if len(msg.split(" ")) == 4:
			tstat.cool(msg.split(" ")[2], msg.split(" ")[3])
		time.sleep(5)
		temp(phone, msg)
	except:
		log.error(phone + " " + msg)
		pass

def copyschedule(src, dest):
	try:
		return str(tstat.copyschedule(src, dest))
	except:
		log.error()
		pass

def fall():
	return spring()

def fan(phone, msg):
	try:
		setting = msg.split(" ")[2] if len(msg.split(" ")) == 3 else ""
		currmode = tstat.fan(setting)
		voice.send_sms(phone, str(currmode))
	except:
		log.error(phone + " " + msg)
		pass

def heat(phone, msg):
	try:
		if len(msg.split(" ")) == 2:
			tstat.heat()
		if len(msg.split(" ")) == 3:
			tstat.heat(msg.split(" ")[2])
		if len(msg.split(" ")) == 4:
			tstat.heat(msg.split(" ")[2], msg.split(" ")[3])
		time.sleep(5)
		temp(phone, msg)
	except:
		log.error(phone + " " + msg)
		pass

def hold(phone, msg):
	try:
		tstat.hold(msg.split(" ")[2])
		time.sleep(1)
		return temp(phone, "loc temp")
	except:
		log.error()
		pass

def login():
	try:
		voice.login('cloud@fellinicg.com', 'Cl0ud9143574211')
		writelog("Logged in...")
	except:
		writelog("Error in login()")
		pass		

def mode(phone, msg):
	try:
		type = msg.split(" ")[2] if len(msg.split(" ")) == 3 else ""
		currmode = tstat.mode(type)
		voice.send_sms(phone, str(currmode))
	except:
		log.error(phone + " " + msg)
		pass

def off(phone, msg):
	ret = "SUCCESS"
	try:
		cmd = "net rpc shutdown -S " + msg.split(" ")[0] + " -U rs%shuTdowNmYpc -t 30 -C 'Remote Shutdown'"
		os.system(cmd)
	except:
		ret = "FAILURE"
		log.error(phone + " " + msg)
		pass
	finally:
		voice.send_sms(phone, ret)

def on(phone, msg):
	ret = "SUCCESS"
	try:
		if msg.split(" ")[0].upper().startswith("IBP"):
			os.system("wakeonlan 00:23:54:d4:9f:ce")
		elif msg.split(" ")[0].upper().startswith("HAB"):
			os.system("wakeonlan BC:5F:F4:45:57:0D")
		else:
			ret = "FAILURE"
	except:
		ret = "FAILURE"
		log.error(phone + " " + msg)
		pass
	finally:
		voice.send_sms(phone, ret)
		
def process(phone, msg):
	if phone.replace("+1", "") in authorized and len(msg.split(" ")) > 1:
		try:
			#globals()[msg.split(" ")[1]] in funcs
			if not (msg.split(" ")[0].lower().startswith("hab") or msg.split(" ")[0].lower().startswith("ibp")):
				tstat.location = thermostat(msg)
			globals()[msg.split(" ")[1]](phone, msg)
		except:
			log.error(phone + " " + msg)
			pass

def reboot(phone, msg):
	ret = "SUCCESS"
	try:
		command = "/usr/bin/sudo /sbin/shutdown -r 1"
		process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
		output = process.communicate()[0]
	except:
		ret = "FAILURE"
		log.error(phone + " " + msg)
		pass
	finally:
		voice.send_sms(phone, output)

def reset(phone, msg):
	try:
		tstat.reset()
	except:
		log.error(phone + " " + msg)
		pass

def restart(phone, msg):
	ret = "SUCCESS"
	try:
		command = "/usr/bin/sudo /bin/systemctl restart rcasa"
		process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
		output = process.communicate()[0]
	except:
		ret = "FAILURE"
		log.error(phone + " " + msg)
		pass
	finally:
		voice.send_sms(phone, output)
	
def snapshot(phone, msg):
	try:
		bcc = [""]
		eml = MIMEMultipart()
		if phone.replace("+1", "") == "6462343309":
			eml['To'] = "ksfellini@gmail.com"
			bcc = ["tmfellini@felliniconsulting.com"]
		else:
			eml['To'] = "tmfellini@felliniconsulting.com"
		eml["Subject"] = "Snapshot"

		part = MIMEText(msg)
		eml.attach(part)

		snapshot = "/home/fcg/snapshot.jpg"
		command = "avconv -rtsp_transport tcp -i rtsp://192.168.1.10:554/11 -f image2 -vframes 1 -pix_fmt yuvj420p " + snapshot
		process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
		process.wait()
		
		part = MIMEApplication(open(snapshot,"rb").read())
		part.add_header("Content-Disposition", "attachment", filename="snapshot.jpg")
		eml.attach(part)

		server = smtplib.SMTP("smtp.gmail.com:587")
		server.ehlo()
		server.starttls()
		server.login("cloud@fellinicg.com", "Cl0ud9143574211")
		server.sendmail("cloud@fellinicg.com", [eml['To']] + bcc, eml.as_string())
		server.quit()
		img = ""
		part = ""
		eml = ""
		
		voice.send_sms(phone, "Email sent")
	except:
		log.error(phone + " " + msg)
		pass

def spring():
	try:
		if tname == "livingroom" or tname == "familyroom":
			#ret = tstat.setschedule("0600,68,0900,68,1700,68,2100,62", "mon,tue,wed,thur,fri,sat,sun")
			ret = tstat.setschedule("0500,68,0800,66,1800,66,2100,62", "mon,tue,wed,thur,fri,sat,sun")
		elif tname == "upstairs":
			#ret = tstat.setschedule("0600,68,0900,68,1700,68,2100,66", "mon,tue,wed,thur,fri,sat,sun")
			ret = tstat.setschedule("0500,68,0730,62,2000,66,2100,62", "mon,tue,wed,thur,fri,sat,sun")
	except:
		log.error()
		pass
	finally:
		return str(ret)

def summer():
	try:
		ret = tstat.setschedule("0500,50,0800,50,1800,50,2100,50", "mon,tue,wed,thur,fri,sat,sun")
	except:
		log.error()
		pass
	finally:
		return str(ret)

def temp(phone, msg):
	temp = msg.split(" ")[2] if len(msg.split(" ")) >= 3 else ""
	hold = msg.split(" ")[3] if len(msg.split(" ")) == 4 else "hold" if tstat.hold() == 1 else ""
	try:
		resp = None
		if temp != "":
			mode = tstat.mode()
			if mode.lower() == "cool":
				tstat.cool(temp, hold)
			elif mode.lower() == "heat":
				tstat.heat(temp, hold)
		ret = tstat.temp()
		if ret:
			resp = "On " + str(tstat.gettime()) 
			resp += "\n" + str(tstat.getname()) + " is " + str(ret[0]) + " deg"
			resp += "\nTarget temperature is " + str(ret[1]) if len(ret) > 1 else ""
			resp += "\nThe fan is " + str(ret[5])
			resp += "\nHold is " + str(ret[2])
		voice.send_sms(phone, resp)
	except:
		log.error(phone + " " + msg)
		pass
	
def thermostat(msg):
	therm = {"u":"upstairs","up":"upstairs","upstairs":"upstairs","f":"familyroom","fr":"familyroom","fam":"familyroom","familyroom":"familyroom","l":"livingroom","lr":"livingroom","liv":"livingroom","livingroom":"livingroom","c":"closet","supp":"closet","closet":"closet"}
	ret = None
	try:
		ret = therm[msg.split(" ")[0].lower()]
	except:
		pass
	finally:
		return ret

def writelog(txt):
	file = "rcasa.log"
	log.write(txt, file)
	
authorized = ['9145198939','9142660266','6462343309']
tstat = rstat.thermostat()

writelog("Service starting...")
voice = Voice()
login()

while 1:
	try:
		voice.sms()
		if voice.sms.data['unreadCounts']['sms'] > 0:
			for message in voice.sms().messages:
				if not message.isRead:
					message.delete()
					message.mark(1)
					texts = re.findall(re.escape(message.phoneNumber) + r'\:\s*\<\/span\>\s*\<span\s+class="gc\-message\-sms\-text"\>(.*?)\<\/span\>', voice.sms.html, re.S)
					for txt in texts:
						writelog(txt + " from " + str(message.phoneNumber))
						process(message.phoneNumber, txt)
	except:
		log.error()
		time.sleep(5)
		login()
		pass
