__author__ = 'Stefan Honeder'
__version__ = '1.0'

# ############################################################################
# Copyright (c) 2017 Stefan Honeder
# All rights reserved.
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
#
# TECHNICAL ASSISTANCE CENTER (TAC) SUPPORT IS NOT AVAILABLE FOR THIS SCRIPT.
# ############################################################################

from os.path import dirname
import sys
import traceback
import argparse
import logging
sys.path.extend(".")
sys.path.extend(dirname(__file__))

from GenericDeviceReport import GenericReport



def main():

    #let's parse the arguments that we want to have
    parser = argparse.ArgumentParser(description='Receive Inventory from Prime including Devicename and Serialnumbers')

    parser.add_argument(dest = "hostname", help="Prime Server IP Address or DNS Name")
    parser.add_argument("-d", "--directory",dest ="directory",default="reports/", help="Directory to store retrieved data in")
    parser.add_argument("-u", "--username",dest ="username", help="API User name", required=True)
    parser.add_argument("-p", "--password",dest = "password", help="API Password", required=True)
    parser.add_argument("-v", "--verbose",dest ="verbose",action="count", help="Increase the amount of logging")

    args = parser.parse_args()

    # increase the logging level for this script if requested
    if args.verbose == 1:
        logging.basicConfig(level=logging.INFO)
    elif args.verbose == 2:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.WARN)

    print ("Start executing...")
    result = False
    try:
        genericReport = GenericReport(args.hostname,args.username,args.password)
        result=genericReport.getDeviceInventory()
        if result:
            print("...finished successfully!")
        else:
            print("...something went wrong!")
    except:
        err = sys.exc_info()[0]
        msg_det = sys.exc_info()[1]
        print("We encountered an Error (%s):%s"%(err,msg_det))
        print("%s"%traceback.format_exc())

if __name__ == "__main__":
    main()



