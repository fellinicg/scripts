#!/usr/bin/env python

import sys
import rstat
from rstat import log
import os

def writelog(txt):
	file = "suppheat.log"
	log.write(txt, file)

try:
	supp = rstat.thermostat('closet')
	if sys.argv[1].lower() == 'off':
		rs = supp.mode('off')
		writelog("Supplemental heat is now " + rs + ".")
		sys.exit()
	fr = rstat.thermostat('familyroom')
	curr = fr.temp()
	if curr[3].lower() == 'heat':
		if float(curr[0]) < 69:
			supp.mode('heat')
			writelog("Familyroom temperture is " + curr[0] + " degrees. Supplemental heat has been turned on.")
		else:
			supp.mode('off')
			writelog("Familyroom temperture is " + curr[0] + " degrees. Supplemental heat has been turned off.")
	else:
		writelog("Heat is currently off in the familyroom.")
	sys.exit()
except:
	log.error()
	sys.exit()
