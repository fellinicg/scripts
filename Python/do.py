#!/usr/bin/env python

import datetime
import inspect
import os
import re
import rstat
import sys
import time

class log:
	@classmethod
	def write(cls, txt = "", file = "log/site.log"):
		f = None
		try:
			if len(txt.strip()):
				file =  (os.getcwd() if len(sys.path[0]) == 0 else sys.path[0]) + "/" + file
				f = open(file, "a")
				f.write(datetime.datetime.now().strftime("%m/%d/%Y %I:%M:%S %p: ") + str(txt) + "\n")
				f.close()
		except:
			if f:
				f.close()
			pass
			
	@classmethod
	def error(cls, txt=None):
		err = "File: " + inspect.trace()[0][1] + ", Function: " + inspect.trace()[0][3] + ", Line: " + str(inspect.trace()[0][2]) + ", Error: " + str(sys.exc_info()[1])
		if txt:
			err += ", Msg: " + txt
		log.write(err, "log/error.log")

def cool(target="", hold=""):
	try:
		tstat.cool(target, hold)
		time.sleep(1)
		return temp()
	except:
		log.error()
		pass

def copyschedule(src, dest):
	try:
		return str(tstat.copyschedule(src, dest))
	except:
		log.error()
		pass

def fall():
	return spring()

def fan(setting=""):
	try:
		return tstat.fan(setting)
	except:
		log.error(setting)
		pass

def gettime():
	try:
		return tstat.gettime()
	except:
		log.error()
		pass

def heat(target="", hold=""):
	try:
		tstat.heat(target, hold)
		time.sleep(1)
		return temp()
	except:
		log.error()
		pass

def hold(state):
	try:
		tstat.hold(state)
		time.sleep(1)
		return temp()
	except:
		log.error()
		pass

def mode(type=""):
	try:
		return tstat.mode(type)
	except:
		log.error(type)
		pass

def name(name=""):
	try:
		if name: tstat.setname(name)
		return tstat.getname()
	except:
		log.error()
		pass

def off(name):
	ret = "SUCCESS"
	try:
		cmd = "net rpc shutdown -S " + name + " -U tfellini%543mainst -t 30 -C 'Remote Shutdown'"
		os.system(cmd)
	except:
		ret = "FAILURE"
		log.error()
		pass
	finally:
		return ret

def on(name):
	ret = "SUCCESS"
	try:
		if name.upper().startswith("IBP"):
			os.system("wakeonlan 00:23:54:d4:9f:ce")
		elif name.upper().startswith("HAB"):
			os.system("wakeonlan BC:5F:F4:45:57:0D")
		else:
			ret = "FAILURE"
	except:
		ret = "FAILURE"
		log.error()
		pass
	finally:
		return ret
		
def reset():
	try:
		tstat.reset()
		time.sleep(1)
		return temp()
	except:
		log.error()
		pass
		
def schedule(action, arg1="", arg2=""):
	ret = ""
	try:
		if action.lower() == "get":
			if arg1 != "":
				ret = tstat.getschedule(arg1)
			else:
				ret = tstat.getschedule()
		elif action.lower() == "set":
			ret = tstat.setschedule(arg1, arg2)
	except:
		log.error()
		pass
	finally:
		return str(ret)

def settime():
	try:
		return tstat.settime()
	except:
		log.error()
		pass

def spring():
	try:
		if tname == "livingroom":
			#ret = tstat.setschedule("0600,68,0900,68,1700,68,2100,62", "mon,tue,wed,thur,fri,sat,sun")
			ret = tstat.setschedule("0600,68,0900,66,1800,66,2100,62", "mon,tue,wed,thur,fri,sat,sun")
		elif tname == "familyroom":
			#ret = tstat.setschedule("0600,68,0900,68,1700,68,2100,62", "mon,tue,wed,thur,fri,sat,sun")
			ret = tstat.setschedule("0500,68,0900,68,1800,68,2100,62", "mon,tue,wed,thur,fri,sat,sun")
		elif tname == "upstairs":
			#ret = tstat.setschedule("0600,68,0900,68,1700,68,2100,66", "mon,tue,wed,thur,fri,sat,sun")
			ret = tstat.setschedule("0500,68,0730,68,1800,68,2100,68", "mon,tue,wed,thur,fri,sat,sun")
	except:
		log.error()
		pass
	finally:
		return str(ret)

def summer():
	try:
		ret = tstat.setschedule("0500,50,0800,50,1800,50,2100,50", "mon,tue,wed,thur,fri,sat,sun")
	except:
		log.error()
		pass
	finally:
		return str(ret)

def temp():
	try:
		return tstat.temp()
	except:
		log.error()
		pass
	
def thermostat(msg):
	therm = {"u":"upstairs","up":"upstairs","upstairs":"upstairs","f":"familyroom","fr":"familyroom","fam":"familyroom","familyroom":"familyroom","l":"livingroom","lr":"livingroom","liv":"livingroom","livingroom":"livingroom","supp":"closet","c":"closet","closet":"closet"}
	try:
		return therm[msg.split(" ")[0].lower()]
	except:
		pass

def winter():
	try:
		if tname == "familyroom":
			ret = tstat.setschedule("0600,70,0800,70,1800,70,2100,62", "mon,tue,wed,thur,fri,sat,sun")
		if tname == "livingroom":
			ret = tstat.setschedule("0600,68,0900,68,1700,68,2000,62", "mon,tue,wed,thur,fri,sat,sun")
		elif tname == "upstairs":
			ret = tstat.setschedule("0500,70,0730,68,1800,70,2100,68", "mon,tue,wed,thur,fri,sat,sun")
	except:
		log.error()
		pass
	finally:
		return str(ret)

		
if __name__ == '__main__':
	if len(sys.argv) == 1: sys.exit()
	tname = thermostat(sys.argv[1])
	tstat = rstat.thermostat(tname)
	if not tstat.isvalid(): sys.exit()
	if len(sys.argv) == 2:
		print tstat.temp()
	else:
		if len(sys.argv) == 3:
			print globals()[sys.argv[2]]()
		elif len(sys.argv) == 4:
			print globals()[sys.argv[2]](sys.argv[3])
		elif len(sys.argv) == 5:
			print globals()[sys.argv[2]](sys.argv[3], sys.argv[4])
		elif len(sys.argv) == 6:
			print globals()[sys.argv[2]](sys.argv[3], sys.argv[4], sys.argv[5])			
