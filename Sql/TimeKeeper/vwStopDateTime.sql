use TimeKeeper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwStopDateTime'))
drop view dbo.vwStopDateTime
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170315
-- ============================================================
create view dbo.vwStopDateTime
as
	select 
		a.EMPID
		, b.FIRSTNAME
		, b.LASTNAME
		, a.StopTime 
	from 
		(
			select EMPID, dateadd(second, datediff(second, getdate(), getutcdate()) * -1, max(EVENT_TIME_UTC)) StopTime 
			from dbo.Events
			where EVENTTYPE = 0 and EMPID <> 0
			group by EMPID, convert(varchar, EVENT_TIME_UTC, 101)
		) a
	inner join dbo.Emp b on a.EMPID = b.ID
go