use TimeKeeper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwStartDateTime'))
drop view dbo.vwStartDateTime
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170315
-- ============================================================
create view dbo.vwStartDateTime
as
	select 
		a.EMPID
		, b.FIRSTNAME
		, b.LASTNAME
		, a.StartTime 
	from 
		(
			select EMPID, dateadd(second, datediff(second, getdate(), getutcdate()) * -1, min(EVENT_TIME_UTC)) StartTime 
			from dbo.Events
			where EVENTTYPE = 0 and EMPID <> 0
			group by EMPID, convert(varchar, EVENT_TIME_UTC, 101)
		) a
	inner join dbo.Emp b on a.EMPID = b.ID

go