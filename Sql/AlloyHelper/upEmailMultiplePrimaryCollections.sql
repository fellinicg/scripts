use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upEmailMultiplePrimaryCollections') and type in (N'P', N'PC'))
drop procedure dbo.upEmailMultiplePrimaryCollections
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170725
-- ============================================================
create procedure dbo.upEmailMultiplePrimaryCollections
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @coldelimeter varchar(10) = char(9)
	declare @profile_name varchar(30) = 'PATCDB32_Mail'
	declare @recipients varchar(max) = 'pgornati@panynj.gov;doforte@panynj.gov;tgreen@panynj.gov'
	declare @subject varchar(max) = 'Servers in Multiple Primary Collections as of ' + convert(varchar, getdate(), 101)
	declare @rpt varchar(max) = '<html><body style="font-family:Courier;white-space: pre">' + char(10) + '<b><u>' + convert(char(15), 'Server') + '</b></u><b><u>' + @coldelimeter + convert(char(100), 'CollectionName') + '&nbsp;</b></u>' + char(10)

	-- Report body
	select @rpt = @rpt + 
		convert(char(15), isnull(b.ServerName, '')) + @coldelimeter + 
		stuff((select ', ' + convert(varchar(max), CollectionName)
			from (
					select 
					d.Name CollectionName
					from AN7.dbo.Object_Relations r 
					inner join AN7.dbo.Computers c on r.Left_Object_ID = c.ID
					inner join AN7.dbo.Documents d on r.Right_Object_ID = d.ID
					inner join (select ID from dbo.vwMultiplePrimarySccmCollections) x on r.Left_Object_ID = x.ID
					where r.Type_ID = '55EEBEDE-6820-4E88-A333-AFF58F23768B'
					and c.ID = b.ID
				) a
			order by a.CollectionName
			for xml path('')),1,1,'') + 
		char(10)
	from (
			select
				c.ID
				, c.ServerName
			from dbo.vwMultiplePrimarySccmCollections c
		)  b
	order by b.ServerName

	-- Close HTML
	set @rpt = @rpt + '</body></html>'

	-- Send email
	exec msdb.dbo.sp_send_dbmail @profile_name = @profile_name, @recipients = @recipients, @subject = @subject, @body = @rpt, @body_format = 'HTML', @importance = 'High'

	-- Output for log
	print @recipients
	print @rpt

end
