use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateSANEnclosures') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateSANEnclosures
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170926
-- ============================================================
create procedure dbo.upUpdateSANEnclosures
as
begin

	-- Initialize settings
	set nocount on

	-- Update data in AN7 (Alloy)
	update 
		AN7.dbo.Hardware
	set 
		Description = f.type
		, Status_ID = case f.status when 'online' then '0502DC22-19BA-4452-920E-4E33BC176F36' when 'offline' then '070FC5FD-3138-4A18-917B-7FEB1AD2E68E' else '75E3D693-D12F-4611-A992-78E941C1BC31' end
		, UDF_EnclosureID = f.id
		, UDF_DriveSlots = f.drive_slots 
		, UDF_PSUs = f.total_PSUs + ' (' + f.online_PSUs + ')' 
		, UDF_FirmwareLevel = f.firmware_level_1 + ' - ' + f.firmware_level_2
		, UDF_DataRate = f.interface_speed
	from 
		openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from SanEnclosures.csv') f
	inner join 
		dbo.vwSANEnclosures e on f.SAN = e.San and f.id = e.UDF_EnclosureID
	inner join 
		AN7.dbo.Hardware h on e.EnclosureID = h.ID 

end