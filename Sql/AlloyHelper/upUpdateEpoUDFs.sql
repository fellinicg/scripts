use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateEpoUDFs') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateEpoUDFs
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170808
-- ============================================================
create procedure dbo.upUpdateEpoUDFs
as
begin

	-- Initialize settings
	set nocount on

	-- Update data in AN7 (Alloy)
	update AN7.dbo.Computers set 
		UDF_InEPO = case when e.DeviceName is not null then 'X' else null end
		, UDF_EPOLastUpdate = e.LastUpdate
	from AN7.dbo.Computers c
	left join Omni.dbo.vwEpoAll e on c.Name = e.DeviceName and c.Primary_IP_Address = e.IPAddress

end
go