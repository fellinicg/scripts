use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upRemoveSANConnections') and type in (N'P', N'PC'))
drop procedure dbo.upRemoveSANConnections
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170929
-- ============================================================
create procedure dbo.upRemoveSANConnections
as
begin

	-- Initialize settings
	set nocount on

	-- Remove relations
	delete from AN7.dbo.Object_Relations
	where ID in (
					select 
						r.ID
					from AN7.dbo.Object_Relations r 
						inner join AN7.dbo.Computers c on r.Right_Object_ID = c.ID 
						inner join dbo.vwSANs s on r.Left_Object_ID = s.SanID
						left join openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from SanHosts.csv') f on s.San = f.SAN and (f.Host = c.Computer_Name or f.Host = c.Name)
					where 
						r.Type_ID = 'D0EDA051-AFE3-4BF4-B504-69ED81C7A3A5'	-- Connected (Type)
						and
						f.SAN is null
					union
					select 
						r.ID
					from AN7.dbo.Object_Relations r 
						inner join AN7.dbo.Hardware h on r.Right_Object_ID = h.ID 
						inner join dbo.vwSANs s on r.Left_Object_ID = s.SanID
						left join openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from SanHosts.csv') f on s.San = f.SAN and (f.Host = h.Name)
					where 
						r.Type_ID = 'D0EDA051-AFE3-4BF4-B504-69ED81C7A3A5'	-- Connected (Type)
						and
						f.SAN is null
				)

	-- Output for log
	print convert(varchar, @@rowcount) + ' removed.'

end
