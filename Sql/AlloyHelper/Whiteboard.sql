-- ******************** Alloy-Prime ***********************
select distinct
	p.*
	, n.*
from 
	openrowset('Microsoft.ACE.OLEDB.12.0',	'Excel 12.0;Database=C:\Feeds\AlloyNavigator.xlsx', 'select * from [DataFromAlloyNavigator$]') n
left join (
	select 
		DeviceName
		, Location
		, case when charindex('/', interfaceipAddress) > 0 then left(interfaceipAddress, charindex('/', interfaceipAddress) - 1) else interfaceipAddress end 'InterfaceIP'
		, name 'Interface'
	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds\', 'select * from [Prime-IpInterfaces.csv]')) p on n.[IP Address] = p.InterfaceIP
where 
	n.[IP Address] is not null
order by
	n.type


insert into tbAlloyServers (AlloyID, ServerName, AlloyDesc, AlloySystemName, AlloySystemType)
select 
	ID
	,Computer_Name
	,Description
	,UDF_SystemName
	,UDF_SystemType
from 
	AN7.dbo.Computer_List_Active
left join dbo.tbAlloyServers on ID = AlloyID
where 
	type = 'server'
	and
	AlloyID is null
	
	
select 
	--c.ID
	replace(replace(c.Computer_Name, '.panynj.gov', ''), '.pacorp', '') Computer_Name
	, isnull(replace(replace(c.Description, char(10), ''), char(13), ''), '') Description
	, isnull(c.Status, '') Status
	, isnull(c.Owner, '') Owner
	, isnull(c.Organization, '') Organization
	, isnull(c.UDF_Department, '') Department
	, isnull(p.Business_Phone, '') Business_Phone
	, isnull(p.Mobile_Phone, '') Mobile_Phone
	, isnull(c.UDF_PMRY_Support, '') PomeroySupportLevel
	, isnull(c.UDF_ICS, '') IsICS
	--, c.*
from AN7.dbo.Computer_List c
	left join AN7.dbo.Person_List p on c.Owner_ID = p.ID
where 
	c.Type = 'Server'
	and 
	c.Status not in ('Retired', 'To Be Retired', 'In Stock')
order by c.Owner, c.Status



select
	c.ID
	, c.Computer_Name
	, a.Name 'Collection'
	, a.Keywords
from dbo.vwSccmCollections_Active a
	inner join AN7.dbo.Object_Relations r on a.ID = r.Right_Object_ID
	inner join AN7.dbo.Computers c on r.Left_Object_ID = c.ID
where r.Type_ID = '55EEBEDE-6820-4E88-A333-AFF58F23768B'
order by a.Name, c.Computer_Name


select
	s.CollectionID
	, s.Name
	, s.TotalClients
	, a.TotalServers
from (select distinct CollectionID, Name, TotalClients from dbo.vwSccmServerCompliance) s
inner join (select CollectionID , count(1) 'TotalServers' from dbo.vwServerCollections group by CollectionID) a on s.CollectionID = a.CollectionID
where s.TotalClients <> a.TotalServers

--select *
--	from AN7.dbo.Object_Relations r 
--		inner join AN7.dbo.Documents d on r.Right_Object_ID = d.ID 
--		inner join AN7.dbo.Computers c on r.Left_Object_ID = c.ID
--		left join teledv10.CM_PR1.dbo.CollectionMembers m on d.Keywords = m.SiteID and c.Name = m.Name
--	where 
--		r.Type_ID = '55EEBEDE-6820-4E88-A333-AFF58F23768B'	-- References (Type)
--		and
--		d.Type_ID = '045B9EBD-780A-4C42-9ECB-2EF31756FA03'	-- SCCM Collection
--		and 
--		m.MachineID is null	-- Machine is no longer in Collection



select 
	m.*
	, c.UDF_SCCMClient
	, c.ID
from teledv10.CM_PR1.dbo.CollectionMembers m
inner join AN7.dbo.Computers c on m.Name = c.Name
where m.SiteID = 'PR10027D'


select 
replace(replace(Computer_Name, '.panynj.gov', ''), '.pacorp', '')
, *
from dbo.vwServerCollections
where CollectionID = 'PR10027D'

select
	--p.EMPLID
	--, p.NAME
	--, p.USERID
	--, a.UserName
	--, p.status
	--, 
	p.*
from dbo.vwPeoplesoft p
	left join dbo.vwActiveDirectory a on p.EMPLID = a.EmployeeID
where p.Status in ('A','L','P','S','W')
	and a.SID is null


	insert into [dbo].[Object_Relations] (Left_Object_ID, Right_Object_ID, Type_ID)
	select 
		h.ID
		, 'ABF35B51-60DD-498E-9ADD-6A47AE954E0D' --SAN
		, '6FCF5FE4-0DAC-41AE-93B7-3D0388ADE8CA'
		--h.*
	from AN7.dbo.Hardware h
		left join AN7.dbo.Object_Relations r on h.ID = r.Left_Object_ID
		left join AN7.dbo.Hardware s on r.Right_Object_ID = s.ID
	where 
		h.Type_ID = '828CAC0E-AF8B-45EC-A13A-BA14EB4EEADD'		-- SAN
		and
		h.Category_ID = '3438DAC0-DB1A-4BD9-B24C-19177D07631D'	-- Enclosure
		and 
		s.ID is null
		and
		h.name like 'TELELLV7K%'
	order by h.Name