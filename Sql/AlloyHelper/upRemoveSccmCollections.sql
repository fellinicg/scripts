use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upRemoveSccmCollections') and type in (N'P', N'PC'))
drop procedure dbo.upRemoveSccmCollections
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170726
-- ============================================================
create procedure dbo.upRemoveSccmCollections
as
begin

	-- Initialize settings
	set nocount on

	-- Set status to Retired for any collection that the Primary designation was removed in SCCM
	update AN7.dbo.Documents set Status_ID = '56F6B084-B001-4697-BD98-27F5478E9628'
	from AN7.dbo.Documents d
	inner join teledv10.CM_PR1.dbo.v_Collections c on c.SiteID = d.Keywords and d.Type_ID = '045B9EBD-780A-4C42-9ECB-2EF31756FA03'
	where 
		right(c.CollectionName, 1) <> '#' and right(c.CollectionName, 1) <> '*'
		and
		d.Status_ID <> '56F6B084-B001-4697-BD98-27F5478E9628'

	-- Output for log
	print convert(varchar, @@rowcount) + ' retired.'

end
