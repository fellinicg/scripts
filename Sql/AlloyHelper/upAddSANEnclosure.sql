use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddSANEnclosure') and type in (N'P', N'PC'))
drop procedure dbo.upAddSANEnclosure
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170926
-- ============================================================
create procedure dbo.upAddSANEnclosure
	@Name varchar(100)
	, @Description varchar(max)
	, @Type_ID uniqueidentifier
	, @Category_ID uniqueidentifier
	, @Status_ID uniqueidentifier
	, @Serial_Num varchar(100)
	, @UDF_EnclosureID varchar(10)
	, @UDF_DriveSlots int
	, @UDF_PSUs varchar(5)
	, @UDF_FirmwareLevel varchar(50)
	, @SAN_ID uniqueidentifier
	, @Result char(1) output
	, @ID uniqueidentifier output
as
begin

	-- Initialize settings
	set nocount on
	set @Result = 'F'

	-- Declarations
	declare @NewID uniqueidentifier = newid()
	declare @NextID int = null
	declare @OID varchar(10)

	-- Begin transaction
	begin tran

	-- Try (error trapping)
	begin try
		-- Check @NewID does not already exists
		if not exists(select 1 from AN7.dbo.Object_Index where ID = @NewID)
		begin
			-- Get next ID for OID
			exec AN7.dbo.anit_GetID N'Hardware', @NextID output
		
			-- Format properly
			set @OID = 'H' + format(@NextID,'00000#')

			-- Insert into Hardware table
			insert into AN7.dbo.Hardware (
				ID
				, OID
				, Name
				, Description
				, Type_ID
				, Category_ID
				, Status_ID
				, Serial_Num
				, UDF_EnclosureID
				, UDF_DriveSlots
				, UDF_PSUs
				, UDF_FirmwareLevel) 
			values (
				@NewID
				, @OID
				, @Name
				, @Description
				, @Type_ID
				, @Category_ID
				, @Status_ID
				, @Serial_Num
				, @UDF_EnclosureID
				, @UDF_DriveSlots
				, @UDF_PSUs
				, @UDF_FirmwareLevel)

			-- Verify success
			if (@@rowcount = 1)
			begin
				-- Relate Enclosure to SAN
				insert into AN7.dbo.Object_Relations (Left_Object_ID, Right_Object_ID, Type_ID)
				select @NewID, @SAN_ID, '6FCF5FE4-0DAC-41AE-93B7-3D0388ADE8CA'

				-- Verify success
				if (@@rowcount = 1)
				begin
					-- Set output parameter
					set @Result = 'S'
					set @ID = @NewID

					-- Commit
					commit tran
				end
				else
				begin
					-- Rollback
					rollback tran
					return
				end
			end
			else
			begin
				-- Rollback
				rollback tran
				return
			end
		end
		else
			-- Rollback  
			rollback tran

	end try

	-- Catch
	begin catch
		set @Result = 'F'
		rollback tran
	end catch

end