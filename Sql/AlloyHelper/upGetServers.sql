use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetServers') and type in (N'P', N'PC'))
drop procedure dbo.upGetServers
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170718
-- ============================================================
create procedure dbo.upGetServers
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select
		c.ID
		, c.Name 'Server'
		, case 
			when c.UDF_BackedUp is not null then upper(left(c.UDF_BackedUp, 1))
			else c.UDF_BackedUp
		  end 'BackedUp'
		, s.Status
	from AN7.dbo.Computers c
	inner join AN7.dbo.Status s on c.Status_ID = s.ID
	where c.Type_ID in (
			'629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3' --Server
			)
	order by c.Name

end
go
grant exec on dbo.upGetServers to backup_writer, backup_reader