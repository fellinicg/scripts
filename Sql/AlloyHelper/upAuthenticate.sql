use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAuthenticate') and type in (N'P', N'PC'))
drop procedure dbo.upAuthenticate
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170718
-- ============================================================
create procedure dbo.upAuthenticate
	@Function varchar(10)
	, @Rights char(1) output 
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	if is_member('dbo') = 1 set @Rights = 'W'
	else if @Function = 'backup' 
			set @Rights = 
				case 
					when is_member('backup_writer') = 1 then 'W'
					when is_member('backup_reader') = 1 then 'R'
					else 'X'
				end

	return

end
