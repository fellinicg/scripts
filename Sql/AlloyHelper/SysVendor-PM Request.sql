-- HeavyBid
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'HCSS'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('heavybid', AlloyDesc) > 0

-- VMware
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'VMware'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('vcenter', AlloyDesc) > 0

-- WebEOC
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Intermedix'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('webEOC', AlloyDesc) > 0

-- Trim
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Hewlett-Packard'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('trim', AlloyDesc) > 0
	and 
	charindex('oracle', AlloyDesc) = 0

-- LDRPS
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Sungard'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('ldrps', AlloyDesc) > 0

-- LawManager
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Bridgeway'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('law', AlloyDesc) > 0

-- IndustrySafe
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'IndustrySafe'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('IndustrySafe', AlloyDesc) > 0

-- Clearwell
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Veritas'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('Clearwell', AlloyDesc) > 0

-- eDirectory
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Novell'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('eDirectory', AlloyDesc) > 0

-- FireEye
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'FireEye'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('fireeye', AlloyDesc) > 0

-- OPAL
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'OPAL Manufacturing'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('opal', AlloyDesc) > 0

-- Dell Quest
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Quest Software'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('dell', AlloyDesc) > 0

-- Trend Micro
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Trend Micro'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('trend micro', AlloyDesc) > 0

-- ESRI
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'ESRI'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	(
		charindex('esri', AlloyDesc) > 0
		or
		charindex('GIS', AlloyDesc) > 0
	)

-- Upstream
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Innovation Data Processing'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('Upstream', AlloyDesc) > 0

-- e-Learning
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'NetDimensions'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('e-learning', AlloyDesc) > 0

-- ePO
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'McAfee'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('epo', AlloyDesc) > 0
	and
	charindex('repo', AlloyDesc) = 0

-- Splunk
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Splunk'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('Splunk', AlloyDesc) > 0

-- Schedulesoft
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Workload'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('schedulesoft', AlloyDesc) > 0

-- SAP
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'CMC Americas/SAP'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	(
		charindex('AVM', AlloyDesc) > 0
		or
		charindex('SAP App Gateway', AlloyDesc) > 0
		or
		AlloyDesc = 'SAP'
	)

-- IDMS
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'IntelliSoft'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('IDMS', AlloyDesc) > 0

-- Solarwinds
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Solarwinds'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('Solarwinds', AlloyDesc) > 0

-- PARCS
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Designa'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('parcs', AlloyDesc) > 0

-- Check Point
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Check Point Software Technologies'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('Checkpoint', AlloyDesc) > 0

-- RWIS
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Vaisala'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('RWIS', AlloyDesc) > 0

-- Verint
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Verint'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('Verint', AlloyDesc) > 0

-- Livelink/Opentext
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'OpenText'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	(
		charindex('Livelink', AlloyDesc) > 0
		or 
		charindex('opentext', AlloyDesc) > 0
		or
		charindex('Archive CS', AlloyDesc) > 0
		or
		charindex('ecm', AlloyDesc) > 0
		or
		charindex('CS 10.5', AlloyDesc) > 0
		or
		charindex('Encase', AlloyDesc) > 0
		or
		charindex('DRP ', AlloyDesc) > 0
	)

-- Malwarebytes
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Malwarebytes'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('Malwarebytes', AlloyDesc) > 0

-- Maximo/Tivoli
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'IBM'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	(
		charindex('maximo', AlloyDesc) > 0
		or
		charindex('tivoli', AlloyDesc) > 0
		or
		charindex('omnibus', AlloyDesc) > 0
	)

-- eCw
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'eClinicalWorks'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('ecw', AlloyDesc) > 0

-- Alloy
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Alloy Software'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('Alloy', AlloyDesc) > 0

-- VAMMS
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Trapeze Group'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('VAMMS', AlloyDesc) > 0

-- ENET
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Port Authority - TEC'
from 
	dbo.tbAlloyServers

where 
	ProposedSystemName = 'ENET'

-- TOMS
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Gentrack'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('TOMS', AlloyDesc) > 0

-- Avigilon
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Avigilon'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	(
		charindex('Avigilion', AlloyDesc) > 0
		or
		charindex('Avigilon', AlloyDesc) > 0
	)

-- Genetec
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Genetec'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('Genetec', AlloyDesc) > 0

-- Citrix
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Citrix'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	(
		charindex('Citrix', AlloyDesc) > 0
		or
		charindex('ciitrix', AlloyDesc) > 0
	)

-- Ansaldo
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Ansaldo STS(Hitachi)'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('Ansaldo', AlloyDesc) > 0

-- ESX
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'VMware'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('IS', ServerName) > 0
	and
	charindex('ESX', AlloyDesc) > 0

-- Lenel
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Lenel'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('Lenel', AlloyDesc) > 0

-- ITCS
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Conduent'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	charindex('ITCS', AlloyDesc) > 0

-- Microsoft
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Microsoft'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	(
		charindex('File Cluster', AlloyDesc) > 0
		or
		charindex('ad dc', AlloyDesc) > 0
		or
		charindex('adfs', AlloyDesc) > 0
		or
		charindex('SCCM', AlloyDesc) > 0
		or
		charindex('Direct Access', AlloyDesc) > 0
		or
		charindex('Sharepoint', AlloyDesc) > 0
		or
		charindex('SCOM', AlloyDesc) > 0
		or
		(
			charindex('UAG', AlloyDesc) > 0
			and
			charindex('DRP', AlloyDesc) = 0
		)
	)

-- Oracle
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemVendor = 'Oracle'
from 
	dbo.tbAlloyServers
where 
	ProposedSystemVendor is null
	and
	(
		charindex('Cognos', AlloyDesc) > 0
		or
		charindex('Peoplesoft', AlloyDesc) > 0
		or
		charindex('Primavera', AlloyDesc) > 0
		or
		charindex('budgetpro', AlloyDesc) > 0
		or
		charindex('budget pro', AlloyDesc) > 0
	)