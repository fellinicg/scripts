use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwChangeAuditor'))
drop view dbo.vwChangeAuditor
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20171219
-- ============================================================
create view dbo.vwChangeAuditor
as

	select *
	from openquery([PATCAS92], 'select ServerName, OSVERSION, Deleted, Workstation, Status 
								from 
									PATCAS92.ChangeAuditor.Topology.Server s
								left join 
									(	
										select 
											a.AgentID
											, a.Status
											, b.StatusTime
										from 
											PATCAS92.ChangeAuditor.System.AgentStatus a
										inner join
											(select AgentID, max(StatusTime) StatusTime from PATCAS92.ChangeAuditor.System.AgentStatus group by AgentID) b on a.AgentID = b.AgentID and a.StatusTime = b.StatusTime
									) a on s.ServerID = a.AgentID
								where 
									s.Deleted = 0')
go