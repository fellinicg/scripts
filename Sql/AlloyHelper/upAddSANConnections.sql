use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddSANConnections') and type in (N'P', N'PC'))
drop procedure dbo.upAddSANConnections
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170929
-- ============================================================
create procedure dbo.upAddSANConnections
as
begin

	-- Initialize settings
	set nocount on

	-- Add relations
	insert into AN7.dbo.Object_Relations (
		Left_Object_ID
		, Right_Object_ID
		, Type_ID)
	select 
		s.SanID
		, c.ID
		, 'D0EDA051-AFE3-4BF4-B504-69ED81C7A3A5'	-- Connected (Type)
	from 
		openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from SanHosts.csv') f
	inner join 
		dbo.vwSANs s on f.SAN = s.San
	inner join 
		AN7.dbo.Computer_List c on f.Host = c.Computer_Name or f.Host = c.Name
	left join 
		AN7.dbo.Object_Relations r on s.SanID = r.Left_Object_ID and c.ID = r.Right_Object_ID and 'D0EDA051-AFE3-4BF4-B504-69ED81C7A3A5' = r.Type_ID
	where
		r.ID is null	-- Relation does not exist
	union
	select 
		s.SanID
		, c.ID
		, 'D0EDA051-AFE3-4BF4-B504-69ED81C7A3A5'	-- Connected (Type)
	from 
		openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from SanHosts.csv') f
	inner join 
		dbo.vwSANs s on f.SAN = s.San
	inner join 
		AN7.dbo.Hardware c on f.Host = c.Name
	left join 
		AN7.dbo.Object_Relations r on s.SanID = r.Left_Object_ID and c.ID = r.Right_Object_ID and 'D0EDA051-AFE3-4BF4-B504-69ED81C7A3A5' = r.Type_ID
	where
		r.ID is null	-- Relation does not exist

	-- Output for log
	print convert(varchar, @@rowcount) + ' added.'

end
