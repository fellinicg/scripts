use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwSANEnclosures'))
drop view dbo.vwSANEnclosures
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170810
-- ============================================================
create view dbo.vwSANEnclosures
as
	select 
		h.ID EnclosureID
		, h.Name Enclosure
		, h.Model
		, h.Serial_Num
		, h.UDF_EnclosureID
		, h.Description
		, h.UDF_FirmwareLevel
		, h.UDF_PSUs
		, h.UDF_DriveSlots
		, format(a.Warranty_Exp, 'M/d/yyyy') 'Warranty_Exp'
		, h.Notes
		, s.Name San
		, s.ID SanID
	from AN7.dbo.Hardware h
		left join AN7.dbo.Object_Relations r on h.ID = r.Left_Object_ID
		left join AN7.dbo.Hardware s on r.Right_Object_ID = s.ID
		left join AN7.dbo.Assets a on h.ID = a.Associated_CI_ID
	where 
		h.Type_ID = '828CAC0E-AF8B-45EC-A13A-BA14EB4EEADD'		-- SAN
		and
		h.Category_ID = '3438DAC0-DB1A-4BD9-B24C-19177D07631D'	-- Enclosure
		and
		h.Status_ID <> '5D1D8BAC-C52D-464F-909B-6C2373052F68' -- Not Retired

go
