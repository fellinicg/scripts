use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddSANDrive') and type in (N'P', N'PC'))
drop procedure dbo.upAddSANDrive
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170926
-- ============================================================
create procedure dbo.upAddSANDrive
	@Name varchar(100)
	, @Type_ID uniqueidentifier
	, @Category_ID uniqueidentifier
	, @Status_ID uniqueidentifier
	, @Serial_Num varchar(100)
	, @Product varchar(100)
	, @UDF_EnclosureID varchar(10)
	, @UDF_SlotID varchar(15)
	, @UDF_RPM int
	, @UDF_MdiskID int
	, @UDF_Capacity varchar(10)
	, @UDF_DataRate varchar(10)
	, @UDF_FirmwareLevel varchar(50)
	, @Enclosure_ID uniqueidentifier
	, @Result char(1) output
	, @ID uniqueidentifier output
as
begin

	-- Initialize settings
	set nocount on
	set @Result = 'F'

	-- Declarations
	declare @NewID uniqueidentifier = newid()
	declare @NextID int = null
	declare @OID varchar(10)

	-- Begin transaction
	begin tran

	-- Try (error trapping)
	begin try
		-- Check @NewID does not already exists
		if not exists(select 1 from AN7.dbo.Object_Index where ID = @NewID)
		begin
			-- Get next ID for OID
			exec AN7.dbo.anit_GetID N'Hardware', @NextID output
		
			-- Format properly
			set @OID = 'H' + format(@NextID,'00000#')

			-- Insert into Hardware table
			insert into AN7.dbo.Hardware (
				ID
				, OID
				, Name
				, Type_ID 
				, Category_ID 
				, Status_ID 
				, Serial_Num
				, Product 
				, UDF_EnclosureID 
				, UDF_SlotID 
				, UDF_RPM 
				, UDF_MdiskID 
				, UDF_Capacity
				, UDF_DataRate
				, UDF_FirmwareLevel) 
			values (
				@NewID
				, @OID
				, @Name
				, @Type_ID 
				, @Category_ID 
				, @Status_ID 
				, @Serial_Num
				, @Product 
				, @UDF_EnclosureID 
				, @UDF_SlotID 
				, @UDF_RPM 
				, @UDF_MdiskID 
				, @UDF_Capacity
				, @UDF_DataRate
				, @UDF_FirmwareLevel)

			-- Verify success
			if (@@rowcount = 1)
			begin
				-- Relate Drive to Enclosure
				insert into AN7.dbo.Object_Relations (Left_Object_ID, Right_Object_ID, Type_ID)
				select @NewID, @Enclosure_ID, '6FCF5FE4-0DAC-41AE-93B7-3D0388ADE8CA'

				-- Verify success
				if (@@rowcount = 1)
				begin
					-- Set output parameter
					set @Result = 'S'
					set @ID = @NewID

					-- Commit
					commit tran
				end
				else
				begin
					-- Rollback
					rollback tran
					return
				end
			end
			else
			begin
				-- Rollback
				rollback tran
				return
			end
		end
		else
			-- Rollback  
			rollback tran

	end try

	-- Catch
	begin catch
		set @Result = 'F'
		rollback tran
	end catch

end