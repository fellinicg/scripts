use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddSccmCollections') and type in (N'P', N'PC'))
drop procedure dbo.upAddSccmCollections
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170718
-- ============================================================
create procedure dbo.upAddSccmCollections
--	@Result char(1) = 'S' output
as
begin

	-- Initialize settings
--	set nocount on

	-- Declarations
	declare @TempTbl table (SiteID varchar(10), CollectionName varchar(500), FolderPath varchar(max))
	declare @ID varchar(10)
	declare @Name varchar(500)
	declare @Result char(1)

	-- Insert data into table variable
	insert into @TempTbl
	select 
		c.SiteID
		, c.CollectionName
		, replace(stuff((select convert(varchar(max), ' => ' + f.Name)
				from teledv10.CM_PR1.dbo.FolderMembers fm
				inner join teledv10.CM_PR1.dbo.vSMS_Folders_Flat ff on fm.ContainerNodeID = ff.ContainerNodeID
				inner join teledv10.CM_PR1.dbo.vSMS_Folders f on ff.ParentContainerNodeId = f.ContainerNodeID
				where fm.InstanceKey = c.SiteID
				for xml path('')), 1, 7, ''), '&gt;', '>')  as FolderName
	from teledv10.CM_PR1.dbo.v_Collections c
	left join AN7.dbo.Documents d on c.SiteID = d.Keywords and d.Type_ID = '045B9EBD-780A-4C42-9ECB-2EF31756FA03' --or c.CollectionName = d.Name
	where 
	(right(c.CollectionName, 1) = '#' or right(c.CollectionName, 1) = '*')
	and d.ID is null

	-- Loop over table variable
	while exists (select 1 from @TempTbl)
	begin
		-- Store next available SiteID and CollectionName
		select @ID = SiteID, @Name = CollectionName from @TempTbl

		-- Add SCCM collection to Alloy
		exec dbo.upAddSccmCollectionAsDocument @Name, @ID, @Result output

		-- Output for log
		if @Result = 'S'
			print @Name + ' added.'
		else
			print @Name + 'not added.'

		-- Delete the data added to Alloy from the table variable
		delete from @TempTbl where SiteID = @ID
	end
end
