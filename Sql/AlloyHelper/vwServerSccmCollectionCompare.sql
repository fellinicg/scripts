use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwServerSccmCollectionCompare'))
drop view dbo.vwServerSccmCollectionCompare
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170801
-- ============================================================
create view dbo.vwServerSccmCollectionCompare
as

select 
	replace(replace(c.Computer_Name, '.panynj.gov', ''), '.pacorp', '') Alloy_Computer_Name
	, isnull(s.Collection, '') SCCM_Collection
	, isnull(s.FolderPath, '') SCCM_FolderPath
	, isnull(c.UDF_InSCCM, '') Alloy_InSCCM
	, isnull(c.UDF_PMRY_Support, '') Alloy_Support
	, isnull(c.Status, '') Alloy_Status
from AN7.dbo.Computer_List c
	left join dbo.vwServerCollections s on c.ID = s.ID
where 
	c.UDF_InSCCM = 'X'
	and
	c.Type = 'Server'
	and 
	c.Status not in ('Retired', 'To Be Retired')

go
