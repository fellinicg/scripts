use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwSANDrivesWIthIssues'))
drop view dbo.vwSANDrivesWIthIssues
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20171010
-- ============================================================
create view dbo.vwSANDrivesWIthIssues
as

	select
		San
		, Enclosure_ID
		, Slot_ID
		, Serial_Num
		, Capacity
		, DataRate
		, RPM
		, UDF_FirmwareLevel Firmware
		, convert(varchar, Warranty_Exp, 101) WarrantyExpiration
		, Status
		, [Use]
	from 
		dbo.vwSANDrives
	where 
		Status in ('Offline', 'Degraded')
		and
		[Use] is not null

go