use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwSccmServerCompliance'))
drop view dbo.vwSccmServerCompliance
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170802
-- ============================================================
create view dbo.vwSccmServerCompliance
as

select CollectionID, Title, Name, TotalClients, Installed_NotApplicable, Required, Unknown, PercentSuccess, PercentFailure
from openquery(
	TELEDV10,
	'select
		c.CollectionID
		, l.Title
		, c.Name
		, count(1) TotalClients
		, sum(case when u.status in (1, 3) then 1 else 0 end) Installed_NotApplicable
		, sum(case when u.status = 2 then 1 else 0 end) Required
		, sum(case when u.status = 0 then 1 else 0 end) Unknown
		, round((cast(sum(case when u.status in (1, 3) then 1 else 0 end) as float) / count(1)) * 100, 2) PercentSuccess
		, round((cast(count(case when u.status not in (1, 3) then ''*'' end) as float)/count(1)) * 100, 2) as PercentFailure
	from CM_PR1.dbo.v_Update_ComplianceStatusAll u
	inner join CM_PR1.dbo.v_R_System s on u.ResourceID = s.ResourceID
	inner join CM_PR1.dbo.v_FullCollectionMembership f on u.ResourceID = f.ResourceID
	inner join CM_PR1.dbo.v_Collection c on c.CollectionID = f.CollectionID
	inner join CM_PR1.dbo.v_AuthListInfo l on u.CI_ID = l.CI_ID
	where right(c.Name, 1) in (''*'', ''#'')
	group by l.Title, c.Name, c.CollectionID
	order by l.Title, c.Name'
)

go
