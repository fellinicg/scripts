use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddSccmCollectionRelations') and type in (N'P', N'PC'))
drop procedure dbo.upAddSccmCollectionRelations
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170718
-- ============================================================
create procedure dbo.upAddSccmCollectionRelations
as
begin

	-- Initialize settings
	set nocount on

	-- Add relations
	insert into AN7.dbo.Object_Relations (
		Left_Object_ID
		, Right_Object_ID
		, Type_ID)
	select
		c.ID										-- Computer
		, d.ID										-- Document
		, '55EEBEDE-6820-4E88-A333-AFF58F23768B'	-- References (Type)
	from AN7.dbo.Documents d
		inner join teledv10.CM_PR1.dbo.CollectionMembers m on d.Keywords = m.SiteID
		left join AN7.dbo.Computers c on m.name = c.Name
		left join AN7.dbo.Object_Relations r on c.ID = r.Left_Object_ID and d.ID = r.Right_Object_ID and '55EEBEDE-6820-4E88-A333-AFF58F23768B' = r.Type_ID
	where 
		d.Type_ID = '045B9EBD-780A-4C42-9ECB-2EF31756FA03'	-- SCCM Collection
		and
		r.ID is null	-- Relation does not exist
		and
		c.ID is not null	-- Computer exists in Alloy

	-- Output for log
	print convert(varchar, @@rowcount) + ' added.'

end
