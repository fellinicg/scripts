use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateComputerMonitorRelationships') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateComputerMonitorRelationships
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170808
-- ============================================================
create procedure dbo.upUpdateComputerMonitorRelationships
as
begin

	-- Initialize settings
	set nocount on

	-- Update data in AN7 (Alloy)
	insert into AN7.dbo.Object_Relations (
		Left_Object_ID
		, Right_Object_ID
		, Type_ID)
	select 
		c.ID
		, h.ID
		, rt.ID
	from AN7.dbo.Hardware h
	inner join AD7.dbo.Inv_Monitors m on 
		h.Serial_Num = m.Serial_Number
	inner join AN7.dbo.Computers c on 
		m.Node_ID = c.Import_ID
	inner join AN7.dbo.Object_Relation_Type rt on 
		rt.Name = 'Related Peripheral'
	left join AN7.dbo.Object_Relations r on 
		r.Left_Object_ID = c.ID and r.Right_Object_ID = h.ID and r.Type_ID = rt.ID
	where 
		h.Category_ID = 'A11DBC58-5B70-4504-BB4C-0C8908746579' -- Monitor
		and
		r.ID is null
end
go