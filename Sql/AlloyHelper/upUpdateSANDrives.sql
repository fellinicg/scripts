use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateSANDrives') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateSANDrives
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170926
-- ============================================================
create procedure dbo.upUpdateSANDrives
as
begin

	-- Initialize settings
	set nocount on

	-- Update data in AN7 (Alloy)
	update 
		AN7.dbo.Hardware 
	set 
		Status_ID = case f.status when 'online' then '0502DC22-19BA-4452-920E-4E33BC176F36' when 'offline' then '070FC5FD-3138-4A18-917B-7FEB1AD2E68E' when 'degraded' then '4DB4BEA4-86B7-4874-A5C8-5E3AC76FF603' else '75E3D693-D12F-4611-A992-78E941C1BC31' end
		, Serial_Num = f.FRU_identity
		, Product = f.product_id
		, UDF_EnclosureID = f.enclosure_id 
		, UDF_SlotID = f.slot_id
		, UDF_RPM = f.RPM 
		, UDF_MdiskID = f.mdisk_id 
		, UDF_Capacity = f.capacity 
		, UDF_DataRate = f.interface_speed 
		, UDF_FirmwareLevel = f.firmware_level
		, UDF_Use = f.[use]
	from 
		openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from SanDrives.csv where enclosure_id is not null and slot_id is not null') f
	inner join 
		dbo.vwSANDrives d on f.SAN = d.San and f.enclosure_id = d.Enclosure_ID and f.slot_id = d.Slot_ID
	inner join 
		AN7.dbo.Hardware h on d.DriveID = h.ID 
		
end