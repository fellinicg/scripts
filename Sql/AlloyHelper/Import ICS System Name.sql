use AN7

-- Import New Field Values
--
insert into dbo.cfgCustFieldValues (ID, Field_ID, Field_Value, Tags)
select 
	newid()
	, Field_ID
	, UDF_SystemName
	, '' Tags
from (
		select distinct
			'43697819-4DAB-4547-B9E4-C639472753CA' Field_ID
			, a.[New System Name] UDF_SystemName
		from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\ics.xlsx', 'select * from [Sheet1$]') a
		left join dbo.cfgCustFieldValues f on a.[New System Name] = f.Field_Value and f.Field_ID = '43697819-4DAB-4547-B9E4-C639472753CA'
		where f.Field_ID is null and a.[New System Name] is not null
) a

-- Update UDF_SystemName
--
update dbo.Computers set UDF_SystemName = a.[New System Name]
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\ics.xlsx', 'select * from [Sheet1$]') a
left join dbo.Computers c on a.[Computer Name] = c.Name
