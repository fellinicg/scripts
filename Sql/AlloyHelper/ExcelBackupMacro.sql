use AlloyHelper
exec sp_addrolemember @rolename = 'backup_writer', @membername = 'PANYNJ\siniguez'

use AN7
go
create user [PANYNJ\siniguez];
exec sp_addrolemember @rolename = 'db_datareader', @membername = 'PANYNJ\siniguez'
grant update on object::dbo.Computers (UDF_BackedUp) to [PANYNJ\siniguez]
