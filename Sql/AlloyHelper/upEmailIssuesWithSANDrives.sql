use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upEmailIssuesWithSANDrives') and type in (N'P', N'PC'))
drop procedure dbo.upEmailIssuesWithSANDrives
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20171010
-- ============================================================
create procedure dbo.upEmailIssuesWithSANDrives
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @coldelimeter varchar(10) = char(9)
	declare @profile_name varchar(30) = 'PATCDB32_Mail'
	declare @recipients varchar(max) = 'dfitterman@panynj.gov'
	declare @subject varchar(max) = 'Failed or degraded SAN drives as of ' + convert(varchar, getdate(), 101)
	declare @rpt varchar(max) = '<html><body style="font-family:Courier;white-space: pre">'
	+ '<b><u>' + convert(char(15), 'SAN') + '</b></u>'
	+ '<b><u>' + convert(char(6), 'Encl') + '</b></u>'
	+ '<b><u>' + convert(char(6), 'Slot') + '</b></u>'
	+ '<b><u>' + convert(char(25), 'Serial') + '</b></u>'
	+ '<b><u>' + convert(char(10), 'Capacity') + '</b></u>'
	+ '<b><u>' + convert(char(6), 'Rate') + '</b></u>'
	+ '<b><u>' + convert(char(7), 'RPM') + '</b></u>'
	+ '<b><u>' + convert(char(10), 'Firmware') + '</b></u>'
	+ '<b><u>' + convert(char(12), 'Warranty') + '</b></u>'
	+ '<b><u>' + convert(char(10), 'Status') + '</b></u>'
	+ '<b><u>' + convert(char(8), 'Use') + '</b></u>' + char(10)

	-- Report body	-- Report body
	select @rpt = @rpt
		+ convert(char(15), SAN)
		+ convert(char(6), Enclosure_ID)
		+ convert(char(6), Slot_ID)
		+ convert(char(25), Serial_Num)
		+ convert(char(10), Capacity)
		+ convert(char(6), DataRate)
		+ convert(char(7), RPM)
		+ convert(char(10), Firmware)
		+ convert(char(12), WarrantyExpiration)
		+ convert(char(10), Status)
		+ convert(char(8), [Use]) + char(10)
	from dbo.vwSANDrivesWIthIssues
	order by San, Enclosure_ID, Slot_ID

	-- Close HTML
	set @rpt = @rpt + '</body></html>'

	-- Send email
	exec msdb.dbo.sp_send_dbmail @profile_name = @profile_name, @recipients = @recipients, @subject = @subject, @body = @rpt, @body_format = 'HTML', @importance = 'High'

	-- Output for log
	print @recipients
	print @rpt

end
