use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwServerCollections'))
drop view dbo.vwServerCollections
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170801
-- ============================================================
create view dbo.vwServerCollections
as

select
	c.ID
	, replace(replace(c.Computer_Name, '.panynj.gov', ''), '.pacorp', '') Computer_Name
	, a.Name 'Collection'
	, a.Description 'FolderPath'
	, a.Keywords 'CollectionID'
from dbo.vwSccmCollections_Active a
	inner join AN7.dbo.Object_Relations r on a.ID = r.Right_Object_ID
	inner join AN7.dbo.Computers c on r.Left_Object_ID = c.ID
where r.Type_ID = '55EEBEDE-6820-4E88-A333-AFF58F23768B'

go
