use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwSccmServersOutofCompliance'))
drop view dbo.vwSccmServersOutofCompliance
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170802
-- ============================================================
create view dbo.vwSccmServersOutofCompliance
as

select CollectionID, Title, CollectionName, MachineName
from openquery(
	TELEDV10,
	'select 
		c.CollectionID
		, l.Title
		, c.Name CollectionName
		, s.Name0 MachineName
	from CM_PR1.dbo.v_Update_ComplianceStatusAll u
	inner join CM_PR1.dbo.v_R_System s on u.ResourceID = s.ResourceID
	inner join CM_PR1.dbo.v_FullCollectionMembership f on u.ResourceID = f.ResourceID
	inner join CM_PR1.dbo.v_Collection c on c.CollectionID = f.CollectionID
	inner join CM_PR1.dbo.v_AuthListInfo l on u.CI_ID = l.CI_ID
	where right(c.Name, 1) in (''*'', ''#'')
		and u.Status = 2
	order by l.Title, c.Name'
)

go
