use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddSccmCollectionAsDocument') and type in (N'P', N'PC'))
drop procedure dbo.upAddSccmCollectionAsDocument
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170718
-- ============================================================
create procedure dbo.upAddSccmCollectionAsDocument
	@Name varchar(100)
	, @Keywords varchar(10)
	, @Result char(1) output
as
begin

	-- Initialize settings
	set nocount on
	set @Result = 'F'

	-- Declarations
	declare @NewID uniqueidentifier = newid()
	declare @NextID int = null
	declare @OID varchar(10)

	-- Begin transaction
	begin tran

	-- Try (error trapping)
	begin try
		-- Check @NewID does nto already exists
		if not exists(select 1 from AN7.dbo.Object_Index where ID = @NewID)
		begin
			-- Get next ID for OID
			exec AN7.dbo.anit_GetID N'Document', @NextID output
		
			-- Format properly
			set @OID = 'D' + format(@NextID,'00000#')

			-- Insert into Documents table
			insert into AN7.dbo.Documents (
				ID
				, OID
				, Name
				, Type_ID
				, Category_ID
				, Status_ID
				, Title
				, Keywords
				, Version) 
			values (
				@newid
				, @oid
				, @Name
				, '045B9EBD-780A-4C42-9ECB-2EF31756FA03'	-- SCCM Collection
				, '18280B65-1228-4F5F-90AD-86886E95DB7C'	-- Reference
				, 'DDF385BD-D888-4695-8630-7115C3587C8D'	-- Draft
				, @Name
				, @Keywords
				, '1')

			-- Verify success
			if (@@rowcount = 1)
			begin
				-- Set output parameter
				set @Result = 'S'

				-- Commit
				commit tran
			end
			else
			begin
				-- Rollback
				rollback tran
				return
			end
		end
		else
			-- Rollback  
			rollback tran

	end try

	-- Catch
	begin catch
		set @Result = 'F'
		rollback tran
	end catch

end
