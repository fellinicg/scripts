use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddSANDrives') and type in (N'P', N'PC'))
drop procedure dbo.upAddSANDrives
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170926
-- ============================================================
create procedure dbo.upAddSANDrives
as
begin

	-- Declarations
	declare @TempTbl table (Name varchar(100), Type_ID uniqueidentifier, Category_ID uniqueidentifier, Status_ID uniqueidentifier, Serial_Num varchar(100), Product varchar(100), UDF_EnclosureID varchar(10)
							, UDF_SlotID varchar(15), UDF_RPM int, UDF_MdiskID int, UDF_Capacity varchar(10), UDF_DataRate varchar(10), UDF_FirmwareLevel varchar(50), Enclosure_ID uniqueidentifier)
	declare @Name varchar(100)
	declare @Type_ID uniqueidentifier
	declare @Category_ID uniqueidentifier
	declare @Status_ID uniqueidentifier
	declare @Serial_Num varchar(100)
	declare @Product varchar(100)
	declare @UDF_EnclosureID varchar(10)
	declare @UDF_SlotID varchar(15)
	declare @UDF_RPM int
	declare @UDF_MdiskID int
	declare @UDF_Capacity varchar(10)
	declare @UDF_DataRate varchar(10)
	declare @UDF_FirmwareLevel varchar(50)
	declare @Enclosure_ID uniqueidentifier
	declare @Result char(1)
	declare @ID uniqueidentifier

	-- Insert data into table variable
	insert into @TempTbl
	select 
		f.SAN + ' {' + f.enclosure_id + ',' + f.slot_id + '}' 'Name'
		, '828CAC0E-AF8B-45EC-A13A-BA14EB4EEADD' 'Type_ID'
		, '154FD72A-10F4-4723-9B35-57AA908A65D3' 'Category_ID'
		, case f.status when 'online' then '0502DC22-19BA-4452-920E-4E33BC176F36' when 'offline' then '070FC5FD-3138-4A18-917B-7FEB1AD2E68E' else '75E3D693-D12F-4611-A992-78E941C1BC31' end 'Status_ID'
		, f.FRU_identity 'Serial_Num'
		, f.product_id 'Product'
		, f.enclosure_id 'UDF_EnclosureID'
		, f.slot_id 'UDF_SlotID'
		, f.RPM 'UDF_RPM'
		, f.mdisk_id 'UDF_MdiskID'
		, f.capacity 'UDF_Capacity'
		, f.interface_speed 'UDF_DataRate'
		, f.firmware_level 'UDF_FirmwareLevel'
		, d.EnclosureID 'Enclosure_ID'
	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from SanDrives.csv where enclosure_id is not null and slot_id is not null') f
	left join dbo.vwSANDrives d on f.SAN = d.San and f.enclosure_id = d.Enclosure_ID and f.slot_id = d.Slot_ID
	where d.DriveID is null

	-- Loop over table variable
	while exists (select 1 from @TempTbl)
	begin
		-- Store next row
		select 
			@Name = Name
			, @Type_ID = Type_ID
			, @Category_ID = Category_ID
			, @Status_ID = Status_ID
			, @Serial_Num = Serial_Num
			, @Product = Product 
			, @UDF_EnclosureID = UDF_EnclosureID
			, @UDF_SlotID = UDF_SlotID
			, @UDF_RPM = UDF_RPM
			, @UDF_MdiskID = UDF_MdiskID
			, @UDF_Capacity = UDF_Capacity
			, @UDF_DataRate = UDF_DataRate
			, @UDF_FirmwareLevel = UDF_FirmwareLevel
			, @Enclosure_ID = Enclosure_ID
		from @TempTbl

		-- Add drive to Alloy
		exec dbo.upAddSANDrive @Name, @Type_ID, @Category_ID, @Status_ID, @Serial_Num, @Product, @UDF_EnclosureID, @UDF_SlotID, @UDF_RPM, @UDF_MdiskID, @UDF_Capacity, @UDF_DataRate, @UDF_FirmwareLevel, @Enclosure_ID, @Result output, @ID output

		-- Output for log
		if @Result = 'S'
		begin
			print @Name + ' added.'
			-- Add Asset
			exec dbo.upAddAsset @Name, @Product, @ID, @Result output
		end
		else
			print @Name + 'not added.'

		-- Delete the data added to Alloy from the table variable
		delete from @TempTbl where Serial_Num = @Serial_Num
	end
end
