use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateBackedUp') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateBackedUp
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170718
-- ============================================================
create procedure dbo.upUpdateBackedUp
	@ID uniqueidentifier
	, @Value char (1)
as
begin

	-- Initialize settings
	set nocount on

	-- Update data in AN7 (Alloy)
	update AN7.dbo.Computers
	set UDF_BackedUp = case upper(@Value) when 'Y' then 'Yes' when 'N' then 'No' else null end
	where ID = @ID

end
go
grant exec on dbo.upUpdateBackedUp to backup_writer