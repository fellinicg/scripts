use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upEmailConnectedToSANNotInAlloy') and type in (N'P', N'PC'))
drop procedure dbo.upEmailConnectedToSANNotInAlloy
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170929
-- ============================================================
create procedure dbo.upEmailConnectedToSANNotInAlloy
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @coldelimeter varchar(10) = char(9)
	declare @profile_name varchar(30) = 'PATCDB32_Mail'
	declare @recipients varchar(max) = 'eradoncic@panynj.gov;mgrandowicz@panynj.gov;acamaione@panynj.gov'
	declare @subject varchar(max) = 'Servers connected to a SAN, Missing from Alloy as of ' + convert(varchar, getdate(), 101)
	declare @rpt varchar(max) = '<html><body style="font-family:Courier;white-space: pre"><b><u>' + convert(char(15), 'Server') + '</b></u>' + char(10)

	-- Report body
	select @rpt = @rpt + convert(char(15), Host) + char(10)
	from (
			select distinct
				f.Host
			from 
				openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from SanHosts.csv') f
			inner join 
				dbo.vwSANs s on f.SAN = s.San
			left join 
				AN7.dbo.Computers c on f.Host = c.Computer_Name or f.Host = c.Name
			left join 
				AN7.dbo.Hardware h on f.Host = h.Name
			where
				c.ID is null	-- Computer does not exist
				and
				h.ID is null	-- Hardware does not exist			
	) a
	order by Host

	-- Close HTML
	set @rpt = @rpt + '</body></html>'

	-- Send email
	exec msdb.dbo.sp_send_dbmail @profile_name = @profile_name, @recipients = @recipients, @subject = @subject, @body = @rpt, @body_format = 'HTML', @importance = 'High'

	-- Output for log
	print @recipients
	print @rpt

end
