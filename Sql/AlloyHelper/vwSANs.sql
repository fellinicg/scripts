use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwSANs'))
drop view dbo.vwSANs
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170810
-- ============================================================
create view dbo.vwSANs
as
	select 
		ID SanID
		, Name San
	from AN7.dbo.Hardware 
	where 
		Type_ID = '828CAC0E-AF8B-45EC-A13A-BA14EB4EEADD'	-- SAN
		and
		Category_ID is null	
		and
		Status_ID <> '5D1D8BAC-C52D-464F-909B-6C2373052F68' -- Not Retired
go
