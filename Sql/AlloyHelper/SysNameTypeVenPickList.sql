-- UDF_SystemName
--
insert into AN7.dbo.cfgCustFieldValues (ID, Field_ID, Field_Value, Tags)
select 
	newid()
	, Field_ID
	, UDF_SystemName
	, '' Tags
from (
		select
			distinct
			'43697819-4DAB-4547-B9E4-C639472753CA' Field_ID
			, c.UDF_SystemName
		from 
			AN7.dbo.Computers c
		left join
			AN7.dbo.cfgCustFieldValues v on '43697819-4DAB-4547-B9E4-C639472753CA' = v.Field_ID and c.UDF_SystemName =  v.Field_Value
		where
			c.UDF_SystemName is not null
			and
			v.ID is null
) a

-- UDF_SystemType
--
insert into AN7.dbo.cfgCustFieldValues (ID, Field_ID, Field_Value, Tags)
select 
	newid()
	, Field_ID
	, UDF_SystemType
	, '' Tags
from (
		select
			distinct
			'60120182-47E6-4656-91A9-F15BBE2C0E53' Field_ID
			, c.UDF_SystemType
		from 
			AN7.dbo.Computers c
		left join
			AN7.dbo.cfgCustFieldValues v on '60120182-47E6-4656-91A9-F15BBE2C0E53' = v.Field_ID and c.UDF_SystemType =  v.Field_Value
		where
			c.UDF_SystemType is not null
			and
			v.ID is null
) a

-- UDF_SystemVendor
--
insert into AN7.dbo.cfgCustFieldValues (ID, Field_ID, Field_Value, Tags)
select 
	newid()
	, Field_ID
	, UDF_SystemVendor
	, '' Tags
from (
		select
			distinct
			'6F2860B6-3416-4180-90E4-5F26F59779BA' Field_ID
			, c.UDF_SystemVendor
		from 
			AN7.dbo.Computers c
		left join
			AN7.dbo.cfgCustFieldValues v on '6F2860B6-3416-4180-90E4-5F26F59779BA' = v.Field_ID and c.UDF_SystemVendor =  v.Field_Value
		where
			c.UDF_SystemVendor is not null
			and
			v.ID is null
) a