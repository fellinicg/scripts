use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwMultiplePrimarySccmCollections'))
drop view dbo.vwMultiplePrimarySccmCollections
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170801
-- ============================================================
create view dbo.vwMultiplePrimarySccmCollections
as

select
	c.ID
	, c.Name ServerName
from dbo.vwSccmCollections_Active a
	inner join AN7.dbo.Object_Relations r on a.ID = r.Right_Object_ID
	inner join AN7.dbo.Computers c on r.Left_Object_ID = c.ID
where r.Type_ID = '55EEBEDE-6820-4E88-A333-AFF58F23768B'
group by c.ID, c.Name
having count(1) > 1

go