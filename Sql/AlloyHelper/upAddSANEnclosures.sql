use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddSANEnclosures') and type in (N'P', N'PC'))
drop procedure dbo.upAddSANEnclosures
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170926
-- ============================================================
create procedure dbo.upAddSANEnclosures
--	@Result char(1) = 'S' output
as
begin

	-- Declarations
	declare @TempTbl table (Name varchar(100), Description varchar(max), Type_ID uniqueidentifier, Category_ID uniqueidentifier, Status_ID uniqueidentifier, 
							Serial_Num varchar(100), UDF_EnclosureID varchar(10), UDF_DriveSlots int, UDF_PSUs varchar(5), UDF_FirmwareLevel varchar(50), SAN_ID uniqueidentifier)
	declare @Name varchar(100)
	declare @Description varchar(max)
	declare @Type_ID uniqueidentifier
	declare @Category_ID uniqueidentifier
	declare @Status_ID uniqueidentifier
	declare @Serial_Num varchar(100)
	declare @UDF_EnclosureID varchar(10)
	declare @UDF_DriveSlots int
	declare @UDF_PSUs varchar(5)
	declare @UDF_FirmwareLevel varchar(50)
	declare @SAN_ID uniqueidentifier
	declare @Result char(1)
	declare @ID uniqueidentifier

	-- Insert data into table variable
	insert into @TempTbl
	select 
		f.SAN + ' {'  + f.id + '}' 'Name'
		, f.type 'Description'
		, '828CAC0E-AF8B-45EC-A13A-BA14EB4EEADD' 'Type_ID'
		, '3438DAC0-DB1A-4BD9-B24C-19177D07631D' 'Category_ID'
		, case f.status when 'online' then '0502DC22-19BA-4452-920E-4E33BC176F36' when 'offline' then '070FC5FD-3138-4A18-917B-7FEB1AD2E68E' else '75E3D693-D12F-4611-A992-78E941C1BC31' end 'Status_ID'
		, f.serial_number
		, f.id 'UDF_EnclosureID'
		, f.drive_slots 'UDF_DriveSlots'
		, f.total_PSUs + ' (' + f.online_PSUs + ')' 'UDF_PSUs'
		, f.firmware_level_1 + ' - ' + f.firmware_level_2 'UDF_FirmwareLevel'
		, e.SanID 'SAN_ID'
	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from SanEnclosures.csv') f
	left join dbo.vwSANEnclosures e on f.SAN = e.San and f.id = e.UDF_EnclosureID
	where e.EnclosureID is null	

	-- Loop over table variable
	while exists (select 1 from @TempTbl)
	begin
		-- Store next row
		select 
			@Name = Name
			, @Description = Description
			, @Type_ID = Type_ID
			, @Category_ID = Category_ID
			, @Status_ID = Status_ID
			, @Serial_Num = Serial_Num
			, @UDF_EnclosureID = UDF_EnclosureID
			, @UDF_DriveSlots = UDF_DriveSlots
			, @UDF_PSUs = UDF_PSUs
			, @UDF_FirmwareLevel = UDF_FirmwareLevel
			, @SAN_ID = SAN_ID
		from @TempTbl

		-- Add enclosure to Alloy
		exec dbo.upAddSANEnclosure @Name, @Description, @Type_ID, @Category_ID, @Status_ID, @Serial_Num, @UDF_EnclosureID, @UDF_DriveSlots, @UDF_PSUs, @UDF_FirmwareLevel, @SAN_ID, @Result output, @ID output

		-- Output for log
		if @Result = 'S'
		begin
			print @Name + ' added.'
			-- Add Asset
			exec dbo.upAddAsset @Name, @Description, @ID, @Result output
		end
		else
			print @Name + 'not added.'

		-- Delete the data added to Alloy from the table variable
		delete from @TempTbl where Serial_Num = @Serial_Num
	end
end
