use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upRemoveSccmCollectionRelations') and type in (N'P', N'PC'))
drop procedure dbo.upRemoveSccmCollectionRelations
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170718
-- ============================================================
create procedure dbo.upRemoveSccmCollectionRelations
as
begin

	-- Initialize settings
	set nocount on

	-- Remove relations
	delete from AN7.dbo.Object_Relations
	from AN7.dbo.Object_Relations r 
		inner join AN7.dbo.Documents d on r.Right_Object_ID = d.ID 
		inner join AN7.dbo.Computers c on r.Left_Object_ID = c.ID
		left join teledv10.CM_PR1.dbo.CollectionMembers m on d.Keywords = m.SiteID and c.Name = m.Name
	where 
		r.Type_ID = '55EEBEDE-6820-4E88-A333-AFF58F23768B'	-- References (Type)
		and
		d.Type_ID = '045B9EBD-780A-4C42-9ECB-2EF31756FA03'	-- SCCM Collection
		and 
		m.MachineID is null	-- Machine is no longer in Collection

	-- Output for log
	print convert(varchar, @@rowcount) + ' removed.'

end
