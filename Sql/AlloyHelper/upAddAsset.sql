use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddAsset') and type in (N'P', N'PC'))
drop procedure dbo.upAddAsset
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170926
-- ============================================================
create procedure dbo.upAddAsset
	@Name varchar(100)
	, @Description varchar(max)
	, @ID uniqueidentifier
	, @Result char(1) output
as
begin

	-- Initialize settings
	set nocount on
	set @Result = 'F'

	-- Declarations
	declare @NewID uniqueidentifier = newid()
	declare @NextID int = null
	declare @OID varchar(25)
	declare @Asset_Tag varchar(80)

	-- Begin transaction
	begin tran

	-- Try (error trapping)
	begin try
		-- Check @NewID does not already exists
		if not exists(select 1 from AN7.dbo.Object_Index where ID = @NewID)
		begin
			-- Get next ID for OID
			exec AN7.dbo.anit_GetID N'Asset_OID', @NextID output
		
			-- Format properly
			set @OID = 'A' + format(@NextID,'00000#')

			-- Get next ID for Asset_Tag
			exec AN7.dbo.anit_GetID N'Asset', @NextID output
		
			-- Format properly
			set @Asset_Tag = 'AT' + format(@NextID,'00000#')

			-- Insert into Assets table
			insert into AN7.dbo.Assets (
				ID
				, OID
				, Type_ID
				, Status_ID
				, Name
				, Description
				, Associated_CI_ID
				, Depreciation_Method_ID
				, Lifespan
				, Asset_Tag
				, Qty) 
			values (
				@NewID
				, @OID
				, 'C95EAF5A-A870-4A68-860A-91D55A9AC68E'
				, 'F61148A7-3A30-4A13-8355-68D2FBE54DE7'
				, @Name
				, @Description
				, @ID
				, '8425D422-FACA-4127-A883-AD5484EEDE15'
				, 3
				, @Asset_Tag
				, 1)

			-- Verify success
			if (@@rowcount = 1)
			begin
				-- Set output parameter
				set @Result = 'S'

				-- Commit
				commit tran
			end
			else
			begin
				-- Rollback
				rollback tran
				return
			end
		end
		else
			-- Rollback  
			rollback tran

	end try

	-- Catch
	begin catch
		set @Result = 'F'
		rollback tran
	end catch

end