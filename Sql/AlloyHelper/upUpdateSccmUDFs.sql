use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateSccmUDFs') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateSccmUDFs
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170808
-- ============================================================
create procedure dbo.upUpdateSccmUDFs
as
begin

	-- Initialize settings
	set nocount on

	-- Update data in AN7 (Alloy)
	update 
		AN7.dbo.Computers
	set 
		UDF_SCCM_UserID = s.User_Name0
		, UDF_SCCM_Time_Stamp = s2.LastDDR
		, UDF_SCCMClient = case when s.Client0 = 1 then 'Yes' else 'No' end
		, UDF_SCCMClientActivity = case when s2.ClientActiveStatus='1' then 'Active' when s2.ClientActiveStatus='0' then 'Inactive' end
		, UDF_SCCMClientVersion = s.Client_Version0
		, UDF_SCCMHeartbeatDDR = s2.LastDDR
		, UDF_InSCCM = case when s.Client0 = 1 then 'X' else null end
	from 
		TELEDV10.[CM_PR1].dbo.v_R_System s
	left join
		TELEDV10.[CM_PR1].dbo.v_CH_ClientSummary s2 on s.ResourceId = s2.ResourceId
	inner join
		AN7.dbo.Computers d on s.Name0 = d.Name
end
go
