-- Match enclosures by serial
select 
	f.[SAN Name]
	, f.[Serial]
	, e.Enclosure
	, e.San
	, e.Serial_Num
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [20170809 - SAN Enclosures.csv]') f
full join dbo.vwSANEnclosures e on replace(f.Serial, 'SN ', '') = e.Serial_Num
order by f.[SAN Name], e.Enclosure

-- Match drives by serial
select 
	f.[SAN]
	, f.[Serial Number]
	, f.[Tray, Slot]
	, d.Drive
	, d.TraySlot
	, d.Serial_Num
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [20170809 - SAN Drives.csv]') f
full join dbo.vwSANDrives d on f.[Serial Number] = d.Serial_Num
order by f.[SAN], f.[Tray, Slot], d.Drive