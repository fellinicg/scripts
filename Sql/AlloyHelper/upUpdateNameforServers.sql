use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateNameforServers') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateNameforServers
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170808
-- ============================================================
create procedure dbo.upUpdateNameforServers
as
begin

	-- Initialize settings
	set nocount on

	-- Update data in AN7 (Alloy)
	update AN7.dbo.Computers
	set Name = replace(replace(Computer_Name, '.panynj.gov', ''), '.pacorp','')
	from AN7.dbo.Computers
	where 
		Name <> Computer_Name
		and 
		Type_ID = '629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3'	-- Server

end
go
