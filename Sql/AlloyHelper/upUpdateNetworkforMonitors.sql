use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateNetworkforMonitors') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateNetworkforMonitors
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170808
-- ============================================================
create procedure dbo.upUpdateNetworkforMonitors
as
begin

	-- Initialize settings
	set nocount on

	-- Update data in AN7 (Alloy)
	update 
		AN7.dbo.Hardware 
	set 
		Network_ID = c.Network_ID
	from AN7.dbo.Hardware h
	inner join AD7.dbo.Inv_Monitors m on 
		h.Serial_Num = m.Serial_Number
	inner join AN7.dbo.Computers c on 
		m.Node_ID = c.Import_ID
	where 
		h.Category_ID = 'A11DBC58-5B70-4504-BB4C-0C8908746579'
end
go