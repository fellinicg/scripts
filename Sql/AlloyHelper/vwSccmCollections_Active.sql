use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwSccmCollections_Active'))
drop view dbo.vwSccmCollections_Active
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170801
-- ============================================================
create view dbo.vwSccmCollections_Active
as

select 
	ID
    , Name
	, Keywords
	, Description
from AN7.dbo.Documents
where 
	Type_ID = '045B9EBD-780A-4C42-9ECB-2EF31756FA03'	-- SCCM Collection
	and 
	Status_ID in (
		'DDF385BD-D888-4695-8630-7115C3587C8D'	-- Draft
		, '03C70C05-3843-4AC7-B633-FB88BDB11918' -- Live
	)

go
