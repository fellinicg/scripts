--select * from dbo.tbAlloyServers where ProposedSystemVendor is null and AlloyDesc is not null order by AlloyDesc

--select distinct ProposedSystemType from dbo.tbAlloyServers where ProposedSystemType is not null order by ProposedSystemType

select 
*
from
	dbo.tbAlloyServers
where 
	--charindex(char(10), AlloyDesc) > 0
	--or
	--charindex(char(13), AlloyDesc) > 0
	charindex('fireeye', AlloyDesc) > 0



--select * from dbo.tbAlloyServers where ProposedSystemName is not null and AlloyDesc is not null order by AlloyDesc



---- SystemType = Database
--update 
--	dbo.tbAlloyServers 
--set 
--	ProposedSystemType = 'Database'
--where 
--	ProposedSystemType is null
--	and
--	(
--		charindex('DV', ServerName) > 0
--		or
--		charindex('DB', ServerName) > 0
--	)

-- ESX
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'HyperVisor'
where 
	--ProposedSystemType is null
	--and
	charindex('IS', ServerName) > 0
	and
	charindex('ESX', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'ESX'
where 
	(
	charindex('IS', ServerName) > 0
	and
	charindex('ESX', AlloyDesc) > 0
	)

-- Livelink
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Enterprise Content Management'
where 
	ProposedSystemType is null
	and
	(
		charindex('Livelink', AlloyDesc) > 0
		or 
		charindex('opentext', AlloyDesc) > 0
		or
		charindex('Archive CS', AlloyDesc) > 0
		or
		charindex('ecm', AlloyDesc) > 0
		or
		charindex('CS 10.5', AlloyDesc) > 0
		or
		charindex('DRP ', AlloyDesc) > 0	)

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'OpenText Content Suite'
where 
	ProposedSystemName is null
	and
	(
		charindex('Livelink', AlloyDesc) > 0
		or 
		charindex('opentext', AlloyDesc) > 0
		or
		charindex('Archive CS', AlloyDesc) > 0
		or
		charindex('ecm', AlloyDesc) > 0
		or
		charindex('CS 10.5', AlloyDesc) > 0
		or
		charindex('DRP ', AlloyDesc) > 0
	)

-- MS File Cluster
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'File Cluster'
where 
	ProposedSystemType is null
	and
	charindex('File Cluster', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'File Server'
where 
	ProposedSystemName is null
	and
	charindex('File Cluster', AlloyDesc) > 0

-- SCCM
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'System Configuration Management'
where 
	ProposedSystemType is null
	and
	charindex('SCCM', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'SCCM'
where 
	ProposedSystemName is null
	and
	charindex('SCCM', AlloyDesc) > 0

-- Malwarebytes
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Endpoint Protection'
where 
	ProposedSystemType is null
	and
	charindex('Malwarebytes', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Malwarebytes'
where 
	ProposedSystemName is null
	and
	charindex('Malwarebytes', AlloyDesc) > 0

-- Maximo
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Enterprise Asset Management'
where 
	ProposedSystemType is null
	and
	charindex('maximo', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Maximo'
where 
	ProposedSystemName is null
	and
	charindex('maximo', AlloyDesc) > 0

-- Alloy
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'IT Asset Management'
where 
	ProposedSystemType is null
	and
	charindex('Alloy', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Alloy'
where 
	ProposedSystemName is null
	and
	charindex('Alloy', AlloyDesc) > 0

-- eCw
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Electronic Medical Records'
where 
	ProposedSystemType is null
	and
	charindex('ecw', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'eClinicalWorks'
where 
	ProposedSystemName is null
	and
	charindex('ecw', AlloyDesc) > 0

-- VAMMS
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Enterprise Asset Management'
where 
	ProposedSystemType is null
	and
	charindex('VAMMS', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Trapeze EAM'
where 
	ProposedSystemName is null
	and
	charindex('VAMMS', AlloyDesc) > 0

-- TOMS
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Airport Management'
where 
	ProposedSystemType is null
	and
	charindex('TOMS', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'TOMS'
where 
	ProposedSystemName is null
	and
	charindex('TOMS', AlloyDesc) > 0

-- XenServer
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'HyperVisor'
where 
	ProposedSystemType is null
	and
	charindex('XenServer', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'XenServer'
where 
	ProposedSystemName is null
	and
	charindex('XenServer', AlloyDesc) > 0

-- XenApp
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Application Virtualization'
where 
	(ProposedSystemType is null or ProposedSystemType = 'Database')
	and
	(
		charindex('ciitrix', AlloyDesc) > 0
		or
		charindex('citrix', AlloyDesc) > 0
		or
		charindex('XenApp', AlloyDesc) > 0
	)

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'XenApp'
where 
	ProposedSystemName is null
	and
	(
		charindex('ciitrix', AlloyDesc) > 0
		or
		charindex('citrix', AlloyDesc) > 0
		or
		charindex('XenApp', AlloyDesc) > 0
	)

-- ENET
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Information Portal'
where 
	ProposedSystemType is null
	and
	charindex('ENET', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'ENET'
where 
	--ProposedSystemName is null
	--and
	(
		charindex('ENET', AlloyDesc) > 0
		and
		charindex('genetec', AlloyDesc) = 0
	)
-- Genetec
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Closed Circuit TV'
where 
	ProposedSystemType is null
	and
	charindex('Genetec', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Security Center'
where 
	charindex('Genetec', AlloyDesc) > 0

-- Bitlocker
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Encryption'
where 
	ProposedSystemType is null
	and
	charindex('bitlock', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Bitlocker'
where 
	charindex('bitlock', AlloyDesc) > 0

-- Avigilon/Avigilion
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Closed Circuit TV'
where 
	ProposedSystemType is null
	and
	(
		charindex('Avigilion', AlloyDesc) > 0
		or
		charindex('Avigilon', AlloyDesc) > 0
	)

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Control Center'
where 
	charindex('Avigilion', AlloyDesc) > 0
	or
	charindex('Avigilon', AlloyDesc) > 0

-- Verint
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Closed Circuit TV'
where 
	ProposedSystemType is null
	and
	charindex('Verint', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Nextiva'
where 
	charindex('Verint', AlloyDesc) > 0

-- Cognos
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Business Intelligence'
where 
	ProposedSystemType is null
	and
	charindex('Cognos', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Cognos'
where 
	charindex('Cognos', AlloyDesc) > 0

-- ADFS
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Federation Services'
where 
	ProposedSystemType is null
	and
	charindex('adfs', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'ADFS'
where 
	charindex('adfs', AlloyDesc) > 0

-- AD DC
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Directory Services'
where 
	ProposedSystemType is null
	and
	charindex('ad dc', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Domain Controller'
where 
	charindex('ad dc', AlloyDesc) > 0

-- Centralized Traffic Control/Automatic Train Supervision
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Traffic Control & Supervision'
where 
	ProposedSystemType is null
	and
	charindex('Ansaldo', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Centralized Traffic Control/Automatic Train Supervision'
where 
	charindex('Ansaldo', AlloyDesc) > 0

-- ATC Snooper
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Automated Train Control'
where 
	ProposedSystemType is null
	and
	charindex('ATC Snooper', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'ATC Snooper'
where 	
	charindex('ATC Snooper', AlloyDesc) > 0

-- DirectAccess
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Remote Access'
where 
	ProposedSystemType is null
	and
	charindex('Direct Access', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'DirectAccess'
where 	
	charindex('Direct Access', AlloyDesc) > 0

-- Lenel
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Access Control'
where 
	ProposedSystemType is null
	and
	charindex('Lenel', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Onguard'
where 	
	charindex('Lenel', AlloyDesc) > 0

-- ITCS
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Integrated Toll Collection'
where 
	ProposedSystemType is null
	and
	charindex('ITCS', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'ITCS'
where 	
	charindex('ITCS', AlloyDesc) > 0

-- SharePoint
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Collaboration'
where 
	ProposedSystemType is null
	and
	charindex('Sharepoint', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'SharePoint'
where 
	ProposedSystemName is null
	and
	charindex('Sharepoint', AlloyDesc) > 0

-- RWIS
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Road Weather Information'
where 
	ProposedSystemType is null
	and
	charindex('RWIS', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'RWIS'
where 
	ProposedSystemName is null
	and
	charindex('RWIS', AlloyDesc) > 0

-- PeopleSoft
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Human Resource Management'
where 
	ProposedSystemType is null
	and
	charindex('PeopleSoft', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'PeopleSoft'
where 
	ProposedSystemName is null
	and
	charindex('PeopleSoft', AlloyDesc) > 0

-- Primavera
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Project Portfolio Management'
where 
	ProposedSystemType is null
	and
	charindex('Primavera', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Primavera'
where 
	ProposedSystemName is null
	and
	charindex('Primavera', AlloyDesc) > 0

-- Check Point
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Security Appliance'
where 
	ProposedSystemType is null
	and
	charindex('Checkpoint', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Firewall'
where 
	ProposedSystemName is null
	and
	charindex('Checkpoint', AlloyDesc) > 0

-- PARCS
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Parking Access Revenue Control'
where 
	ProposedSystemType is null
	and
	charindex('PARCS', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'ABACUS'
where 
	ProposedSystemName is null
	and
	charindex('PARCS', AlloyDesc) > 0

-- SCOM
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Monitoring'
where 
	ProposedSystemType is null
	and
	charindex('SCOM', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'System Center Operations Manager'
where 
	ProposedSystemName is null
	and
	charindex('SCOM', AlloyDesc) > 0

-- Solarwinds
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Monitoring'
where 
	ProposedSystemType is null
	and
	charindex('Solarwinds', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Solarwinds'
where 
	ProposedSystemName is null
	and
	charindex('Solarwinds', AlloyDesc) > 0

-- IDMS
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Identity Management/Credential Issuance'
where 
	ProposedSystemType is null
	and
	charindex('IDMS', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'AirportICE'
where 
	ProposedSystemName is null
	and
	charindex('IDMS', AlloyDesc) > 0

-- SAP App Gateway
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Agency Vendor Management'
where 
	ProposedSystemType is null
	and
	charindex('SAP App Gateway', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'App Gateway'
where 
	ProposedSystemName is null
	and
	charindex('SAP App Gateway', AlloyDesc) > 0

-- Schedulesoft
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Workforce Management'
where 
	ProposedSystemType is null
	and
	charindex('schedulesoft', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'ScheduleSoft'
where 
	ProposedSystemName is null
	and
	charindex('schedulesoft', AlloyDesc) > 0

-- Splunk
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Security Event Information Management'
where 
	ProposedSystemType is null
	and
	charindex('Splunk', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Splunk'
where 
	ProposedSystemName is null
	and
	charindex('Splunk', AlloyDesc) > 0

-- ePO
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Security Management'
where 
	ProposedSystemType is null
	and
	charindex('epo', AlloyDesc) > 0
	and
	charindex('repo', AlloyDesc) = 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'ePolicy Orchestrator'
where 
	ProposedSystemName is null
	and
	charindex('epo', AlloyDesc) > 0
	and
	charindex('repo', AlloyDesc) = 0

-- e-Learning
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Talent Management'
where 
	ProposedSystemType is null
	and
	charindex('e-learning', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Talent Suite'
where 
	ProposedSystemName is null
	and
	charindex('e-learning', AlloyDesc) > 0

-- Upstream
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Data Protection'
where 
	ProposedSystemType is null
	and
	charindex('Upstream', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'FDR/Upstream'
where 
	ProposedSystemName is null
	and
	charindex('Upstream', AlloyDesc) > 0

-- UAG
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Remote Access'
where 
	ProposedSystemType is null
	and
	charindex('UAG', AlloyDesc) > 0
	and
	charindex('DRP', AlloyDesc) = 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Unified Access Gateway'
where 
	ProposedSystemName is null
	and
	charindex('UAG', AlloyDesc) > 0
	and
	charindex('DRP', AlloyDesc) = 0

-- ESRI
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Geographic Information'
where 
	(ProposedSystemType is null or ProposedSystemType = 'Database')
	and
	(
		charindex('esri', AlloyDesc) > 0
		or
		charindex('GIS', AlloyDesc) > 0
	)

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'ArcGIS'
where 
	ProposedSystemName is null
	and
	(
		charindex('esri', AlloyDesc) > 0
		or
		charindex('GIS', AlloyDesc) > 0
	)

-- Trend Micro
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Encryption'
where 
	ProposedSystemType is null
	and
	charindex('trend micro', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Endpoint Encryption'
where 
	ProposedSystemName is null
	and
	charindex('trend micro', AlloyDesc) > 0

-- Quest
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Microsoft Platform Management'
where 
	ProposedSystemType is null
	and
	charindex('dell', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = ltrim(rtrim(replace(replace(replace(replace(replace(replace(replace(replace(AlloyDesc, 'Dell ', ''), 'Quest ', ''), 'TEC', ''), 'primary', ''), 'secondary', ''), 'Mgr', 'Manager'), 'O365 ', ''), 'qmm ' ,'')))
where 
	ProposedSystemName is null
	and
	charindex('dell', AlloyDesc) > 0

-- BudgetPro
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Financial Planning'
where 
	(ProposedSystemType is null or ProposedSystemType = 'Database')
	and
	(
		charindex('budgetpro', AlloyDesc) > 0
		or
		charindex('budget pro', AlloyDesc) > 0
	)

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Hyperion BudgetPro'
where 
	ProposedSystemName is null
	and
	(
		charindex('budgetpro', AlloyDesc) > 0
		or
		charindex('budget pro', AlloyDesc) > 0
	)

-- Encase
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Digital Investigation'
where 
	ProposedSystemType is null
	and
	charindex('encase', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'EnCase Forensic'
where 
	ProposedSystemName is null
	and
	charindex('encase', AlloyDesc) > 0

-- OPAL
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Vending Machine Management'
where 
	ProposedSystemType is null
	and
	charindex('opal', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Easy Vending'
where 
	ProposedSystemName is null
	and
	charindex('opal', AlloyDesc) > 0

-- FireEye
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Security Appliance'
where 
	ProposedSystemType is null
	and
	charindex('fireeye', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'FireEye'
where 
	ProposedSystemName is null
	and
	charindex('fireeye', AlloyDesc) > 0

-- eDirectory
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Directory Services'
where 
	ProposedSystemType is null
	and
	charindex('eDirectory', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'eDirectory'
where 
	ProposedSystemName is null
	and
	charindex('eDirectory', AlloyDesc) > 0

-- Clearwell
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'eDiscovery'
where 
	(ProposedSystemType is null or ProposedSystemType = 'Database')
	and
	charindex('Clearwell', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Clearwell'
where 
	ProposedSystemName is null
	and
	charindex('Clearwell', AlloyDesc) > 0

-- IndustrySafe
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Safety Management'
where 
	(ProposedSystemType is null or ProposedSystemType = 'Database')
	and
	charindex('IndustrySafe', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'IndustrySafe'
where 
	ProposedSystemName is null
	and
	charindex('IndustrySafe', AlloyDesc) > 0

-- LawManager
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Case Management'
where 
	(ProposedSystemType is null or ProposedSystemType = 'Database')
	and
	charindex('Law', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'LawManager'
where 
	ProposedSystemName is null
	and
	charindex('Law', AlloyDesc) > 0

-- LDRPS
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Business Continuity Management'
where 
	(ProposedSystemType is null or ProposedSystemType = 'Database')
	and
	charindex('ldrps', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'LDRPS'
where 
	ProposedSystemName is null
	and
	charindex('ldrps', AlloyDesc) > 0

-- OMNIbus
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Event Management'
where 
	(ProposedSystemType is null or ProposedSystemType = 'Database')
	and
	(
		charindex('tivoli', AlloyDesc) > 0
		or
		charindex('omnibus', AlloyDesc) > 0
	)

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'Tivoli OMNIbus'
where 
	ProposedSystemName is null
	and
	(
		charindex('tivoli', AlloyDesc) > 0
		or
		charindex('omnibus', AlloyDesc) > 0
	)

-- Trim
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Records Management'
where 
	(ProposedSystemType is null or ProposedSystemType = 'Database')
	and
	charindex('trim', AlloyDesc) > 0
	and 
	charindex('oracle', AlloyDesc) = 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'TRIM'
where 
	ProposedSystemName is null
	and
	charindex('trim', AlloyDesc) > 0
	and 
	charindex('oracle', AlloyDesc) = 0

-- WebEOC
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Incident Management'
where 
	(ProposedSystemType is null or ProposedSystemType = 'Database')
	and
	charindex('WebEOC', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'WebEOC'
where 
	ProposedSystemName is null
	and
	charindex('WebEOC', AlloyDesc) > 0

-- VMware
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Server Management'
where 
	(ProposedSystemType is null or ProposedSystemType = 'Database')
	and
	charindex('vcenter', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'vCenter'
where 
	ProposedSystemName is null
	and
	charindex('vcenter', AlloyDesc) > 0

-- HeavyBid
update 
	dbo.tbAlloyServers 
set 
	ProposedSystemType = 'Construction Estimating and Bidding'
where 
	(ProposedSystemType is null or ProposedSystemType = 'Database')
	and
	charindex('HeavyBid', AlloyDesc) > 0

update 
	dbo.tbAlloyServers 
set 
	ProposedSystemName = 'HeavyBid'
where 
	ProposedSystemName is null
	and
	charindex('HeavyBid', AlloyDesc) > 0