use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwSANDrives'))
drop view dbo.vwSANDrives
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170810
-- ============================================================
create view dbo.vwSANDrives
as
	select 
		h.ID DriveID
		, h.Name Drive
		, h.Serial_Num
		, h.Product
		, h.UDF_Capacity Capacity
		, h.UDF_DataRate DataRate
		, h.UDF_EnclosureID Enclosure_ID
		, h.UDF_SlotID Slot_ID
		, h.UDF_Use [Use]
		, h.UDF_RPM RPM
		, st.Status
		, h.UDF_FirmwareLevel
		, a.Warranty_Exp
		, h.Notes
		, e.ID EnclosureID
		, e.Name Enclosure
		, s.ID SanID
		, s.Name San
	from AN7.dbo.Hardware h
		inner join AN7.dbo.Status st on h.Status_ID = st.ID
		left join AN7.dbo.Object_Relations r on h.ID = r.Left_Object_ID
		left join AN7.dbo.Hardware e on r.Right_Object_ID = e.ID
		left join AN7.dbo.Object_Relations sr on e.ID = sr.Left_Object_ID
		left join AN7.dbo.Hardware s on sr.Right_Object_ID = s.ID
		left join AN7.dbo.Assets a on h.ID = a.Associated_CI_ID
	where 
		h.Type_ID = '828CAC0E-AF8B-45EC-A13A-BA14EB4EEADD'		-- SAN
		and
		h.Category_ID = '154FD72A-10F4-4723-9B35-57AA908A65D3'	-- Drive
		and
		h.Status_ID <> '5D1D8BAC-C52D-464F-909B-6C2373052F68' -- Not Retired

go