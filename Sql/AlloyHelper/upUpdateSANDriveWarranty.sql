use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateSANDriveWarranty') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateSANDriveWarranty
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20171005
-- ============================================================
create procedure dbo.upUpdateSANDriveWarranty
as
begin

	-- Initialize settings
	set nocount on

	-- Update data in AN7 (Alloy)
	update 
		a 
	set 
		Warranty_Exp = c.Warranty_Exp
	from 
		AN7.dbo.Assets a
	inner join 
		dbo.vwSANDrives b on a.Associated_CI_ID = b.DriveID
	left join 
		AN7.dbo.Assets c on b.EnclosureID = c.Associated_CI_ID
	where 
		isnull(a.Warranty_Exp, '1/1/1900') <> isnull(c.Warranty_Exp, '1/1/1900')
		
end