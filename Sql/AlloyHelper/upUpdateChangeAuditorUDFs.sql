use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateChangeAuditorUDFs') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateChangeAuditorUDFs
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20171219
-- ============================================================
create procedure dbo.upUpdateChangeAuditorUDFs
as
begin

	-- Initialize settings
	set nocount on

	-- Update data in AN7 (Alloy)
	update AN7.dbo.Computers set 
		UDF_InChangeAuditor = case when s.Workstation is not null then 'X' else null end
	from AN7.dbo.Computers c
	left join dbo.vwChangeAuditor s 
		on c.Name = s.ServerName and s.Status in (2,3)
	where 
		c.Type_ID in (
			'629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3' -- Server
		)

end
go