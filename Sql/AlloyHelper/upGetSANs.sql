use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetSANs') and type in (N'P', N'PC'))
drop procedure dbo.upGetSANs
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170921
-- ============================================================
create procedure dbo.upGetSANs
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		SanID
		, ltrim(rtrim(San)) San
	from AlloyHelper.dbo.vwSANs
	where 
		San like '%V7%' 
		or 
		San like '%V3%'

end
go