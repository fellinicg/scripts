use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upEmailInSccmNotInAlloy') and type in (N'P', N'PC'))
drop procedure dbo.upEmailInSccmNotInAlloy
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170725
-- ============================================================
create procedure dbo.upEmailInSccmNotInAlloy
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @coldelimeter varchar(10) = char(9)
	declare @profile_name varchar(30) = 'PATCDB32_Mail'
	declare @recipients varchar(max) = 'eradoncic@panynj.gov;pgornati@panynj.gov;doforte@panynj.gov'
	declare @subject varchar(max) = 'Servers in SCCM Missing from Alloy as of ' + convert(varchar, getdate(), 101)
	declare @rpt varchar(max) = '<html><body style="font-family:Courier;white-space: pre">' + char(10) + '<b><u>' + convert(char(15), 'Server') + '</b></u><b><u>' + @coldelimeter + convert(char(100), 'CollectionName') + '&nbsp;</b></u>' + char(10)

	-- Report body
	select @rpt = @rpt + convert(char(15), isnull(m.Name, '')) + @coldelimeter + convert(char(100), isnull(s.CollectionName, '')) + char(10)
	from AN7.dbo.Documents d
		inner join teledv10.CM_PR1.dbo.CollectionMembers m on d.Keywords = m.SiteID
		inner join teledv10.CM_PR1.dbo.v_Collections s on m.SiteID = s.SiteID
		left join AN7.dbo.Computers c on m.name = c.Name
	where 
		d.Type_ID = '045B9EBD-780A-4C42-9ECB-2EF31756FA03'	-- SCCM Collection
		and
		c.ID is null	-- Computer does not exist in Alloy
	order by s.CollectionName, m.Name

	-- Close HTML
	set @rpt = @rpt + '</body></html>'

	-- Send email
	exec msdb.dbo.sp_send_dbmail @profile_name = @profile_name, @recipients = @recipients, @subject = @subject, @body = @rpt, @body_format = 'HTML', @importance = 'High'

	-- Output for log
	print @recipients
	print @rpt

end
