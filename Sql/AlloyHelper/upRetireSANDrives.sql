use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upRetireSANDrives') and type in (N'P', N'PC'))
drop procedure dbo.upRetireSANDrives
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170926
-- ============================================================
create procedure dbo.upRetireSANDrives
as
begin

	-- Initialize settings
	set nocount on

	-- Retire drives
	update
		h
	set
		Status_ID = '5D1D8BAC-C52D-464F-909B-6C2373052F68'	-- Retired
	from 
		openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from SanDrives.csv where enclosure_id is not null and slot_id is not null') f
	inner join 
		dbo.vwSANDrives d on f.SAN = d.San and f.enclosure_id = d.Enclosure_ID and f.slot_id = d.Slot_ID
	inner join 
		AN7.dbo.Hardware h on d.DriveID = h.ID and f.FRU_identity <> h.Serial_Num

	-- Output for log
	print convert(varchar, @@rowcount) + ' retired.'

end
