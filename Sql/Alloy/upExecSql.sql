use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upExecSql') and type in (N'P', N'PC'))
drop procedure dbo.upExecSql
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20130221
-- ============================================================
create procedure dbo.upExecSql
	@DB sysname
	,@SQL nvarchar(max)
as
begin

	-- Declarations
	declare @context sysname = '[' + @DB + '].sys.sp_ExecuteSQL'
	
	-- Initialize settings
	set nocount on

	-- Exec in appropriate database
	exec @context @sql
	
end
