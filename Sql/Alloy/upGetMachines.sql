use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetMachines') and type in (N'P', N'PC'))
drop procedure dbo.upGetMachines
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20130416
-- ============================================================
create procedure dbo.upGetMachines
	@Search varchar(50)
as
begin

	-- Initialize settings
	set nocount on

	-- Return a list of servers
	select ID, Name, UDF_MajorApplications
	from AN6.dbo.Computers
	where charindex(@Search, Name, 1) > 0 
	order by Name

end
