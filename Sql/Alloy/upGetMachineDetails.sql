use AlloyHelper
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetMachineDetails') and type in (N'P', N'PC'))
drop procedure dbo.upGetMachineDetails
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20130416
-- ============================================================
create procedure dbo.upGetMachineDetails
	@ID uniqueidentifier
as
begin

	-- Initialize settings
	set nocount on

	-- Return a list of servers
	select 
		cn.ID
		,cn.Import_ID
		,cn.Name
		--,cn.Description
		,ot.Type
		,s.Status
		,cn.UDF_MajorApplications Major_Applications
		,cn.Serial_Num Serial_#
		,cn.Model
		--,cn.Computer_Name
		,cn.CPU
		,cn.CPU_Count
		,cn.RAM
		--,cn.CPU_Speed
		--,cn.HDD
		--,cn.HDD_Free
		--,cn.Install_Date
		,cn.Lan_Card
		,cn.MAC
		,cn.Primary_IP_Address
		,cn.Display_Adapter
		--,b.Brand
		,cn.OS_Name
		,cn.OS_ServicePack
		,cn.OS_Version
		--,cn.Product
		--,cn.Workgroup
		--,cn.Monitor
		--,cn.UDF_OriginalName
		,cn.UDF_HardwareUnderWarranty Under_Warranty
		,ou.Name Organization
		,cn.Domain
		,l.Location
		,l.Address
		,cn.Notes
		,cn.Audit_Date
	from AN6.dbo.Computers cn
		left join AN6.dbo.Object_Type ot on cn.Type_ID = ot.ID
		left join AN6.dbo.Status s on cn.Status_ID = s.ID
		left join AN6.dbo.Organizational_Units ou on cn.Organization_ID = ou.ID
		left join AN6.dbo.Locations l on cn.Location_ID = l.ID
		left join AN6.dbo.Brands b on cn.Manufacturer_ID = b.ID
	where cn.ID = @ID

end
