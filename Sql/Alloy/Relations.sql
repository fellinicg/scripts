insert into dbo.Object_Relations (Left_Object_ID, Right_Object_ID, Type_ID)
select a.ID, b.ID, '6FCF5FE4-0DAC-41AE-93B7-3D0388ADE8CA'
from dbo.Hardware a
left join dbo.Hardware b on 
	charindex(a.Notes, b.Name) = 1 
	and b.Category_ID = '3438DAC0-DB1A-4BD9-B24C-19177D07631D'
	and left(a.UDF_TraySlot, charindex(',', a.UDF_TraySlot) -1) = b.UDF_EnclosureNumber
where a.Category_ID = '154FD72A-10F4-4723-9B35-57AA908A65D3'
order by b.ID

--insert into dbo.Object_Relations (Left_Object_ID, Right_Object_ID, Type_ID)
--select a.ID, b.ID, '6FCF5FE4-0DAC-41AE-93B7-3D0388ADE8CA'
--from 
--(
--select ID, Name
--from [dbo].[Hardware]
--where Type_id = '828CAC0E-AF8B-45EC-A13A-BA14EB4EEADD'
--	and Category_ID = '3438DAC0-DB1A-4BD9-B24C-19177D07631D'
--) a
--left join
--(
--select ID, Name
--from [dbo].[Hardware]
--where Type_id = '828CAC0E-AF8B-45EC-A13A-BA14EB4EEADD'
--	and Category_id is null
--) b on charindex(b.Name, a.Name) > 0
--left join dbo.Object_Relations c on a.ID = c.Left_Object_ID and b.ID = c.Right_Object_ID
--where b.ID is not null and c.ID is null

select a.ID, b.ID
from dbo.Hardware a
left join dbo.Hardware b on 
	charindex(a.Notes, b.Name) = 1 
	and b.Category_ID = '3438DAC0-DB1A-4BD9-B24C-19177D07631D'
	and left(a.UDF_TraySlot, charindex(',', a.UDF_TraySlot) -1) = b.UDF_EnclosureNumber
where a.Category_ID = '154FD72A-10F4-4723-9B35-57AA908A65D3'
order by a.Name

select *
from (
select a.ID 'LeftObj', b.ID 'RightObj', '6FCF5FE4-0DAC-41AE-93B7-3D0388ADE8CA' 'TypeObj'
from dbo.Hardware a
left join dbo.Hardware b on 
	charindex(a.Notes, b.Name) = 1 
	and b.Category_ID = '3438DAC0-DB1A-4BD9-B24C-19177D07631D'
	and left(a.UDF_TraySlot, charindex(',', a.UDF_TraySlot) -1) = b.UDF_EnclosureNumber
where a.Category_ID = '154FD72A-10F4-4723-9B35-57AA908A65D3'
) a
inner join dbo.Object_Relations b on a.LeftObj = b.Left_Object_ID and a.RightObj = b.Right_Object_ID and a.TypeObj = b.Type_ID