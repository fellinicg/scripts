/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [ID]
      ,[Field_ID]
      ,[Table_ID]
      ,[Custom_Label]
      ,[Ref_Table_ID]
      ,[Ref_Condition]
      ,[Mandatory]
      ,[Read_Only]
      ,[Filter]
  FROM [AN7].[dbo].[cfgCustTableFields]
  where Field_ID = '54308D9C-B475-4FB0-AF7D-79E5A85525A1'

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [ID]
      ,[Field_Name]
      ,[Default_Label]
      ,[User_Field]
      ,[Field_Type]
      ,[Field_Order]
      ,[Group_Name]
      ,[Ref_Type]
      ,[Location_Index]
      ,[Hidden]
      ,[Decimal_Digits]
  FROM [AN7].[dbo].[cfgCustFields]
  where id = '54308D9C-B475-4FB0-AF7D-79E5A85525A1'

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [ID]
      ,[Field_ID]
      ,[Field_Value]
      ,[Rank]
      ,[Tags]
  FROM [AN7].[dbo].[cfgCustFieldValues]
  where Field_ID = '54308D9C-B475-4FB0-AF7D-79E5A85525A1'
  order by Rank
--insert into dbo.cfgCustFieldValues values (newid(), '54308D9C-B475-4FB0-AF7D-79E5A85525A1', 'Test', 99, '')
