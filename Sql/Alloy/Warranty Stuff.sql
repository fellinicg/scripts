use Omni

select SerialNumber, Model Model2, case len(type) when 4 then Type else '' end 'Type', WarrExpDate, ExpirationDate, ServerName
from
(
select a.SerialNumber, a.Model, substring(a.Model, charindex('[', a.Model)+1,4) 'Type', a.WarrExpDate, c.ExpirationDate, b.ServerName, b.Model 'Model2'
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\ServerExpDates.xlsx', 'select * from [Sheet1$]') a
left join dbo.tbServer b on a.SerialNumber = b.SerialNumber
left join dbo.tbServerWarranty c on b.ServerID = c.ServerID
) a

select b.Serial_Num, b.Model
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\ServerExpDates.xlsx', 'select * from [Sheet1$]') a
left join AN7.dbo.Computers b on a.SerialNumber = b.Serial_Num


select *, datediff(year, a.ManufDate, getdate()) 'Age'
from (
select a.*, dateadd(year,-3, a.WarrExpDate) 'ManufDate'
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\ServerExpDates.xlsx', 'select * from [Sheet1$]') a
) a
order by Age desc

select b.Name, c.Warranty_Exp, a.[Warranty Expiration Date]
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\ServerExpDates.xlsx', 'select * from [Sheet1$]') a
left join AN7.dbo.Computers b on a.SerialNumber = b.Serial_Num
left join AN7.dbo.Assets c on b.ID = c.Associated_CI_ID
where isnull(c.Warranty_Exp, '1/1/1900') <> a.[Warranty Expiration Date]
--where c.Warranty_Exp is null or isnull(c.Warranty_Exp, '1/1/1900') <> a.[Warranty Expiration Date]

select c.Name, SerialNumber, WarrExpDate, Warranty_Exp, w.Model, c.model
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\ServerExpDates.xlsx', 'select * from [Sheet1$]') w
inner join dbo.CI_Asset_List c on w.SerialNumber = c.Serial_Num
where w.warrexpdate <> c.Warranty_Exp