select
	Computer_name
	, Attribute
	, Action
	, Old_Value
	, New_Value
	, Change_Date
	, Changed_by
from dbo.History_List a
inner join dbo.Computer_List b on a.Object_ID = b.ID
where Changed_by = 'Paul Gornati'
and Attribute in ('PMRY_Support'--, 'Support_Status'
)
and convert(date, Change_Date) between '7/1/2017' and '7/31/2017'
order by Change_date