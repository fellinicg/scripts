select 
	r.Object_Name 'Cluster'
	, c.Name 'Server'
	, d.Product_Name
	, d.Version
	, d.Install_Date
	, c.Serial_Num
	, c.UDF_SiteName
	, UDF_MajorApplications 'Major Applications'
	, c.Manufacturer
	, c.Model
	, '' 'NEC_License'
	--, c.*
from dbo.Object_Relation_Union r
left join dbo.Detected_Product_List d on r.Object_ID = d.Object_ID and charindex('expresscluster', d.Product_Name) > 0
left join dbo.Computer_List c on r.Object_ID = c.ID
where r.Type = 'Cluster'
	--and charindex('expresscluster', d.Product_Name) > 0
order by r.Object_Name