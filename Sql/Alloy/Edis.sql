select  distinct
	a.[Computer Name]
	, a.[Primary IP Address]
	, a.MAC
	, b.Subnet
	, left(c.WirelessAccessPointName, 3) Location
	, b.ScopeID
from openrowset('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=C:\Feeds\20170523-EdisNoLocations.xlsx', 'select * from [sheet1$]') a
left join dbo.tbDhcpScope b on b.Octet1 = parsename(a.[Primary IP Address], 4)
	and b.Octet2 = parsename(a.[Primary IP Address], 3)
	and parsename(a.[Primary IP Address], 2) between b.LowerOctet3 and b.UpperOctet3
	and parsename(a.[Primary IP Address], 1) between b.LowerOctet4 and b.UpperOctet4
left join dbo.tbWirelessAccessPoint c on b.ScopeID = c.ScopeID
order by a.[Computer Name]


select a.Name 'ServerName', e.Product_Name
from dbo.Computers a
	inner join dbo.Object_Type b on a.Type_ID = b.ID
	inner join dbo.Status c on a.Status_ID = c.ID
	inner join dbo.Detected_Software_Products d on a.ID = d.Object_ID
	inner join dbo.Soft_Products e on d.Soft_Product_ID = e.ID
where charindex('server', b.Type) > 0
	and c.Status not in ('Retired', 'Deployed', 'In Stock')
order by a.Name, e.Product_Name

--begin tran
--update dbo.Computers set Owner_ID = d.Id
select distinct
      a.Object_ID
      ,a.Changed_by
      ,a.Changed_by_ID
      ,a.Change_Date
      ,a.Action
      ,a.Attribute
      ,a.New_Value
      ,a.Old_Value
	  ,c.Full_Name 'Current_Owner'
	  ,d.Full_Name 'Old_Owner'
	  --,e.Status 'Old_Owner_Status'
	  --,e.ID
	  ,b.Owner_ID
	  ,b.UDF_ScannedUser_ID
      ,a.Field_Name
from dbo.History a
	inner join dbo.Computers b on a.Object_ID = b.ID
	inner join dbo.Persons c on b.Owner_ID = c.ID
	inner join dbo.Persons d on a.Old_Value = d.Full_Name
	--inner join dbo.Status e on d.Status_ID = e.ID
where a.Attribute = 'Owner'
	and a.Action = 'Changed'
	and a.Changed_by_ID = 'E821BDDA-AE77-4C6B-B74F-50105FB8FC23' --TMF
	and a.Change_Date between '3/1/2016' and '3/2/2016'
	and a.New_Value in ('miguser Miguel', 'comanche')
	and a.New_Value = c.Full_Name
	and d.Status_ID = 'A83B21FD-3BF0-4184-B94A-8A8FF3DDFCA3' --Active
--order by a.Object_ID

--commit tran

select a.*
from dbo.History a
	inner join (select Object_ID, max(Change_Date) Change_Date from dbo.History where Attribute = 'Owner' and Action = 'Changed' group by Object_ID) b on a.Object_ID = b.Object_ID and a.Change_Date = b.Change_Date
where a.Attribute = 'Owner'
	and a.Action = 'Changed'
	and a.New_Value in ('miguser Miguel', 'comanche', 'SYSTEM')

--select *
--from openrowset('Microsoft.ACE.OLEDB.12.0','Text;Database=c:\feeds','select * from [Maas360Dec.csv]')
begin tran

-- Update Owner_ID with ScannedUser_ID when they don't match 
--
update dbo.Computers set Owner_ID = UDF_ScannedUser_ID
--select a.Owner_ID, a.UDF_ScannedUser_ID, a.Name
from dbo.Computers a
where 
	Owner_ID <> UDF_ScannedUser_ID
	and
	a.Status_ID in (
		'CBF98E44-75AD-42A9-AA5D-EBEFD7A26F90',
		'7ED012D2-A4AB-4A12-995D-C769E7346C6A',
		'C0B5AB2C-4920-431C-863F-03EB37D65EE5',
		'98F0084C-FDC6-4BA1-96A5-152298363590')
	and 
	a.Type_ID in (
		'49AB351E-E54A-4498-AA33-213C089732D3',
		'ED829940-EA05-4B5C-80B0-43DE6841D2F4',
		'B2F31EB3-13D2-4F66-B384-2CDF35627649',
		'5FA79873-76EB-4A3F-B808-51CD97824EBF',
		'B1DCDEE2-7B1F-4558-9BF9-8BE0912FA387',
		'4AB82B56-A00E-43A2-A599-A2A08EE300A3')
	and
	a.UDF_ScannedUser_ID not in ('E3EF474C-63AE-453E-9260-53CDD56B7BB8') -- Exclude SYSTEM

commit tran

begin tran

-- Update Location_ID and Orgnaization_ID with latest from Persons table 
--
update dbo.Computers set Organization_ID = b.Organization_ID, Location_ID = b.Location_ID
--select a.Name, a.Owner_ID 'CID', b.ID 'PID', a.Location_ID 'CLOC', b.Location_ID 'PLOC', a.Organization_ID 'CORG', b.Organization_ID 'PORG'
from dbo.Computers a
	inner join dbo.Persons b on  a.Owner_ID = b.ID and (a.Location_ID <> b.Location_ID or a.Organization_ID <> b.Organization_ID)
where 
	a.Status_ID in (
		'CBF98E44-75AD-42A9-AA5D-EBEFD7A26F90',
		'7ED012D2-A4AB-4A12-995D-C769E7346C6A',
		'C0B5AB2C-4920-431C-863F-03EB37D65EE5',
		'98F0084C-FDC6-4BA1-96A5-152298363590')
	and 
	a.Type_ID in (
		'49AB351E-E54A-4498-AA33-213C089732D3',
		'ED829940-EA05-4B5C-80B0-43DE6841D2F4',
		'B2F31EB3-13D2-4F66-B384-2CDF35627649',
		'5FA79873-76EB-4A3F-B808-51CD97824EBF',
		'B1DCDEE2-7B1F-4558-9BF9-8BE0912FA387',
		'4AB82B56-A00E-43A2-A599-A2A08EE300A3')
commit tran

--select a.Owner_ID, p.ID, a.Name
--from dbo.Computers a
--	inner join dbo.Persons p on  a.Owner_ID = p.ID
--	inner join dbo.Locations b on p.Location_ID = b.ID
--	inner join dbo.Organizational_Units c on p.Organization_ID = c.ID
--where 
--	a.Status_ID in (
--		'CBF98E44-75AD-42A9-AA5D-EBEFD7A26F90',
--		'7ED012D2-A4AB-4A12-995D-C769E7346C6A',
--		'C0B5AB2C-4920-431C-863F-03EB37D65EE5',
--		'98F0084C-FDC6-4BA1-96A5-152298363590')
--	and 
--	a.Type_ID in (
--		'49AB351E-E54A-4498-AA33-213C089732D3',
--		'ED829940-EA05-4B5C-80B0-43DE6841D2F4',
--		'B2F31EB3-13D2-4F66-B384-2CDF35627649',
--		'5FA79873-76EB-4A3F-B808-51CD97824EBF',
--		'B1DCDEE2-7B1F-4558-9BF9-8BE0912FA387',
--		'4AB82B56-A00E-43A2-A599-A2A08EE300A3')
