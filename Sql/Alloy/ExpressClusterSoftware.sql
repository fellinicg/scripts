/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [ID]
      ,[Product_Name]
      ,[Manufacturer_ID]
      ,[Version]
      ,[Web_Page]
      ,[Platform_ID]
      ,[Notes]
      ,[Part_of_Suite_ID]
      ,[Description]
      ,[Part_Number]
      ,[New]
      ,[Group_ID]
      ,[Modified_Date]
      ,[Category_ID]
  FROM [AN7].[dbo].[Soft_Products]
  where charindex('expresscluster', Product_Name) > 0

select distinct Server, c.CPU_Count, c.UDF_SiteName
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\xc.xlsx', 'select * from [Sheet1$]') w
--left join dbo.Computer_List c on w.Server  = c.Name
left join dbo.Computers c on w.Server  = c.Name

select 
	distinct 
	w.Server
	, UDF_MajorApplications 'Major Applications'
	, c.Primary_IP_Address 'IP Address'
	, c.Manufacturer
	, c.Model
	, r.object_name 'Cluster'
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\xc.xlsx', 'select * from [Sheet1$]') w
--left join dbo.Computer_List c on w.Server  = c.Name
left join dbo.Computer_List c on w.Server  = c.Name and Status_ID not in ('C8455FDB-F267-4452-8E73-E3407E82D391', '3417FB44-7DF7-4C9F-9A25-AAB4CC4C43E6')
left join dbo.Object_Relation_Union r on c.ID = r.Object_ID and r.Type = 'Cluster'
order by w.server