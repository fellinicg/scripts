--Check brand count
select count(1) 
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20161020-NewEnclosures.xlsx', 'select * from [Sheet1$]') a
union 
select count(1)
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20161020-NewEnclosures.xlsx', 'select * from [Sheet1$]') a
	left join dbo.Brands b on a.Manufacturer = b.Brand

-- Update SAN Enclosures into Hardware
update dbo.Hardware set 
	Type_ID = '828CAC0E-AF8B-45EC-A13A-BA14EB4EEADD'
	, Category_ID = '3438DAC0-DB1A-4BD9-B24C-19177D07631D'
	, Status_ID = 'BC3C4756-9764-4BFD-95CD-4C9819871D3E'
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20161020-NewEnclosures.xlsx', 'select * from [Sheet1$]') a
	inner join dbo.Hardware h on a.Serial = h.Serial_Num
where Type_ID is null

-- Add SAN to Hardware
--insert into dbo.Hardware (Name, Type_ID, Status_ID)
--select 
--	a.Name
--	, '828CAC0E-AF8B-45EC-A13A-BA14EB4EEADD'
--	, 'BC3C4756-9764-4BFD-95CD-4C9819871D3E'
--from (select distinct rtrim(left(h.Name, charindex('{', h.Name) - 1)) 'Name'
--				from dbo.Hardware h
--				where h.Type_id = '828CAC0E-AF8B-45EC-A13A-BA14EB4EEADD'
--					and h.Category_ID = '3438DAC0-DB1A-4BD9-B24C-19177D07631D') a
--left join dbo.Hardware b on a.Name = b.Name
--where b.Name is null

----insert into dbo.Hardware (Name, Description, Type_ID, Category_ID, Status_ID, Serial_Num, Manufacturer_ID, Model, UDF_EnclosureNumber)
--select 
--	rtrim(substring(a.[SAN Name], 1, charindex(' ', a.[SAN Name]))) + ' {' + convert(varchar, a.ID) + '}' 'Name'
--	, a.[SAN Name] 'Description'
--	, '828CAC0E-AF8B-45EC-A13A-BA14EB4EEADD' 'Type_ID'
--	, '3438DAC0-DB1A-4BD9-B24C-19177D07631D' 'Category_ID'
--	, 'BC3C4756-9764-4BFD-95CD-4C9819871D3E' 'Status_ID'
--	, a.Serial 'Serial_Num'
--	, b.ID 'Manufacturer_ID'
--	, a.[Enlosure Type] 'Model'
--	, a.ID 'UDF_EnclosureNumber'
--from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20161020-NewEnclosures.xlsx', 'select * from [Sheet1$]') a
--	left join dbo.Hardware h on a.Serial = h.Serial_Num
--	left join dbo.Brands b on a.Manufacturer = b.Brand
--where h.ID is null

-- Insert SAN Drives into Hardware
insert into dbo.Hardware (Name, Notes, Type_ID, Category_ID, Status_ID, Serial_Num, Product, UDF_Capacity, UDF_DataRate, UDF_TraySlot)
select
	a.SAN  + ' {' + convert(varchar, a.TraySlot) + '}' 'Name'
	,  a.SAN 'Notes'
	, '828CAC0E-AF8B-45EC-A13A-BA14EB4EEADD' 'Type_ID'
	, '154FD72A-10F4-4723-9B35-57AA908A65D3' 'Category_ID'
	, 'BC3C4756-9764-4BFD-95CD-4C9819871D3E' 'Status_ID'
	, a.SerialNumber
	, a.ProductID
	, a.Capacity
	, a.DataRate
	, a.TraySlot
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20161020-NewEnclosures.xlsx', 'select * from [Sheet2$]') a
	left join dbo.Hardware h on a.SerialNumber = h.Serial_Num
where h.ID is null

-- Relate SAN Drives to Enclosure
insert into dbo.Object_Relations (Left_Object_ID, Right_Object_ID, Type_ID)
select 
	h.ID
	, e.ID
	, '6FCF5FE4-0DAC-41AE-93B7-3D0388ADE8CA'
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20161020-NewEnclosures.xlsx', 'select * from [Sheet2$]') a
	inner join dbo.Hardware h on a.SerialNumber = h.Serial_Num
	left join dbo.Object_Relations r on h.ID = r.Left_Object_ID
	inner join dbo.Hardware e on h.Notes + ' {' + left(h.UDF_TraySlot, charindex(',', h.UDF_TraySlot) - 1) + '}' = e.Name
where r.ID is null
