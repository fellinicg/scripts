-- No Warranty Exp
--
select 
	a.Name
	, isnull(convert(varchar, a.Warranty_Exp), '') Warranty_Exp
	, a.SerialNumber
	, a.Description
	, isnull(a.Status, '') Status
	, a.Brand
	, a.OS_Name
	, b.Created_Date AlloyCreateDt
from dbo.vwServer a
inner join AN7.dbo.Computer_List b on a.ID = b.ID
where a.Warranty_Exp is null
	and charindex('vmware', a.Brand) = 0
order by a.Status, a.Description

-- Active Servers
--
select 
	a.Name
	, convert(varchar, a.Warranty_Exp, 101) Warranty_Exp
	, a.SerialNumber
	, a.Description
	, isnull(a.Status, '') Status
	, a.Brand
	, a.OS_Name
	, convert(varchar, b.Created_Date, 101) AlloyCreateDt
from dbo.vwServer a
inner join AN7.dbo.Computer_List b on a.ID = b.ID
where a.Warranty_Exp is not null
	and charindex('vmware', a.Brand) = 0 
	and charindex('PMRY-VM', a.Description) = 0
	and a.Description not in ('Decomm')
order by a.Warranty_Exp, a.Status, a.Description

-- Active Servers
--
select 
	a.Name
	, a.SerialNumber
	, a.Description
	, isnull(a.Status, '') Status
	, a.Brand
	, a.OS_Name
	, convert(varchar, b.Created_Date, 101) AlloyCreateDt
from dbo.vwServer a
inner join AN7.dbo.Computer_List b on a.ID = b.ID
where charindex('vmware', a.Brand) = 0 
	and charindex('PMRY-VM', a.Description) = 1
order by a.Status, a.Description

select 
	a.Name
	, a.SerialNumber
	, a.Description
	, isnull(a.Status, '') Status
	, a.Brand
	, a.OS_Name
	, convert(varchar, b.Created_Date, 101) AlloyCreateDt
from dbo.vwServer a
inner join AN7.dbo.Computer_List b on a.ID = b.ID
where charindex('vmware', a.Brand) = 1 
	and charindex('PMRY-P', a.Description) = 1
order by a.Status, a.Description