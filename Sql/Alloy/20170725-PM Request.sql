 select
	c.Name
	, c.Computer_Name
	, c.Description
	, p.First_Name + ' ' + p.Last_Name
	, c.Owner_ID
--begin tran
--update dbo.Computers set Owner_ID = 'B3F9E2F2-0956-497F-A5A1-69907E2F731C'
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\patch_June17_Servers.xlsx', 'select * from [Server Details$]') n
	inner join dbo.Computers c on n.[Computer Name] = c.Computer_Name
	left join dbo.Persons p on c.Owner_ID = p.ID
where 
	(c.Description like '%Verint%' or c.Description like '%Lenel%')
	and
	(
		--c.Computer_Name like 'JFK%' or		-- Qam Rahman D974A68C-63EA-471A-A93C-06EE778A84E8
		--c.Computer_Name like 'EWR%' or		-- Qam Rahman D974A68C-63EA-471A-A93C-06EE778A84E8
		--c.Computer_Name like 'LGA%' 		-- Qam Rahman D974A68C-63EA-471A-A93C-06EE778A84E8

		--c.Computer_Name like 'GWB%' or		-- Michael Burke	B3F9E2F2-0956-497F-A5A1-69907E2F731C
		--c.Computer_Name like 'LT%' or		-- Michael Burke	B3F9E2F2-0956-497F-A5A1-69907E2F731C
		--c.Computer_Name like 'HT%' or		-- Michael Burke	B3F9E2F2-0956-497F-A5A1-69907E2F731C
		--c.Computer_Name like 'PABT%' or		-- Michael Burke	B3F9E2F2-0956-497F-A5A1-69907E2F731C
		--c.Computer_Name like 'OBX%' or		-- Michael Burke	B3F9E2F2-0956-497F-A5A1-69907E2F731C
		--c.Computer_Name like 'GB%' or		-- Michael Burke	B3F9E2F2-0956-497F-A5A1-69907E2F731C
		--c.Computer_Name like 'BB%' 		-- Michael Burke	B3F9E2F2-0956-497F-A5A1-69907E2F731C

		c.Computer_Name like 'PCD%' or		-- Neel Keer	65327355-F538-41AA-B0F2-D6CAF41326F8
		c.Computer_Name like 'PN%' or		-- Neel Keer	65327355-F538-41AA-B0F2-D6CAF41326F8
		c.Computer_Name like 'BMT%' or		-- Neel Keer	65327355-F538-41AA-B0F2-D6CAF41326F8
		c.Computer_Name like 'PATH%' or		-- Neel Keer	65327355-F538-41AA-B0F2-D6CAF41326F8
		c.Computer_Name like 'PTH%' or		-- Neel Keer	65327355-F538-41AA-B0F2-D6CAF41326F8
		c.Computer_Name like 'PTCC%' or		-- Neel Keer	65327355-F538-41AA-B0F2-D6CAF41326F8
		c.Computer_Name like 'JSTC%' or		-- Neel Keer	65327355-F538-41AA-B0F2-D6CAF41326F8
		c.Computer_Name like 'HBN%' 		-- Neel Keer	65327355-F538-41AA-B0F2-D6CAF41326F8
	)
--order by n.[Computer Name]

--commit tran