select distinct nn.Name, nn.DNS_Name, na.IP_Address, na.MAC_Address
from dbo.Inv_Network_Adapters na
inner join dbo.Inv_NetNodes nn on na.Node_ID = nn.ID
where len(na.MAC_Address) > 0
order by Name