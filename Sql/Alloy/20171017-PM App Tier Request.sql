select 
	c.Computer_Name
	, c.Description
	, c.UDF_SystemName
	, a.*
from 
	AN7.dbo.Computers c
left join openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20140218-PANYNJAppTier.xlsx', 'select * from [Application Tier$]') a 
	on 
		c.Description = a.Application
		or
		charindex(c.Description, a.Application) > 0
		or
		charindex(a.Application, c.Description) > 0
		or
		c.UDF_SystemName = a.Application
		or
		charindex(c.UDF_SystemName, a.Application) > 0
		or
		charindex(a.Application, c.UDF_SystemName) > 0
where 
	c.Type_ID in (
		'629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3' --Server
	)
	and
	c.Status_ID not in (
		'3417FB44-7DF7-4C9F-9A25-AAB4CC4C43E6' --Retired
	)
order by c.Computer_Name