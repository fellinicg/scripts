select
	c.Name
	, c.UDF_MajorApplications
	, s.Status
	, case 
		when charindex('lab', c.UDF_MajorApplications) > 0 then 'Lab'
		when charindex('dev', c.UDF_MajorApplications) > 0 then 'Development'
		when charindex('refresh', c.UDF_MajorApplications) > 0 then 'Development'
		when charindex('test', c.UDF_MajorApplications) > 0 then 'Development'
		when charindex('QA', c.UDF_MajorApplications) > 0 then 'Development'
		when charindex('prod', c.UDF_MajorApplications) > 0 then 'Production'
		when charindex('prd', c.UDF_MajorApplications) > 0 then 'Production'
		when charindex('cluster', c.UDF_MajorApplications) > 0 then 'Cluster'
		when charindex('stand by', c.UDF_MajorApplications) > 0 then 'Cluster'
		when charindex('stand-by', c.UDF_MajorApplications) > 0 then 'Cluster'
		when charindex('primary', c.UDF_MajorApplications) > 0 then 'Cluster'
		when charindex('master', c.UDF_MajorApplications) > 0 then 'Cluster'
		when charindex('non-active', c.UDF_MajorApplications) > 0 then 'Cluster'
	  end 'Translation'
	, t.Type
from dbo.Computers c
inner join dbo.Status s on c.Status_ID = s.ID
inner join dbo.Object_Type t on c.Type_ID = t.ID
where 
	c.Type_ID in (
				'E00622AC-074D-4F49-9654-8CEA4FC56C4E'	-- Server ICS
				,'629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3'	-- Server
				,'B37E1387-CF14-4562-930D-DD48D5F1837A'	-- VM Server
				)
	and
	c.Status_ID not in (
				'3417FB44-7DF7-4C9F-9A25-AAB4CC4C43E6' -- Retired
				)
order by c.UDF_MajorApplications