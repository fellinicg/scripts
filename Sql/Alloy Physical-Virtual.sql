--Physical
select Name, UDF_MajorApplications, Description, Serial_Num
from AN6.dbo.Computers
where Type_ID = '629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3'
and convert(varchar(max), Description) = 'PMRY-P'
and UDF_MajorApplications is not null
and (UDF_MajorApplications like '%test%'
	or UDF_MajorApplications like '%dev%'
	or UDF_MajorApplications like '%lab%')
order by Name

--Virtual
select Name, UDF_MajorApplications, Description, Serial_Num
from AN6.dbo.Computers
where Type_ID = '629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3'
and convert(varchar(max), Description) = 'PMRY-VM'
and UDF_MajorApplications is not null
and (UDF_MajorApplications like '%test%'
	or UDF_MajorApplications like '%dev%'
	or UDF_MajorApplications like '%lab%')
order by Name

--Unknown Physical
select Name, UDF_MajorApplications, Description, Serial_Num
from AN6.dbo.Computers
where Type_ID = '629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3'
and convert(varchar(max), Description) = 'PMRY-P'
and UDF_MajorApplications is null
order by Name

--Unknown Virtual
select Name, UDF_MajorApplications, Description, Serial_Num
from AN6.dbo.Computers
where Type_ID = '629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3'
and convert(varchar(max), Description) = 'PMRY-VM'
and UDF_MajorApplications is null
order by Name

--Unknown
select Name, UDF_MajorApplications, Description, Serial_Num
from AN6.dbo.Computers
where Type_ID = '629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3'
and UDF_MajorApplications is null
order by Name