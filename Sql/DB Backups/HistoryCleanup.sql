USE DBATools
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.HistoryCleanup') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.HistoryCleanup
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091209
-- ============================================================
CREATE PROCEDURE dbo.HistoryCleanup
	@TimeSpan int
	,@TimeInterval char(1) = 'Y'
	,@Backup bit = 0
	,@Job bit = 0
	,@MaintPlan bit = 0
AS
BEGIN

	-- Declarations
	declare
		@CurrentDT datetime,
		@HistDT datetime,
		@HistDTStr nvarchar(19)
		
	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @CurrentDT = getdate()
	select @HistDT =
		case upper(@TimeInterval) 
			when 'H' then DATEADD(hh,-@TimeSpan, @CurrentDT)
			when 'D' then DATEADD(dd,-@TimeSpan, @CurrentDT)
			when 'W' then DATEADD(ww,-@TimeSpan, @CurrentDT)
			when 'M' then DATEADD(mm,-@TimeSpan, @CurrentDT)
			else DATEADD(yy,-@TimeSpan, @CurrentDT)
		end
	set @HistDTStr = CONVERT(nvarchar(19), @HistDT, 126)
	
	-- Backup History
	if (@Backup = 1) exec msdb.dbo.sp_delete_backuphistory @HistDT

	-- Job History
	if (@Job = 1) exec msdb.dbo.sp_purge_jobhistory  @oldest_date = @HistDTStr
	
	-- Maintenance Plan History
	if (@MaintPlan = 1) exec msdb..sp_maintplan_delete_log null, null, @HistDTStr

END



GO


