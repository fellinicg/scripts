-- Add New Servers
--insert into [TELEDB29].PomeroyMgmt.dbo.tbServer(ServerName, ServerType, OSType, OnPawanet, SupportBeginDate)
select a.[Server Name] sn, 'P', a.[Major OS], 1, a.[Support Date]
from dbo.tbPhysical a
left join 
	(select * from [TELEDB29].PomeroyMgmt.dbo.vwServerInfo) b
	on a.[server name] = b.servername
where serverid is null
--order by sn
union all
select a.[Server Name] sn, 'V', a.[Major OS], 1, c.[Support Date]
from dbo.tbVirtual a
inner join dbo.tbAll c on rtrim(ltrim(a.[Server Name])) = rtrim(ltrim(c.[Server Name]))
left join 
	(select * from [TELEDB29].PomeroyMgmt.dbo.vwServerInfo) b
	on a.[Server Name] = b.servername
where serverid is null
order by sn


-- Add New Servers Deployment Status
--insert into [TELEDB29].PomeroyMgmt.dbo.tbSrvDeploymentStatus(ServerID, DeploymentStatusID)
select b.ServerID, case [Deployment Status] when 'Unbuilt' then 4 else 2 end
from dbo.tbAll a
inner join
	(select a.ServerID, a.ServerName
	from [TELEDB29].PomeroyMgmt.dbo.tbServer a
	left join [TELEDB29].PomeroyMgmt.dbo.tbSrvDeploymentStatus b on a.ServerID = b.ServerID
	where b.ServerID is null) b on a.[Server Name] = b.ServerName

-- Decomm Servers	
--insert into [TELEDB29].PomeroyMgmt.dbo.tbSrvDeploymentStatus(ServerID, DeploymentStatusID)
select b.ServerID, 3
from dbo.tbDecomm a
inner join 
	(select * from [TELEDB29].PomeroyMgmt.dbo.vwServerInfo) b
	on a.[server name] = b.servername
where b.ServerName <> 'EWR-ECLIPSX'
	and b.DeploymentStatus <> 'Decommissioned'
order by date, [server name]

-- Add warranty expiration date
--insert into [TELEDB29].PomeroyMgmt.dbo.tbServerWarranty(ServerID, ExpirationDate)
select b.ServerID, a.Warranty
from dbo.tbAll a
inner join 
(
	select s.ServerID, s.ServerName
	from [TELEDB29].PomeroyMgmt.dbo.tbServer s
	left join [TELEDB29].PomeroyMgmt.dbo.tbServerWarranty w on s.ServerID = w.ServerID
	where w.ServerID is null
) b on a.[Server Name] = b.ServerName
where a.Warranty is not null


-- Update warranty expiration date
--update [TELEDB29].PomeroyMgmt.dbo.tbServerWarranty set ExpirationDate = b.Warranty, UpdateDateTime = current_timestamp
select b.Warranty, b.ExpirationDate, current_timestamp
from [TELEDB29].PomeroyMgmt.dbo.tbServerWarranty a
inner join 
(
		select b.ServerID, a.Warranty, b.ExpirationDate
		from dbo.tbAll a
		inner join 
		(
			select s.ServerID, s.ServerName, w.ExpirationDate, w.CreateDateTime
			from [TELEDB29].PomeroyMgmt.dbo.tbServerWarranty w
			inner join [TELEDB29].PomeroyMgmt.dbo.tbServer s on w.ServerID = s.ServerID
			where w.ExpirationDate is not null
		) b on a.[Server Name] = b.ServerName
		where a.Warranty is not null
			and a.Warranty <> b.ExpirationDate
) b on a.ServerID = b.ServerID