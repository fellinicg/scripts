declare @pg_size int, @Instancename varchar(50)

select @pg_size = low from master..spt_values where number = 1 and type = 'E'

--select 'Memory usage details for SQL Server instance ' + @@SERVERNAME  + ' (' + CAST(SERVERPROPERTY('productversion') AS VARCHAR) + ' - ' +  SUBSTRING(@@VERSION, CHARINDEX('X',@@VERSION),4)  + ' - ' + CAST(SERVERPROPERTY('edition') AS VARCHAR) + ')' ' '

select 
	convert(real, physical_memory_in_bytes / 1048576.0) as [Physical Memory (MB)]
	,convert(real, physical_memory_in_bytes/1073741824.0) as [Physical Memory (GB)]
	,convert(real, virtual_memory_in_bytes/1048576.0) as [Virtual Memory (MB)]
	,convert(real, (bpool_committed * 8) / 1024.0) as [Buffer Pool Committed (MB)]
	,convert(real, (bpool_commit_target * 8) / 1024.0) as [Buffer Pool Commit Target (MB)]
	,convert(real, (bpool_visible * 8) / 1024.0) as [Buffer Pool Visible (MB)]  
from sys.dm_os_sys_info

select counter_name as [Memory]
	,cntr_value as KB
	,convert(int, round(cntr_value / 1024.0, 0)) as MB
	,convert(real, round((cntr_value / 1048576.0), 4)) as GB
from sys.dm_os_performance_counters 
where counter_name in (
		'Total Server Memory (KB)'
		,'Target Server Memory (KB)'
		,'Connection Memory (KB)'
		,'Lock Memory (KB)'
		,'SQL Cache Memory (KB)'
		,'Optimizer Memory (KB)'
		,'Granted Workspace Memory (KB)'
	)

select counter_name as 'Pages'
	,convert(int, cntr_value) as [8K]
	,convert(int, (cntr_value * @pg_size) / 1024.0) as KB
	,convert(int, (cntr_value * @pg_size) / 1048576.0) as MB
from sys.dm_os_performance_counters 
where object_name like '%Buffer Manager%'
	and counter_name in (
		'Total pages'
		,'Database pages'
		,'Free pages'
		,'Reserved pages'
		,'Stolen pages'
	)
union all
select counter_name
	,convert(int, cntr_value) as [8K]
	,convert(int, (cntr_value * @pg_size) / 1024.0) as KB
	,convert(int, (cntr_value * @pg_size) / 1048576.0) as MB
from sys.dm_os_performance_counters 
where object_name like '%Plan Cache%'
	and counter_name = 'Cache Pages' 
	and instance_name = '_Total'