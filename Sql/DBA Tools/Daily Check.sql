declare @tmp table (ServerName varchar(50))
declare
	@server varchar(50)
	,@cmd varchar(max)

set nocount on

insert into @tmp
select distinct isnull(si.SQLInstanceName, s.ServerName) ServerName
from teledb22.PomeroyMgmt.dbo.tbServer s
inner join teledb22.PomeroyMgmt.dbo.tbDatabase d on s.ServerID = d.ServerID
left join teledb22.PomeroyMgmt.dbo.tbSQLInstance si on s.ServerID = si.ServerID
where s.OnPawanet = 1 and s.IsOnline = 1 and d.IsOnline = 1
order by ServerName

while exists(select 1 from @tmp)
begin
	select top 1 @server = servername from @tmp order by servername
	set @cmd = 'use [master]
		create table #DBInfo (ServerName varchar(100),DatabaseName varchar(100),FileSizeMB varchar(20),
		LogicalFileName varchar(500),PhysicalFileName varchar(1000),Status varchar(100),
		Updateability varchar(100),RecoveryMode varchar(100),FreeSpaceMB varchar(20),FreeSpacePct varchar(7),
		MaxSize varchar(50),Growth varchar(50),PollDate datetime)
		declare @command varchar(5000)  
		set @command = ''''Use [' + '?' + ']
		select  
		@@servername
		,' + '''''' + '''''?''''' + '''''' + '
		,convert(varchar, cast(round(sysfiles.size/128.00, 2) as decimal(10,2))) + ' + '''''''''' + ' MB' + '''''''''' + '
		,sysfiles.name
		,sysfiles.filename
		,convert(varchar,DatabasePropertyEx(''''''''?'''''''',''''''''Status''''''''))
		,convert(varchar,DatabasePropertyEx(''''''''?'''''''',''''''''Updateability''''''''))
		,convert(varchar,DatabasePropertyEx(''''''''?'''''''',''''''''Recovery''''''''))
		,convert(varchar, cast(round(sysfiles.size/128.00 - fileproperty(sysfiles.name, ' + '''''''''' +  'SpaceUsed' + '''''''''' + ' )/128.00, 2) as decimal(10,2))) + ' + '''''''''' + ' MB' + '''''''''' + '
		,cast(100 * (cast(((sysfiles.size/128.00 - fileproperty(sysfiles.name, ' + '''''''''' + 'SpaceUsed' + '''''''''' + ' )/128.00)/(sysfiles.size/128.00)) AS decimal(4,2))) AS varchar(8)) + ' + '''''''''' + '%' + '''''''''' + '
		,case maxsize when -1 then ''''''''Unlimited'''''''' else convert(varchar, (convert(bigint, maxsize) * 8) / 1024) + ' + '''''''''' + ' MB' + '''''''''' + ' end
		,case status & 0x100000 when 0 then convert(varchar, (growth * 8) / 1024) + ' + '''''''''' + ' MB' + '''''''''' + ' else convert(varchar, growth) + ' + '''''''''' + '%' + '''''''''' + ' end
		,getdate()
		from dbo.sysfiles
		''''
		insert into #DBInfo
		exec sp_MSForEachDB @command
		select ServerName,LogicalFileName,FileSizeMB,FreeSpaceMB,FreeSpacePct,
		DatabaseName,RecoveryMode,PhysicalFileName,Status,Updateability,
		MaxSize,Growth from #DBInfo order by DatabaseName
		exec xp_fixeddrives'
	exec('exec [' + @server + '].master.dbo.sp_sqlexec ''' + @cmd + '''')
	delete from @tmp where servername = @server
end


		--select  
		--convert(varchar, cast(round(sysfiles.size/128.00, 2) as decimal(10,2))) + ' MB' 'Size'
		--,sysfiles.name
		--,sysfiles.filename
		--,convert(varchar, cast(round(sysfiles.size/128.00 - fileproperty(sysfiles.name, 'SpaceUsed')/128.00, 2) as decimal(10,2))) + ' MB' 'Space Used'
		--,cast(100 * (cast(((sysfiles.size/128.00 - fileproperty(sysfiles.name, 'SpaceUsed')/128.00)/(sysfiles.size/128.00)) AS decimal(4,2))) AS varchar(8)) + '%' '% free'
		--,case maxsize when -1 then 'Unlimited' else convert(varchar, (convert(bigint, maxsize) * 8) / 1024) + ' MB' end
		--,case status & 0x100000 when 0 then convert(varchar, (growth * 8) / 1024) + ' MB' else convert(varchar, growth) + '%' end
		--,getdate()
		--from dbo.sysfiles
