set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.tfDelimitedListToTable') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.tfDelimitedListToTable
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20161026
-- ============================================================
create function dbo.tfDelimitedListToTable (@CommaDelimitedList varchar(max), @Delimiter char(1))
returns @ResultTable table (ListItem varchar(500) null index ix1 clustered)
as
begin

	-- Declarations
	declare
		@StartPos int = 0
		,@EndPos int = 0
		,@DelimiterCount int
		,@Counter int = 0

	-- Initialize variables
	if right(@CommaDelimitedList, 1) <> @Delimiter set @CommaDelimitedList = @CommaDelimitedList + @Delimiter
	set @DelimiterCount = datalength(@CommaDelimitedList) - datalength(replace(@CommaDelimitedList, @Delimiter, ''))

	-- Process list
	while @Counter < @DelimiterCount
	begin
		set @EndPos = charindex(@Delimiter, @CommaDelimitedList, @StartPos)
		--select @StartPos, @EndPos
		insert into @ResultTable(ListItem)
		select substring(@CommaDelimitedList, @StartPos, @EndPos - @StartPos) --@EndPos - @StartPos
		set @StartPos = @EndPos + 1
		set @Counter = @counter + 1
	end
	return

end