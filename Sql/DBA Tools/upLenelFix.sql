USE [master]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upLenelFix') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upLenelFix
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20110209
-- ============================================================
CREATE PROCEDURE dbo.upLenelFix
AS
BEGIN

	-- Declarations
	declare @machine varchar(100)
	declare @currsrv varchar(100)

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @machine = convert(varchar, serverproperty('servername'))
	select @currsrv = name from sys.servers where server_id = 0

	-- Only rename if necessary
	if (@@servername <> @machine)
	begin
		exec sp_dropserver @currsrv
		exec sp_addserver @machine, 'local'
	end

END
GO
grant exec on dbo.upLenelFix to nec

