-- Change server name
EXEC sp_dropserver 'Your_OLD_Computer_Name'
GO

EXEC sp_addserver 'Your_NEW_Computer_Name', 'local'
GO

-- Excel
SELECT a.*
FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0', 
   'Excel 8.0;Database=d:\system\TranSys\data.xls', 'SELECT * FROM [data$]') 
   AS a
   
-- Map existing database user to SQL Server login
sp_change_users_login 'update_one', 'loginname', 'loginname'

select 'sp_change_users_login ''update_one'', ''' + name + ''', ''' + name + ''''
from sys.sysusers
where issqluser = 1 and hasdbaccess = 1

-- Detach database
exec sp_detach_db @dbname = N'LT'

-- Attach database
exec sp_attach_db @dbname = N'GWB', 
    @filename1 = N'E:\SQL\Data\GWB.mdf', 
    @filename2 = N'E:\SQL\Log\GWB_1.LDF';

-- Set database mode
alter database databasenamehere set multi_user with no_wait
alter database databasenamehere set single_user with no_wait

-- Bring database online from Restoring...
restore database dbname with recovery

-- Dependencies
select do.name, o.name, c.name
from sys.sysdepends d
inner join sys.sysobjects do on d.id = do.id
inner join sys.sysobjects o on d.depid = o.id
inner join sys.syscolumns c on d.depid = c.id and d.depnumber = c.colid 
where d.id = 8387099

--select *
--from sys.sysdepends d
--inner join sys.sysobjects o on d.id = o.id

--select *
--from sysobjects
--where id = 1207675350

--select *
--from sys.syscolumns
--where id = 1207675350
--and colid in (1,20)