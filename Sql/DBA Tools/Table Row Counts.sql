select 
	sc.name +'.'+ ta.name TableName
	,sum(pa.rows) RowCnt
from sys.tables ta
inner join sys.partitions pa on pa.object_id = ta.object_id
inner join sys.schemas sc on ta.schema_id = sc.schema_id
where 
	ta.is_ms_shipped = 0 
	and pa.index_id IN (1,0)
group by sc.name,ta.name
order by sum(pa.rows) desc
