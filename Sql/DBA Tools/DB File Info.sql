declare @tmp table (idx int identity(1,1), ServerName varchar(50), DB varchar(50))
declare
	@id int
	,@server varchar(50)
	,@last varchar(50)
	,@db varchar(50)
	,@cmd varchar(max)

set nocount on
set @last = ''

insert into @tmp
select isnull(si.SQLInstanceName, s.ServerName) 'ServerName', d.DBName
from teledb22.PomeroyMgmt.dbo.tbDatabase d
inner join teledb22.PomeroyMgmt.dbo.tbUARoleServer u on d.ServerID = u.ServerID and u.UARoleID = 1 
	and u.UserAccountID = 1 and u.IsPrimary = 1
inner join teledb22.PomeroyMgmt.dbo.tbServer s on d.ServerID = s.ServerID
left join teledb22.PomeroyMgmt.dbo.tbSQLInstance si on s.ServerID = si.ServerID
where d.IsSupported = 1 and d.IsOnline = 1
order by s.ServerName, d.DBName

while exists(select 1 from @tmp)
begin
	select top 1 @id = idx, @server = servername, @db = db from @tmp order by idx
	if (@server <> @last)
	begin
		--exec ('[' + @server + '].master..xp_fixeddrives')
		set @cmd = 'use [master]
			create table #DBInfo (ServerName varchar(100),DatabaseName varchar(100),FileSizeMB varchar(20),
			LogicalFileName varchar(500),PhysicalFileName varchar(1000),Status varchar(100),
			Updateability varchar(100),RecoveryMode varchar(100),FreeSpaceMB varchar(20),FreeSpacePct varchar(7),
			MaxSize varchar(50),Growth varchar(50),PollDate datetime)
			declare @command varchar(5000)  
			set @command = ''''Use [' + '?' + ']
			select  
			@@servername
			,' + '''''' + '''''?''''' + '''''' + '
			,convert(varchar, cast(round(sysfiles.size/128.00, 2) as decimal(10,2))) + ' + '''''''''' + ' MB' + '''''''''' + '
			,sysfiles.name
			,sysfiles.filename
			,convert(varchar,DatabasePropertyEx(''''''''?'''''''',''''''''Status''''''''))
			,convert(varchar,DatabasePropertyEx(''''''''?'''''''',''''''''Updateability''''''''))
			,convert(varchar,DatabasePropertyEx(''''''''?'''''''',''''''''Recovery''''''''))
			,convert(varchar, cast(round(sysfiles.size/128.00 - fileproperty(sysfiles.name, ' + '''''''''' +  'SpaceUsed' + '''''''''' + ' )/128.00, 2) as decimal(10,2))) + ' + '''''''''' + ' MB' + '''''''''' + '
			,cast(100 * (cast(((sysfiles.size/128.00 - fileproperty(sysfiles.name, ' + '''''''''' + 'SpaceUsed' + '''''''''' + ' )/128.00)/(sysfiles.size/128.00)) AS decimal(4,2))) AS varchar(8)) + ' + '''''''''' + '%' + '''''''''' + '
			,case maxsize when -1 then ''''''''Unlimited'''''''' else convert(varchar, (convert(bigint, maxsize) * 8) / 1024) + ' + '''''''''' + ' MB' + '''''''''' + ' end
			,case status & 0x100000 when 0 then convert(varchar, (growth * 8) / 1024) + ' + '''''''''' + ' MB' + '''''''''' + ' else convert(varchar, growth) + ' + '''''''''' + '%' + '''''''''' + ' end
			,getdate()
			from dbo.sysfiles
			''''
			insert into #DBInfo
			exec sp_MSForEachDB @command
			select ServerName,LogicalFileName,FileSizeMB,FreeSpaceMB,FreeSpacePct,
			DatabaseName,RecoveryMode,PhysicalFileName,Status,Updateability,
			MaxSize,Growth from #DBInfo order by DatabaseName
			exec xp_fixeddrives'
		exec('exec [' + @server + '].master.dbo.sp_sqlexec ''' + @cmd + '''')
	end
--	print @db
	delete from @tmp where idx = @id
	set @last = @server
end

--DBCC execution completed. If DBCC printed error messages, contact your system administrator.