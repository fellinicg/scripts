declare @bakpath varchar(200)
declare @db varchar(200)
declare @bakfile varchar(200)

set @bakpath = ''
set @db = ''
set @bakfile = convert(varchar, current_timestamp, 112) + '_' + replace(convert(varchar, current_timestamp, 108), ':', '') + '_' + @db + '.bak'

exec('backup database [' + @db + '] to disk = ''' + @bakpath + @bakfile + '''')