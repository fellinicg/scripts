USE [msdb]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[upRemoteDBANotification]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[upRemoteDBANotification]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091112
-- ============================================================
CREATE PROCEDURE dbo.upRemoteDBANotification
	@recipients nvarchar(1000),
	@subject nvarchar(255),
	@body nvarchar(4000)
AS
BEGIN

	-- Declarations
	declare 
--		@profile nvarchar(128),
		@mailprofile nvarchar(128)

	-- Initialize settings
	set nocount on

	-- Initialize variables
--	set @profile = 'DBA'
	set	@mailprofile = 'Alerts'

	-- Replace \n with carriage return
	set @body = replace(@body, '\n', char(10))

	-- Send email
  EXEC msdb.dbo.sp_send_dbmail @profile_name = @mailprofile, @recipients = @recipients,
		@subject = @subject ,@body = @body, @importance = 'High'

END
