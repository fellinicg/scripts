/*****************************************************************************************
  This script is for SQL 2005 SP1+
  You will need to replace dba@YOURDOMAIN.com with an appropriate email address.
*****************************************************************************************/
USE [msdb]
GO
IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'spDBA_job_notification Test Job')
EXEC msdb.dbo.sp_delete_job @job_name=N'spDBA_job_notification Test Job', @delete_unused_schedule=1
GO


/** Create an Operator [DBA] ******/
IF NOT EXISTS(SELECT * FROM  msdb.dbo.sysoperators WHERE NAME='DBA') 
EXEC msdb.dbo.sp_add_operator @name=N'DBA', 
		@enabled=1, 
		@weekday_pager_start_time=90000, 
		@weekday_pager_end_time=180000, 
		@saturday_pager_start_time=90000, 
		@saturday_pager_end_time=180000, 
		@sunday_pager_start_time=90000, 
		@sunday_pager_end_time=180000, 
		@pager_days=0, 
		@email_address=N'dba@YOURDOMAIN.com', 
		@category_name=N'[Uncategorized]'

GO

/*************************************************************************************************************
Create: the SQL Job [Test_Database_Mail] 
        You can ignore the following message: "Warning: Non-existent step referenced by @on_fail_step_id."
*************************************************************************************************************/
BEGIN TRANSACTION
DECLARE @ReturnCode INT, @jobname VARCHAR(200), @stepname VARCHAR(200)

SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 05/11/2007 09:48:02 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
SET @jobname ='spDBA_job_notification Test Job'
IF NOT EXISTS(SELECT * FROM  msdb.dbo.sysjobs WHERE NAME=@jobname) 
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@jobname, 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'DBA', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback


/****** Object:  Step [Simple select]    Script Date: 05/11/2007 09:48:03 ******/
SET @stepname = 'Simple select'
IF NOT EXISTS (SELECT * FROM  msdb.dbo.sysjobs sj JOIN  msdb.dbo.sysjobsteps sjs ON sj.job_id =sjs.job_id WHERE sj.NAME=@jobname AND sjs.step_NAME=@stepname)
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=@stepname, 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=2, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'select job_id,originatin_server,name,descriptio from msdb.dbo.sysjobs', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

/****** Object:  Step [Failure Notification Detailed]    Script Date: 05/11/2007 09:48:03 ******/
SET @stepname = 'Failure Notification Detailed'
IF NOT EXISTS (SELECT * FROM  msdb.dbo.sysjobs sj JOIN  msdb.dbo.sysjobsteps sjs ON sj.job_id =sjs.job_id WHERE sj.NAME=@jobname AND sjs.step_NAME=@stepname)
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N, 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC spDBA_job_notification $(ESCAPE_NONE(JOBID))', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = '(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO 
