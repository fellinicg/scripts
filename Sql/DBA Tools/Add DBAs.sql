declare @dba table(login varchar(50))
declare @domain varchar(10)
declare @login varchar(50)

set @domain = 'PANYNJ\'

insert into @dba values(@domain + 'tfellini')
insert into @dba values(@domain + 'sdey')
insert into @dba values(@domain + 'clam')
insert into @dba values(@domain + 'mmeng')

while (select count(login) from @dba) > 0
begin
	select top 1 @login = login from @dba
	if  not exists (select * from sys.server_principals where name = @login)
		exec('create login [' + @login + '] from windows with default_database=[master], default_language=[us_english]')
	exec sys.sp_addsrvrolemember @loginame = @login, @rolename = N'sysadmin'
	delete from @dba where login = @login
end