declare
	@RestoreToDB varchar(100)
	,@BackupFile varchar(500)
	,@LogicalDB varchar(100)
	,@PhysicalDB varchar(500)
	,@LogicalLog varchar(100)
	,@PhysicalLog varchar(500)
	,@Recovery varchar(20)

set @RestoreToDB = 'NewDatabase'
set @BackupFile = 'C:\Documents and Settings\tfellini.PANYNJ\My Documents\MSSQL\Backup\TFPOMEROY\PomeroyMgmt\FULL\20100615_220001_TFPOMEROY_PomeroyMgmt_FULL.bak'
set @LogicalDB = 'PomeroyMgmt'
set @PhysicalDB = 'C:\Documents and Settings\tfellini.PANYNJ\My Documents\MSSQL\Data\NewDatabase.mdf'
set @LogicalLog = 'PomeroyMgmt_log'
set @PhysicalLog = 'C:\Documents and Settings\tfellini.PANYNJ\My Documents\MSSQL\Data\NewDatabase_Log.ldf'
set @Recovery = 'RECOVERY' -- NORECOVERY, RECOVERY, STANDBY

--RESTORE DATABASE @RestoreToDB
--FROM DISK = @BackupFile
--WITH
--MOVE @LogicalDB TO @PhysicalDB,
--MOVE @LogicalLog TO @PhysicalLog
--,NORECOVERY

--RESTORE LOG @RestoreToDB
--FROM DISK = 'C:\Documents and Settings\tfellini.PANYNJ\My Documents\MSSQL\Backup\TFPOMEROY\PomeroyMgmt\LOG\20100616_100001_TFPOMEROY_PomeroyMgmt_LOG.trn'
--WITH RECOVERY --NORECOVERY RECOVERY

--RESTORE LOG @RestoreToDB
--FROM DISK = 'C:\Documents and Settings\tfellini.PANYNJ\My Documents\MSSQL\Backup\TFPOMEROY\PomeroyMgmt\LOG\20100616_100001_TFPOMEROY_PomeroyMgmt_LOG.trn'
--WITH STOPAT = N'6/28/2007 4:01:45 PM' WITH RECOVERY