use PANYNJ

set nocount on

declare @tmp table (idx int identity(1,1), sql varchar(max))
--declare @rs table (idx int identity (1,1), tbl varchar(100), col varchar(500), ct bigint)
declare @rs int
declare @params nvarchar(500)
declare @rowid int
declare @sql nvarchar(max)
declare @mailprofile varchar(50)
declare @recipients varchar(max)
declare @subject varchar(50)
declare @body varchar(max)
declare @hack varchar(200)

set @hack = '''</title><style>'''
set @mailprofile = 'Alerts'
set @recipients = 'tfellini@panynj.gov'
set @subject = 'PANYNJ Site'
set @body = 'Suspicous text ' + @hack + ' found.'
set @params = N'@ct int output'

insert into @tmp
select 'select @ct = count(1) from ' + t.name + ' where charindex('+ @hack + ', ' + c.name + ') > 0' 
from sys.columns c
inner join sys.tables t on c.object_id = t.object_id
where c.system_type_id in (
35
,99
,167
,175
,231
,239
,241
)

while exists (select 1 from @tmp)
begin
	select @rowid = idx, @sql = sql from @tmp
	--print(@sql)
	exec sp_executesql @sql, @params, @ct = @rs output
	if @rs = 0
	begin
		exec msdb.dbo.sp_send_dbmail @profile_name = @mailprofile, @recipients = @recipients,
			@subject = @subject ,@body = @body, @importance = 'High'
		return
	end
	delete from @tmp where idx = @rowid
end