declare @endpoint nvarchar(100),
				@login nvarchar(100),
				@role nvarchar(100)
declare @portno int

set @endpoint = 'Mirroring'
set @portno = 7034
set @role = 'witness' --partner
set @role = 'partner'
set @login = 'PANYNJ\SQLServer_SVC'
set @login = 'PANYNJ\SVC-TSD-MSSQL-SERVER'

-- Drop endpoint
if  exists (select * from sys.endpoints e where e.name = @endpoint) 
exec ('drop endpoint ' + @endpoint)

-- Create endpoint
exec ('create endpoint ' + @endpoint + ' state = started as tcp 
	 (listener_port = ' + @portno + ') for database_mirroring (role=' + @role + ')')

-- Create login & grant connect rights
if  not exists (select * from sys.server_principals where name = @login)
exec ('create login [' + @login + '] from windows')
exec ('grant connect on endpoint::' + @endpoint + ' TO [' + @login + ']')

--===========================================================================
-- Witness
if  exists (select * from sys.endpoints e where e.name = @endpoint) 
exec ('drop endpoint ' + @endpoint)

exec ('create endpoint ' + @endpoint + ' state = started as tcp 
	 (listener_port = ' + @portno + ') for database_mirroring (role=witness)')

CREATE LOGIN [PANYNJ\SQLServer_SVC] FROM WINDOWS ;

GRANT CONNECT ON ENDPOINT::Mirroring TO [PANYNJ\SQLServer_SVC];

-- Mirror
IF  EXISTS (SELECT * FROM sys.endpoints e WHERE e.name = N'Mirroring') 
DROP ENDPOINT Mirroring

CREATE ENDPOINT Mirroring STATE = STARTED AS TCP ( LISTENER_PORT = 7023 ) FOR DATABASE_MIRRORING (ROLE=PARTNER)
    
ALTER DATABASE xxxx SET PARTNER OFF
ALTER DATABASE xxxx SET PARTNER ='TCP://Principle FQDN:PORT NO of Principle Endpoint'

-- Principle
IF  EXISTS (SELECT * FROM sys.endpoints e WHERE e.name = N'Mirroring') 
DROP ENDPOINT Mirroring

CREATE ENDPOINT Mirroring STATE = STARTED AS TCP ( LISTENER_PORT = 7022 ) FOR DATABASE_MIRRORING (ROLE=PARTNER)
    
ALTER DATABASE xxxx SET PARTNER ='TCP://Mirror FQDN:PORT NO of Mirror Endpoint'

-- High Performance Async
ALTER DATABASE xxxx SET SAFETY OFF

ALTER DATABASE xxxx SET WITNESS OFF
ALTER DATABASE xxxx SET WITNESS = 'TCP://Witness FQDN:PORT NO of Witness Endpoint'

--select * from sys.database_mirroring
--select * from sys.database_mirroring_witnesses
--select * from sys.database_mirroring_endpoints

--select * from sys.tcp_endpoints

--SELECT name, is_broker_enabled FROM sys.databases

--     ALTER DATABASE cur_lmrp SET PARTNER FAILOVER
--GO 

--     ALTER DATABASE PomeroyMgmt SET PARTNER FAILOVER

--ALTER DATABASE cur_lmrp SET ENABLE_BROKER
--ALTER DATABASE PomeroyMgmt SET ENABLE_BROKER

--SELECT * FROM DATABASE_MIRRORING_STATE_CHANGE WHERE State = 7 OR State = 8