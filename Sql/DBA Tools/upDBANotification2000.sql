USE master
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'dbo.upDBANotification2000') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upDBANotification2000
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091113
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091113
-- ============================================================
CREATE PROCEDURE dbo.upDBANotification2000
	@jobid uniqueidentifier
AS
BEGIN

	-- Declarations
	declare 
		@instid int,
		@stepid int,
		@jobname nvarchar(128),
		@stepname nvarchar(128),
		@severity nvarchar(50),
		@database nvarchar(128),
		@rundate nvarchar(20),
		@command nvarchar(4000),
		@message nvarchar(1024),
		@profile nvarchar(128),
		@recipients nvarchar(1000),
		@subject nvarchar(255),
		@body nvarchar(4000),
		@mailprofile nvarchar(128),
		@cmd nvarchar(4000)

	-- Initialize settings
	set nocount on

	-- Initialize variables
	select @instid = max(instance_id) from msdb.dbo.sysjobhistory 
		where job_id = @jobid and run_status = 0 and step_id <> 0
	select @stepid = step_id from msdb.dbo.sysjobhistory 
		where instance_id = @instid
	set @profile = 'DBA'
	set	@mailprofile = 'Alerts'

	-- Retrieve job failure info
	select 
		@jobname =  sj.[name]
		,@stepname = sjh.step_name
		,@severity = sjh.sql_severity
		,@database = sjs.database_name
		,@rundate = convert(varchar, convert(datetime, convert(varchar, sjh.run_date)), 110) + ' ' + 
								stuff(stuff(convert(varchar, sjh.run_time), 5, 0, ':'), 3, 0, ':') 
		,@command = sjs.command
		,@message = sjh.[message]
	from msdb.dbo.sysjobs sj
		left join msdb.dbo.sysjobhistory sjh on sj.job_id = sjh.job_id and sjh.instance_id = @instid
		left join msdb.dbo.sysjobsteps sjs on sj.job_id = sjs.job_id and sjs.step_id = @stepid
	where sj.job_id = @jobid

	-- Populate email recipients from sysoperators table
	select @recipients = 
		email_address + ';' + 
		case isnull(pager_address, '') when '' then '' else (pager_address + ';') end 
	from msdb.dbo.sysoperators
	where [name] = @profile

	-- Populate email subject
	set @subject =@jobname + ' FAILED on ' + @@servername

	-- Populate email body
	set @body = 
		'     JOB: ' + isnull(@jobname, '') + '\n' +
		'    STEP: ' + isnull(@stepname, '') + '\n' +
		'SEVERITY: ' + isnull(@severity, '') + '\n' +
		'DATABASE: ' + isnull(@database, '') + '\n' +
		'    DATE: ' + isnull(@rundate, '') + '\n' +
		' COMMAND: ' + isnull(@command, '') + '\n\n' +
		replace(isnull(@message, ''), '  ', '\n')

  -- Replace single quote with right single quote
	set @subject =  replace(@subject, char(39), char(146))
	set	@body = replace(@body, char(39), char(146))
  
	-- Send email
	set @cmd = 'cd \"Program Files"\"Microsoft SQL Server"\80\Tools\binn&osql -Spatcas09\sql2005 -Uremotemail -PSMTPemail02 -Q "'
	set @cmd = 'osql -Spatcas09\sql2005 -Uremotemail -PSMTPemail02 -Q "'
	set @cmd = @cmd + 'exec msdb.dbo.upRemoteDBANotification @recipients = ''' + @recipients 
		+ ''', @subject = ''' + @subject + ''', @body = ''' + @body + ''''
	set @cmd = @cmd + '"'

	exec master..xp_cmdshell @cmd

END