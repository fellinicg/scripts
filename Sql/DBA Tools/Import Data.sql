use Tools

insert into PomeroyMgmt.dbo.tbServer (ServerName, onPawanet, SupportBeginDate)
select distinct [Server Name], case [On PAWANET] when 'yes' then 1 else 0 end,
	current_timestamp
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=c:\temp\SLPomeroy.xls', 'select * from [Sheet1$]')
order by [Server Name]

--declare @import table 
--([SERVER] varchar(500),[DB Name] varchar(500),[DEPARTMENT] varchar(500),[APPLICATION] varchar(500),
--[COMMENTS 2008] varchar(500),[SUPPORT DATE] varchar(500),[CLIENT CONTACT] varchar(500),[LOCATION] varchar(500),
--[IP ADDRESS] varchar(500),[O/S] varchar(500),[SERVER MAKE/ MODEL] varchar(500),[DB VERSION] varchar(500),
--[Primary Pomeroy  DBA] varchar(500),[Backup Pomeroy DBA] varchar(500),[Pomeroy SA] varchar(500),
--[PA  Contact] varchar(500),[Vendor] varchar(500),[PA Dept CTM] varchar(500),[Backup Software] varchar(500),
--[FDR  Profile Name] varchar(500),[Director Installed?] varchar(500),[NOTES] varchar(500),[STATUS] varchar(500))
truncate table tbImport

insert into tbImport
select a.*, '', ''
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=c:\temp\import.xlsx', 'select * from [Sheet1$]') 
   as a

update tbImport set pawanet = b.pawnet, mon = b.monitored
from tbImport a 
left join (select distinct [server], pawnet, monitored
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=c:\temp\import.xlsx', 'select * from [Sheet3$]')) b
	on a.server = b.server

--declare @pawanet table (servername varchar(50),pawanet varchar(10), mon varchar(10))

--insert into @pawanet
--select distinct a.[server], a.pawnet, a.monitored
--from openrowset('Microsoft.ACE.OLEDB.12.0',
--	'Excel 12.0;Database=c:\temp\import.xlsx', 'select * from [Sheet3$]') 
--   as a
--order by a.[server]

--  Populate tbApplication
truncate table dbo.tbApplication
insert into dbo.tbApplication (Description)
select distinct [APPLICATION] from tbImport where [APPLICATION] is not null order by [APPLICATION]

--  Populate tbLocation
truncate table dbo.tbLocation
insert into dbo.tbLocation (ShortDesc, Description)
select distinct [location], [location] from tbImport order by [tbImport].[location]

--  Populate tbDepartment
truncate table dbo.tbDepartment
insert into dbo.tbDepartment (Description, CompanyID)
select distinct [department], 1 from tbImport order by department

--  Populate tbServer
truncate table dbo.tbServer
insert into dbo.tbServer (ServerName, LocationID, OnPawanet, Monitored)
select distinct a.[server], b.LocationID, case isnull(c.pawanet, 'YES') when 'yes' then 1 else 0 end
, isnull(upper(left(c.mon, 1)), 'R')
from tbImport a
left join dbo.tbLocation b on  a.[location] = b.ShortDesc
left join tbImport c on a.[server] = c.[server]
order by a.[server]

--  Populate tbDBApplication
truncate table dbo.tbDBApplication
insert into dbo.tbDBApplication (DatabaseID, ApplicationID)
select distinct a.DatabaseID, c.ApplicationID
from tbDatabase a 
left join (select distinct DBName, [APPLICATION]
					from tbImport where [application] is not null) b on a.DBName = b.DBName
left join dbo.tbApplication c on b.[application] = c.Description
where b.DBName is not null and b.[application] is not null

-- Populate tbDBPlatform
truncate table dbo.tbDBPlatform
declare @result char(1)
exec dbo.upAddDBPlatforms @result output

-- Populate tbDatabases
truncate table dbo.tbDatabase
exec dbo.upAddDatabases @result output

-- Update tbDatabase (DepartmentID)
update dbo.tbDatabase set DepartmentID = d.DepartmentID
from dbo.tbDatabase a
left join dbo.tbServer b on a.ServerID = b.ServerID
left join dbo.tbImport c on a.DBName = c.DBNAME and b.ServerName = c.server
left join dbo.tbDepartment d on c.Department = d.Description
where d.DepartmentID is not null

-- Update Server IPs
exec dbo.upUpdateServersIP

-- Populate dbo.tbDeploymentStatus
insert into dbo.tbDeploymentStatus (Description)
select distinct ISNULL(Deployment_Status, 'Unknown') ds
from ProjMgt.dbo.SERVER_GEN_DTL
order by ds