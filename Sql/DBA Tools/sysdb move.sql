-- Msdb
ALTER DATABASE msdb MODIFY FILE (NAME = MSDBData, FILENAME = 'G:\SystemDBs\PATH_APPS\msdbdata.mdf')
ALTER DATABASE msdb MODIFY FILE ( NAME = MSDBLog, FILENAME = 'G:\SystemDBs\PATH_APPS\msdblog.ldf')

--Tempdb
ALTER DATABASE tempdb MODIFY FILE (NAME = tempdev, FILENAME = 'F:\Tempdb\TSD_APPS\tempdb.mdf')
ALTER DATABASE tempdb MODIFY FILE ( NAME = templog, FILENAME = 'F:\Tempdb\TSD_APPS\templog.ldf')

-- mssqlsystemresource
ALTER DATABASE mssqlsystemresource MODIFY FILE (NAME = data, FILENAME = 'G:\SystemDBs\PATH_APPS\mssqlsystemresource.ldf')
ALTER DATABASE mssqlsystemresource MODIFY FILE (NAME = log, FILENAME = 'G:\SystemDBs\PATH_APPS\mssqlsystemresource.ldf')


-- -dE:\SystemDBs\Default\master.mdf;-eC:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\LOG\ERRORLOG;-lE:\SystemDBs\Default\mastlog.ldf

SELECT is_broker_enabled 
FROM sys.databases
WHERE name = N'msdb';

-- STOP SQL Server Agent
ALTER DATABASE msdb SET ENABLE_BROKER
-- START SQL Server Agent

-- In DOS
NET START MSSQLSERVER /f /T3608
sqlcmd
ALTER DATABASE mssqlsystemresource MODIFY FILE (NAME = data, FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Data\mssqlsystemresource.mdf')
ALTER DATABASE mssqlsystemresource MODIFY FILE (NAME = log, FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Data\mssqlsystemresource.ldf')
ALTER DATABASE mssqlsystemresource SET READ_ONLY