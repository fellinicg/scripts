-- Employee search
select top 20
	emp.FIRSTNAME
	,emp.LASTNAME
	,evt.EVDESCR
	,rdr.READERDESC
	,convert(varchar, dateadd(second, datediff(second, getdate(), getutcdate()) * -1, evts.EVENT_TIME_UTC), 100)
from dbo.EMP emp
inner join dbo.EVENTS evts on emp.ID = evts.EMPID
inner join dbo.EVENT evt on evts.EVENTID = evt.EVENTID
inner join dbo.READER rdr on evts.DEVID = rdr.READERID
where emp.LASTNAME = 'blumberg'
order by evts.EVENT_TIME_UTC desc

return;

-- Current day
select 
	emp.FIRSTNAME
	,emp.LASTNAME
	,evt.EVDESCR
	,rdr.READERDESC
	,convert(varchar, dateadd(hh, -4, evts.EVENT_TIME_UTC), 100)
from dbo.EMP emp
inner join dbo.EVENTS evts on emp.ID = evts.EMPID
inner join dbo.EVENT evt on evts.EVENTID = evt.EVENTID
inner join dbo.READER rdr on evts.DEVID = rdr.READERID
where evts.EVENT_TIME_UTC >= convert(varchar, current_timestamp, 101)
order by evts.EVENT_TIME_UTC desc