declare @FileVersion varchar(500), @ProcessorCount varchar(500), @PhysicalMemory varchar(500)

create table #tmp(idx int, name varchar(64), iv varchar(100), description varchar(500))

insert into #tmp
exec master..xp_msver

select @FileVersion = isnull(iv, description) from #tmp where name = 'FileVersion'
select @ProcessorCount = isnull(iv, description) from #tmp where name = 'ProcessorCount'
select @PhysicalMemory = isnull(iv, description) from #tmp where name = 'PhysicalMemory'

select 
serverproperty ('ServerName') 'Server Name'
,@ProcessorCount 'Processor Count'
,@PhysicalMemory + ' MB' 'Physical Memory'
,''
,''
,''
,left(@FileVersion, charindex('.', @FileVersion)-1) 'SQL Version'
,replace(convert(varchar, serverproperty ('Edition')), 'Edition', '') 'SQL Edition'
,serverproperty ('ProductLevel') 'Product Level'
,@FileVersion 'File Version'
--,serverproperty('ProductVersion') 'Product Version'
,serverproperty ('LicenseType') 'License Type'
,ISNULL(serverproperty('NumLicenses'), 'N/A') 'Num Licenses'
,case serverproperty ('IsClustered') when 0 then 'N' else 'Y' end 'Is Clustered'

drop table #tmp

select serverproperty('Edition') 'Edition', serverproperty('ProductLevel') 'Service Pack'
	,serverproperty('NumLicenses') '# of licenses'
exec master..xp_msver

insert into dbo.tbServerIBMDir (ServerID, IBMDirectorID, IBMDirDateCreated)
select ServerID, ibm.managed_obj_id, ibm.date_created 
from dbo.tbServer 
,teleas22.IBMDirector.dbo.TWG_MANAGED_OBJECT ibm
where LABEL like ServerName + '%'
order by ServerID