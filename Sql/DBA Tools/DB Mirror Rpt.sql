select 
@@servername 'Server',
d.name 'Database',
case role 
	when 0 then 'Principle' 
	when 1 then 'Mirror' 
	else 'Unknown' end 'Role', 
case status 
	when 0 then 'Suspended' 
	when 1 then 'Disconnected' 
	when 2 then 'Synchronizing'
	when 3 then 'Pending Failover' 
	when 4 then 'Synchronized'
	else 'Unknown' end 'Mirroring State', 
case witness_status 
	when 1 then 'Connected' 
	when 2 then 'Disconnected'
	else 'Unknown' end 'Witness Connection', 
--convert(varchar, local_time) 'Time Recorded',
local_time 'Time Recorded'
from msdb.dbo.dbm_monitor_data dbm
left join master.sys.databases d on dbm.database_id = d.database_id 
where status < 4 or witness_status <> 1
order by local_time, d.name