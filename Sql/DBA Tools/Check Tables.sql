set nocount on
declare @dbs table (idx int identity (1,1), db varchar(200), tbl varchar(500))
declare @db varchar(200), @tbl varchar(500), @idx int
insert into @dbs
select name, 'select top 1 a.name from [' + name + '].sys.tables a inner join sys.schemas b on a.schema_id = b.schema_id where b.name = ''dbo''' from sys.databases where state = 0

while exists(select 1 from @dbs)
begin
	declare @dbtbl table (tblname varchar(200))
	declare @tblname nvarchar(1000)
	select top 1 @db = db, @tbl = tbl, @idx = idx from @dbs order by idx
	select @db
	insert into @dbtbl
	exec (@tbl)
	select @tblname = 'select top 1 * from ' + @db + '.dbo.' + tblname from @dbtbl
	exec (@tblname)
	delete from @dbs where idx = @idx
end

