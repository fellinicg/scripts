use msdb

declare @OperatorName sysname
declare @CategoryName sysname
declare @AlertName varchar(500)
declare @IDs varchar(500)
declare @CurrID int
declare @CurrPos int

-- Defaults
set @OperatorName = N'DBA'
set @CategoryName = N'SQL Server Agent Alerts'

-- Add Alert Category if it does not exist
if not exists (select 1 
							from msdb.dbo.syscategories
              where category_class = 2  -- ALERT
              and category_type = 3
              and name = @CategoryName)
	exec dbo.sp_add_category @class = N'ALERT', @type = N'NONE', @name = @CategoryName;

-- Severity Alerts   
set @IDs = '7,19,20,21,22,23,24,25'

while (len(@IDs) > 0)
begin
	set @CurrPos = charindex(',', @IDs)
	set @CurrID = case @CurrPos when 0 then @IDs else replace(left(@IDs, @CurrPos), ',', '') end
	set @AlertName = 
		case @CurrID
			when 7 then @@SERVERNAME + N' Alert - Severity ' + convert(varchar, @CurrID) + ' Notification: Status Information'
			when 19 then @@SERVERNAME + N' Alert - Severity ' + convert(varchar, @CurrID) + ' Error: Fatal Error in Resource'
			when 20 then @@SERVERNAME + N' Alert - Severity ' + convert(varchar, @CurrID) + ' Error: Fatal Error in Current Process'
			when 21 then @@SERVERNAME + N' Alert - Severity ' + convert(varchar, @CurrID) + ' Error: Fatal Error in Database Process'
			when 22 then @@SERVERNAME + N' Alert - Severity ' + convert(varchar, @CurrID) + ' Error: Fatal Error Table Integrity Suspect'
			when 23 then @@SERVERNAME + N' Alert - Severity ' + convert(varchar, @CurrID) + ' Error: Fatal Error Database Integrity Suspect'
			when 24 then @@SERVERNAME + N' Alert - Severity ' + convert(varchar, @CurrID) + ' Error: Fatal Hardware Error'
			when 25 then @@SERVERNAME + N' Alert - Severity ' + convert(varchar, @CurrID) + ' Error: Fatal Error'
			else ''
		end

	-- Add alert if it does not exist
	if not exists (select name from msdb.dbo.sysalerts where name = @AlertName)
		 exec msdb.dbo.sp_add_alert @name = @AlertName, 
									 @message_id=0, @severity=@CurrID, @enabled=1, 
									 @delay_between_responses=900, @include_event_description_in=1,
									 @category_name = @CategoryName, 
									 @job_id=N'00000000-0000-0000-0000-000000000000';

	-- Add a notification if it does not exist
	if not exists(select 1 from dbo.sysalerts as sa inner join dbo.sysnotifications as sn 
								on sa.id = sn.alert_id where sa.name = @AlertName)
		 exec msdb.dbo.sp_add_notification @alert_name = @AlertName, @operator_name=@OperatorName, @notification_method = 1;

	set @IDs = case @CurrPos when 0 then '' else substring(@IDs, @CurrPos + 1, 500) end
end

-- Message ID Alerts   
set @IDs = '825'

while (len(@IDs) > 0)
begin
	set @CurrPos = charindex(',', @IDs)
	set @CurrID = case @CurrPos when 0 then @IDs else replace(left(@IDs, @CurrPos), ',', '') end
	set @AlertName = 
		case @CurrID
			when 825 then @@SERVERNAME + N' Alert - Error ' + convert(varchar, @CurrID) + ': Read-Retry Required'
			else ''
		end

	-- Add alert if it does not exist
	if not exists (select name from msdb.dbo.sysalerts where name = @AlertName)
		 exec msdb.dbo.sp_add_alert @name = @AlertName, 
									 @message_id=@CurrID, @severity=0, @enabled=1, 
									 @delay_between_responses=900, @include_event_description_in=1,
									 @category_name = @CategoryName, 
									 @job_id=N'00000000-0000-0000-0000-000000000000';

	-- Add a notification if it does not exist
	if not exists(select 1 from dbo.sysalerts as sa inner join dbo.sysnotifications as sn 
								on sa.id = sn.alert_id where sa.name = @AlertName)
		 exec msdb.dbo.sp_add_notification @alert_name = @AlertName, @operator_name=@OperatorName, @notification_method = 1;

	set @IDs = case @CurrPos when 0 then '' else substring(@IDs, @CurrPos + 1, 500) end
end