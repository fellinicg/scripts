USE Master
GO
sp_configure 'show advanced', 1;
go 
RECONFIGURE WITH OVERRIDE
GO 
sp_configure 'Database Mail XPs', 1;
RECONFIGURE WITH OVERRIDE
GO
DECLARE 
	@sMsg varchar(300)
	,@profile_name sysname
	,@domain varchar(100)
	,@account_name sysname
	,@SMTP_servername sysname
	,@email_address NVARCHAR(128)
	,@replyto_address NVARCHAR(128)
	,@display_name NVARCHAR(128)

-- Account information. Replace with the information for your account.		
	SET @account_name = 'Alerts';
	SET @domain = 'pacorp.panynj.gov'
	SET @SMTP_servername = 'pasmsrv'
	SET @email_address = 'sqlalerts@' + lower(@@servername) + '.' + @domain;
	SET @replyto_address = 'tfellini@panynj.gov'
  SET @display_name = 'SQL Alerts';


-- Profile name. Replace with the name for your profile       
	SET @profile_name = @account_name;

 
-- Verify the specified account and profile do not already exist.
IF EXISTS (SELECT * FROM msdb.dbo.sysmail_profile WHERE name = @profile_name)
BEGIN
  set @sMsg = 'The specified Database Mail profile (' + @profile_name + ') already exists.'
--  RAISERROR(@sMsg, 16, 1) ;
  GOTO done;
END;

IF EXISTS (SELECT * FROM msdb.dbo.sysmail_account WHERE name = @account_name )
BEGIN
 set @sMsg = 'The specified Database Mail account (' + @account_name + ') already exists.'
-- RAISERROR(@sMsg, 16, 1) ;
 GOTO done;
END;

-- Start a transaction before adding the account and the profile
BEGIN TRANSACTION ;

DECLARE @rv INT;

-- Add the account
EXECUTE @rv = msdb.dbo.sysmail_add_account_sp
    @account_name = @account_name,
    @email_address = @email_address,
    @display_name = @display_name,
    @replyto_address = @replyto_address,
		@description = @display_name,
    @mailserver_name = @SMTP_servername;

IF @rv<>0
BEGIN
	set @sMsg = 'Failed to create the specified Database Mail account (' + @account_name + ').'
	RAISERROR(@sMsg, 16, 1) ;
    GOTO done;
END

-- Add the profile
EXECUTE @rv=msdb.dbo.sysmail_add_profile_sp
    @profile_name = @profile_name , @description = @display_name 

IF @rv<>0
BEGIN
    set @sMsg = 'Failed to create the specified Database Mail profile (' + @profile_name + ').'    
	RAISERROR(@sMsg, 16, 1) ;
	ROLLBACK TRANSACTION;
    GOTO done;
END;

-- Associate the account with the profile.
EXECUTE @rv=msdb.dbo.sysmail_add_profileaccount_sp
    @profile_name = @profile_name,
    @account_name = @account_name,
    @sequence_number = 1 ;

IF @rv<>0
BEGIN	
	set @sMsg = 'Failed to associate the speficied profile with the specified account (' + @account_name + ').'	
	RAISERROR(@sMsg, 16, 1) ;    	
	ROLLBACK TRANSACTION;
    GOTO done;
END;

COMMIT TRANSACTION;

done:

if not exists ( select profile_id from msdb.dbo.sysmail_principalprofile where  [profile_id] in (select profile_id from msdb.dbo.sysmail_profile where name =  @profile_name ) )
    EXECUTE msdb.dbo.sysmail_add_principalprofile_sp  @principal_name = 'public',  @profile_name = @profile_name,  @is_default = 1 ;

EXECUTE msdb.dbo.sysmail_start_sp ;
