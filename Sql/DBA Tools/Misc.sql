sp_change_users_login 'update_one', 'opalev_dbuser', 'opalev_dbuser'

EXEC master.dbo.upDBANotification $(ESCAPE_NONE(JOBID)), $(ESCAPE_NONE(STRTDT)), $(ESCAPE_NONE(STRTTM))

use master

declare @cmd varchar(8000)

set @cmd = 'cd \"Program Files"\"Microsoft SQL Server"\80\Tools\binn&osql -Spatcas09\sql2005 -Uremotemail -PSMTPemail02 -Q "'
set @cmd = @cmd + 'exec msdb.dbo.upRemoteDBANotification @recipients = ''tfellini@panynj.gov'', @subject = ''DBA Alert'', @body = ''SQL Server Alert Test #4'''
set @cmd = @cmd + '"'

exec master..xp_cmdshell @cmd

declare
	@Today varchar(8)

select @Today = convert(varchar, datepart(yyyy, current_timestamp)) + 
	convert(varchar, datepart(mm, current_timestamp)) +
	convert(varchar, datepart(dd, current_timestamp))

select
	sj.name, 
	sjh.step_name, 
	sjh.message, 
	convert(varchar, convert(smalldatetime, convert(varchar, convert(datetime, convert(varchar, sjh.run_date)), 110) + ' ' + 
		reverse(stuff(stuff(reverse(case convert(varchar, sjh.run_time) when '0' then '000000' else convert(varchar, sjh.run_time) end), 5, 0, ':'), 3, 0, ':'))), 109) 'StartDate', 
	convert(varchar,
		dateadd(ss, (sjh.run_duration % 10000) %100,
			dateadd(mi, (sjh.run_duration % 10000) / 100, 
				dateadd(hh, sjh.run_duration / 10000 * 3600, 
					convert(datetime, 
						convert(varchar, convert(datetime, convert(varchar, sjh.run_date)), 110)
									+ ' ' 
									+ reverse(stuff(stuff(reverse(case convert(varchar, sjh.run_time) when '0' then '000000' else convert(varchar, sjh.run_time) end), 5, 0, ':'), 3, 0, ':')
						)
					)
				)
			)
		), 109) 'EndDate'
from msdb.dbo.sysjobhistory sjh
inner join msdb.dbo.sysjobs sj on sjh.job_id = sj.job_id
where sjh.run_date = @Today
order by sjh.job_id, sjh.instance_id


select top 10 *
from msdb.dbo.sysjobs

select count(*)
from msdb.dbo.sysjobhistory

select *
from msdb.dbo.sysjobactivity

	select 
		sj.[name]
		,sjh.step_name
		,sjh.sql_severity
		,sjs.database_name
		,convert(varchar, convert(datetime, convert(varchar, sjh.run_date)), 110) + ' ' + 
								stuff(stuff(convert(varchar, sjh.run_time), 5, 0, ':'), 3, 0, ':') 
		,sjs.command
		,sjh.[message]
	from msdb.dbo.sysjobs sj
		left join msdb.dbo.sysjobhistory sjh on sj.job_id = sjh.job_id and sjh.instance_id = @instid
		left join msdb.dbo.sysjobsteps sjs on sj.job_id = sjs.job_id and sjs.step_id = @stepid
	where sj.job_id = @jobid

select @@servername --+ char(10) + char(10) + @@version

select name from dbo.sysdatabases order by name
--
exec sp_helpdb CALS_EWR_ARCH
exec sp_helpdb WTCAccessControl
--exec sp_helpdb WTCMobileManagement

SELECT 'insert into dbo.tbOS (OSID, ShortDesc, Description, IsDeleted) ' +
	'values(''' + convert(varchar, OSID) + ''',''' + ShortDesc + ''',''' + Description + 
	''',''' + convert(varchar, IsDeleted) +
	''')'
  FROM Tools.dbo.tbOS



--select a.ServerName, b.Description, c.Description, d.FirstName + ' ' + d.LastName,
--e.FirstName + ' ' + e.LastName, f.FirstName + ' ' + f.LastName, g.Description
--from dbo.tbServer a
--left join dbo.tbOS b on a.OSID = b.OSID
--left join dbo.tbDBPlatform c on a.DBPlatformID = c.DBPlatformID
--left join dbo.tbUser d on a.PrimaryDBA = d.UserID
--left join dbo.tbUser e on a.SecondaryDBA = e.UserID
--left join dbo.tbUser f on a.ServerAdmin = f.UserID
--left join dbo.tbLocation g on a.LocationID = g.LocationID

insert into tbsqlinstance (SQLInstanceName, ServerID)
select 
case servername when 'PATCAS09' then 'PATCAS09/SQL2005' else servername end
,serverid
from tbserver

SELECT a.*
FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0', 
   'Excel 8.0;Database=c:\temp\import.xls', 'SELECT * FROM [Sheet1$]') 
   AS a


SELECT a.*
FROM OPENROWSET('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=c:\temp\import.xlsx', 'SELECT * FROM [Sheet1$]') 
   AS a

exec master..xp_fileexist 'c:\temp\import.xlsx'

select distinct 
PrimaryPomeroyDBA
from tbImport
union
select distinct 
BackupPomeroyDBA
from tbImport
union
select distinct 
PomeroySA
from tbImport
union
select distinct 
PAContact
from tbImport
union
select distinct 
CLIENTCONTACT
from tbImport

select dbo.Function1('patcdb01')

--EXEC sp_configure 'CLR ENABLED' , '1'
--GO
--RECONFIGURE
--GO
--
--ALTER DATABASE Tools SET TRUSTWORTHY ON

exec teledb05.master..xp_fixeddrives 

DBCC LOGINFO('SC_DB06') 

DBCC SQLPERF(logspace)

DBCC OPENTRAN

SELECT [MANAGED_OBJ_ID]
      ,[IP_INDEX]
      ,[IP_ADDRESS]
      ,[IP_HOSTNAME]
      ,[IP_DOMAIN]
      ,[SUBNET_MASK]
      ,[NAMESERVER1]
      ,[NAMESERVER2]
      ,[DEFAULT_GATEWAY]
  FROM [IBMDirector].[dbo].[TWG_IP_ADDRESS]

select ip_Address
from dbo.twg_ip_address
group by ip_address
having count(ip_Address) > 1

SELECT [MANAGED_OBJ_ID]
      ,[LABEL]
      ,[DATE_CREATED]
      ,[FIRST_ATTEMPT]
      ,[LAST_ATTEMPT]
      ,[LAST_UPDATE]
      ,[STATE]
      ,[LAST_STAT_UPDT]
  FROM [IBMDirector].[dbo].[TWG_MANAGED_OBJECT]

SELECT [MANAGED_OBJ_ID]
      ,[COMPUTER_NAME]
      ,[USERNAME]
      ,[DOMAIN]
      ,[WORKGROUP]
      ,[REG_OWNER]
      ,[REG_ORGANIZATION]
      ,[REG_PRODUCT_ID]
  FROM [IBMDirector].[dbo].[TWG_WINDOWS_NETWORK_ID]

set statistics time on
select * from dbo.vwSrvDbSupported
select * from dbo.tfSrvDbSupported()
set statistics time off

-- GRANT
declare @t table (idx int identity(1,1), schem varchar(5), sp varchar(100))
declare @idx int, @schem varchar(5), @sp varchar(100)

insert into @t (schem, sp)
SELECT ROUTINE_SCHEMA, ROUTINE_NAME
FROM INFORMATION_SCHEMA.ROUTINES 
WHERE ROUTINE_NAME NOT LIKE 'dt_%'
AND ROUTINE_TYPE = 'PROCEDURE'

while exists(select 1 from @t)
begin
	select top 1 @idx=idx, @schem=schem, @sp=sp from @t
	exec ('grant exec on [' + @schem + '].[' + @sp + '] to system')
	delete from @t where idx=@idx
end

--select * 
--from sys.database_permissions a
--inner join sys.objects b on a.major_id = b.object_id
--where a.grantee_principal_id = 7

select a.name, b.*
from sys.objects a
left join sys.database_permissions b on a.object_id = b.major_id
where a.type = 'P'

select a.name, a.create_date, a.modify_date, b.name, b.create_date, b.modify_date
from PomeroyMgmt.sys.objects a
inner join PomeroyMgmtPROD.sys.objects b on a.name = b.name and a.modify_date > b.modify_date
where a.type in ('P','TF','FN','U')
order by a.create_date desc

select a.name, a.create_date, a.modify_date, b.name, b.create_date, b.modify_date
from PomeroyMgmt.sys.objects a
left join PomeroyMgmtPROD.sys.objects b on a.name = b.name
where a.type in ('P','TF','FN','U')
 and b.name is null
 order by a.name
 
 
 DECLARE @DBInfo TABLE  
( ServerName VARCHAR(100),  
DatabaseName VARCHAR(100),  
FileSizeMB INT,  
LogicalFileName sysname,  
PhysicalFileName NVARCHAR(520),  
Status sysname,  
Updateability sysname,  
RecoveryMode sysname,  
FreeSpaceMB INT,  
FreeSpacePct VARCHAR(7),  
FreeSpacePages INT,  
PollDate datetime)  

DECLARE @command VARCHAR(5000)  

SELECT @command = 'Use [' + '?' + '] SELECT  
@@servername as ServerName,  
' + '''' + '?' + '''' + ' AS DatabaseName,  
CAST(sysfiles.size/128.0 AS int) AS FileSize,  
sysfiles.name AS LogicalFileName, sysfiles.filename AS PhysicalFileName,  
CONVERT(sysname,DatabasePropertyEx(''?'',''Status'')) AS Status,  
CONVERT(sysname,DatabasePropertyEx(''?'',''Updateability'')) AS Updateability,  
CONVERT(sysname,DatabasePropertyEx(''?'',''Recovery'')) AS RecoveryMode,  
CAST(sysfiles.size/128.0 - CAST(FILEPROPERTY(sysfiles.name, ' + '''' +  
       'SpaceUsed' + '''' + ' ) AS int)/128.0 AS int) AS FreeSpaceMB,  
CAST(100 * (CAST (((sysfiles.size/128.0 -CAST(FILEPROPERTY(sysfiles.name,  
' + '''' + 'SpaceUsed' + '''' + ' ) AS int)/128.0)/(sysfiles.size/128.0))  
AS decimal(4,2))) AS varchar(8)) + ' + '''' + '%' + '''' + ' AS FreeSpacePct,  
GETDATE() as PollDate FROM dbo.sysfiles'  
INSERT INTO @DBInfo  
   (ServerName,  
   DatabaseName,  
   FileSizeMB,  
   LogicalFileName,  
   PhysicalFileName,  
   Status,  
   Updateability,  
   RecoveryMode,  
   FreeSpaceMB,  
   FreeSpacePct,  
   PollDate)  
EXEC sp_MSForEachDB @command  

SELECT  
   ServerName,  
   DatabaseName,  
   FileSizeMB,  
   LogicalFileName,  
   PhysicalFileName,  
   Status,  
   Updateability,  
   RecoveryMode,  
   FreeSpaceMB,  
   FreeSpacePct,  
   PollDate  
FROM @DBInfo  
ORDER BY  
   ServerName,  
   DatabaseName  
   
   
use INCAPS_DEV 

DBCC LOGINFO('INCAPS_DEV') 

DBCC SQLPERF(logspace)

DBCC OPENTRAN

backup log INCAPS_DEV with truncate_only 

dbcc shrinkfile (INCAPS_log, 1)
   