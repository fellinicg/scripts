use msdb

set nocount on

if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.sfOFN') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfOFN
go
create function dbo.sfOFN (@ofn varchar(max))
	returns varchar(max) as
begin
	set @ofn = reverse(@ofn)
	set @ofn = reverse(left(@ofn, charindex('\', @ofn)-1))
	set @ofn = left(@ofn, charindex('_$', @ofn)+1)
	return(@ofn)
end
go

declare @tmp table (idx int identity(1,1), cmd varchar(max))
declare @exc varchar(max)

insert into @tmp
select 'exec msdb.dbo.sp_update_jobstep @job_id=' + char(39) + convert(varchar(max), js.job_id) + char(39) + ', @step_id=' + convert(varchar, step_id) +
	', @output_file_name=' + char(39) + replace(output_file_name, dbo.sfOFN(output_file_name), j.name + '_$') + char(39)
from msdb.dbo.sysjobsteps js
inner join msdb.dbo.sysjobs j on js.job_id = j.job_id
where output_file_name like '%$(ESCAPE_SQUOTE(STRTTM)).txt'

select j.name, js.step_name, js.output_file_name
from msdb.dbo.sysjobsteps js
inner join msdb.dbo.sysjobs j on js.job_id = j.job_id
where output_file_name is not null

while exists(select 1 from @tmp)
begin
	select top 1 @exc = cmd from @tmp order by idx
	exec (@exc)
	delete from @tmp where idx = (select top 1 idx from @tmp order by idx)
end

select j.name, js.step_name, js.output_file_name
from msdb.dbo.sysjobsteps js
inner join msdb.dbo.sysjobs j on js.job_id = j.job_id
where output_file_name is not null

if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.sfOFN') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfOFN

set nocount off