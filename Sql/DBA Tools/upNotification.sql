USE master
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upNotification') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upNotification
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20110728
-- ============================================================
CREATE PROCEDURE dbo.upNotification
	@jobid uniqueidentifier
	,@rundt varchar(8) = null
	,@runtime varchar(6) = null
	,@recipients nvarchar(max)
AS
BEGIN

	-- Declarations
	declare 
		@instid int,
		@stepid int,
		@jobname nvarchar(128),
		@stepname nvarchar(128),
		@severity nvarchar(50),
		@database nvarchar(128),
		@rundate nvarchar(20),
		@command nvarchar(max),
		@message nvarchar(1024),
		@profile nvarchar(128),
		@subject nvarchar(255),
		@body nvarchar(max),
		@mailprofile nvarchar(128)

	-- Initialize settings
	set nocount on

	-- Initialize variables
	select @instid = max(instance_id) from msdb.dbo.sysjobhistory 
		where job_id = @jobid and run_date = isnull(@rundt, run_date) and run_time = isnull(@runtime, run_time)
			and step_id <> 0 and run_status = 0
	select @stepid = step_id from msdb.dbo.sysjobhistory 
		where instance_id = @instid
	set @profile = 'DBA'
	set	@mailprofile = 'Alerts'

	-- Retrieve job failure info
	select 
		@jobname = sj.[name]
		,@stepname = sjh.step_name
		,@severity = sjh.sql_severity
		,@database = sjs.database_name
		,@rundate = convert(varchar, convert(datetime, convert(varchar, sjh.run_date)), 110) + ' ' + 
								stuff(stuff(convert(varchar, sjh.run_time), 5, 0, ':'), 3, 0, ':') 
		,@command = sjs.command
		,@message = sjh.[message]
	from msdb.dbo.sysjobs sj
		left join msdb.dbo.sysjobhistory sjh on sj.job_id = sjh.job_id and sjh.instance_id = @instid
		left join msdb.dbo.sysjobsteps sjs on sj.job_id = sjs.job_id and sjs.step_id = @stepid
	where sj.job_id = @jobid

	-- Populate email subject
	select @subject = @jobname + ' ' + case when @stepname is null then 'COMPLETED' else 'FAILED' end + ' on ' + @@servername

	-- Populate email body
	set @body = 
		'     JOB: ' + isnull(@jobname, '') + char(10) +
		'    STEP: ' + isnull(@stepname, '') + char(10) +
		'SEVERITY: ' + isnull(@severity, '') + char(10) +
		'DATABASE: ' + isnull(@database, '') + char(10) +
		'    DATE: ' + isnull(@rundate, '') + char(10) +
		' COMMAND: ' + isnull(@command, '') + char(10) + char(10) +
		replace(isnull(@message, ''), '    ', char(10))

	-- Send email
  EXEC msdb.dbo.sp_send_dbmail @profile_name = @mailprofile, @recipients = @recipients,
		@subject = @subject ,@body = @body, @importance = 'High'

END