set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.tfCommaListToTable') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.tfCommaListToTable
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091119
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091119
-- ============================================================
create function dbo.tfCommaListToTable (@CommaDelimitedList varchar(8000))
returns @ResultTable table (ListItem varchar(120) not null)
as
begin

	-- Declarations
	declare
		@TokenLen int,
		@Token varchar(120),
		@Remainder varchar(8000)

	-- Initialize variables
	set @Remainder = @CommaDelimitedList

	while len(@Remainder) > 0
	begin
		set @TokenLen = case patindex('%,%', @Remainder) 
										when 0 then len(@Remainder) 
										else patindex('%,%', @Remainder) - 1 end
		set @Token = RTRIM(LTRIM(left(@Remainder, @TokenLen)))
		insert @ResultTable(ListItem) values(@Token)
		set @Remainder = right(@Remainder, len(@Remainder) - @TokenLen)
		if (left(@Remainder, 1) = ',')
			set @Remainder = right(@Remainder, len(@Remainder) - 1)
	end

	return

end