use Temp

declare 
	@share varchar(max),
	@file varchar(max),
	@tab varchar(max),
	@openrowset varchar(max)

set @file = 'Servers 2012 Aug.xlsx'
set @share = '\\teledsrv1\DATA\TSD\SHARE\Documentation Library\Services\Server Support\Server and Printer Lists\Server Inventory\'
set @openrowset = '''Microsoft.ACE.OLEDB.12.0'', ''Excel 12.0;Database=' + @share + @file + ''', '

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAll') AND type in (N'U'))
drop table dbo.tbAll

set @tab = 'All'
exec ('select * into tbAll from openrowset(' + @openrowset + '''select * from [' + @tab + '$] where [Support Date] is not null'')')
update dbo.tbAll set [Server Name] = rtrim(replace(convert(varchar, [Server Name]), char(160), ''))
 
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPhysical') AND type in (N'U'))
drop table dbo.tbPhysical

set @tab = 'Physical'
exec ('select * into tbPhysical from openrowset(' + @openrowset + '''select * from [' + @tab + '$] where [Support Date] is not null'')')
update dbo.tbPhysical set [Server Name] = rtrim(replace(convert(varchar, [Server Name]), char(160), ''))
 
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbVirtual') AND type in (N'U'))
drop table dbo.tbVirtual

set @tab = 'Virtual'
exec ('select * into tbVirtual from openrowset(' + @openrowset + '''select * from [' + @tab + '$] where [Server Name] is not null'')')
update dbo.tbVirtual set [Server Name] = rtrim(replace(convert(varchar, [Server Name]), char(160), ''))
 
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbNew') AND type in (N'U'))
drop table dbo.tbNew

set @tab = 'All - New on Bottom'
exec ('select * into tbNew from openrowset(' + @openrowset + '''select * from [' + @tab + '$] where [Support Date] is not null'')')
update dbo.tbNew set [Server Name] = rtrim(replace(convert(varchar, [Server Name]), char(160), ''))

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbDecomm') AND type in (N'U'))
drop table dbo.tbDecomm

set @tab = 'Decomm'
exec ('select * into tbDecomm from openrowset(' + @openrowset + '''select * from [' + @tab + '$]'')')
update dbo.tbDecomm set [Server Name] = rtrim(replace(convert(varchar, [Server Name]), char(160), '')) 
