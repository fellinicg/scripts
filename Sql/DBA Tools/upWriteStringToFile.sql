USE master
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (select * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upWriteStringToFile') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upWriteStringToFile
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091113
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091113
-- ============================================================
CREATE PROCEDURE dbo.upWriteStringToFile
	@String varchar(max)
	,@Filename varchar(400)
AS
BEGIN

	-- Declarations
	declare
		@FS int
		,@FileID int
		,@hr int

	-- Initialize settings
	set nocount on

	-- Initialize variables

	-- Create object
	exec @hr = sp_OACreate 'Scripting.FileSystemObject', @FS out

	-- Open file
	exec @hr = sp_OAMethod @FS, 'OpenTextFile', @FileID OUT, @FileName, 8, 2

	-- Write to file
	exec @hr = sp_OAMethod @FileID, 'WriteLine', null, @String

	-- Close file and destroy object
	exec @hr = sp_OADestroy @FileID
	exec @hr = sp_OADestroy @FS

END
