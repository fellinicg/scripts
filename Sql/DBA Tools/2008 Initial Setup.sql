declare @drive char(1)

select @drive = left(physical_name, 1)
from sys.master_files
where name = 'master'

exec ('alter database msdb modify file (NAME = MSDBData, filename = ''' + @drive + ':\SystemDBs\msdbdata.mdf'')')
exec ('alter database msdb modify file ( NAME = MSDBLog, filename = ''' + @drive + ':\SystemDBs\msdblog.ldf'')')

alter login sa enable 
go
alter login sa with NAME = comanche
go
alter login comanche disable

declare @dba table(login varchar(50))
declare @domain varchar(10)
declare @login varchar(50)

set @domain = 'PANYNJ\'

insert into @dba values(@domain + 'tfellini')
insert into @dba values(@domain + 'sdey')
insert into @dba values(@domain + 'clam')
insert into @dba values(@domain + 'mmeng')

while (select count(login) from @dba) > 0
begin
	select top 1 @login = login from @dba
	if  not exists (select * from sys.server_principals where name = @login)
		exec('create login [' + @login + '] from windows with default_database=[master], default_language=[us_english]')
	exec sys.sp_addsrvrolemember @loginame = @login, @rolename = N'sysadmin'
	delete from @dba where login = @login
end

exec sys.sp_configure 'backup compression default', '1'
go
reconfigure with override
go
sp_configure 'show advanced options', 1;
go
reconfigure;
go
sp_configure 'min server memory', 1024;
go
sp_configure 'max server memory', 3584;
go
reconfigure;
go

select name, physical_name from sys.master_files
select @@servername