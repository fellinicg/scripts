declare @tbl table (idx int identity(1,1), obj sysname, stat sysname)
declare @ObjectName sysname
declare @StatsName sysname
declare @idx int

insert into @tbl
select object_name(object_id) as 'ObjectName', [name] as 'StatsName' FROM sys.stats
where (indexproperty(object_id, [name], 'IsAutoStatistics') = 1 OR indexproperty(object_id, [name], 'IsStatistics') = 1)
and objectproperty(object_id, 'IsMSShipped') = 0

while exists (select 1 from @tbl)
begin
	select top 1  @idx = idx, @ObjectName = quotename(obj), @StatsName = quotename(stat) from @tbl
	exec ('drop statistics ' + @ObjectName + '.' + @StatsName)
	delete from @tbl where idx = @idx
end