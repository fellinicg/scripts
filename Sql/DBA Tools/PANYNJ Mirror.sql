--TELE Outbound

create master key encryption by password = '%2FJpnJwWuX&Pbs%CcWC';
go

use master;
create certificate teledb21_cert 
   with subject = 'TELEDB21 Certificate';
go

create endpoint mirroring
   state = started
   as tcp (
      listener_port=7024
      , listener_ip = all
   ) 
   for database_mirroring ( 
      authentication = certificate teledb21_cert
      , encryption = required algorithm aes
      , role = all
   );
go

backup certificate teledb21_cert to file = 'c:\teledb21_cert.cer';
go

--TELE Inbound

use master
go

create login patcdb10_mirror with password = 'SHkE*-VSPrm+dtT56Vjt';
go

create user patcdb10_mirror for login patcdb10_mirror;
go

create certificate patcdb10_cert
   authorization patcdb10_mirror
   from file = 'c:\patcdb10_cert.cer'
go

grant connect on endpoint::mirroring to patcdb10_mirror;
go

--PATC Outbound
create master key encryption by password = '%2FJpnJwWuX&Pbs%CcWC';
go

use master;
create certificate patcdb10_cert 
   with subject = 'PATCDB10 Certificate';
go

create endpoint mirroring
   state = started
   as tcp (
      listener_port=7024
      , listener_ip = all
   ) 
   for database_mirroring ( 
      authentication = certificate patcdb10_cert
      , encryption = required algorithm aes
      , role = all
   );
go

backup certificate patcdb10_cert to file = 'c:\patcdb10_cert.cer';
go

--PATC Inbound
use master
go

create login teledb21_mirror with password = 'SHkE*-VSPrm+dtT56Vjt';
go

create user teledb21_mirror for login teledb21_mirror;
go

create certificate teledb21_cert
   authorization teledb21_mirror
   from file = 'c:\teledb21_cert.cer'
go

grant connect on endpoint::mirroring to teledb21_mirror;
go

--PATC
alter database PANYNJ2 set partner = 'tcp://teledb21:7024';

--TELE
alter database PANYNJ2 set partner = 'tcp://patcdb10:7024';