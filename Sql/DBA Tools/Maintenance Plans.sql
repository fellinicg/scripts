if charindex('Microsoft SQL Server 2005', @@version) > 0
	begin

		select @@servername, a.name, a.create_date, a.owner, b.name, b.date_modified, '', 
		case b.enabled when 1 then 'Y' else 'N' end
		from msdb.dbo.sysmaintplan_plans a
		left join msdb.dbo.sysjobs_view b on rtrim(a.name) = left(b.name, len(rtrim(a.name)))

	end
else
	begin

		select @@servername, b.plan_name, b.date_created, b.owner, d.name, d.date_modified, a.database_name, 
		case d.enabled when 1 then 'Y' else 'N' end
		from msdb.dbo.sysdbmaintplan_databases a
		left join msdb.dbo.sysdbmaintplans b on a.plan_id = b.plan_id
		left join msdb.dbo.sysdbmaintplan_jobs c on a.plan_id = c.plan_id
		left join msdb.dbo.sysjobs d on c.job_id = d.job_id
		order by b.plan_name, d.name, a.database_name

	end