set nocount on

declare @name varchar(64)
declare @offline bit

if object_id('tempdb..#tmp') is not null
	drop table #tmp

create table #tmp(name varchar(64), offline bit default 0)

if charindex('Microsoft SQL Server 2005', @@version) > 0
	begin

		insert into #tmp
		select name, case state when 6 then 1 end from master.sys.databases
		where name not in ('master','tempdb','model','msdb')
		order by name

		while exists(select 1 from #tmp)
		begin
			select top 1 @name = name, @offline = offline from #tmp order by name
			if (@offline = 1)
				select @@servername, @name, 'Offline'
			else
				exec ('select @@servername, ''' + @name + '''
							,name
							,physical_name ''filename''
							,convert(varchar, size * 8) + '' KB'' ''size''
							,case max_size when -1 then ''Unlimited'' else convert(varchar, convert(bigint, max_size) * 8) + '' KB'' end ''maxsize''
							,case is_percent_growth when 1 then convert(varchar, growth) + ''%'' else convert(varchar, growth * 8) + '' KB'' end ''growth''
							from [' + @name + '].sys.database_files')
			delete from #tmp where name = @name
		end

	end
else
	begin

		insert into #tmp
		select name, case version when 0 then 1 end from master.dbo.sysdatabases
		where name not in ('master','tempdb','model','msdb')
		order by name

		while exists(select 1 from #tmp)
		begin
			select top 1 @name = name, @offline = offline from #tmp order by name
			if (@offline = 1)
				select @@servername, @name, 'Offline'
			else
				exec ('select @@servername, ''' + @name + '''
							,name
							,filename
							,convert(varchar, size * 8) + '' KB'' ''size''
							,case maxsize when -1 then ''Unlimited'' else convert(varchar, convert(bigint, maxsize) * 8) + '' KB'' end ''maxsize''
							,case status & 0x100000 when 0 then convert(varchar, growth * 8) + '' KB'' else convert(varchar, growth) + ''%'' end ''growth''
							from [' + @name + '].dbo.sysfiles')
			delete from #tmp where name = @name

		end

	end

drop table #tmp
