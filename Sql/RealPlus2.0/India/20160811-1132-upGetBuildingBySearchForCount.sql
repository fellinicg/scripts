USE [Jaguar_v2_Prototype2_BHS_2]
GO

/****** Object:  StoredProcedure [dbo].[upGetBuildingBySearchForCount]    Script Date: 8/11/2016 11:18:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Maunish Shah
-- OrigCreateDate: 20160712
-- Purpose: Get the count on buildings selection in search criteria screen
-- Modified on: 20160727
-- ============================================================
CREATE procedure [dbo].[upGetBuildingBySearchForCount]	
	@AddressOrBuildingName varchar(max) = NULL
	, @UnitIdentifier varchar(max) =  null
	, @GridNorth1 int = null  --integrated
	, @GridNorth2 int = null  --integrated
	, @GridEast1 int = null   --integrated
	, @GridEast2 int = null   --integrated
	, @GridWest1 int = null   --integrated
	, @GridWest2 int = null   --integrated
	, @GridSouth1 int = null  --integrated
	, @GridSouth2 int = null  --integrated
	, @AreaOrNeighborhood varchar(max) = null  
	, @Zipcode varchar(max) = null --integrated
	, @OwnershipType varchar(max) = null  
	, @BuildingPeriod varchar(max) = NULL
	, @BuildingType varchar(max) = null --integrated
	, @AttendedLobby varchar(max) = null  --integrated
	, @BuildingAmenity varchar(max) = null --integrated
	, @BuildingAllow varchar(max) = null  --integrated
	, @BuildingNotAllow varchar(max) = null	 --integrated
	, @ManagedBuilding varchar(max) = null 
	, @ManagingAgent varchar(max) = null	 
	, @MinYearBuilt int = NULL --integrated
	, @MapSelectedShapes varchar(max) = NULL	 --integrated
	, @circleRadius varchar(max) = null --integrated
	, @shouldReturnList bit = 0 -- flag to identify whether to return the count or listingids column
	, @agensavedcriteriaid int = 0 -- internal for development
	, @AgentID  int = null-- internal for development
	, @Detail varchar(200) = null-- internal for development
	, @CustomerID int = null-- internal for development
as
begin


	SELECT  @AddressOrBuildingName =CASE WHEN LTRIM(@AddressOrBuildingName) = ''  THEN NULL ELSE  @AddressOrBuildingName END,
			@UnitIdentifier =CASE WHEN LTRIM(@UnitIdentifier) = ''  THEN NULL ELSE  @UnitIdentifier END,
			@GridNorth1 =CASE WHEN LTRIM(@GridNorth1) = 0  THEN NULL ELSE  @GridNorth1 END,
			@GridNorth2 =CASE WHEN LTRIM(@GridNorth2) = 0  THEN NULL ELSE  @GridNorth2 END,
			@GridEast1 =CASE WHEN LTRIM(@GridEast1) = 0  THEN NULL ELSE  @GridEast1 END,
			@GridEast2 =CASE WHEN LTRIM(@GridEast2) = 0  THEN NULL ELSE  @GridEast2 END,
			@GridWest1 =CASE WHEN LTRIM(@GridWest1) = 0  THEN NULL ELSE  @GridWest1 END,
			@GridWest2 =CASE WHEN LTRIM(@GridWest2) = 0  THEN NULL ELSE  @GridWest2 END,
			@GridSouth1 =CASE WHEN LTRIM(@GridSouth1) = 0  THEN NULL ELSE  @GridSouth1 END,
			@GridSouth2 =CASE WHEN LTRIM(@GridSouth2) = 0  THEN NULL ELSE  @GridSouth2 END,
			@AreaOrNeighborhood =CASE WHEN LTRIM(@AreaOrNeighborhood) = ''  THEN NULL ELSE  @AreaOrNeighborhood END,
			@Zipcode =CASE WHEN LTRIM(@Zipcode) = ''  THEN NULL ELSE  @Zipcode END,
			@OwnershipType=CASE WHEN LTRIM(@OwnershipType) = ''  THEN NULL ELSE  @OwnershipType END,			
			@BuildingPeriod=CASE WHEN LTRIM(@BuildingPeriod) = ''  THEN NULL ELSE  @BuildingPeriod END,			
			@BuildingType=CASE WHEN LTRIM(@BuildingType) = ''  THEN NULL ELSE  @BuildingType END,			
			@AttendedLobby=CASE WHEN LTRIM(@AttendedLobby) = ''  THEN NULL ELSE  @AttendedLobby END,			
			@BuildingAmenity=CASE WHEN LTRIM(@BuildingAmenity) = ''  THEN NULL ELSE  @BuildingAmenity END,			
			@BuildingAllow=CASE WHEN LTRIM(@BuildingAllow) = ''  THEN NULL ELSE  @BuildingAllow END,
			@BuildingNotAllow=CASE WHEN LTRIM(@BuildingNotAllow) = ''  THEN NULL ELSE  @BuildingNotAllow END,			
			@ManagedBuilding=CASE WHEN LTRIM(@ManagedBuilding) = ''  THEN NULL ELSE  @ManagedBuilding END,			
			@ManagingAgent=CASE WHEN LTRIM(@ManagingAgent) = ''  THEN NULL ELSE  @ManagingAgent END,			
			@MinYearBuilt=CASE WHEN LTRIM(@MinYearBuilt) = 0  THEN NULL ELSE  @MinYearBuilt END
			
	SET FMTONLY OFF
	-- Initialize settings
	set nocount on

	

	-- Declarations
	declare @sql varchar(max) = ''
	declare @TblResult table (PropertyID int index ix1 clustered)
	DECLARE @WorkInProgress XML
	declare @propertyCount int = 0

	-- Grid
	if (@GridNorth1 is not null or @GridSouth1 is not null or @GridEast1 is not null or @GridWest1 is not null)
		begin
			set @sql =  @sql + '
						(
						select distinct pan.PropertyID
						from (
								select distinct PropertyID
								from tbPropertyAttributeNumeric
								where AttributeID = 1211 and AttributeValue <= ' + cast(isnull(@GridNorth1, 1000000) as varchar(10)) + '
								intersect
								select distinct PropertyID
								from tbPropertyAttributeNumeric
								where AttributeID = 1212 and AttributeValue >= ' + cast(isnull(@GridSouth1, 0) as varchar(10)) + '
								intersect
								select distinct PropertyID
								from tbPropertyAttributeNumeric
								where AttributeID = 1213 and AttributeValue <= ' + cast(isnull(@GridEast1, 1000000) as varchar(10)) + '
								intersect
								select distinct PropertyID
								from dbo.tbPropertyAttributeNumeric
								where AttributeID = 1214 and AttributeValue >= ' + cast(isnull(@GridWest1, 0) as varchar(10)) + '
							) pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID							
							inner join properties ep on p.EtlID = ep.propertyid
						where p.ParentPropertyID is null'
			if (@GridNorth2 is not null or @GridSouth2 is not null or @GridEast2 is not null or @GridWest2 is not null)
				set @sql = @sql + '
						union
						select distinct pan.PropertyID
						from (
								select distinct PropertyID
								from tbPropertyAttributeNumeric
								where AttributeID = 1211 and AttributeValue <= ' + cast(isnull(@GridNorth2, 1000000) as varchar(10)) + '
								intersect
								select distinct PropertyID
								from tbPropertyAttributeNumeric
								where AttributeID = 1212 and AttributeValue >= ' + cast(isnull(@GridSouth2, 0) as varchar(10)) + '
								intersect
								select distinct PropertyID
								from tbPropertyAttributeNumeric
								where AttributeID = 1213 and AttributeValue <= ' + cast(isnull(@GridEast2, 1000000) as varchar(10)) + '
								intersect
								select distinct PropertyID
								from dbo.tbPropertyAttributeNumeric
								where AttributeID = 1214 and AttributeValue >= ' + cast(isnull(@GridWest2, 0) as varchar(10)) + '
							) pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID							
							inner join properties ep on p.EtlID = ep.propertyid
						where p.ParentPropertyID is null'
			set @sql = @sql + ') intersect'
		end

	-- AddressID
	if @AddressOrBuildingName is not null 
		begin
			
			set @sql =  @sql + '
						(
						select distinct pa.propertyId
						from dbo.properties pa
							inner join dbo.tfcommalisttotable(' + char(39) + @AddressOrBuildingName + char(39) + ') a on pa.rp_bin = a.ListItem
							inner join dbo.tbProperty p on pa.propertyId = p.EtlID
							inner join properties ep on p.EtlID = ep.propertyid
						where p.ParentPropertyID is null
						) intersect'
		end

	-- UnitIdentifier
	if @UnitIdentifier is not null 
		begin
			set @sql = @sql + '
						(
						select distinct pat.PropertyID
						from dbo.tbPropertyAttributeText pat 
							inner join dbo.tfCommaListToTable(' + char(39) + @UnitIdentifier + char(39) + ') on AttributeValue = ListItem
							inner join dbo.tbProperty p on pat.propertyId = p.EtlID
							inner join properties ep on p.EtlID = ep.propertyid
						where p.ParentPropertyID is null
							and pat.AttributeID in (100)
						) intersect'
		end
	-- Neighborhood
	if @AreaOrNeighborhood is not null 
		begin
			set @sql = @sql + '
						(
						select distinct pa.PropertyID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @AreaOrNeighborhood + char(39) + ') t on pa.AttributeValueID = t.ListItem	
							--inner join dbo.tbProperty p on pa.PropertyID = p.PropertyID							
							inner join properties ep on p.EtlID = ep.propertyid
							--where p.ParentPropertyID is null
						) intersect'
		end

		-- Zipcode
	if @Zipcode is not null 
		begin
			set @sql = @sql + '
						(
						select distinct pat.PropertyID
						from dbo.tbPropertyAttributeText pat 
							inner join dbo.tfCommaListToTable(' + char(39) + @Zipcode + char(39) + ') on AttributeValue = ListItem
							--inner join dbo.tbProperty p on pa.PropertyID = p.PropertyID							
							inner join properties ep on p.EtlID = ep.propertyid
							--where p.ParentPropertyID is null and 
							where pat.AttributeID in (1259)
						) intersect'
		end

-- OwnershipType
	if @OwnershipType is not null 
		begin
			
			set @sql = @sql + '
						(select  ep.propertyid
							from dbo.tbProperty p
							inner join properties ep on p.EtlID = ep.propertyid
							inner join dbo.tbPropertyAttribute pa on pa.PropertyID = p.PropertyID
							inner join dbo.tfCommaListToTable(' + char(39) + @OwnershipType + char(39) + ') t on pa.AttributeValueID = t.ListItem
							where p.ParentPropertyID is null
						) intersect'
			
		end


		-- BuildingPeriod
	if @BuildingPeriod is not null
		begin
			set @sql = @sql + '
						(select  ep.propertyid
							from dbo.tbProperty p
							inner join properties ep on p.EtlID = ep.propertyid
							inner join dbo.tbPropertyAttribute pa on pa.PropertyID = p.PropertyID
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingPeriod + char(39) + ') t on pa.AttributeValueID = t.ListItem
							where p.ParentPropertyID is null
						) intersect'
		end
-- BuildingType
	if @BuildingType is not null
		begin
			set @sql = @sql + '
						(select  ep.propertyid
							from dbo.tbProperty p
							inner join properties ep on p.EtlID = ep.propertyid
							inner join dbo.tbPropertyAttribute pa on pa.PropertyID = p.PropertyID
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingType + char(39) + ') t on pa.AttributeValueID = t.ListItem
							where p.ParentPropertyID is null
						) intersect'
		end

		-- BuildingPersonnel
	if @AttendedLobby is not null
		begin
			set @sql = @sql + '
						(select  ep.propertyid
							from dbo.tbProperty p
							inner join properties ep on p.EtlID = ep.propertyid
							inner join dbo.tbPropertyAttribute pa on pa.PropertyID = p.PropertyID
							inner join dbo.tfCommaListToTable(' + char(39) + @AttendedLobby + char(39) + ') t on pa.AttributeValueID = t.ListItem
							where p.ParentPropertyID is null
						) intersect'
		end

-- BuildingAmenity
	if @BuildingAmenity is not null
		begin
			set @sql = @sql + '
						(select  ep.propertyid
							from dbo.tbProperty p
							inner join properties ep on p.EtlID = ep.propertyid
							inner join dbo.tbPropertyAttribute pa on pa.PropertyID = p.PropertyID
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingAmenity + char(39) + ') t on pa.AttributeValueID = t.ListItem
							where p.ParentPropertyID is null
						) intersect'
		end

	-- BuildingAllow
	if @BuildingAllow is not null
		begin
			set @sql = @sql + '
						(select  ep.propertyid
							from dbo.tbProperty p
							inner join properties ep on p.EtlID = ep.propertyid
							inner join dbo.tbPropertyAttribute pa on pa.PropertyID = p.PropertyID
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingAllow + char(39) + ') t on pa.AttributeValueID = t.ListItem
							where p.ParentPropertyID is null
						) intersect'
		end

	-- ManagedBuildings
	if @ManagedBuilding is not null
		begin
			set @sql = @sql + '
						(select  ep.propertyid
							from dbo.tbProperty p
							inner join properties ep on p.EtlID = ep.propertyid
							inner join dbo.tbPropertyAttribute pa on pa.PropertyID = p.PropertyID
							inner join dbo.tfCommaListToTable(' + char(39) + @ManagedBuilding + char(39) + ') t on pa.AttributeValueID = t.ListItem
							where p.ParentPropertyID is null
						) intersect'
		end

	-- BuildingNotAllow
	if @BuildingNotAllow is not null
		begin
			set @sql = @sql + '
						(select  ep.propertyid
							from dbo.tbProperty p
							inner join properties ep on p.EtlID = ep.propertyid
							inner join dbo.tbPropertyAttribute pa on pa.PropertyID = p.PropertyID
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingNotAllow + char(39) + ') t on pa.AttributeValueID = t.ListItem
							where p.ParentPropertyID is null
						) intersect'
		end


		-- ManagingAgent
	if @ManagingAgent is not null 
		begin
			set @sql = @sql + '
						(
						select distinct pat.PropertyID
						from dbo.tbPropertyAttributeText pat 
							--inner join dbo.tbProperty p on pa.PropertyID = p.PropertyID							
							inner join properties ep on p.EtlID = ep.propertyid
						where pat.IsDeleted = 0 and --p.ParentPropertyID is null and 
							and pat.AttributeID = 1184
							and charindex(' + char(39) + @ManagingAgent + char(39) + ', AttributeValue) > 0
						) intersect'
		end

		-- MinYearBuilt
	if @MinYearBuilt is not null 
		begin
			set @sql = @sql + '
						(
						select distinct pan.PropertyID
						from dbo.tbPropertyAttributeNumeric pan
							--inner join dbo.tbProperty p on pa.PropertyID = p.PropertyID							
							inner join properties ep on p.EtlID = ep.propertyid
						where pan.IsDeleted = 0  and --p.ParentPropertyID is null and 							
							and pan.AttributeID = 106 
							and AttributeValue >= ' + cast(@MinYearBuilt as varchar) + '						
						) intersect'
		end


		-- Remove last intersect if it exists
	if right(@sql, 9) = 'intersect' set @sql = left(@sql, len(@sql) - 9)
	
	-- Check if 0 criteria got selected (BAD)
	if isnull(@sql,'') = ''
		begin
			-- Execute final sql statement to insert PropertyIDs into table variable			
			insert into @tblresult
			select top 1000 l.propertyid from dbo.vwbuildingbysearch l				
		end
	else
		begin
			-- Execute final sql statement to insert PropertyIDs into table variable
			--	print @sql
			insert into @tblresult
			exec(@sql)
		END

---- End of new code by nirav 
--declare @tblresultForNRecordsOnly table (PropertyID int index ix1 clustered)
--insert into @tblresultForNRecordsOnly
--select top 1000* from @tblresult

--DECLARE @MapSelectedShapes varchar(max)
--SET @MapSelectedShapes= '-74.00115966796875,40.77469185382073;-74.00115966796875,40.78762523112725;-73.95421028137207,40.78762523112725;-73.95421028137207,40.77469185382073;-74.00115966796875,40.77469185382073;:-73.97884368896484,40.792758888618756;-73.97884368896484,40.792758888618756;-73.97772789001463,40.78372598556362;-73.97772789001463,40.78372598556362;-73.94116401672363,40.784505852991366;-73.94116401672363,40.784505852991366;-73.97103309631348,40.79236900450889;-73.97103309631348,40.79236900450889;-73.97232055664062,40.786845400329625;-73.97232055664062,40.786845400329625;-73.97884368896484,40.792758888618756;:'
--SET @MapSelectedShapes= ''
IF ((@MapSelectedShapes IS NOT null) AND (@MapSelectedShapes <> ''))
	BEGIN
			declare @tblcircleRadius table (indexnumber int IDENTITY(1,1) index ix1 clustered,radiusvalues decimal(30,15))
			declare @tblresult_copy table (indexnumber int IDENTITY(1,1) index ix1 clustered,PropertyID int index ix1PropertyID nonclustered, latitude decimal(18,15) , longitude decimal(18,15)  )
			declare @tblresultFull_copy table (indexnumber int IDENTITY(1,1) index ix1 clustered,PropertyID int index ix1PropertyID nonclustered, latitude decimal(18,15) , longitude decimal(18,15))
			declare @tblresult_distinct table (indexnumber int IDENTITY(1,1) index ix1 clustered,PropertyID int index ix1PropertyID nonclustered)

			INSERT INTO @tblresult_copy (PropertyID,latitude,longitude)			
			select P.PropertyID, bba.Latitude, bba.Longitude 	
			from dbo.tbProperty p 
			inner join dbo.properties bbp on p.EtlID = bbp.propertyid
			inner join dbo.addresses bba on bbp.addressid = bba.addressid
			inner join @tblresult T on t.PropertyID = p.PropertyID
			where p.IsDeleted = 0 and PropertyClassID = 1

			if (@circleRadius <> '' and (@circleRadius is not null))
			begin
			INSERT INTO @tblcircleRadius
			SELECT ListItem FROM [dbo].[tfDelimeterListToTable](@circleRadius,';')
			end

			
			declare @tblresult_AfterMap table (PropertyID int index ix1 clustered)
			declare @tblresult_circle table (PropertyID int index ix1 clustered)
			declare @tblresult_line table (PropertyID int index ix1 clustered)
			declare @tblresult_copydistinct table (PropertyID int index ix1 clustered)
			
			
			declare @shapes varchar(max)
			set @shapes = @MapSelectedShapes			
			declare @tblMapShapes table (indexnumber int index ix1 clustered , shapecoordinates varchar(max), isforCircle bit null, isforLine bit null)
			
			INSERT INTO @tblMapShapes
			SELECT indexnumber, ListItem,null,null  FROM [dbo].[tfDelimeterListToTable](@shapes,':')	
			
			DELETE FROM @tblMapShapes WHERE [@tblMapShapes].shapecoordinates = ':'
			
			declare @shapeCount int
			set @shapeCount = (SELECT count(*) FROM @tblMapShapes)
						
			declare @shapeCounter int = 0
			declare @shapeNumber int= 0
			declare @shapestr varchar(max)
			
			declare @tblMapShapeCoordinate table (indexnumber int  index ix1 clustered, shapecoordinates varchar(max))
			
			-- for indentifying the polygon, square, rectangle, triangle, circle, line	   
			IF (@shapeCount > 0)
			  BEGIN
				WHILE (@shapeCounter < @shapeCount)
				   BEGIN			
						DELETE FROM @tblMapShapeCoordinate
						
						set @shapestr = (SELECT shapecoordinates FROM @tblMapShapes tms WHERE tms.indexnumber = @shapeCounter + 1)		
						
						DECLARE @shapestrForCircle int

						SET @shapestrForCircle = LEN(@shapestr) - LEN(REPLACE(@shapestr, ';', ''))

						IF (@shapestrForCircle = 1)
						BEGIN
								-- print 'shapestr' + @shapestr + ' - is for circle'
								UPDATE @tblMapShapes SET isforCircle = 1 WHERE [@tblMapShapes].indexnumber = @shapeCounter + 1
						END

						IF (@shapestrForCircle = 2)
						BEGIN
								-- print 'shapestr' + @shapestr + ' - is for line'
								UPDATE @tblMapShapes SET isforLine = 1 WHERE [@tblMapShapes].indexnumber = @shapeCounter + 1
						END

						SET @shapeCounter = @shapeCounter + 1
				   END
			  end

			SET @shapeCounter = 0			   
			
			-- for polygon, square, rectangle, triangle	   
			IF (@shapeCount > 0)
			  BEGIN
				WHILE (@shapeCounter < @shapeCount)
				   BEGIN	
						DELETE FROM @tblMapShapeCoordinate
						
						set @shapestr = (SELECT shapecoordinates FROM @tblMapShapes tms WHERE tms.indexnumber = @shapeCounter + 1 AND tms.isforCircle IS NULL AND isforline IS null)														
							
						set @shapestr = replace (@shapestr, ',',' ')
						set @shapestr = replace (@shapestr, ';',',')
						set @shapestr = replace (@shapestr, ':','')
						set @shapestr = 'POLYGON((' + @shapestr + '))'

						set @shapestr  = replace(@shapestr, ',))','))' )
												
						-- print '*******************************************************************************************************'
						insert INTO @tblMapShapeCoordinate
						SELECT indexnumber, ListItem FROM [dbo].[tfDelimeterListToTable](@shapestr,';')									
						DELETE FROM @tblMapShapeCoordinate WHERE shapecoordinates = ';'		
						UPDATE @tblMapShapeCoordinate SET shapecoordinates = shapecoordinates + ';'	
						
						DECLARE @g geography;
						DECLARE @h geography;
						--SET @g = geography::STGeomFromText('POLYGON((-73.98267924785614 40.73544168255703,-73.98311913013458 40.734852286139734,-73.98084461688995 40.7338848517358, -73.9804208278656 40.73447832156747, -73.98267924785614 40.73544168255703))', 4326);
						SET @g = geography::STGeomFromText(@shapestr, 4326);

						
						insert into @tblresult_AfterMap
						SELECT rc.PropertyID
						FROM @tblresult_copy rc 
						where (@g.STIntersects(geography::Point(rc.latitude, rc.longitude , 4326))) = 1			   		
						
					set @shapeCounter = @shapeCounter + 1
				END	
			  end
			-- for circle
			IF (@shapeCount > 0)
			  BEGIN
				  DECLARE @circleshapescount int = 0
				  DECLARE @circleshapescounter int = 0
				  DECLARE @coordinateltforcircle  decimal(18,15)
				  DECLARE @coordinatelgforcircle  decimal(18,15)
				  DECLARE @coordinatesforcircle varchar(max)
				  DECLARE @dist int
				  SET @dist=1000
				  SET @circleshapescount = (SELECT count(*) FROM @tblMapShapes WHERE isforCircle = 1)
			  
				  DECLARE @radiuscount int
				  SET @radiuscount = (SELECT count(*) FROM @tblcircleRadius tr)

				  declare @tblMapCircle table (indexnumber int IDENTITY(1,1) , shapecoordinates varchar(max))
				  INSERT INTO @tblMapCircle
				  SELECT shapecoordinates FROM @tblMapShapes WHERE isforCircle = 1 AND isforLine IS null
			  			  
				  WHILE (@circleshapescounter < @circleshapescount)
				   BEGIN
						-- print 'inside begin for circle'						
						
						IF (@radiuscount > 0)
						BEGIN
							SET @dist=(SELECT cast(radiusvalues AS decimal(30,15)) FROM @tblcircleRadius tcr WHERE tcr.indexnumber = @circleshapescounter + 1) 
							-- print @dist
						end


						SET @dist=(SELECT cast(radiusvalues AS decimal(30,15)) FROM @tblcircleRadius tcr WHERE tcr.indexnumber = @circleshapescounter + 1) 
						-- print @dist

		  			    SET @coordinatesforcircle = (SELECT shapecoordinates FROM @tblMapCircle tms WHERE tms.indexnumber = @circleshapescounter + 1)
										
						SET @coordinatesforcircle = REPLACE(@coordinatesforcircle,';','')		
				
						SET @coordinateltforcircle = cast(substring(@coordinatesforcircle,charindex(',',@coordinatesforcircle)+1,len(@coordinatesforcircle)) AS decimal(18,15))
						SET @coordinatelgforcircle = substring(@coordinatesforcircle,1,charindex(',',@coordinatesforcircle)-1)
												
						declare @tmp table (PropertyID int, distance float)					
							insert into @tmp
						SELECT PropertyID,isnull((((acos(sin((@coordinateltforcircle*pi()/180)) * sin((dest.latitude*pi()/180))+cos((@coordinateltforcircle*pi()/180))*cos((dest.latitude*pi()/180))*cos(((@coordinatelgforcircle-dest.longitude)*pi()/180))))*180/pi())*60*1.1515*1609.344),0) as distance						
						from @tblresult_copy AS dest 

						insert INTO @tblresult_circle
						SELECT DISTINCT PropertyID FROM @tmp WHERE distance < @dist

						SET @circleshapescounter = @circleshapescounter + 1
				   END
			  end
		
			-- for line
			IF (@shapeCount > 0)
			  BEGIN
				  DECLARE @lineshapescount int = 0
				  DECLARE @lineshapescounter int = 0
				  DECLARE @coordinateltforline  decimal(18,15)
				  DECLARE @coordinatelgforline  decimal(18,15)
				  DECLARE @coordinatesforline varchar(max)
				  DECLARE @coordinatesforlinep1 varchar(max)
				  DECLARE @coordinateltforlinep1  decimal(18,15)
				  DECLARE @coordinatelgforlinep1  decimal(18,15)
				  DECLARE @coordinatesforlinep2 varchar(max)
				  DECLARE @coordinateltforlinep2  decimal(18,15)
				  DECLARE @coordinatelgforlinep2  decimal(18,15)
				  DECLARE @shapelinestr varchar(max)
				  DECLARE @bottomleftlt decimal(18,15)
				  DECLARE @bottomleftlg decimal(18,15)
				  DECLARE @topleftlt decimal(18,15)
				  DECLARE @topleftlg decimal(18,15)
				  DECLARE @toprightlt decimal(18,15)
				  DECLARE @toprightlg decimal(18,15)				  
				  DECLARE @bottomrightlt decimal(18,15)
				  DECLARE @bottomrightlg decimal(18,15)

				  DECLARE @radius int
				  SET @radius=1000

				  SET @lineshapescount = (SELECT count(*) FROM @tblMapShapes WHERE isforLine = 1)
			  
				  declare @tblMapLine table (indexnumber int IDENTITY(1,1) , shapecoordinates varchar(max))
				  INSERT INTO @tblMapLine
				  SELECT shapecoordinates FROM @tblMapShapes WHERE isforLine = 1 AND isforCircle IS null
			  	
				  WHILE (@lineshapescounter < @lineshapescount)
				   BEGIN
						-- print 'inside begin for line'												
		  			    SET @coordinatesforline = (SELECT shapecoordinates FROM @tblMapline tms WHERE tms.indexnumber = @lineshapescounter + 1)						
						SET @coordinatesforline = substring(@coordinatesforline, 1, (len(@coordinatesforline) - 1))
						-- print @coordinatesforline
						SET @coordinatesforlinep1 = substring(@coordinatesforline,1,charindex(';',@coordinatesforline)-1)
						SET @coordinatesforlinep2 = substring(@coordinatesforline,charindex(';',@coordinatesforline)+1,len(@coordinatesforline))
						
						SET @coordinatesforlinep1 = REPLACE(@coordinatesforlinep1,';','')
						SET @coordinateltforlinep1 = cast(substring(@coordinatesforlinep1,charindex(',',@coordinatesforlinep1)+1,len(@coordinatesforlinep1)) AS decimal(18,15))
						SET @coordinatelgforlinep1 = substring(@coordinatesforlinep1,1,charindex(',',@coordinatesforlinep1)-1)

						SET @coordinatesforlinep2 = REPLACE(@coordinatesforlinep2,';','')
						SET @coordinateltforlinep2 = cast(substring(@coordinatesforlinep2,charindex(',',@coordinatesforlinep2)+1,len(@coordinatesforlinep2)) AS decimal(18,15))
						SET @coordinatelgforlinep2 = substring(@coordinatesforlinep2,1,charindex(',',@coordinatesforlinep2)-1)

						-- logic to create 4 sets of latlongs from 2 line points. i.e BottomLeft TopLeft TopRight BottomRight

						DECLARE @startltp1 decimal(18,15)
						DECLARE @startlgp1 decimal(18,15)
						DECLARE @startltp2 decimal(18,15)
						DECLARE @startlgp2 decimal(18,15)
						SET @startltp1 = @coordinateltforlinep1
						SET @startlgp1 = @coordinatelgforlinep1
						SET @startltp2 = @coordinateltforlinep2
						SET @startlgp2 = @coordinatelgforlinep2
						DECLARE @dx decimal(18,15)
						DECLARE @dy decimal(18,15)
						DECLARE @deltaltp1 decimal(18,15)
						DECLARE @deltalgp1 decimal(18,15)
						DECLARE @deltaltp2 decimal(18,15)
						DECLARE @deltalgp2 decimal(18,15)
						DECLARE @finalltp1 decimal(18,15)
						DECLARE @finallgp1 decimal(18,15)
						DECLARE @finalltp2 decimal(18,15)
						DECLARE @finallgp2 decimal(18,15)
						DECLARE @R int
						SET @R = 30.48 -- 1Ft = 0.3048 m .. we need 100Ft
						DECLARE @theta decimal(18,15)
						SET @theta = 135

						SET @dx = @R*cos(@theta)
						SET @dy = @R*sin(@theta)

						SET @deltalgp1 = @dx/(111320*cos(degrees(41.88592)))
						SET @deltaltp1 = @dy/110540

						SET @deltalgp2 = @dx/(111320*cos(degrees(41.88592)))
						SET @deltaltp2 = @dy/110540

						SET @finallgp1 = @startlgp1 + @deltalgp1
						SET @finalltp1 = @startltp1 + @deltaltp1

						SET @finallgp2 = @startlgp2 + @deltalgp2
						SET @finalltp2 = @startltp2 + @deltaltp2


						-- logic to create 4 sets ends here

						DELETE FROM @tblMapShapeCoordinate
						
						set @shapelinestr = cast(@finallgp1 AS varchar) + ',' + cast(@finalltp1 AS varchar) + ';' + cast(@finallgp2 AS varchar) + ',' + cast(@finalltp2 AS varchar)
						DECLARE @g_line geography;
						DECLARE @h_line geography;
						--SET @g = geography::STGeomFromText('POLYGON((-73.98267924785614 40.73544168255703,-73.98311913013458 40.734852286139734,-73.98084461688995 40.7338848517358, -73.9804208278656 40.73447832156747, -73.98267924785614 40.73544168255703))', 4326);
						SET @g_line = geography::STGeomFromText(@shapelinestr, 4326);

						
						insert into @tblresult_line
						SELECT rc.PropertyID
						FROM @tblresult_copy rc 
						where (@g_line.STIntersects(geography::Point(rc.latitude, rc.longitude , 4326))) = 1
						
						SET @lineshapescounter = @lineshapescounter + 1
				   END
			  end			
		
			INSERT INTO @tblresult_copydistinct
			SELECT DISTINCT PropertyID FROM @tblresult_AfterMap

			INSERT INTO @tblresult_copydistinct
			SELECT DISTINCT PropertyID FROM @tblresult_circle

			INSERT INTO @tblresult_copydistinct
			SELECT DISTINCT PropertyID FROM @tblresult_line
			
			declare @tblFinalOutput table (PropertyID int)
			insert INTO @tblFinalOutput
			select distinct PropertyID from @tblresult_copydistinct

			-- Return data
			if (@shouldReturnList = 1)
			begin
				
				select @WorkInProgress = ISNULL((
									SELECT top 1000 property.propertyid as id, 
									0 checked, 
									0 removed
									from dbo.tbProperty property
									left join dbo.tbPropertyAddress pa on property.PropertyID = pa.PropertyAddressID and pa.IsPrimary = 1
									left join  dbo.tbPostalAddress a on pa.PostalAddressID = a.PostalAddressID
									left join dbo.vwPropertyAttributeNumeric pan on property.PropertyID = pan.PropertyID
									left join dbo.vwPropertyAttributeText pat on property.PropertyID = pat.PropertyID
									inner join @tblFinalOutput r on property.propertyid = r.propertyid									
									FOR XML AUTO, BINARY BASE64,root('wip')) , '')				
				
			end
			else
			begin

			set @propertyCount  = (select count(distinct
				p.PropertyID) as propertyCount
		
			from @tblFinalOutput r
				inner join dbo.tbProperty p on r.PropertyID = p.PropertyID
				left join dbo.tbPropertyAddress pa on p.PropertyID = pa.PropertyAddressID and pa.IsPrimary = 1
				left join  dbo.tbPostalAddress a on pa.PostalAddressID = a.PostalAddressID
				left join dbo.vwPropertyAttributeNumeric pan on p.PropertyID = pan.PropertyID
				left join dbo.vwPropertyAttributeText pat on p.PropertyID = pat.PropertyID
			where p.IsDeleted = 0)

				
				if (@propertyCount > 1000)
				 select 1000 as propertyCount
				 else
				 select @propertyCount as propertyCount

			
			end
			
	end
	else
	BEGIN
			-- Return data
			if (@shouldReturnList = 1)
			begin			

				select @WorkInProgress = ISNULL((
									SELECT top 1000 property.propertyid as id, 
									0 checked, 
									0 removed
									from dbo.tbProperty property
									left join dbo.tbPropertyAddress pa on property.PropertyID = pa.PropertyAddressID and pa.IsPrimary = 1
									left join  dbo.tbPostalAddress a on pa.PostalAddressID = a.PostalAddressID
									left join dbo.vwPropertyAttributeNumeric pan on property.PropertyID = pan.PropertyID
									left join dbo.vwPropertyAttributeText pat on property.PropertyID = pat.PropertyID
									inner join @tblresult r on property.propertyid = r.propertyid									
									FOR XML AUTO, BINARY BASE64,root('wip')) , '')	
				
			end
			else
			begin
			select count(distinct
				p.PropertyID) as propertyCount
		
			from @tblresult r
				inner join dbo.tbProperty p on r.PropertyID = p.PropertyID
				left join dbo.tbPropertyAddress pa on p.PropertyID = pa.PropertyAddressID and pa.IsPrimary = 1
				left join  dbo.tbPostalAddress a on pa.PostalAddressID = a.PostalAddressID
				left join dbo.vwPropertyAttributeNumeric pan on p.PropertyID = pan.PropertyID
				left join dbo.vwPropertyAttributeText pat on p.PropertyID = pat.PropertyID
			where p.IsDeleted = 0
			end

	END

--/*Map Related logic ends*/
	
if (@shouldReturnList = 1)
begin
declare @AgentWorkInProgressID int
	
	select top 1 @AgentWorkInProgressID = AgentWorkInProgressID
	from tbAgentWorkInProgress
	where AgentSavedCriteriaID = @agensavedcriteriaid
	if (isnull(@CustomerID, 0) = 0)
		set @CustomerID = NULL

	declare @newlisting int

	
	
	
	if (isnull(@AgentWorkInProgressID,0) > 0 )
	  begin

	  select @newlisting = count(*) from
	  (select Tbl.Col.value('@id', 'int') as propertyID
	   from @WorkInProgress.nodes('//wip/property') Tbl(Col)) N
	   
	  
	   
	   if ( @newlisting > 0)
	   begin
		declare @CurrentWorkInProgress xml 

		select top 1 @CurrentWorkInProgress = WorkInProgress 
		from tbAgentWorkInProgress 
		where AgentWorkInProgressID = @AgentWorkInProgressID
			
		

		if (@CurrentWorkInProgress IS NOT NULL)
		select @WorkInProgress = 
			(select propertyID  as id,  checked,  removed
			from (select N.propertyID,
			isnull(C.Checked, N.Checked) as Checked,
			isnull(C.Removed, N.Removed) as Removed 
		from (select Tbl.Col.value('@id', 'int') as propertyID,
				Tbl.Col.value('@checked', 'int') as Checked,
				Tbl.Col.value('@removed', 'int')	as Removed
		from @WorkInProgress.nodes('//wip/property') Tbl(Col) ) as N
		LEFT JOIN 
		(select Tbl.Col.value('@id', 'int')			as propertyID,
				Tbl.Col.value('@checked', 'int') as Checked,
				Tbl.Col.value('@removed', 'int')	as Removed
			from @CurrentWorkInProgress.nodes('//wip/property') Tbl(Col) ) as C
				on C.propertyID = N.propertyID) as property 
			for xml auto, binary base64,root('wip'))
			
			 
		end
		update tbAgentWorkInProgress
		set AgentID = @AgentID,
			CustomerID= @CustomerID,
			Detail= @Detail,
			WorkInProgress= @WorkInProgress,
			IsDeleted=0,
			UpdateDateTime=getdate(),
			UpdateID = @AgentID
		where AgentWorkInProgressID = @AgentWorkInProgressID
	  end
	else 
	  begin
		insert into tbAgentWorkInProgress (AgentID,
											CustomerID,
											Detail,
											WorkInProgress,
											AgentSavedCriteriaID,
											SortBy,
											Bookmark,
											IsDeleted,
											CreateDateTime,
											CreateID) 
										values (@AgentID,
											@CustomerID,
											@Detail,
											@WorkInProgress,
											@agensavedcriteriaid,
											0,
											null,
											0,
											getdate(),
											@AgentID)
	  end
end	
end

GO


