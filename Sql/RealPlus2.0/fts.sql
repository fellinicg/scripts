USE [Jaguar_v2_Prototype2_BHS_2]
GO

/****** Object:  Table [dbo].[big_text]    Script Date: 10/26/2016 12:13:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[big_text3](
	[noteindexid] [int] NOT NULL,
	[recordid] [int] NULL,
	[recordtype] [varchar](1) NULL,
	[notetype] [varchar](50) NULL,
	[note] [varchar](max) NULL,
	[company] [varchar](4) NULL,
	[source] [varchar](25) NULL,
	[agent] [varchar](25) NULL,
	[securegroup] [varchar](15) NULL,
	[detailtype] [varchar](4) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

CREATE UNIQUE CLUSTERED INDEX [ClusteredIndex-noteindexid] ON [dbo].[big_text3]
(
	[noteindexid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [NonClusteredIndex-rectyperecidnotetype] ON [dbo].[big_text3]
(
	[recordtype] ASC,
	[recordid] ASC,
	[notetype] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [NonClusteredIndex-recordid] ON [dbo].[big_text3]
(
	[recordid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [NonClusteredIndex-recordtype] ON [dbo].[big_text3]
(
	[recordtype] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [NonClusteredIndex-notetype] ON [dbo].[big_text3]
(
	[notetype] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE FULLTEXT INDEX ON [dbo].[big_text3](note)
   KEY INDEX [ClusteredIndex-noteindexid]
   WITH STOPLIST = SYSTEM;