--select pa.PropertyAttributeID, pa.PropertyID, a.AttributeValueID, b.AttributeValueID
--from vwattributevalues a
--	inner join vwattributevalues b on a.ValueName = b.ValueName
--	inner join tbpropertyattribute pa on a.AttributeValueID = pa.AttributeValueID
--where a.attributename = 'neighborhood1' and a.AttributeValueID <> b.AttributeValueID

----commit tran
--insert into tbPropertyAttribute (PropertyID, AttributeValueID)
--select distinct top 20 l.PropertyID, 210
--from dbo.tbListing l
--	inner join dbo.tbProperty p on l.PropertyID = p.PropertyID
--	left join dbo.tbPropertyAttribute pa on p.PropertyID = pa.PropertyID
--	left join dbo.vwAttributeValues av on pa.AttributeValueID = av.AttributeValueID and av.AttributeID = 71
--where l.IsDeleted = 0
--group by l.PropertyID, av.AttributeID
--having count(1) = 1

--insert into tbPropertyAttribute (PropertyID, AttributeValueID)
--select top 1665 l.PropertyID, 265
--from dbo.tbListing l
--	left join dbo.tbPropertyAttribute pat on l.PropertyID = pat.PropertyID and pat.AttributeValueID in (259,261,265)
--where l.IsDeleted = 0 and pat.propertyid is null

--select top 100 p.PropertyID, '3% by seller'
--from dbo.tbProperty p
--	left join (
--		select PropertyID
--		from dbo.tbAttributeValue av
--			inner join dbo.tbPropertyAttribute pa on av.AttributeValueID = pa.AttributeValueID
--		where av.AttributeID = 37) n on p.PropertyID = n.PropertyID
--where PropertyClassID = 1 and n.PropertyID is null

insert into dbo.tbPropertyAttributeText (PropertyID, AttributeID, AttributeValue)
select top 500 p.PropertyID, 34, 'Stucco'
from dbo.tbProperty p
	left join (
		select PropertyID
		from dbo.tbPropertyAttributeText pat
		where pat.AttributeID = 34) n on p.PropertyID = n.PropertyID
where p.PropertyClassID = 1 and n.PropertyID is null




--insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
--select top 15 l.PropertyID, 1197, 3000
--from dbo.tbListing l
--	left join (
--		select PropertyID
--		from dbo.tbPropertyAttributeNumeric pat
--		where pat.AttributeID = 1197) n on l.PropertyID = n.PropertyID
--where l.IsDeleted = 0 and n.PropertyID is null

--insert into dbo.tbListingAttributeText (ListingID, AttributeID, AttributeValue)
--select top 15 l.ListingID, 87, 'yes'
--from dbo.tbListing l
--	left join (
--		select ListingID
--		from dbo.tbListingAttributeText pat
--		where pat.AttributeID = 87) n on l.ListingID = n.ListingID
--where l.IsDeleted = 0 and n.ListingID is null


--insert into dbo.tbListingAttributeNumeric (ListingID, AttributeID, AttributeValue)
--select top 15 l.ListingID, 79, 103
--from dbo.tbListing l
--	left join (
--		select ListingID
--		from dbo.tbListingAttributeNumeric pat
--		where pat.AttributeID = 79) n on l.ListingID = n.ListingID
--where l.IsDeleted = 0 and n.ListingID is null