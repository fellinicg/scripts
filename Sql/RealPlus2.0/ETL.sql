select 
'if  exists (select * from sys.foreign_keys where object_id = object_id(N' + char(39) + a.CONSTRAINT_SCHEMA + '.' + a.CONSTRAINT_NAME + char(39) + ') and parent_object_id = object_id(N'+ char(39) + b.TABLE_SCHEMA + '.' + b.TABLE_NAME + char(39) + '))' + char(10) + 'alter table ' + b.TABLE_SCHEMA + '.' + b.TABLE_NAME + ' drop constraint ' + a.CONSTRAINT_NAME + char(10) + 'go'
,'alter table ' + a.CONSTRAINT_SCHEMA + '.' + b.TABLE_NAME + ' with check add constraint ' + a.CONSTRAINT_NAME + char(10) + 'foreign key(' + b.COLUMN_NAME + ') references ' + c.TABLE_SCHEMA + '.' + c.TABLE_NAME + ' (' + c.COLUMN_NAME + ')' + char(10) + 'go'
from INFORMATION_SCHEMA.TABLE_CONSTRAINTS f
inner join INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS a on f.CONSTRAINT_NAME = a.CONSTRAINT_NAME
inner join INFORMATION_SCHEMA.KEY_COLUMN_USAGE b on a.CONSTRAINT_NAME = b.CONSTRAINT_NAME
inner join INFORMATION_SCHEMA.KEY_COLUMN_USAGE c on a.UNIQUE_CONSTRAINT_NAME = c.CONSTRAINT_NAME
where f.CONSTRAINT_TYPE = 'FOREIGN KEY' 
and c.TABLE_NAME = 'tbProperty'

truncate table dbo.units
truncate table dbo.properties
truncate table dbo.tbPropertyAttribute
truncate table dbo.tbPropertyAttributeNumeric
truncate table dbo.tbPropertyAttributeText
truncate table dbo.tbListingAttribute
truncate table dbo.tbListingAttributeNumeric
truncate table dbo.tbListingAttributeText

if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingPropertyID') and parent_object_id = object_id(N'dbo.tbListing'))
alter table dbo.tbListing drop constraint fktbListingPropertyID
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyAddressProperty') and parent_object_id = object_id(N'dbo.tbPropertyAddress'))
alter table dbo.tbPropertyAddress drop constraint fktbPropertyAddressProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyAliasProperty') and parent_object_id = object_id(N'dbo.tbPropertyAlias'))
alter table dbo.tbPropertyAlias drop constraint fktbPropertyAliasProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyAttributeNumericProperty') and parent_object_id = object_id(N'dbo.tbPropertyAttributeNumeric'))
alter table dbo.tbPropertyAttributeNumeric drop constraint fktbPropertyAttributeNumericProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyAttributeProperty') and parent_object_id = object_id(N'dbo.tbPropertyAttribute'))
alter table dbo.tbPropertyAttribute drop constraint fktbPropertyAttributeProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyAttributeTextProperty') and parent_object_id = object_id(N'dbo.tbPropertyAttributeText'))
alter table dbo.tbPropertyAttributeText drop constraint fktbPropertyAttributeTextProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyCommentProperty') and parent_object_id = object_id(N'dbo.tbPropertyComment'))
alter table dbo.tbPropertyComment drop constraint fktbPropertyCommentProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyContactAddressProperty') and parent_object_id = object_id(N'dbo.tbPropertyContactAddress'))
alter table dbo.tbPropertyContactAddress drop constraint fktbPropertyContactAddressProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyContactEmailProperty') and parent_object_id = object_id(N'dbo.tbPropertyContactEmail'))
alter table dbo.tbPropertyContactEmail drop constraint fktbPropertyContactEmailProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyContactPhoneProperty') and parent_object_id = object_id(N'dbo.tbPropertyContactPhone'))
alter table dbo.tbPropertyContactPhone drop constraint fktbPropertyContactPhoneProperty
go

truncate table dbo.tbProperty

alter table dbo.tbListing with check add constraint fktbListingPropertyID
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
alter table dbo.tbPropertyAddress with check add constraint fktbPropertyAddressProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
alter table dbo.tbPropertyAlias with check add constraint fktbPropertyAliasProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
alter table dbo.tbPropertyAttributeNumeric with check add constraint fktbPropertyAttributeNumericProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
alter table dbo.tbPropertyAttribute with check add constraint fktbPropertyAttributeProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
alter table dbo.tbPropertyAttributeText with check add constraint fktbPropertyAttributeTextProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
alter table dbo.tbPropertyComment with check add constraint fktbPropertyCommentProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
alter table dbo.tbPropertyContactAddress with check add constraint fktbPropertyContactAddressProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
alter table dbo.tbPropertyContactEmail with check add constraint fktbPropertyContactEmailProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
alter table dbo.tbPropertyContactPhone with check add constraint fktbPropertyContactPhoneProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go

==========================================

if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentListingListing') and parent_object_id = object_id(N'dbo.tbAgentListing'))
alter table dbo.tbAgentListing drop constraint fktbAgentListingListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingAttributeListing') and parent_object_id = object_id(N'dbo.tbListingAttribute'))
alter table dbo.tbListingAttribute drop constraint fktbListingAttributeListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingAttributeNumericListing') and parent_object_id = object_id(N'dbo.tbListingAttributeNumeric'))
alter table dbo.tbListingAttributeNumeric drop constraint fktbListingAttributeNumericListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingAttributeTextListing') and parent_object_id = object_id(N'dbo.tbListingAttributeText'))
alter table dbo.tbListingAttributeText drop constraint fktbListingAttributeTextListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingCommentListing') and parent_object_id = object_id(N'dbo.tbListingComment'))
alter table dbo.tbListingComment drop constraint fktbListingCommentListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingContactAddressListing') and parent_object_id = object_id(N'dbo.tbListingContactAddress'))
alter table dbo.tbListingContactAddress drop constraint fktbListingContactAddressListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingContactEmailListing') and parent_object_id = object_id(N'dbo.tbListingContactEmail'))
alter table dbo.tbListingContactEmail drop constraint fktbListingContactEmailListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingContactPhoneListing') and parent_object_id = object_id(N'dbo.tbListingContactPhone'))
alter table dbo.tbListingContactPhone drop constraint fktbListingContactPhoneListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingMediaListing') and parent_object_id = object_id(N'dbo.tbListingMedia'))
alter table dbo.tbListingMedia drop constraint fktbListingMediaListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingOfficeListing') and parent_object_id = object_id(N'dbo.tbListingOffice'))
alter table dbo.tbListingOffice drop constraint fktbListingOfficeListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingOpenHouseListing') and parent_object_id = object_id(N'dbo.tbListingOpenHouse'))
alter table dbo.tbListingOpenHouse drop constraint fktbListingOpenHouseListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingStatusListing') and parent_object_id = object_id(N'dbo.tbListingStatus'))
alter table dbo.tbListingStatus drop constraint fktbListingStatusListing
go

truncate table tbListing

alter table dbo.tbAgentListing with check add constraint fktbAgentListingListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListingAttribute with check add constraint fktbListingAttributeListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListingAttributeNumeric with check add constraint fktbListingAttributeNumericListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListingAttributeText with check add constraint fktbListingAttributeTextListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListingComment with check add constraint fktbListingCommentListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListingContactAddress with check add constraint fktbListingContactAddressListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListingContactEmail with check add constraint fktbListingContactEmailListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListingContactPhone with check add constraint fktbListingContactPhoneListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListingMedia with check add constraint fktbListingMediaListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListingOffice with check add constraint fktbListingOfficeListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListingOpenHouse with check add constraint fktbListingOpenHouseListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListingStatus with check add constraint fktbListingStatusListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go


insert into dbo.properties
select 
	--top 1
	src.*
from Jaguar_v2_ETL.dbo.properties src 
left join dbo.properties dest on src.propertyid = dest.propertyid
where dest.propertyid is null

insert into dbo.units
select 
	--top 1
	src.*
from Jaguar_v2_ETL.dbo.units src
left join dbo.units dest on src.unitid = dest.unitid
where dest.unitid is null

insert into dbo.listings
select 
--	top 100
	src.*
from Jaguar_v2_ETL.dbo.listings src
left join dbo.listings dest on src.listingid = dest.listingid