use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwListingBySearch'))
drop view dbo.vwListingBySearch
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160415
-- ============================================================
create view dbo.vwListingBySearch
as
	select 
		l.ListingID
		, ld.DetailText ListingDescription
		, c.CompanyName ListingCompany
		, l.ListingCategoryID
		, p.PropertyID
		, p.ParentPropertyID
		, dbo.sfGetAttributeValue(l.ListingProviderID) ListingProvider
		, dbo.sfGetAttributeValue(l.ListingCategoryID) ListingCategory
		, dbo.sfGetAttributeValue(l.ListingTypeID) ListingType
		, dbo.sfGetAttributeValue(p.PropertyClassID) PropertyClass
		, dbo.sfGetPropertyAttribute(p.ParentPropertyID, 13) BuildingFeatures
		, dbo.sfGetPropertyAttribute(p.ParentPropertyID, 19) BuildingType
--		, dbo.sfGetPropertyAttribute(p.ParentPropertyID, 72) 'Ownershiptype'
		, dbo.sfGetPropertyAttribute(p.ParentPropertyID, 1215) Neighborhood
		, dbo.sfGetPropertyAttribute(p.PropertyID, 71) OutdoorSpace
		, dbo.sfGetPropertyAttribute(p.ParentPropertyID, 14) BuildingName
		, dbo.sfGetPropertyAttribute(p.ParentPropertyID, 12) BuildingAllow
		, dbo.sfGetPropertyAttribute(p.ParentPropertyID, 16) BuildingNotAllow
		, dbo.sfGetPropertyAttribute(p.ParentPropertyID, 1118) BuildingPeriod
		, dbo.sfGetListingMediaXML(l.ListingID) ListingMediaXML
		, l.InitialPrice
		, l.CurrentPrice
		, dbo.sfGetListingSoldPrice(l.ListingID) SoldPrice
		, l.CurrentStatus
		, l.ListDate
		, dbo.sfGetListingContractSignDate(l.ListingID) ContractSignDate
		, lat.ListingNumber
		, lat.ListingNumberReference
		, ppa.Line1 AddressLine1
		, coalesce(pat.UnitIdentifier, ppat.UnitIdentifier) UnitIdentifier
		, ppa.CityTown
		, ppa.StateProvince
		, ppa.PostalCode
		, pp.Longitude
		, pp.Latitude
		, lan.ClosingPrice
		, lan.BuyerCommission
		, lan.SellerCommission
		, lan.TotalCommission
		, dbo.sfGetListingAttribute(l.ListingID, 'pricepersqft') PricePerSqFt
		, dbo.sfGetListingAttribute(l.ListingID, 'hasoh') HasOH
		, dbo.sfGetPropertyAttribute(p.PropertyID, 1197) RETaxes
		, dbo.sfGetPropertyAttribute(p.ParentPropertyID, 1191) DoorMan
		, dbo.sfGetPropertyAttribute(p.ParentPropertyID, 1219) CrossStreets
		, dbo.sfGetPropertyAttribute(p.PropertyID, 1218) LastAskPrice
		, dbo.sfGetListingAttribute(l.ListingID, 'IsNew') IsNew
		, coalesce(pan.AssociationFee, ppan.AssociationFee) AssociationFee
		, coalesce(pan.Bathrooms, ppan.Bathrooms) Bathrooms
		, coalesce(pan.Bedrooms, ppan.Bedrooms) Bedrooms
		, coalesce(pan.HalfBathrooms, ppan.HalfBathrooms) HalfBathrooms
		, coalesce(pan.Rooms, ppan.Rooms) Rooms
		, coalesce(pan.SquareFootage, ppan.SquareFootage) SquareFootage
		, coalesce(pan.Elevators, ppan.Elevators) Elevators
		, coalesce(pan.FinanceAllowedPercent, ppan.FinanceAllowedPercent) FinanceAllowedPercent
		, coalesce(pan.Floors, ppan.Floors) Floors
		, coalesce(pan.Shares, ppan.Shares) Shares
		, coalesce(pan.TaxDeductPct, ppan.TaxDeductPct) TaxDeductPct
		, coalesce(pan.Units, ppan.Units) Units
		, coalesce(pan.YearBuilt, ppan.YearBuilt) YearBuilt
		, coalesce(pan.YearIncorporated, ppan.YearIncorporated) YearIncorporated
		--, coalesce(pan.MaintCC, ppan.MaintCC) MaintCC
		--, coalesce(pan.MaidBed, ppan.MaidBed) MaidBed
		--, coalesce(pan.MaidBath, ppan.MaidBath) MaidBath
		--, coalesce(pat.AptViews, ppat.AptViews) AptViews
		--, coalesce(pat.Exposure, ppat.Exposure) Exposure
		--, coalesce(pat.AptType, ppat.AptType) AptType
		--, coalesce(pat.HasWashDry, ppat.HasWashDry) HasWashDry
		--, coalesce(pat.HasAc, ppat.HasAc) HasAc
		--, coalesce(pat.HasFireplace, ppat.HasFireplace) HasFireplace
		, coalesce(pat.FlipTax, ppat.FlipTax) FlipTax
		--, coalesce(pat.Facade, ppat.Facade) Facade
		--, coalesce(pat.HasStorage, ppat.HasStorage) HasStorage
		--, coalesce(pat.HasLaundry, ppat.HasLaundry) HasLaundry
		--, coalesce(pat.HasPool, ppat.HasPool) HasPool
		--, coalesce(pat.HasParking, ppat.HasParking) HasParking
		--, coalesce(pat.HasHealthClub, ppat.HasHealthClub) HasHealthClub
		--, coalesce(pat.HeatType, ppat.HeatType) HeatType
		--, coalesce(pat.HouseStyle, ppat.HouseStyle) HouseStyle
		--, coalesce(pan.BuildingSize, ppan.BuildingSize) BuildingSize
		--, coalesce(pat.IsFurnished, ppat.IsFurnished) IsFurnished
		--, coalesce(pat.LeaseTerm, ppat.LeaseTerm) LeaseTerm	
		--, coalesce(pat.LotSize, ppat.LotSize) LotSize	
		--, coalesce(pat.Block, ppat.Block) Block	
		--, coalesce(pat.Lot, ppat.Lot) Lot	
		--, coalesce(pat.IsLandmark, ppat.IsLandmark) IsLandmark	
		--, coalesce(pat.Zoning, ppat.Zoning) Zoning	
		--, coalesce(pat.CertOfOcc, ppat.CertOfOcc) CertOfOcc	
		--, coalesce(pat.BuildingExtension, ppat.BuildingExtension) BuildingExtension	
		--, coalesce(pat.IsDeliveredVacant, ppat.IsDeliveredVacant) IsDeliveredVacant	
		--, coalesce(pat.StructureStyle, ppat.StructureStyle) StructureStyle	
		, l.UpdateDateTime ListingUpdated
		, l.CreateDateTime ListingCreated
	from dbo.tbListing l
		inner join dbo.tbListingOffice lo on l.ListingID = lo.ListingID
		inner join dbo.tbOffice o on lo.OfficeID = o.OfficeID
		inner join dbo.tbCompany c on o.CompanyID = c.CompanyID
		left join dbo.vwListingDescription ld on l.ListingID = ld.ListingID
		left join dbo.tbProperty p on l.PropertyID = p.PropertyID
		left join dbo.tbProperty pp on p.ParentPropertyID = pp.PropertyID
		left join dbo.tbPropertyAddress pa on pp.PropertyID = pa.PropertyID and pa.IsPrimary = 1
		left join dbo.tbPostalAddress ppa on pa.PostalAddressID = ppa.PostalAddressID
		left join dbo.vwListingAttributeNumeric lan on l.ListingID = lan.ListingID
		left join dbo.vwListingAttributeText lat on l.ListingID = lat.ListingID
		left join dbo.vwPropertyAttributeNumeric pan on p.PropertyID = pan.PropertyID
		left join dbo.vwPropertyAttributeNumeric ppan on p.ParentPropertyID = ppan.PropertyID
		left join dbo.vwPropertyAttributeText pat on p.PropertyID = pat.PropertyID
		left join dbo.vwPropertyAttributeText ppat on p.ParentPropertyID = ppat.PropertyID
	where l.IsDeleted = 0