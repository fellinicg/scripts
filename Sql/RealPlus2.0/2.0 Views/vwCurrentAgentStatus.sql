use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwCurrentAgentStatus'))
drop view dbo.vwCurrentAgentStatus
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160307
-- ============================================================
create view dbo.vwCurrentAgentStatus
--with schemabinding
as

	select a.AgentStatusID, a.AgentID, c.ValueName
	from dbo.tbAgentStatus a
		inner join (select distinct AgentID, max(CreateDateTime) CreateDateTime	from dbo.tbAgentStatus group by AgentID) b on a.AgentID = b.AgentID and a.CreateDateTime = b.CreateDateTime
		inner join dbo.vwAttributeValues c on a.StatusID = c.AttributeValueID