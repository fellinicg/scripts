use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwListingAttribute'))
drop view dbo.vwListingAttribute
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160321
-- ============================================================
create view dbo.vwListingAttribute
as

	select lan.ListingID, a.AttributeCode, a.AttributeName, convert(varchar, lan.AttributeValue) AttributeValue
	from dbo.tbListingAttributeNumeric lan 
	inner join dbo.tbAttribute a on lan.AttributeID = a.AttributeID
	where lan.IsDeleted = 0 and a.IsDeleted = 0
	union
	select lat.ListingID, a.AttributeCode, a.AttributeName, lat.AttributeValue
	from dbo.tbListingAttributeText lat 
	inner join dbo.tbAttribute a on lat.AttributeID = a.AttributeID