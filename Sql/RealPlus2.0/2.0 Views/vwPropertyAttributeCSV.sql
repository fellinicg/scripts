use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwPropertyAttributeCSV'))
drop view dbo.vwPropertyAttributeCSV
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160413
-- ============================================================
create view dbo.vwPropertyAttributeCSV
as
	select distinct
		pa.PropertyID
		, att.AttributeID
		, att.AttributeName
		, stuff((select ',' + convert(varchar(max), c.ValueName)
				from dbo.tbPropertyAttribute a
					inner join dbo.tbAttributeValue b on a.AttributeValueID = b.AttributeValueID and b.AttributeID = av.AttributeID
					inner join dbo.tbValue c on b.ValueID = c.ValueID and c.IsDeleted = 0
				where a.PropertyID = pa.PropertyID
				for xml path('')), 1, 1, '') as AttributeValues
	from dbo.tbPropertyAttribute pa
		inner join dbo.tbAttributeValue av on pa.AttributeValueID = av.AttributeValueID and av.Isdeleted = 0
		inner join dbo.tbAttribute att on av.AttributeID = att.AttributeID and att.IsDeleted = 0