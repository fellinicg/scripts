use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwPropertyInfo'))
drop view dbo.vwPropertyInfo
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160314
-- ============================================================
create view dbo.vwPropertyInfo
--with schemabinding
as

	select 
		p.PropertyID
		, p.ParentPropertyID
		, p.PropertyClassID
		, pc.ValueName 'PropertyClass'
		, p.Block
		, p.Lot
		, p.Section
		, p.Latitude
		, p.Longitude
		, p.IsDeleted 'PropertyIsDeleted'
		, p.EtlID 'PropertyEtl'
		, pa.PropertyAddressID
		, a.Line1
		, a.Line2
		, a.CityTown
		, a.StateProvince
		, a.PostalCode
		, pa.IsPrimary
		, pa.IsDeleted 'PropertyAddressIsDeleted'
	from dbo.tbProperty p
		left join dbo.tfAttributeValue() pc on p.PropertyClassID = pc.AttributeValueID
		left join dbo.tbPropertyAddress pa on p.PropertyID = pa.PropertyID
		left join dbo.tbPostalAddress a on pa.PostalAddressID = a.PostalAddressID
	where p.ParentPropertyID is not null