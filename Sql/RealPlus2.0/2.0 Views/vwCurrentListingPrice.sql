use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwCurrentListingPrice'))
drop view dbo.vwCurrentListingPrice
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160320
-- ============================================================
create view dbo.vwCurrentListingPrice
with schemabinding
as

	select a.ListingPriceID, a.ListingID, a.ListingPrice
	from dbo.tbListingPrice a
		inner join 
		(
			select distinct ListingID, max(PriceDateTime) PriceDateTime 
			from dbo.tbListingPrice 
			group by ListingID
		) b on a.ListingID = b.ListingID and a.PriceDateTime = b.PriceDateTime