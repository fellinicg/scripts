use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwListingMediaXML'))
drop view dbo.vwListingMediaXML
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160409
-- ============================================================
create view dbo.vwListingMediaXML
as

	select 
		l.ListingID
		, (select lm.ListingMediaID, lm.ListingMediaTypeID, v.ValueName as ListingMediaType, m.Link, lm.AdditionalDetail, lm.SortOrder
			from dbo.tbListingMedia lm
				inner join dbo.tbMedia m on lm.MediaID = m.MediaID
				inner join dbo.tbAttributeValue av on lm.ListingMediaTypeID = av.AttributeValueID
				inner join dbo.tbValue v on av.ValueID = v.ValueID
            where lm.ListingID = l.ListingID and lm.IsDeleted = 0 
			for xml raw) as ListingMediaXML
--			for xml path('ListingMedia'), type) as ListingMediaXML
	from dbo.tbListing l
	where l.Isdeleted = 0