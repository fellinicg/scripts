use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwPropertyAttributeText'))
drop view dbo.vwPropertyAttributeText
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160329
-- ============================================================
create view dbo.vwPropertyAttributeText
as
	select
		PropertyID
		, coalesce(max(case when AttributeID = 37 then AttributeValue end), null) as FlipTax
		, coalesce(max(case when AttributeID = 100 then AttributeValue end), null) as UnitIdentifier
	from dbo.tbPropertyAttributeText
	group by PropertyID