use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwCurrentListingStatus'))
drop view dbo.vwCurrentListingStatus
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160309
-- ============================================================
create view dbo.vwCurrentListingStatus
as

	select ls.ListingStatusID, ls.ListingID, av.AttributeValueID, v.ValueName, cs.StatusDateTime
	from dbo.tbListingStatus ls
		inner join 
		(
			select distinct ListingID, max(StatusDateTime) StatusDateTime 
			from dbo.tbListingStatus 
			group by ListingID
		) cs on ls.ListingID = cs.ListingID and ls.StatusDateTime = cs.StatusDateTime
		inner join dbo.tbAttributeValue av on ls.StatusID = av.AttributeValueID
		inner join dbo.tbValue v on av.ValueID = v.ValueID
		--inner join dbo.vwAttributeValues av on ls.StatusID = av.AttributeValueID