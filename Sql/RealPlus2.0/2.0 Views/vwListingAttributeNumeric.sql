use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwListingAttributeNumeric'))
drop view dbo.vwListingAttributeNumeric
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160329
-- ============================================================
create view dbo.vwListingAttributeNumeric
as
	select
		ListingID
		, coalesce(max(case when AttributeID = 20 then AttributeValue end), null) as BuyerCommission
		, coalesce(max(case when AttributeID = 25 then AttributeValue end), null) as ClosingPrice
		, coalesce(max(case when AttributeID = 88 then AttributeValue end), null) as SellerCommission
		, coalesce(max(case when AttributeID = 98 then AttributeValue end), null) as TotalCommission
	from dbo.tbListingAttributeNumeric
	group by ListingID