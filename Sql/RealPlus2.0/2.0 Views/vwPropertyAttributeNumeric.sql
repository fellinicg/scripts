use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwPropertyAttributeNumeric'))
drop view dbo.vwPropertyAttributeNumeric
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160329
-- ============================================================
create view dbo.vwPropertyAttributeNumeric
as
	select
		PropertyID
		, coalesce(max(case when AttributeID = 9 then AttributeValue end), null) as AssociationFee
		, coalesce(max(case when AttributeID = 10 then AttributeValue end), null) as Bathrooms
		, coalesce(max(case when AttributeID = 11 then AttributeValue end), null) as Bedrooms
		, coalesce(max(case when AttributeID = 31 then AttributeValue end), null) as Elevators
		, coalesce(max(case when AttributeID = 36 then AttributeValue end), null) as FinanceAllowedPercent
		, coalesce(max(case when AttributeID = 39 then AttributeValue end), null) as Floors
		, coalesce(max(case when AttributeID = 41 then AttributeValue end), null) as HalfBathrooms
		, coalesce(max(case when AttributeID = 85 then AttributeValue end), null) as Rooms
		, coalesce(max(case when AttributeID = 89 then AttributeValue end), null) as Shares
		, coalesce(max(case when AttributeID = 91 then AttributeValue end), null) as SquareFootage
		, coalesce(max(case when AttributeID = 97 then AttributeValue end), null) as TaxDeductPct
		, coalesce(max(case when AttributeID = 102 then AttributeValue end), null) as Units
		, coalesce(max(case when AttributeID = 106 then AttributeValue end), null) as YearBuilt
		, coalesce(max(case when AttributeID = 107 then AttributeValue end), null) as YearIncorporated
	from dbo.tbPropertyAttributeNumeric
	group by PropertyID