use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwPropertyAttribute'))
drop view dbo.vwPropertyAttribute
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160329
-- ============================================================
create view dbo.vwPropertyAttribute
as
	select
		PropertyID
		, coalesce(max(case when AttributeID = 7 then ValueName end), null) as ApartmentType
		, coalesce(max(case when AttributeID = 12 then ValueName end), null) as BuildingAllow
		, coalesce(max(case when AttributeID = 13 then ValueName end), null) as BuildingAmenity
		, coalesce(max(case when AttributeID = 14 then ValueName end), null) as BuildingEra
		, coalesce(max(case when AttributeID = 16 then ValueName end), null) as BuildingNotAllow
		, coalesce(max(case when AttributeID = 17 then ValueName end), null) as BuildingPersonnel
		, coalesce(max(case when AttributeID = 19 then ValueName end), null) as BuildingType
		, coalesce(max(case when AttributeID = 33 then ValueName end), null) as Exposure
		, coalesce(max(case when AttributeID = 68 then ValueName end), null) as IsNewConstruction
		, coalesce(max(case when AttributeID = 67 then ValueName end), null) as Neighborhood
		, coalesce(max(case when AttributeID = 71 then ValueName end), null) as OutdoorSpace
		, coalesce(max(case when AttributeID = 72 then ValueName end), null) as OwnershipType
		, coalesce(max(case when AttributeID = 105 then ValueName end), null) as 'View'
	from dbo.tbPropertyAttribute pa
		inner join dbo.tbAttributeValue av on pa.AttributeValueID = av.AttributeValueID
		inner join dbo.tbValue v on av.ValueID = v.ValueID
	group by PropertyID