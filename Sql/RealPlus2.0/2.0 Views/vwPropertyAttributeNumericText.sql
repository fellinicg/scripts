use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwPropertyAttributeNumericText'))
drop view dbo.vwPropertyAttributeNumericText
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160321
-- ============================================================
create view dbo.vwPropertyAttributeNumericText
as

	select pan.PropertyID, a.AttributeCode, a.AttributeName, convert(varchar, pan.AttributeValue) AttributeValue
	from dbo.tbPropertyAttributeNumeric pan 
	inner join dbo.tbAttribute a on pan.AttributeID = a.AttributeID
	where pan.IsDeleted = 0 and a.IsDeleted = 0
	union
	select pat.PropertyID, a.AttributeCode, a.AttributeName, pat.AttributeValue
	from dbo.tbPropertyAttributeText pat 
	inner join dbo.tbAttribute a on pat.AttributeID = a.AttributeID