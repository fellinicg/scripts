use Jaguar_v2_Prototype2_Dev
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwExclusiveAgent'))
drop view dbo.vwExclusiveAgent
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160716
-- ============================================================
create view dbo.vwExclusiveAgent
as

	select
		c.recordid ListingID
		, isnull(firstname, '') + ' ' + isnull(lastname, '') FullName
		, email EmailAddress
		, officephone FullPhoneNumber
	from dbo.contacts c
	inner join 
		(	select distinct recordid, max(contactid) contactid 
			from dbo.contacts
			where recordtype = 'L'
				and contacttype = 'E'
			group by recordid ) ec on c.contactid = ec.contactid
