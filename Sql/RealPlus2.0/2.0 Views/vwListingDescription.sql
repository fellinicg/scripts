use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwListingDescription'))
drop view dbo.vwListingDescription
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160422
-- ============================================================
create view dbo.vwListingDescription
as

	select l.ListingID, d.DetailText
	from dbo.tbDetail d
		inner join dbo.tbListingComment lc on d.DetailID = lc.DetailID
		inner join dbo.tbListing l on lc.ListingID = l.ListingID
	where d.DetailTypeID = 171