use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwAgentInfo'))
drop view dbo.vwAgentInfo
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160313
-- ============================================================
create view dbo.vwAgentInfo
as

	select a.AgentID
		, a.AgentNumber
		, d.ValueName 'Status'
		, b.FullName
		, e.OfficeID
		, e.OfficeCode
		, e.Detail
		, f.CompanyID
		, f.CompanyName
	from dbo.tbAgent a
		inner join dbo.tbPerson b on a.AgentID = b.PersonID
		inner join dbo.tbAgentOffice c on a.AgentID = c.AgentID
		inner join dbo.vwCurrentAgentStatus d on a.AgentID = d.AgentID
		inner join dbo.tbOffice e on c.OfficeID = e.OfficeID
		inner join dbo.tbCompany f on e.CompanyID = f.CompanyID