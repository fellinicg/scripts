use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwListingBySearchV2'))
drop view dbo.vwListingBySearchV2
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160415
-- ============================================================
create view dbo.vwListingBySearchV2
as
	select top 1000
		l.ListingID
		, convert(nvarchar, bbd.note) ListingDescription
		, bbl.listingcompany ListingCompany
		, l.ListingCategoryID
		, p.PropertyID
		, p.ParentPropertyID
		, dbo.sfGetAttributeValue(l.ListingProviderID) ListingProvider
		, dbo.sfGetAttributeValue(l.ListingCategoryID) ListingCategory
		, dbo.sfGetAttributeValue(l.ListingTypeID) ListingType
		, dbo.sfGetAttributeValue(p.PropertyClassID) PropertyClass
		, dbo.sfGetPropertyAttribute(p.ParentPropertyID, 13) BuildingFeatures
		, dbo.sfGetPropertyAttribute(p.ParentPropertyID, 19) BuildingType
		, bbu.overallcondition condition
		, dbo.sfGetPropertyAttribute(p.ParentPropertyID, 72) Ownershiptype
		, bba.neighborhood_display Neighborhood
		, dbo.sfGetPropertyAttribute(p.PropertyID, 71) OutdoorSpace
		, bbu.hasoutdoorspace hasOutdoorSpace
		, bbu.haslobbyattendant haslobbyattendant
		, bbp.buildingname BuildingName
		, dbo.sfGetPropertyAttribute(p.ParentPropertyID, 12) BuildingAllow
		, dbo.sfGetPropertyAttribute(p.ParentPropertyID, 16) BuildingNotAllow
		, dbo.sfGetPropertyAttribute(p.ParentPropertyID, 1118) BuildingPeriod
		, dbo.sfGetPropertyAttribute(p.ParentPropertyID, 1119) furnishedRental
		, bbu.petpolicy petpolicy
		, dbo.sfGetListingMediaXML(l.ListingID) ListingMediaXML
		--		, dbo.sfGetBldgMediaXML(l.ListingID) BldgMediaXML
		, convert(xml, NULL) BldgMediaXML
		, l.InitialPrice
		, l.CurrentPrice
		, convert(float, bbl.closingprice) SoldPrice
		, l.CurrentStatus
		, l.ListDate
		, convert(datetime2, bbl.vecontractsigneddate) ContractSignDate
		, bbl.listingnumber ListingNumber
		, bbl.webid ListingNumberReference
		, bba.addressid PostalAddressID
		, bbl.displayaddress AddressLine1
		, bbu.unitnumber_full UnitIdentifier
		, bba.city CityTown
		, bba.state StateProvince
		, bba.postalcode PostalCode
		, convert(varchar, bba.longitude) Longitude
		, convert(varchar, bba.latitude) Latitude
		, convert(float, bbl.closingprice) ClosingPrice
		, convert(float, bbl.buyercommission) BuyerCommission
		, convert(float, bbl.sellercommission) SellerCommission
		, convert(float, bbl.totalcommission) TotalCommission
		, dbo.sfGetListingAttribute(l.ListingID, 'pricepersqft') PricePerSqFt
		, dbo.sfGetListingAttribute(l.ListingID, 'hasoh') HasOH
		, convert(varchar, bbl.taxes)  RETaxes
		, bbp.doorman DoorMan
		, bba.cross_street CrossStreets
		, convert(varchar, bbl.price) LastAskPrice
		, bbp.newconstruction IsNew
		, convert(float, bbl.associationfee) AssociationFee
		, convert(float, bbu.bathrooms) Bathrooms
		, convert(float, bbu.bedrooms) Bedrooms
		, convert(float, bbu.halfbaths) HalfBathrooms
		, convert(float, bbu.totalrooms) Rooms
		, convert(float, bbu.totalsqft) SquareFootage
		, convert(float, bbp.elevators) Elevators
		, convert(float, bbp.financingallowedpercent) FinanceAllowedPercent
		, convert(float, bbp.totalfloors) Floors
		, convert(float, bbp.totalshares) Shares
		, convert(float, bbp.taxdeduction) TaxDeductPct
		, convert(float, bbp.totalunits) Units
		, convert(float, bbp.yearbuilt) YearBuilt
		, convert(float, bbp.yearincorporated) YearIncorporated
		, convert(float, NULL) MaintCC
		, convert(float, bbu.maidbedrooms) MaidBed
		, convert(float, bbu.maidbathrooms) MaidBath
		, dbo.sfGetPropertyAttribute(p.PropertyID, 105) AptViews
		, dbo.sfGetPropertyAttribute(p.PropertyID, 33) Exposure
		, dbo.sfGetPropertyAttribute(p.PropertyID, 7) AptType
		, bbu.haswashdry HasWashDry
		, bbu.hasac HasAc
		, bbu.hasfireplace HasFireplace
		, bbp.fliptax FlipTax
		, dbo.sfGetPropertyAttribute(p.PropertyID, 34) Facade
		, bbp.storage HasStorage
		, bbp.laundry HasLaundry
		, bbp.pool HasPool
		, convert(varchar, bbp.offstreetparkingspots) HasParking
		, bbp.healthclub HasHealthClub
		, bbu.heatingtype HeatType
		, dbo.sfGetListingAttributeByID(l.ListingID, 114) HouseStyle
		, convert(float, NULL) BuildingSize
		, bbu.isfurnished IsFurnished
		, bbl.leaseterm LeaseTerm
		, bbp.lotsize LotSize	
		, convert(varchar,bba.block) Block	
		, convert(varchar,bba.lot) Lot	
		, bbp.landmark IsLandmark
		, bbp.zoning Zoning	
		, bbu.certificateofoccupancy CertOfOcc
		, dbo.sfGetListingAttributeByID(l.ListingID, 1206) BuildingExtension
		, bbl.delivered_vacant IsDeliveredVacant
		, dbo.sfGetListingAttributeByID(l.ListingID, 114) StructureStyle
		, convert(datetime2, bbl.modifieddate) ListingUpdated
		, convert(datetime2, bbl.createddate) ListingCreated
		, convert(numeric, bbl.furnishedrent) FurnishedPrice
		, convert(varchar, NULL) FullName
		, convert(varchar, NULL) FullPhoneNumber
		, convert(nvarchar, NULL) EmailAddress
		, convert(varchar, NULL) NeighborhoodCode
		, bbp.airconditioningtype typeofac
		, dbo.sfGetPropertyAttribute(p.PropertyID, 1257) managementcompany
		, dbo.sfGetPropertyAttribute(p.PropertyID, 86) petsallowed
		, convert(varchar,datediff(d,l.ListDate,getdate())) DOM
		, '0' PriceIndicator
	from dbo.tbListing l
		inner join dbo.listings bbl on l.ListingID = bbl.listingid
		inner join dbo.units bbu on bbl.unitid = bbu.unitid
		inner join dbo.properties bbp on bbl.propertyid = bbp.propertyid
		inner join dbo.addresses bba on bbp.addressid = bba.addressid
		left join dbo.big_text bbd on bbl.listingid = bbd.recordid and bbd.recordtype = 'L'
		left join dbo.tbProperty p on l.PropertyID = p.PropertyID
	where l.IsDeleted = 0
