use Jaguar_v2_Prototype2_Dev
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwPropertyAttributePivot'))
drop view dbo.vwPropertyAttributePivot
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160711
-- ============================================================
create view dbo.vwPropertyAttributePivot
as
	select PropertyID, buildingallow, buildingamenity, buildingnotallow, buildingpersonnel, buildingtype, facade, outdoorspace, ownershiptype, views, housestyle, buildingperiod, [advaptmisc-s], [advaptmisc-r], thtype, exposure, apartmenttype
	from 
	(
		select distinct
			pa.PropertyID
			, att.AttributeName
			, stuff((select ',' + convert(varchar(max), c.ValueName)
					from dbo.tbPropertyAttribute a
						inner join dbo.tbAttributeValue b on a.AttributeValueID = b.AttributeValueID and b.AttributeID = av.AttributeID
						inner join dbo.tbValue c on b.ValueID = c.ValueID and c.IsDeleted = 0
					where a.PropertyID = pa.PropertyID
					for xml path('')), 1, 1, '') as AttributeValues
		from dbo.tbPropertyAttribute pa
			inner join dbo.tbAttributeValue av on pa.AttributeValueID = av.AttributeValueID and av.Isdeleted = 0
			inner join dbo.tbAttribute att on av.AttributeID = att.AttributeID and att.IsDeleted = 0
	) a
	pivot 
	(
	max(AttributeValues)
	for AttributeName in (buildingallow, buildingamenity, buildingnotallow, buildingpersonnel, buildingtype, facade, outdoorspace, ownershiptype, views, housestyle, buildingperiod, [advaptmisc-s], [advaptmisc-r], thtype, exposure, apartmenttype)
	) p