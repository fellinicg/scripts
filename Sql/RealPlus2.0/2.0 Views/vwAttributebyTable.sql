use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwAttributebyTable'))
drop view dbo.vwAttributebyTable
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160330
-- ============================================================
create view dbo.vwAttributebyTable
as

	select 'tbPropertyAttribute' as 'TableName', PropertyAttributeID as 'ID', AttributeValueID
	from dbo.tbPropertyAttribute
	union
	select 'tbListingAttribute' as 'TableName', ListingAttributeID as 'ID', AttributeValueID
	from dbo.tbListingAttribute
	union
	select 'tbPersonAttribute' as 'TableName', PersonAttributeID as 'ID', AttributeValueID
	from dbo.tbPersonAttribute