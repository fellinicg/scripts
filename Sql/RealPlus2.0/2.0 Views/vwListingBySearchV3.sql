use Jaguar_v2_Prototype2_Dev
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwListingBySearchV3'))
drop view dbo.vwListingBySearchV3
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160711
-- ============================================================
create view dbo.vwListingBySearchV3
as
	select
		l.ListingID
		, convert(nvarchar, bbd.note) ListingDescription
		, bbl.listingcompany ListingCompany
		, l.ListingCategoryID
		, p.PropertyID
		, p.ParentPropertyID
		, dbo.sfGetAttributeValue(l.ListingProviderID) ListingProvider
		, dbo.sfGetAttributeValue(l.ListingCategoryID) ListingCategory
		, dbo.sfGetAttributeValue(l.ListingTypeID) ListingType
		, dbo.sfGetAttributeValue(p.PropertyClassID) PropertyClass
		, convert(varchar, NULL) BuildingFeatures
		, ppa.buildingtype
		, bbu.overallcondition condition
		, ppa.ownershiptype
		, bba.neighborhood_display Neighborhood
		, pa.outdoorspace
		, bbu.hasoutdoorspace hasOutdoorSpace
		, bbu.haslobbyattendant haslobbyattendant
		, bbp.buildingname BuildingName
		, ppa.buildingallow
		, ppa.buildingnotallow
		, ppa.BuildingPeriod
		, convert(varchar, NULL) furnishedRental
		, bbu.petpolicy petpolicy
		, dbo.sfGetListingMediaXML(l.ListingID) ListingMediaXML
		--------		, dbo.sfGetBldgMediaXML(l.ListingID) BldgMediaXML
		, convert(xml, NULL) BldgMediaXML
		, l.InitialPrice
		, l.CurrentPrice
		, convert(float, bbl.closingprice) SoldPrice
		, l.CurrentStatus
		, l.ListDate
		, convert(datetime2, bbl.vecontractsigneddate) ContractSignDate
		, bbl.listingnumber ListingNumber
		, bbl.webid ListingNumberReference
		, bba.addressid PostalAddressID
		, bbl.displayaddress AddressLine1
		, bbu.unitnumber_full UnitIdentifier
		, bba.city CityTown
		, bba.state StateProvince
		, bba.postalcode PostalCode
		, convert(varchar, bba.longitude) Longitude
		, convert(varchar, bba.latitude) Latitude
		, convert(float, bbl.closingprice) ClosingPrice
		, convert(float, bbl.buyercommission) BuyerCommission
		, convert(float, bbl.sellercommission) SellerCommission
		, convert(float, bbl.totalcommission) TotalCommission
		, convert(varchar, NULL) PricePerSqFt
		, convert(varchar, NULL) HasOH
		, convert(varchar, bbl.taxes)  RETaxes
		, bbp.doorman DoorMan
		, bba.cross_street CrossStreets
		, convert(varchar, bbl.price) LastAskPrice
		, bbp.newconstruction IsNew
		, convert(float, bbl.associationfee) AssociationFee
		, convert(float, bbu.bathrooms) Bathrooms
		, convert(float, bbu.bedrooms) Bedrooms
		, convert(float, bbu.halfbaths) HalfBathrooms
		, convert(float, bbu.totalrooms) Rooms
		, convert(float, bbu.totalsqft) SquareFootage
		, convert(float, bbp.elevators) Elevators
		, convert(float, bbp.financingallowedpercent) FinanceAllowedPercent
		, convert(float, bbp.totalfloors) Floors
		, convert(float, bbp.totalshares) Shares
		, convert(float, bbp.taxdeduction) TaxDeductPct
		, convert(float, bbp.totalunits) Units
		, convert(float, bbp.yearbuilt) YearBuilt
		, convert(float, bbp.yearincorporated) YearIncorporated
		, convert(float, NULL) MaintCC
		, convert(float, bbu.maidbedrooms) MaidBed
		, convert(float, bbu.maidbathrooms) MaidBath
		, pa.[views] AptViews
		, convert(varchar, NULL) Exposure
		, convert(varchar, NULL) AptType
		, bbu.haswashdry HasWashDry
		, bbu.hasac HasAc
		, bbu.hasfireplace HasFireplace
		, bbp.fliptax FlipTax
		, dbo.sfGetPropertyAttribute(p.PropertyID, 34) Facade
		, bbp.storage HasStorage
		, bbp.laundry HasLaundry
		, bbp.pool HasPool
		, convert(varchar, bbp.offstreetparkingspots) HasParking
		, bbp.healthclub HasHealthClub
		, bbu.heatingtype HeatType
		, dbo.sfGetListingAttributeByID(l.ListingID, 114) HouseStyle
		, convert(float, NULL) BuildingSize
		, bbu.isfurnished IsFurnished
		, bbl.leaseterm LeaseTerm
		, bbp.lotsize LotSize	
		, convert(varchar,bba.block) Block	
		, convert(varchar,bba.lot) Lot	
		, bbp.landmark IsLandmark
		, bbp.zoning Zoning	
		, bbu.certificateofoccupancy CertOfOcc
		, convert(varchar, NULL) BuildingExtension
		, bbl.delivered_vacant IsDeliveredVacant
		, dbo.sfGetListingAttributeByID(l.ListingID, 114) StructureStyle
		, convert(datetime2, bbl.modifieddate) ListingUpdated
		, convert(datetime2, bbl.createddate) ListingCreated
		, convert(numeric, bbl.furnishedrent) FurnishedPrice
		, ea.FullName
		, ea.FullPhoneNumber
		, ea.EmailAddress
		, convert(varchar, NULL) NeighborhoodCode
		, bbp.airconditioningtype typeofac
		, convert(varchar, NULL) managementcompany
		, convert(varchar, NULL) petsallowed
		, convert(varchar,datediff(d,l.ListDate,getdate())) as DOM
		, '0' PriceIndicator
	from dbo.tbListing l
		inner join dbo.listings bbl on l.ListingID = bbl.listingid
		inner join dbo.units bbu on bbl.unitid = bbu.unitid
		inner join dbo.properties bbp on bbl.propertyid = bbp.propertyid
		inner join dbo.addresses bba on bbp.addressid = bba.addressid
		left join dbo.big_text bbd on bbl.listingid = bbd.recordid --and bbd.recordtype = 'L'
		left join dbo.tbProperty p on l.PropertyID = p.PropertyID
		left join dbo.vwPropertyAttributePivot pa on p.PropertyID = pa.PropertyID
		left join dbo.vwPropertyAttributePivot ppa on p.ParentPropertyID = ppa.PropertyID
		left join dbo.vwExclusiveAgent ea on l.ListingID = ea.ListingID
	where l.IsDeleted = 0