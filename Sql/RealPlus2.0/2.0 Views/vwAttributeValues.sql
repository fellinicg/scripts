use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwAttributeValues'))
drop view dbo.vwAttributeValues
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160229
-- ============================================================
create view dbo.vwAttributeValues
--with schemabinding
as

	select 
		a.AttributeValueID
		, b.AttributeID
		, c.ValueID
		, b.AttributeName
		, c.ValueName
		, a.IsDeleted
		, a.EtlID
	from dbo.tbAttributeValue a
		inner join dbo.tbAttribute b on a.AttributeID = b.AttributeID
		inner join dbo.tbValue c on a.ValueID = c.ValueID