use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwListing'))
drop view dbo.vwListing
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160321
-- ============================================================
create view dbo.vwListing
--with schemabinding
as

	select 
		l.ListingID
		, p.PropertyID
		, dbo.sfGetAttributeValue(l.ListingProviderID) 'ListingProvider'
		, dbo.sfGetAttributeValue(l.ListingCategoryID) 'ListingCategory'
		, dbo.sfGetAttributeValue(l.ListingTypeID) 'ListingType'
		, l.InitialPrice
--		, clp.ListingPrice 'CurrentPrice'
		, dbo.sfGetListingAttributeNumeric(l.ListingID, 25) 'ClosingPrice'
--		, cs.ValueName 'CurrentStatus'
		, dbo.sfGetListingAttributeNumeric(l.ListingID, 20) 'BuyerCommission'
		, dbo.sfGetListingAttributeNumeric(l.ListingID, 88) 'SellerCommission'
		, dbo.sfGetListingAttributeNumeric(l.ListingID, 98) 'TotalCommission'
		, dbo.sfGetListingAttributeText(l.ListingID, 53) 'ListingNumber'
		, dbo.sfGetListingAttributeText(l.ListingID, 54) 'ListingNumberReference'
		, l.ListDate
		, dbo.sfGetAttributeValue(p.PropertyClassID) 'PropertyClass'
		----, coalesce(p.Line1, bld.Line1) 'Line1'
		, dbo.sfGetPropertyAttributeText(p.PropertyID, 100) 'UnitIdentifier'
		----, coalesce(p.CityTown, bld.CityTown) 'CityTown'
		----, coalesce(p.StateProvince, bld.StateProvince) 'StateProvince'
		----, coalesce(p.PostalCode, bld.PostalCode) 'PostalCode'
		, dbo.sfGetPropertyAttributeNumeric(p.PropertyID, 9) 'AssociationFee'
		, dbo.sfGetPropertyAttributeNumeric(p.PropertyID, 10) 'Bathrooms'
		, dbo.sfGetPropertyAttributeNumeric(p.PropertyID, 11) 'Bedrooms'
		, dbo.sfGetPropertyAttributeNumeric(p.PropertyID, 41) 'HalfBathrooms'
		, dbo.sfGetPropertyAttributeNumeric(p.PropertyID, 85) 'Rooms'
		, dbo.sfGetPropertyAttributeNumeric(p.PropertyID, 91) 'SquareFootage'
		, dbo.sfGetPropertyAttributeNumeric(p.ParentPropertyID, 31) 'Elevators'
		, dbo.sfGetPropertyAttributeNumeric(p.ParentPropertyID, 36) 'FinanceAllowedPercent'
		, dbo.sfGetPropertyAttributeText(p.ParentPropertyID, 37) 'FlipTax'
		, dbo.sfGetPropertyAttributeNumeric(p.ParentPropertyID, 39) 'Floors'
		, dbo.sfGetPropertyAttributeNumeric(p.ParentPropertyID, 89) 'Shares'
		, dbo.sfGetPropertyAttributeNumeric(p.ParentPropertyID, 97) 'TaxDeductPct'
		, dbo.sfGetPropertyAttributeNumeric(p.ParentPropertyID, 10) 'Units'
		, dbo.sfGetPropertyAttributeNumeric(p.ParentPropertyID, 106) 'YearBuilt'
		, dbo.sfGetPropertyAttributeNumeric(p.ParentPropertyID, 107) 'YearIncorporated'
		, l.UpdateDateTime 'ListingUpdated'
		, l.CreateDateTime 'ListingCreated'
	from dbo.tbListing l
		left join dbo.tbProperty p on l.PropertyID = p.PropertyID
