use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwAttributes'))
drop view dbo.vwAttributes
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160303
-- ============================================================
create view dbo.vwAttributes
as

	select
		AttributeID
      , AttributeName
      , IsDeleted
	from dbo.tbAttribute