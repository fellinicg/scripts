use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwListingCategory'))
drop view dbo.vwListingCategory
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160224
-- ============================================================
create view dbo.vwListingCategory
as

	select 
		a.AttributeValueID
		, b.ValueName
		, a.IsDeleted 'AttributeValueIsDeleted'
		, b.IsDeleted 'ValueIsDeleted'
	from dbo.tbAttributeValue a
		inner join dbo.tbValue b on a.ValueID = b.ValueID
	where 
		a.AttributeID = 17