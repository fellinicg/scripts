use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwView'))
drop view dbo.vwView
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160311
-- ============================================================
create view dbo.vwView
as

	select a.AttributeValueID, b.AttributeName, c.ValueName, a.Display, a.IsDeleted, a.EtlID
	from dbo.tbAttributeValue a
		inner join dbo.tbAttribute b on a.AttributeID = b.AttributeID
		inner join dbo.tbValue c on a.ValueID = c.ValueID
	where b.AttributeName = 'View'