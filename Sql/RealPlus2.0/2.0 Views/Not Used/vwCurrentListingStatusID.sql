use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwCurrentListingStatusID'))
drop view dbo.vwCurrentListingStatusID
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160321
-- ============================================================
create view dbo.vwCurrentListingStatusID
as

	select ls.ListingID, av.AttributeValueID
	from dbo.tbListingStatus ls
		inner join dbo.tfCurrentListingStatus() lls on ls.ListingID = lls.ListingID and ls.StatusDateTime = lls.StatusDateTime
		inner join dbo.tbAttributeValue av on ls.StatusID = av.AttributeValueID
go
