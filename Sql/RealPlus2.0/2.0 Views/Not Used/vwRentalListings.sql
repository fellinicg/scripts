use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwRentalListings'))
drop view dbo.vwRentalListings
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160224
-- ============================================================
create view dbo.vwRentalListings
as

	select ListingID, ListingCategoryID, PostalAddressID, AgentID, OwnerID, IsDeleted, CreateDateTime, UpdateDateTime, ListingIDv15
	from dbo.tbListing
	where 
		ListingCategoryID = 770