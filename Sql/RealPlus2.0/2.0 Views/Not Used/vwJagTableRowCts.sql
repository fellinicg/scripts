use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwJagTableRowCts'))
drop view dbo.vwJagTableRowCts
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160202
-- ============================================================
create view dbo.vwJagTableRowCts
as

	select
	--	schema_name(a.schema_id) + '.' + a.Name
		a.Name 'TableName'
		, max(b.rows) 'RecordCount'
	from Jaguar_v2_Development.sys.objects a
	inner join Jaguar_v2_Development.sys.partitions b ON a.object_id = b.object_id
	where a.type = 'U'
	group by a.Name
