use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwLastestListingStatus'))
drop view dbo.vwLastestListingStatus
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160321
-- ============================================================
create view dbo.vwLastestListingStatus
with schemabinding
as

	select distinct ListingID, max(StatusDateTime) StatusDateTime 
	from dbo.tbListingStatus 
	group by ListingID

go
