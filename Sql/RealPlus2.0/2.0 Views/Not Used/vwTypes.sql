use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwTypes'))
drop view dbo.vwTypes
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160229
-- ============================================================
create view dbo.vwTypes
as

	select
		TypeID
      , TypeName
      , IsDeleted
	from dbo.tbType