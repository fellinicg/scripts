use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwPropertyListing'))
drop view dbo.vwPropertyListing
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160315
-- ============================================================
create view dbo.vwPropertyListing
as

	select 
		l.ListingID
		, p.PropertyID
		, l.EtlID 'ListingEtlID'
		, p.EtlID 'PropertyEtlID'
		--, p.PropertyClassID
		--, pc.ValueName 'PropertyClass'
		--, p.Block
		--, p.Lot
		--, p.Section
		--, p.Latitude
		--, p.Longitude
		--, p.IsDeleted 'PropertyIsDeleted'
		--, pa.PropertyAddressID
		--, a.Line1
		--, a.Line2
		--, a.CityTown
		--, a.StateProvince
		--, a.PostalCode
		--, pa.IsPrimary
		--, pa.IsDeleted 'PropertyAddressIsDeleted'
	from dbo.tbListing l
		--inner join dbo.tbPostalAddress a on l.PostalAddressID = a.PostalAddressID
		inner join dbo.tbPropertyAddress pa on l.PostalAddressID = pa.PostalAddressID
		inner join dbo.tbProperty p on pa.PropertyID = p.PropertyID
		--left join dbo.vwAttributeValues pc on p.PropertyClassID = pc.AttributeValueID
		--left join dbo.tbPostalAddress a on pa.PostalAddressID = a.PostalAddressID