use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwJagTables'))
drop view dbo.vwJagTables
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160205
-- ============================================================
create view dbo.vwJagTables
as

	select 
		t.name 'TableName'
		, c.name 'Column'
		, s.name 'DataType'
		, c.max_length 'Length'
		, case c.is_identity when 0 then 'No' else 'Yes' end 'Identity'
		, case c.is_nullable when 0 then 'No' else 'Yes' end 'Nullable'
		, c.column_id 'ColNum'
	from Jaguar_v2_Development.sys.tables t
	inner join Jaguar_v2_Development.sys.columns c on t.object_id = c.object_id
	inner join Jaguar_v2_Development.sys.types s on c.system_type_id = s.system_type_id
	where t.type = 'U'