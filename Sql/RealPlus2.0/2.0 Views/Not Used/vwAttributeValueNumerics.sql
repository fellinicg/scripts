use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwAttributeValueNumerics'))
drop view dbo.vwAttributeValueNumerics
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160311
-- ============================================================
create view dbo.vwAttributeValueNumerics
as

	select a.AttributeValueID, b.AttributeID, b.AttributeName, c.ValueName, d.NumericValue, a.Display, a.SortOrder, a.IsDeleted, a.EtlID
	from dbo.tbAttributeValue a
		inner join dbo.tbAttribute b on a.AttributeID = b.AttributeID
		inner join dbo.tbValue c on a.ValueID = c.ValueID
		left join dbo.tbAttributeValueNumeric d on a.AttributeValueID = d.AttributeValueID