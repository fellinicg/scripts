use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwListingAttributeText'))
drop view dbo.vwListingAttributeText
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160329
-- ============================================================
create view dbo.vwListingAttributeText
as
	select
		ListingID
		, coalesce(max(case when AttributeID = 53 then AttributeValue end), null) as ListingNumber
		, coalesce(max(case when AttributeID = 54 then AttributeValue end), null) as ListingNumberReference
	from dbo.tbListingAttributeText
	group by ListingID