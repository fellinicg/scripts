use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAttributeValueAttribute') and parent_object_id = object_id(N'dbo.tbAttributeValue'))
alter table dbo.tbAttributeValue drop constraint fktbAttributeValueAttribute
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingAttributeNumericAttribute') and parent_object_id = object_id(N'dbo.tbListingAttributeNumeric'))
alter table dbo.tbListingAttributeNumeric drop constraint fktbListingAttributeNumericAttribute
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingAttributeTextAttribute') and parent_object_id = object_id(N'dbo.tbListingAttributeText'))
alter table dbo.tbListingAttributeText drop constraint fktbListingAttributeTextAttribute
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyAttributeNumericAttribute') and parent_object_id = object_id(N'dbo.tbPropertyAttributeNumeric'))
alter table dbo.tbPropertyAttributeNumeric drop constraint fktbPropertyAttributeNumericAttribute
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyAttributeTextAttribute') and parent_object_id = object_id(N'dbo.tbPropertyAttributeText'))
alter table dbo.tbPropertyAttributeText drop constraint fktbPropertyAttributeTextAttribute
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttribute') and type in (N'U'))
drop table dbo.tbAttribute
go
create table dbo.tbAttribute(
	AttributeID int identity(1,1) not null,
	AttributeCode varchar(10) null,
	AttributeName varchar(100) not null,
	IsDeleted bit not null constraint dftbAttributeIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbAttributeCreateDate  default (getdate()),
	CreateID int not null constraint dftbAttributeCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbAttributeUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbAttributeUpdateID  default (0),
 constraint pktbAttribute primary key clustered 
(
	AttributeID asc
) on [PRIMARY],
 constraint aktbAttribute unique nonclustered 
(
	AttributeName ASC
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAttribute', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains distinct values that will be used as attributes in the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAttribute'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAttribute', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAttribute', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAttribute', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAttribute', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAttribute', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAttribute', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbAttributeValue with check add constraint fktbAttributeValueAttribute
foreign key(AttributeID) references dbo.tbAttribute (AttributeID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingAttributeNumeric') and type in (N'U'))
alter table dbo.tbListingAttributeNumeric with check add constraint fktbListingAttributeNumericAttribute
foreign key(AttributeID) references dbo.tbAttribute (AttributeID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingAttributeText') and type in (N'U'))
alter table dbo.tbListingAttributeText with check add constraint fktbListingAttributeTextAttribute
foreign key(AttributeID) references dbo.tbAttribute (AttributeID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPropertyAttributeNumeric') and type in (N'U'))
alter table dbo.tbPropertyAttributeNumeric with check add constraint fktbPropertyAttributeNumericAttribute
foreign key(AttributeID) references dbo.tbAttribute (AttributeID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPropertyAttributeText') and type in (N'U'))
alter table dbo.tbPropertyAttributeText with check add constraint fktbPropertyAttributeTextAttribute
foreign key(AttributeID) references dbo.tbAttribute (AttributeID)
go