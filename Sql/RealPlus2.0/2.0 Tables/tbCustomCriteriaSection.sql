use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCustomCriteriaSection') and type in (N'U'))
drop table dbo.tbCustomCriteriaSection
go
create table dbo.tbCustomCriteriaSection(
	CustomCriteriaSectionID int identity(1,1) not null,
	ShortLabel varchar(20) null,
	LongLabel varchar(100) not null,
	IsDeleted bit not null constraint dftbCustomCriteriaSectionIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbCustomCriteriaSectionCreateDate  default (getdate()),
	CreateID int not null constraint dftbCustomCriteriaSectionCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbCustomCriteriaSectionUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbCustomCriteriaSectionUpdateID  default (0),
 constraint pkCustomCriteriaSectionID primary key clustered 
(
	CustomCriteriaSectionID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomCriteriaSection', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the sections on the search criteria pages to be used to customize lists.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomCriteriaSection'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomCriteriaSection', N'COLUMN', N'ShortLabel'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Custom short label.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomCriteriaSection', @level2type=N'COLUMN',@level2name=N'ShortLabel'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomCriteriaSection', N'COLUMN', N'LongLabel'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Custom long label.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomCriteriaSection', @level2type=N'COLUMN',@level2name=N'LongLabel'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomCriteriaSection', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomCriteriaSection', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomCriteriaSection', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomCriteriaSection', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomCriteriaSection', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomCriteriaSection', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
