use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbUserRoleRole') and parent_object_id = object_id(N'dbo.tbUserRole'))
alter table dbo.tbUserRole drop constraint fktbUserRoleRole
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbRole') and type in (N'U'))
drop table dbo.tbRole
go
create table dbo.tbRole(
	RoleID tinyint identity(1,1) not null,
	RoleName varchar(50) not null,
	IsDeleted bit constraint dftbRoleIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbRoleCreateDate  default (getdate()),
	CreateID int not null constraint dftbRoleCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbRoleUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbRoleUpdateID  default (0),
 constraint pktbRole primary key clustered 
(
	RoleID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbRole', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the roles used for access control to various parts of the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbRole'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbRole', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbRole', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbRole', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbRole', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbRole', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbRole', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbUserRole') and type in (N'U'))
alter table dbo.tbUserRole with check add constraint fktbUserRoleRole
foreign key(RoleID) references dbo.tbRole (RoleID)
go