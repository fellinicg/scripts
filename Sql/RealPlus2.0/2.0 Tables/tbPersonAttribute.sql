use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPersonAttribute') and type in (N'U'))
drop table dbo.tbPersonAttribute
go
create table dbo.tbPersonAttribute(
	PersonAttributeID int identity(1,1) not null,
	PersonID int not null,
	AttributeValueID int not null,
	IsDeleted bit not null constraint dftbPersonAttributeIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbPersonAttributeCreateDate  default (getdate()),
 	CreateID int not null constraint dftbPersonAttributeCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbPersonAttributeUpdateDateTime  default (getdate()),
 	UpdateID int not null constraint dftbPersonAttributeUpdateID  default (0),
constraint pktbPersonAttribute primary key clustered 
(
	PersonAttributeID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonAttribute', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains attribute values for a Person.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonAttribute'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonAttribute', N'COLUMN', N'PersonID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbPerson table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonAttribute', @level2type=N'COLUMN',@level2name=N'PersonID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonAttribute', N'COLUMN', N'AttributeValueID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonAttribute', @level2type=N'COLUMN',@level2name=N'AttributeValueID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonAttribute', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonAttribute', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonAttribute', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonAttribute', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonAttribute', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonAttribute', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPerson') and type in (N'U'))
alter table dbo.tbPersonAttribute with check add constraint fktbPersonAttributePerson 
foreign key(PersonID) references dbo.tbPerson (PersonID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbPersonAttribute with check add constraint fktbPersonAttributeValue 
foreign key(AttributeValueID) references dbo.tbAttributeValue (AttributeValueID)