use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentStatusStatus') and parent_object_id = object_id(N'dbo.tbAgentStatus'))
alter table dbo.tbAgentStatus drop constraint fktbAgentStatusStatus
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbCommunityType') and parent_object_id = object_id(N'dbo.tbCommunity'))
alter table dbo.tbCommunity drop constraint fktbCommunityType
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbCustomCriteriaValueAttributeValue') and parent_object_id = object_id(N'dbo.tbCustomCriteriaValue'))
alter table dbo.tbCustomCriteriaValue drop constraint fktbCustomCriteriaValueAttributeValue
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbDetailType') and parent_object_id = object_id(N'dbo.tbDetail'))
alter table dbo.tbDetail drop constraint fktbDetailType
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingAttributeValue') and parent_object_id = object_id(N'dbo.tbListingAttribute'))
alter table dbo.tbListingAttribute drop constraint fktbListingAttributeValue
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingStatusStatus') and parent_object_id = object_id(N'dbo.tbListingStatus'))
alter table dbo.tbListingStatus drop constraint fktbListingStatusStatus
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPersonAddressType') and parent_object_id = object_id(N'dbo.tbPersonAddress'))
alter table dbo.tbPersonAddress drop constraint fktbPersonAddressType
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPersonEmailType') and parent_object_id = object_id(N'dbo.tbPersonEmail'))
alter table dbo.tbPersonEmail drop constraint fktbPersonEmailType
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyAttributeValue') and parent_object_id = object_id(N'dbo.tbPropertyAttribute'))
alter table dbo.tbPropertyAttribute drop constraint fktbPropertyAttributeValue
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyPropertyClass') and parent_object_id = object_id(N'dbo.tbProperty'))
alter table dbo.tbProperty drop constraint fktbPropertyPropertyClass
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
drop table dbo.tbAttributeValue
go
create table dbo.tbAttributeValue(
	AttributeValueID int identity(1,1) not null,
	AttributeID int not null,
	ValueID int not null,
	--Display bit not null constraint dftbAttributeValueDisplay default 1,
	--SortOrder smallint not null constraint dftbAttributeValueSortOrder default 0,
	IsDeleted bit not null constraint dftbAttributeValueIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbAttributeValueCreateDate  default (getdate()),
	CreateID int not null constraint dftbAttributeValueCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbAttributeValueUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbAttributeValueUpdateID  default (0),
	EtlID int null,
 constraint pktbAttributeValue primary key clustered 
(
	AttributeValueID asc
) on [PRIMARY],
 constraint akAttributeValue unique nonclustered 
(
	AttributeID, ValueID
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAttributeValue', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the unique combination of attribute and value.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAttributeValue'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAttributeValue', N'COLUMN', N'AttributeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttribute table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAttributeValue', @level2type=N'COLUMN',@level2name=N'AttributeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAttributeValue', N'COLUMN', N'ValueID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAttributeValue', @level2type=N'COLUMN',@level2name=N'ValueID'
go
--if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAttributeValue', N'COLUMN', N'Display'))
--exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record should be displayed in the GUI.'
--, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAttributeValue', @level2type=N'COLUMN',@level2name=N'Display'
--go
--if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAttributeValue', N'COLUMN', N'SortOrder'))
--exec sys.sp_addextendedproperty @name=N'Description', @value=N'Custom sort order for the GUI at the global level.'
--, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAttributeValue', @level2type=N'COLUMN',@level2name=N'SortOrder'
--go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAttributeValue', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAttributeValue', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAttributeValue', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAttributeValue', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAttributeValue', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAttributeValue', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttribute') and type in (N'U'))
alter table dbo.tbAttributeValue with check add constraint fktbAttributeValueAttribute
foreign key(AttributeID) references dbo.tbAttribute (AttributeID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbValue') and type in (N'U'))
alter table dbo.tbAttributeValue with check add constraint fktbAttributeValueValue
foreign key(ValueID) references dbo.tbValue (ValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgentStatus') and type in (N'U'))
alter table dbo.tbAgentStatus with check add constraint fktbAgentStatusStatus
foreign key(StatusID) references dbo.tbAttributeValue (AttributeValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCommunity') and type in (N'U'))
alter table dbo.tbCommunity with check add constraint fktbCommunityType
foreign key(CommunityTypeID) references dbo.tbAttributeValue (AttributeValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCustomCriteriaValue') and type in (N'U'))
alter table dbo.tbCustomCriteriaValue with check add constraint fktbCustomCriteriaValueAttributeValue
foreign key(AttributeValueID) references dbo.tbAttributeValue (AttributeValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbDetail') and type in (N'U'))
alter table dbo.tbDetail with check add constraint fktbDetailType
foreign key(DetailTypeID) references dbo.tbAttributeValue (AttributeValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingAttribute') and type in (N'U'))
alter table dbo.tbListingAttribute with check add constraint fktbListingAttributeValue
foreign key(AttributeValueID) references dbo.tbAttributeValue (AttributeValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingStatus') and type in (N'U'))
alter table dbo.tbListingStatus with check add constraint fktbListingStatusStatus
foreign key(StatusID) references dbo.tbAttributeValue (AttributeValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPersonAddress') and type in (N'U'))
alter table dbo.tbPersonAddress with check add constraint fktbPersonAddressType
foreign key(AddressTypeID) references dbo.tbAttributeValue (AttributeValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPersonEmail') and type in (N'U'))
alter table dbo.tbPersonEmail with check add constraint fktbPersonEmailType
foreign key(EmailTypeID) references dbo.tbAttributeValue (AttributeValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPropertyAttribute') and type in (N'U'))
alter table dbo.tbPropertyAttribute with check add constraint fktbPropertyAttributeValue
foreign key(AttributeValueID) references dbo.tbAttributeValue (AttributeValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbProperty') and type in (N'U'))
alter table dbo.tbProperty with check add constraint fktbPropertyPropertyClass
foreign key(PropertyClassID) references dbo.tbAttributeValue (AttributeValueID)
go