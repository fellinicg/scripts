use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPersonAddress') and type in (N'U'))
drop table dbo.tbPersonAddress
go
create table dbo.tbPersonAddress(
	PersonAddressID int identity(1,1) not null,
	PersonID int not null,
	PostalAddressID int not null,
	AddressTypeID int not null,
	IsPreferred bit constraint dftbPersonAddressIsPreferred default 0,
	IsDeleted bit not null constraint dftbPersonAddressIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbPersonAddressCreateDate  default (getdate()),
	CreateID int not null constraint dftbPersonAddressCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbPersonAddressUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbPersonAddressUpdateID  default (0),
 constraint pktbPersonAddress primary key clustered 
 (
	PersonAddressID asc
 )with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY],
 constraint aktbPersonAddress unique nonclustered 
 (
	PersonID, PostalAddressID, AddressTypeID
 )with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonAddress', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the address to person relationship.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonAddress'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonAddress', N'COLUMN', N'PersonID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbPerson table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonAddress', @level2type=N'COLUMN',@level2name=N'PersonID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonAddress', N'COLUMN', N'PostalAddressID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbPostalAddress table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonAddress', @level2type=N'COLUMN',@level2name=N'PostalAddressID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonAddress', N'COLUMN', N'AddressTypeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonAddress', @level2type=N'COLUMN',@level2name=N'AddressTypeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonAddress', N'COLUMN', N'IsPreferred'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record is the preferred address.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonAddress', @level2type=N'COLUMN',@level2name=N'IsPreferred'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonAddress', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonAddress', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonAddress', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonAddress', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonAddress', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonAddress', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPerson') and type in (N'U'))
alter table dbo.tbPersonAddress with check add constraint fktbPersonAddressPerson foreign key(PersonID)
references dbo.tbPerson (PersonID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPostalAddress') and type in (N'U'))
alter table dbo.tbPersonAddress with check add constraint fktbPersonAddressPostalAddress foreign key(PostalAddressID)
references dbo.tbPostalAddress (PostalAddressID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbPersonAddress with check add constraint fktbPersonAddressType foreign key(AddressTypeID)
references dbo.tbAttributeValue (AttributeValueID)