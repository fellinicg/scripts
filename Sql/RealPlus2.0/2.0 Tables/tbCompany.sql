use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbOfficeCompany') and parent_object_id = object_id(N'dbo.tbOffice'))
alter table dbo.tbOffice drop constraint fktbOfficeCompany
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCompany') and type in (N'U'))
drop table dbo.tbCompany
go
create table dbo.tbCompany(
	CompanyID int identity(1,1) not null,
	CompanyName varchar(50) not null,
	IsDeleted bit not null constraint dftbCompanyIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbCompanyCreateDate  default (getdate()),
	CreateID int not null constraint dftbCompanyCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbCompanyUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbCompanyUpdateID  default (0),
 constraint pktbCompany primary key clustered 
(
	CompanyID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCompany', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the base information about a Company.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCompany'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCompany', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCompany', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCompany', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCompany', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCompany', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCompany', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
alter table dbo.tbCompany add constraint aktbCompany 
unique nonclustered (CompanyName asc) with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbOffice') and type in (N'U'))
alter table dbo.tbOffice with check add constraint fktbOfficeCompany
foreign key(CompanyID) references dbo.tbCompany (CompanyID)
go