use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingStatus') and type in (N'U'))
drop table dbo.tbListingStatus
go
create table dbo.tbListingStatus(
	ListingStatusID int identity(1,1) not null,
	ListingID int not null,
	StatusID int not null,
	--IsCurrent bit constraint dftbListingStatusIsCurrent default 1,
	IsDeleted bit not null constraint dftbListingStatusIsDeleted default 0,	
	StatusDateTime datetime2 not null,
	CreateDateTime datetime2 not null constraint dftbListingStatusCreateDate  default (getdate()),
 	CreateID int not null constraint dftbListingStatusCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbListingStatusUpdateDateTime  default (getdate()),
 	UpdateID int not null constraint dftbListingStatusUpdateID  default (0), 
	EtlID int null,
constraint pktbListingStatus primary key clustered 
(
	ListingStatusID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingStatus') and name = N'ncixtbListingStatusListing')
drop index ncixtbListingStatusListing on dbo.tbListingStatus
create nonclustered index ncixtbListingStatusListing on dbo.tbListingStatus(ListingID asc)
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingAttribute') and name = N'ncixtbListingStatusStatus')
drop index ncixtbListingStatusStatus on dbo.tbListingStatus
create nonclustered index ncixtbListingStatusStatus on dbo.tbListingStatus(StatusID asc)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingStatus', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the listing status history.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingStatus'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingStatus', N'COLUMN', N'ListingID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbListing table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingStatus', @level2type=N'COLUMN',@level2name=N'ListingID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingStatus', N'COLUMN', N'StatusID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingStatus', @level2type=N'COLUMN',@level2name=N'StatusID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingStatus', N'COLUMN', N'StatusDateTime'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Actual status datetime.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingStatus', @level2type=N'COLUMN',@level2name=N'StatusDateTime'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingStatus', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingStatus', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingStatus', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingStatus', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingStatus', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingStatus', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListing') and type in (N'U'))
alter table dbo.tbListingStatus with check add constraint fktbListingStatusListing foreign key(ListingID)
references dbo.tbListing (ListingID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbListingStatus with check add constraint fktbListingStatusStatus foreign key(StatusID)
references dbo.tbAttributeValue (AttributeValueID)