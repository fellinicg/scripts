use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbUserAccount') and type in (N'U'))
drop table dbo.tbUserAccount
go
create table dbo.tbUserAccount(
	UserAccountID int not null,
	AccountName varchar(20) not null,
	PasswordEncrypted varchar(200) not null,
	UserAccountStatusID int not null,
	IsDeleted bit not null constraint dftbUserAccountIsDeleted  default ((0)),
	CreateDateTime datetime2 not null constraint dftbUserAccountCreateDateTime  default (getdate()),
	CreateID int not null constraint dftbUserAccountCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbUserAccountUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbUserAccountUpdateID  default (0),
 constraint pktbUserAccount primary key clustered 
(
	UserAccountID ASC
) ON [PRIMARY],
 constraint aktbUserAccount unique nonclustered 
(
	AccountName ASC
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUserAccount', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the base information for all UserAccounts.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUserAccount'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUserAccount', N'COLUMN', N'UserAccountID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Primary key and foreign key to the tbPerson table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUserAccount', @level2type=N'COLUMN',@level2name=N'UserAccountID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUserAccount', N'COLUMN', N'UserAccountStatusID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbAccountValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUserAccount', @level2type=N'COLUMN',@level2name=N'UserAccountStatusID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUserAccount', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUserAccount', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUserAccount', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUserAccount', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUserAccount', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUserAccount', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPerson') and type in (N'U'))
alter table dbo.tbUserAccount with check add constraint fktbUserAccountPerson foreign key(UserAccountID)
references dbo.tbPerson (PersonID)