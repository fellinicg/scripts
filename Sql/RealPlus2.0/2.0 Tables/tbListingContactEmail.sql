use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingContactEmail') and type in (N'U'))
drop table dbo.tbListingContactEmail
go
create table dbo.tbListingContactEmail(
	ListingContactEmailID int identity(1,1) not null,
	ListingID int not null,
	ListingContactTypeID int not null,
	PersonEmailID int not null,
	IsDeleted bit constraint dftbListingContactEmailIsDeleted default 0,	
 constraint pktbListingContactEmail primary key clustered 
(
	ListingContactEmailID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingContactEmail') and name = N'ncixtbListingContactEmailListing')
drop index ncixtbListingContactEmailListing on dbo.tbListingContactEmail
create nonclustered index ncixtbListingContactEmailListing on dbo.tbListingContactEmail(ListingID asc)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingContactEmail', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains listing email contact information.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingContactEmail'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingContactEmail', N'COLUMN', N'ListingID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbListing table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingContactEmail', @level2type=N'COLUMN',@level2name=N'ListingID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingContactEmail', N'COLUMN', N'ListingContactTypeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingContactEmail', @level2type=N'COLUMN',@level2name=N'ListingContactTypeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingContactEmail', N'COLUMN', N'PersonEmailID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbPersonEmail table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingContactEmail', @level2type=N'COLUMN',@level2name=N'PersonEmailID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingContactEmail', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingContactEmail', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListing') and type in (N'U'))
alter table dbo.tbListingContactEmail with check add constraint fktbListingContactEmailListing foreign key(ListingID)
references dbo.tbListing (ListingID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbListingContactEmail with check add constraint fktbListingContactEmailContactType foreign key(ListingContactTypeID)
references dbo.tbAttributeValue (AttributeValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPersonEmail') and type in (N'U'))
alter table dbo.tbListingContactEmail with check add constraint fktbListingContactEmailPerson foreign key(PersonEmailID)
references dbo.tbPersonEmail (PersonEmailID)
go