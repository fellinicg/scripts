use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingCredit') and type in (N'U'))
drop table dbo.tbListingCredit
go
create table dbo.tbListingCredit(
	ListingCreditID int identity(1,1) not null,
	ListingID int not null,
	AgentID int not null,
	ListingCreditTypeID int not null,
	CreditPercent float null,
	IsDeleted bit not null constraint dftbListingCreditIsDeleted default 0,	
	CreditDateTime datetime2 null,
	CreateDateTime datetime2 not null constraint dftbListingCreditCreateDate  default (getdate()),
 	CreateID int not null constraint dftbListingCreditCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbListingCreditUpdateDateTime  default (getdate()),
 	UpdateID int not null constraint dftbListingCreditUpdateID  default (0),
	EtlID int null,
 constraint pktbListingCredit primary key clustered 
(
	ListingCreditID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingCredit') and name = N'ncixtbListingCreditListing')
drop index ncixtbListingCreditListing on dbo.tbListingCredit
create nonclustered index ncixtbListingCreditListing on dbo.tbListingCredit(ListingID asc)
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingCredit') and name = N'ncixtbListingCreditAgent')
drop index ncixtbListingCreditAgent on dbo.tbListingCredit
create nonclustered index ncixtbListingCreditAgent on dbo.tbListingCredit(AgentID asc)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingCredit', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the listing credit history.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingCredit'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingCredit', N'COLUMN', N'ListingID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbListing table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingCredit', @level2type=N'COLUMN',@level2name=N'ListingID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingCredit', N'COLUMN', N'AgentID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAgent table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingCredit', @level2type=N'COLUMN',@level2name=N'AgentID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingCredit', N'COLUMN', N'ListingCreditTypeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingCredit', @level2type=N'COLUMN',@level2name=N'ListingCreditTypeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingCredit', N'COLUMN', N'CreditDateTime'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Actual Credit datetime.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingCredit', @level2type=N'COLUMN',@level2name=N'CreditDateTime'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingCredit', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingCredit', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingCredit', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingCredit', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingCredit', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingCredit', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListing') and type in (N'U'))
alter table dbo.tbListingCredit with check add constraint fktbListingCreditListing foreign key(ListingID)
references dbo.tbListing (ListingID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgent') and type in (N'U'))
alter table dbo.tbListingCredit with check add constraint fktbListingCreditAgent foreign key(AgentID)
references dbo.tbAgent (AgentID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbListingCredit with check add constraint fktbListingCreditType foreign key(ListingCreditTypeID)
references dbo.tbAttributeValue (AttributeValueID)
go
