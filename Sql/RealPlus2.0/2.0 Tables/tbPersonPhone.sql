use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPersonPhone') and type in (N'U'))
drop table dbo.tbPersonPhone
go
create table dbo.tbPersonPhone(
	PersonPhoneID int identity(1,1) not null,
	PersonID int not null,
	PhoneTypeID int not null,
	FullPhoneNumber varchar(30) not null,
	AreaCode tinyint null,
	PhoneNumber int null,
	Extension smallint null,
	TxtMsgCapable bit not null constraint dftbPersonPhoneTextMsgCapable  default (0),
	IsPreferred bit not null constraint dftbPersonPhoneIsPhone default 0,
	IsDeleted bit not null constraint dftbPersonPhoneIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbPersonPhoneCreateDate  default (getdate()),
	CreateID int not null constraint dftbPersonPhoneCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbPersonPhoneUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbPersonPhoneUpdateID  default (0),
 constraint pkPersonPhoneID primary key clustered 
 (
	PersonPhoneID ASC
 )with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY],
 constraint akPersonPhoneID unique nonclustered 
 (
	PersonID, PhoneTypeID, FullPhoneNumber
 )with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonPhone', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the phone to person relationship.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonPhone'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonPhone', N'COLUMN', N'PersonID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbPerson table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonPhone', @level2type=N'COLUMN',@level2name=N'PersonID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonPhone', N'COLUMN', N'PhoneTypeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonPhone', @level2type=N'COLUMN',@level2name=N'PhoneTypeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonPhone', N'COLUMN', N'TxtMsgCapable'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record is text message capable.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonPhone', @level2type=N'COLUMN',@level2name=N'TxtMsgCapable'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonPhone', N'COLUMN', N'IsPreferred'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record is the preferred Phone.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonPhone', @level2type=N'COLUMN',@level2name=N'IsPreferred'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonPhone', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonPhone', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonPhone', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonPhone', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonPhone', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonPhone', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPerson') and type in (N'U'))
alter table dbo.tbPersonPhone  with check add  constraint fktbPersonPhonePerson foreign key(PersonID)
references dbo.tbPerson (PersonID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbPersonPhone with check add constraint fktbPersonPhoneType foreign key(PhoneTypeID)
references dbo.tbAttributeValue (AttributeValueID)
