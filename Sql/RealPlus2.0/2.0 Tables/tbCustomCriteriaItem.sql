use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCustomCriteriaItem') and type in (N'U'))
drop table dbo.tbCustomCriteriaItem
go
create table dbo.tbCustomCriteriaItem(
	CustomCriteriaItemID int identity(1,1) not null,
	CustomCriteriaSectionID int not null,
	AttributeValueID int null,
	CompanyID int null,
	ShortLabel varchar(20) null,
	LongLabel varchar(100) not null,
	NumericValue decimal(12,2) null,
	SortOrder tinyint not null constraint dftbCustomCriteriaItemSortOrder default 0,
	Display bit not null constraint dftbCustomCriteriaItemDisplay default 1,	
	IsDeleted bit not null constraint dftbCustomCriteriaItemIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbCustomCriteriaItemCreateDate  default (getdate()),
	CreateID int not null constraint dftbCustomCriteriaItemCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbCustomCriteriaItemUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbCustomCriteriaItemUpdateID  default (0),
 constraint pkCustomCriteriaItemID primary key clustered 
(
	CustomCriteriaItemID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomCriteriaItem', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains item entries and labels for the search criteria properties page at the global and company level.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomCriteriaItem'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomCriteriaItem', N'COLUMN', N'AttributeValueID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomCriteriaItem', @level2type=N'COLUMN',@level2name=N'AttributeValueID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomCriteriaItem', N'COLUMN', N'CompanyID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbCompany table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomCriteriaItem', @level2type=N'COLUMN',@level2name=N'CompanyID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomCriteriaItem', N'COLUMN', N'ShortLabel'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Custom short label for possible use in GUI.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomCriteriaItem', @level2type=N'COLUMN',@level2name=N'ShortLabel'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomCriteriaItem', N'COLUMN', N'LongLabel'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Custom label for use in the GUI.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomCriteriaItem', @level2type=N'COLUMN',@level2name=N'LongLabel'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomCriteriaItem', N'COLUMN', N'SortOrder'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Custom sort order for the GUI at the global and company level.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomCriteriaItem', @level2type=N'COLUMN',@level2name=N'SortOrder'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomCriteriaItem', N'COLUMN', N'Display'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record should be displayed in the GUI at the global and company level.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomCriteriaItem', @level2type=N'COLUMN',@level2name=N'Display'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomCriteriaItem', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomCriteriaItem', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomCriteriaItem', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomCriteriaItem', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomCriteriaItem', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomCriteriaItem', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCustomCriteriaSection') and type in (N'U'))
alter table dbo.tbCustomCriteriaItem with check add constraint fktbCustomCriteriaSection
foreign key(CustomCriteriaSectionID) references dbo.tbCustomCriteriaSection (CustomCriteriaSectionID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCompany') and type in (N'U'))
alter table dbo.tbCustomCriteriaItem with check add constraint fktbCustomCriteriaItemCompany
foreign key(CompanyID) references dbo.tbCompany (CompanyID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbCustomCriteriaItem with check add constraint fktbCustomCriteriaItemAttributeValue
foreign key(AttributeValueID) references dbo.tbAttributeValue (AttributeValueID)