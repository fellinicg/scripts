use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPropertyAlias') and type in (N'U'))
drop table dbo.tbPropertyAlias
go
create table dbo.tbPropertyAlias(
	PropertyAliasID int identity(1,1) not null,
	PropertyID int not null,
	PropertyAliasName varchar(100) not null,
	IsDeleted bit constraint dftbPropertyAliasIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbPropertyAliasCreateDate  default (getdate()),
	UpdateDateTime datetime2 not null constraint dftbPropertyAliasUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbPropertyAliasUpdateID  default (0),
 constraint pktbPropertyAlias primary key clustered 
(
	PropertyAliasID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAlias', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains an alias for a property.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAlias'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAlias', N'COLUMN', N'PropertyID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbProperty table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAlias', @level2type=N'COLUMN',@level2name=N'PropertyID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAlias', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAlias', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAlias', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAlias', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbProperty') and type in (N'U'))
alter table dbo.tbPropertyAlias with check add constraint fktbPropertyAliasProperty foreign key(PropertyID)
references dbo.tbProperty (PropertyID)