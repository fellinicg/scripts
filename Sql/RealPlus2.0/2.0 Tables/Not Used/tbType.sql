use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
--if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbType') and parent_object_id = object_id(N'dbo.tbUserAccount'))
--alter table dbo.tbUserAccount drop constraint fktbType
--go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbType') and type in (N'U'))
drop table dbo.tbType
go
create table dbo.tbType(
	TypeID int identity(1,1) not null,
--	TypeCode varchar(10) not null,
	TypeName varchar(100) not null,
	IsDeleted bit constraint dftbTypeIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbTypeCreateDate  default (getdate()),
 constraint pktbType primary key clustered 
(
	TypeID asc
) on [PRIMARY],
 constraint aktbType unique nonclustered 
(
	TypeName ASC
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
--go
--alter table dbo.tbUserAccount  with check add  constraint fktbType foreign key(AccountStatus)
--references dbo.tbType (AccountStatus)