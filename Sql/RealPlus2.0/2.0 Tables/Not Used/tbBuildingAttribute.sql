use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbBuildingAttribute') and type in (N'U'))
drop table dbo.tbBuildingAttribute
go
create table dbo.tbBuildingAttribute(
	BuildingAttributeID int identity(1,1) not null,
	BuildingID int not null,
	AttributeValueID int not null,
	IsDeleted bit constraint dftbBuildingAttributeIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbBuildingAttributeCreateDate  default (getdate()),
	UpdateDateTime datetime2 not null constraint dftbBuildingAttributeUpdateDateTime  default (getdate()),
 constraint pktbBuildingAttribute primary key clustered 
(
	BuildingAttributeID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
alter table dbo.tbBuildingAttribute with check add constraint fktbBuildingAttributeBuilding foreign key(BuildingID)
references dbo.tbBuilding (BuildingID)
go
alter table dbo.tbBuildingAttribute with check add constraint fktbBuildingAttributeValue foreign key(AttributeValueID)
references dbo.tbAttributeValue (AttributeValueID)