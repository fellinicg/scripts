use tmf_RealPlus2
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingType') and type in (N'U'))
drop table dbo.tbListingType
go
create table dbo.tbListingType(
	ListingTypeID tinyint identity(1,1) not null,
	IsDeleted bit constraint dftbListingTypeIsDeleted default 0,	
	CreateDateTime datetime not null constraint dftbListingTypeCreateDate  default (getdate()),
	UpdateDateTime datetime NOT NULL constraint dftbListingTypeUpdateDateTime  default (getdate()),
 constraint pktbListingType primary key clustered 
(
	ListingTypeID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
