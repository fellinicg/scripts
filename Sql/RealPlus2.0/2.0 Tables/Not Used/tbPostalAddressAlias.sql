use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPostalAddressAlias') and type in (N'U'))
drop table dbo.tbPostalAddressAlias
go
create table dbo.tbPostalAddressAlias(
	PostalAddressAliasID int identity(1,1) not null,
	PostalAddressID int not null,
	PostalAddressAliasName varchar(100) not null,
	IsDeleted bit constraint dftbPostalAddressAliasIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbPostalAddressAliasCreateDate  default (getdate()),
	UpdateDateTime datetime2 not null constraint dftbPostalAddressAliasUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbPostalAddressAliasUpdateID  default (0),
 constraint pktbPostalAddressAlias primary key clustered 
(
	PostalAddressAliasID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPostalAddressAlias', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the base information about a PostalAddressAlias.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPostalAddressAlias'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPostalAddressAlias', N'COLUMN', N'PostalAddressID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbPostalAddress table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPostalAddressAlias', @level2type=N'COLUMN',@level2name=N'PostalAddressID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPostalAddressAlias', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPostalAddressAlias', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPostalAddressAlias', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPostalAddressAlias', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
alter table dbo.tbPostalAddressAlias with check add constraint fktbPostalAddressAliasPostalAddress foreign key(PostalAddressID)
references dbo.tbPostalAddress (PostalAddressID)