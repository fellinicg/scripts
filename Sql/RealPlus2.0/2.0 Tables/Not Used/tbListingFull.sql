use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingFull') and type in (N'U'))
drop table dbo.tbListingFull
go
create table dbo.tbListingFull(
	ListingID int not null,
	ListingNumber varchar(20) not null,
	ListingNumberReference varchar(20) null,
	ListingAgentID int not null,
	ListingOfficeID int not null,
	ListingTypeID int not null,
	ListingProviderID int not null,
	BuildingID int null,
	UnitNumber int null,
	SubUnitNumber varchar(20) null,
	WebNeighborhoodID int null,
	AddressDisplayTypeID int null,
	SchoolDistrictID int null,
	ElementarySchoolID int null,
	MiddleSchoolID int null,
	HighSchoolID int null,
	ZoningTypeID int null,
	WaterTypeID int null,
	SewerTypeID int null,
	FuelTypeID int null,
	NewConstruction bit null,
	OwnerName varchar(50) null,
	OwnerPhoneNumber varchar(20) null,
	OwnerViewRightID int null,
	OwnerAddress varchar(40) null,
	OwnerUnitNumber varchar(15) null,
	OwnerCityID int null,
	OwnerStateID int null,
	OwnerPostalCodeID int null,
	CertificateOfOccupancy bit null,
	Acres float null,
	Plat varchar(50) null,
	Parcel varchar(50) null,
	LegalDescription varchar(100) null,
	Lot varchar(20) null,
	Block varchar(20) null,
	LotSize varchar(20) null,
	FrontFootage float null,
	DepthFootage float null,
	TaxClassID int null,
	KeyLocationID int null,
	FeaturedProperty bit not null,
	Sponsored bit not null,
	ViewDescription varchar(150) null,
	SyndicationGroupID int null,
	NextReviewDate datetime null,
	ListedDate datetime not null,
	CreateDate datetime not null,
	ModDate datetime not null,
	UnitNumberFull varchar(40) null,
	MLS_ID varchar(20) null,
	IsTownhouse bit null,
	NoBoardApproval bit null,
	UnitNumberSort varchar(50) null,
	PendingApprovalTypeID int null,
 constraint pktbListingFull primary key clustered 
(
	ListingID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off