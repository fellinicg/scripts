use tmf_RealPlus2
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbFieldType') and type in (N'U'))
drop table dbo.tbFieldType
go
create table dbo.tbFieldType(
	FieldTypeID tinyint identity(1,1) not null,
	Detail varchar(50) not null,
	IsDeleted bit constraint dftbFieldTypeIsDeleted default 0,	
	CreateDateTime datetime not null constraint dftbFieldTypeCreateDate  default (getdate()),
	UpdateDateTime datetime NOT NULL constraint dftbFieldTypeUpdateDateTime  default (getdate()),
 constraint pktbFieldType primary key clustered 
(
	FieldTypeID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
