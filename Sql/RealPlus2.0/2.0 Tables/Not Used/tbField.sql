use tmf_RealPlus2
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbField') and type in (N'U'))
drop table dbo.tbField
go
create table dbo.tbField(
	FieldID int identity(1,1) not null,
	FieldType tinyint not null,
	Detail varchar(50) not null,
	IsDeleted bit constraint dftbFieldIsDeleted default 0,	
	CreateDateTime datetime not null constraint dftbFieldCreateDate  default (getdate()),
	UpdateDateTime datetime NOT NULL constraint dftbFieldUpdateDateTime  default (getdate()),
 constraint pktbField primary key clustered 
(
	FieldID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
