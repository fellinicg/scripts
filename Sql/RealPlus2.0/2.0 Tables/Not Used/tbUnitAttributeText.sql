use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbUnitAttributeText') and type in (N'U'))
drop table dbo.tbUnitAttributeText
go
create table dbo.tbUnitAttributeText(
	UnitAttributeTextID int identity(1,1) not null,
	UnitID int not null,
	AttributeID int not null,
	AttributeValue varchar(200) not null,
	IsDeleted bit not null constraint dftbUnitAttributeTextIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbUnitAttributeTextCreateDate  default (getdate()),
	CreateID int not null constraint dftbUnitAttributeTextCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbUnitAttributeTextUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbUnitAttributeTextUpdateID  default (0),
 constraint pktbUnitAttributeText primary key clustered 
(
	UnitAttributeTextID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUnitAttributeText', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains text attribute values for a unit where the value is both Text and is not stored in tbAttributeValue.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUnitAttributeText'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUnitAttributeText', N'COLUMN', N'UnitID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbUnit table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUnitAttributeText', @level2type=N'COLUMN',@level2name=N'UnitID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUnitAttributeText', N'COLUMN', N'AttributeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttribute table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUnitAttributeText', @level2type=N'COLUMN',@level2name=N'AttributeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUnitAttributeText', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUnitAttributeText', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUnitAttributeText', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUnitAttributeText', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUnitAttributeText', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUnitAttributeText', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
alter table dbo.tbUnitAttributeText with check add constraint fktbUnitAttributeTextUnit foreign key(UnitID)
references dbo.tbUnit (UnitID)
go
alter table dbo.tbUnitAttributeText with check add constraint fktbUnitAttributeTextAttribute foreign key(AttributeID)
references dbo.tbAttribute (AttributeID)