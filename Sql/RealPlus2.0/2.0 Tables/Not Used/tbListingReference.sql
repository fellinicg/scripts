use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingReference') and type in (N'U'))
drop table dbo.tbListingReference
go
create table dbo.tbListingReference(
	ListingReferenceID int identity(1,1) not null,
	ListingID int not null,
	ListingReference1 varchar(100) null,
	ListingReference2 varchar(100) null,
	CreateDateTime datetime2 not null constraint dftbListingReferenceCreateDate  default (getdate()),
	UpdateDateTime datetime2 not null constraint dftbListingReferenceUpdateDateTime  default (getdate()),
 constraint pktbListingReference primary key clustered 
(
	ListingReferenceID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
alter table dbo.tbListingReference with check add constraint fktbListingReferenceListing foreign key(ListingID)
references dbo.tbListing (ListingID)