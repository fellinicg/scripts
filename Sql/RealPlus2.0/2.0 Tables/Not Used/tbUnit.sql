use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbUnitAttributeNumericUnit') and parent_object_id = object_id(N'dbo.tbUnitAttributeNumeric'))
alter table dbo.tbUnitAttributeNumeric drop constraint fktbUnitAttributeNumericUnit
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbUnitAttributeTextUnit') and parent_object_id = object_id(N'dbo.tbUnitAttributeText'))
alter table dbo.tbUnitAttributeText drop constraint fktbUnitAttributeTextUnit
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbUnitAttributeUnit') and parent_object_id = object_id(N'dbo.tbUnitAttribute'))
alter table dbo.tbUnitAttribute drop constraint fktbUnitAttributeUnit
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbUnit') and type in (N'U'))
drop table dbo.tbUnit
go
create table dbo.tbUnit(
	UnitID int identity(1,1) not null,
	PropertyID int not null,
	UnitTypeID int not null,
	--Unit varchar(40) not null,
	--UnitNumber int null,
	--UnitAlpha varchar(40) null,
	--Bedrooms tinyint null,
	--HalfBedrooms tinyint null,
	--Bathrooms tinyint null,
	--HalfBathrooms tinyint null,
	IsDeleted bit not null constraint dftbUnitIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbUnitCreateDate  default (getdate()),
	CreateID int not null constraint dftbCreateUpdateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbUnitUpdateDate  default (getdate()),
	UpdateID int not null constraint dftbUnitUpdateID  default (0),
 constraint pktbUnit primary key clustered 
(
	UnitID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUnit', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the base information for units.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUnit'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUnit',  N'COLUMN', N'PropertyID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbProperty table.', @level0type=N'SCHEMA'
,@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUnit', @level2type=N'COLUMN',@level2name=N'PropertyID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUnit',  N'COLUMN', N'UnitTypeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.', @level0type=N'SCHEMA'
,@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUnit', @level2type=N'COLUMN',@level2name=N'UnitTypeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUnit', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUnit', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUnit', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUnit', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUnit', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUnit', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
alter table dbo.tbUnit with check add constraint fktbUnitProperty 
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
alter table dbo.tbUnit with check add constraint fktbUnitUnitType 
foreign key(UnitTypeID) references dbo.tbAttributeValue (AttributeValueID)
go
alter table dbo.tbUnitAttributeNumeric with check add constraint fktbUnitAttributeNumericUnit
foreign key(UnitID) references dbo.tbUnit (UnitID)
go
alter table dbo.tbUnitAttributeText with check add constraint fktbUnitAttributeTextUnit
foreign key(UnitID) references dbo.tbUnit (UnitID)
go
alter table dbo.tbUnitAttribute with check add constraint fktbUnitAttributeUnit
foreign key(UnitID) references dbo.tbUnit (UnitID)
go