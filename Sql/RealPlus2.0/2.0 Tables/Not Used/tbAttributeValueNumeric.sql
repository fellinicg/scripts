use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValueNumeric') and type in (N'U'))
drop table dbo.tbAttributeValueNumeric
go
create table dbo.tbAttributeValueNumeric(
	AttributeValueID int not null,
	NumericValue bigint not null,
	IsDeleted bit not null constraint dftbAttributeValueNumericIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbAttributeValueNumericCreateDate  default (getdate()),
	CreateID int not null constraint dftbAttributeValueNumericCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbAttributeValueNumericUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbAttributeValueNumericUpdateID  default (0),
 constraint pktbAttributeValueNumeric primary key clustered 
(
	AttributeValueID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAttributeValueNumeric', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Table to support GUI where list needs a non-numeric label and numeric value.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAttributeValueNumeric'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAttributeValueNumeric', N'COLUMN', N'AttributeValueID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAttributeValueNumeric', @level2type=N'COLUMN',@level2name=N'AttributeValueID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAttributeValueNumeric', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAttributeValueNumeric', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAttributeValueNumeric', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAttributeValueNumeric', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAttributeValueNumeric', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAttributeValueNumeric', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
alter table dbo.tbAttributeValueNumeric with check add constraint fktbAttributeValueNumericAttributeValue
foreign key(AttributeValueID) references dbo.tbAttributeValue (AttributeValueID)
go