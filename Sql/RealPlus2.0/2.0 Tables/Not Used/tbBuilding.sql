use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbBuilding') and type in (N'U'))
drop table dbo.tbBuilding
go
create table dbo.tbBuilding(
	BuildingID int identity(1,1) not null,
	BuildingTypeID int not null,
	OwnershipTypeID int not null,
	PostalAddressID int not null,
	YearBuilt smallint null,
	Units smallint null,
	Floors tinyint null,
	FinancingAllowed bit null,
	NewDevelopment bit null,
	IsDeleted bit constraint dftbBuildingIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbBuildingCreateDate  default (getdate()),
 	UpdateDateTime datetime2 not null constraint dftbBuildingUpdateDateTime  default (getdate()),
constraint pktbBuilding primary key clustered 
(
	BuildingID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
alter table dbo.tbBuilding with check add constraint fktbBuildingType foreign key(BuildingTypeID)
references dbo.tbAttributeValue (AttributeValueID)
go
alter table dbo.tbBuilding with check add constraint fktbOwnershipType foreign key(OwnershipTypeID)
references dbo.tbAttributeValue (AttributeValueID)
go
alter table dbo.tbBuilding with check add constraint fktbBuildingPostalAddress foreign key(PostalAddressID)
references dbo.tbPostalAddress (PostalAddressID)