use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbUnitAddress') and type in (N'U'))
drop table dbo.tbUnitAddress
go
create table dbo.tbUnitAddress(
	UnitAddressID int identity(1,1) not null,
	PostalAddressID int not null,
	BuildingUnitID int not null,
	IsDeleted bit constraint dftbUnitAddressIsDeleted default 0,	
	CreateDateTime datetime not null constraint dftbUnitAddressCreateDate  default (getdate()),
 constraint pktbUnitAddress primary key clustered 
(
	UnitAddressID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
