use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
--if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAttributeType') and parent_object_id = object_id(N'dbo.tbUserAccount'))
--alter table dbo.tbUserAccount drop constraint fktbAttributeType
--go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeType') and type in (N'U'))
drop table dbo.tbAttributeType
go
create table dbo.tbAttributeType(
	AttributeTypeID int identity(1,1) not null,
	AttributeID int not null,
	TypeID int not null,
	IsDeleted bit constraint dftbAttributeTypeIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbAttributeTypeCreateDate  default (getdate()),
 constraint pktbAttributeType primary key clustered 
(
	AttributeTypeID asc
) on [PRIMARY],
 constraint akAttributeType unique nonclustered 
(
	AttributeID, TypeID
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]


go
set ansi_padding off
go
--alter table dbo.tbUserAccount  with check add  constraint fktbAttributeType foreign key(AccountStatus)
--references dbo.tbAttributeType (AccountStatus)