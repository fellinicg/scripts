use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCompanySearchProperyPreference') and type in (N'U'))
drop table dbo.tbCompanySearchProperyPreference
go
create table dbo.tbCompanySearchProperyPreference(
	CompanySearchProperyPreferenceID int identity(1,1) not null,
	CompanyID int not null,
	AttributeValueID int not null,
	CustomLabel varchar(100) null,
	SortOrder tinyint not null constraint dftbCompanySearchProperyPreferenceSortOrder default 0,
	Display bit not null constraint dftbCompanySearchProperyPreferenceDisplay default 1,	
	IsDeleted bit not null constraint dftbCompanySearchProperyPreferenceIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbCompanySearchProperyPreferenceCreateDate  default (getdate()),
	CreateID int not null constraint dftbCompanySearchProperyPreferenceCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbCompanySearchProperyPreferenceUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbCompanySearchProperyPreferenceUpdateID  default (0),
 constraint pkCompanySearchProperyPreferenceID primary key clustered 
(
	CompanySearchProperyPreferenceID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCompanySearchProperyPreference', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains label changes for the search properties page per company.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCompanySearchProperyPreference'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCompanySearchProperyPreference', N'COLUMN', N'CompanyID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbCompany table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCompanySearchProperyPreference', @level2type=N'COLUMN',@level2name=N'CompanyID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCompanySearchProperyPreference', N'COLUMN', N'AttributeValueID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCompanySearchProperyPreference', @level2type=N'COLUMN',@level2name=N'AttributeValueID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCompanySearchProperyPreference', N'COLUMN', N'CustomLabel'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Custom label to be displayed in the GUI.  Nullable to allow for custom sort without custom label'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCompanySearchProperyPreference', @level2type=N'COLUMN',@level2name=N'CustomLabel'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCompanySearchProperyPreference', N'COLUMN', N'SortOrder'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Custom sort order for the GUI at the company level.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCompanySearchProperyPreference', @level2type=N'COLUMN',@level2name=N'SortOrder'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCompanySearchProperyPreference', N'COLUMN', N'Display'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record should be displayed in the GUI at the company level.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCompanySearchProperyPreference', @level2type=N'COLUMN',@level2name=N'Display'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCompanySearchProperyPreference', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCompanySearchProperyPreference', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCompanySearchProperyPreference', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCompanySearchProperyPreference', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCompanySearchProperyPreference', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCompanySearchProperyPreference', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
alter table dbo.tbCompanySearchProperyPreference with check add constraint fktbCompanySearchProperyPreferenceCompanyID 
foreign key(CompanyID) references dbo.tbCompany (CompanyID)
go
alter table dbo.tbCompanySearchProperyPreference with check add constraint fktbCompanySearchProperyPreferenceAttributeValueID
foreign key(AttributeValueID) references dbo.tbAttributeValue (AttributeValueID)