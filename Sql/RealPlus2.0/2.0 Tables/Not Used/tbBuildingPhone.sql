use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbBuildingPhone') and type in (N'U'))
drop table dbo.tbBuildingPhone
go
create table dbo.tbBuildingPhone(
	BuildingPhoneID int identity(1,1) not null,
	BuildingID int not null,
	PhoneTypeID int not null,
	FullPhoneNumber varchar(30) not null,
	AreaCode tinyint null,
	PhoneNumber int null,
	Extension smallint null,
	TxtMsgCapable bit not null constraint dftbBuildingPhoneTextMsgCapable  default (0),
	IsPreferred bit not null,
	CreateDateTime datetime not null constraint dftbBuildingPhoneCreateDate  default (getdate()),
	UpdateDateTime datetime not null constraint dftbBuildingPhoneUpdateDateTime  default (getdate()),
 constraint pkBuildingPhoneID primary key clustered 
(
	BuildingPhoneID ASC
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off

