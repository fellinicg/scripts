use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbBuildingUnit') and type in (N'U'))
drop table dbo.tbBuildingUnit
go
create table dbo.tbBuildingUnit(
	BuildingUnitID int identity(1,1) not null,
	BuildingID int not null,
	PostalAddressID int not null,
	Unit varchar(40) not null,
	UnitNumber int null,
	UnitAlpha varchar(40) null,
	Bedrooms tinyint null,
	HalfBedrooms tinyint null,
	Bathrooms tinyint null,
	HalfBathrooms tinyint null,
	IsDeleted bit constraint dftbBuildingUnitIsDeleted default 0,	
	CreateDateTime datetime not null constraint dftbBuildingUnitCreateDate  default (getdate()),
 constraint pktbBuildingUnit primary key clustered 
(
	BuildingUnitID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
