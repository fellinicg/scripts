use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgentSavedCriteriaDetail') and type in (N'U'))
drop table dbo.tbAgentSavedCriteriaDetail
go
create table dbo.tbAgentSavedCriteriaDetail(
	AgentSavedCriteriaID int not null,
	CriteriaXml xml not null,
	--Section tinyint not null,
	--Field varchar(100) not null,
	--Value nvarchar(1000) not null,
	IsDeleted bit not null constraint dftbAgentSavedCriteriaDetailIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbAgentSavedCriteriaDetailCreateDate  default (getdate()),
	CreateID int not null constraint dftbAgentSavedCriteriaDetailCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbAgentSavedCriteriaDetailUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbAgentSavedCriteriaDetailUpdateID  default (0),
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentSavedCriteriaDetail', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the details for an agents saved criteria.  Design this table as best serves your needs or expand tbAgentSavedCriteria.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentSavedCriteriaDetail'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentSavedCriteriaDetail', N'COLUMN', N'AgentSavedCriteriaID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAgentSavedCriteria table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentSavedCriteriaDetail', @level2type=N'COLUMN',@level2name=N'AgentSavedCriteriaID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentSavedCriteriaDetail', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentSavedCriteriaDetail', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentSavedCriteriaDetail', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentSavedCriteriaDetail', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentSavedCriteriaDetail', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentSavedCriteriaDetail', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
create clustered index idxtbAgentSavedCriteriaDetailAgentSavedCriteriaID on dbo.tbAgentSavedCriteriaDetail (AgentSavedCriteriaID asc)
with (pad_index = off, statistics_norecompute = off, sort_in_tempdb = off, drop_existing = off, online = off, allow_row_locks = on, allow_page_locks = on)
go
alter table dbo.tbAgentSavedCriteriaDetail with check add constraint fktbAgentSavedCriteriaDetailAgentCriteria 
foreign key(AgentSavedCriteriaID) references dbo.tbAgentSavedCriteria (AgentSavedCriteriaID)
go