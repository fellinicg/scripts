use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCompanyAddress') and type in (N'U'))
drop table dbo.tbCompanyAddress
go
create table dbo.tbCompanyAddress(
	CompanyAddressID int identity(1,1) not null,
	CompanyID int not null,
	PostalAddressID int not null,
	AddressTypeID int not null,
	IsPreferred bit constraint dftbCompanyAddressIsPreferred default 0,
	IsDeleted bit constraint dftbCompanyAddressIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbCompanyAddressCreateDate  default (getdate()),
	UpdateDateTime datetime2 not null constraint dftbCompanyAddressUpdateDateTime  default (getdate()),
constraint pktbCompanyAddress primary key clustered 
(
	CompanyAddressID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
go
alter table dbo.tbCompanyAddress with check add constraint fktbCompanyAddressCompany foreign key(CompanyID)
references dbo.tbCompany (CompanyID)
go
alter table dbo.tbCompanyAddress with check add constraint fktbCompanyAddressPostalAddress foreign key(PostalAddressID)
references dbo.tbPostalAddress (PostalAddressID)
go
alter table dbo.tbCompanyAddress with check add constraint fktbCompanyAddressType foreign key(AddressTypeID)
references dbo.tbAttributeValue (AttributeValueID)