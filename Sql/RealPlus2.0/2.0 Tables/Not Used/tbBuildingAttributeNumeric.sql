use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbBuildingAttributeNumeric') and type in (N'U'))
drop table dbo.tbBuildingAttributeNumeric
go
create table dbo.tbBuildingAttributeNumeric(
	BuildingAttributeNumericID int identity(1,1) not null,
	BuildingID int not null,
	AttributeID int not null,
	AttributeValue int not null,
	IsDeleted bit constraint dftbBuildingAttributeNumericIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbBuildingAttributeNumericCreateDate  default (getdate()),
	UpdateDateTime datetime2 not null constraint dftbBuildingAttributeNumericUpdateDateTime  default (getdate()),
 constraint pktbBuildingAttributeNumeric primary key clustered 
(
	BuildingAttributeNumericID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
alter table dbo.tbBuildingAttributeNumeric with check add constraint fktbBuildingAttributeNumericBuilding foreign key(BuildingID)
references dbo.tbBuilding (BuildingID)
go
alter table dbo.tbBuildingAttributeNumeric with check add constraint fktbBuildingAttributeNumericAttribute foreign key(AttributeID)
references dbo.tbAttribute (AttributeID)