use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbBuildingAttributeText') and type in (N'U'))
drop table dbo.tbBuildingAttributeText
go
create table dbo.tbBuildingAttributeText(
	BuildingAttributeTextID int identity(1,1) not null,
	BuildingID int not null,
	AttributeID int not null,
	AttributeValue varchar(200) not null,
	IsDeleted bit constraint dftbBuildingAttributeTextIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbBuildingAttributeTextCreateDate  default (getdate()),
	UpdateDateTime datetime2 not null constraint dftbBuildingAttributeTextUpdateDateTime  default (getdate()),
 constraint pktbBuildingAttributeText primary key clustered 
(
	BuildingAttributeTextID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
alter table dbo.tbBuildingAttributeText with check add constraint fktbBuildingAttributeTextBuilding foreign key(BuildingID)
references dbo.tbBuilding (BuildingID)
go
alter table dbo.tbBuildingAttributeText with check add constraint fktbBuildingAttributeTextAttribute foreign key(AttributeID)
references dbo.tbAttribute (AttributeID)