use tmf_RealPlus2
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbUserAccountAddress') and type in (N'U'))
drop table dbo.tbUserAccountAddress
go
create table dbo.tbUserAccountAddress(
	UserAccountAddressID int identity(1,1) not null,
	MailAddressID int not null,
	UserAccountID int not null,
	IsPreferred bit constraint dftbUserAccountAddressIsPreferred default 0,
	IsDeleted bit constraint dftbUserAccountAddressIsDeleted default 0,	
	CreateDateTime datetime not null constraint dftbUserAccountAddressCreateDate  default (getdate()),
	UpdateDateTime datetime NOT NULL constraint dftbUserAccountAddressUpdateDateTime  default (getdate()),
 constraint pktbUserAccountAddress primary key clustered 
(
	UserAccountAddressID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
