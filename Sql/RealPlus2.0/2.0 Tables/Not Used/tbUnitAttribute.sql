use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbUnitAttribute') and type in (N'U'))
drop table dbo.tbUnitAttribute
go
create table dbo.tbUnitAttribute(
	UnitAttributeID int identity(1,1) not null,
	UnitID int not null,
	AttributeValueID int not null,
	IsDeleted bit not null constraint dftbUnitAttributeIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbUnitAttributeCreateDate  default (getdate()),
	CreateID int not null constraint dftbUnitAttributeCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbUnitAttributeUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbUnitAttributeUpdateID  default (0),
 constraint pktbUnitAttribute primary key clustered 
(
	UnitAttributeID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUnitAttribute', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains attribute values for a unit.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUnitAttribute'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUnitAttribute', N'COLUMN', N'UnitID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbUnit table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUnitAttribute', @level2type=N'COLUMN',@level2name=N'UnitID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUnitAttribute', N'COLUMN', N'AttributeValueID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUnitAttribute', @level2type=N'COLUMN',@level2name=N'AttributeValueID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUnitAttribute', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUnitAttribute', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUnitAttribute', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUnitAttribute', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUnitAttribute', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUnitAttribute', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if not exists (select * from sys.indexes where object_id = object_id(N'dbo.tbUnitAttribute') and name = N'aktbUnitAttribute')
alter table dbo.tbUnitAttribute add constraint aktbUnitAttribute unique nonclustered (UnitID, AttributeValueID asc) with (pad_index = off, statistics_norecompute = off, sort_in_tempdb = off, ignore_dup_key = off, online = off, allow_row_locks = on, allow_page_locks = on)
go
alter table dbo.tbUnitAttribute with check add constraint fktbUnitAttributeUnit foreign key(UnitID)
references dbo.tbUnit (UnitID)
go
alter table dbo.tbUnitAttribute with check add constraint fktbUnitAttributeValue foreign key(AttributeValueID)
references dbo.tbAttributeValue (AttributeValueID)