use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgentWorkInProgressList') and type in (N'U'))
drop table dbo.tbAgentWorkInProgressList
go
create table dbo.tbAgentWorkInProgressList(
	AgentWorkInProgressID int not null,
	ListingID int not null,
	IsChecked bit not null constraint dftbAgentWorkInProgressIsChecked default 0,
	IsRemoved bit not null constraint dftbAgentWorkInProgressListIsRemoved default 0,	
	--IsDeleted bit not null constraint dftbAgentWorkInProgressListIsDeleted default 0,	
	--CreateDateTime datetime2 not null constraint dftbAgentWorkInProgressListCreateDate  default (getdate()),
	--CreateID int not null constraint dftbAgentWorkInProgressListCreateID  default (0),
	--UpdateDateTime datetime2 not null constraint dftbAgentWorkInProgressListUpdateDateTime  default (getdate()),
	--UpdateID int not null constraint dftbAgentWorkInProgressListUpdateID  default (0),
 constraint pktbAgentWorkInProgressList primary key clustered 
(
	AgentWorkInProgressID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentWorkInProgressList', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the details for an agents work in progress.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentWorkInProgressList'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentWorkInProgressList', N'COLUMN', N'AgentWorkInProgressID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAgentWorkInProgress table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentWorkInProgressList', @level2type=N'COLUMN',@level2name=N'AgentWorkInProgressID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentWorkInProgressList', N'COLUMN', N'ListingID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbListing table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentWorkInProgressList', @level2type=N'COLUMN',@level2name=N'ListingID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentWorkInProgressList', N'COLUMN', N'IsChecked'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been marked with a check in the GUI.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentWorkInProgressList', @level2type=N'COLUMN',@level2name=N'IsChecked'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentWorkInProgressList', N'COLUMN', N'IsRemoved'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been explicitly removed from the list via the GUI.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentWorkInProgressList', @level2type=N'COLUMN',@level2name=N'IsRemoved'
go
--if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentWorkInProgressList', N'COLUMN', N'IsDeleted'))
--exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
--, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentWorkInProgressList', @level2type=N'COLUMN',@level2name=N'IsDeleted'
--go
--if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentWorkInProgressList', N'COLUMN', N'CreateID'))
--exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
--, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentWorkInProgressList', @level2type=N'COLUMN',@level2name=N'CreateID'
--go
--if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentWorkInProgressList', N'COLUMN', N'UpdateID'))
--exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
--, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentWorkInProgressList', @level2type=N'COLUMN',@level2name=N'UpdateID'
--go
alter table dbo.tbAgentWorkInProgressList with check add constraint fktbAgentWorkInProgressListAgentWiP 
foreign key(AgentWorkInProgressID) references dbo.tbAgentWorkInProgress (AgentWorkInProgressID)
go
alter table dbo.tbAgentWorkInProgressList with check add constraint fktbAgentWorkInProgressListListingID 
foreign key(ListingID) references dbo.tbListing (ListingID)
go