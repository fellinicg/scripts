use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbBuildingAddress') and type in (N'U'))
drop table dbo.tbBuildingAddress
go
create table dbo.tbBuildingAddress(
	BuildingAddressID int identity(1,1) not null,
	PostalAddressID int not null,
	BuildingID int not null,
	IsDeleted bit constraint dftbBuildingAddressIsDeleted default 0,	
	CreateDateTime datetime not null constraint dftbBuildingAddressCreateDate  default (getdate()),
 constraint pktbBuildingAddress primary key clustered 
(
	BuildingAddressID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
