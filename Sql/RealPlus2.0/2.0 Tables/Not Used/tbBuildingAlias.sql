use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbBuildingAlias') and type in (N'U'))
drop table dbo.tbBuildingAlias
go
create table dbo.tbBuildingAlias(
	BuildingAliasID int identity(1,1) not null,
	BuildingID int not null,
	BuildingAliasName varchar(100) not null,
	IsDeleted bit constraint dftbBuildingAliasIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbBuildingAliasCreateDate  default (getdate()),
 constraint pktbBuildingAlias primary key clustered 
(
	BuildingAliasID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
