use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAccountStatus') and parent_object_id = object_id(N'dbo.tbUserAccount'))
alter table dbo.tbUserAccount drop constraint fktbAccountStatus
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAccountStatus') and type in (N'U'))
drop table dbo.tbAccountStatus
go
create table dbo.tbAccountStatus(
	AccountStatus char(1) not null,
	Detail varchar(40) not null,
 constraint PK_tbAccountStatus primary key clustered 
(
	AccountStatus asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
go
alter table dbo.tbUserAccount  with check add  constraint fktbAccountStatus foreign key(AccountStatus)
references dbo.tbAccountStatus (AccountStatus)