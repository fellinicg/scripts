use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPostalCode') and type in (N'U'))
drop table dbo.tbPostalCode
go
create table dbo.tbPostalCode(
	PostalCodeID int identity(1,1) not null,
	PostalCode char(5) not null,
	CityTown varchar(40) not null,
	StateProvince char(2) not null,
	Country char(2) null,
	--Latitude float null,
	--Longitude float null,
	IsDeleted bit not null constraint dftbPostalCodeIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbPostalCodeCreateDate  default (getdate()),
	UpdateDateTime datetime2 not null constraint dftbPostalCodeUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbPostalCodeUpdateID  default (0),
 constraint pktbPostalCode primary key clustered 
(
	PostalCodeID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPostalCode', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the base information about a PostalCode.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPostalCode'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPostalCode', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPostalCode', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPostalCode', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPostalCode', @level2type=N'COLUMN',@level2name=N'UpdateID'
go