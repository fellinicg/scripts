use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPropertyAttribute') and type in (N'U'))
drop table dbo.tbPropertyAttribute
go
create table dbo.tbPropertyAttribute(
	PropertyAttributeID int identity(1,1) not null,
	PropertyID int not null,
	AttributeValueID int not null,
	IsDeleted bit not null constraint dftbPropertyAttributeIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbPropertyAttributeCreateDate  default (getdate()),
	CreateID int not null constraint dftbPropertyAttributeCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbPropertyAttributeUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbPropertyAttributeUpdateID  default (0),
 constraint pktbPropertyAttribute primary key clustered 
(
	PropertyAttributeID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbPropertyAttribute') and name = N'ncixtbPropertyAttributeProperty')
drop index ncixtbPropertyAttributeProperty on dbo.tbPropertyAttribute
create nonclustered index ncixtbPropertyAttributeProperty on dbo.tbPropertyAttribute(PropertyID asc)
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbPropertyAttribute') and name = N'ncixtbPropertyAttributeAttr')
drop index ncixtbPropertyAttributeAttr on dbo.tbPropertyAttribute
create nonclustered index ncixtbPropertyAttributeAttr on dbo.tbPropertyAttribute(AttributeValueID asc)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAttribute', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains attribute values for a property.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAttribute'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAttribute', N'COLUMN', N'PropertyID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbProperty table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAttribute', @level2type=N'COLUMN',@level2name=N'PropertyID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAttribute', N'COLUMN', N'AttributeValueID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAttribute', @level2type=N'COLUMN',@level2name=N'AttributeValueID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAttribute', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAttribute', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAttribute', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAttribute', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAttribute', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAttribute', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if not exists (select * from sys.indexes where object_id = object_id(N'dbo.tbPropertyAttribute') and name = N'aktbPropertyAttribute')
alter table dbo.tbPropertyAttribute add constraint aktbPropertyAttribute unique nonclustered (PropertyID, AttributeValueID asc) with (pad_index = off, statistics_norecompute = off, sort_in_tempdb = off, ignore_dup_key = off, online = off, allow_row_locks = on, allow_page_locks = on)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbProperty') and type in (N'U'))
alter table dbo.tbPropertyAttribute with check add constraint fktbPropertyAttributeProperty 
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbPropertyAttribute with check add constraint fktbPropertyAttributeValue 
foreign key(AttributeValueID) references dbo.tbAttributeValue (AttributeValueID)