use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPropertyAddress') and type in (N'U'))
drop table dbo.tbPropertyAddress
go
create table dbo.tbPropertyAddress(
	PropertyAddressID int identity(1,1) not null,
	PropertyID int not null,
	PostalAddressID int not null,
	IsPrimary bit not null constraint dftbPropertyAddressIsPrimary default 0,
	IsDeleted bit not null constraint dftbPropertyAddressIsDeleted default 0,	
	CreateDateTime datetime not null constraint dftbPropertyAddressCreateDate  default (getdate()),
	CreateID int not null constraint dftbPropertyAddressCreateID  default (0),
	UpdateDateTime datetime NOT NULL constraint dftbPropertyAddressUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbPropertyAddressUpdateID  default (0),
constraint PK_tbPropertyAddress primary key clustered 
(
	PropertyAddressID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbPropertyAddress') and name = N'ncixtbPropertyAddressProperty')
drop index ncixtbPropertyAddressProperty on dbo.tbPropertyAddress
create nonclustered index ncixtbPropertyAddressProperty on dbo.tbPropertyAddress(PropertyID asc)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAddress', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the property to address relationship.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAddress'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAddress', N'COLUMN', N'PropertyID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbProperty table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAddress', @level2type=N'COLUMN',@level2name=N'PropertyID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAddress', N'COLUMN', N'PostalAddressID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbPostalAddress table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAddress', @level2type=N'COLUMN',@level2name=N'PostalAddressID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAddress', N'COLUMN', N'IsPrimary'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record is the primary address.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAddress', @level2type=N'COLUMN',@level2name=N'IsPrimary'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAddress', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAddress', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAddress', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAddress', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAddress', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAddress', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbProperty') and type in (N'U'))
alter table dbo.tbPropertyAddress with check add constraint fktbPropertyAddressProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPostalAddress') and type in (N'U'))
alter table dbo.tbPropertyAddress with check add constraint fktbPropertyAddressPostalAddress
foreign key(PostalAddressID) references dbo.tbPostalAddress (PostalAddressID)
go