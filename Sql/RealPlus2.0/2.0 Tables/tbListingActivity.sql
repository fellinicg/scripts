--use Jaguar_v2_Prototype2
--use Jaguar_v2_Prototype2_Dev
--use Jaguar_v2_Prototype2_RealPlus
use Jaguar_v2_Prototype2_BHS_2
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingActivity') and type in (N'U'))
drop table dbo.tbListingActivity
go
create table dbo.tbListingActivity(
	ListingActivityID int identity(1,1) not null,
	ListingID int not null,
	ActivityID int not null,
	IsDeleted bit not null constraint dftbListingActivityIsDeleted default 0,	
	ActivityDateTime datetime2 not null,
	CreateDateTime datetime2 not null constraint dftbListingActivityCreateDate  default (getdate()),
 	CreateID int not null constraint dftbListingActivityCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbListingActivityUpdateDateTime  default (getdate()),
 	UpdateID int not null constraint dftbListingActivityUpdateID  default (0), 
	EtlID int null,
constraint pktbListingActivity primary key clustered 
(
	ListingActivityID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingActivity') and name = N'ncixtbListingActivityListing')
drop index ncixtbListingActivityListing on dbo.tbListingActivity
create nonclustered index ncixtbListingActivityListing on dbo.tbListingActivity(ListingID asc)
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingAttribute') and name = N'ncixtbListingActivity')
drop index ncixtbListingActivity on dbo.tbListingActivity
create nonclustered index ncixtbListingActivity on dbo.tbListingActivity(ActivityID asc)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingActivity', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the listing activity.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingActivity'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingActivity', N'COLUMN', N'ListingID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbListing table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingActivity', @level2type=N'COLUMN',@level2name=N'ListingID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingActivity', N'COLUMN', N'ActivityID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingActivity', @level2type=N'COLUMN',@level2name=N'ActivityID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingActivity', N'COLUMN', N'ActivityDateTime'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Actual activity datetime.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingActivity', @level2type=N'COLUMN',@level2name=N'ActivityDateTime'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingActivity', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingActivity', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingActivity', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingActivity', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingActivity', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingActivity', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListing') and type in (N'U'))
alter table dbo.tbListingActivity with check add constraint fktbListingActivityListing foreign key(ListingID)
references dbo.tbListing (ListingID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbListingActivity with check add constraint fktbListingActivityStatus foreign key(ActivityID)
references dbo.tbAttributeValue (AttributeValueID)