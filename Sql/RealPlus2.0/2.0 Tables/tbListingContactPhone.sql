use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingContactPhone') and type in (N'U'))
drop table dbo.tbListingContactPhone
go
create table dbo.tbListingContactPhone(
	ListingContactPhoneID int identity(1,1) not null,
	ListingID int not null,
	ListingContactTypeID int not null,
	PersonPhoneID int not null,
	IsDeleted bit constraint dftbListingContactPhoneIsDeleted default 0,	
 constraint pktbListingContactPhone primary key clustered 
(
	ListingContactPhoneID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingContactPhone') and name = N'ncixtbListingContactPhoneListing')
drop index ncixtbListingContactPhoneListing on dbo.tbListingContactPhone
create nonclustered index ncixtbListingContactPhoneListing on dbo.tbListingContactPhone(ListingID asc)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingContactPhone', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains listing phone contact information.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingContactPhone'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingContactPhone', N'COLUMN', N'ListingID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbListing table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingContactPhone', @level2type=N'COLUMN',@level2name=N'ListingID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingContactPhone', N'COLUMN', N'ListingContactTypeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingContactPhone', @level2type=N'COLUMN',@level2name=N'ListingContactTypeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingContactPhone', N'COLUMN', N'PersonPhoneID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbPersonPhone table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingContactPhone', @level2type=N'COLUMN',@level2name=N'PersonPhoneID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingContactPhone', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingContactPhone', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListing') and type in (N'U'))
alter table dbo.tbListingContactPhone with check add constraint fktbListingContactPhoneListing foreign key(ListingID)
references dbo.tbListing (ListingID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbListingContactPhone with check add constraint fktbListingContactPhoneContactType foreign key(ListingContactTypeID)
references dbo.tbAttributeValue (AttributeValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPersonPhone') and type in (N'U'))
alter table dbo.tbListingContactPhone with check add constraint fktbListingContactPhonePerson foreign key(PersonPhoneID)
references dbo.tbPersonPhone (PersonPhoneID)
go