use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPropertyComment') and type in (N'U'))
drop table dbo.tbPropertyComment
go
create table dbo.tbPropertyComment(
	PropertyCommentID int identity(1,1) not null,
	PropertyID int not null,
	DetailID int not null,
	CompanyID int null,
	PersonID int null,
	IsDeleted bit constraint dftbPropertyCommentIsDeleted default 0,	
 constraint pktbPropertyComment primary key clustered 
(
	PropertyCommentID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbPropertyComment') and name = N'ncixtbPropertyCommentProperty')
drop index ncixtbPropertyCommentProperty on dbo.tbPropertyComment
create nonclustered index ncixtbPropertyCommentProperty on dbo.tbPropertyComment(PropertyID asc)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyComment', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the relationship betwee tbDetail and tbProperty to store comments about a property.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyComment'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyComment', N'COLUMN', N'PropertyID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbProperty table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyComment', @level2type=N'COLUMN',@level2name=N'PropertyID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyComment', N'COLUMN', N'DetailID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbDetail table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyComment', @level2type=N'COLUMN',@level2name=N'DetailID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyComment', N'COLUMN', N'CompanyID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbCompany table.  Provides optional company level comments'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyComment', @level2type=N'COLUMN',@level2name=N'CompanyID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyComment', N'COLUMN', N'PersonID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbPerson table.  Provides optional person level comments'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyComment', @level2type=N'COLUMN',@level2name=N'PersonID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyComment', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyComment', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbProperty') and type in (N'U'))
alter table dbo.tbPropertyComment with check add constraint fktbPropertyCommentProperty foreign key(PropertyID)
references dbo.tbProperty (PropertyID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbDetail') and type in (N'U'))
alter table dbo.tbPropertyComment with check add constraint fktbPropertyCommentDetail foreign key(DetailID)
references dbo.tbDetail (DetailID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCompany') and type in (N'U'))
alter table dbo.tbPropertyComment with check add constraint fktbPropertyCommentCompany foreign key(CompanyID)
references dbo.tbCompany (CompanyID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPropertyComment') and type in (N'U'))
alter table dbo.tbPropertyComment with check add constraint fktbPropertyCommentPerson foreign key(PersonID)
references dbo.tbPerson (PersonID)
go