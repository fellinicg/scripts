use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentListingListing') and parent_object_id = object_id(N'dbo.tbAgentListing'))
alter table dbo.tbAgentListing drop constraint fktbAgentListingListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentListingPinListing') and parent_object_id = object_id(N'dbo.tbAgentListingPin'))
alter table dbo.tbAgentListingPin drop constraint fktbAgentListingPinListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingAttributeListing') and parent_object_id = object_id(N'dbo.tbListingAttribute'))
alter table dbo.tbListingAttribute drop constraint fktbListingAttributeListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingAttributeNumericListing') and parent_object_id = object_id(N'dbo.tbListingAttributeNumeric'))
alter table dbo.tbListingAttributeNumeric drop constraint fktbListingAttributeNumericListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingAttributeTextListing') and parent_object_id = object_id(N'dbo.tbListingAttributeText'))
alter table dbo.tbListingAttributeText drop constraint fktbListingAttributeTextListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingMediaListing') and parent_object_id = object_id(N'dbo.tbListingMedia'))
alter table dbo.tbListingMedia drop constraint fktbListingMediaListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingOfficeListing') and parent_object_id = object_id(N'dbo.tbListingOffice'))
alter table dbo.tbListingOffice drop constraint fktbListingOfficeListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingPriceListing') and parent_object_id = object_id(N'dbo.tbListingPrice'))
alter table dbo.tbListingPrice drop constraint fktbListingPriceListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingStatusListing') and parent_object_id = object_id(N'dbo.tbListingStatus'))
alter table dbo.tbListingStatus drop constraint fktbListingStatusListing
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListing') and type in (N'U'))
drop table dbo.tbListing
go
create table dbo.tbListing(
	ListingID int not null,
	ListingCategoryID int not null,
	PropertyID int not null,
	ListingTypeID int not null,
	ListingProviderID int not null,
	InitialPrice money null,
	CurrentPriceID int null,
	CurrentPrice money null,
	CurrentStatusID int null,
	CurrentStatus varchar(100) null,
	IsDeleted bit not null constraint dftbListingIsDeleted default 0,
	IsActive bit not null constraint dftbListingIsActive default 1,
	ListDate date not null,
	CreateDateTime datetime2 not null constraint dftbListingCreateDate  default (getdate()),
	CreateID int not null constraint dftbListingCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbListingUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbListingUpdateID  default (0),
	--EtlID int not null,
 constraint pktbListing primary key clustered 
(
	ListingID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from sys.indexes WHERE object_id = object_id(N'dbo.tbListing') and name = N'ncixtbListingPropertyID')
create nonclustered index ncixtbListingPropertyID on dbo.tbListing(PropertyID asc)
with (pad_index = off, statistics_norecompute = off, sort_in_tempdb = off, drop_existing = off, online = off, allow_row_locks = on, allow_page_locks = on)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListing', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the base information for all listings.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListing'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListing', N'COLUMN', N'ListingCategoryID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListing', @level2type=N'COLUMN',@level2name=N'ListingCategoryID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListing', N'COLUMN', N'PropertyID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbPropertyID table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListing', @level2type=N'COLUMN',@level2name=N'PropertyID'
go
--if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListing', N'COLUMN', N'OfficeID'))
--exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbOffice table.'
--, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListing', @level2type=N'COLUMN',@level2name=N'OfficeID'
--go
--if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListing', N'COLUMN', N'ListingStatusID'))
--exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbListingStatus table.'
--, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListing', @level2type=N'COLUMN',@level2name=N'ListingStatusID'
--go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListing', N'COLUMN', N'ListingTypeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListing', @level2type=N'COLUMN',@level2name=N'ListingTypeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListing', N'COLUMN', N'ListingProviderID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListing', @level2type=N'COLUMN',@level2name=N'ListingProviderID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListing', N'COLUMN', N'InitialPrice'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Field to store the initial price.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListing', @level2type=N'COLUMN',@level2name=N'InitialPrice'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListing', N'COLUMN', N'ListDate'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'List date.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListing', @level2type=N'COLUMN',@level2name=N'ListDate'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListing', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListing', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListing', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListing', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListing', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListing', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbListing with check add constraint fktbListingListingCategory
foreign key(ListingCategoryID) references dbo.tbAttributeValue (AttributeValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbProperty') and type in (N'U'))
alter table dbo.tbListing with check add constraint fktbListingPropertyID
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbListing with check add constraint fktbListingListingType
foreign key(ListingTypeID) references dbo.tbAttributeValue (AttributeValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbListing with check add constraint fktbListingListingProvider
foreign key(ListingProviderID) references dbo.tbAttributeValue (AttributeValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgentListing') and type in (N'U'))
alter table dbo.tbAgentListing with check add constraint fktbAgentListingListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingAttribute') and type in (N'U'))
alter table dbo.tbListingAttribute with check add constraint fktbListingAttributeListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingAttributeNumeric') and type in (N'U'))
alter table dbo.tbListingAttributeNumeric with check add constraint fktbListingAttributeNumericListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingAttributeText') and type in (N'U'))
alter table dbo.tbListingAttributeText with check add constraint fktbListingAttributeTextListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingMedia') and type in (N'U'))
alter table dbo.tbListingMedia with check add constraint fktbListingMediaListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingOffice') and type in (N'U'))
alter table dbo.tbListingOffice with check add constraint fktbListingOfficeListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingPrice') and type in (N'U'))
alter table dbo.tbListingPrice with check add constraint fktbListingPriceListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingStatus') and type in (N'U'))
alter table dbo.tbListingStatus with check add constraint fktbListingStatusListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go