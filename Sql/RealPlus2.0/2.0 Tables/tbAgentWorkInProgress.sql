use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentWorkInProgressListAgentWiP') and parent_object_id = object_id(N'dbo.tbAgentWorkInProgressList'))
alter table dbo.tbAgentWorkInProgressList drop constraint fktbAgentWorkInProgressListAgentWiP
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgentWorkInProgress') and type in (N'U'))
drop table dbo.tbAgentWorkInProgress
go
create table dbo.tbAgentWorkInProgress(
	AgentWorkInProgressID int identity(1,1) not null,
	AgentID int not null,
	CustomerID int null,
	Detail varchar(200) not null,
	WorkInProgress xml not null,
	AgentSavedCriteriaID int null,
	SortBy tinyint not null constraint dftbAgentWorkInProgressSortBy default 0,
	Bookmark int null,
	IsDeleted bit not null constraint dftbAgentWorkInProgressIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbAgentWorkInProgressCreateDate  default (getdate()),
	CreateID int not null constraint dftbAgentWorkInProgressCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbAgentWorkInProgressUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbAgentWorkInProgressUpdateID  default (0),
 constraint pktbAgentWorkInProgress primary key clustered 
(
	AgentWorkInProgressID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentWorkInProgress', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains an agents work in progress.  An agent can save their own or associate it to a customer and can also link it to a save criteria.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentWorkInProgress'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentWorkInProgress', N'COLUMN', N'AgentID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAgent table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentWorkInProgress', @level2type=N'COLUMN',@level2name=N'AgentID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentWorkInProgress', N'COLUMN', N'Detail'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Detail/description is required.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentWorkInProgress', @level2type=N'COLUMN',@level2name=N'Detail'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentWorkInProgress', N'COLUMN', N'CustomerID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbCustomer table, but not required.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentWorkInProgress', @level2type=N'COLUMN',@level2name=N'CustomerID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentWorkInProgress', N'COLUMN', N'AgentSavedCriteriaID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAgentSavedCriteria table, but not required.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentWorkInProgress', @level2type=N'COLUMN',@level2name=N'AgentSavedCriteriaID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentWorkInProgress', N'COLUMN', N'SortBy'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'0 would be no sort selected, the rest would correspond to drop down list.  Retype field as you feel necessary'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentWorkInProgress', @level2type=N'COLUMN',@level2name=N'SortBy'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentWorkInProgress', N'COLUMN', N'Bookmark'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Thought maybe the listingid, but retype/define field as necessary.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentWorkInProgress', @level2type=N'COLUMN',@level2name=N'Bookmark'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentWorkInProgress', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentWorkInProgress', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentWorkInProgress', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentWorkInProgress', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentWorkInProgress', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentWorkInProgress', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgent') and type in (N'U'))
alter table dbo.tbAgentWorkInProgress with check add constraint fktbAgentWorkInProgressAgent 
foreign key(AgentID) references dbo.tbAgent (AgentID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCustomer') and type in (N'U'))
alter table dbo.tbAgentWorkInProgress with check add constraint fktbAgentWorkInProgressCustomer
foreign key(CustomerID) references dbo.tbCustomer (CustomerID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgentSavedCriteria') and type in (N'U'))
alter table dbo.tbAgentWorkInProgress with check add constraint fktbAgentWorkInProgressCriteria
foreign key(AgentSavedCriteriaID) references dbo.tbAgentSavedCriteria (AgentSavedCriteriaID)
go