use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentAgentStatusAgent') and parent_object_id = object_id(N'dbo.tbAgent'))
alter table dbo.tbAgent drop constraint fktbAgentAgentStatusAgent
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentListingAgent') and parent_object_id = object_id(N'dbo.tbAgentListing'))
alter table dbo.tbAgentListing drop constraint fktbAgentListingAgent
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentListingPinAgent') and parent_object_id = object_id(N'dbo.tbAgentListingPin'))
alter table dbo.tbAgentListingPin drop constraint fktbAgentListingPinAgent
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentOfficeAgent') and parent_object_id = object_id(N'dbo.tbAgentOffice'))
alter table dbo.tbAgentOffice drop constraint fktbAgentOfficeAgent
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentSavedCriteriaAgent') and parent_object_id = object_id(N'dbo.tbAgentSavedCriteria'))
alter table dbo.tbAgentSavedCriteria drop constraint fktbAgentSavedCriteriaAgent
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentStatusAgent') and parent_object_id = object_id(N'dbo.tbAgentStatus'))
alter table dbo.tbAgentStatus drop constraint fktbAgentStatusAgent
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentWorkInProgressAgent') and parent_object_id = object_id(N'dbo.tbAgentWorkInProgress'))
alter table dbo.tbAgentWorkInProgress drop constraint fktbAgentWorkInProgressAgent
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbCustomerAgent') and parent_object_id = object_id(N'dbo.tbCustomer'))
alter table dbo.tbCustomer drop constraint fktbCustomerAgent
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbSharedCustomerAgent') and parent_object_id = object_id(N'dbo.tbSharedCustomer'))
alter table dbo.tbSharedCustomer drop constraint fktbSharedCustomerAgent
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgent') and type in (N'U'))
drop table dbo.tbAgent
go
create table dbo.tbAgent(
	AgentID int not null,
	AgentNumber varchar(20) not null,
	--AgentStatusID int null,
	IsDeleted bit not null constraint dftbAgentIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbAgentCreateDate  default (getdate()),
	CreateID int not null constraint dftbAgentCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbAgentUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbAgentUpdateID  default (0),
	EtlID int null,
 constraint pktbAgent primary key clustered 
(
	AgentID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgent', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the base information for all agents that are also valid users.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgent'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgent', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgent', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
--if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgent',  N'COLUMN', N'AgentStatusID'))
--exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAgentStatus table.', @level0type=N'SCHEMA'
--,@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgent', @level2type=N'COLUMN',@level2name=N'AgentStatusID'
--go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgent', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgent', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgent', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgent', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPerson') and type in (N'U'))
alter table dbo.tbAgent with check add constraint fktbAgentPerson
foreign key(AgentID) references dbo.tbPerson (PersonID)
go
--if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgentStatus') and type in (N'U'))
--alter table dbo.tbAgent with check add constraint fktbAgentAgentStatus
--foreign key(AgentStatusID) references dbo.tbAgentStatus (AgentStatusID)
--go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgentStatus') and type in (N'U'))
alter table dbo.tbAgentStatus with check add constraint fktbAgentAgentStatusAgent
foreign key(AgentID) references dbo.tbAgentStatus (AgentID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgentListing') and type in (N'U'))
alter table dbo.tbAgentListing with check add constraint fktbAgentListingAgent
foreign key(AgentID) references dbo.tbAgent (AgentID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgentListingPin') and type in (N'U'))
alter table dbo.tbAgentListingPin with check add constraint fktbAgentListingPinAgent
foreign key(AgentID) references dbo.tbAgent (AgentID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgentOffice') and type in (N'U'))
alter table dbo.tbAgentOffice with check add constraint fktbAgentOfficeAgent
foreign key(AgentID) references dbo.tbAgent (AgentID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgentSavedCriteria') and type in (N'U'))
alter table dbo.tbAgentSavedCriteria with check add constraint fktbAgentSavedCriteriaAgent
foreign key(AgentID) references dbo.tbAgent (AgentID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgentWorkInProgress') and type in (N'U'))
alter table dbo.tbAgentWorkInProgress with check add constraint fktbAgentWorkInProgressAgent
foreign key(AgentID) references dbo.tbAgent (AgentID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCustomer') and type in (N'U'))
alter table dbo.tbCustomer with check add constraint fktbCustomerAgent
foreign key(AgentID) references dbo.tbAgent (AgentID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbSharedCustomer') and type in (N'U'))
alter table dbo.tbSharedCustomer with check add constraint fktbSharedCustomerAgent
foreign key(AgentID) references dbo.tbAgent (AgentID)
go