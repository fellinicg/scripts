use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPropertyContactEmail') and type in (N'U'))
drop table dbo.tbPropertyContactEmail
go
create table dbo.tbPropertyContactEmail(
	PropertyContactEmailID int identity(1,1) not null,
	PropertyID int not null,
	PropertyContactTypeID int not null,
	PersonEmailID int not null,
	IsDeleted bit constraint dftbPropertyContactEmailIsDeleted default 0,	
 constraint pktbPropertyContactEmail primary key clustered 
(
	PropertyContactEmailID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbPropertyContactEmail') and name = N'ncixtbPropertyContactEmailProperty')
drop index ncixtbPropertyContactEmailProperty on dbo.tbPropertyContactEmail
create nonclustered index ncixtbPropertyContactEmailProperty on dbo.tbPropertyContactEmail(PropertyID asc)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyContactEmail', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains property email contact information.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyContactEmail'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyContactEmail', N'COLUMN', N'PropertyID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbProperty table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyContactEmail', @level2type=N'COLUMN',@level2name=N'PropertyID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyContactEmail', N'COLUMN', N'PropertyContactTypeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyContactEmail', @level2type=N'COLUMN',@level2name=N'PropertyContactTypeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyContactEmail', N'COLUMN', N'PersonEmailID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbPersonEmail table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyContactEmail', @level2type=N'COLUMN',@level2name=N'PersonEmailID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyContactEmail', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyContactEmail', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbProperty') and type in (N'U'))
alter table dbo.tbPropertyContactEmail with check add constraint fktbPropertyContactEmailProperty foreign key(PropertyID)
references dbo.tbProperty (PropertyID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbPropertyContactEmail with check add constraint fktbPropertyContactEmailContactType foreign key(PropertyContactTypeID)
references dbo.tbAttributeValue (AttributeValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPersonEmail') and type in (N'U'))
alter table dbo.tbPropertyContactEmail with check add constraint fktbPropertyContactEmailPerson foreign key(PersonEmailID)
references dbo.tbPersonEmail (PersonEmailID)
go