use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbDetail') and type in (N'U'))
drop table dbo.tbDetail
go
create table dbo.tbDetail(
	DetailID int identity(1,1) not null,
	DetailTypeID int not null,
	DetailText nvarchar(max) not null,
	IsDeleted bit constraint dftbDetailIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbDetailCreateDate  default (getdate()),
	CreateID int not null constraint dftbDetailCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbDetailUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbDetailUpdateID  default (0),
	EtlID int null,
 constraint pktbDetail primary key clustered 
(
	DetailID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbDetail') and name = N'ncixtbDetailType')
drop index ncixtbDetailType on dbo.tbDetail
create nonclustered index ncixtbDetailType on dbo.tbDetail(DetailTypeID asc)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbDetail', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains large text entries of different types.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbDetail'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbDetail', N'COLUMN', N'DetailTypeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAgent table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbDetail', @level2type=N'COLUMN',@level2name=N'DetailTypeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbDetail', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbDetail', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbDetail', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbDetail', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbDetail', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbDetail', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbDetail  with check add constraint fktbDetailType 
foreign key(DetailTypeID) references dbo.tbAttributeValue (AttributeValueID)
