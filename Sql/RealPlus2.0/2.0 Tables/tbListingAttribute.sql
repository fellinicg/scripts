use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingAttribute') and type in (N'U'))
drop table dbo.tbListingAttribute
go
create table dbo.tbListingAttribute(
	ListingAttributeID int identity(1,1) not null,
	ListingID int not null,
	AttributeValueID int not null,
	IsDeleted bit not null constraint dftbListingAttributeIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbListingAttributeCreateDate  default (getdate()),
 	CreateID int not null constraint dftbListingAttributeCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbListingAttributeUpdateDateTime  default (getdate()),
 	UpdateID int not null constraint dftbListingAttributeUpdateID  default (0),
constraint pktbListingAttribute primary key clustered 
(
	ListingAttributeID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingAttribute') and name = N'ncixtbListingAttributeListing')
drop index ncixtbListingAttributeListing on dbo.tbListingAttribute
create nonclustered index ncixtbListingAttributeListing on dbo.tbListingAttribute(ListingID asc)
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingAttribute') and name = N'ncixtbListingAttributeAttr')
drop index ncixtbListingAttributeAttr on dbo.tbListingAttribute
create nonclustered index ncixtbListingAttributeAttr on dbo.tbListingAttribute(AttributeValueID asc)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingAttribute', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains attribute values for a Listing.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingAttribute'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingAttribute', N'COLUMN', N'ListingID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbListing table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingAttribute', @level2type=N'COLUMN',@level2name=N'ListingID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingAttribute', N'COLUMN', N'AttributeValueID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingAttribute', @level2type=N'COLUMN',@level2name=N'AttributeValueID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingAttribute', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingAttribute', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingAttribute', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingAttribute', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingAttribute', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingAttribute', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListing') and type in (N'U'))
alter table dbo.tbListingAttribute with check add constraint fktbListingAttributeListing 
foreign key(ListingID) references dbo.tbListing (ListingID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbListingAttribute with check add constraint fktbListingAttributeValue 
foreign key(AttributeValueID) references dbo.tbAttributeValue (AttributeValueID)