use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingPrice') and type in (N'U'))
drop table dbo.tbListingPrice
go
create table dbo.tbListingPrice(
	ListingPriceID int identity(1,1) not null,
	ListingID int not null,
	ListingPrice money not null,
	--IsCurrent bit constraint dftbListingPriceIsCurrent default 1,	
	IsDeleted bit not null constraint dftbListingPriceIsDeleted default 0,	
	PriceDateTime datetime2 not null,
	CreateDateTime datetime2 not null constraint dftbListingPriceCreateDate  default (getdate()),
 	CreateID int not null constraint dftbListingPriceCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbListingPriceUpdateDateTime  default (getdate()),
 	UpdateID int not null constraint dftbListingPriceUpdateID  default (0),
	EtlID int null,
 constraint pktbListingPrice primary key clustered 
(
	ListingPriceID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingPrice') and name = N'ncixtbListingPriceListing')
drop index ncixtbListingPriceListing on dbo.tbListingPrice
create nonclustered index ncixtbListingPriceListing on dbo.tbListingPrice(ListingID asc)
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingPrice') and name = N'ncixtbListingPricePrice')
drop index ncixtbListingPricePrice on dbo.tbListingPrice
create nonclustered index ncixtbListingPricePrice on dbo.tbListingPrice(ListingPrice asc)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingPrice', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the listing Price history.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingPrice'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingPrice', N'COLUMN', N'ListingID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbListing table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingPrice', @level2type=N'COLUMN',@level2name=N'ListingID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingPrice', N'COLUMN', N'PriceDateTime'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Actual price datetime.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingPrice', @level2type=N'COLUMN',@level2name=N'PriceDateTime'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingPrice', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingPrice', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingPrice', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingPrice', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingPrice', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingPrice', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListing') and type in (N'U'))
alter table dbo.tbListingPrice with check add constraint fktbListingPriceListing foreign key(ListingID)
references dbo.tbListing (ListingID)