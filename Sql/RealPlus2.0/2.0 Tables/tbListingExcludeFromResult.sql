use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingExcludeFromResult') and type in (N'U'))
drop table dbo.tbListingExcludeFromResult
go
create table dbo.tbListingExcludeFromResult(
	ListingExcludeFromResultID int identity(1,1) not null,
	ListingID int not null,
	CompanyID int null,
	AgentID int null,
	CustomerID int null,
	IsDeleted bit not null constraint dftbListingExcludeFromResultIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbListingExcludeFromResultCreateDate  default (getdate()),
 	CreateID int not null constraint dftbListingExcludeFromResultCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbListingExcludeFromResultUpdateDateTime  default (getdate()),
 	UpdateID int not null constraint dftbListingExcludeFromResultUpdateID  default (0),
 constraint pktbListingExcludeFromResult primary key clustered 
(
	ListingExcludeFromResultID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingExcludeFromResult') and name = N'ncixtbListingExcludeFromResultListing')
drop index ncixtbListingExcludeFromResultListing on dbo.tbListingExcludeFromResult
create nonclustered index ncixtbListingExcludeFromResultListing on dbo.tbListingExcludeFromResult(ListingID asc)
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingExcludeFromResult') and name = N'ncixtbListingExcludeFromResultCompany')
drop index ncixtbListingExcludeFromResultCompany on dbo.tbListingExcludeFromResult
create nonclustered index ncixtbListingExcludeFromResultCompany on dbo.tbListingExcludeFromResult(AgentID asc)
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingExcludeFromResult') and name = N'ncixtbListingExcludeFromResultAgent')
drop index ncixtbListingExcludeFromResultAgent on dbo.tbListingExcludeFromResult
create nonclustered index ncixtbListingExcludeFromResultAgent on dbo.tbListingExcludeFromResult(AgentID asc)
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingExcludeFromResult') and name = N'ncixtbListingExcludeFromResultCustomer')
drop index ncixtbListingExcludeFromResultCustomer on dbo.tbListingExcludeFromResult
create nonclustered index ncixtbListingExcludeFromResultCustomer on dbo.tbListingExcludeFromResult(CustomerID asc)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingExcludeFromResult', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the listings to exclude from results by all, company, agent and/or customer.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingExcludeFromResult'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingExcludeFromResult', N'COLUMN', N'ListingID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbListing table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingExcludeFromResult', @level2type=N'COLUMN',@level2name=N'ListingID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingExcludeFromResult', N'COLUMN', N'CompanyID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbCompany table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingExcludeFromResult', @level2type=N'COLUMN',@level2name=N'CompanyID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingExcludeFromResult', N'COLUMN', N'AgentID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAgent table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingExcludeFromResult', @level2type=N'COLUMN',@level2name=N'AgentID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingExcludeFromResult', N'COLUMN', N'CustomerID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbCustomer table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingExcludeFromResult', @level2type=N'COLUMN',@level2name=N'CustomerID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingExcludeFromResult', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingExcludeFromResult', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingExcludeFromResult', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingExcludeFromResult', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingExcludeFromResult', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingExcludeFromResult', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListing') and type in (N'U'))
alter table dbo.tbListingExcludeFromResult with check add constraint fktbListingExcludeFromResultListing foreign key(ListingID)
references dbo.tbListing (ListingID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgent') and type in (N'U'))
alter table dbo.tbListingExcludeFromResult with check add constraint fktbListingExcludeFromResultAgent foreign key(AgentID)
references dbo.tbAgent (AgentID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCustomer') and type in (N'U'))
alter table dbo.tbListingExcludeFromResult with check add constraint fktbListingExcludeFromResultCustomer foreign key(CustomerID)
references dbo.tbCustomer (CustomerID)
go