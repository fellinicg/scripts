use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPropertyAttributeText') and type in (N'U'))
drop table dbo.tbPropertyAttributeText
go
create table dbo.tbPropertyAttributeText(
	PropertyAttributeTextID int identity(1,1) not null,
	PropertyID int not null,
	AttributeID int not null,
	AttributeValue varchar(200) not null,
	IsDeleted bit constraint dftbPropertyAttributeTextIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbPropertyAttributeTextCreateDate  default (getdate()),
	CreateID int not null constraint dftbPropertyAttributeTextCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbPropertyAttributeTextUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbPropertyAttributeTextUpdateID  default (0),
 constraint pktbPropertyAttributeText primary key clustered 
(
	PropertyAttributeTextID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from sys.indexes WHERE object_id = object_id(N'dbo.tbPropertyAttributeText') and name = N'ncixtbPropertyAttributeTextProperty')
create nonclustered index ncixtbPropertyAttributeTextProperty on dbo.tbPropertyAttributeText(PropertyID asc)
with (pad_index = off, statistics_norecompute = off, sort_in_tempdb = off, drop_existing = off, online = off, allow_row_locks = on, allow_page_locks = on)
go
if not exists (select * from sys.indexes WHERE object_id = object_id(N'dbo.tbPropertyAttributeText') and name = N'ncixtbPropertyAttributeTextAttrVal')
create nonclustered index ncixtbPropertyAttributeTextAttrVal on dbo.tbPropertyAttributeText(AttributeValue asc)
with (pad_index = off, statistics_norecompute = off, sort_in_tempdb = off, drop_existing = off, online = off, allow_row_locks = on, allow_page_locks = on)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAttributeText', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains numeric attribute values for a property where the value is both string and is not stored in tbAttributeValue.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAttributeText'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAttributeText', N'COLUMN', N'PropertyID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbProperty table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAttributeText', @level2type=N'COLUMN',@level2name=N'PropertyID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAttributeText', N'COLUMN', N'AttributeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttribute table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAttributeText', @level2type=N'COLUMN',@level2name=N'AttributeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAttributeText', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAttributeText', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAttributeText', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAttributeText', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAttributeText', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAttributeText', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbProperty') and type in (N'U'))
alter table dbo.tbPropertyAttributeText with check add constraint fktbPropertyAttributeTextProperty foreign key(PropertyID)
references dbo.tbProperty (PropertyID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttribute') and type in (N'U'))
alter table dbo.tbPropertyAttributeText with check add constraint fktbPropertyAttributeTextAttribute foreign key(AttributeID)
references dbo.tbAttribute (AttributeID)