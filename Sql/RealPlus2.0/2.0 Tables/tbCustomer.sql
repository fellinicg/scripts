use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentListingPinCustomer') and parent_object_id = object_id(N'dbo.tbAgentListingPin'))
alter table dbo.tbAgentListingPin drop constraint fktbAgentListingPinCustomer
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentSavedCriteriaCustomer') and parent_object_id = object_id(N'dbo.tbAgentSavedCriteria'))
alter table dbo.tbAgentSavedCriteria drop constraint fktbAgentSavedCriteriaCustomer
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentWorkInProgressCustomer') and parent_object_id = object_id(N'dbo.tbAgentWorkInProgress'))
alter table dbo.tbAgentWorkInProgress drop constraint fktbAgentWorkInProgressCustomer
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCustomer') and type in (N'U'))
drop table dbo.tbCustomer
go
create table dbo.tbCustomer(
	CustomerID int not null,
	AgentID int not null,
	StatusID int not null,
	IsDeleted bit not null constraint dftbCustomerIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbCustomerCreateDate  default (getdate()),
 	CreateID int not null constraint dftbCustomerCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbCustomerUpdateDateTime  default (getdate()),
 	UpdateID int not null constraint dftbCustomerUpdateID  default (0),
constraint pktbCustomer primary key clustered 
(
	CustomerID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomer', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the relationship between customers and agents.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomer'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomer', N'COLUMN', N'AgentID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAgent table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomer', @level2type=N'COLUMN',@level2name=N'AgentID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomer', N'COLUMN', N'StatusID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomer', @level2type=N'COLUMN',@level2name=N'StatusID'
go
--if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomer', N'COLUMN', N'RecentMatchSettingID'))
--exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
--, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomer', @level2type=N'COLUMN',@level2name=N'RecentMatchSettingID'
--go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomer', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomer', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomer', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomer', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCustomer', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCustomer', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPerson') and type in (N'U'))
alter table dbo.tbCustomer  with check add constraint fktbCustomerPerson
foreign key(CustomerID) references dbo.tbPerson (PersonID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgent') and type in (N'U'))
alter table dbo.tbCustomer  with check add constraint fktbCustomerAgent 
foreign key(AgentID) references dbo.tbAgent (AgentID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgentListingPin') and type in (N'U'))
alter table dbo.tbAgentListingPin with check add constraint fktbAgentListingPinCustomer
foreign key(CustomerID) references dbo.tbCustomer (CustomerID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgentSavedCriteria') and type in (N'U'))
alter table dbo.tbAgentSavedCriteria with check add constraint fktbAgentSavedCriteriaCustomer
foreign key(CustomerID) references dbo.tbCustomer (CustomerID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgentWorkInProgress') and type in (N'U'))
alter table dbo.tbAgentWorkInProgress with check add constraint fktbAgentWorkInProgressCustomer
foreign key(CustomerID) references dbo.tbCustomer (CustomerID)
go