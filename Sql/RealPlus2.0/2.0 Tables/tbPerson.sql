use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentPerson') and parent_object_id = object_id(N'dbo.tbAgent'))
alter table dbo.tbAgent drop constraint fktbAgentPerson
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbCustomerPerson') and parent_object_id = object_id(N'dbo.tbCustomer'))
alter table dbo.tbCustomer drop constraint fktbCustomerPerson
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingUpdateID') and parent_object_id = object_id(N'dbo.tbListing'))
alter table dbo.tbListing drop constraint fktbListingUpdateID
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbOfficeContactPerson') and parent_object_id = object_id(N'dbo.tbOfficeContact'))
alter table dbo.tbOfficeContact drop constraint fktbOfficeContactPerson
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPersonAddressPerson') and parent_object_id = object_id(N'dbo.tbPersonAddress'))
alter table dbo.tbPersonAddress drop constraint fktbPersonAddressPerson
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPersonEmailPerson') and parent_object_id = object_id(N'dbo.tbPersonEmail'))
alter table dbo.tbPersonEmail drop constraint fktbPersonEmailPerson
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPersonPhonePerson') and parent_object_id = object_id(N'dbo.tbPersonPhone'))
alter table dbo.tbPersonPhone drop constraint fktbPersonPhonePerson
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbUserAccountPerson') and parent_object_id = object_id(N'dbo.tbUserAccount'))
alter table dbo.tbUserAccount drop constraint fktbUserAccountPerson
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbUserRolePerson') and parent_object_id = object_id(N'dbo.tbUserRole'))
alter table dbo.tbUserRole drop constraint fktbUserRolePerson
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPerson') and type in (N'U'))
drop table dbo.tbPerson
go
create table dbo.tbPerson(
	PersonID int identity(1,1) not null,
	NamePrefix varchar(20) null,
	FirstGivenName varchar(40) null,
	SecondGivenName varchar(40) null,
	FamilyName varchar(60) null,
	NameSuffix varchar(20) null,
	FullName varchar(200) not null,
	IsDeleted bit not null constraint dftbPersonIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbPersonCreateDate  default (getdate()),
	CreateID int not null constraint dftbPersonCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbPersonUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbPersonUpdateID  default (0),
	EtlID int null,
 constraint _tbPerson primary key clustered 
(
	PersonID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPerson', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the base information about a person.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPerson'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPerson', N'COLUMN', N'FullName'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'To allow for the lack of control, this is the only non-null field.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPerson', @level2type=N'COLUMN',@level2name=N'FullName'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPerson', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPerson', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPerson', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPerson', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPerson', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPerson', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgent') and type in (N'U'))
alter table dbo.tbAgent with check add constraint fktbAgentPerson
foreign key(AgentID) references dbo.tbPerson (PersonID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCustomer') and type in (N'U'))
alter table dbo.tbCustomer with check add constraint fktbCustomerPerson
foreign key(CustomerID) references dbo.tbPerson (PersonID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListing') and type in (N'U'))
alter table dbo.tbListing with check add constraint fktbListingUpdateID
foreign key(UpdateID) references dbo.tbPerson (PersonID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbOfficeContact') and type in (N'U'))
alter table dbo.tbOfficeContact with check add constraint fktbOfficeContactPerson
foreign key(PersonID) references dbo.tbPerson (PersonID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPersonAddress') and type in (N'U'))
alter table dbo.tbPersonAddress with check add constraint fktbPersonAddressPerson
foreign key(PersonID) references dbo.tbPerson (PersonID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPersonEmail') and type in (N'U'))
alter table dbo.tbPersonEmail with check add constraint fktbPersonEmailPerson
foreign key(PersonID) references dbo.tbPerson (PersonID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPersonPhone') and type in (N'U'))
alter table dbo.tbPersonPhone with check add constraint fktbPersonPhonePerson
foreign key(PersonID) references dbo.tbPerson (PersonID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbUserAccount') and type in (N'U'))
alter table dbo.tbUserAccount with check add constraint fktbUserAccountPerson
foreign key(PersonID) references dbo.tbPerson (PersonID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbUserRole') and type in (N'U'))
alter table dbo.tbUserRole with check add constraint fktbUserRolePerson
foreign key(PersonID) references dbo.tbPerson (PersonID)
go