use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbOfficeContact') and type in (N'U'))
drop table dbo.tbOfficeContact
go
create table dbo.tbOfficeContact(
	OfficeID int not null,
	PersonID int not null,
	IsPrimary bit not null constraint dftbOfficeContactIsPrimary default 0,	
	IsDeleted bit not null constraint dftbOfficeContactIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbOfficeContactCreateDate  default (getdate()),
	CreateID int not null constraint dftbOfficeContactCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbOfficeContactUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbOfficeContactUpdateID  default (0),
 constraint pktbOfficeContact primary key clustered 
(
	OfficeID, PersonID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbOfficeContact', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the contacts for each office.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbOfficeContact'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbOfficeContact', N'COLUMN', N'OfficeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbOffice table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbOfficeContact', @level2type=N'COLUMN',@level2name=N'OfficeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbOfficeContact', N'COLUMN', N'PersonID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbPerson table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbOfficeContact', @level2type=N'COLUMN',@level2name=N'PersonID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbOfficeContact', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbOfficeContact', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbOfficeContact', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbOfficeContact', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbOfficeContact', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbOfficeContact', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbOffice') and type in (N'U'))
alter table dbo.tbOfficeContact  with check add constraint fktbOfficeContactOffice foreign key(OfficeID)
references dbo.tbOffice (OfficeID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPerson') and type in (N'U'))
alter table dbo.tbOfficeContact  with check add constraint fktbOfficeContactPerson foreign key(PersonID)
references dbo.tbPerson (PersonID)