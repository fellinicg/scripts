use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbSharedCustomer') and type in (N'U'))
drop table dbo.tbSharedCustomer
go
create table dbo.tbSharedCustomer(
	CustomerID int not null,
	AgentID int not null,
	PermissionID char(1) not null constraint dftbSharedCustomerPermissionID default 'R',
	IsDeleted bit constraint dftbSharedCustomerIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbSharedCustomerCreateDate  default (getdate()),
	CreateID int not null constraint dftbSharedCustomerCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbSharedCustomerUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbSharedCustomerUpdateID  default (0),
 constraint pktbSharedCustomer primary key clustered 
(
	CustomerID, AgentID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbSharedCustomer', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the shared relationships between agents and customers.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbSharedCustomer'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbSharedCustomer', N'COLUMN', N'AgentID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAgent table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbSharedCustomer', @level2type=N'COLUMN',@level2name=N'AgentID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbSharedCustomer', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbSharedCustomer', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbSharedCustomer', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbSharedCustomer', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbSharedCustomer', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbSharedCustomer', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCustomer') and type in (N'U'))
alter table dbo.tbSharedCustomer  with check add constraint fktbSharedCustomer foreign key(CustomerID)
references dbo.tbCustomer (CustomerID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgent') and type in (N'U'))
alter table dbo.tbSharedCustomer  with check add constraint fktbSharedCustomerAgent foreign key(AgentID)
references dbo.tbAgent (AgentID)