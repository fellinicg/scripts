use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingPostalAddress') and parent_object_id = object_id(N'dbo.tbListing'))
alter table dbo.tbListing drop constraint fktbListingPostalAddress
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbOfficePostalAddress') and parent_object_id = object_id(N'dbo.tbOffice'))
alter table dbo.tbOffice drop constraint fktbOfficePostalAddress
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPersonAddressPostalAddress') and parent_object_id = object_id(N'dbo.tbPersonAddress'))
alter table dbo.tbPersonAddress drop constraint fktbPersonAddressPostalAddress
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyAddressPostalAddress') and parent_object_id = object_id(N'dbo.tbPropertyAddress'))
alter table dbo.tbPropertyAddress drop constraint fktbPropertyAddressPostalAddress
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPostalAddress') and type in (N'U'))
drop table dbo.tbPostalAddress
go
create table dbo.tbPostalAddress(
	PostalAddressID int identity(1,1) not null,
	--Number int null,
	--Street varchar(80) null,
	Line1 varchar(80) not null,
	Line2 varchar(80) null,
	Line3 varchar(80) null,
	Line4 varchar(80) null,
	CityTown varchar(40) null,
	StateProvince varchar(40) null,
	PostalCode varchar(20) null,
	Country char(2) null,
	Latitude float null,
	Longitude float null,
	IsDeleted bit constraint dftbPostalAddressIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbPostalAddressCreateDate  default (getdate()),
	CreateID int not null constraint dftbPostalAddressCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbPostalAddressUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbPostalAddressUpdateID  default (0),
 constraint pktbPostalAddress primary key clustered 
(
	PostalAddressID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPostalAddress', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the base information about an address.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPostalAddress'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPostalAddress', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPostalAddress', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPostalAddress', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPostalAddress', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPostalAddress', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPostalAddress', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbOffice') and type in (N'U'))
alter table dbo.tbOffice with check add constraint fktbOfficePostalAddress
foreign key(PostalAddressID) references dbo.tbPostalAddress (PostalAddressID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPersonAddress') and type in (N'U'))
alter table dbo.tbPersonAddress with check add constraint fktbPersonAddressPostalAddress
foreign key(PostalAddressID) references dbo.tbPostalAddress (PostalAddressID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPropertyAddress') and type in (N'U'))
alter table dbo.tbPropertyAddress with check add constraint fktbPropertyAddressPostalAddress
foreign key(PostalAddressID) references dbo.tbPostalAddress (PostalAddressID)
go