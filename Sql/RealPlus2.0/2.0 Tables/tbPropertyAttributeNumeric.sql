use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPropertyAttributeNumeric') and type in (N'U'))
drop table dbo.tbPropertyAttributeNumeric
go
create table dbo.tbPropertyAttributeNumeric(
	PropertyAttributeNumericID int identity(1,1) not null,
	PropertyID int not null,
	AttributeID int not null,
	AttributeValue float not null,
	IsDeleted bit not null constraint dftbPropertyAttributeNumericIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbPropertyAttributeNumericCreateDate  default (getdate()),
	CreateID int not null constraint dftbPropertyAttributeNumericCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbPropertyAttributeNumericUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbPropertyAttributeNumericUpdateID  default (0),
 constraint pktbPropertyAttributeNumeric primary key nonclustered 
(
	PropertyAttributeNumericID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from sys.indexes WHERE object_id = object_id(N'dbo.tbPropertyAttributeNumeric') and name = N'ncixtbPropertyAttributeNumericProperty')
create clustered index ncixtbPropertyAttributeNumericProperty on dbo.tbPropertyAttributeNumeric(PropertyID asc)
go
if not exists (select * from sys.indexes WHERE object_id = object_id(N'dbo.tbPropertyAttributeNumeric') and name = N'ncixtbPropertyAttributeNumericAttrVal')
create nonclustered index ncixtbPropertyAttributeNumericAttrVal on dbo.tbPropertyAttributeNumeric(AttributeValue asc)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAttributeNumeric', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains numeric attribute values for a property where the value is both numeric and is not stored in tbAttributeValue.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAttributeNumeric'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAttributeNumeric', N'COLUMN', N'PropertyID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbProperty table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAttributeNumeric', @level2type=N'COLUMN',@level2name=N'PropertyID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAttributeNumeric', N'COLUMN', N'AttributeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttribute table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAttributeNumeric', @level2type=N'COLUMN',@level2name=N'AttributeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAttributeNumeric', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAttributeNumeric', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAttributeNumeric', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAttributeNumeric', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyAttributeNumeric', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyAttributeNumeric', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbProperty') and type in (N'U'))
alter table dbo.tbPropertyAttributeNumeric with check add constraint fktbPropertyAttributeNumericProperty foreign key(PropertyID)
references dbo.tbProperty (PropertyID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttribute') and type in (N'U'))
alter table dbo.tbPropertyAttributeNumeric with check add constraint fktbPropertyAttributeNumericAttribute foreign key(AttributeID)
references dbo.tbAttribute (AttributeID)