use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgentOffice') and type in (N'U'))
drop table dbo.tbAgentOffice
go
create table dbo.tbAgentOffice(
	AgentOfficeID int identity(1,1) not null,
	AgentID int not null,
	OfficeID int not null,
	--IsCurrent bit not null constraint dftbAgentOfficeIsCurrent default 0,
	IsDeleted bit not null constraint dftbAgentOfficeIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbAgentOfficeCreateDate  default (getdate()),
	CreateID int not null constraint dftbAgentOfficeCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbAgentOfficeUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbAgentOfficeUpdateID  default (0),
 constraint pktbAgentOffice primary key clustered 
(
	AgentOfficeID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentOffice', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the agent to office relationship.  An agent can belong to more than one office.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentOffice'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentOffice', N'COLUMN', N'AgentID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAgent table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentOffice', @level2type=N'COLUMN',@level2name=N'AgentID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentOffice', N'COLUMN', N'OfficeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbOffice table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentOffice', @level2type=N'COLUMN',@level2name=N'OfficeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentOffice', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentOffice', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentOffice', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentOffice', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentOffice', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentOffice', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgent') and type in (N'U'))
alter table dbo.tbAgentOffice with check add constraint fktbAgentOfficeAgent 
foreign key(AgentID) references dbo.tbAgent (AgentID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbOffice') and type in (N'U'))
alter table dbo.tbAgentOffice with check add constraint fktbAgentOfficeOffice 
foreign key(OfficeID) references dbo.tbOffice (OfficeID)
go