use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingComment') and type in (N'U'))
drop table dbo.tbListingComment
go
create table dbo.tbListingComment(
	ListingCommentID int identity(1,1) not null,
	ListingID int not null,
	DetailID int not null,
	CompanyID int null,
	PersonID int null,
	IsDeleted bit constraint dftbListingCommentIsDeleted default 0,	
 constraint pktbListingComment primary key clustered 
(
	ListingCommentID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingComment') and name = N'ncixtbListingCommentListing')
drop index ncixtbListingCommentListing on dbo.tbListingComment
create nonclustered index ncixtbListingCommentListing on dbo.tbListingComment(ListingID asc)
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingComment') and name = N'ncixtbListingCommentDetail')
drop index ncixtbListingCommentDetail on dbo.tbListingComment
create nonclustered index ncixtbListingCommentDetail on dbo.tbListingComment(DetailID asc)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingComment', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the relationship betwee tbDetail and tbListing to store comments about a Listing.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingComment'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingComment', N'COLUMN', N'ListingID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbListing table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingComment', @level2type=N'COLUMN',@level2name=N'ListingID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingComment', N'COLUMN', N'DetailID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbDetail table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingComment', @level2type=N'COLUMN',@level2name=N'DetailID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingComment', N'COLUMN', N'CompanyID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbCompany table.  Provides optional company level comments'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingComment', @level2type=N'COLUMN',@level2name=N'CompanyID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingComment', N'COLUMN', N'PersonID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbPerson table.  Provides optional person level comments'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingComment', @level2type=N'COLUMN',@level2name=N'PersonID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingComment', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingComment', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListing') and type in (N'U'))
alter table dbo.tbListingComment with check add constraint fktbListingCommentListing foreign key(ListingID)
references dbo.tbListing (ListingID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbDetail') and type in (N'U'))
alter table dbo.tbListingComment with check add constraint fktbListingCommentDetail foreign key(DetailID)
references dbo.tbDetail (DetailID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCompany') and type in (N'U'))
alter table dbo.tbListingComment with check add constraint fktbListingCommentCompany foreign key(CompanyID)
references dbo.tbCompany (CompanyID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPerson') and type in (N'U'))
alter table dbo.tbListingComment with check add constraint fktbListingCommentPerson foreign key(PersonID)
references dbo.tbPerson (PersonID)
go