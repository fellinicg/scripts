use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyAddressProperty') and parent_object_id = object_id(N'dbo.tbPropertyAddress'))
alter table dbo.tbPropertyAddress drop constraint fktbPropertyAddressProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyAliasProperty') and parent_object_id = object_id(N'dbo.tbPropertyAlias'))
alter table dbo.tbPropertyAlias drop constraint fktbPropertyAliasProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyAttributeNumericProperty') and parent_object_id = object_id(N'dbo.tbPropertyAttributeNumeric'))
alter table dbo.tbPropertyAttributeNumeric drop constraint fktbPropertyAttributeNumericProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyAttributeProperty') and parent_object_id = object_id(N'dbo.tbPropertyAttribute'))
alter table dbo.tbPropertyAttribute drop constraint fktbPropertyAttributeProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyAttributeTextProperty') and parent_object_id = object_id(N'dbo.tbPropertyAttributeText'))
alter table dbo.tbPropertyAttributeText drop constraint fktbPropertyAttributeTextProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingPropertyID') and parent_object_id = object_id(N'dbo.tbListing'))
alter table dbo.tbListing drop constraint fktbListingPropertyID
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbProperty') and type in (N'U'))
drop table dbo.tbProperty
go
create table dbo.tbProperty(
	PropertyID int identity(1,1) not null,
	PropertyClassID int not null,
	ParentPropertyID int null,
	--PostalAddressID int not null,
	Block varchar(10) null,
	Lot varchar(10) null,
	Section varchar(10) null,
	Latitude varchar(20) null,
	Longitude varchar(20) null,
	IsDeleted bit not null constraint dftbPropertyIsDeleted default 0,	
	CreateDateTime datetime not null constraint dftbPropertyCreateDate  default (getdate()),
	CreateID int not null constraint dftbPropertyCreateID  default (0),
	UpdateDateTime datetime not null constraint dftbPropertyUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbPropertyUpdateID  default (0),
	EtlID int null,
constraint pktbProperty primary key nonclustered 
(
	PropertyID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from sys.indexes WHERE object_id = object_id(N'dbo.tbProperty') and name = N'ncixtbPropertyParent')
create clustered index ncixtbPropertyParent on dbo.tbProperty(ParentPropertyID asc)
with (pad_index = off, statistics_norecompute = off, sort_in_tempdb = off, drop_existing = off, online = off, allow_row_locks = on, allow_page_locks = on)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbProperty', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the base information for all Propertys.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbProperty'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbProperty', N'COLUMN', N'PropertyClassID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbProperty', @level2type=N'COLUMN',@level2name=N'PropertyClassID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbProperty', N'COLUMN', N'ParentPropertyID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbProperty table. Parent-Child relationship (eg. Building-Unit)'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbProperty', @level2type=N'COLUMN',@level2name=N'ParentPropertyID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbProperty', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbProperty', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbProperty', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbProperty', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbProperty', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbProperty', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbProperty with check add constraint fktbPropertyPropertyClass
foreign key(PropertyClassID) references dbo.tbAttributeValue (AttributeValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPropertyAddress') and type in (N'U'))
alter table dbo.tbPropertyAddress with check add constraint fktbPropertyAddressProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPropertyAlias') and type in (N'U'))
alter table dbo.tbPropertyAlias with check add constraint fktbPropertyAliasProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPropertyAttributeNumeric') and type in (N'U'))
alter table dbo.tbPropertyAttributeNumeric with check add constraint fktbPropertyAttributeNumericProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPropertyAttribute') and type in (N'U'))
alter table dbo.tbPropertyAttribute with check add constraint fktbPropertyAttributeProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPropertyAttributeText') and type in (N'U'))
alter table dbo.tbPropertyAttributeText with check add constraint fktbPropertyAttributeTextProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListing') and type in (N'U'))
alter table dbo.tbListing with check add constraint fktbListingPropertyID
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go