use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingAttributeNumeric') and type in (N'U'))
drop table dbo.tbListingAttributeNumeric
go
create table dbo.tbListingAttributeNumeric(
	ListingAttributeNumericID int identity(1,1) not null,
	ListingID int not null,
	AttributeID int not null,
	AttributeValue float not null,
	IsDeleted bit not null constraint dftbListingAttributeNumericIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbListingAttributeNumericCreateDate  default (getdate()),
 	CreateID int not null constraint dftbListingAttributeNumericCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbListingAttributeNumericUpdateDateTime  default (getdate()),
 	UpdateID int not null constraint dftbListingAttributeNumericUpdateID  default (0),
 constraint pktbListingAttributeNumeric primary key clustered 
(
	ListingAttributeNumericID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from sys.indexes WHERE object_id = object_id(N'dbo.tbListingAttributeNumeric') and name = N'ncixtbListingAttributeNumeric')
create nonclustered index ncixtbListingAttributeNumeric on dbo.tbListingAttributeNumeric(ListingID asc)
go
if not exists (select * from sys.indexes WHERE object_id = object_id(N'dbo.tbListingAttributeNumeric') and name = N'ncixtbListingAttributeNumericAttr')
create nonclustered index ncixtbListingAttributeNumericAttr on dbo.tbListingAttributeNumeric(AttributeValue asc)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingAttributeNumeric', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains numeric attribute values for a listing where the value is both numeric and is not stored in tbAttributeValue.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingAttributeNumeric'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingAttributeNumeric', N'COLUMN', N'ListingID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbListing table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingAttributeNumeric', @level2type=N'COLUMN',@level2name=N'ListingID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingAttributeNumeric', N'COLUMN', N'AttributeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttribute table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingAttributeNumeric', @level2type=N'COLUMN',@level2name=N'AttributeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingAttributeNumeric', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingAttributeNumeric', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingAttributeNumeric', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingAttributeNumeric', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingAttributeNumeric', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingAttributeNumeric', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListing') and type in (N'U'))
alter table dbo.tbListingAttributeNumeric with check add constraint fktbListingAttributeNumericListing foreign key(ListingID)
references dbo.tbListing (ListingID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttribute') and type in (N'U'))
alter table dbo.tbListingAttributeNumeric with check add constraint fktbListingAttributeNumericAttribute foreign key(AttributeID)
references dbo.tbAttribute (AttributeID)