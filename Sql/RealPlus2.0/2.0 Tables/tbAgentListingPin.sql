use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgentListingPin') and type in (N'U'))
drop table dbo.tbAgentListingPin
go
create table dbo.tbAgentListingPin(
	AgentListingPinID int identity(1,1) not null,
	AgentID int not null,
	ListingID int not null,
	CustomerID int null,
	PickListed bit not null constraint dftbAgentListingPinPickListed default 0,
	Liked bit not null constraint dftbAgentListingPinLiked default 0,
	Disiked bit not null constraint dftbAgentListingPinDisiked default 0,
	Visited bit not null constraint dftbAgentListingPinVisited default 0,
	--IsDeleted bit not null constraint dftbAgentListingPinIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbAgentListingPinCreateDate  default (getdate()),
	CreateID int not null constraint dftbAgentListingPinCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbAgentListingPinUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbAgentListingPinUpdateID  default (0),
 constraint pktbAgentListingPin primary key clustered 
(
	AgentListingPinID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentListingPin', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains whether an agent has pinned a listing for pick list, like, dislike or visited.  Can save personal and per customer'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentListingPin'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentListingPin', N'COLUMN', N'AgentID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAgent table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentListingPin', @level2type=N'COLUMN',@level2name=N'AgentID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentListingPin', N'COLUMN', N'ListingID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbListing table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentListingPin', @level2type=N'COLUMN',@level2name=N'ListingID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentListingPin', N'COLUMN', N'CustomerID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbCustomer table, but not required.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentListingPin', @level2type=N'COLUMN',@level2name=N'CustomerID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentListingPin', N'COLUMN', N'PickListed'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this listing has been pick listed.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentListingPin', @level2type=N'COLUMN',@level2name=N'PickListed'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentListingPin', N'COLUMN', N'Liked'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this listing has been liked.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentListingPin', @level2type=N'COLUMN',@level2name=N'Liked'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentListingPin', N'COLUMN', N'Disiked'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this listing has been disiked.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentListingPin', @level2type=N'COLUMN',@level2name=N'Disiked'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentListingPin', N'COLUMN', N'Visited'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this listing has been visited.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentListingPin', @level2type=N'COLUMN',@level2name=N'Visited'
go
--if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentListingPin', N'COLUMN', N'IsDeleted'))
--exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
--, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentListingPin', @level2type=N'COLUMN',@level2name=N'IsDeleted'
--go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentListingPin', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentListingPin', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbAgentListingPin', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbAgentListingPin', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgent') and type in (N'U'))
alter table dbo.tbAgentListingPin with check add constraint fktbAgentListingPinAgent
foreign key(AgentID) references dbo.tbAgent (AgentID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListing') and type in (N'U'))
alter table dbo.tbAgentListingPin with check add constraint fktbAgentListingPinListing 
foreign key(ListingID) references dbo.tbListing (ListingID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCustomer') and type in (N'U'))
alter table dbo.tbAgentListingPin with check add constraint fktbAgentListingPinCustomer
foreign key(CustomerID) references dbo.tbCustomer (CustomerID)
go