use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPersonEmailPerson') and parent_object_id = object_id(N'dbo.tbPersonEmail'))
alter table dbo.tbPersonEmail drop constraint fktbPersonEmailPerson
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyContactEmailPerson') and parent_object_id = object_id(N'dbo.tbPropertyContactEmail')) 
alter table dbo.tbPropertyContactEmail drop constraint fktbPropertyContactEmailPerson 
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPersonEmail') and type in (N'U'))
drop table dbo.tbPersonEmail
go
create table dbo.tbPersonEmail(
	PersonEmailID int identity(1,1) not null,
	PersonID int not null,
	EmailTypeID int not null,
	EmailAddress nvarchar(254) not null,
	IsValidFormat  as (dbo.ufEmailAddressIsValid(EmailAddress)),
	IsPreferred bit not null constraint dftbPersonEmailIsPreferred default 0,
	IsDeleted bit not null constraint dftbPersonEmailIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbPersonEmailCreateDate  default (getdate()),
	CreateID int not null constraint dftbPersonEmailCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbPersonEmailUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbPersonEmailUpdateID  default (0),
 constraint pktbPersonEmail primary key clustered 
 (
	PersonEmailID asc
 ) with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY],
 constraint aktbPersonEmail unique nonclustered 
 (
	PersonID, EmailTypeID, EmailAddress
 ) with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonEmail', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the email to person relationship.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonEmail'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonEmail', N'COLUMN', N'PersonID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbPerson table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonEmail', @level2type=N'COLUMN',@level2name=N'PersonID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonEmail', N'COLUMN', N'EmailTypeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonEmail', @level2type=N'COLUMN',@level2name=N'EmailTypeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonEmail', N'COLUMN', N'IsValidFormat'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Function to represent whether this record is a valid email address, may be unnecessary in this application.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonEmail', @level2type=N'COLUMN',@level2name=N'IsValidFormat'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonEmail', N'COLUMN', N'IsPreferred'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record is the preferred Email.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonEmail', @level2type=N'COLUMN',@level2name=N'IsPreferred'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonEmail', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonEmail', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonEmail', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonEmail', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonEmail', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonEmail', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPerson') and type in (N'U'))
alter table dbo.tbPersonEmail with check add constraint fktbPersonEmailPerson foreign key(PersonID)
references dbo.tbPerson (PersonID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbPersonEmail with check add constraint fktbPersonEmailType foreign key(EmailTypeID)
references dbo.tbAttributeValue (AttributeValueID)