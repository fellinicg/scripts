use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingOpenHouse') and type in (N'U'))
drop table dbo.tbListingOpenHouse
go
create table dbo.tbListingOpenHouse(
	ListingOpenHouseID int identity(1,1) not null,
	ListingID int not null,
	AgentID int not null,
	OfficeID int not null,
	StartDateTime datetime2 not null,
	EndDateTime datetime2 not null,
	Comment varchar(200) null,
	AppointmentOnly bit not null constraint dftbListingOpenHouseAppointmentOnly default 0,
	IsDeleted bit not null constraint dftbListingOpenHouseIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbListingOpenHouseCreateDate  default (getdate()),
	CreateID int not null constraint dftbListingOpenHouseCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbListingOpenHouseUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbListingOpenHouseUpdateID  default (0),
	EtlID int null,
 constraint pktbListingOpenHouse primary key clustered 
(
	ListingOpenHouseID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingOpenHouse') and name = N'ncixtbtbListingOpenHouseListing')
drop index ncixtbtbListingOpenHouseListing on dbo.tbListingOpenHouse
create nonclustered index ncixtbtbListingOpenHouseListing on dbo.tbListingOpenHouse(ListingID asc)
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingOpenHouse') and name = N'ncixtbtbListingOpenHouseAgent')
drop index ncixtbtbListingOpenHouseAgent on dbo.tbListingOpenHouse
create nonclustered index ncixtbtbListingOpenHouseAgent on dbo.tbListingOpenHouse(AgentID asc)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingOpenHouse', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the listing to open house relationship.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingOpenHouse'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingOpenHouse', N'COLUMN', N'ListingID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbListing table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingOpenHouse', @level2type=N'COLUMN',@level2name=N'ListingID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingOpenHouse', N'COLUMN', N'AgentID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAgent table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingOpenHouse', @level2type=N'COLUMN',@level2name=N'AgentID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingOpenHouse', N'COLUMN', N'OfficeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbOffice table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingOpenHouse', @level2type=N'COLUMN',@level2name=N'OfficeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingOpenHouse', N'COLUMN', N'StartDateTime'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Start date and time of the open house.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingOpenHouse', @level2type=N'COLUMN',@level2name=N'StartDateTime'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingOpenHouse', N'COLUMN', N'EndDateTime'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'End date and time of the open house.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingOpenHouse', @level2type=N'COLUMN',@level2name=N'EndDateTime'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingOpenHouse', N'COLUMN', N'AppointmentOnly'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether an appointment is necessary.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingOpenHouse', @level2type=N'COLUMN',@level2name=N'AppointmentOnly'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingOpenHouse', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingOpenHouse', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingOpenHouse', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingOpenHouse', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingOpenHouse', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingOpenHouse', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListing') and type in (N'U'))
alter table dbo.tbListingOpenHouse with check add constraint fktbListingOpenHouseListing 
foreign key(ListingID) references dbo.tbListing (ListingID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAgent') and type in (N'U'))
alter table dbo.tbListingOpenHouse with check add constraint fktbListingOpenHouseAgent
foreign key(AgentID) references dbo.tbAgent (AgentID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbOffice') and type in (N'U'))
alter table dbo.tbListingOpenHouse with check add constraint fktbListingOpenHouseOffice
foreign key(OfficeID) references dbo.tbOffice (OfficeID)
go