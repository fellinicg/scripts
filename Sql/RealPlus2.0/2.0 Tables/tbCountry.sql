use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCountry') and type in (N'U'))
drop table dbo.tbCountry
go
create table dbo.tbCountry(
	CountryID int identity(1,1) not null,
	CountryCode char(2) not null,
	CountryName varchar(45) not null,
	FipsCode char(2) null,
	North varchar(30) null,
	South varchar(30) null,
	East varchar(30) null,
	West varchar(30) null,
    Capital varchar(30) null,
    ContinentName varchar(15) null,
    Continent char(2) null,
    AreaInSqKm varchar(20) null,
    GeonameId int null,
	IsDeleted bit constraint dftbCountryIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbCountryCreateDate  default (getdate()),
	UpdateDateTime datetime2 not null constraint dftbCountryUpdateDateTime  default (getdate()),
 constraint pktbCountry primary key clustered 
(
	CountryID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbCountry', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Country look up table not being used at the moment.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCountry'
go
