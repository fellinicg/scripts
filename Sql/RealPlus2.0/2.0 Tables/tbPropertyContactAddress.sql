use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPropertyContactAddress') and type in (N'U'))
drop table dbo.tbPropertyContactAddress
go
create table dbo.tbPropertyContactAddress(
	PropertyContactAddressID int identity(1,1) not null,
	PropertyID int not null,
	PropertyContactTypeID int not null,
	PersonAddressID int not null,
	IsDeleted bit constraint dftbPropertyContactAddressIsDeleted default 0,	
 constraint pktbPropertyContactAddress primary key clustered 
(
	PropertyContactAddressID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbPropertyContactAddress') and name = N'ncixtbPropertyContactAddressProperty')
drop index ncixtbPropertyContactAddressProperty on dbo.tbPropertyContactAddress
create nonclustered index ncixtbPropertyContactAddressProperty on dbo.tbPropertyContactAddress(PropertyID asc)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyContactAddress', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains property address contact information.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyContactAddress'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyContactAddress', N'COLUMN', N'PropertyID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbProperty table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyContactAddress', @level2type=N'COLUMN',@level2name=N'PropertyID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyContactAddress', N'COLUMN', N'PropertyContactTypeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyContactAddress', @level2type=N'COLUMN',@level2name=N'PropertyContactTypeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyContactAddress', N'COLUMN', N'PersonAddressID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbPersonAddress table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyContactAddress', @level2type=N'COLUMN',@level2name=N'PersonAddressID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyContactAddress', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyContactAddress', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbProperty') and type in (N'U'))
alter table dbo.tbPropertyContactAddress with check add constraint fktbPropertyContactAddressProperty foreign key(PropertyID)
references dbo.tbProperty (PropertyID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbPropertyContactAddress with check add constraint fktbPropertyContactAddressContactType foreign key(PropertyContactTypeID)
references dbo.tbAttributeValue (AttributeValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPersonAddress') and type in (N'U'))
alter table dbo.tbPropertyContactAddress with check add constraint fktbPropertyContactAddressPerson foreign key(PersonAddressID)
references dbo.tbPersonAddress (PersonAddressID)
go