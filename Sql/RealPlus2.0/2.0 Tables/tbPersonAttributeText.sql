use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPersonAttributeText') and type in (N'U'))
drop table dbo.tbPersonAttributeText
go
create table dbo.tbPersonAttributeText(
	PersonAttributeTextID int identity(1,1) not null,
	PersonID int not null,
	AttributeID int not null,
	AttributeValue varchar(200) not null,
	IsDeleted bit constraint dftbPersonAttributeTextIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbPersonAttributeTextCreateDate  default (getdate()),
	CreateID int not null constraint dftbPersonAttributeTextCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbPersonAttributeTextUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbPersonAttributeTextUpdateID  default (0),
 constraint pktbPersonAttributeText primary key clustered 
(
	PersonAttributeTextID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from sys.indexes WHERE object_id = object_id(N'dbo.tbPersonAttributeText') and name = N'ncixtbPersonAttributeText')
create nonclustered index ncixtbPersonAttributeText on dbo.tbPersonAttributeText(PersonID asc, AttributeValue asc)
with (pad_index = off, statistics_norecompute = off, sort_in_tempdb = off, drop_existing = off, online = off, allow_row_locks = on, allow_page_locks = on)
go
if not exists (select * from ::fn_listextendedProperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonAttributeText', null, null))
exec sys.sp_addextendedProperty @name=N'Description', @value=N'Contains numeric attribute values for a Person where the value is both string and is not stored in tbAttributeValue.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonAttributeText'
go
if not exists (select * from ::fn_listextendedProperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonAttributeText', N'COLUMN', N'PersonID'))
exec sys.sp_addextendedProperty @name=N'Description', @value=N'Foreign key to the tbPerson table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonAttributeText', @level2type=N'COLUMN',@level2name=N'PersonID'
go
if not exists (select * from ::fn_listextendedProperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonAttributeText', N'COLUMN', N'AttributeID'))
exec sys.sp_addextendedProperty @name=N'Description', @value=N'Foreign key to the tbAttribute table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonAttributeText', @level2type=N'COLUMN',@level2name=N'AttributeID'
go
if not exists (select * from ::fn_listextendedProperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonAttributeText', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedProperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonAttributeText', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedProperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonAttributeText', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedProperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonAttributeText', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedProperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPersonAttributeText', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedProperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPersonAttributeText', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPerson') and type in (N'U'))
alter table dbo.tbPersonAttributeText with check add constraint fktbPersonAttributeTextPerson foreign key(PersonID)
references dbo.tbPerson (PersonID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttribute') and type in (N'U'))
alter table dbo.tbPersonAttributeText with check add constraint fktbPersonAttributeTextAttribute foreign key(AttributeID)
references dbo.tbAttribute (AttributeID)