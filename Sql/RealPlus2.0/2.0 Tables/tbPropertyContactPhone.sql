use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPropertyContactPhone') and type in (N'U'))
drop table dbo.tbPropertyContactPhone
go
create table dbo.tbPropertyContactPhone(
	PropertyContactPhoneID int identity(1,1) not null,
	PropertyID int not null,
	PropertyContactTypeID int not null,
	PersonPhoneID int not null,
	IsDeleted bit constraint dftbPropertyContactPhoneIsDeleted default 0,	
 constraint pktbPropertyContactPhone primary key clustered 
(
	PropertyContactPhoneID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbPropertyContactPhone') and name = N'ncixtbPropertyContactPhoneProperty')
drop index ncixtbPropertyContactPhoneProperty on dbo.tbPropertyContactPhone
create nonclustered index ncixtbPropertyContactPhoneProperty on dbo.tbPropertyContactPhone(PropertyID asc)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyContactPhone', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains property phone contact information.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyContactPhone'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyContactPhone', N'COLUMN', N'PropertyID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbProperty table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyContactPhone', @level2type=N'COLUMN',@level2name=N'PropertyID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyContactPhone', N'COLUMN', N'PropertyContactTypeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyContactPhone', @level2type=N'COLUMN',@level2name=N'PropertyContactTypeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyContactPhone', N'COLUMN', N'PersonPhoneID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbPersonPhone table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyContactPhone', @level2type=N'COLUMN',@level2name=N'PersonPhoneID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbPropertyContactPhone', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbPropertyContactPhone', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbProperty') and type in (N'U'))
alter table dbo.tbPropertyContactPhone with check add constraint fktbPropertyContactPhoneProperty foreign key(PropertyID)
references dbo.tbProperty (PropertyID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbPropertyContactPhone with check add constraint fktbPropertyContactPhoneContactType foreign key(PropertyContactTypeID)
references dbo.tbAttributeValue (AttributeValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPersonPhone') and type in (N'U'))
alter table dbo.tbPropertyContactPhone with check add constraint fktbPropertyContactPhonePerson foreign key(PersonPhoneID)
references dbo.tbPersonPhone (PersonPhoneID)
go