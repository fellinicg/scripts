use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAttributeValueValue') and parent_object_id = object_id(N'dbo.tbAttributeValue'))
alter table dbo.tbAttributeValue drop constraint fktbAttributeValueValue
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbValue') and type in (N'U'))
drop table dbo.tbValue
go
create table dbo.tbValue(
	ValueID int identity(1,1) not null,
	ValueCode varchar(10) null,
	ValueName varchar(100) not null,
	IsDeleted bit not null constraint dftbValueIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbValueCreateDate  default (getdate()),
	CreateID int not null constraint dftbValueCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbValueUpdateDate  default (getdate()),
	UpdateID int not null constraint dftbValueUpdateID  default (0),
 constraint pktbValue primary key clustered 
(
	ValueID asc
) on [PRIMARY],
 constraint aktbValue unique nonclustered 
(
	ValueName ASC
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbValue', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains distinct values that will be used as values in the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbValue'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbValue', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbValue', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbValue', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbValue', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbValue', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbValue', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
