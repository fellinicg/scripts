use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentOfficeOffice') and parent_object_id = object_id(N'dbo.tbAgentOffice'))
alter table dbo.tbAgentOffice drop constraint fktbAgentOfficeOffice
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingOfficeOffice') and parent_object_id = object_id(N'dbo.tbListingOffice'))
alter table dbo.tbListingOffice drop constraint fktbListingOfficeOffice
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbOfficeContactOffice') and parent_object_id = object_id(N'dbo.tbOfficeContact'))
alter table dbo.tbOfficeContact drop constraint fktbOfficeContactOffice
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbOffice') and type in (N'U'))
drop table dbo.tbOffice
go
create table dbo.tbOffice(
	OfficeID int identity(1,1) not null,
	CompanyID int not null,
	OfficeCode varchar(10) not null,
	Detail varchar(50) not null,
	PostalAddressID int null,
	PhoneNumber varchar(20) null,
	Fax varchar(20) null,
	Email varchar(50) null,
	IsDeleted bit not null constraint dftbOfficeIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbOfficeCreateDate  default (getdate()),
	CreateID int not null constraint dftbOfficeCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbOfficeUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbOfficeUpdateID  default (0),
	EtlID int null,
 constraint pktbOffice primary key clustered 
(
	OfficeID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbOffice', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the base information about a Office.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbOffice'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbOffice', N'COLUMN', N'CompanyID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbCompany table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbOffice', @level2type=N'COLUMN',@level2name=N'CompanyID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbOffice', N'COLUMN', N'PostalAddressID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbPostalAddress table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbOffice', @level2type=N'COLUMN',@level2name=N'PostalAddressID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbOffice', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbOffice', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbOffice', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbOffice', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbOffice', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbOffice', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCompany') and type in (N'U'))
alter table dbo.tbOffice  with check add constraint fktbOfficeCompany 
foreign key(CompanyID) references dbo.tbCompany (CompanyID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPostalAddress') and type in (N'U'))
alter table dbo.tbOffice  with check add constraint fktbOfficePostalAddress 
foreign key(PostalAddressID) references dbo.tbPostalAddress (PostalAddressID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbOtbAgentOfficeffice') and type in (N'U'))
alter table dbo.tbAgentOffice with check add constraint fktbAgentOfficeOffice
foreign key(OfficeID) references dbo.tbOffice (OfficeID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingOffice') and type in (N'U'))
alter table dbo.tbListingOffice with check add constraint fktbListingOfficeOffice
foreign key(OfficeID) references dbo.tbOffice (OfficeID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbOfficeContact') and type in (N'U'))
alter table dbo.tbOfficeContact with check add constraint fktbOfficeContactOffice
foreign key(OfficeID) references dbo.tbOffice (OfficeID)
go