use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingMedia') and type in (N'U'))
drop table dbo.tbListingMedia
go
create table dbo.tbListingMedia(
	ListingMediaID int identity(1,1) not null,
	ListingID int not null,
	MediaID int not null,
	ListingMediaTypeID int not null,
	Detail varchar(100) null,
	AdditionalDetail varchar(60) null,
	SortOrder tinyint not null constraint dftbListingMediaSortOrder default 0,
	IsDeleted bit not null constraint dftbListingMediaIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbListingMediaCreateDate  default (getdate()),
	CreateID int not null constraint dftbListingMediaCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbListingMediaUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbListingMediaUpdateID  default (0),
	EtlID int null,
 constraint pktbListingMedia primary key clustered 
(
	ListingMediaID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingMedia', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the listing to media relationship.  A listing can have many types of media attahed to it.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingMedia'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingMedia', N'COLUMN', N'ListingID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbListing table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingMedia', @level2type=N'COLUMN',@level2name=N'ListingID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingMedia', N'COLUMN', N'MediaID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbMedia table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingMedia', @level2type=N'COLUMN',@level2name=N'MediaID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingMedia', N'COLUMN', N'ListingMediaTypeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingMedia', @level2type=N'COLUMN',@level2name=N'ListingMediaTypeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingMedia', N'COLUMN', N'Detail'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Detail description field.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingMedia', @level2type=N'COLUMN',@level2name=N'Detail'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingMedia', N'COLUMN', N'AdditionalDetail'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Additional detail description field.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingMedia', @level2type=N'COLUMN',@level2name=N'AdditionalDetail'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingMedia', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingMedia', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingMedia', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingMedia', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingMedia', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingMedia', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListing') and type in (N'U'))
alter table dbo.tbListingMedia with check add constraint fktbListingMediaListing 
foreign key(ListingID) references dbo.tbListing (ListingID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbMedia') and type in (N'U'))
alter table dbo.tbListingMedia with check add constraint fktbListingMediaMedia 
foreign key(MediaID) references dbo.tbMedia (MediaID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbListingMedia with check add constraint fktbListingMediaType
foreign key(ListingMediaTypeID) references dbo.tbAttributeValue (AttributeValueID)
go