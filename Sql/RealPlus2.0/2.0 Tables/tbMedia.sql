use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingMediaMedia') and parent_object_id = object_id(N'dbo.tbListingMedia')) 
alter table dbo.tbListingMedia drop constraint fktbListingMediaMedia
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbMedia') and type in (N'U'))
drop table dbo.tbMedia
go
create table dbo.tbMedia(
	MediaID int identity(1,1) not null,
	MediaTypeID int not null,
	Link varchar(500) not null,
	IsDeleted bit not null constraint dftbMediaIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbMediaCreateDate  default (getdate()),
	CreateID int not null constraint dftbMediaCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbMediaUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbMediaUpdateID  default (0),
 constraint pktbMedia primary key clustered 
(
	MediaID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbMedia', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the base information for media.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbMedia'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbMedia', N'COLUMN', N'MediaTypeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbMedia', @level2type=N'COLUMN',@level2name=N'MediaTypeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbMedia', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbMedia', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbMedia', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbMedia', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbMedia', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbMedia', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbMedia with check add constraint fktbMediaType 
foreign key(MediaTypeID) references dbo.tbAttributeValue (AttributeValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingMedia') and type in (N'U'))
alter table dbo.tbListingMedia with check add constraint fktbListingMediaMedia 
foreign key(MediaID) references dbo.tbMedia (MediaID) 
go