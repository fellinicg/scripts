use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbUserRoleRole') and parent_object_id = object_id(N'dbo.tbUserRole'))
alter table dbo.tbUserRole drop constraint fktbUserRoleRole
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbUserRole') and type in (N'U'))
drop table dbo.tbUserRole
go
create table dbo.tbUserRole(
	PersonID int not null,
	RoleID tinyint not null,
	IsDeleted bit not null constraint dftbUserRoleIsDeleted default 0,	
	CreateDateTime datetime2 not null constraint dftbUserRoleCreateDate  default (getdate()),
	CreateID int not null constraint dftbUserRoleCreateID  default (0),
	UpdateDateTime datetime2 not null constraint dftbUserRoleUpdateDateTime  default (getdate()),
	UpdateID int not null constraint dftbUserRoleUpdateID  default (0),
 constraint pktbUserRole primary key clustered 
(
	PersonID, RoleID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]
go
set ansi_padding off
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUserRole', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains the roles a person has to determine access control to various parts of the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUserRole'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUserRole', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUserRole', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUserRole', N'COLUMN', N'CreateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who created the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUserRole', @level2type=N'COLUMN',@level2name=N'CreateID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbUserRole', N'COLUMN', N'UpdateID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Reference to the tbPerson table for who last updated the record.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbUserRole', @level2type=N'COLUMN',@level2name=N'UpdateID'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPerson') and type in (N'U'))
alter table dbo.tbUserRole with check add constraint fktbUserRolePerson foreign key(PersonID)
references dbo.tbPerson (PersonID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbRole') and type in (N'U'))
alter table dbo.tbUserRole with check add constraint fktbUserRoleRole foreign key(RoleID)
references dbo.tbRole (RoleID)