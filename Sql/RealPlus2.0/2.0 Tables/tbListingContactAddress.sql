use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListingContactAddress') and type in (N'U'))
drop table dbo.tbListingContactAddress
go
create table dbo.tbListingContactAddress(
	ListingContactAddressID int identity(1,1) not null,
	ListingID int not null,
	ListingContactTypeID int not null,
	PersonAddressID int not null,
	IsDeleted bit constraint dftbListingContactAddressIsDeleted default 0,	
 constraint pktbListingContactAddress primary key clustered 
(
	ListingContactAddressID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off
go
if  exists (select * from sys.indexes where object_id = object_id(N'dbo.tbListingContactAddress') and name = N'ncixtbListingContactAddressListing')
drop index ncixtbListingContactAddressListing on dbo.tbListingContactAddress
create nonclustered index ncixtbListingContactAddressListing on dbo.tbListingContactAddress(ListingID asc)
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingContactAddress', null, null))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Contains listing address contact information.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingContactAddress'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingContactAddress', N'COLUMN', N'ListingID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbListing table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingContactAddress', @level2type=N'COLUMN',@level2name=N'ListingID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingContactAddress', N'COLUMN', N'ListingContactTypeID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbAttributeValue table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingContactAddress', @level2type=N'COLUMN',@level2name=N'ListingContactTypeID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingContactAddress', N'COLUMN', N'PersonAddressID'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Foreign key to the tbPersonAddress table.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingContactAddress', @level2type=N'COLUMN',@level2name=N'PersonAddressID'
go
if not exists (select * from ::fn_listextendedproperty(N'Description', N'SCHEMA', N'dbo', N'TABLE', N'tbListingContactAddress', N'COLUMN', N'IsDeleted'))
exec sys.sp_addextendedproperty @name=N'Description', @value=N'Flag to represent whether this record has been deleted from the system.'
, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbListingContactAddress', @level2type=N'COLUMN',@level2name=N'IsDeleted'
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbListing') and type in (N'U'))
alter table dbo.tbListingContactAddress with check add constraint fktbListingContactAddressListing foreign key(ListingID)
references dbo.tbListing (ListingID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAttributeValue') and type in (N'U'))
alter table dbo.tbListingContactAddress with check add constraint fktbListingContactAddressContactType foreign key(ListingContactTypeID)
references dbo.tbAttributeValue (AttributeValueID)
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPersonAddress') and type in (N'U'))
alter table dbo.tbListingContactAddress with check add constraint fktbListingContactAddressPerson foreign key(PersonAddressID)
references dbo.tbPersonAddress (PersonAddressID)
go