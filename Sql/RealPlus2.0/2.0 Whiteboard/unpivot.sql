select name, value
from (
	select 
		convert(varchar, listingid) listingid
		, convert(varchar, unitid) unitid
		, convert(varchar, listingnumber) listingnumber
		, convert(varchar, webid) webid
		, convert(varchar, saleorrental) saleorrental
		, convert(varchar, price) price
	from dbo.listings
	where listingid = 155) l
unpivot (value for name in (listingid, unitid, listingnumber, webid, saleorrental, price)) up
