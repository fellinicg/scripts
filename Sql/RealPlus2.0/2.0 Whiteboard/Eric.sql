--ListedDate/UpdateDate
select ListingID, ListDate, UpdateDateTime
from dbo.tbListing
where ListDate between '3/1/2016' and '4/1/2016'
	or UpdateDateTime between '3/1/2016' and '4/1/2016'


--Financing Allowed
select l.ListingID, pan.AttributeValue
from dbo.tbPropertyAttributeNumeric pan
	inner join dbo.tbProperty p on pan.PropertyID = isnull(p.ParentPropertyID, p.PropertyID)
	inner join dbo.tbListing l on p.PropertyID = l.PropertyID
where pan.AttributeID = 36
	and pan.AttributeValue between 50 and 75

--Tribeca
select l.ListingID, dbo.sfGetAttributeValue(pa.AttributeValueID)
from dbo.tbPropertyAttribute pa
	inner join dbo.tbProperty p on pa.PropertyID = isnull(p.ParentPropertyID, p.PropertyID)
	inner join dbo.tbListing l on p.PropertyID = l.PropertyID
where pa.AttributeValueID in (44)

--FloorNumber
select l.ListingID, lan.AttributeValue
from dbo.tbListingAttributeNumeric lan
	inner join dbo.tbListing l on lan.ListingID = l.ListingID
where lan.AttributeID = 111
	and lan.AttributeValue between 0 and 10

--Balcony, Roof Garden, Terrace
select l.ListingID, dbo.sfGetAttributeValue(pa.AttributeValueID)
from dbo.tbPropertyAttribute pa
	inner join dbo.tbProperty p on pa.PropertyID = p.PropertyID
	inner join dbo.tbListing l on p.PropertyID = l.PropertyID
where pa.AttributeValueID in (208,212,213)
