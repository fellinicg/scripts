select distinct
'use Jaguar_v2_Prototype'
+ char(10) +
'go'
+ char(10) +
'set ansi_nulls on'
+ char(10) +
'go'
+ char(10) +
'set quoted_identifier off'
+ char(10) +
'go'
+ char(10) +
'if  exists (select * from sys.views where object_id = OBJECT_ID(N'+ char(39) + 'dbo.vw' + AttributeName + char(39) + '))'
+ char(10) +
'drop view dbo.vw' + AttributeName
+ char(10) +
'go'
+ char(10) +
'-- ============================================================'
+ char(10) +
'-- OrigAuthorFullName: Todd Fellini'
+ char(10) +
'-- OrigCreateDate: 20160311'
+ char(10) +
'-- ============================================================'
+ char(10) +
'create view dbo.vw' + AttributeName
+ char(10) +
'as'
+ char(10) +
+ char(10) +
'	select a.AttributeValueID, b.AttributeName, c.ValueName, a.Display, a.IsDeleted, a.EtlID'
+ char(10) +
'	from dbo.tbAttributeValue a'
+ char(10) +
'		inner join dbo.tbAttribute b on a.AttributeID = b.AttributeID'
+ char(10) +
'		inner join dbo.tbValue c on a.ValueID = c.ValueID'
+ char(10) +
'	where b.AttributeName = ' + char(39) + AttributeName + char(39) + 'go' + char(39)
from dbo.vwAttributeValues
where attributename not in ('ApartmentType','BuildingAllow')