declare @AttributeID int = 104

select 'Property Text'
--,*
from dbo.tbPropertyAttributeText
where AttributeID = @AttributeID
union
select 'Property Numeric'
--, *
from dbo.tbPropertyAttributeNumeric
where AttributeID = @AttributeID
union
select 'Listing Numeric'
--, *
from dbo.tbListingAttributeNumeric
where AttributeID = @AttributeID
union
select 'Listing Text'
--, *
from dbo.tbListingAttributeText
where AttributeID = @AttributeID
union
select 'AttributeValues'
--,*
from dbo.vwAttributeValues
where AttributeID = @AttributeID

---------------------------

begin tran
update tbListingMedia set IsDeleted = 1
from tbListingMedia lm
inner join (
select 
	l.ListingID
	, al.ListingMediaID
	--, ListingMediaTypeID
	, row_number() over (partition by al.ListingID, al.ListingMediaTypeID order by ListingMediaID) as 'rowid'
from tbListing l
inner join dbo.tbListingMedia al on l.ListingID = al.ListingID
where l.isdeleted = 0) a on a.ListingMediaID = lm.ListingMediaID
where a.rowid > 4

--commit tran

------------------------

--begin tran
--update dbo.tbAgentListing set AgentID = rn
select *
from dbo.tbAgentListing a
inner join (
select al.AgentListingID, row_number() over ( order by AgentListingID) rn
from tbListing l
inner join dbo.tbAgentListing al on l.ListingID = al.ListingID
where l.listingcategoryid = 7
and l.isdeleted = 0) b on a.AgentListingID = b.AgentListingID
--order by l.listingid

--commit tran

---------------------------

select --* --x.ListingID, d.detailTypeid, av.Valuename, x.listitem
x.ListingID
, valuename 'CommentType'
, case isnull(a.ListingID, -1) when -1 then 'No' else 'Yes' end 'HasCommentType'
from 
(
select ListItem, ListingID
from dbo.tfCommaListToTable('3512,3513,3515,3516')
cross apply dbo.tbListing
where isdeleted = 0
) x
inner join vwattributevalues av on x.ListItem = av.attributevalueid
left join (
select distinct l.ListingID, d.detailTypeid--, av.Valuename --, x.listitem
from tbListing l 
inner join tblistingcomment lc on l.listingid = lc.listingid
inner join tbdetail d on lc.detailid = d.detailid
where l.isdeleted = 0) a on x.ListingID = a.ListingID and x.Listitem = a.detailtypeid

--left join tbdetail d on av.attributevalueid = d.detailid

--left join tblistingcomment lc on x.listingid = lc.listingid and 

--left join tbListing l   on l.listingid = x.listingid
--
--

--where l.isdeleted = 0
--group by l.ListingID, d.detailtypeid
--order by x.ListingID, d.detailtypeid

--------------------------------

select distinct l.ListingID, d.detailTypeid--, av.Valuename --, x.listitem
from tbListing l 
inner join tblistingcomment lc on l.listingid = lc.listingid
inner join tbdetail d on lc.detailid = d.detailid
--inner join vwattributevalues av on d.DetailTypeID = av.attributevalueid
where l.isdeleted = 0
--group by l.ListingID, d.detailtypeid
--order by l.ListingID, d.detailtypeid


----------------------------------

select *
from tbDetail
where detailtypeid in (
3512
,3513
,3515
,3516
)