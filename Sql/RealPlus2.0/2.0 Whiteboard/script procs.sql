--use Jaguar_v2_WiP

select
p.name,
'use Jaguar_v2_Prototype2_BHS' + char(13) + char(10) +
'go' + char(13) + char(10) +
'set ansi_nulls on' + char(13) + char(10) +
'go' + char(13) + char(10) +
'set quoted_identifier off' + char(13) + char(10) +
'go' + char(13) + char(10) +
'if  exists (select * from sys.objects where object_id = object_id(N' + char(39) + 'dbo.' + p.name + char(39) + ') and type in (N''P'', N''PC''))' + char(13) + char(10) +
'drop procedure dbo.' + p.name + char(13) + char(10) +
'go' + char(13) + char(10) +
--replace(
--replace(
--replace(
--replace(
--replace(
--replace(
--replace(
--replace(
--replace(
--replace(
--replace(
--replace(
--replace(
--replace(
--replace(
--replace(
--replace(
--replace(
--replace(
--replace(
--replace(
--replace(
--replace(
--replace(
replace(
replace(
replace(
	m.definition, '[dbo]', 'dbo')
	, '[', '')
	, ']', '')
--	--, 'create proc', 'create procedure')
--	, '20160410', '20160412')
--	, '--SELECT OBJECT_SCHEMA_NAME(object_id) as schema_name, * FROM sys.procedures order by modify_date desc', '')
--	, 'values', 'values')
--	, 'declare @ErrMsg varchar(4000)', '-- Initialize variables')
--	, 'set @ErrMsg = ', 'raiserror(')
--	, '...  '' + char(13) + char(10) +', ''', 16, 1)')
--	, 'ISNULL(ERROR_MESSAGE(),'''') ' + char(13) + char(10), '')
--	, 'raiserror(@ErrMsg, 16, 1)' + char(13) + char(10), '')
----	, 'set nocount on' + char(13) + char(10) + char(13) + char(10), '')
--	, 'set nocount on' + char(13) + char(10), '')
--	, '-- Initialize settings', '-- Initialize settings' + char(13) + char(10) + '	set nocount on' + char(13) + char(10))
--	, '		begin try', '	begin try')
--	, ',', ', ')
--	, '	, @CreateDateTime datetime2' + char(13) + char(10), '')
--	, '	, @CreateID int' + char(13) + char(10), '')
--	, '			, CreateDateTime = @CreateDateTime' + char(13) + char(10), '')
--	, '			, CreateID = @CreateID' + char(13) + char(10), '')
--	, '	, @UpdateDateTime datetime2' + char(13) + char(10), '')
--	, '= @UpdateDateTime', '= getdate()')
--	--, '	, @UpdateID int' + char(13) + char(10), '')
--	--, ', @UpdateDateTime, @UpdateID', ', @CreateDateTime, @CreateID')
--	--, '	, @IsDeleted bit' + char(13) + char(10), '')
--	--, ', @IsDeleted', '')
--	--, ', IsDeleted', '')
--	, '	, @EtlID int' + char(13) + char(10), '')
--	, '			, EtlID = @EtlID' + char(13) + char(10), '')
--	, ',  @Result', ', @Result')
--	, '			where', '		where')
--	, 'IsDeleted = @IsDeleted', 'IsDeleted = isnull(@IsDeleted, IsDeleted)')
--	--, ', @EtlID', '')
--	--, ', EtlID', '')
+ char(13) + char(10) +
'go' + char(13) + char(10) +
'grant exec on dbo.' + p.name + ' to web'
from sys.procedures p
inner join sys.sql_modules m on p.object_id = m.object_id
--where OBJECT_SCHEMA_NAME(p.object_id) = 'dbo'
	--and charindex('upUpdate', p.name) = 1
order by p.name
