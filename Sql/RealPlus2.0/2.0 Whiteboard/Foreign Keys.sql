----drop foreign keys
----
--select
--'if  exists (select * from sys.foreign_keys where object_id = object_id(N' + char(39) +
--a.TABLE_SCHEMA + '.' + a.CONSTRAINT_NAME + char(39) + ') and parent_object_id = object_id(N' + char(39) + a.TABLE_SCHEMA + '.' + a.TABLE_NAME + char(39) +
--')) alter table ' + a.TABLE_SCHEMA + '.' + a.TABLE_NAME + ' drop constraint ' + a.CONSTRAINT_NAME
--from INFORMATION_SCHEMA.KEY_COLUMN_USAGE a
--inner join INFORMATION_SCHEMA.TABLE_CONSTRAINTS b on a.CONSTRAINT_NAME = b.CONSTRAINT_NAME
--where b.CONSTRAINT_TYPE = 'FOREIGN KEY' and a.COLUMN_NAME = 'postaladdressid'

use Jaguar_v2_WiP

-- Create foreign keys
--
select 
'if  exists (select * from sys.foreign_keys where object_id = object_id(N' + char(39) + a.CONSTRAINT_SCHEMA + '.' + a.CONSTRAINT_NAME + char(39) + ') and parent_object_id = object_id(N'+ char(39) + b.TABLE_SCHEMA + '.' + b.TABLE_NAME + char(39) + '))' + char(10) + 'alter table ' + b.TABLE_SCHEMA + '.' + b.TABLE_NAME + ' drop constraint ' + a.CONSTRAINT_NAME + char(10) + 'go'
,'alter table ' + a.CONSTRAINT_SCHEMA + '.' + b.TABLE_NAME + ' with check add constraint ' + a.CONSTRAINT_NAME + char(10) + 'foreign key(' + b.COLUMN_NAME + ') references ' + c.TABLE_SCHEMA + '.' + c.TABLE_NAME + ' (' + c.COLUMN_NAME + ')' + char(10) + 'go'
from INFORMATION_SCHEMA.TABLE_CONSTRAINTS f
inner join INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS a on f.CONSTRAINT_NAME = a.CONSTRAINT_NAME
inner join INFORMATION_SCHEMA.KEY_COLUMN_USAGE b on a.CONSTRAINT_NAME = b.CONSTRAINT_NAME
inner join INFORMATION_SCHEMA.KEY_COLUMN_USAGE c on a.UNIQUE_CONSTRAINT_NAME = c.CONSTRAINT_NAME
where f.CONSTRAINT_TYPE = 'FOREIGN KEY' 
and c.TABLE_NAME = 'tbmedia'
