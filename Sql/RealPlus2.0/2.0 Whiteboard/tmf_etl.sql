truncate table tmf.dbo.listings
truncate table tmf.dbo.properties
truncate table tmf.dbo.units
truncate table tmf.dbo.tbProperty
truncate table tmf.dbo.tbPropertyAttribute
truncate table tmf.dbo.tbPropertyAttributeNumeric
truncate table tmf.dbo.tbPropertyAttributeText
truncate table tmf.dbo.tbListing
truncate table tmf.dbo.tbListingAttributeNumeric
truncate table tmf.dbo.tbListingAttributeText
truncate table tmf.dbo.tbListingStatus
truncate table tmf.dbo.tbListingPrice

insert into tmf.dbo.properties
select 
	--top 1
	src.*
from Jaguar_v2_ETL.dbo.properties src --tmf.etl.properties src --Jaguar_v2_ETL.dbo.properties src
left join tmf.dbo.properties dest on src.propertyid = dest.propertyid
where dest.propertyid is null

insert into tmf.dbo.units
select 
	--top 1
	src.*
from Jaguar_v2_ETL.dbo.units src --tmf.etl.properties src --Jaguar_v2_ETL.dbo.properties src
left join tmf.dbo.units dest on src.unitid = dest.unitid
where dest.unitid is null

insert into dbo.tbListingPrice (ListingID, ListingPrice, PriceDateTime)
select listingid, price, transaction_date
from PriceChange
order by listingid, transaction_date

insert into tmf.dbo.listings
select 
	top 1
	src.*
from Jaguar_v2_ETL.dbo.listings src --tmf.etl.properties src --Jaguar_v2_ETL.dbo.properties src
left join tmf.dbo.listings dest on src.listingid = dest.listingid
where dest.listingid is null --and src.listingid not in (10076, 10077, 100)

select count(1) from tmf.dbo.properties
select count(1) from tmf.dbo.tbProperty
select * from tmf.dbo.tbPropertyAttribute 
select * from tmf.dbo.tbPropertyAttributeNumeric
select * from tmf.dbo.tbPropertyAttributeText


insert into dbo.tbListingStatus (ListingID, StatusID, StatusDateTime)
select
	l.ListingID
	, ls.status
	, ls.modifieddate
from dbo.tbListing l
	inner join dbo.listings ls on l.ListingID = ls.listingid
where ls.status is not null