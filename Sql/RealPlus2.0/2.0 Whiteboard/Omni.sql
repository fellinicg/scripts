declare @omni varchar(max) = 'main'

	select FullName
	from dbo.tbPerson
	where charindex(@Omni, FullName) > 0
	union
	select FirstGivenName
	from dbo.tbPerson
	where charindex(@Omni, FirstGivenName) > 0
	union
	select FamilyName
	from dbo.tbPerson
	where charindex(@Omni, FamilyName) > 0
	union
	select SecondGivenName
	from dbo.tbPerson
	where charindex(@Omni, SecondGivenName) > 0
	union
	select EmailAddress
	from dbo.tbPersonEmail
	where charindex(@Omni, EmailAddress) > 0
	union
	select CompanyName
	from dbo.tbCompany
	where charindex(@Omni, CompanyName) > 0
	union
	select OfficeCode
	from dbo.tbOffice
	where charindex(@Omni, OfficeCode) > 0
	union
	select Detail
	from dbo.tbOffice
	where charindex(@Omni, Detail) > 0
	union
	select Line1
	from dbo.tbPostalAddress
	where charindex(@Omni, Line1) > 0
	union
	select Line2
	from dbo.tbPostalAddress
	where charindex(@Omni, Line2) > 0
	union
	select Line3
	from dbo.tbPostalAddress
	where charindex(@Omni, Line3) > 0
	union
	select Line4
	from dbo.tbPostalAddress
	where charindex(@Omni, Line4) > 0
	union
	select CityTown
	from dbo.tbPostalAddress
	where charindex(@Omni, CityTown) > 0
	union
	select StateProvince
	from dbo.tbPostalAddress
	where charindex(@Omni, StateProvince) > 0
	union
	select PostalCode
	from dbo.tbPostalAddress
	where charindex(@Omni, PostalCode) > 0
