truncate table Jaguar_v2_ETL.dbo.properties
truncate table Jaguar_v2_ETL.dbo.tbProperty
truncate table Jaguar_v2_ETL.dbo.tbPropertyAttribute
truncate table Jaguar_v2_ETL.dbo.tbPropertyAttributeNumeric
truncate table Jaguar_v2_ETL.dbo.tbPropertyAttributeText

--insert into Jaguar_v2_ETL.dbo.properties
--select 
--	--top 1
--	src.*
--from Jaguar_v2_ETL.bb.properties src
--left join Jaguar_v2_ETL.dbo.properties dest on src.propertyid = dest.propertyid
--where dest.propertyid is null

insert into Jaguar_v2_ETL.dbo.units
select 
	top 1
	src.*
from Jaguar_v2_ETL.dbo.units src
left join Jaguar_v2_ETL.dbo.units dest on src.unitid = dest.unitid
where dest.unitid is null