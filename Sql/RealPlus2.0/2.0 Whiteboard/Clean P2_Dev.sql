truncate table addresses
truncate table big_text
truncate table contacts
truncate table listings
truncate table media
truncate table properties
truncate table units
truncate table dbo.tbAgentListingPin
truncate table dbo.tbAgentListingPinAction
truncate table dbo.tbAgentWorkInProgress
truncate table dbo.tbAgentSearchResultSetting
truncate table dbo.tbAgentOffice
truncate table tbListingAttribute
truncate table tbListingAttributeNumeric
truncate table tbListingAttributeText
truncate table tbListingComment
truncate table tbListingMedia
truncate table tbListingOpenHouse
truncate table tbListingPrice
truncate table tbListingStatus
truncate table tbListingActivity
truncate table tbPropertyAttribute
truncate table tbPropertyAttributeNumeric
truncate table tbPropertyAttributeText
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentListingListing') and parent_object_id = object_id(N'dbo.tbAgentListing'))
alter table dbo.tbAgentListing drop constraint fktbAgentListingListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingAttributeListing') and parent_object_id = object_id(N'dbo.tbListingAttribute'))
alter table dbo.tbListingAttribute drop constraint fktbListingAttributeListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingAttributeNumericListing') and parent_object_id = object_id(N'dbo.tbListingAttributeNumeric'))
alter table dbo.tbListingAttributeNumeric drop constraint fktbListingAttributeNumericListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingAttributeTextListing') and parent_object_id = object_id(N'dbo.tbListingAttributeText'))
alter table dbo.tbListingAttributeText drop constraint fktbListingAttributeTextListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingCommentListing') and parent_object_id = object_id(N'dbo.tbListingComment'))
alter table dbo.tbListingComment drop constraint fktbListingCommentListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingContactAddressListing') and parent_object_id = object_id(N'dbo.tbListingContactAddress'))
alter table dbo.tbListingContactAddress drop constraint fktbListingContactAddressListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingContactEmailListing') and parent_object_id = object_id(N'dbo.tbListingContactEmail'))
alter table dbo.tbListingContactEmail drop constraint fktbListingContactEmailListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingContactPhoneListing') and parent_object_id = object_id(N'dbo.tbListingContactPhone'))
alter table dbo.tbListingContactPhone drop constraint fktbListingContactPhoneListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingOfficeListing') and parent_object_id = object_id(N'dbo.tbListingOffice'))
alter table dbo.tbListingOffice drop constraint fktbListingOfficeListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingStatusListing') and parent_object_id = object_id(N'dbo.tbListingStatus'))
alter table dbo.tbListingStatus drop constraint fktbListingStatusListing
go
truncate table tbListing
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingPropertyID') and parent_object_id = object_id(N'dbo.tbListing'))
alter table dbo.tbListing drop constraint fktbListingPropertyID
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyAddressProperty') and parent_object_id = object_id(N'dbo.tbPropertyAddress'))
alter table dbo.tbPropertyAddress drop constraint fktbPropertyAddressProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyAliasProperty') and parent_object_id = object_id(N'dbo.tbPropertyAlias'))
alter table dbo.tbPropertyAlias drop constraint fktbPropertyAliasProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyAttributeNumericProperty') and parent_object_id = object_id(N'dbo.tbPropertyAttributeNumeric'))
alter table dbo.tbPropertyAttributeNumeric drop constraint fktbPropertyAttributeNumericProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyAttributeProperty') and parent_object_id = object_id(N'dbo.tbPropertyAttribute'))
alter table dbo.tbPropertyAttribute drop constraint fktbPropertyAttributeProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyAttributeTextProperty') and parent_object_id = object_id(N'dbo.tbPropertyAttributeText'))
alter table dbo.tbPropertyAttributeText drop constraint fktbPropertyAttributeTextProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyCommentProperty') and parent_object_id = object_id(N'dbo.tbPropertyComment'))
alter table dbo.tbPropertyComment drop constraint fktbPropertyCommentProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyContactAddressProperty') and parent_object_id = object_id(N'dbo.tbPropertyContactAddress'))
alter table dbo.tbPropertyContactAddress drop constraint fktbPropertyContactAddressProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyContactEmailProperty') and parent_object_id = object_id(N'dbo.tbPropertyContactEmail'))
alter table dbo.tbPropertyContactEmail drop constraint fktbPropertyContactEmailProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyContactPhoneProperty') and parent_object_id = object_id(N'dbo.tbPropertyContactPhone'))
alter table dbo.tbPropertyContactPhone drop constraint fktbPropertyContactPhoneProperty
go
truncate table tbProperty
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingCommentDetail') and parent_object_id = object_id(N'dbo.tbListingComment'))
alter table dbo.tbListingComment drop constraint fktbListingCommentDetail
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbPropertyCommentDetail') and parent_object_id = object_id(N'dbo.tbPropertyComment'))
alter table dbo.tbPropertyComment drop constraint fktbPropertyCommentDetail
go
truncate table tbDetail
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingMediaMedia') and parent_object_id = object_id(N'dbo.tbListingMedia'))
alter table dbo.tbListingMedia drop constraint fktbListingMediaMedia
go
truncate table tbMedia
go
alter table dbo.tbAgentListing with check add constraint fktbAgentListingListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListingAttribute with check add constraint fktbListingAttributeListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListingAttributeNumeric with check add constraint fktbListingAttributeNumericListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListingAttributeText with check add constraint fktbListingAttributeTextListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListingComment with check add constraint fktbListingCommentListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListingContactAddress with check add constraint fktbListingContactAddressListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListingContactEmail with check add constraint fktbListingContactEmailListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListingContactPhone with check add constraint fktbListingContactPhoneListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListingOffice with check add constraint fktbListingOfficeListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListingStatus with check add constraint fktbListingStatusListing
foreign key(ListingID) references dbo.tbListing (ListingID)
go
alter table dbo.tbListing with check add constraint fktbListingPropertyID
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
alter table dbo.tbPropertyAddress with check add constraint fktbPropertyAddressProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
alter table dbo.tbPropertyAlias with check add constraint fktbPropertyAliasProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
alter table dbo.tbPropertyAttributeNumeric with check add constraint fktbPropertyAttributeNumericProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
alter table dbo.tbPropertyAttribute with check add constraint fktbPropertyAttributeProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
alter table dbo.tbPropertyAttributeText with check add constraint fktbPropertyAttributeTextProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
alter table dbo.tbPropertyComment with check add constraint fktbPropertyCommentProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
alter table dbo.tbPropertyContactAddress with check add constraint fktbPropertyContactAddressProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
alter table dbo.tbPropertyContactEmail with check add constraint fktbPropertyContactEmailProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
alter table dbo.tbPropertyContactPhone with check add constraint fktbPropertyContactPhoneProperty
foreign key(PropertyID) references dbo.tbProperty (PropertyID)
go
alter table dbo.tbListingComment with check add constraint fktbListingCommentDetail
foreign key(DetailID) references dbo.tbDetail (DetailID)
go
alter table dbo.tbPropertyComment with check add constraint fktbPropertyCommentDetail
foreign key(DetailID) references dbo.tbDetail (DetailID)
go
alter table dbo.tbListingMedia with check add constraint fktbListingMediaMedia
foreign key(MediaID) references dbo.tbMedia (MediaID)
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentWorkInProgressCriteria') and parent_object_id = object_id(N'dbo.tbAgentWorkInProgress'))
alter table dbo.tbAgentWorkInProgress drop constraint fktbAgentWorkInProgressCriteria
go
truncate table dbo.tbAgentWorkInProgress
go
alter table dbo.tbAgentWorkInProgress with check add constraint fktbAgentWorkInProgressCriteria
foreign key(AgentSavedCriteriaID) references dbo.tbAgentSavedCriteria (AgentSavedCriteriaID)
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentWorkInProgressCriteria') and parent_object_id = object_id(N'dbo.tbAgentWorkInProgress'))
alter table dbo.tbAgentWorkInProgress drop constraint fktbAgentWorkInProgressCriteria
go	
truncate table dbo.tbAgentSavedCriteria
go	
alter table dbo.tbAgentWorkInProgress with check add constraint fktbAgentWorkInProgressCriteria
foreign key(AgentSavedCriteriaID) references dbo.tbAgentSavedCriteria (AgentSavedCriteriaID)
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentSavedCriteriaCustomer') and parent_object_id = object_id(N'dbo.tbAgentSavedCriteria'))
alter table dbo.tbAgentSavedCriteria drop constraint fktbAgentSavedCriteriaCustomer
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentWorkInProgressCustomer') and parent_object_id = object_id(N'dbo.tbAgentWorkInProgress'))
alter table dbo.tbAgentWorkInProgress drop constraint fktbAgentWorkInProgressCustomer
go
truncate table dbo.tbCustomer
go
alter table dbo.tbAgentSavedCriteria with check add constraint fktbAgentSavedCriteriaCustomer
foreign key(CustomerID) references dbo.tbCustomer (CustomerID)
go
alter table dbo.tbAgentWorkInProgress with check add constraint fktbAgentWorkInProgressCustomer
foreign key(CustomerID) references dbo.tbCustomer (CustomerID)
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentAgentStatusAgent') and parent_object_id = object_id(N'dbo.tbAgent'))
alter table dbo.tbAgent drop constraint fktbAgentAgentStatusAgent
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentListingAgent') and parent_object_id = object_id(N'dbo.tbAgentListing'))
alter table dbo.tbAgentListing drop constraint fktbAgentListingAgent
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentOfficeAgent') and parent_object_id = object_id(N'dbo.tbAgentOffice'))
alter table dbo.tbAgentOffice drop constraint fktbAgentOfficeAgent
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentSavedCriteriaAgent') and parent_object_id = object_id(N'dbo.tbAgentSavedCriteria'))
alter table dbo.tbAgentSavedCriteria drop constraint fktbAgentSavedCriteriaAgent
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentStatusAgent') and parent_object_id = object_id(N'dbo.tbAgentStatus'))
alter table dbo.tbAgentStatus drop constraint fktbAgentStatusAgent
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentWorkInProgressAgent') and parent_object_id = object_id(N'dbo.tbAgentWorkInProgress'))
alter table dbo.tbAgentWorkInProgress drop constraint fktbAgentWorkInProgressAgent
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbCustomerAgent') and parent_object_id = object_id(N'dbo.tbCustomer'))
alter table dbo.tbCustomer drop constraint fktbCustomerAgent
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingCreditAgent') and parent_object_id = object_id(N'dbo.tbListingCredit'))
alter table dbo.tbListingCredit drop constraint fktbListingCreditAgent
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbSharedCustomerAgent') and parent_object_id = object_id(N'dbo.tbSharedCustomer'))
alter table dbo.tbSharedCustomer drop constraint fktbSharedCustomerAgent
go
truncate table dbo.tbAgent
go
alter table dbo.tbAgent with check add constraint fktbAgentAgentStatusAgent
foreign key(AgentID) references dbo.tbAgent (AgentID)
go
alter table dbo.tbAgentListing with check add constraint fktbAgentListingAgent
foreign key(AgentID) references dbo.tbAgent (AgentID)
go
alter table dbo.tbAgentOffice with check add constraint fktbAgentOfficeAgent
foreign key(AgentID) references dbo.tbAgent (AgentID)
go
alter table dbo.tbAgentSavedCriteria with check add constraint fktbAgentSavedCriteriaAgent
foreign key(AgentID) references dbo.tbAgent (AgentID)
go
alter table dbo.tbAgentStatus with check add constraint fktbAgentStatusAgent
foreign key(AgentID) references dbo.tbAgent (AgentID)
go
alter table dbo.tbAgentWorkInProgress with check add constraint fktbAgentWorkInProgressAgent
foreign key(AgentID) references dbo.tbAgent (AgentID)
go
alter table dbo.tbCustomer with check add constraint fktbCustomerAgent
foreign key(AgentID) references dbo.tbAgent (AgentID)
go
alter table dbo.tbListingCredit with check add constraint fktbListingCreditAgent
foreign key(AgentID) references dbo.tbAgent (AgentID)
go
alter table dbo.tbSharedCustomer with check add constraint fktbSharedCustomerAgent
foreign key(AgentID) references dbo.tbAgent (AgentID)
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbAgentOfficeOffice') and parent_object_id = object_id(N'dbo.tbAgentOffice'))
alter table dbo.tbAgentOffice drop constraint fktbAgentOfficeOffice
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbListingOfficeOffice') and parent_object_id = object_id(N'dbo.tbListingOffice'))
alter table dbo.tbListingOffice drop constraint fktbListingOfficeOffice
go
if  exists (select * from sys.foreign_keys where object_id = object_id(N'dbo.fktbOfficeContactOffice') and parent_object_id = object_id(N'dbo.tbOfficeContact'))
alter table dbo.tbOfficeContact drop constraint fktbOfficeContactOffice
go
truncate table dbo.tbOffice
go
alter table dbo.tbAgentOffice with check add constraint fktbAgentOfficeOffice
foreign key(OfficeID) references dbo.tbOffice (OfficeID)
go
alter table dbo.tbListingOffice with check add constraint fktbListingOfficeOffice
foreign key(OfficeID) references dbo.tbOffice (OfficeID)
go
alter table dbo.tbOfficeContact with check add constraint fktbOfficeContactOffice
foreign key(OfficeID) references dbo.tbOffice (OfficeID)
go
