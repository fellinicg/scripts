use Jaguar_v1_Production
go

declare @tbl varchar(100) = 'listings'

select 'select ' + char(39) + 'NULLS' + char(39) + ' Field',  0 colid
union
select ', sum(case when ' + c.name + ' is null then 1 else 0 end) ' + char(39) + c.name + (case c.is_nullable when 0 then ' {REQUIRED}'  else '' end) + char(39) col, c.column_id
from sys.tables t
inner join sys.columns c on t.object_id = c.object_id
where t.name = @tbl
	--and c.is_nullable = 1
union
select ', count(1) TotalRecords', 999
union
select ', ' + char(39) + char(39) + ' ' + @tbl, 10000
union
select 'from dbo.' + @tbl, 100000
order by colid
