use Jaguar_v2_WiP

if object_id('tempdb..#type') is null 
begin
create table #type (typeval varchar(200))
insert into #type (typeval) values (null)
end

--************************
-- Yes/No/Unknown
--************************
insert into dbo.tbAttribute(AttributeName)
select LongName
from Jaguar_v2_Development.dbo.attrPetPolicy
left join dbo.tbAttribute on LongName = AttributeName
where Active = 1
	and AttributeID is null
order by LongName

--************************
-- ListingCategory
--************************
update #type set typeval = 'ListingCategory'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select ListItem
from dbo.tfCommaListToTable('Sale,Rental')
left join dbo.tbAttribute on ListItem = AttributeName
where AttributeID is null
order by ListItem

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from dbo.tfCommaListToTable('Sale,Rental')
inner join dbo.tbAttribute a on ListItem = AttributeName
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- Borough
--************************
update #type set typeval = 'Borough'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select LongName
from Jaguar_v2_Development.dbo.attrBoroughs
left join dbo.tbAttribute on LongName = AttributeName
where Active = 1
	and AttributeID is null
order by LongName

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from Jaguar_v2_Development.dbo.attrBoroughs
inner join dbo.tbAttribute a on LongName = AttributeName and Active = 1
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- Neighborhood
--************************
update #type set typeval = 'Neighborhood'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select LongName
from Jaguar_v2_Development.dbo.attrNeighborhoods
left join dbo.tbAttribute on LongName = AttributeName
where Active = 1
	and AttributeID is null
order by LongName

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from Jaguar_v2_Development.dbo.attrNeighborhoods
inner join dbo.tbAttribute a on LongName = AttributeName and Active = 1
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- BuildingType
--************************
update #type set typeval = 'BuildingType'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select LongName
from Jaguar_v2_Development.dbo.attrBuildingTypes
left join dbo.tbAttribute on LongName = AttributeName
where Active = 1
	and charindex('/', LongName) = 0
	and AttributeID is null
order by LongName

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from Jaguar_v2_Development.dbo.attrBuildingTypes
inner join dbo.tbAttribute a on LongName = AttributeName and Active = 1
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- BuildingEra
--************************

update #type set typeval = 'BuildingEra'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select LongName
from Jaguar_v2_Development.dbo.attrWarTypes
left join dbo.tbAttribute on LongName = AttributeName
where Active = 1
	and AttributeID is null

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from Jaguar_v2_Development.dbo.attrWarTypes
inner join dbo.tbAttribute a on LongName = AttributeName and Active = 1
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- BuildingPersonnel
--************************

update #type set typeval = 'BuildingPersonnel'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select LongName
from Jaguar_v2_Development.dbo.attrBuildingPersonnelTypes
left join dbo.tbAttribute on LongName = AttributeName
where Active = 1
	and AttributeID is null
order by LongName

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from Jaguar_v2_Development.dbo.attrBuildingPersonnelTypes
inner join dbo.tbAttribute a on LongName = AttributeName and Active = 1
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- BuildingNotAllow
--************************

update #type set typeval = 'BuildingNotAllow'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select LongName
from Jaguar_v2_Development.dbo.attrBuildingNotAllows
left join dbo.tbAttribute on LongName = AttributeName
where Active = 1
	and AttributeID is null
order by LongName

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from Jaguar_v2_Development.dbo.attrBuildingNotAllows
inner join dbo.tbAttribute a on LongName = AttributeName and Active = 1
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- BuildingAllow
--************************

update #type set typeval = 'BuildingAllow'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select LongName
from Jaguar_v2_Development.dbo.attrBuildingAllows
left join dbo.tbAttribute on LongName = AttributeName
where Active = 1
	and AttributeID is null
order by LongName

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from Jaguar_v2_Development.dbo.attrBuildingAllows
inner join dbo.tbAttribute a on LongName = AttributeName and Active = 1
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- BuildingAmenity
--************************

update #type set typeval = 'BuildingAmenity'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select LongName
from Jaguar_v2_Development.dbo.attrBuildingFeatures
left join dbo.tbAttribute on LongName = AttributeName
where Active = 1
	and AttributeID is null
order by LongName

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from Jaguar_v2_Development.dbo.attrBuildingFeatures
inner join dbo.tbAttribute a on LongName = AttributeName and Active = 1
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- OwnershipType
--************************

update #type set typeval = 'OwnershipType'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select ListItem
from dbo.tfCommaListToTable('Cooperative,Condo,Condop,Rental,Fee Simple')
left join dbo.tbAttribute on ListItem = AttributeName
where AttributeID is null
order by ListItem

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from dbo.tfCommaListToTable('Cooperative,Condo,Condop,Rental,Fee Simple')
inner join dbo.tbAttribute a on ListItem = AttributeName
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- SaleStatus
--************************

update #type set typeval = 'SaleStatus'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select LongName
from Jaguar_v2_Development.dbo.attrSaleStatuses
left join dbo.tbAttribute on LongName = AttributeName
where Active = 1
	and AttributeID is null
order by LongName

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from Jaguar_v2_Development.dbo.attrSaleStatuses
inner join dbo.tbAttribute a on LongName = AttributeName and Active = 1
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- RentalStatus
--************************

update #type set typeval = 'RentalStatus'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select LongName
from Jaguar_v2_Development.dbo.attrRentalStatuses
left join dbo.tbAttribute on LongName = AttributeName
where Active = 1
	and AttributeID is null
order by LongName

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from Jaguar_v2_Development.dbo.attrRentalStatuses
inner join dbo.tbAttribute a on LongName = AttributeName and Active = 1
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- Exposure
--************************

update #type set typeval = 'Exposure'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select LongName
from Jaguar_v2_Development.dbo.attrHomeExposureTypes
left join dbo.tbAttribute on LongName = AttributeName
where Active = 1
	and AttributeID is null
order by LongName

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from Jaguar_v2_Development.dbo.attrHomeExposureTypes
inner join dbo.tbAttribute a on LongName = AttributeName and Active = 1
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- OutdoorSpace
--************************

update #type set typeval = 'OutdoorSpace'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select LongName
from Jaguar_v2_Development.dbo.attrHomeOutdoorSpaceTypes
left join dbo.tbAttribute on LongName = AttributeName
where Active = 1
	and AttributeID is null
order by LongName

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from Jaguar_v2_Development.dbo.attrHomeOutdoorSpaceTypes
inner join dbo.tbAttribute a on LongName = AttributeName and Active = 1
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- Views
--************************

update #type set typeval = 'Views'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select LongName
from Jaguar_v2_Development.dbo.attrListingViews
left join dbo.tbAttribute on LongName = AttributeName
where Active = 1
	and AttributeID is null
order by LongName

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from Jaguar_v2_Development.dbo.attrListingViews
inner join dbo.tbAttribute a on LongName = AttributeName and Active = 1
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- FirePlace
--************************

--update #type set typeval = 'FirePlace'

--insert into dbo.tbType(TypeName)
--select typeval
--from #type a
--left join dbo.tbType b on a.typeval = b.TypeName
--where b.TypeID is null

--insert into dbo.tbAttribute(AttributeName)
--select ListItem
--from dbo.tfCommaListToTable('Decorative,Gas,Wood')
--left join dbo.tbAttribute on ListItem = AttributeName
--where AttributeID is null
--order by ListItem

--insert into dbo.tbAttributeType(AttributeID, TypeID)
--select a.AttributeID, b.TypeID
--from dbo.tfCommaListToTable('Decorative,Gas,Wood')
--inner join dbo.tbAttribute a on ListItem = AttributeName
--inner join dbo.tbType b on TypeName = (select typeval from #type)
--left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
--where c.AttributeTypeID is null

--************************
-- Facade
--************************

update #type set typeval = 'Facade'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select LongName
from Jaguar_v2_Development.dbo.attrExteriorMaterialTypes
left join dbo.tbAttribute on LongName = AttributeName
where Active = 1
	and AttributeID is null
order by LongName

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from Jaguar_v2_Development.dbo.attrExteriorMaterialTypes
inner join dbo.tbAttribute a on LongName = AttributeName and Active = 1
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- PostalAddresses from Listings
--************************

declare @address table (BuildingID int, Line1 varchar(80), CityTown varchar(40), StateProvince varchar(40), PostalCode varchar(40))

insert into @address
select 
	distinct b.BuildingID, c.FullAddress, c.City, c.STate, c.Zip
from Jaguar_v2_Development.dbo.Listings a
	left join Jaguar_v2_Development.dbo.Buildings b on a.BuildingID = b.BuildingID
	left join Jaguar_v2_Development.dbo.Addresses c on b.BuildingID = c.BuildingID
where 
	ListedDate >= '1/1/2015' and b.Active = 1

insert into dbo.tbPostalAddress(Line1, CityTown, StateProvince, PostalCode)
select distinct Line1, CityTown, StateProvince, PostalCode
from (
	select BuildingID, Line1, CityTown, StateProvince, PostalCode, count(BuildingID) over (partition by BuildingID) CT
	from @address) a
where CT = 1 and len(ltrim(rtrim(isnull(Line1, '')))) > 0

--select BuildingID, Line1, CityTown, StateProvince, PostalCode--, count(*) over (partition by BuildingID)  as CT
--from @address
----where count(*) over (partition by BuildingID) = 1
--qualify count(*) over (partition by BuildingID) = 1

--************************
-- Listings
--************************

insert into dbo.tbListing (ListingIDv15, ListingCategoryID, PostalAddressID)
select 
	a.ListingID
	, case when e.ListingID is null then 770 else 771 end
	, d.PostalAddressID
from Jaguar_v2_Development.dbo.Listings a
	inner join Jaguar_v2_Development.dbo.Buildings b on a.BuildingID = b.BuildingID
	inner join Jaguar_v2_Development.dbo.Addresses c on b.BuildingID = c.BuildingID
	left join dbo.tbPostalAddress d on c.FullAddress = d.Line1 and isnull(c.City, '') = isnull(d.CityTown, '') and isnull(c.STate, '') = isnull(d.StateProvince, '') and isnull(c.Zip, '') = isnull(d.PostalCode, '')
	left join Jaguar_v2_Development.dbo.Sales e on a.ListingID = e.ListingID
where 
	a.ListedDate >= '1/1/2015' 
	and b.Active = 1 
	and c.FullAddress is not null
	and d.PostalAddressID is not null

update dbo.tbListing set ListDateTime=b.ListedDate
from dbo.tbListing a
inner join Jaguar_v2_Development.dbo.Listings b on a.ListingIDv15 = b.ListingID
where b.ListedDate is not null

--************************
-- Buildings
--************************
insert into dbo.tbBuilding (BuildingType, OwnershipType, PostalAddressID, YearBuilt, Units, Floors, FinancingAllowed, NewDevelopment)
select 
	distinct 
	case 
		when c.BuildingTypeID in (2) then 3 
		when c.BuildingTypeID in (4, 8) then 5 
		when c.BuildingTypeID in (0) then 1 
		when c.BuildingTypeID in (5) then 6 
		when c.BuildingTypeID in (6) then 7 
		when c.BuildingTypeID in (1, 10, 7) then 2
		when c.BuildingTypeID in (3, 9) then 4 
		else null 
	end 'BuildingTypeID'
	, case 
		when c.OwnershipTypeID in (2) then 40 
		when c.OwnershipTypeID in (7) then 41 
		when c.OwnershipTypeID in (3) then 42 
		when c.OwnershipTypeID in (4, 10) then 43
		when c.OwnershipTypeID in (6, 8, 11, 12, 13, 14, 15) then 44
		else null 
	end 'OwnershipTypeID'
	, a.PostalAddressID
	, c.YearBuilt
	, c.TotalUnits
	, c.TotalFloors
	, case when isnull(c.FinancingAllowedPercent, 0) > 0 then 1 else 0 end 'FinancingAllowed'
	, case when isnull(c.NewDevelopmentID, 0) > 0 then 1 else 0 end 'NewDevelopment'
from Jaguar_v2_Prototype.dbo.tbListing a
inner join Jaguar_v2_Development.dbo.Listings b on a.ListingIDv15 = b.ListingID
inner join Jaguar_v2_Development.dbo.Buildings c on b.BuildingID = c.BuildingID

--************************
-- ApartmentStyle
--************************
update #type set typeval = 'ApartmentStyle'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select LongName
from Jaguar_v2_Development.dbo.attrApartmentStyles
left join dbo.tbAttribute on LongName = AttributeName
where Active = 1
	and AttributeID is null
order by LongName

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from Jaguar_v2_Development.dbo.attrApartmentStyles
inner join dbo.tbAttribute a on LongName = AttributeName and Active = 1
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- BuildingStyle
--************************

update #type set typeval = 'BuildingStyle'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select BuildingStyle
from openrowset('Microsoft.ACE.OLEDB.12.0','C:\Users\tFellini\SQL Backups\36119.accdb';'admin';'',BuildingStyle)
left join dbo.tbAttribute on BuildingStyle = AttributeName
where AttributeID is null
order by BuildingStyle

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from openrowset('Microsoft.ACE.OLEDB.12.0','C:\Users\tFellini\SQL Backups\36119.accdb';'admin';'',BuildingStyle)
inner join dbo.tbAttribute a on BuildingStyle = AttributeName
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- Condition
--************************

update #type set typeval = 'Condition'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select BuildingCondition
from openrowset('Microsoft.ACE.OLEDB.12.0','C:\Users\tFellini\SQL Backups\36119.accdb';'admin';'',BuildingCondition)
left join dbo.tbAttribute on BuildingCondition = AttributeName
where AttributeID is null
order by BuildingCondition

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from openrowset('Microsoft.ACE.OLEDB.12.0','C:\Users\tFellini\SQL Backups\36119.accdb';'admin';'',BuildingCondition)
inner join dbo.tbAttribute a on BuildingCondition = AttributeName
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- HeatingFuel
--************************

update #type set typeval = 'HeatingFuel'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select HeatingFuel
from openrowset('Microsoft.ACE.OLEDB.12.0','C:\Users\tFellini\SQL Backups\36119.accdb';'admin';'',HeatingFuel)
left join dbo.tbAttribute on HeatingFuel = AttributeName
where AttributeID is null
order by HeatingFuel

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from openrowset('Microsoft.ACE.OLEDB.12.0','C:\Users\tFellini\SQL Backups\36119.accdb';'admin';'',HeatingFuel)
inner join dbo.tbAttribute a on HeatingFuel = AttributeName
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- HeatingType
--************************

update #type set typeval = 'HeatingType'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select HeatingType
from openrowset('Microsoft.ACE.OLEDB.12.0','C:\Users\tFellini\SQL Backups\36119.accdb';'admin';'',HeatingType)
left join dbo.tbAttribute on HeatingType = AttributeName
where AttributeID is null
order by HeatingType

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from openrowset('Microsoft.ACE.OLEDB.12.0','C:\Users\tFellini\SQL Backups\36119.accdb';'admin';'',HeatingType)
inner join dbo.tbAttribute a on HeatingType = AttributeName
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- Sewer
--************************

update #type set typeval = 'Sewer'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select Sewer
from openrowset('Microsoft.ACE.OLEDB.12.0','C:\Users\tFellini\SQL Backups\36119.accdb';'admin';'',Sewer)
left join dbo.tbAttribute on Sewer = AttributeName
where AttributeID is null
order by Sewer

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from openrowset('Microsoft.ACE.OLEDB.12.0','C:\Users\tFellini\SQL Backups\36119.accdb';'admin';'',Sewer)
inner join dbo.tbAttribute a on Sewer = AttributeName
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- Structure
--************************

update #type set typeval = 'Structure'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select Structure
from openrowset('Microsoft.ACE.OLEDB.12.0','C:\Users\tFellini\SQL Backups\36119.accdb';'admin';'',Structure)
left join dbo.tbAttribute on Structure = AttributeName
where AttributeID is null
order by Structure

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from openrowset('Microsoft.ACE.OLEDB.12.0','C:\Users\tFellini\SQL Backups\36119.accdb';'admin';'',Structure)
inner join dbo.tbAttribute a on Structure = AttributeName
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- Utility
--************************

update #type set typeval = 'Utility'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select Utility
from openrowset('Microsoft.ACE.OLEDB.12.0','C:\Users\tFellini\SQL Backups\36119.accdb';'admin';'',Utility)
left join dbo.tbAttribute on Utility = AttributeName
where AttributeID is null
order by Utility

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from openrowset('Microsoft.ACE.OLEDB.12.0','C:\Users\tFellini\SQL Backups\36119.accdb';'admin';'',Utility)
inner join dbo.tbAttribute a on Utility = AttributeName
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- Water
--************************

update #type set typeval = 'Water'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select Water
from openrowset('Microsoft.ACE.OLEDB.12.0','C:\Users\tFellini\SQL Backups\36119.accdb';'admin';'',Water)
left join dbo.tbAttribute on Water = AttributeName
where AttributeID is null
order by Water

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from openrowset('Microsoft.ACE.OLEDB.12.0','C:\Users\tFellini\SQL Backups\36119.accdb';'admin';'',Water)
inner join dbo.tbAttribute a on Water = AttributeName
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- ListingProviders
--************************
update #type set typeval = 'ListingProviders'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select Name
from Jaguar_v2_Development.dbo.ListingProviders
left join dbo.tbAttribute on Name = AttributeName
where Active = 1
	and AttributeID is null
order by Name

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from Jaguar_v2_Development.dbo.ListingProviders
inner join dbo.tbAttribute a on Name = AttributeName and Active = 1
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- AgentTypes
--************************
update #type set typeval = 'AgentTypes'

insert into dbo.tbType(TypeName)
select typeval
from #type a
left join dbo.tbType b on a.typeval = b.TypeName
where b.TypeID is null

insert into dbo.tbAttribute(AttributeName)
select LongName
from Jaguar_v2_Development.dbo.attrAgentTypes
left join dbo.tbAttribute on LongName = AttributeName
where Active = 1
	and AttributeID is null
order by LongName

insert into dbo.tbAttributeType(AttributeID, TypeID)
select a.AttributeID, b.TypeID
from Jaguar_v2_Development.dbo.attrAgentTypes
inner join dbo.tbAttribute a on LongName = AttributeName and Active = 1
inner join dbo.tbType b on TypeName = (select typeval from #type)
left join dbo.tbAttributeType c on a.AttributeID = c.AttributeID and b.TypeID = c.TypeID
where c.AttributeTypeID is null

--************************
-- Person
--************************
--truncate table dbo.tbPerson
set identity_insert dbo.tbPerson on
insert into dbo.tbPerson (PersonID, FirstGivenName, FamilyName, PersonIDv15)
values (0, 'System', 'System', 0)
set identity_insert dbo.tbPerson off
insert into dbo.tbPerson (FirstGivenName, FamilyName, FullName, CreateDateTime, UpdateDateTime, PersonIDv15)
select 
	FirstName
	, LastName
	, FirstName + ' ' + LastName
	, CreateDate
	, ModDate
	, a.PersonID
from Jaguar_v2_Development.dbo.PersonalInfo a
left join dbo.tbPerson b on a.PersonID = b.PersonIDv15
where 
	a.FirstName is not null 
	and a.LastName is not null
	and b.PersonID is null

--************************
-- UserAccount
--************************
--truncate table dbo.tbUserAccount
--set identity_insert dbo.tbUserAccount on
--insert into dbo.tbUserAccount (UserAccountID, PersonID, AccountName, PasswordEncrypted)
--values (0, 0, 'System', '')
--set identity_insert dbo.tbUserAccount off
insert into dbo.tbUserAccount (PersonID, AccountName, PasswordEncrypted, CreateDateTime, UpdateDateTime)
select a.PersonID, b.UserID, b.Password, b.CreateDate, b.ModDate
from dbo.tbPerson a
inner join Jaguar_v2_Development.dbo.People b on a.PersonIDv15 = b.PersonID
left join dbo.tbUserAccount c on a.PersonID = c.PersonID
where c.PersonID is null

--************************
-- Company
--************************
insert into dbo.tbCompany (CompanyName)
select distinct a.Name
from Jaguar_v2_Development.dbo.ParentOffices a
left join dbo.tbCompany b on a.Name = b.CompanyName
where b.CompanyID is null
order by a.Name

--************************
-- Office
--************************
insert into dbo.tbOffice (CompanyID, OfficeCode, Detail, CreateDateTime, UpdateDateTime)
select c.CompanyID, a.ShortName, a.Name, a.CreateDate, a.ModDate
from Jaguar_v2_Development.dbo.Offices a
inner join Jaguar_v2_Development.dbo.ParentOffices b on a.ParentOfficeID = b.ParentOfficeID
inner join dbo.tbCompany c on b.Name = c.CompanyName
left join dbo.tbOffice d on a.ShortName = d.OfficeCode
where d.OfficeID is null

--************************
-- Agent
--************************
insert into dbo.tbAgent (PersonID, AgentNumber, StatusID, CreateDateTime, UpdateDateTime)
select b.PersonID, a.AgentNumber, case a.IsActive when 0 then 'I' when 1 then 'A' end, a.CreateDate, a.ModDate
from Jaguar_v2_Development.dbo.Agents a
inner join dbo.tbPerson b on a.PersonID = b.PersonIDv15
left join dbo.tbAgent c on b.PersonID = c.PersonID
where c.AgentID is null