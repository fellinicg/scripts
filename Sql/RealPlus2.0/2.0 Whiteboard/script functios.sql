set nocount on

select
--ROUTINE_NAME,
'use Jaguar_v2_Prototype2_BHS' + char(13) + char(10) +
'go' + char(13) + char(10) +
'set ansi_nulls on' + char(13) + char(10) +
'go' + char(13) + char(10) +
'set quoted_identifier off' + char(13) + char(10) +
'go' + char(13) + char(10) +
'if  exists (select * from sys.objects where object_id = object_id(N' + char(39) + 'dbo.' + ROUTINE_NAME + char(39) + ') and type in (N''FN'', N''IF'', N''TF'', N''FS'', N''FT''))' + char(13) + char(10) +
'drop function dbo.' + ROUTINE_NAME + char(13) + char(10) +
'go' + char(13) + char(10) +
ROUTINE_DEFINITION + char(13) + char(10) +
'go' + char(13) + char(10)
from INFORMATION_SCHEMA.ROUTINES
where routine_type = 'FUNCTION'
order by ROUTINE_NAME