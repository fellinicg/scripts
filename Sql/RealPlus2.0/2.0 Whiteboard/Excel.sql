--insert into 
--select * 
--from openrowset('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=C:\Users\tFellini\OneDrive\Squarefoot\nyc_zips.xlsx;HDR=YES', 'select * from [zips$]')
--select name from (select 'bldgname' 'name', 1 as Ct union select distinct bldgname, 99 from dbo.build where bldgname is not null) a order by CT

--select a.*
--from openrowset('Microsoft.ACE.OLEDB.12.0',
--	'Excel 12.0;Database=C:\Users\tFellini\OneDrive\Squarefoot\nyc_zips.xlsx', 'select * from [zips$]') a
--where Zipcode in (select zipcode
--from openrowset('Microsoft.ACE.OLEDB.12.0',
--	'Excel 12.0;Database=C:\Users\tFellini\OneDrive\Squarefoot\nyc_zips.xlsx', 'select * from [zips$]') a
--group by zipcode
--having count(zipcode) > 1)
--order by zipcode

select 
replicate('0', 5-len(zip)) + convert(varchar, zip) PostalCode, Primary_city, state, country, latitude, longitude
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Users\tFellini\OneDrive\Squarefoot\zip_code_database.xls', 'select * from [zip_code_database$]') a
where state = 'NY' and decommissioned = 0
