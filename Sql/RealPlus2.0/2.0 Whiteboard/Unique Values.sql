use Informix
go

declare @tbl varchar(100) = 'twnhses'

select 'select distinct ' + c.name + ' from dbo.twnhses where ' + c.name + ' is not null'
from sys.tables t
inner join sys.columns c on t.object_id = c.object_id
where t.name = @tbl
	and c.is_nullable = 1
order by c.column_id
