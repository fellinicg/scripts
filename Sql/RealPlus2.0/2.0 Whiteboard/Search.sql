declare @ListingCategory varchar(20) = 'Sale'
declare @ListingStatus varchar(max) = '163,156'
declare @PriceMin int = 0
declare @PriceMax int = 100000000
declare @Bedrooms float = 3
declare @Bathrooms float = 2
declare @OwnershipType varchar(max) = '26,28'

declare @TblStatus table (ListingID int)
declare @TblPrice table (ListingID int)
declare @TblBed table (PropertyID int)
declare @TblBath table (PropertyID int)


insert into @TblStatus select ListingID from dbo.vwCurrentListingStatusID a inner join (select ListItem from dbo.tfCommaListToTable(@ListingStatus)) b on a.AttributeValueID = b.ListItem
insert into @TblPrice select ListingID from dbo.vwCurrentListingPrice where ListingPrice between @PriceMin and @PriceMax

--declare @ListingCategory varchar(20) = 'Sale'
--declare @ListingStatus varchar(max) = '163,156'
--declare @PriceMin int = 0
--declare @PriceMax int = 100000000
--declare @Bedrooms float = 3
--declare @Bathrooms float = 2
--declare @OwnershipType varchar(max) = '26,28'

--declare @TblStatus table (ListingID int index ix1 clustered)
--declare @TblPrice table (ListingID int index ix1 clustered)
--declare @TblBed table (PropertyID int)
--declare @TblBath table (PropertyID int)


--select ListingID from dbo.vwCurrentListingStatusID a inner join (select ListItem from dbo.tfCommaListToTable(@ListingStatus)) b on a.AttributeValueID = b.ListItem
--intersect
--select ListingID from dbo.vwCurrentListingPrice where ListingPrice between @PriceMin and @PriceMax

insert into @TblStatus select ListingID from dbo.vwCurrentListingStatusID a inner join (select ListItem from dbo.tfCommaListToTable(@ListingStatus)) b on a.AttributeValueID = b.ListItem
insert into @TblPrice select ListingID from dbo.vwCurrentListingPrice where ListingPrice between @PriceMin and @PriceMax

	select
		l.ListingID
		, p.PropertyID
		, dbo.sfGetAttributeValue(l.ListingProviderID) 'ListingProvider'
		, dbo.sfGetAttributeValue(l.ListingCategoryID) 'ListingCategory'
		, dbo.sfGetAttributeValue(l.ListingTypeID) 'ListingType'
		, l.InitialPrice
--		, clp.ListingPrice 'CurrentPrice'
		, dbo.sfGetListingAttributeNumeric(l.ListingID, 25) 'ClosingPrice'
--		, cs.ValueName 'CurrentStatus'
		, dbo.sfGetListingAttributeNumeric(l.ListingID, 20) 'BuyerCommission'
		, dbo.sfGetListingAttributeNumeric(l.ListingID, 88) 'SellerCommission'
		, dbo.sfGetListingAttributeNumeric(l.ListingID, 98) 'TotalCommission'
		, dbo.sfGetListingAttributeText(l.ListingID, 53) 'ListingNumber'
		, dbo.sfGetListingAttributeText(l.ListingID, 54) 'ListingNumberReference'
		, l.ListDate
		, dbo.sfGetAttributeValue(p.PropertyClassID) 'PropertyClass'
		----, coalesce(p.Line1, bld.Line1) 'Line1'
		, dbo.sfGetPropertyAttributeText(p.PropertyID, 100) 'UnitIdentifier'
		----, coalesce(p.CityTown, bld.CityTown) 'CityTown'
		----, coalesce(p.StateProvince, bld.StateProvince) 'StateProvince'
		----, coalesce(p.PostalCode, bld.PostalCode) 'PostalCode'
		, dbo.sfGetPropertyAttributeNumeric(p.PropertyID, 9) 'AssociationFee'
		, dbo.sfGetPropertyAttributeNumeric(p.PropertyID, 10) 'Bathrooms'
		, dbo.sfGetPropertyAttributeNumeric(p.PropertyID, 11) 'Bedrooms'
		, dbo.sfGetPropertyAttributeNumeric(p.PropertyID, 41) 'HalfBathrooms'
		, dbo.sfGetPropertyAttributeNumeric(p.PropertyID, 85) 'Rooms'
		, dbo.sfGetPropertyAttributeNumeric(p.PropertyID, 91) 'SquareFootage'
		, dbo.sfGetPropertyAttributeNumeric(p.ParentPropertyID, 31) 'Elevators'
		, dbo.sfGetPropertyAttributeNumeric(p.ParentPropertyID, 36) 'FinanceAllowedPercent'
		, dbo.sfGetPropertyAttributeText(p.ParentPropertyID, 37) 'FlipTax'
		, dbo.sfGetPropertyAttributeNumeric(p.ParentPropertyID, 39) 'Floors'
		, dbo.sfGetPropertyAttributeNumeric(p.ParentPropertyID, 89) 'Shares'
		, dbo.sfGetPropertyAttributeNumeric(p.ParentPropertyID, 97) 'TaxDeductPct'
		, dbo.sfGetPropertyAttributeNumeric(p.ParentPropertyID, 10) 'Units'
		, dbo.sfGetPropertyAttributeNumeric(p.ParentPropertyID, 106) 'YearBuilt'
		, dbo.sfGetPropertyAttributeNumeric(p.ParentPropertyID, 107) 'YearIncorporated'
		, l.UpdateDateTime 'ListingUpdated'
		, l.CreateDateTime 'ListingCreated'
	from dbo.tbListing l
		inner join (
			select ListingID from @TblStatus
			intersect
			select ListingID from @TblPrice
		) ls on l.ListingID = ls.ListingID
		left join dbo.tbProperty p on l.PropertyID = p.PropertyID


return
--select l.ListingID, l.PropertyID
--from dbo.tbListing l
--	inner join
--	(
--		select ListingID from dbo.vwCurrentListingStatusID a inner join (select ListItem from dbo.tfCommaListToTable(@ListingStatus)) b on a.AttributeValueID = b.ListItem
--		intersect
--		select ListingID from dbo.vwCurrentListingPrice where ListingPrice between @PriceMin and @PriceMax
--	) ls on l.ListingID = ls.ListingID
--where
--	l.ListingCategoryID = dbo.sfGetAttributeValueID('ListingCategory',@ListingCategory)
	
--intersect

--select l.ListingID, l.PropertyID
--from dbo.tbListing l
--	inner join
--	(
		
		select PropertyID from dbo.tfPropertyAttribute('Bedrooms') where AttributeValue >= @Bedrooms
		intersect
		select PropertyID from dbo.tfPropertyAttribute('Bathrooms') where AttributeValue >= @Bathrooms
		--intersect
		--select PropertyID from dbo.tbProperty where ParentPropertyID in (select PropertyID from dbo.tbPropertyAttribute a inner join (select ListItem from dbo.tfCommaListToTable(@OwnershipType)) b on a.AttributeValueID = b.ListItem)
	--) ps on l.PropertyID = ps.PropertyID
--where
--	l.ListingCategoryID = dbo.sfGetAttributeValueID('ListingCategory',@ListingCategory)
		select PropertyID from dbo.tbPropertyAttributeNumeric where AttributeID = 10 and AttributeValue >= @Bathrooms
		intersect
		select PropertyID from dbo.tbPropertyAttributeNumeric where AttributeID = 11 and AttributeValue >= @Bedrooms



declare @ListingCategory varchar(20) = 'Sale'
declare @ListingStatus varchar(max) = '163,156'
declare @PriceMin int = 0
declare @PriceMax int = 10000000
declare @Bedrooms float = 3
declare @Bathrooms float = 2
declare @OwnershipType varchar(max) = '26,28'

--select l.ListingID, l.PropertyID
--from dbo.tbListing l
--	inner join
--	(
--		select ListingID from dbo.vwCurrentListingStatusID a inner join (select ListItem from dbo.tfCommaListToTable(@ListingStatus)) b on a.AttributeValueID = b.ListItem
--		intersect
--		select ListingID from dbo.vwCurrentListingPrice where ListingPrice between @PriceMin and @PriceMax
--	) ls on l.ListingID = ls.ListingID
--where
--	l.ListingCategoryID = dbo.sfGetAttributeValueID('ListingCategory',@ListingCategory)
	
--intersect

--select l.ListingID, l.PropertyID
--from dbo.tbListing l
--	inner join
--	(
		select PropertyID from dbo.tfPropertyAttribute('Bathrooms') where AttributeValue >= @Bathrooms