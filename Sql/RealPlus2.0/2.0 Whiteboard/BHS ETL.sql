-- dbo.addresses
insert into dbo.addresses
select *
from Jaguar_v2_Prototype2_BHS.dbo.addresses

-- dbo.big_text
insert into dbo.big_text
select *
from Jaguar_v2_Prototype2_BHS.dbo.big_text

-- dbo.contacts
insert into dbo.contacts
select *
from Jaguar_v2_Prototype2_BHS.dbo.contacts

-- dbo.properties
--truncate table dbo.properties
set statistics time on 

insert into dbo.properties
select src.*
from Jaguar_v2_Prototype2_BHS.dbo.properties src
	left join dbo.properties dest on src.propertyid = dest.propertyid
where dest.propertyid is null

set statistics time off

-- dbo.units
--truncate table dbo.units
set statistics time on 

insert into dbo.units
select src.*
from Jaguar_v2_Prototype2_BHS.dbo.units src
	left join dbo.units dest on src.unitid = dest.unitid
where dest.unitid is null

set statistics time off

-- dbo.tbListingPrice
--
disable trigger trgUpdateListingPrice on dbo.tbListingPrice
go
set statistics time on 

insert into dbo.tbListingPrice (ListingID, ListingPrice, PriceDateTime)
select distinct listingid, price, transaction_date
from Jaguar_v2_Prototype2_BHS.dbo.PriceChange
where listingid is not null
order by listingid, transaction_date

set statistics time off
go
enable trigger trgUpdateListingPrice on dbo.tbListingPrice

-- dbo.tbListingStatus
--
go
disable trigger trgUpdateListingStatus on dbo.tbListingStatus
go
set statistics time on 

insert into dbo.tbListingStatus (ListingID, StatusID, StatusDateTime)
select distinct sc.listingid, sc.data_change, sc.transaction_date
from Jaguar_v2_Prototype2_BHS.dbo.listings l
	inner join Jaguar_v2_Prototype2_BHS.dbo.StatusChange sc on l.listingid = sc.listingid
where sc.listingid is not null
	and isnumeric(sc.data_change) = 1
order by sc.listingid, sc.transaction_date

set statistics time off
go
enable trigger trgUpdateListingStatus on dbo.tbListingStatus

-- dbo.listings
--truncate table dbo.listings
set statistics time on 

insert into dbo.listings
select src.*
from Jaguar_v2_Prototype2_BHS.dbo.listings src
	left join dbo.listings dest on src.listingid = dest.listingid
where dest.listingid is null

set statistics time off

-- dbo.media
--truncate table dbo.media
set statistics time on 

insert into dbo.media
select src.*
from Jaguar_v2_Prototype2_BHS.dbo.media src
	left join dbo.media dest on src.mediaid = dest.mediaid
where dest.mediaid is null

set statistics time off

-- dbo.openhouse
--truncate table dbo.openhouse
set statistics time on 

insert into dbo.openhouse
select src.*
from Jaguar_v2_Prototype2_BHS.dbo.openhouse src
	left join dbo.openhouse dest on src.openhouseid = dest.openhouseid
where dest.openhouseid is null

set statistics time off

--select p.propertyid, u.unitid, u.ownershiptype, p.ownershiptype
----update dbo.properties set ownershiptype = u.ownershiptype
--from dbo.units u
--	inner join dbo.properties p on u.propertyid = p.propertyid
--where u.ownershiptype = 225
-- and p.ownershiptype is null

--insert into dbo.tbListingPrice (ListingID, ListingPrice, PriceDateTime)
--select l.ListingID, el.closingprice, coalesce(el.closingdate, el.statusdate, el.updted, el.modifieddate)
--from dbo.tbListing l
--	left join dbo.tbListingPrice lp on l.ListingID = lp.ListingID
--	inner join dbo.listings el on l.ListingID = el.listingid
--where lp.ListingPriceID is null
--	and el.closingprice is not null


--select l.ListingID, l.CurrentStatusID, ls.ListingStatusID
----update dbo.tbListing set CurrentStatusID = ls.ListingStatusID
--from dbo.tbListing l 
--	inner join (
--				select ls.ListingStatusID, ls.ListingID
--				from dbo.tbListingStatus ls
--					inner join 
--					(
--						select distinct ListingID, max(StatusDateTime) StatusDateTime 
--						from dbo.tbListingStatus 
--						group by ListingID
--					) cs on ls.ListingID = cs.ListingID and ls.StatusDateTime = cs.StatusDateTime) ls on l.ListingID = ls.ListingID
--where CurrentStatusID is null

--insert into dbo.tbListingStatus (ListingID, StatusID, StatusDateTime)
--select l.ListingID, 220, coalesce(el.closingdate, el.statusdate, el.updted, el.modifieddate)
--from dbo.tbListing l
--	inner join listings el on l.ListingID = el.listingid 
--where CurrentStatusID is null
--	and l.ListingCategoryID = 7
--	and el.closingprice is not null

--insert into dbo.tbListingStatus (ListingID, StatusID, StatusDateTime)
--select l.ListingID, 156, coalesce(el.closingdate, el.statusdate, el.updted, el.modifieddate)
--from dbo.tbListing l
--	inner join listings el on l.ListingID = el.listingid 
--where CurrentStatusID is null
--	and l.ListingCategoryID = 8
--	and el.closingprice is not null