---- Make sure column names match and you have added the C
---- Delete temporary table
--if Object_ID('tempdb.dbo.#Customers') is not null drop table #Customers

---- Insert contents of file on .122 server into temporary table
--select * 
--into #Customers
--from openquery([54.208.100.122], 'select * from openrowset(''Microsoft.ACE.OLEDB.12.0'', ''Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents'', ''select * from FullCustomerList.csv where customerid is not null'')')

select 
	row_number() over(order by company asc) * 1000000 as Row#,
	company
from (select distinct substring([Agent ID], charindex('_', [Agent ID]), 100) company from #Customers) a
order by company

exec dbo.upTest3
@GridNorth1 = '30'
,@GridSouth1 = '20'
,@GridEast1 = '500'
,@GridWest1 = '600'
,@AddressOrBuildingName = '1087767,1047569'
,@BuildingAllow = '174, 173, 176, 178, 2556, 177, 175'

exec dbo.upGetListingBySearchV3Count
@shouldReturnList = 1
,@AgentID = 1
, @ListingCategory = 8
,@GridNorth1 = '30'
,@GridSouth1 = '20'
,@GridEast1 = '500'
,@GridWest1 = '600'

exec dbo.upGetListingBySearchV4
@ListingCategory = 8    --Sales
	, @ListActivityStatus = '2552'
	, @ListActivityStart = '7/1/2016'
	, @ListActivityEnd = '8/1/2016'
, @PriceMin = 1000000
, @PriceMax = 5000000
--, @ManagedBuildings = '2559'
--, @ManagingAgent = 'HALS'
--, @ListUpdateDateStart = '7/1/16'
--, @ListUpdateDateEnd = '8/2/16'
	@ListingCategory = 8    --Sales
	--, @AddressID = '1087767$$   1047569'
	, @PriceMin = 5000000
	, @PriceMax = 10000000
	, @Ownershiptype = '25,26,27,24,225,226'
	--, @Ownershiptype = '26'
	, @TownhouseType = '2590'
	--, @FloorMin = 20
	--, @FloorMax = 40
	--, @IncPenthouses = 1
	, @OwnershipType = '225'
	, @Townhousetype = '2595'--,207'--,208,209,210,211,212,213'
	--, @BuildingAllow = '174'--, 173, 176, 178, 2556'--, 177, 175'


exec dbo.upGetListingBySearchV3
@ListingCategory = 8    --Sales
, @PriceMin = 1000000
, @PriceMax = 1500000
, @Bedrooms = 5

use Jaguar_v2_WiP

select PropertyID, AssociationFee, Bathrooms, Bedrooms, Elevators, FinanceAllowedPercent, Floors, HalfBathrooms, Rooms, Shares, SquareFootage, TaxDeductPct, Units, YearBuilt, YearIncorporated
from 
	(select 
		pan.PropertyID
		, a.AttributeName
		--, pan.AttributeValue
		, stuff((
              select ', ' + convert(varchar(max), AttributeValue)
              from dbo.tbPropertyAttributeNumeric
              where PropertyID = pan.PropertyID and AttributeID = pan.AttributeID
              for xml path('')),1,1,'') as 'CSV'
	from dbo.tbPropertyAttributeNumeric pan
		inner join dbo.tbAttribute a on a.AttributeID = pan.AttributeID) pan
	pivot
	--(max(AttributeValue) for AttributeName in (AssociationFee,Bathrooms,Bedrooms,Elevators,FinanceAllowedPercent,FloorNumber,Floors,HalfBathrooms,Rooms,Shares,SquareFootage,TaxDeductPct,TotalFees,Units,YearBuilt,YearIncorporated)) as pvt
	(max(CSV) for AttributeName in (AssociationFee, Bathrooms, Bedrooms, Elevators, FinanceAllowedPercent, Floors, HalfBathrooms, Rooms, Shares, SquareFootage, TaxDeductPct, Units, YearBuilt, YearIncorporated)) as pvt

--select *
--from dbo.tbPropertyAttributeNumeric pan
--	pivot (max(AttributeValue) for AttributeID in ([39], [106])) p
--where PropertyID = 1
--	and pan.AttributeID in (39, 106)




select *
from
	(select PropertyID, a.AttributeName, stuff((select ',' + convert(varchar(max), v.ValueName)-- v.ValueName
	from dbo.tbPropertyAttribute pa
		inner join dbo.tbAttributeValue av on pa.AttributeValueID = av.AttributeValueID and av.Isdeleted = 0
		inner join dbo.tbAttribute a on av.AttributeID = a.AttributeID and a.IsDeleted = 0
		inner join dbo.tbValue v on av.ValueID = v.ValueID and v.Isdeleted = 0
		--inner join dbo.tbAttribute a on pan.AttributeID = a.AttributeID
	--where PropertyID = 2
	) as pan
	pivot
	(max(ValueName) for AttributeName in (ApartmentType,Area,BuildingAllow,BuildingAmenity,BuildingEra,BuildingNotAllow,BuildingPersonnel,BuildingType,Exposure,IsNewConstruction,Neighborhood,OutdoorSpace,OwnershipType,[View])) as pvt
	order by PropertyID


	--select PropertyID, a.AttributeName, AttributeValue
	--from dbo.tbPropertyAttributeNumeric pan inner join dbo.tbAttribute a on pan.AttributeID = a.AttributeID
	--where PropertyID = 1





select p.PropertyID, p.ParentPropertyID, pat.AttributeValue 'UnitIdentifier'
from dbo.tbProperty p
	left join dbo.tbPropertyAttributeText pat on p.PropertyID = pat.PropertyID and pat.AttributeID = 100
where PropertyClassID = 6
order by p.ParentPropertyID, p.PropertyID, pat.AttributeValue


insert into dbo.tbAttributeValue (AttributeID, ValueID)
select a.AttributeID, v.ValueID
from dbo.tbAttribute a
	cross apply dbo.tbValue v
	left join dbo.tbAttributeValue av on a.AttributeID = av.AttributeID
where 
--(charindex('sponsor', AttributeName) > 0 or charindex('boardapproval', a.AttributeName) > 0)
	(dbo.beginswith(AttributeName, 'is') = 1 or dbo.beginswith(AttributeName, 'has') = 1)
	and v.Valuename in ('yes','no')
	and av.AttributeValueID is null
order by a.AttributeID, v.ValueID


set identity_insert Jaguar_v2_Prototype2.dbo.tbAttributeValue on

insert into Jaguar_v2_Prototype2.dbo.tbAttributeValue (AttributeValueID, AttributeID, ValueID, IsDeleted, CreateDateTime, CreateID, UpdateDateTime, UpdateID, EtlID)
select a.*
from dbo.tbAttributeValue a
left join Jaguar_v2_Prototype2.dbo.tbAttributeValue b on a.AttributeValueID = b.AttributeValueID
where b.AttributeValueID is null

select distinct a.AttributeName, ', coalesce(max(case when AttributeID = ' + convert(varchar, pa.AttributeID) + ' then AttributeValue end), null) as ' + a.AttributeName
from dbo.tbPropertyAttributeNumeric pa
	inner join dbo.tbAttribute a on pa.AttributeID = a.AttributeID
--union
select distinct a.AttributeName, ', coalesce(max(case when AttributeID = ' + convert(varchar, pa.AttributeID) + ' then AttributeValue end), null) as ' + a.AttributeName
from dbo.tbPropertyAttributeText pa
	inner join dbo.tbAttribute a on pa.AttributeID = a.AttributeID
order by a.AttributeName

select 
	ListingPriceID
	, ListingPrice
	, ((coalesce(lag(ListingPrice) over (order by ListingPriceID), ListingPrice) - ListingPrice) / ListingPrice) * -100 as pctdiff
from dbo.tbListingPrice
where ListingID = 513285


use Jaguar_v2_WiP
go
exec dbo.upGetListingBySearchOmni
@ListingCategory = 8    --Sales
, @Omni = 'mel'
, @ListingStatus = '163,157'  --Active, Contract Signed
, @PriceMin = 1000000
, @PriceMax = 1500000
, @Bedrooms = 2
, @Bathrooms = 1
, @Rooms = 1
, @OwnershipType = '24,26'   --Coop, Condo
--, @ListDateStart = '1/1/2016'
--, @ListDateEnd = '6/1/2016'
--, @OpenHouseStart = '1/1/2016'
--, @OpenHouseEnd = '6/1/2016'
--, @WebOrListingNum = '14165769'
, @Neighborhood = '44,110,70'  --Tribeca, Chelsea, Park Slope
, @BuildingPersonnel = '188'  --Doorman
--, @BuildingAllow = '175'   --Pets
, @ListingType = '19'
, @MinSqFt = 1000
, @IncMissingSqFt = 0

select t.name, i.name, i.type_desc, c.name, ic.*
from sys.indexes i
inner join sys.index_columns ic on i.object_id = ic.object_id and i.index_id = ic.index_id
inner join sys.tables t on i.object_id = t.object_id
inner join sys.columns c on i.object_id = c.object_id and ic.column_id = c.column_id
where charindex('ncix', i.name) = 1
order by t.name, ic.index_id

--declare @xml xml = (select ListingID 'value' from dbo.tbListing for xml raw)

--select *
--from information_schema.CHECK_CONSTRAINTS

exec sp_helpindex 'tbListingAttributeNumeric'

--exec dbo.upAgentGetWorkInProgress 1, 1--, 1

--exec dbo.upGetWorkInProgressListings 1,1

--exec dbo.upGetSaleStatus
--exec dbo.upGetRentalStatus
--exec dbo.upGetListingType
--exec dbo.upGetBuildingAmenity
--exec dbo.upGetBuildingAllow
--exec dbo.upGetBuildingNotAllow

Update dbo.tbAttributeValue set SortOrder = b.SortOrder
from dbo.tbAttributeValue a
inner join (
select a.AttributeValueID, b.SortOrder
from dbo.vwAttributeValues a
inner join dbo.vwAttributeValues b on a.ValueName = b.ValueName
where a.AttributeName = 'MinPricePerSqFt' and b.attributeName = 'MaxPricePerSqFt') b on a.AttributeValueID = b.AttributeValueID

select top 5 *  from BuildingParentOfficeComments where Comments is not null
select top 5 * from ListingParentOfficeComments where Comments is not null and ListingID = 500473
--select top 5 * from ListingPersonalComments where Comments is not null
select top 5 * from OpenHouses where Comments is not null
select top 5 Comments, * from Rentals where Comments is not null
select top 5 Comments, * from Sales where Comments is not null


select specific_name, parameter_name
from information_schema.parameters
where specific_name like 'upget%'
order by specific_name, ordinal_position

select distinct b.name TableName, rows RowCt
from sys.partitions a
inner join sys.tables b on a.object_id = b.object_id
where rows > 0

select name StoredProcedure
from sys.procedures
where charindex('up', name) = 1
order by name

select Name 'View'
from sys.views
order by name
select db_name(dbid) DBName, count(dbid) NumberOfConnections, loginame LoginName
from sys.sysprocesses
where dbid > 0
group by dbid, loginame
order by dbid, loginname

exec dbo.upUpdateDbStructDesc 'Contains numeric attribute values for a property where the value is both string and is not stored in tbAttributeValue.', 'tbPropertyAttributeText'

declare @aa varchar (max)
set @aa = ''
select @aa = coalesce (case when @aa = '' then AttributeName else @aa + ',' + AttributeName end,'')
from dbo.tbAttribute

select @aa

select top 10 *
from dbo.coop
where listed is not null
order by updted desc
where numb in (992358,169514)

select *
from dbo.build
where numb in (29350,7045)
order by numb desc

select *
from INFORMATION_SCHEMA.COLUMNS
where TABLE_NAME = 'Listings'
and IS_NULLABLE = 'NO'

select addressid, fulladdress, formaladdress,displayaddress,botbaaddress,shortaddress,housenumber_sort,housenumber_full,housenumber_low,housenumber_high,streetname,streetname_full,streetname_direction,streetname_short,streetname_prefix,streetname_extension,streetname_suffix,streettype,addresstype,longitude,latitude,city,state,postalcode,town,province,precompass,postcompass,borough,block,blockchar,lot,lotchar,neighborhoodid,sectionid,taxid,rls_streetid,botba_adr_id,botba_bbl_id,botba_bin_id,grid_north,grid_south,grid_east,grid_west,cross_street,bin,bob_bin,bob_bbl,createddate,modifieddate,createdby,modifiedby
into Informix.dbo.addresses
from openquery([ROLEX], 'select addressid, fulladdress, formaladdress,displayaddress,botbaaddress,shortaddress,housenumber_sort,housenumber_full,housenumber_low,housenumber_high,streetname,streetname_full,streetname_direction,streetname_short,streetname_prefix,streetname_extension,streetname_suffix,streettype,addresstype,longitude,latitude,city,state,postalcode,town,province,precompass,postcompass,borough,block,blockchar,lot,lotchar,neighborhoodid,sectionid,taxid,rls_streetid,botba_adr_id,botba_bbl_id,botba_bin_id,grid_north,grid_south,grid_east,grid_west,cross_street,bin,bob_bin,bob_bbl,createddate,modifieddate,createdby,modifiedby from addresses')

select TypeName 'Type', AttributeName 'Attribute'
from dbo.vwAttributeTypes
order by TypeName, AttributeName

--select count(1)
--from dbo.Rentals a
--inner join dbo.Sales b on a.ListingID = b.ListingID

select count(ListingID), count(distinct ListingID)
from dbo.Rentals a

select count(a.ListingID), count(distinct a.ListingID)
from dbo.Rentals a
inner join dbo.Listings b on a.ListingID = b.ListingID

select count(ListingID), count(distinct ListingID)
from dbo.Sales

select count(b.ListingID), count(distinct b.ListingID)
from dbo.Sales a
inner join dbo.Listings b on a.ListingID = b.ListingID

select count(1),  (890522 + 897245) 'Sales+Rentals'
from dbo.Listings

select TABLE_CATALOG 'Database', TABLE_SCHEMA 'Schema', TABLE_NAME 'Table', COLUMN_NAME 'Column', DATA_TYPE 'DataType', isnull(convert(varchar, CHARACTER_MAXIMUM_LENGTH), '') 'Length', isnull(COLUMN_DEFAULT, '') 'ColumnDefault'
from INFORMATION_SCHEMA.COLUMNS
where charindex('tb', TABLE_NAME) = 1
order by TABLE_NAME, ORDINAL_POSITION

select *
from dbo.Buildings
where Buildingname in (
	select BuildingName, count(buildingName)
	from dbo.Buildings
	where BuildingID in (
		select 
			distinct b.BuildingID
		from dbo.Listings a
			inner join dbo.Buildings b on a.BuildingID = b.BuildingID
		where a.BuildingID is not null
			and ListedDate > '1/1/2015'
			and b.Active = 1
	)
	and buildingname is not null
	group by BuildingID, BuildingName
	having count(BuildingName) = 1
	order by buildingname
)
select *
from dbo.buildings
where buildingname = 'THE ENCLAVE'--'123 ON THE PARK'

select distinct FullAddress, City, State, Zip, AreaID, NeighborhoodID
from dbo.Addresses
where fulladdress is not null
and AreaID = 9
order by FullAddress

--select c.*, b.*, a.*
--from dbo.Addresses a
--inner join dbo.attrNeighborhoods b on a.NeighborhoodID = b.ID
--inner join dbo.attrAreas c on a.AreaID = c.ID
--where FullAddress = ' Bainbridge Street'

select * from dbo.attrAreas

select distinct AreaID, Longname
from dbo.Addresses
inner join dbo.attrAreas on AreaID = ID
where fulladdress is not null

select BuildingID
from dbo.Addresses
group by buildingid
having count(buildingid)>5


select * from dbo.Addresses where BuildingID = 89552
select * from dbo.Listings where BuildingID = 89552

select buildingID
from dbo.listings
where UnitNumber is not null
group by buildingid
having count(buildingid)>5
--where ListingID in (
--1190901
--,1192298
--,1275525
--,1275526
--,2663052
--,1187106
--,1186582
--)

select *
--count(distinct UnitNumber )
from dbo.Listings
where UnitNumber = 1702160203 --is not null --and SubUnitNumber is not null

select *
from dbo.coop a
inner join
(
select CoopID 
from dbo.tbTest a 
inner join (select ListItem from dbo.tfCommaListToTable('1,2')) b on a.AttributeID = b.ListItem
intersect
select CoopID 
from dbo.tbTest a 
inner join (select ListItem from dbo.tfCommaListToTable('6')) b on a.AttributeID = b.ListItem
intersect
select CoopID 
from dbo.tbTest a 
inner join (select ListItem from dbo.tfCommaListToTable('9')) b on a.AttributeID = b.ListItem
intersect
select CoopID 
from dbo.tbTest a 
inner join (select ListItem from dbo.tfCommaListToTable('5')) b on a.AttributeID = b.ListItem
) b on a.numb = b.CoopID

select *
from openquery([REALPLUS], 'select max(length(bldgname)) from build') 

select * 
into dbo.build
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Feeds\build.xlsx', 'select * from [Sheet1$]')
select * 
into dbo.tbZips
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Feeds\nyc_zips.xlsx', 'select * from [zips$]')
		
SELECT object_name(object_id) AS name,
    partition_id, partition_number AS pnum,  rows,
    allocation_unit_id AS au_id, type_desc as page_type_desc,
    total_pages AS pages
FROM sys.partitions p  JOIN sys.allocation_units a
   ON p.partition_id = a.container_id
where object_name(object_id) = 'ListingParentOfficeComments'


SELECT allocated_page_file_id as PageFID, allocated_page_page_id as PagePID,
       object_id as ObjectID, partition_id AS PartitionID,
       allocation_unit_type_desc as AU_type, page_type as PageType
FROM sys.dm_db_database_page_allocations
       (db_id('Jaguar_v2_Development'), object_id('ListingParentOfficeComments'), null, null, 'DETAILED');
	   
select FullAddress
from dbo.Addresses
group by FullAddress
having count(FullAddress) > 5

select *
from dbo.Addresses
where FullAddress = ' Cooper Street'

select 
--top 100 ListingID
--max(datalength(comments))
--count(1)
comments
from ListingParentOfficeComments
where charindex(comments, '1 bath') > 0
--where ListingID = 604024
--group by ListingID
--having count(1) > 1

select *
from dbo.Offices
where ParentOfficeID = 2255

select *
from dbo.ParentOffices 
where ParentOfficeID = 2255

select ParentOfficeID, count(1)
from dbo.Offices
group by ParentOfficeID
having count(1) > 1

select convert(bigint, listingnumber)
from dbo.Listings

select 
--distinct UnitNumberFull
*
from dbo.Listings
where BuildingID = 75071 and UnitNumberFull = '10B'