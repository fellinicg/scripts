use  Jaguar_v2_WiP

-- System account
set identity_insert dbo.tbPerson on
insert into dbo.tbPerson (PersonID, FirstGivenName, FamilyName, FullName, EtlID)
select 0, 'System', 'System', 'System', 0
where not exists (select 1 from dbo.tbPerson where FirstGivenName = 'System' and FamilyName = 'System')
set identity_insert dbo.tbPerson off

insert into dbo.tbPerson (FirstGivenName, FamilyName, FullName, CreateDateTime, UpdateDateTime, EtlID)
select 
	FirstName
	, LastName
	, FirstName + ' ' + LastName
	, CreateDate
	, ModDate
	, a.PersonID
from Jaguar_v1_Production.dbo.PersonalInfo a
left join dbo.tbPerson b on a.PersonID = b.EtlID
where 
	a.FirstName is not null 
	and a.LastName is not null
	and b.PersonID is null