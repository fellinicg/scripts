
----truncate table dbo.tbWorkingSet

------ tbWorkingSet
----insert into dbo.tbWorkingSet (WorkingID)
----select ListingID
----from Jaguar_v1_Production.dbo.Listings
----where ListingNumber in (13272919,12525009,14374921,13259506,13045944,13294288,14324720,14046358,13393955,13667168,13761503
----,12062448,13696417,13157909,14253709,14220190,11914892,1054082,1065230,1062156)

------tbPostalAddress
--insert into Jaguar_v2_Prototype.dbo.tbPostalAddress (Line1, CityTown, StateProvince, PostalCode, Country, Longitude, Latitude, EtlID)
--select distinct FullAddress, c.CityTown, c.StateProvince, Zip, c.Country, a.Longitude, a.Latitude, a.BuildingID
--from Jaguar_v2_Prototype.dbo.tbWorkingSet w
--	inner join dbo.Listings b on w.WorkingID = b.ListingID
--	inner join dbo.Addresses a on b.BuildingID = a.BuildingID
--	left join Jaguar_v2_Prototype.dbo.tbPostalCode c on a.zip = c.PostalCode
--	left join Jaguar_v2_Prototype.dbo.tbPostalAddress d on a.FullAddress = d.Line1
--	left join Jaguar_v2_Prototype.dbo.tbProperty p on a.BuildingID = p.EtlID
--where p.PropertyID is null

------tbProperty
--insert into Jaguar_v2_Prototype.dbo.tbProperty (PropertyClassID, Block, Lot, CreateDateTime, UpdateDateTime, UpdateID, EtlID)
--select distinct c.AttributeValueID, l.Block, l.Lot, a.CreateDate, a.ModDate, 0, a.BuildingID
--from Jaguar_v2_Prototype.dbo.tbWorkingSet w
--	inner join dbo.Listings l on w.WorkingID = l.ListingID
--	inner join dbo.buildings a on a.BuildingID = l.BuildingID
--	inner join dbo.Addresses b on a.BuildingID = b.BuildingID
--	left join Jaguar_v2_Prototype.dbo.tbProperty p on a.BuildingID = p.EtlID
--	,(select AttributeValueID from Jaguar_v2_Prototype.dbo.vwAttributeValues where AttributeName = 'PropertyClass' and ValueName = 'Structure') c
--where p.PropertyID is null

------tbPropertyAddress
--insert into Jaguar_v2_Prototype.dbo.tbPropertyAddress (PropertyID, PostalAddressID, IsPrimary)
--select a.PropertyID, b.PostalAddressID, 1
--from Jaguar_v2_Prototype.dbo.tbProperty a
--inner join Jaguar_v2_Prototype.dbo.tbPostalAddress b on a.EtlID = b.EtlID
--left join Jaguar_v2_Prototype.dbo.tbPropertyAddress c on a.PropertyID = c.PropertyID and b.PostalAddressID = c.PostalAddressID
--where c.PropertyAddressID is null

------tbUnit
--insert into Jaguar_v2_Prototype.dbo.tbUnit (PropertyID, UnitTypeID)
--select p.PropertyID, ut.AttributeValueID
--from Jaguar_v2_Prototype.dbo.vwPropertyInfo p
--	left join Jaguar_v2_Prototype.dbo.vwAttributeValues ut on ut.AttributeName = 'UnitType' and ut.ValueName = 'Residence'
--	left join Jaguar_v2_Prototype.dbo.tbUnit u on p.PropertyID = u.PropertyID
--where 
--	p.PropertyClass = 'Structure'
--	and 
--	u.UnitID is null

------tbListing
--insert into Jaguar_v2_Prototype.dbo.tbListing (ListingCategoryID, PostalAddressID, ListingTypeID, ListingProviderID, InitialPrice, ListDate, CreateDateTime, UpdateDateTime, EtlID)
--select 
--	case when b.ListingID is null then 2 when c.ListingID is null then 1 end
--	, pa.PostalAddressID
--	, lt.AttributeValueID
--	, lp.AttributeValueID
--	, coalesce(b.OriginalPrice, rh.NewRentalPrice)
--	, a.ListedDate
--	, a.CreateDate
--	, a.ModDate
--	, a.ListingID
--from Jaguar_v2_Prototype.dbo.tbWorkingSet w
--	inner join dbo.Listings a on w.WorkingID = a.ListingID
--	left join dbo.Sales b on a.ListingID = b.ListingID
--	left join dbo.Rentals c on a.ListingID = c.ListingID
--	left join 
--		(select r.RentalID, r.NewRentalPrice from dbo.RentalHistories r inner join (select distinct RentalID, min(EventDate) EventDate from dbo.RentalHistories group by RentalID) rh on r.RentalID = rh.RentalID and r.EventDate = rh.EventDate) rh on c.RentalID = rh.RentalID
--	left join Jaguar_v2_Prototype.dbo.vwAttributeValues lt on lt.AttributeName = 'ListingType' and a.ListingTypeID = lt.EtlID
--	left join Jaguar_v2_Prototype.dbo.vwAttributeValues lp on lp.AttributeName = 'ListingProvider' and a.ListingProviderID = lp.EtlID
--	left join Jaguar_v2_Prototype.dbo.tbPostalAddress pa on a.BuildingID = pa.EtlID
--	left join Jaguar_v2_Prototype.dbo.tbListing l on a.ListingID = l.EtlID
--where l.ListingID is null

------tbListingMedia
--insert into Jaguar_v2_Prototype.dbo.tbListingMedia (ListingID, MediaID, Detail, AdditionalDetail, SortOrder, CreateDateTime, UpdateDateTime, EtlID)
--select l.ListingID, b.AttributeValueID, a.Description, a.MediaDescription, a.SequenceNumber, a.CreateDate, a.ModDate, a.ListingPictureID
--from Jaguar_v2_Prototype.dbo.tbWorkingSet w
--	inner join dbo.ListingPictures a on w.WorkingID = a.ListingID
--	left join Jaguar_v2_Prototype.dbo.vwAttributeValues b on b.AttributeName = 'ImageType' and a.PictureTypeID = b.EtlID
--	left join Jaguar_v2_Prototype.dbo.tbListing l on a.ListingID = l.EtlID
--	left join Jaguar_v2_Prototype.dbo.tbListingMedia dest on l.ListingID = dest.ListingID
--where dest.ListingID is null

--select UnitNumberFull
--from Jaguar_v2_Prototype.dbo.tbWorkingSet w
--	inner join dbo.Listings a on w.WorkingID = a.ListingID
--	left join dbo.Sales b on a.ListingID = b.ListingID
--	left join dbo.Rentals c on a.ListingID = c.ListingID
--	left join dbo.Homes d on a.ListingID = d.ListingID
--	left join dbo.Buildings e on a.BuildingID = e.BuildingID
--	left join dbo.extraHomes f on d.HomeID = f.KeyID
--where UnitNumberFull is not null

----UnitIdentifier
insert into Jaguar_v2_Prototype.dbo.tbUnitAttributeText (UnitID, AttributeID, AttributeValue)
select distinct u.UnitID, v.AttributeID, a.UnitNumberFull
from Jaguar_v2_Prototype.dbo.tbWorkingSet w
	inner join dbo.Listings a on w.WorkingID = a.ListingID
	inner join Jaguar_v2_Prototype.dbo.tbListing l on a.ListingID = l.EtlID
	inner join Jaguar_v2_Prototype.dbo.tbPropertyAddress pa on l.PostalAddressID = pa.PostalAddressID	
	inner join Jaguar_v2_Prototype.dbo.tbUnit u on u.PropertyID = pa.PropertyID
	inner join Jaguar_v2_Prototype.dbo.vwAttributes v on v.AttributeName = 'UnitIdentifier'
	left join Jaguar_v2_Prototype.dbo.tbUnitAttributeText z on z.UnitID = u.UnitID and z.AttributeID = v.AttributeID and z.AttributeValue = a.UnitNumberFull
where UnitNumberFull is not null
	and z.UnitAttributeTextID is null

----Apartmentstyle
insert into Jaguar_v2_Prototype.dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
select distinct pa.PropertyID, b.AttributeValueID
from Jaguar_v2_Prototype.dbo.tbWorkingSet w
	inner join dbo.Listings a on w.WorkingID = a.ListingID
	inner join dbo.Homes d on a.ListingID = d.ListingID
	inner join dbo.mvaHomeApartmentStyles f on d.HomeID = f.KeyID
	inner join Jaguar_v2_Prototype.dbo.vwAttributeValues b on b.AttributeName = 'ApartmentType' and f.ValueID = b.EtlID
	inner join Jaguar_v2_Prototype.dbo.tbListing l on a.ListingID = l.EtlID
	inner join Jaguar_v2_Prototype.dbo.tbPropertyAddress pa on l.PostalAddressID = pa.PostalAddressID
	left join Jaguar_v2_Prototype.dbo.tbPropertyAttribute z on z.PropertyID = pa.PropertyID and z.AttributeValueID = b.AttributeValueID
where z.PropertyAttributeID is null

--Sale price
insert into Jaguar_v2_Prototype.dbo.tbListingPrice (ListingID, ListingPrice, PriceDateTime)
select distinct l.ListingID, c.NewSalePrice, c.CreateDate
from Jaguar_v2_Prototype.dbo.tbWorkingSet w	
	inner join dbo.Listings a on w.WorkingID = a.ListingID 
	inner join dbo.Sales b on a.ListingID = b.ListingID
	inner join dbo.SaleHistories c on b.SaleID = c.SaleID
	inner join Jaguar_v2_Prototype.dbo.tbListing l on a.ListingID = l.EtlID
	left join Jaguar_v2_Prototype.dbo.tbListingPrice lp on lp.ListingID = l.ListingID and lp.ListingPrice = c.NewSalePrice and lp.PriceDateTime = c.CreateDate
where c.NewSalePrice is not null
	and lp.ListingPriceID is null
order by l.ListingID, c.CreateDate

-- Sale status
insert into Jaguar_v2_Prototype.dbo.tbListingStatus (ListingID, StatusID, StatusDateTime)
select distinct l.ListingID, v.AttributeValueID, c.CreateDate
from Jaguar_v2_Prototype.dbo.tbWorkingSet w	
	inner join dbo.Listings a on w.WorkingID = a.ListingID 
	inner join dbo.Sales b on a.ListingID = b.ListingID
	inner join dbo.SaleHistories c on b.SaleID = c.SaleID
	inner join Jaguar_v2_Prototype.dbo.tbListing l on a.ListingID = l.EtlID
	inner join Jaguar_v2_Prototype.dbo.vwAttributeValues v on v.AttributeName = 'SaleStatus' and c.NewSaleStatusID = v.EtlID
	left join Jaguar_v2_Prototype.dbo.tbListingStatus dest on dest.ListingID = l.ListingID and dest.StatusID = v.AttributeValueID and dest.StatusDateTime = c.CreateDate
where c.NewSaleStatusID is not null
	and dest.ListingStatusID is null
order by l.ListingID, c.CreateDate

--Rental price
insert into Jaguar_v2_Prototype.dbo.tbListingPrice (ListingID, ListingPrice, PriceDateTime)
select distinct l.ListingID, c.NewRentalPrice, c.CreateDate
from Jaguar_v2_Prototype.dbo.tbWorkingSet w	
	inner join dbo.Listings a on w.WorkingID = a.ListingID 
	inner join dbo.Rentals b on a.ListingID = b.ListingID
	inner join dbo.RentalHistories c on b.RentalID = c.RentalID
	inner join Jaguar_v2_Prototype.dbo.tbListing l on a.ListingID = l.EtlID
	left join Jaguar_v2_Prototype.dbo.tbListingPrice lp on lp.ListingID = l.ListingID and lp.ListingPrice = c.NewRentalPrice and lp.PriceDateTime = c.CreateDate
where c.NewRentalPrice is not null
	and lp.ListingPriceID is null
order by l.ListingID, c.CreateDate

-- Rental status
insert into Jaguar_v2_Prototype.dbo.tbListingStatus (ListingID, StatusID, StatusDateTime)
select distinct l.ListingID, v.AttributeValueID, c.CreateDate
from Jaguar_v2_Prototype.dbo.tbWorkingSet w	
	inner join dbo.Listings a on w.WorkingID = a.ListingID 
	inner join dbo.Rentals b on a.ListingID = b.ListingID
	inner join dbo.RentalHistories c on b.RentalID = c.RentalID
	inner join Jaguar_v2_Prototype.dbo.tbListing l on a.ListingID = l.EtlID
	inner join Jaguar_v2_Prototype.dbo.vwAttributeValues v on v.AttributeName = 'RentalStatus' and c.NewRentalStatusID = v.EtlID
	left join Jaguar_v2_Prototype.dbo.tbListingStatus dest on dest.ListingID = l.ListingID and dest.StatusID = v.AttributeValueID and dest.StatusDateTime = c.CreateDate
where c.NewRentalStatusID is not null
	and dest.ListingStatusID is null
order by l.ListingID, c.CreateDate

--AgentOwned
insert into Jaguar_v2_Prototype.dbo.tbListingAttribute (ListingID, AttributeValueID)
select distinct l.ListingID, av.AttributeValueID
from Jaguar_v2_Prototype.dbo.tbWorkingSet w	
	inner join dbo.Sales b on w.WorkingID = b.ListingID
	inner join Jaguar_v2_Prototype.dbo.tbListing l on w.WorkingID = l.EtlID
	left join Jaguar_v2_Prototype.dbo.vwAttributeValues av on av.AttributeName = 'AgentOwned' and av.ValueName = (case b.AgentOwned when 0 then 'No' when 1 then 'Yes' end)
	left join Jaguar_v2_Prototype.dbo.tbListingAttribute dest on dest.ListingID = l.ListingID and dest.AttributeValueID = av.AttributeValueID
where dest.ListingID is null

--BuildingAllow
insert into Jaguar_v2_Prototype.dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
select distinct l.PropertyID, av.AttributeValueID
from Jaguar_v2_Prototype.dbo.tbWorkingSet w
	inner join dbo.Listings a on w.WorkingID = a.ListingID
	inner join dbo.Buildings b on a.BuildingID = b.BuildingID
	inner join dbo.mvaBuildingAllows f on b.BuildingID = f.KeyID
	inner join Jaguar_v2_Prototype.dbo.vwAttributeValues av on av.AttributeName = 'BuildingAllow' and f.ValueID = av.EtlID
	inner join Jaguar_v2_Prototype.dbo.vwPropertyListing l on a.ListingID = l.ListingEtlID
	left join Jaguar_v2_Prototype.dbo.tbPropertyAttribute dest on dest.PropertyID = l.PropertyID and dest.AttributeValueID = av.AttributeValueID
where dest.PropertyAttributeID is null
order by l.PropertyID

--BuildingNotAllow
insert into Jaguar_v2_Prototype.dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
select distinct l.PropertyID, av.AttributeValueID
from Jaguar_v2_Prototype.dbo.tbWorkingSet w
	inner join dbo.Listings a on w.WorkingID = a.ListingID
	inner join dbo.Buildings b on a.BuildingID = b.BuildingID
	inner join dbo.mvaBuildingNotAllows f on b.BuildingID = f.KeyID
	inner join Jaguar_v2_Prototype.dbo.vwAttributeValues av on av.AttributeName = 'BuildingNotAllow' and f.ValueID = av.EtlID
	inner join Jaguar_v2_Prototype.dbo.vwPropertyListing l on a.ListingID = l.ListingEtlID
	left join Jaguar_v2_Prototype.dbo.tbPropertyAttribute dest on dest.PropertyID = l.PropertyID and dest.AttributeValueID = av.AttributeValueID
where dest.PropertyAttributeID is null
order by l.PropertyID

--BuildingAmmenity
insert into Jaguar_v2_Prototype.dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
select distinct l.PropertyID, av.AttributeValueID
from Jaguar_v2_Prototype.dbo.tbWorkingSet w
	inner join dbo.Listings a on w.WorkingID = a.ListingID
	inner join dbo.Buildings b on a.BuildingID = b.BuildingID
	inner join dbo.mvaBuildingFeatures f on b.BuildingID = f.KeyID
	inner join Jaguar_v2_Prototype.dbo.vwAttributeValues av on av.AttributeName = 'BuildingAmenity' and f.ValueID = av.EtlID
	inner join Jaguar_v2_Prototype.dbo.vwPropertyListing l on a.ListingID = l.ListingEtlID
	left join Jaguar_v2_Prototype.dbo.tbPropertyAttribute dest on dest.PropertyID = l.PropertyID and dest.AttributeValueID = av.AttributeValueID
where dest.PropertyAttributeID is null
order by l.PropertyID

--BuildingPersonnel
insert into Jaguar_v2_Prototype.dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
select distinct l.PropertyID, av.AttributeValueID
from Jaguar_v2_Prototype.dbo.tbWorkingSet w
	inner join dbo.Listings a on w.WorkingID = a.ListingID
	inner join dbo.BuildingPersonnel b on a.BuildingID = b.BuildingID
	inner join Jaguar_v2_Prototype.dbo.vwAttributeValues av on av.AttributeName = 'BuildingPersonnel' and b.BuildingPersonnelTypeID = av.EtlID
	inner join Jaguar_v2_Prototype.dbo.vwPropertyListing l on a.ListingID = l.ListingEtlID
	left join Jaguar_v2_Prototype.dbo.tbPropertyAttribute dest on dest.PropertyID = l.PropertyID and dest.AttributeValueID = av.AttributeValueID
where dest.PropertyAttributeID is null
order by l.PropertyID

--Exposure
insert into Jaguar_v2_Prototype.dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
select distinct l.PropertyID, av.AttributeValueID
from Jaguar_v2_Prototype.dbo.tbWorkingSet w
	--inner join dbo.Listings a on w.WorkingID = a.ListingID
	inner join dbo.Homes b on w.WorkingID = b.ListingID
	inner join dbo.mvaHomeExposureTypes f on b.HomeID = f.KeyID
	inner join Jaguar_v2_Prototype.dbo.vwAttributeValues av on av.AttributeName = 'Exposure' and f.ValueID = av.EtlID
	inner join Jaguar_v2_Prototype.dbo.vwPropertyListing l on w.WorkingID = l.ListingEtlID
	left join Jaguar_v2_Prototype.dbo.tbPropertyAttribute dest on dest.PropertyID = l.PropertyID and dest.AttributeValueID = av.AttributeValueID
where dest.PropertyAttributeID is null
order by l.PropertyID