use Jaguar_v1_Production

--------insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
--------select *
--------from openrowset('Microsoft.ACE.OLEDB.12.0',
--------	'Excel 12.0;Database=C:\Users\tFellini\OneDrive\Squarefoot\attributes.xlsx', 'select rents from [Rents$]') a
--------left join Jaguar_v2_Prototype.dbo.tbValue on Rents = ValueName
--------where ValueName is null
--------order by Rents

--insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
--select Value
--from openrowset('Microsoft.ACE.OLEDB.12.0',
--	'Excel 12.0;Database=C:\Users\tFellini\OneDrive\Squarefoot\attributes.xlsx', 'select * from [Rooms$] order by Sort') a
--left join Jaguar_v2_Prototype.dbo.tbValue on Value = ValueName
--where ValueName is null

--insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
--select Value
--from openrowset('Microsoft.ACE.OLEDB.12.0',
--	'Excel 12.0;Database=C:\Users\tFellini\OneDrive\Squarefoot\attributes.xlsx', 'select * from [DetailType$]') a
--left join Jaguar_v2_Prototype.dbo.tbValue on Value = ValueName
--where ValueName is null

--insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
--select ListItem
--from Jaguar_v2_Prototype.dbo.tfCommaListToTable('Sale,Rental,Commercial,Residence,Active,Inactive,Land,Structure,Yes,No,Maybe,Unknown,Excellent,Fair,Good,Normal,Poor,Image,Video,Building,SingleFamily,MultiFamily,Unit')
--left join Jaguar_v2_Prototype.dbo.tbValue on ListItem = ValueName
--where ValueID is null
--order by ListItem

--insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
--select ListItem
--from Jaguar_v2_Prototype.dbo.tfCommaListToTable('Cooperative,Condo,Condop,Rental,Fee Simple')
--left join Jaguar_v2_Prototype.dbo.tbValue on ListItem = ValueName
--where ValueID is null
--order by ListItem

---- Data from Excel
----
insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
select distinct Value
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Users\tFellini\OneDrive\Squarefoot\attributes.xlsx', 'select * from [Values$]') a
left join Jaguar_v2_Prototype.dbo.tbValue on Value = ValueName
where ValueName is null

---- Data from 1.5
----
insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
select Name
from dbo.ListingProviders
left join Jaguar_v2_Prototype.dbo.tbValue on Name = ValueName
where ValueID is null
order by Name

insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
select LongName
from dbo.attrListingTypes
left join Jaguar_v2_Prototype.dbo.tbValue on LongName = ValueName
where ValueID is null
order by LongName

insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
select distinct LongName
from dbo.attrBuildingTypes
left join Jaguar_v2_Prototype.dbo.tbValue on LongName = ValueName
where charindex('/', LongName) = 0
	and ValueID is null
order by LongName

insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
select distinct LongName
from dbo.attrWarTypes
left join Jaguar_v2_Prototype.dbo.tbValue on LongName = ValueName
where ValueID is null
order by LongName

insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
select distinct LongName
from dbo.attrBuildingPersonnelTypes
left join Jaguar_v2_Prototype.dbo.tbValue on LongName = ValueName
where ValueID is null
order by LongName

insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
select distinct LongName
from dbo.attrBuildingNotAllows
left join Jaguar_v2_Prototype.dbo.tbValue on LongName = ValueName
where ValueID is null
order by LongName

insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
select distinct LongName
from dbo.attrBuildingAllows
left join Jaguar_v2_Prototype.dbo.tbValue on LongName = ValueName
where ValueID is null
order by LongName

insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
select distinct LongName
from dbo.attrBuildingFeatures
left join Jaguar_v2_Prototype.dbo.tbValue on LongName = ValueName
where ValueID is null
order by LongName


insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
select LongName
from dbo.attrSaleStatuses
left join Jaguar_v2_Prototype.dbo.tbValue on LongName = ValueName
where ValueID is null
union
select LongName
from dbo.attrRentalStatuses
left join Jaguar_v2_Prototype.dbo.tbValue on LongName = ValueName
where ValueID is null
order by LongName

insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
select LongName
from dbo.attrHomeExposureTypes
left join Jaguar_v2_Prototype.dbo.tbValue on LongName = ValueName
where ValueID is null
order by LongName

insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
select LongName
from dbo.attrHomeOutdoorSpaceTypes
left join Jaguar_v2_Prototype.dbo.tbValue on LongName = ValueName
where ValueID is null
order by LongName

insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
select LongName
from dbo.attrListingViews
left join Jaguar_v2_Prototype.dbo.tbValue on LongName = ValueName
where ValueID is null
order by LongName

insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
select LongName
from dbo.attrExteriorMaterialTypes
left join Jaguar_v2_Prototype.dbo.tbValue on LongName = ValueName
where ValueID is null
order by LongName

insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
select LongName
from dbo.attrApartmentStyles
left join Jaguar_v2_Prototype.dbo.tbValue on LongName = ValueName
where ValueID is null
order by LongName

insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
select LongName
from dbo.attrAreas
left join Jaguar_v2_Prototype.dbo.tbValue on LongName = ValueName
where ValueID is null
order by LongName

insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
select LongName
from dbo.attrNeighborhoods
left join Jaguar_v2_Prototype.dbo.tbValue on LongName = ValueName
where ValueID is null
order by LongName

insert into Jaguar_v2_Prototype.dbo.tbValue (ValueName)
select LongName
from dbo.attrPictureTypes
left join Jaguar_v2_Prototype.dbo.tbValue on LongName = ValueName
where ValueID is null
order by LongName