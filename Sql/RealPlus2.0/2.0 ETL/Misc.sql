--select COlumn_name, AttributeName
select c.COLUMN_NAME, a.AttributeName,
'------tbPropertyAttributeNumeric (' + a.AttributeName + ')
--insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
select distinct p.PropertyID, a.AttributeID, lf.' + c.COLUMN_NAME + '
from dbo.tbProperty p
	left join dbo.tbListingSaleFull lf on p.EtlID = lf.ListingID
	left join dbo.tbAttribute a on a.AttributeName = ' + char(39) + a.AttributeName + char(39) + '
	left join dbo.tbPropertyAttributeNumeric dest on dest.PropertyID = p.PropertyID and dest.AttributeID = a.AttributeID and dest.AttributeValue = lf.' + c.COLUMN_NAME + '
where lf.' + c.COLUMN_NAME + ' is not null
	and dest.PropertyAttributeNumericID is null'
,
'------tbListingAttributeNumeric (' + a.AttributeName + ')
--insert into dbo.tbListingAttributeNumeric (ListingID, AttributeID, AttributeValue)
select distinct l.ListingID, a.AttributeID, lf.' + c.COLUMN_NAME + '
from dbo.tbListing l
	left join dbo.tbListingSaleFull lf on l.ListingID = lf.ListingID
	left join dbo.tbAttribute a on a.AttributeName = ' + char(39) + a.AttributeName + char(39) + '
	left join dbo.tbListingAttributeNumeric dest on dest.ListingID = l.ListingID and dest.AttributeID = a.AttributeID and dest.AttributeValue = lf.' + c.COLUMN_NAME + '
where lf.' + c.COLUMN_NAME + ' is not null
	and dest.ListingAttributeNumericID is null'
from information_schema.COLUMNS c 
left join dbo.tbAttribute a on c.COLUMN_NAME = a.AttributeName or c.COLUMN_NAME = a.AttributeCode
where c.table_name = 'tblistingsalefull'
	--and a.AttributeName is not null
order by c.COLUMN_NAME


select distinct
	l.ListingID
	, lc.ValueName 'ListingCategory'
	, lt.ValueName 'ListingType'
	, l.InitialPrice
	, cp.ListingPrice 'CurrentPrice'
	, cs.ValueName 'CurrentStatus'
	, bc.AttributeValue 'BuyerCommission'
	, sc.AttributeValue 'SellerCommission'
	, tc.AttributeValue 'TotalCommission'
	, ln.AttributeValue 'ListingNumber'
	, lnr.AttributeValue 'ListingNumberReference'
	, l.ListDate
	, l.UpdateDateTime
	, l.CreateDateTime
from dbo.tbListing l
	left join dbo.vwAttributeValues lc on l.ListingCategoryID = lc.AttributeValueID
	left join dbo.vwAttributeValues lt on l.ListingCategoryID = lt.AttributeValueID
	left join dbo.vwCurrentListingPrice cp on l.ListingID = cp.ListingID
	left join dbo.vwCurrentListingStatus cs on l.ListingID = cs.ListingID
	left join dbo.tfListingAttribute('BuyerCommission') bc on l.ListingID = bc.ListingID
	left join dbo.tfListingAttribute('SellerCommission') sc on l.ListingID = sc.ListingID
	left join dbo.tfListingAttribute('TotalCommission') tc on l.ListingID = tc.ListingID
	left join dbo.tfListingAttribute('ListingNumber') ln on l.ListingID = ln.ListingID
	left join dbo.tfListingAttribute('ListingNumberReference') lnr on l.ListingID = lnr.ListingID
order by l.ListingID