use Jaguar_v2_WiP

insert into dbo.tbUserAccount (PersonID, AccountName, PasswordEncrypted, UserAccountStatusID, CreateDateTime, UpdateDateTime)
select distinct a.PersonID, b.UserID, b.Password, d.AttributeValueID, b.CreateDate, b.ModDate
from dbo.tbPerson a
inner join Jaguar_v1_Production.dbo.People b on a.EtlID = b.PersonID
left join dbo.tbUserAccount c on a.PersonID = c.PersonID
, (select AttributeValueID from dbo.vwAttributeValues where AttributeName = 'Status' and ValueName = 'Active') d
where c.PersonID is null