use Jaguar_v2_WiP  --from Jaguar_v2_Prototype.dbo.tfCommaListToTable('Bathrooms,Bedrooms,')

insert into dbo.tbAttribute (AttributeName)
select distinct Attribute
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Users\tFellini\OneDrive\Squarefoot\attributes.xlsx', 'select * from [Attributes$]') a
left join dbo.tbAttribute on Attribute = AttributeName
where AttributeName is null
order by Attribute
