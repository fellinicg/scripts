use Jaguar_v2_WiP

insert into dbo.tbUnit (PropertyID, UnitTypeID)
select p.PropertyID, ut.AttributeValueID
from dbo.vwPropertyInfo p
	left join dbo.vwAttributeValues ut on ut.AttributeName = 'UnitType' and ut.ValueName = 'Residence'
	left join dbo.tbUnit u on p.PropertyID = u.PropertyID
where 
	p.PropertyClass = 'Structure'
	and 
	u.UnitID is null


--select UnitNumberFull
--from Jaguar_v2_Prototype.dbo.tbWorkingSet w
--	inner join dbo.Listings l on w.WorkingID = l.ListingID
--	inner join dbo.buildings a on a.BuildingID = l.BuildingID






--Jaguar_v2_Prototype.dbo.tbUnit