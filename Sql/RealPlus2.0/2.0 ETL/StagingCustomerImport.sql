-- Preload file work
-- =================
-- 1. Combine the files into one file named FullCustomerList.csv (or change below), and copy to C:\Feeds on .122 server.
-- 2. Make sure column names match the queries below.
-- 3. Add _BHS or __HALS to the Agent ID column entries to ensure (hopefully) uniqueness for Agent ID
-- 4. Add the CompanyNumber column and assign the following values depending on the record source:
--    a. BHS = 1000000 
--    b. HALS = 2000000

-- Delete temporary table
if Object_ID('tempdb.dbo.#Customers') is not null drop table #Customers

-- Insert contents of file on .122 server into temporary table
select * 
into #Customers
--from openquery([54.208.100.122], 'select * from openrowset(''Microsoft.ACE.OLEDB.12.0'', ''Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents'', ''select * from FullCustomerList.csv where customerid is not null'')')
from openquery([54.208.100.122], 'select * from openrowset(''Microsoft.ACE.OLEDB.12.0'', ''Text;Database=C:\Feeds'', ''select * from FullCustomerList.csv where customerid is not null'')')

-- Alter temporary table to add EtlID to make a unique id as CustomerID may not be unique across companies
alter table #Customers add EtlID int null

-- Create EtlID using CompanyNumber (Created by me) + CustomerID
update #Customers set EtlID = CompanyNumber + CustomerID

-- Insert customers into tbPerson
insert into dbo.tbPerson (FirstGivenName, SecondGivenName, FamilyName, FullName, EtlID)
select 
	FirstName
	, LastName
	, LastName
	, ltrim(rtrim(ltrim(rtrim(isnull(FirstName, ''))) + ' ' + ltrim(rtrim(isnull(LastName, '')))))
	, a.EtlID
from #Customers a
left join dbo.tbPerson p on a.EtlID = p.EtlID
where p.PersonID is null

-- Insert addresses into tbPostalAddress
insert into dbo.tbPostalAddress(Line1, Line2, CityTown, StateProvince, PostalCode)
select distinct
	a.Address
	, a.Apt
	, a.City
	, a.State
	, a.Zipcode
from #Customers a
inner join dbo.tbPerson p on a.EtlID = p.EtlID
left join dbo.tbPostalAddress pa on a.Address = pa.Line1 and isnull(a.Apt, '') = isnull(pa.Line2, '') and isnull(a.City, '') = isnull(pa.CityTown, '') and isnull(a.State, '') = isnull(pa.StateProvince, '') and isnull(a.Zipcode, '') = isnull(pa.PostalCode, '')
where a.Address <> 'NULL'
	and pa.PostalAddressID is null

-- Assign postal address to customer as AddressType home
insert into dbo.tbPersonAddress (PersonID, PostalAddressID, AddressTypeID)
select 
	p.PersonID
	, pa.PostalAddressID
	, 239 'AddressTypeID'
from #Customers a
inner join dbo.tbPerson p on a.EtlID = p.EtlID
inner join dbo.tbPostalAddress pa on a.Address = pa.Line1 and isnull(a.Apt, '') = isnull(pa.Line2, '') and isnull(a.City, '') = isnull(pa.CityTown, '') and isnull(a.State, '') = isnull(pa.StateProvince, '') and isnull(a.Zipcode, '') = isnull(pa.PostalCode, '')
left join dbo.tbPersonAddress d on p.PersonID = d.PersonID and pa.PostalAddressID = d.PostalAddressID and d.AddressTypeID = 239
where a.Address <> 'NULL'
	and d.PersonAddressID is null

-- Insert Email into tbPersonEmail as EmailType home
insert into dbo.tbPersonEmail(PersonID, EmailTypeID, EmailAddress)
select 
	p.PersonID
	, 249 'EmailTypeID'
	, a.Email
from #Customers a
inner join dbo.tbPerson p on a.EtlID = p.EtlID
left join dbo.tbPersonEmail pe on p.PersonID = pe.PersonID and pe.EmailTypeID = 249
where a.Email <> 'NULL'
	and pe.PersonEmailID is null

-- Insert AdditionalEmail into tbPersonEmail as EmailType other
insert into dbo.tbPersonEmail(PersonID, EmailTypeID, EmailAddress)
select 
	p.PersonID
	, 252 'EmailTypeID'
	, a.AdditionalEmail
from #Customers a
inner join dbo.tbPerson p on a.EtlID = p.EtlID
left join dbo.tbPersonEmail pe on p.PersonID = pe.PersonID and pe.EmailTypeID = 252
where a.AdditionalEmail <> 'NULL'
	and pe.PersonEmailID is null

-- Insert PrimaryPhone into tbPersonPhone as PhoneType other and IsPreferred
insert into dbo.tbPersonPhone(PersonID, PhoneTypeID, FullPhoneNumber, IsPreferred)
select 
	p.PersonID
	, 257 'phonetypeID'
	, a.PrimaryPhone
	, 1
from #Customers a
inner join dbo.tbPerson p on a.EtlID = p.EtlID
left join dbo.tbPersonPhone d on p.PersonID = d.PersonID and d.PhoneTypeID = 257
where a.PrimaryPhone <> 'NULL'
	and d.PersonPhoneID is null

-- Insert OfficePhone into tbPersonPhone as PhoneType office
insert into dbo.tbPersonPhone(PersonID, PhoneTypeID, FullPhoneNumber)
select 
	p.PersonID
	, 256 'phonetypeID'
	, a.OfficePhone
from #Customers a
inner join dbo.tbPerson p on a.EtlID = p.EtlID
left join dbo.tbPersonPhone d on p.PersonID = d.PersonID and d.PhoneTypeID = 256
where a.OfficePhone <> 'NULL'
	and d.PersonPhoneID is null

-- Insert HomePhone into tbPersonPhone as PhoneType home
insert into dbo.tbPersonPhone(PersonID, PhoneTypeID, FullPhoneNumber)
select 
	p.PersonID
	, 254 'phonetypeID'
	, a.HomePhone
from #Customers a
inner join dbo.tbPerson p on a.EtlID = p.EtlID
left join dbo.tbPersonPhone d on p.PersonID = d.PersonID and d.PhoneTypeID = 254
where a.HomePhone <> 'NULL'
	and d.PersonPhoneID is null

-- Insert CellPhone into tbPersonPhone as PhoneType mobile
insert into dbo.tbPersonPhone(PersonID, PhoneTypeID, FullPhoneNumber)
select 
	p.PersonID
	, 255 'phonetypeID'
	, a.CellPhone
from #Customers a
inner join dbo.tbPerson p on a.EtlID = p.EtlID
left join dbo.tbPersonPhone d on p.PersonID = d.PersonID and d.PhoneTypeID = 255
where a.CellPhone <> 'NULL'
	and d.PersonPhoneID is null

-- Insert agent's customers into tbCustomer
insert into dbo.tbCustomer(CustomerID, AgentID, StatusID)
select 
	c.PersonID
	, a.AgentID
	, case f.[ActiveInactive] when 'I' then 167 else 166 end
from #Customers f
inner join dbo.tbPerson c on f.EtlID = c.EtlID
inner join dbo.tbAgent a on f.[Agent ID] = a.AgentNumber
left join dbo.tbCustomer d on c.PersonID = d.CustomerID and a.AgentID = d.AgentID
where d.CustomerID is null

-- Insert notes into tbDetail
insert into dbo.tbDetail(DetailTypeID, DetailText, CreateID, UpdateID, EtlID)
select 
	3719
	, f.Notes
	, a.AgentID
	, a.AgentID
	, f.EtlID
from #Customers f
inner join dbo.tbPerson c on f.EtlID = c.EtlID
inner join dbo.tbAgent a on f.[Agent ID] = a.AgentNumber
left join dbo.tbDetail d on f.EtlID = d.EtlID and d.DetailTypeID = 3719 and d.CreateID = a.AgentID
where f.Notes <> 'NULL'
	and d.DetailID is null

-- Insert reference to tbDetail via AttributeValue = DetailID into tbPersonAttributeText
insert into dbo.tbPersonAttributeText(PersonID, AttributeID, AttributeValue, CreateID, UpdateID)
select 
	c.PersonID
	, 1227
	, d.DetailID
	, d.CreateID
	, d.CreateID
from #Customers f
inner join dbo.tbDetail d on f.Etlid = d.EtlID and d.DetailTypeID = 3719
inner join dbo.tbPerson c on f.EtlID = c.EtlID
left join dbo.tbPersonAttributeText pa on d.DetailID = pa.AttributeValue and pa.AttributeID = 1227 and c.PersonID = pa.PersonID
where pa.PersonAttributeTextID is null