use Jaguar_v1_Production
insert into Jaguar_v2_WiP.dbo.tbListingSaleFull (ListingID, SaleID, HomeID, BuildingID, ListingAgentID, ListingOfficeID, ListingNumber, ListingNumberReference, ListingType, ListingProvider, BuildingName, FullAddress, Zip, Neighborhood, UnitNumber, SubUnitNumber, Acres, Plat, Parcel, LegalDescription, Lot, Block, LotSize, FrontFootage, DepthFootage, NextReviewDate, ListedDate, IsTownhouse, SaleStatus, SaleSubStatus, SalePrice, OriginalPrice, HotSheetDate, AvailableDate, ExpirationDate, ShareDate, AccumulatedDaysActive, BuyerCommission, SellerCommission, TotalCommission, AgentOwned, AgentInstructions, ShowingInstructions, WebHeadline, WebDescription, TotalMonthlyFees, AssociationFee, AssessedValue, Assessments, Terms, Taxes, TaxYear, Included, Excluded, ClosingDate, ClosingPrice, ClosingPriceApproximate, ClosingAgentID, ClosingOfficeID, ClosingComments, ClosingConcessions, ClosingListingProviderID, FirstDateToShowing, HotSheetReason, ClosingPriceAccuracy, HomeStyle, HomeOwnershipType, YearBuilt, Bedrooms, Bathrooms, HalfBaths, QuarterBaths, TotalRooms, TotalSqFt, Stories, Fireplaces, DecorativeFireplaces, WorkingFireplaces, GasFireplaces, NewDevelopmentID, BuildingManagementAgentID, Phone, BuildingLotSize, BuildingType, BuildingOwnershipType, BuildingAirConditionType, BuildingNewConstructionType, TotalUnits, TotalOwnedUnits, TotalRegulatedUnits, TotalRentedUnits, TotalSponsoredUnits, BuildingTotalFloors, BuildingTotalRooms, BuildingYearBuilt, YearRenovated, YearIncorporated, FinancingAllowedPercent, UtilitiesIncluded, Elevators, PercentSold, AverageInterestRate, YearMortgageDue, BuildingMortgage, TotalShares, ReserveFund, BoardApproval, BoardRemarks, Book, Page, BldgLot, BldgBlock, TaxDeduction, FlipTax, WarType, SecurityFeatures, Comments, Conversion, Active, ManagedByParentOfficeID, BrokerSpecialist, BrokerSpecialistPhone, BldgAssessments, BuildingNumber, BuildingUtilitiesIncluded)
select --top 10000
	Listings.ListingID
	, Sales.SaleID
	, Homes.HomeID
	, Buildings.BuildingID
	, Listings.ListingAgentID
	, Listings.ListingOfficeID
	, Listings.ListingNumber
	, Listings.ListingNumberReference
	, attrListingTypes.LongName as ListingType
	, ListingProviders.Name as ListingProvider
	, Buildings.BuildingName
	, Addresses.FullAddress
	, Addresses.Zip
	, attrNeighborhoods.LongName as Neighborhood
	, Listings.UnitNumber
	, Listings.SubUnitNumber
	, Listings.Acres
	, Listings.Plat
	, Listings.Parcel
	, Listings.LegalDescription
	, Listings.Lot
	, Listings.Block
	, Listings.LotSize
	, Listings.FrontFootage
	, Listings.DepthFootage
	, Listings.NextReviewDate
	, Listings.ListedDate
	, Listings.IsTownhouse
	, attrSaleStatuses.LongName as SaleStatus
	, attrSaleContingencyTypes.LongName as SaleSubStatus
	, Sales.SalePrice
	, Sales.OriginalPrice
	, Sales.HotSheetDate
	, Sales.AvailableDate
	, Sales.ExpirationDate
	, Sales.ShareDate
	, Sales.AccumulatedDaysActive
	, Sales.BuyerCommission
	, Sales.SellerCommission
	, Sales.TotalCommission
	, Sales.AgentOwned
	, Sales.AgentInstructions
	, Sales.ShowingInstructions
	, Sales.WebHeadline
	, Sales.WebDescription
	, Sales.TotalMonthlyFees
	, Sales.AssociationFee
	, Sales.AssessedValue
	, Sales.Assessments
	, Sales.Terms
	, Sales.Taxes
	, Sales.TaxYear
	, Sales.Included
	, Sales.Excluded
	, Sales.ClosingDate
	, Sales.ClosingPrice
	, Sales.ClosingPriceApproximate
	, Sales.ClosingAgentID
	, Sales.ClosingOfficeID
	, Sales.ClosingComments
	, Sales.ClosingConcessions
	, Sales.ClosingListingProviderID
	, Sales.FirstDateToShowing
	, Sales.HotSheetReason
	, Sales.ClosingPriceAccuracy
	, attrHomeStyles.LongName as HomeStyle
	, attrOwnershipTypes.LongName as HomeOwnershipType
	, Homes.YearBuilt
	, Homes.Bedrooms
	, Homes.Bathrooms
	, Homes.HalfBaths
	, Homes.QuarterBaths
	, Homes.TotalRooms
	, Homes.TotalSqFt
	, Homes.Stories
	, Homes.Fireplaces
	, Homes.DecorativeFireplaces
	, Homes.WorkingFireplaces
	, Homes.GasFireplaces
	, Buildings.NewDevelopmentID
	, Buildings.ManagementAgentID as BuildingManagementAgentID
	, Buildings.Phone
	, Buildings.LotSize as BuildingLotSize
	, attrBuildingTypes.LongName as BuildingType
	, attrOwnershipTypes_1.LongName as BuildingOwnershipType
	, attrBuildingAirConditioningTypes.LongName as BuildingAirConditionType
	, attrBuildingNewConstructionTypes.LongName as BuildingNewConstructionType
	, Buildings.TotalUnits
	, Buildings.TotalOwnedUnits
	, Buildings.TotalRegulatedUnits
	, Buildings.TotalRentedUnits
	, Buildings.TotalSponsoredUnits
	, Buildings.TotalFloors as BuildingTotalFloors
	, Buildings.TotalRooms as BuildingTotalRooms
	, Buildings.YearBuilt as BuildingYearBuilt
	, Buildings.YearRenovated
	, Buildings.YearIncorporated
	, Buildings.FinancingAllowedPercent
	, Buildings.UtilitiesIncluded
	, Buildings.Elevators
	, Buildings.PercentSold
	, Buildings.AverageInterestRate
	, Buildings.YearMortgageDue
	, Buildings.BuildingMortgage
	, Buildings.TotalShares
	, Buildings.ReserveFund
	, Buildings.BoardApproval
	, Buildings.BoardRemarks
	, Buildings.Book
	, Buildings.Page
	, Buildings.Lot as BldgLot
	, Buildings.Block as BldgBlock
	, Buildings.TaxDeduction
	, Buildings.FlipTax
	, attrWarTypes.LongName as WarType
	, Buildings.SecurityFeatures
	, Buildings.Comments
	, Buildings.Conversion
	, Buildings.Active
	, Buildings.ManagedByParentOfficeID
	, Buildings.BrokerSpecialist
	, Buildings.BrokerSpecialistPhone
	, Buildings.Assessments as BldgAssessments
	, Buildings.BuildingNumber
	, attrBuildingUtilitiesIncluded.LongName as BuildingUtilitiesIncluded
--into Jaguar_v2_WiP.dbo.tbListingSaleFull
from 
	Listings inner join
    Sales on Listings.ListingID = Sales.ListingID inner join
    Homes on Listings.ListingID = Homes.ListingID inner join
    Buildings on Listings.BuildingID = Buildings.BuildingID inner join
	Addresses on Buildings.BuildingID = Addresses.BuildingID and Addresses.ListingID is null left join
	attrNeighborhoods on Addresses.NeighborhoodID = attrNeighborhoods.ID inner join
    attrListingTypes on Listings.ListingTypeID = attrListingTypes.ID inner join
    ListingProviders on Listings.ListingProviderID = ListingProviders.ListingProviderID inner join
    attrSaleStatuses on Sales.SaleStatusID = attrSaleStatuses.ID left outer join
    attrSaleContingencyTypes on Sales.SaleContingencyTypeID = attrSaleContingencyTypes.ID left outer join
    attrHomeStyles on Homes.HomeStyleID = attrHomeStyles.ID left outer join
    attrOwnershipTypes on Homes.OwnershipTypeID = attrOwnershipTypes.ID left outer join
    attrBuildingTypes on Buildings.BuildingTypeID = attrBuildingTypes.ID left outer join
    attrOwnershipTypes as attrOwnershipTypes_1 on Buildings.OwnershipTypeID = attrOwnershipTypes_1.ID left outer join
    attrBuildingAirConditioningTypes on Buildings.BuildingAirConditioningTypeID = attrBuildingAirConditioningTypes.ID left outer join
    attrBuildingNewConstructionTypes on Buildings.BuildingNewConstructionTypeID = attrBuildingNewConstructionTypes.ID left outer join
    attrWarTypes on Buildings.WarTypeID = attrWarTypes.ID left outer join
    attrBuildingUtilitiesIncluded on Buildings.UtilitiesIncludedID = attrBuildingUtilitiesIncluded.ID
where     (not (Buildings.BuildingName is null)) and (not (Listings.UnitNumber is null))
--order by ListedDate desc