use Jaguar_v2_WiP

insert into dbo.tbPostalCode (PostalCode, CityTown, StateProvince, Country)
select distinct
	replicate('0', 5-len(zip)) + convert(varchar, zip) PostalCode
	, a.Primary_city
	, a.state
	, a.country
--	, latitude
--	, longitude
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Users\tFellini\OneDrive\Squarefoot\zip_code_database.xls', 'select * from [zip_code_database$]') a
left join dbo.tbPostalCode b on replicate('0', 5-len(a.zip)) + convert(varchar, a.zip) = b.PostalCode
where a.decommissioned = 0
and b.PostalCodeID is null

--insert into dbo.tbPostalAddress (Number, Street, Line1, PostalCode, Country, Latitude, Longitude)
--select --distinct
--	Number
--	, street
--	, ltrim(convert(varchar, isnull(number, '')) + ' ' + isnull(street, ''))
--	, postcode
--	, 'US'
--	, lat
--	, lon
--from openrowset('Microsoft.ACE.OLEDB.12.0',
--	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot',
--	'select * from statewide.csv')
--where 
--lat = 40.6991833	
--and
--lon = -73.8424099