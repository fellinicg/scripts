-- Insert customers into tbPerson
insert into dbo.tbPerson (FirstGivenName, SecondGivenName, FamilyName, FullName, EtlID)
select 
	FirstName
	, LastName
	, LastName
	, ltrim(rtrim(ltrim(rtrim(isnull(FirstName, ''))) + ' ' + ltrim(rtrim(isnull(LastName, '')))))
	, CustomerID
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents',
	'select * from CustomersRealplus.csv') a
left join dbo.tbPerson p on a.CustomerID = p.EtlID
where p.PersonID is null

-- Insert addresses into tbPostalAddress
insert into dbo.tbPostalAddress(Line1, Line2, CityTown, StateProvince, PostalCode)
select 
	a.HomeStreet
	, a.Apt
	, a.HomeCity
	, a.HomeState
	, a.HomePostalcode
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents',
	'select * from CustomersRealplus.csv') a
inner join dbo.tbPerson p on a.CustomerID = p.EtlID
left join dbo.tbPostalAddress pa on a.HomeStreet = pa.Line1 and isnull(a.Apt, '') = isnull(pa.Line2, '') and isnull(a.HomeCity, '') = isnull(pa.CityTown, '') and isnull(a.HomeState, '') = isnull(pa.StateProvince, '') and isnull(a.HomePostalcode, '') = isnull(pa.PostalCode, '')
where a.HomeStreet <> 'NULL'
	and pa.PostalAddressID is null

-- Assign postal address to customer
insert into dbo.tbPersonAddress (PersonID, PostalAddressID, AddressTypeID)
select 
	p.PersonID
	, pa.PostalAddressID
	, 239 'AddressTypeID'
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents',
	'select * from CustomersRealplus.csv') a
inner join dbo.tbPerson p on a.CustomerID = p.EtlID
inner join dbo.tbPostalAddress pa on a.HomeStreet = pa.Line1 and isnull(a.Apt, '') = isnull(pa.Line2, '') and isnull(a.HomeCity, '') = isnull(pa.CityTown, '') and isnull(a.HomeState, '') = isnull(pa.StateProvince, '') and isnull(a.HomePostalcode, '') = isnull(pa.PostalCode, '')
left join dbo.tbPersonAddress d on p.PersonID = d.PersonID and pa.PostalAddressID = d.PostalAddressID and d.AddressTypeID = 239
where a.HomeStreet <> 'NULL'
	and d.PersonAddressID is null

-- Insert Email into tbPersonEmail as EmailType home
insert into dbo.tbPersonEmail(PersonID, EmailTypeID, EmailAddress)
select 
	p.PersonID
	, 249 'EmailTypeID'
	, a.Email
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents',
	'select * from CustomersRealplus.csv') a
inner join dbo.tbPerson p on a.CustomerID = p.EtlID
left join dbo.tbPersonEmail pe on p.PersonID = pe.PersonID and pe.EmailTypeID = 249
where a.Email <> 'NULL'
	and pe.PersonEmailID is null

---- Insert PrimaryPhone into tbPersonPhone as PhoneType other and IsPreferred
--insert into dbo.tbPersonPhone(PersonID, PhoneTypeID, FullPhoneNumber, IsPreferred)
--select 
--	p.PersonID
--	, 257 'phonetypeID'
--	, a.PrimaryPhone
--	, 1
--from openrowset('Microsoft.ACE.OLEDB.12.0',
--	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents',
--	'select * from CustomersRealplus.csv') a
--inner join dbo.tbPerson p on a.CustomerID = p.EtlID
--left join dbo.tbPersonPhone d on p.PersonID = d.PersonID and d.PhoneTypeID = 257
--where a.PrimaryPhone <> 'NULL'
--	and d.PersonPhoneID is null

-- Insert OfficePhone into tbPersonPhone as PhoneType office
insert into dbo.tbPersonPhone(PersonID, PhoneTypeID, FullPhoneNumber)
select 
	p.PersonID
	, 256 'phonetypeID'
	, a.OfficePhone
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents',
	'select * from CustomersRealplus.csv') a
inner join dbo.tbPerson p on a.CustomerID = p.EtlID
left join dbo.tbPersonPhone d on p.PersonID = d.PersonID and d.PhoneTypeID = 256
where a.OfficePhone <> 'NULL'
	and d.PersonPhoneID is null

-- Insert HomePhone into tbPersonPhone as PhoneType home
insert into dbo.tbPersonPhone(PersonID, PhoneTypeID, FullPhoneNumber)
select 
	p.PersonID
	, 254 'phonetypeID'
	, a.HomePhone
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents',
	'select * from CustomersRealplus.csv') a
inner join dbo.tbPerson p on a.CustomerID = p.EtlID
left join dbo.tbPersonPhone d on p.PersonID = d.PersonID and d.PhoneTypeID = 254
where a.HomePhone <> 'NULL'
	and d.PersonPhoneID is null

-- Insert CellPhone into tbPersonPhone as PhoneType mobile
insert into dbo.tbPersonPhone(PersonID, PhoneTypeID, FullPhoneNumber)
select 
	p.PersonID
	, 255 'phonetypeID'
	, a.CellPhone
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents',
	'select * from CustomersRealplus.csv') a
inner join dbo.tbPerson p on a.CustomerID = p.EtlID
left join dbo.tbPersonPhone d on p.PersonID = d.PersonID and d.PhoneTypeID = 255
where a.CellPhone <> 'NULL'
	and d.PersonPhoneID is null

-- Insert agent's customers into tbCustomer
insert into dbo.tbCustomer(CustomerID, AgentID, StatusID)
select 
	c.PersonID
	, a.AgentID
	, case f.[Active/Inactive] when 'I' then 167 else 166 end
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents',
	'select * from CustomersRealplus.csv') f
inner join dbo.tbPerson c on f.CustomerID = c.EtlID
inner join dbo.tbAgent a on f.AgentID = a.AgentNumber
left join dbo.tbCustomer d on c.PersonID = d.CustomerID and a.AgentID = d.AgentID
where d.CustomerID is null