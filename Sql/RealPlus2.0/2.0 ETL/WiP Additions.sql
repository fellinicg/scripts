select max(AttributeID) from dbo.tbAttribute

select max(AttributeID) from Jaguar_v2_Prototype2.dbo.tbAttribute

--insert into dbo.tbAttributeValue (AttributeID, ValueID)
select 
av.AttributeValueID
,av.AttributeID
,av.ValueID
from dbo.tbAttribute a
left join dbo.tbValue v on 1=1
left join dbo.tbAttributeValue av on a.AttributeID = av.AttributeID and  v.ValueID = av.ValueID
where 
	--av.AttributeValueID is null
	--and
	a.AttributeID in (5,28,32,75)
	and
	v.ValueName in ('Home','Mobile','Office','Other','Work')
order by a.AttributeID, v.ValueName

select distinct ',' + char(39) + ValueName + char(39)
from pvwAttributeValues
where AttributeName in ('AddressType','ContactType','EmailType','PhoneType')

select *
from dbo.tbValue
where ValueName in ('Home','Mobile','Office','Other','Work')

insert into dbo.tbValue (ValueName)
select ListItem from dbo.tfCommaListToTable('Mobile,Work')


insert into tbvalue(valuename)
select Listitem
from dbo.tfCommaListToTable('realtime,weekly,monthly,daily-sunday,daily-monday,daily-tuesday,daily-wednesday,daily-thursday,daily-friday,daily-saturday')

insert into dbo.tbAttribute (AttributeName)
values ('recent match settings')

--insert into dbo.tbAttributeValue (AttributeID, ValueID)
select a.AttributeID, b.ValueID
from dbo.tbAttribute a
, dbo.tbValue b 
where a.AttributeName = 'recent match settings'
and b.ValueName in (select listitem from dbo.tfCommaListToTable('realtime,weekly,monthly,daily-sunday,daily-monday,daily-tuesday,daily-wednesday,daily-thursday,daily-friday,daily-saturday'))

insert into dbo.tbAttributeValue (AttributeID, ValueID)
select a.AttributeID, b.ValueID
from dbo.tbAttribute a, dbo.tbValue b 
where a.AttributeName = 'recent match settings'
and b.ValueName in (select listitem from dbo.tfCommaListToTable('realtime,weekly,monthly,daily-sunday,daily-monday,daily-tuesday,daily-wednesday,daily-thursday,daily-friday,daily-saturday'))