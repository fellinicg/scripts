--insert into dbo.tbAttributeValueNumeric (AttributeValueID, NumericValue)
--select AttributeValueID, SortOrder-1
--from dbo.vwAttributeValues
--where AttributeName in ('Bathrooms','Bedrooms','HalfBathrooms','TotalRooms')
--order by AttributeID, sortorder

insert into dbo.tbAttributeValueNumeric (AttributeValueID, NumericValue)
select AttributeValueID, cast(cast(ValueName as money) as bigint)
where charindex('$', ValueName) > 0
