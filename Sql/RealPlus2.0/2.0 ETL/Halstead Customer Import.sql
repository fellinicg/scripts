-- Clear EtlID in tbPerson
update dbo.tbPerson set EtlID = null

-- Insert customers into tbPerson
insert into dbo.tbPerson (FirstGivenName, FamilyName, FullName, EtlID)
select 
	FirstName
	, LastName
	, ltrim(rtrim(FirstName)) + ' ' + ltrim(rtrim(LastName))
	, CustomerID
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents',
	'select * from Halstead_Customers_perChuck.csv') a
left join dbo.tbPerson p on a.CustomerID = p.EtlID
where p.PersonID is null

-- Insert addresses into tbPostalAddress
insert into dbo.tbPostalAddress(Line1, CityTown, StateProvince, PostalCode)
select 
	a.Address
	, a.City
	, a.State
	, a.Zipcode
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents',
	'select * from Halstead_Customers_perChuck.csv') a
inner join dbo.tbPerson p on a.CustomerID = p.EtlID
left join dbo.tbPostalAddress pa on a.Address = pa.Line1 and a.City = pa.CityTown and a.State = pa.StateProvince and a.Zipcode = pa.PostalCode
where a.Address <> 'NULL'
	and pa.PostalAddressID is null

-- Assign postal address to customer
insert into dbo.tbPersonAddress (PersonID, PostalAddressID, AddressTypeID)
select 
	p.PersonID
	, pa.PostalAddressID
	, 239 'AddressTypeID'
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents',
	'select * from Halstead_Customers_perChuck.csv') a
inner join dbo.tbPerson p on a.CustomerID = p.EtlID
inner join dbo.tbPostalAddress pa on a.Address = pa.Line1 and a.City = pa.CityTown and a.State = pa.StateProvince and a.Zipcode = pa.PostalCode
left join dbo.tbPersonAddress d on p.PersonID = d.PersonID and pa.PostalAddressID = d.PostalAddressID and d.AddressTypeID = 239
where a.Address <> 'NULL'
	and d.PersonAddressID is null

-- Insert Email into tbPersonEmail as EmailType home
insert into dbo.tbPersonEmail(PersonID, EmailTypeID, EmailAddress)
select 
	p.PersonID
	, 249 'EmailTypeID'
	, a.Email
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents',
	'select * from Halstead_Customers_perChuck.csv') a
inner join dbo.tbPerson p on a.CustomerID = p.EtlID
left join dbo.tbPersonEmail pe on p.PersonID = pe.PersonID and pe.EmailTypeID = 249
where a.Email <> 'NULL'
	and pe.PersonEmailID is null

-- Insert AdditionalEmail into tbPersonEmail as EmailType other
insert into dbo.tbPersonEmail(PersonID, EmailTypeID, EmailAddress)
select 
	p.PersonID
	, 252 'EmailTypeID'
	, a.AdditionalEmail
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents',
	'select * from Halstead_Customers_perChuck.csv') a
inner join dbo.tbPerson p on a.CustomerID = p.EtlID
left join dbo.tbPersonEmail pe on p.PersonID = pe.PersonID and pe.EmailTypeID = 252
where a.AdditionalEmail <> 'NULL'
	and pe.PersonEmailID is null

-- Insert PrimaryPhone into tbPersonPhone as PhoneType other and IsPreferred
insert into dbo.tbPersonPhone(PersonID, PhoneTypeID, FullPhoneNumber, IsPreferred)
select 
	p.PersonID
	, 257 'phonetypeID'
	, a.PrimaryPhone
	, 1
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents',
	'select * from Halstead_Customers_perChuck.csv') a
inner join dbo.tbPerson p on a.CustomerID = p.EtlID
left join dbo.tbPersonPhone d on p.PersonID = d.PersonID and d.PhoneTypeID = 257
where a.PrimaryPhone <> 'NULL'
	and d.PersonPhoneID is null

-- Insert OfficePhone into tbPersonPhone as PhoneType office
insert into dbo.tbPersonPhone(PersonID, PhoneTypeID, FullPhoneNumber)
select 
	p.PersonID
	, 256 'phonetypeID'
	, a.OfficePhone
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents',
	'select * from Halstead_Customers_perChuck.csv') a
inner join dbo.tbPerson p on a.CustomerID = p.EtlID
left join dbo.tbPersonPhone d on p.PersonID = d.PersonID and d.PhoneTypeID = 256
where a.OfficePhone <> 'NULL'
	and d.PersonPhoneID is null

-- Insert HomePhone into tbPersonPhone as PhoneType home
insert into dbo.tbPersonPhone(PersonID, PhoneTypeID, FullPhoneNumber)
select 
	p.PersonID
	, 254 'phonetypeID'
	, a.HomePhone
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents',
	'select * from Halstead_Customers_perChuck.csv') a
inner join dbo.tbPerson p on a.CustomerID = p.EtlID
left join dbo.tbPersonPhone d on p.PersonID = d.PersonID and d.PhoneTypeID = 254
where a.HomePhone <> 'NULL'
	and d.PersonPhoneID is null

-- Insert CellPhone into tbPersonPhone as PhoneType mobile
insert into dbo.tbPersonPhone(PersonID, PhoneTypeID, FullPhoneNumber)
select 
	p.PersonID
	, 255 'phonetypeID'
	, a.CellPhone
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents',
	'select * from Halstead_Customers_perChuck.csv') a
inner join dbo.tbPerson p on a.CustomerID = p.EtlID
left join dbo.tbPersonPhone d on p.PersonID = d.PersonID and d.PhoneTypeID = 255
where a.CellPhone <> 'NULL'
	and d.PersonPhoneID is null

-- Insert agent's customers into tbCustomer
insert into dbo.tbCustomer(CustomerID, AgentID, StatusID)
select 
	c.PersonID
	, a.PersonID
	, case f.ActiveInactive when 'I' then 167 else 166 end
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot\Documents',
	'select * from Halstead_Customers_perChuck.csv') f
inner join dbo.tbPerson c on f.CustomerID = c.EtlID
inner join dbo.tbPerson a on left(f.AgentFirstName, 4) = left(a.FirstGivenName, 4) and f.AgentLastName = a.FamilyName and a.SecondGivenName is not null and a.CreateDateTime > '10/31/2016'
left join dbo.tbCustomer d on c.PersonID = d.CustomerID and a.PersonID = d.AgentID
where d.CustomerID is null