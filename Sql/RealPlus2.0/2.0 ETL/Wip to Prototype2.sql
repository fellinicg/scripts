--set identity_insert Jaguar_v2_Prototype2.dbo.tbAttributeValue on

--insert into Jaguar_v2_Prototype2.dbo.tbAttributeValue (AttributeValueID, AttributeID, ValueID)
--select 
--av.AttributeValueID
--,av.AttributeID
--,av.ValueID
--from dbo.tbAttribute a
--left join dbo.tbValue v on 1=1
--left join dbo.tbAttributeValue av on a.AttributeID = av.AttributeID and  v.ValueID = av.ValueID
--where 
--	--av.AttributeValueID is null
--	--and
--	a.AttributeID in (5,28,32,75)
--	and
--	v.ValueName in ('Home','Mobile','Office','Other','Work')

----select x.AttributeValueID, x.AttributeID, x.ValueID
----from dbo.tbAttributeValue x  
----inner join dbo.tbAttribute a on x.AttributeID = a.AttributeID
----inner join dbo.tbValue b  on x.ValueID = b.ValueID
----where a.AttributeName = 'recent match settings'
----and b.ValueName in (select listitem from dbo.tfCommaListToTable('realtime,weekly,monthly,daily-sunday,daily-monday,daily-tuesday,daily-wednesday,daily-thursday,daily-friday,daily-saturday'))

--set identity_insert Jaguar_v2_Prototype2.dbo.tbAttributeValue off

--set identity_insert Jaguar_v2_Prototype2.dbo.tbAttribute on

--insert into Jaguar_v2_Prototype2.dbo.tbAttribute (AttributeID, AttributeName)
--select AttributeID, AttributeName
--from tbAttribute
--where AttributeName = 'recent match settings'

--set identity_insert Jaguar_v2_Prototype2.dbo.tbAttribute off

--set identity_insert Jaguar_v2_Prototype2.dbo.tbValue on

--insert into Jaguar_v2_Prototype2.dbo.tbValue (ValueID, ValueName)
--select valueID, valuename
--from tbvalue a
--inner join dbo.tfCommaListToTable('Mobile,Work') b on a.valuename = b.listitem

--set identity_insert Jaguar_v2_Prototype2.dbo.tbValue off