use Jaguar_v1_Production

---- From Excel
insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID)
select b.AttributeID, c.ValueID
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Users\tFellini\OneDrive\Squarefoot\attributes.xlsx', 'select * from [Values$] where Attribute is not null') a
inner join Jaguar_v2_Prototype.dbo.tbAttribute b on a.Attribute = b.AttributeName
inner join Jaguar_v2_Prototype.dbo.tbValue c on a.Value = c.ValueName
left join Jaguar_v2_Prototype.dbo.vwAttributeValues dest on a.Attribute = dest.AttributeName and a.Value = dest.ValueName
where dest.AttributeValueID is null

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID)
select a.AttributeID, b.ValueID
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Users\tFellini\OneDrive\Squarefoot\attributes.xlsx', 'select * from [Attributes$] where type = ''Boolean''') x
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on x.Attribute = a.AttributeName
join Jaguar_v2_Prototype.dbo.tbValue b on 1 = 1 and ValueName in ('Yes','No')
left join Jaguar_v2_Prototype.dbo.tbAttributeValue c on a.AttributeID = c.AttributeID and b.ValueID = c.ValueID
where c.AttributeValueID is null
order by Attribute

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID)
select distinct b.AttributeID, v.ValueID
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Users\tFellini\OneDrive\Squarefoot\attributes-KH.xlsx', 'select * from [SaleMiscFeatures$]') a
inner join Jaguar_v2_Prototype.dbo.tbValue v on a.Value = v.ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute b on 'SaleMiscFeatures' = b.AttributeName
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = b.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by b.AttributeID, v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID)
select distinct b.AttributeID, v.ValueID
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Users\tFellini\OneDrive\Squarefoot\attributes-KH.xlsx', 'select * from [RentMiscFeatures$]') a
inner join Jaguar_v2_Prototype.dbo.tbValue v on a.Value = v.ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute b on 'RentMiscFeatures' = b.AttributeName
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = b.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by b.AttributeID, v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID)
select distinct b.AttributeID, v.ValueID
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Users\tFellini\OneDrive\Squarefoot\attributes-KH.xlsx', 'select * from [SqFt$]') a
inner join Jaguar_v2_Prototype.dbo.tbValue v on a.Value = v.ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute b on 'MinSqFt' = b.AttributeName
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = b.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by b.AttributeID, v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID)
select distinct b.AttributeID, v.ValueID
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Users\tFellini\OneDrive\Squarefoot\attributes-KH.xlsx', 'select * from [SqFt$]') a
inner join Jaguar_v2_Prototype.dbo.tbValue v on a.Value = v.ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute b on 'MaxSqFt' = b.AttributeName
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = b.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by b.AttributeID, v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID)
select distinct b.AttributeID, v.ValueID
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot','select * from pricepersqft.txt') a
inner join Jaguar_v2_Prototype.dbo.tbValue v on a.Value = v.ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute b on 'MinPricePerSqFt' = b.AttributeName
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = b.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by b.AttributeID, v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID)
select distinct b.AttributeID, v.ValueID
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot','select * from pricepersqft.txt') a
inner join Jaguar_v2_Prototype.dbo.tbValue v on a.Value = v.ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute b on 'MaxPricePerSqFt' = b.AttributeName
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = b.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by b.AttributeID, v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID)
select distinct b.AttributeID, v.ValueID
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\Users\tFellini\OneDrive\Squarefoot','select * from MaxMonthlyExpense.txt') a
inner join Jaguar_v2_Prototype.dbo.tbValue v on a.Value = v.ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute b on 'MaxMonthlyExpense' = b.AttributeName
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = b.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by b.AttributeID, v.ValueID

--insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, SortOrder)
--select distinct b.AttributeID, v.ValueID, ROW_NUMBER() over (order by b.AttributeID, v.ValueID)
--from openrowset('Microsoft.ACE.OLEDB.12.0',
--	'Excel 12.0;Database=C:\Users\tFellini\OneDrive\Squarefoot\attributes.xlsx', 'select * from [Rooms$] order by Attribute, Sort') a
--inner join Jaguar_v2_Prototype.dbo.tbValue v on a.Value = v.ValueName
--inner join Jaguar_v2_Prototype.dbo.tbAttribute b on a.Attribute = b.AttributeName
--left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = b.AttributeID and va.ValueID = v.ValueID
--where va.AttributeValueID is null
--order by b.AttributeID, v.ValueID

------insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID)
------select distinct b.AttributeID, v.ValueID
------from openrowset('Microsoft.ACE.OLEDB.12.0',
------	'Excel 12.0;Database=C:\Users\tFellini\OneDrive\Squarefoot\attributes.xlsx', 'select * from [DetailType$]') a
------inner join Jaguar_v2_Prototype.dbo.tbValue v on a.Value = v.ValueName
------inner join Jaguar_v2_Prototype.dbo.tbAttribute b on AttributeName = 'DetailType' 
------left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = b.AttributeID and va.ValueID = v.ValueID
------where va.AttributeValueID is null
------order by b.AttributeID, v.ValueID

------select a.AttributeID, b.ValueID
------from Jaguar_v2_Prototype.dbo.tbAttribute a
------	join Jaguar_v2_Prototype.dbo.tbValue b on 1 = 1
------	left join Jaguar_v2_Prototype.dbo.tbAttributeValue c on a.AttributeID = c.AttributeID and b.ValueID = c.ValueID
------where charindex('Has', AttributeName) = 1 and ValueName in ('Yes','No')
------	and c.AttributeValueID is null


------insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID)
------select a.AttributeID, v.ValueID
------from Jaguar_v2_Prototype.dbo.tfCommaListToTable('Sale,Rental')
------left join Jaguar_v2_Prototype.dbo.tbValue v on ListItem = ValueName
------left join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'ListingCategory'
------left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
------where va.AttributeValueID is null
------order by v.ValueID

------insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display)
------select distinct a.AttributeID, v.ValueID, 1
------from Jaguar_v2_Prototype.dbo.tbValue v
------inner join Jaguar_v2_Prototype.dbo.tfCommaListToTable('Cooperative,Condo,Condop,Rental,Fee Simple') on v.ValueName = Listitem
------inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'OwnershipType' 
------left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
------where va.AttributeValueID is null
------order by v.ValueID

------insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID)
------select a.AttributeID, v.ValueID
------from Jaguar_v2_Prototype.dbo.tfCommaListToTable('Commercial,Residence')
------left join Jaguar_v2_Prototype.dbo.tbValue v on ListItem = ValueName
------left join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'UnitType'
------left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
------where va.AttributeValueID is null
------order by v.ValueID

------insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID)
------select a.AttributeID, v.ValueID
------from Jaguar_v2_Prototype.dbo.tfCommaListToTable('Land,Structure,SingleFamily,MultiFamily')
------left join Jaguar_v2_Prototype.dbo.tbValue v on ListItem = ValueName
------left join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'PropertyClass'
------left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
------where va.AttributeValueID is null
------order by v.ValueID

------insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID)
------select a.AttributeID, v.ValueID
------from Jaguar_v2_Prototype.dbo.tfCommaListToTable('Active,Inactive')
------left join Jaguar_v2_Prototype.dbo.tbValue v on ListItem = ValueName
------left join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'Status'
------left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
------where va.AttributeValueID is null
------order by v.ValueID

---- Miscellaneous
----
insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID)
select a.AttributeID, v.ValueID
from Jaguar_v2_Prototype.dbo.tfCommaListToTable('Home,Work,Office,Mobile,Other')
left join Jaguar_v2_Prototype.dbo.tbValue v on ListItem = ValueName
left join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'PhoneType'
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID)
select a.AttributeID, v.ValueID
from Jaguar_v2_Prototype.dbo.tfCommaListToTable('Home,Work,Office,Other') x
left join Jaguar_v2_Prototype.dbo.tbValue v on x.ListItem = v.ValueName
join Jaguar_v2_Prototype.dbo.tfCommaListToTable('ContactType,AddressType,EmailType') y on 1=1
left join Jaguar_v2_Prototype.dbo.tbAttribute a on a.AttributeName = y.ListItem
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by a.AttributeName, v.ValueName

---- 1.5
----
insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display, EtlID)
select a.AttributeID, v.ValueID, Active, ID
from dbo.attrListingTypes
inner join Jaguar_v2_Prototype.dbo.tbValue v on LongName = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'ListingType' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display, EtlID)
select a.AttributeID, v.ValueID, Active, ListingProviderID
from dbo.ListingProviders
inner join Jaguar_v2_Prototype.dbo.tbValue v on Name = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'ListingProvider' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display, EtlID)
select distinct a.AttributeID, v.ValueID, Active, ID
from dbo.attrBuildingTypes
inner join Jaguar_v2_Prototype.dbo.tbValue v on LongName = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'BuildingType' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display, EtlID)
select distinct a.AttributeID, v.ValueID, Active, ID
from dbo.attrWarTypes
inner join Jaguar_v2_Prototype.dbo.tbValue v on LongName = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'BuildingEra' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display, EtlID)
select distinct a.AttributeID, v.ValueID, Active, ID
from dbo.attrBuildingPersonnelTypes
inner join Jaguar_v2_Prototype.dbo.tbValue v on LongName = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'BuildingPersonnel' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display, EtlID)
select distinct a.AttributeID, v.ValueID, Active, ID
from dbo.attrBuildingNotAllows
inner join Jaguar_v2_Prototype.dbo.tbValue v on LongName = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'BuildingNotAllow' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display, EtlID)
select distinct a.AttributeID, v.ValueID, Active, ID
from dbo.attrBuildingAllows
inner join Jaguar_v2_Prototype.dbo.tbValue v on LongName = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'BuildingAllow' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display, EtlID)
select distinct a.AttributeID, v.ValueID, Active, ID
from dbo.attrBuildingFeatures
inner join Jaguar_v2_Prototype.dbo.tbValue v on LongName = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'BuildingAmenity' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by v.ValueID


insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display)
select distinct a.AttributeID, v.ValueID, Active
from dbo.attrSaleStatuses
inner join Jaguar_v2_Prototype.dbo.tbValue v on LongName = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'ListingStatus' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
union
select distinct a.AttributeID, v.ValueID, Active
from dbo.attrRentalStatuses
inner join Jaguar_v2_Prototype.dbo.tbValue v on LongName = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'ListingStatus' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display, EtlID)
select distinct a.AttributeID, v.ValueID, Active, ID
from dbo.attrSaleStatuses
inner join Jaguar_v2_Prototype.dbo.tbValue v on LongName = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'SaleStatus' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display, EtlID)
select distinct a.AttributeID, v.ValueID, Active, ID
from dbo.attrRentalStatuses
inner join Jaguar_v2_Prototype.dbo.tbValue v on LongName = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'RentalStatus' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display, EtlID)
select distinct a.AttributeID, v.ValueID, Active, ID
from dbo.attrHomeExposureTypes
inner join Jaguar_v2_Prototype.dbo.tbValue v on LongName = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'Exposure' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display, EtlID)
select distinct a.AttributeID, v.ValueID, Active, ID
from dbo.attrHomeOutdoorSpaceTypes
inner join Jaguar_v2_Prototype.dbo.tbValue v on LongName = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'OutdoorSpace' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display, EtlID)
select distinct a.AttributeID, v.ValueID, Active, ID
from dbo.attrListingViews
inner join Jaguar_v2_Prototype.dbo.tbValue v on LongName = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'View' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display, EtlID)
select distinct a.AttributeID, v.ValueID, Active, ID
from dbo.attrExteriorMaterialTypes
inner join Jaguar_v2_Prototype.dbo.tbValue v on LongName = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'Facade' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display, EtlID)
select distinct a.AttributeID, v.ValueID, Active, ID
from dbo.attrApartmentStyles
inner join Jaguar_v2_Prototype.dbo.tbValue v on LongName = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'ApartmentType' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display, EtlID)
select distinct a.AttributeID, v.ValueID, Active, ID
from dbo.attrAreas
inner join Jaguar_v2_Prototype.dbo.tbValue v on LongName = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'Area' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display, EtlID)
select distinct a.AttributeID, v.ValueID, Active, ID
from dbo.attrNeighborhoods
inner join Jaguar_v2_Prototype.dbo.tbValue v on LongName = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'Neighborhood' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, SortOrder)
select distinct a.AttributeID, v.ValueID, ROW_NUMBER() over (order by try_parse(replace(v.ValueName, '$', '') as bigint))
from Jaguar_v2_Prototype.dbo.tbValue v
join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'PriceMin'
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
	and v.ValueName in ('$0','$100,000','$250,000','$500,000','$750,000','$1,000,000','$1,500,000','$2,000,000','$2,500,000','$3,000,000','$3,500,000','$4,000,000','$4,500,000','$5,000,000','$10,000,000','$15,000,000','$20,000,000','$30,000,000','$40,000,000','$50,000,000')

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, SortOrder)
select distinct a.AttributeID, v.ValueID, ROW_NUMBER() over (order by try_parse(replace(v.ValueName, '$', '') as bigint))
from Jaguar_v2_Prototype.dbo.tbValue v
join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'PriceMax'
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
	and v.ValueName in ('$0','$100,000','$250,000','$500,000','$750,000','$1,000,000','$1,500,000','$2,000,000','$2,500,000','$3,000,000','$3,500,000','$4,000,000','$4,500,000','$5,000,000','$10,000,000','$15,000,000','$20,000,000','$30,000,000','$40,000,000','$50,000,000')

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, SortOrder)
select distinct a.AttributeID, v.ValueID, ROW_NUMBER() over (order by try_parse(replace(v.ValueName, '$', '') as bigint))
from Jaguar_v2_Prototype.dbo.tbValue v
join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'RentMax'
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where v.ValueName in ('$0','$1,000','$2,000','$3,000','$4,000','$5,000','$6,000','$7,000','$8,000','$9,000','$10,000')
and
va.AttributeValueID is null

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, SortOrder)
select distinct a.AttributeID, v.ValueID, ROW_NUMBER() over (order by try_parse(replace(v.ValueName, '$', '') as bigint))
from Jaguar_v2_Prototype.dbo.tbValue v
join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'RentMin'
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where v.ValueName in ('$0','$1,000','$2,000','$3,000','$4,000','$5,000','$6,000','$7,000','$8,000','$9,000','$10,000')
and
va.AttributeValueID is null

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display)
select distinct a.AttributeID, v.ValueID, 1
from Jaguar_v2_Prototype.dbo.tbValue v
inner join Jaguar_v2_Prototype.dbo.tfCommaListToTable('Image,Video') on v.ValueName = Listitem
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'MediaType' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display, EtlID)
select distinct a.AttributeID, v.ValueID, Active, ID
from dbo.attrPictureTypes
inner join Jaguar_v2_Prototype.dbo.tbValue v on LongName = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'ImageType' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by v.ValueID

insert into Jaguar_v2_Prototype.dbo.tbAttributeValue (AttributeID, ValueID, Display, EtlID)
select distinct a.AttributeID, v.ValueID, Active, ID
from dbo.attrLeaseTypes
inner join Jaguar_v2_Prototype.dbo.tbValue v on LongName = ValueName
inner join Jaguar_v2_Prototype.dbo.tbAttribute a on AttributeName = 'LeaseType' 
left join Jaguar_v2_Prototype.dbo.tbAttributeValue va on va.AttributeID = a.AttributeID and va.ValueID = v.ValueID
where va.AttributeValueID is null
order by v.ValueID
