use Jaguar_v1_Production

------select *
--------distinct right(source, 4)
------from dbo.ListingPictures
------where right(source, 4) = '.xls'
--------left(right(source, 4), 1) = '.'
--------Source = 'http://assets.prudentialelliman.com/ListingImages/building_floorplans/DE/55995.915185149.gif'
--inner join Jaguar_v2_Prototype.dbo.vwAttributeValues b on b.AttributeName = 'MediaType' and case a.PictureTypeID when 0 then 'Photo' when 1 then 'FloorPlan' end = b.ValueName

--tbMedia
insert into Jaguar_v2_Prototype.dbo.tbMedia (MediaTypeID, Link)
select distinct b.AttributeValueID, a.Source--, a.Description, a.MediaDescription
from dbo.ListingPictures a
inner join Jaguar_v2_Prototype.dbo.vwAttributeValues b on b.AttributeName = 'MediaType' and b.ValueName = 'Image'
left join Jaguar_v2_Prototype.dbo.tbMedia c on b.AttributeValueID = c.MediaTypeID and a.Source = c.Link
where c.MediaID is null

