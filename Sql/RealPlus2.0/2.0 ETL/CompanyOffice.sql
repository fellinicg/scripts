use Jaguar_v2_WiP

insert into dbo.tbCompany (CompanyName)
select distinct a.Name
from Jaguar_v1_Production.dbo.ParentOffices a
	left join dbo.tbCompany b on a.Name = b.CompanyName
where b.CompanyID is null
order by a.Name

insert into dbo.tbOffice (CompanyID, OfficeCode, Detail, PhoneNumber, Fax, Email, CreateDateTime, UpdateDateTime, EtlID)
select c.CompanyID, a.ShortName, a.Name, a.Phone, a.Fax, a.Email, a.CreateDate, a.ModDate, a.OfficeID
from Jaguar_v1_Production.dbo.Offices a
inner join Jaguar_v1_Production.dbo.ParentOffices b on a.ParentOfficeID = b.ParentOfficeID
inner join dbo.tbCompany c on b.Name = c.CompanyName
left join dbo.tbOffice d on a.ShortName = d.OfficeCode
where d.OfficeID is null

insert into dbo.tbPostalAddress (Line1, Line2, CityTown, StateProvince, PostalCode, Country)
select distinct a.Address1, a.Address2, pc.CityTown, pc.StateProvince, pc.PostalCode, pc.Country
from Jaguar_v1_Production.dbo.Offices a
	inner join dbo.tbOffice b on a.ShortName = b.OfficeCode
	inner join Jaguar_v1_Production.dbo.attrPostalCodes p on a.PostalCodeID = p.ID
	inner join dbo.tbPostalCode pc on p.LongName = pc.PostalCode
	left join dbo.tbPostalAddress dest on a.Address1 = dest.Line1 and isnull(a.Address2, '') = isnull(dest.Line2, '') and p.longname = dest.PostalCode
where 
	a.Address1 is not null 
	and 
	a.CityID is not null
	and
	dest.PostalAddressID is null

update dbo.tbOffice set PostalAddressID = x.PostalAddressID
from dbo.tbOffice o
	inner join Jaguar_v1_Production.dbo.Offices a on o.EtlID = a.OfficeID
	inner join Jaguar_v1_Production.dbo.attrPostalCodes p on a.PostalCodeID = p.ID
	left join dbo.tbPostalAddress x on a.Address1 = x.Line1 and isnull(a.Address2, '') = isnull(x.Line2, '') and p.longname = x.PostalCode
where 
	a.Address1 is not null 
	and 
	a.CityID is not null