use Jaguar_v1_Production

truncate table Jaguar_v2_Prototype.dbo.tbWorkingSet

--insert into Jaguar_v2_Prototype.dbo.tbWorkingSet (WorkingID)
--select ListingID
--from dbo.Listings
--where ListingID in (511976, 705340)

insert into Jaguar_v2_Prototype.dbo.tbWorkingSet (WorkingID)
select ListingID
from dbo.Listings
where ListingNumber in (
13272919
,12525009
,14374921
,13259506
,13045944
,13294288
,14324720
,14046358
,13393955
,13667168
,13761503
,12062448
,13696417
,13157909
,14253709
,14220190
,11914892
,1054082
,1065230
,1062156
)