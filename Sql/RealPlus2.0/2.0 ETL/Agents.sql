use Jaguar_v2_WiP

--tbAgent
insert into dbo.tbAgent (AgentID, AgentNumber, CreateDateTime, UpdateDateTime, EtlID)
select distinct p.PersonID, a.AgentNumber, a.CreateDate, a.ModDate, a.AgentID
from 
	--dbo.tbListingSaleFull lf
	--inner join Jaguar_v1_Production.dbo.Listings l on lf.ListingID = l.ListingID
	--inner join 
	Jaguar_v1_Production.dbo.Agents a 
	inner join dbo.tbPerson p on a.PersonID = p.EtlID
	left join dbo.tbAgent dest on p.PersonID = dest.AgentID
where dest.AgentID is null

--tbAgentStatus
insert into dbo.tbAgentStatus (AgentID, StatusID, CreateID, CreateDateTime)
select b.AgentID, c.AttributeValueID, 0, a.CreateDate
from Jaguar_v1_Production.dbo.AgentHistory a
	inner join dbo.tbAgent b on a.PersonID = b.EtlID
	inner join (select AttributeValueID, case ValueName when 'Active' then 1 when 'Inactive' then 0 else null end ID from dbo.vwAttributeValues where AttributeName = 'Status') c on a.NewIsActive = c.ID
	left join dbo.tbAgentStatus x on b.AgentID = x.AgentID and c.AttributeValueID = x.StatusID and a.CreateDate = x.CreateDateTime
where a.NewIsActive is not null
	and x.AgentStatusID is null
order by a.PersonID, a.CreateDate

--Update tbAgent
--update Jaguar_v2_Prototype.dbo.tbAgent set AgentStatusID = b.AgentStatusID
--from Jaguar_v2_Prototype.dbo.tbAgent a
--inner join (
--select a.AgentStatusID, a.AgentID
--from Jaguar_v2_Prototype.dbo.tbAgentStatus a
--inner join (
--select distinct AgentID, max(CreateDateTime) CreateDateTime
--from Jaguar_v2_Prototype.dbo.tbAgentStatus
--group by AgentID
--) b on a.AgentID = b.AgentID and a.CreateDateTime = b.CreateDateTime) b on a.AgentID = b.AgentID

--tbAgentOffice
insert into dbo.tbAgentOffice (AgentID, OfficeID, CreateID, CreateDateTime)
select b.AgentID, c.OfficeID, 0, a.CreateDate
from Jaguar_v1_Production.dbo.AgentHistory a
inner join dbo.tbAgent b on a.PersonID = b.EtlID
inner join dbo.tbOffice c on a.NewOfficeID = c.EtlID
left join dbo.tbAgentOffice x on b.AgentID = x.AgentID and c.OfficeID = x.OfficeID and a.CreateDate = x.CreateDateTime
where a.NewOfficeID is not null
	and x.AgentOfficeID is null
order by a.PersonID, a.CreateDate

