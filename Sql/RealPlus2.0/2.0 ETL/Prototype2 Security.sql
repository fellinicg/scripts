use Jaguar_v2_Prototype2

if not exists (select * from sys.database_principals where name = N'sfa_sahil')
create user sfa_sahil for login sfa_sahil with default_schema=dbo
go
sp_addrolemember @rolename = 'db_owner', @membername = 'sfa_sahil'
go

if not exists (select * from sys.database_principals where name = N'sfa_mishant')
create user sfa_mishant for login sfa_mishant with default_schema=dbo
go
sp_addrolemember @rolename = 'db_owner', @membername = 'sfa_mishant'
go

if not exists (select * from sys.database_principals where name = N'sfa_maunish')
create user sfa_maunish for login sfa_maunish with default_schema=dbo
go
sp_addrolemember @rolename = 'db_owner', @membername = 'sfa_maunish'
go

if not exists (select * from sys.database_principals where name = N'sfa_kalpesh')
create user sfa_kalpesh for login sfa_kalpesh with default_schema=dbo
go
sp_addrolemember @rolename = 'db_owner', @membername = 'sfa_kalpesh'
go

if not exists (select * from sys.database_principals where name = N'rp_web')
create user rp_web for login rp_web with default_schema=dbo
go
sp_addrolemember @rolename = 'web', @membername = 'rp_web'
go

if not exists (select * from sys.database_principals where name = N'rp_robert')
create user rp_robert for login rp_robert with default_schema=dbo
go
sp_addrolemember @rolename = 'web', @membername = 'rp_robert'
go
sp_addrolemember @rolename = 'viewdefinition', @membername = 'rp_robert'
go
sp_addrolemember @rolename = 'db_datareader', @membername = 'rp_robert'
go
