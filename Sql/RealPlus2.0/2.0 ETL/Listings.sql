use Jaguar_v1_Production

--tbListing
insert into Jaguar_v2_Prototype.dbo.tbListing (ListingCategoryID, PostalAddressID, ListingTypeID, ListingProviderID, InitialPrice, ListDate, CreateDateTime, UpdateDateTime, EtlID)
select 
	case when b.ListingID is null then 2 when c.ListingID is null then 1 end
	, pa.PostalAddressID
--	, o.OfficeID
	, lt.AttributeValueID
	, lp.AttributeValueID
	, coalesce(b.OriginalPrice, rh.NewRentalPrice)
	, a.ListedDate
	, a.CreateDate
	, a.ModDate
	, a.ListingID
from Jaguar_v2_Prototype.dbo.tbWorkingSet w
	inner join dbo.Listings a on w.WorkingID = a.ListingID
	left join dbo.Sales b on a.ListingID = b.ListingID
	left join dbo.Rentals c on a.ListingID = c.ListingID
	left join 
		(select r.RentalID, r.NewRentalPrice from dbo.RentalHistories r inner join (select distinct RentalID, min(EventDate) EventDate from dbo.RentalHistories group by RentalID) rh on r.RentalID = rh.RentalID and r.EventDate = rh.EventDate) rh on c.RentalID = rh.RentalID
--	left join Jaguar_v2_Prototype.dbo.tbOffice o on o.EtlID = a.ListingOfficeID
	left join Jaguar_v2_Prototype.dbo.vwAttributeValues lt on lt.AttributeName = 'ListingType' and a.ListingTypeID = lt.EtlID
	left join Jaguar_v2_Prototype.dbo.vwAttributeValues lp on lp.AttributeName = 'ListingProvider' and a.ListingProviderID = lp.EtlID
	left join Jaguar_v2_Prototype.dbo.tbPostalAddress pa on a.BuildingID = pa.EtlID
	left join Jaguar_v2_Prototype.dbo.tbListing l on a.ListingID = l.EtlID
where l.ListingID is null

--tbListingMedia
insert into Jaguar_v2_Prototype.dbo.tbListingMedia (ListingID, MediaID, Detail, AdditionalDetail, SortOrder, CreateDateTime, UpdateDateTime, EtlID)
select l.ListingID, b.AttributeValueID, a.Description, a.MediaDescription, a.SequenceNumber, a.CreateDate, a.ModDate, a.ListingPictureID
from Jaguar_v2_Prototype.dbo.tbWorkingSet w
	inner join dbo.ListingPictures a on w.WorkingID = a.ListingID
	left join Jaguar_v2_Prototype.dbo.vwAttributeValues b on b.AttributeName = 'ImageType' and a.PictureTypeID = b.EtlID
	left join Jaguar_v2_Prototype.dbo.tbListing l on a.ListingID = l.EtlID
	left join Jaguar_v2_Prototype.dbo.tbListingMedia dest on l.ListingID = dest.ListingID
where dest.ListingID is null














--select ListingID,ListingNumber,ListingAgentID,ListingOfficeID,ListingTypeID,ListingProviderID,FeaturedProperty,Sponsored,ListedDate,CreateDate,ModDate,NewConstruction,NoBoardApproval,OwnerUnitNumber
--,OwnerPostalCodeID,OwnerCityID,OwnerStateID,OwnerAddress,LotSize,AddressDisplayTypeID,Lot,Block,SyndicationGroupID,DepthFootage,FrontFootage,CertificateOfOccupancy,MLS_ID,ListingNumberReference,SubUnitNumber
--,UnitNumber,NextReviewDate,UnitNumberSort,UnitNumberFull,BuildingID,IsTownhouse
--from dbo.Listings
--where ListingID in (511976, 705340)

--select *
--from Jaguar_v2_Prototype.dbo.tbPerson

--select d.financingalloweddate,
--d.*
--,c.*
--,b.*
--,a.*
--from dbo.Listings a
--inner join dbo.Sales b on a.ListingID = b.ListingID
--left join dbo.Homes c on a.ListingID = c.ListingID
--left join dbo.Buildings d on a.BuildingID = d.BuildingID
--where a.ListingID in (511976, 705340)

------select 
------l.ListingID
------,l.ListingNumber
------,l.ListingNumberReference
------,l.ListingAgentID
------,l.ListingOfficeID
------,o.OfficeID 'NewOfficeID'
------,l.ListingTypeID
------,lt.AttributeValueID 'NewListingTypeID'
------,l.ListingProviderID
------,lp.AttributeValueID 'NewListingProviderID'
------,l.BuildingID
------,isnull(convert(varchar, UnitNumber), '') + isnull(convert(varchar, SubUnitNumber), '')
------,NewConstruction
------,CertificateOfOccupancy
------from dbo.Listings l
------left join Jaguar_v2_Prototype.dbo.tbOffice o on o.EtlID = l.ListingOfficeID
------left join Jaguar_v2_Prototype.dbo.vwAttributeValues lt on lt.AttributeName = 'ListingType' and l.ListingTypeID = lt.EtlID
------left join Jaguar_v2_Prototype.dbo.vwAttributeValues lp on lp.AttributeName = 'ListingProvider' and l.ListingProviderID = lp.EtlID
------where l.ListingID in (511976, 705340)

------select *
------from dbo.Addresses
------where buildingid in (69560,76646)

------select *
------from dbo.Offices
------where OfficeID in (
------1190
------,2129)


