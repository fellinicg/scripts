use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddListingCredit') and type in (N'P', N'PC'))
drop procedure dbo.upAddListingCredit
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddListingCredit
(
	@ListingID int
	, @AgentID int
	, @ListingCreditTypeID int
	, @CreditPercent float
	, @CreditDateTime datetime2
	, @CreateDateTime datetime2
	, @CreateID int
	, @ListingCreditID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbListingCredit
			(ListingID, AgentID, ListingCreditTypeID, CreditPercent, CreditDateTime, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@ListingID, @AgentID, @ListingCreditTypeID, @CreditPercent, @CreditDateTime, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @ListingCreditID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddListingCredit - ListingCredit was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddListingCredit to web