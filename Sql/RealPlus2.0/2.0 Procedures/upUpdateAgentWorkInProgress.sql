use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateAgentWorkInProgress') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateAgentWorkInProgress
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160328
-- ============================================================
create procedure dbo.upUpdateAgentWorkInProgress
(
	@AgentWorkInProgressID int
	, @AgentID int
	, @CustomerID int  = null
	, @Detail varchar(200) = null
	, @WorkInProgress xml = null
	, @SortBy bit = 0
	, @Bookmark int = null
	, @IsDeleted bit = null
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Update record
	update dbo.tbAgentWorkInProgress
	set 
		Detail = isnull(@Detail, Detail)
		, WorkInProgress = isnull(@WorkInProgress, WorkInProgress)
		, SortBy = isnull(@SortBy, SortBy)
		, Bookmark = isnull(@Bookmark, Bookmark)
		, IsDeleted = isnull(@IsDeleted, IsDeleted)
		, UpdateID = @AgentID
		, UpdateDateTime = getdate()	
	where 
		AgentWorkInProgressID = @AgentWorkInProgressID
		and
		AgentID = @AgentID
		and
		CustomerID = isnull(@CustomerID, CustomerID)

	-- Check for success
	if (@@error = 0) and (@@rowcount > 0)
		begin
			set @Result = 'S'
		end
	else
		raiserror('upUpdateAgentWorkInProgress - Work in progress was not successfully saved', 16, 1)
end
go
grant exec on dbo.upUpdateAgentWorkInProgress to web