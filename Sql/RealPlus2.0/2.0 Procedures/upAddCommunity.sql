use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddCommunity') and type in (N'P', N'PC'))
drop procedure dbo.upAddCommunity
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddCommunity
(
	@CommunityTypeID int
	, @CommunityName varchar(100)
	, @CreateDateTime datetime2
	, @CreateID int
	, @CommunityID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbCommunity
			(CommunityTypeID, CommunityName, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@CommunityTypeID, @CommunityName, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @CommunityID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddCommunity - Community was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddCommunity to web