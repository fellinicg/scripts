use Jaguar_v2_Prototype2_BHS_2
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetPropertyAttributes') and type in (N'P', N'PC'))
drop procedure dbo.upGetPropertyAttributes
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160804
-- ============================================================
create procedure dbo.upGetPropertyAttributes
	@PropertyID int
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		pa.PropertyID
		, a.AttributeID
		, a.AttributeCode
		, a.AttributeName
		, v.ValueName AttributeValue
	from dbo.tbPropertyAttribute pa
		inner join dbo.tbAttributeValue av on pa.AttributeValueID = av.AttributeValueID and av.Isdeleted = 0
		inner join dbo.tbAttribute a on av.AttributeID = a.AttributeID and a.IsDeleted = 0
		inner join dbo.tbValue v on av.ValueID = v.ValueID and v.Isdeleted = 0
	where 
		pa.IsDeleted = 0
		and pa.PropertyID = @PropertyID
	union
	select 
		pan.PropertyID
		, a.AttributeID
		, a.AttributeCode
		, a.AttributeName
		, convert(varchar, pan.AttributeValue) AttributeValue
	from dbo.tbPropertyAttributeNumeric pan
		inner join dbo.tbAttribute a on pan.AttributeID = a.AttributeID and a.IsDeleted = 0
	where 
		pan.IsDeleted = 0 
		and pan.PropertyID = @PropertyID
	union
	select 
		pat.PropertyID
		, a.AttributeID
		, a.AttributeCode
		, a.AttributeName
		, convert(varchar, pat.AttributeValue) AttributeValue
	from dbo.tbPropertyAttributeText pat
		inner join dbo.tbAttribute a on pat.AttributeID = a.AttributeID and a.IsDeleted = 0
	where 
		pat.IsDeleted = 0 
		and pat.PropertyID = @PropertyID
	union
	select 
		@PropertyID
		, -2
		, null
		, v.AttributeName
		, v.AttributeValue
	from (
		select
 			convert(varchar(250), p.addressid) as 'addressid'
 			, convert(varchar(250), p.displayaddress) as 'displayaddress'
 			, convert(varchar(250), p.webneighborhood) as 'webneighborhood'
 			, convert(varchar(250), p.propertytype) as 'propertytype'
 			--, convert(varchar(250), p.buildingtype) as 'buildingtype'
 			, convert(varchar(250), p.developmentid) as 'developmentid'
 			--, convert(varchar(250), p.ownershiptype) as 'ownershiptype'
 			, convert(varchar(250), p.lotwidth) as 'lotwidth'
 			, convert(varchar(250), p.lotlength) as 'lotlength'
 			, convert(varchar(250), p.lotsize) as 'lotsize'
 			--, convert(varchar(250), p.buildingname) as 'buildingname'
 			, convert(varchar(250), p.internalreferenceno) as 'internalreferenceno'
 			, convert(varchar(250), p.phone) as 'phone'
 			, convert(varchar(250), p.totalfloors) as 'totalfloors'
 			, convert(varchar(250), p.totalunits) as 'totalunits'
 			, convert(varchar(250), p.totalrooms) as 'totalrooms'
 			, convert(varchar(250), p.totalownedunits) as 'totalownedunits'
 			, convert(varchar(250), p.totalregulatedunits) as 'totalregulatedunits'
 			, convert(varchar(250), p.totalrentedunits) as 'totalrentedunits'
 			, convert(varchar(250), p.totalsponsoredunits) as 'totalsponsoredunits'
 			--, convert(varchar(250), p.yearbuilt) as 'yearbuilt'
 			, convert(varchar(250), p.yearrenovated) as 'yearrenovated'
 			, convert(varchar(250), p.yearincorporated) as 'yearincorporated'
 			, convert(varchar(250), p.utilitiesincluded) as 'utilitiesincluded'
 			, convert(varchar(250), p.elevators) as 'elevators'
 			--, convert(varchar(250), p.financingallowedpercent) as 'financingallowedpercent'
 			, convert(varchar(250), p.percentsold) as 'percentsold'
 			, convert(varchar(250), p.totalshares) as 'totalshares'
 			, convert(varchar(250), p.boardapprovaltorent) as 'boardapprovaltorent'
 			, convert(varchar(250), p.lot) as 'lot'
 			, convert(varchar(250), p.block) as 'block'
 			, convert(varchar(250), p.taxdeduction) as 'taxdeduction'
 			, convert(varchar(250), p.fliptax) as 'fliptax'
 			--, convert(varchar(250), p.postwar) as 'postwar'
 			, convert(varchar(250), p.managingagent) as 'managingagent'
 			, convert(varchar(250), p.airconditioningtype) as 'airconditioningtype'
 			, convert(varchar(250), p.heatingtype) as 'heatingtype'
 			, convert(varchar(250), p.newconstruction) as 'newconstruction'
 			, convert(varchar(250), p.parcel) as 'parcel'
 			, convert(varchar(250), p.frontfootage) as 'frontfootage'
 			, convert(varchar(250), p.depthfootage) as 'depthfootage'
 			, convert(varchar(250), p.acres) as 'acres'
 			, convert(varchar(250), p.facade) as 'facade'
 			, convert(varchar(250), p.housestyle) as 'housestyle'
 			, convert(varchar(250), p.housetype) as 'housetype'
 			, convert(varchar(250), p.occupancytype) as 'occupancytype'
 			, convert(varchar(250), p.buildingnumber) as 'buildingnumber'
 			, convert(varchar(250), p.garagetype) as 'garagetype'
 			, convert(varchar(250), p.garagestalls) as 'garagestalls'
 			, convert(varchar(250), p.offstreetparkingspots) as 'offstreetparkingspots'
 			, convert(varchar(250), p.watertype) as 'watertype'
 			, convert(varchar(250), p.sewertype) as 'sewertype'
 			, convert(varchar(250), p.fueltype) as 'fueltype'
 			, convert(varchar(250), p.super) as 'super'
 			, convert(varchar(250), p.superremarks) as 'superremarks'
 			, convert(varchar(250), p.doorman) as 'doorman'
 			, convert(varchar(250), p.doormanremarks) as 'doormanremarks'
 			, convert(varchar(250), p.fulltimedoorman) as 'fulltimedoorman'
 			, convert(varchar(250), p.elevman) as 'elevman'
 			, convert(varchar(250), p.elevmanremarks) as 'elevmanremarks'
 			, convert(varchar(250), p.concierge) as 'concierge'
 			, convert(varchar(250), p.conciergeremarks) as 'conciergeremarks'
 			, convert(varchar(250), p.createddate) as 'createddate'
 			, convert(varchar(250), p.modifieddate) as 'modifieddate'
 			, convert(varchar(250), p.createdby) as 'createdby'
 			, convert(varchar(250), p.modifiedby) as 'modifiedby'
 			, convert(varchar(250), p.security) as 'security'
 			, convert(varchar(250), p.healthclub) as 'healthclub'
 			, convert(varchar(250), p.pool) as 'pool'
 			, convert(varchar(250), p.courtyard) as 'courtyard'
 			, convert(varchar(250), p.taxabateexpire) as 'taxabateexpire'
 			, convert(varchar(250), p.livework) as 'livework'
 			, convert(varchar(250), p.storage) as 'storage'
 			, convert(varchar(250), p.roofgdn) as 'roofgdn'
 			, convert(varchar(250), p.bob_bin) as 'bob_bin'
 			, convert(varchar(250), p.rp_bin) as 'rp_bin'
 			, convert(varchar(250), p.landmark) as 'landmark'
 			, convert(varchar(250), p.zoning) as 'zoning'
 			, convert(varchar(250), p.elevator_in_building) as 'elevator_in_building'
 			, convert(varchar(250), p.pied_a_terre) as 'pied_a_terre'
 			, convert(varchar(250), p.washdry_allowed) as 'washdry_allowed'
 			, convert(varchar(250), p.laundry) as 'laundry'
 			, convert(varchar(250), p.corpownership) as 'corpownership'
 			--, convert(varchar(250), p.buildingallowed) as 'buildingallowed'
 			--, convert(varchar(250), p.buildingamenities) as 'buildingamenities'
 			--, convert(varchar(250), p.buildingnotallowed) as 'buildingnotallowed'
 			--, convert(varchar(250), p.buildingpersonnel) as 'buildingpersonnel'
 			--, convert(varchar(250), p.townhousefeatures) as 'townhousefeatures'
 			--, convert(varchar(250), p.noboardapproval) as 'noboardapproval'
 			--, convert(varchar(250), p.buildingextension) as 'buildingextension'
 			, convert(varchar(250), p.hasStorage) as 'hasStorage'
 			, convert(varchar(250), p.hasLaundry) as 'hasLaundry'
 			, convert(varchar(250), p.hasHealthClub) as 'hasHealthClub'
 			--, convert(varchar(250), p.managed_by) as 'managed_by'
 			--, convert(varchar(250), p.terrahld) as 'terrahld'
			---------------------ADDRESSES----------------------------
 			, convert(varchar(250), a.fulladdress) as 'fulladdress'
 			, convert(varchar(250), a.formaladdress) as 'formaladdress'
 			--, convert(varchar(250), a.displayaddress) as 'displayaddress'
 			, convert(varchar(250), a.botbaaddress) as 'botbaaddress'
 			, convert(varchar(250), a.shortaddress) as 'shortaddress'
 			, convert(varchar(250), a.housenumber_sort) as 'housenumber_sort'
 			, convert(varchar(250), a.housenumber_full) as 'housenumber_full'
 			, convert(varchar(250), a.housenumber_low) as 'housenumber_low'
 			, convert(varchar(250), a.housenumber_high) as 'housenumber_high'
 			, convert(varchar(250), a.streetname) as 'streetname'
 			, convert(varchar(250), a.streetname_full) as 'streetname_full'
 			, convert(varchar(250), a.streetname_direction) as 'streetname_direction'
 			, convert(varchar(250), a.streetname_short) as 'streetname_short'
 			, convert(varchar(250), a.streetname_prefix) as 'streetname_prefix'
 			, convert(varchar(250), a.streetname_extension) as 'streetname_extension'
 			, convert(varchar(250), a.streetname_suffix) as 'streetname_suffix'
 			, convert(varchar(250), a.streettype) as 'streettype'
 			, convert(varchar(250), a.addresstype) as 'addresstype'
 			, convert(varchar(250), a.longitude) as 'longitude'
 			, convert(varchar(250), a.latitude) as 'latitude'
 			, convert(varchar(250), a.city) as 'city'
 			, convert(varchar(250), a.state) as 'state'
 			, convert(varchar(250), a.postalcode) as 'postalcode'
 			, convert(varchar(250), a.town) as 'town'
 			, convert(varchar(250), a.province) as 'province'
 			, convert(varchar(250), a.precompass) as 'precompass'
 			, convert(varchar(250), a.postcompass) as 'postcompass'
 			, convert(varchar(250), a.borough) as 'borough'
 			--, convert(varchar(250), a.block) as 'block'
 			, convert(varchar(250), a.blockchar) as 'blockchar'
 			--, convert(varchar(250), a.lot) as 'lot'
 			, convert(varchar(250), a.lotchar) as 'lotchar'
 			, convert(varchar(250), a.neighborhoodid) as 'neighborhoodid'
 			, convert(varchar(250), a.sectionid) as 'sectionid'
 			, convert(varchar(250), a.taxid) as 'taxid'
 			, convert(varchar(250), a.rls_streetid) as 'rls_streetid'
 			, convert(varchar(250), a.botba_adr_id) as 'botba_adr_id'
 			, convert(varchar(250), a.botba_bbl_id) as 'botba_bbl_id'
 			, convert(varchar(250), a.botba_bin_id) as 'botba_bin_id'
 			, convert(varchar(250), a.grid_north) as 'grid_north'
 			, convert(varchar(250), a.grid_south) as 'grid_south'
 			, convert(varchar(250), a.grid_east) as 'grid_east'
 			, convert(varchar(250), a.grid_west) as 'grid_west'
 			, convert(varchar(250), a.cross_street) as 'cross_street'
 			, convert(varchar(250), a.bin) as 'bin'
 			--, convert(varchar(250), a.bob_bin) as 'bob_bin'
 			, convert(varchar(250), a.bob_bbl) as 'bob_bbl'
 			--, convert(varchar(250), a.createddate) as 'createddate'
 			--, convert(varchar(250), a.modifieddate) as 'modifieddate'
 			--, convert(varchar(250), a.createdby) as 'createdby'
 			--, convert(varchar(250), a.modifiedby) as 'modifiedby'
 			, convert(varchar(250), a.neighborhood_display) as 'neighborhood_display'
 			, convert(varchar(250), a.section_display) as 'section_display'
 			--, convert(varchar(250), a.rp_bin) as 'rp_bin'
 			--, convert(varchar(250), a.neighborhoodattribute) as 'neighborhoodattribute'
		from dbo.tbProperty r
			inner join properties p on r.EtlID = p.propertyid
			left join addresses a on p.addressid = a.addressid
		where r.PropertyID = @PropertyID
		) h
	unpivot 
	(AttributeValue for AttributeName in 
		(
			-- **************************** PROPERTIES ****************************
			addressid,
			displayaddress,
			webneighborhood,
			propertytype,
			--buildingtype,
			developmentid,
			--ownershiptype,
			lotwidth,
			lotlength,
			lotsize,
			--buildingname,
			internalreferenceno,
			phone,
			totalfloors,
			totalunits,
			totalrooms,
			totalownedunits,
			totalregulatedunits,
			totalrentedunits,
			totalsponsoredunits,
			--yearbuilt,
			yearrenovated,
			yearincorporated,
			utilitiesincluded,
			elevators,
			--financingallowedpercent,
			percentsold,
			totalshares,
			boardapprovaltorent,
			lot,
			block,
			taxdeduction,
			fliptax,
			--postwar,
			managingagent,
			airconditioningtype,
			heatingtype,
			newconstruction,
			parcel,
			frontfootage,
			depthfootage,
			acres,
			facade,
			housestyle,
			housetype,
			occupancytype,
			buildingnumber,
			garagetype,
			garagestalls,
			offstreetparkingspots,
			watertype,
			sewertype,
			fueltype,
			super,
			superremarks,
			doorman,
			doormanremarks,
			fulltimedoorman,
			elevman,
			elevmanremarks,
			concierge,
			conciergeremarks,
			createddate,
			modifieddate,
			createdby,
			modifiedby,
			security,
			healthclub,
			pool,
			courtyard,
			taxabateexpire,
			livework,
			storage,
			roofgdn,
			bob_bin,
			rp_bin,
			landmark,
			zoning,
			elevator_in_building,
			pied_a_terre,
			washdry_allowed,
			laundry,
			corpownership,
			--buildingallowed,
			--buildingamenities,
			--buildingnotallowed,
			--buildingpersonnel,
			--townhousefeatures,
			--noboardapproval,
			--buildingextension,
			hasStorage,
			hasLaundry,
			hasHealthClub,
			--managed_by,
			--terrahld,
			-- **************************** ADDRESSES ****************************
			fulladdress,
			formaladdress,
			--displayaddress,
			botbaaddress,
			shortaddress,
			housenumber_sort,
			housenumber_full,
			housenumber_low,
			housenumber_high,
			streetname,
			streetname_full,
			streetname_direction,
			streetname_short,
			streetname_prefix,
			streetname_extension,
			streetname_suffix,
			streettype,
			addresstype,
			longitude,
			latitude,
			city,
			state,
			postalcode,
			town,
			province,
			precompass,
			postcompass,
			borough,
			--block,
			blockchar,
			--lot,
			lotchar,
			neighborhoodid,
			sectionid,
			taxid,
			rls_streetid,
			botba_adr_id,
			botba_bbl_id,
			botba_bin_id,
			grid_north,
			grid_south,
			grid_east,
			grid_west,
			cross_street,
			bin,
			--bob_bin,
			bob_bbl,
			--createddate,
			--modifieddate,
			--createdby,
			--modifiedby,
			neighborhood_display,
			section_display
			--rp_bin,
			--neighborhoodattribute
		)
	) v
	order by AttributeName

end
go
grant exec on dbo.upGetPropertyAttributes to web