use Jaguar_v2_WiP
use Jaguar_v2_Prototype2
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetBuildingBySearch') and type in (N'P', N'PC'))
drop procedure dbo.upGetBuildingBySearch
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160413
-- ============================================================
create procedure dbo.upGetBuildingBySearch
	@AddressOrBuildingName varchar(max) = null
	, @UnitIdentifier varchar(max) = null
	, @GridNorth1 int = null
	, @GridNorth2 int = null
	, @GridEast1 int = null
	, @GridEast2 int = null
	, @GridWest1 int = null
	, @GridWest2 int = null
	, @GridSouth1 int = null
	, @GridSouth2 int = null
	, @AreaOrNeighborhood varchar(max) = null
	, @Zipcode varchar(max) = null
	, @BuildingType varchar(max) = null
	, @BuildingPersonnel varchar(max) = null
	, @BuildingAmenity varchar(max) = null
	, @BuildingAllow varchar(max) = null
	, @BuildingNotAllow varchar(max) = null
	, @MinYearBuilt int = null
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @TblResult table (PropertyID int index ix1 clustered)

	-- Build result table of all buildings
	insert into @tblresult 
	--select PropertyID from dbo.tbProperty where PropertyClassID = 1
	select distinct pp.PropertyID
	from dbo.tbListing l
		inner join dbo.tbProperty p on l.PropertyID = p.PropertyID
		inner join dbo.tbProperty pp on p.ParentPropertyID = pp.PropertyID
	where l.IsDeleted = 0
		and p.ParentPropertyID is not null

	-- Build result table of all buildings or by grid
	insert into @tblresult 
	select distinct pa.PropertyID
	from dbo.tbPropertyAddress pa
		--inner join dbo.tbProperty p on pa.PropertyID = p.PropertyID and p.PropertyClassID = 1
		inner join @tblresult p on pa.PropertyID = p.PropertyID
	where isnull(North, -1) = coalesce(@GridNorth1, North, -1)
		and isnull(East, -1) = coalesce(@GridEast1, East, -1)
		and isnull(West, -1) = coalesce(@GridWest1, West, -1)
		and isnull(South, -1) = coalesce(@GridSouth1, South, -1)
	union
	select distinct pa.PropertyID
	from dbo.tbPropertyAddress pa
--		inner join dbo.tbProperty p on pa.PropertyID = p.PropertyID and p.PropertyClassID = 1
		inner join @tblresult p on pa.PropertyID = p.PropertyID
	where isnull(North, -1) = coalesce(@GridNorth2, North, -1)
		and isnull(East, -1) = coalesce(@GridEast2, East, -1)
		and isnull(West, -1) = coalesce(@GridWest2, West, -1)
		and isnull(South, -1) = coalesce(@GridSouth2, South, -1)

	-- MinYearBuilt
	if @MinYearBuilt is not null 
		begin
			declare @TblMinYearBuilt table (PropertyID int index ix1 clustered)

			insert into @TblMinYearBuilt 
			select a.PropertyID 
			from @TblResult a
				inner join dbo.tbPropertyAttributeNumeric b on a.PropertyID = b.PropertyID
			where b.AttributeID = 106
				and b.AttributeValue >= @MinYearBuilt

			delete @TblResult
			from @TblResult a
				inner join (
					select PropertyID from @TblResult
					except
					select PropertyID from @TblMinYearBuilt) b on a.PropertyID = b.PropertyID
		end

	-- Zipcode
	if @Zipcode is not null 
		begin
			declare @TblZipcode table (PropertyID int index ix1 clustered)

			insert into @TblZipcode 
			select a.PropertyID 
			from @TblResult a
				inner join dbo.tbPropertyAddress pa on a.PropertyID = pa.PropertyID
				inner join dbo.tbPostalAddress adr on pa.PostalAddressID = adr.PostalAddressID
			where charindex(@Zipcode, adr.PostalCode) > 0

			delete @TblResult
			from @TblResult a
				inner join (
					select PropertyID from @TblResult
					except
					select PropertyID from @TblZipcode) b on a.PropertyID = b.PropertyID
		end

	-- BuildingType
	if @BuildingType is not null
		begin
			declare @TblBuildingType table (PropertyID int index ix1 clustered)

			insert into @TblBuildingType 
			select a.PropertyID
			from @TblResult a
				inner join dbo.tbPropertyAttribute pa on a.PropertyID = pa.PropertyID
				inner join dbo.tfCommaListToTable(@BuildingType) t on pa.AttributeValueID = t.ListItem

			delete @TblResult
			from @TblResult a
				inner join (
					select PropertyID from @TblResult
					except
					select PropertyID from @TblBuildingType) b on a.PropertyID = b.PropertyID
		end

	-- BuildingPersonnel
	if @BuildingPersonnel is not null
		begin
			declare @TblBuildingPersonnel table (PropertyID int index ix1 clustered)

			insert into @TblBuildingPersonnel 
			select a.PropertyID
			from @TblResult a
				inner join dbo.tbPropertyAttribute pa on a.PropertyID = pa.PropertyID
				inner join dbo.tfCommaListToTable(@BuildingPersonnel) t on pa.AttributeValueID = t.ListItem

			delete @TblResult
			from @TblResult a
				inner join (
					select PropertyID from @TblResult
					except
					select PropertyID from @TblBuildingPersonnel) b on a.PropertyID = b.PropertyID
		end

	-- BuildingAmenity
	if @BuildingAmenity is not null
		begin
			declare @TblBuildingAmenity table (PropertyID int index ix1 clustered)

			insert into @TblBuildingAmenity 
			select a.PropertyID
			from @TblResult a
				inner join dbo.tbPropertyAttribute pa on a.PropertyID = pa.PropertyID
				inner join dbo.tfCommaListToTable(@BuildingAmenity) t on pa.AttributeValueID = t.ListItem

			delete @TblResult
			from @TblResult a
				inner join (
					select PropertyID from @TblResult
					except
					select PropertyID from @TblBuildingAmenity) b on a.PropertyID = b.PropertyID
		end

	-- BuildingAllow
	if @BuildingAllow is not null
		begin
			declare @TblBuildingAllow table (PropertyID int index ix1 clustered)

			insert into @TblBuildingAllow 
			select a.PropertyID
			from @TblResult a
				inner join dbo.tbPropertyAttribute pa on a.PropertyID = pa.PropertyID
				inner join dbo.tfCommaListToTable(@BuildingAllow) t on pa.AttributeValueID = t.ListItem

			delete @TblResult
			from @TblResult a
				inner join (
					select PropertyID from @TblResult
					except
					select PropertyID from @TblBuildingAllow) b on a.PropertyID = b.PropertyID
		end

	-- BuildingNotAllow
	if @BuildingNotAllow is not null
		begin
			declare @TblBuildingNotAllow table (PropertyID int index ix1 clustered)

			insert into @TblBuildingNotAllow 
			select a.PropertyID
			from @TblResult a
				inner join dbo.tbPropertyAttribute pa on a.PropertyID = pa.PropertyID
				inner join dbo.tfCommaListToTable(@BuildingNotAllow) t on pa.AttributeValueID = t.ListItem

			delete @TblResult
			from @TblResult a
				inner join (
					select PropertyID from @TblResult
					except
					select PropertyID from @TblBuildingNotAllow) b on a.PropertyID = b.PropertyID
		end

	-- Return data
	select distinct
		p.PropertyID
		, a.Line1
		, a.Line2
		, a.Line3
		, a.Line4
		, a.CityTown
		, a.StateProvince
		, a.PostalCode
		, dbo.sfGetPropertyAttribute(p.PropertyID, 72) 'OwnershipType'
		, dbo.sfGetPropertyAttribute(p.PropertyID, 1118) 'BuildingPeriod'
		, pan.Elevators
		, pan.FinanceAllowedPercent
		, pan.Floors
		, dbo.sfGetPropertyAttribute(p.PropertyID, 45) 'HasFireplace'
		, dbo.sfGetPropertyAttribute(p.PropertyID, 43) 'HasAC'
		, dbo.sfGetPropertyAttribute(p.PropertyID, 49) 'HasWashDry'
		, dbo.sfGetPropertyAttribute(p.PropertyID, 7) 'ApartmentType'
		, dbo.sfGetPropertyAttribute(p.PropertyID, 33) 'Exposure'
		, dbo.sfGetPropertyAttribute(p.PropertyID, 105) 'Views'
	from @tblresult r
		inner join dbo.tbProperty p on r.PropertyID = p.PropertyID
		left join dbo.tbPropertyAddress pa on p.PropertyID = pa.PropertyAddressID and pa.IsPrimary = 1
		left join  dbo.tbPostalAddress a on pa.PostalAddressID = a.PostalAddressID
		left join dbo.vwPropertyAttributeNumeric pan on p.PropertyID = pan.PropertyID
		left join dbo.vwPropertyAttributeText pat on p.PropertyID = pat.PropertyID
	where p.IsDeleted = 0
	order by p.PropertyID

end
go
grant exec on dbo.upGetBuildingBySearch to web