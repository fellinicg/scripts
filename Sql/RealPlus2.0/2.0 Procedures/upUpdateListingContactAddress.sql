use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateListingContactAddress') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateListingContactAddress
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upUpdateListingContactAddress
(
	@ListingContactAddressID int
	, @ListingContactTypeID int = null
	, @PersonAddressID int = null
	, @IsDeleted bit = null
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbListingContactAddress
			set ListingContactTypeID = isnull(@ListingContactTypeID, ListingContactTypeID)
			, PersonAddressID = isnull(@PersonAddressID, PersonAddressID)
			, IsDeleted = isnull(@IsDeleted, IsDeleted)
		where ListingContactAddressID = @ListingContactAddressID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateListingContactAddress - ListingContactAddress was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateListingContactAddress to web