use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetSections') and type in (N'P', N'PC'))
drop procedure dbo.upGetSections
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160323
-- ============================================================
create procedure dbo.upGetSections
	@ShowDeleted bit = 0
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		CustomCriteriaSectionID
		, ShortLabel
		, LongLabel
		, IsDeleted
	from dbo.tbCustomCriteriaSection
	where IsDeleted = @ShowDeleted
	order by LongLabel

end
go
grant exec on dbo.upGetSections to web