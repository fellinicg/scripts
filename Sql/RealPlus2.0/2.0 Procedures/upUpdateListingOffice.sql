use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateListingOffice') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateListingOffice
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160412
-- ============================================================
create procedure dbo.upUpdateListingOffice
(
	@ListingOfficeID int
	, @IsDeleted bit
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbListingOffice
			set IsDeleted = @IsDeleted
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
			where ListingOfficeID = @ListingOfficeID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateListingOffice - ListingOffice was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateListingOffice to web