use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetListingBySearchOpt') and type in (N'P', N'PC'))
drop procedure dbo.upGetListingBySearchOpt
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160321
-- ============================================================
create procedure dbo.upGetListingBySearchOpt
	@ListingCategory int
	, @ActiveInactive varchar(3) = '0,1'
	, @ListingStatus varchar(max) = null
	, @WebOrListingNum varchar(max) = null
	, @PriceMin int = null
	, @PriceMax int = null
	, @Bedrooms float = null
	, @Bathrooms float = null
	, @HalfBathrooms float = null
	, @Rooms float = null
	, @OwnershipType varchar(max) = null
	, @ListDateStart date = null
	, @ListDateEnd date = null
	, @UpdateDateStart date = null
	, @UpdateDateEnd date = null
	, @ListUpdateDateStart date = null
	, @ListUpdateDateEnd date = null
	, @OpenHouseStart date = null
	, @OpenHouseEnd date = null
	, @MinSqFt int = null
	, @MaxPriceSqFt int = null
	, @IncMissingSqFt bit = 1
	, @MaxMthlyExp int = null
	, @Neighborhood varchar(max) = null
	, @BuildingPersonnel varchar(max) = null
	, @BuildingAllow varchar(max) = null
	, @BuildingNotAllow varchar(max) = null
	, @ListActivityStart date = null
	, @ListActivityEnd date = null
	, @ListActivityStatus int = null
	, @OutdoorSpace varchar(max) = null
	, @Views varchar(max) = null
	, @Exposure varchar(max) = null
	, @MinYearBuilt int = null
	, @ListingType varchar(max) = null
	, @AddressID varchar(max) = null
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @TblListingCategory table (ListingID int index ix1 clustered, IsRemoved bit)

	-- Build result table
	-- ListingCategory is required, so this becomes our universe
	insert into @TblListingCategory 
	select ListingID, 0
	from dbo.tbListing 
		inner join dbo.tfCommaListToTable(@ActiveInactive) on IsActive = ListItem
	where IsDeleted = 0
		and ListingCategoryID = @ListingCategory

	-- AddressID
	if @AddressID is not null 
		begin
			declare @TblAddress table (ListingID int index ix1 clustered)

			insert into @TblAddress 
			select l.ListingID
			from dbo.tbPropertyAddress pa
				inner join dbo.tfcommalisttotable(@AddressID) a on pa.PropertyAddressID = a.ListItem
				inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
				inner join dbo.tbListing l on p.PropertyID = l.PropertyID
			union
			select l.ListingID
			from dbo.tbPropertyAddress pa
				inner join dbo.tfcommalisttotable(@AddressID) a on pa.PropertyAddressID = a.ListItem
				inner join dbo.tbProperty p on pa.PropertyID = p.PropertyID
				inner join dbo.tbListing l on p.PropertyID = l.PropertyID

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblAddress) b on a.ListingID = b.ListingID
		end

	-- WebOrListingNum
	if @WebOrListingNum is not null 
		begin
			declare @TblWebOrListingNum table (ListingID int index ix1 clustered)

			insert into @TblWebOrListingNum 
			select ListingID
			from dbo.tfGetListingAttributeText(53)
				inner join dbo.tfCommaListToTable(@WebOrListingNum) on charindex(ListItem, AttributeValue) > 0		--AttributeValue = ListItem
			union
			select ListingID
			from dbo.tfGetListingAttributeText(54)
				inner join dbo.tfCommaListToTable(@WebOrListingNum) on charindex(ListItem, AttributeValue) > 0		--AttributeValue = ListItem

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblWebOrListingNum) b on a.ListingID = b.ListingID
		end

	-- PriceMin and PriceMax
	if (@PriceMin is not null or @PriceMax is not null) 
		begin
			declare @TblPrice table (ListingID int index ix1 clustered)

			insert into @TblPrice 
			select ls.ListingID
			from @TblListingCategory ls
				--inner join dbo.vwCurrentListingPrice b on ls.ListingID = b.ListingID
				inner join dbo.tbListing l on ls.ListingID = l.ListingID
			where CurrentPrice between isnull(@PriceMin, 0) and isnull(@PriceMax, 1000000000)

			select count(ListingID)
			from (
			select ListingID
			from @TblListingCategory
			intersect
			select ListingID
			from @TblPrice) a

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblPrice) b on a.ListingID = b.ListingID

			select count(ListingID) from @TblListingCategory

		end

	-- MinYearBuilt
	if @MinYearBuilt is not null 
		begin
			declare @TblMinYearBuilt table (ListingID int index ix1 clustered)

			insert into @TblMinYearBuilt 
			select b.ListingID 
			from @TblListingCategory ls
				inner join dbo.tfPropertyAttributeNumeric(106) b on ls.ListingID = b.ListingID
			where AttributeValue >= @MinYearBuilt

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblMinYearBuilt) b on a.ListingID = b.ListingID
		end

	-- Bedrooms
	if @Bedrooms is not null 
		begin
			declare @TblBed table (ListingID int index ix1 clustered)

			insert into @TblBed 
			select b.ListingID 
			from @TblListingCategory ls
				inner join dbo.tfPropertyAttributeNumeric(11) b on ls.ListingID = b.ListingID
			where AttributeValue >= @Bedrooms

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblBed) b on a.ListingID = b.ListingID
		end
	
	-- Bathrooms
	if @Bathrooms is not null 
		begin
			declare @TblBath table (ListingID int index ix1 clustered)

			insert into @TblBath 
			select b.ListingID 
			from @TblListingCategory ls
				inner join dbo.tfPropertyAttributeNumeric(10) b on ls.ListingID = b.ListingID
			where AttributeValue >= @Bathrooms

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblBath) b on a.ListingID = b.ListingID
		end

	-- HalfBathrooms
	if @HalfBathrooms is not null 
		begin
			declare @TblHalfBath table (ListingID int index ix1 clustered)

			insert into @TblHalfBath 
			select b.ListingID 
			from @TblListingCategory ls
				inner join dbo.tfPropertyAttributeNumeric(41) b on ls.ListingID = b.ListingID
			where AttributeValue >= @HalfBathrooms

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblHalfBath) b on a.ListingID = b.ListingID
		end

	-- Rooms
	if @Rooms is not null 
		begin
			declare @TblRooms table (ListingID int index ix1 clustered)

			insert into @TblRooms 
			select b.ListingID 
			from @TblListingCategory ls
				inner join dbo.tfPropertyAttributeNumeric(85) b on ls.ListingID = b.ListingID
			where AttributeValue >= @Rooms

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblRooms) b on a.ListingID = b.ListingID
		end

	-- OwnershipType
	if @OwnershipType is not null 
		begin
			declare @TblOwnershipType table (ListingID int index ix1 clustered)

			insert into @TblOwnershipType
			select ls.ListingID
			from @TblListingCategory ls
				inner join dbo.tbListing l on ls.ListingID = l.ListingID
				inner join dbo.tbProperty p on l.PropertyID = p.PropertyID
				inner join dbo.tbPropertyAttribute pa on p.ParentPropertyID = pa.PropertyID or p.PropertyID = pa.PropertyID
				inner join dbo.tfCommaListToTable(@OwnershipType) t on pa.AttributeValueID = t.ListItem
			--select l.ListingID
			--from dbo.tbPropertyAttribute pa
			--	inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
			--	inner join dbo.tbListing l on p.PropertyID = l.PropertyID
			--	inner join dbo.tfCommaListToTable(@OwnershipType) t on pa.AttributeValueID = t.ListItem

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblOwnershipType) b on a.ListingID = b.ListingID
		end

	-- ListDateStart and ListDateEnd
	if (@ListDateStart is not null or @ListDateEnd is not null) 
		begin
			declare @TblListDate table (ListingID int index ix1 clustered)

			insert into @TblListDate 
			select ls.ListingID 
			from @TblListingCategory ls
				inner join dbo.tbListing b on ls.ListingID = b.ListingID
			where ListDate between isnull(@ListDateStart, '1/1/1753') and isnull(@ListDateEnd, '12/31/9999')

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblListDate) b on a.ListingID = b.ListingID
		end

	-- UpdateDateStart and UpdateDateEnd
	if (@UpdateDateStart is not null or @UpdateDateEnd is not null) 
		begin
			declare @TblUpdateDate table (ListingID int index ix1 clustered)

			insert into @TblUpdateDate 
			select ls.ListingID 
			from @TblListingCategory ls
				inner join dbo.tbListing b on ls.ListingID = b.ListingID
			where convert(date, UpdateDateTime) between isnull(@UpdateDateStart, '1/1/1753') and isnull(@UpdateDateEnd, '12/31/9999')

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblUpdateDate) b on a.ListingID = b.ListingID
		end

	-- ListUpdateDateStart and ListUpdateDateEnd
	if (@ListUpdateDateStart is not null or @ListUpdateDateEnd is not null) 
		begin
			declare @TblListUpdateDate table (ListingID int index ix1 clustered)

			insert into @TblListUpdateDate 
			select ls.ListingID 
			from @TblListingCategory ls
				inner join dbo.tbListing b on ls.ListingID = b.ListingID
			where ListDate between isnull(@ListUpdateDateStart, '1/1/1753') and isnull(@ListUpdateDateEnd, '12/31/9999')
			union all
			select ls.ListingID 
			from @TblListingCategory ls
				inner join dbo.tbListing b on ls.ListingID = b.ListingID
			where convert(date, UpdateDateTime) between isnull(@ListUpdateDateStart, '1/1/1753') and isnull(@ListUpdateDateEnd, '12/31/9999')

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblListUpdateDate) b on a.ListingID = b.ListingID
		end

	-- OpenHouseStart and OpenHouseEnd
	if (@OpenHouseStart is not null or @OpenHouseEnd is not null) 
		begin
			declare @TblOpenHouse table (ListingID int index ix1 clustered)

			insert into @TblOpenHouse 
			select ls.ListingID 
			from @TblListingCategory ls
				inner join dbo.tbListingOpenHouse b on ls.ListingID = b.ListingID
			where convert(date, StartDateTime) between isnull(@OpenHouseStart, '1/1/1753') and isnull(@OpenHouseEnd, '12/31/9999')
				or convert(date, EndDateTime) between isnull(@OpenHouseStart, '1/1/1753') and isnull(@OpenHouseEnd, '12/31/9999')

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblOpenHouse) b on a.ListingID = b.ListingID
		end

	-- MinSqFt
	if @MinSqFt is not null 
		begin
			declare @TblMinSqFt table (ListingID int index ix1 clustered)

			insert into @TblMinSqFt 
			select ls.ListingID 
			from @TblListingCategory ls
				inner join dbo.tfPropertyAttributeNumeric(91) b on ls.ListingID = b.ListingID
			where AttributeValue >= @MinSqFt
			union 
			select ls.ListingID 
			from @TblListingCategory ls
				inner join dbo.tfChildPropertyAttributeNumericMissing(91) b on ls.ListingID = b.ListingID
			where 1 = @IncMissingSqFt

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblMinSqFt) b on a.ListingID = b.ListingID
		end
	
	-- MaxPriceSqFt
	if @MaxPriceSqFt is not null 
		begin
			declare @TblMaxPriceSqFt table (ListingID int index ix1 clustered)

			insert into @TblMaxPriceSqFt 
			select ls.ListingID 
			from @TblListingCategory ls
				inner join dbo.tfPropertyAttributeNumeric(61) b on ls.ListingID = b.ListingID 
			where AttributeValue >= @MaxPriceSqFt
			union 
			select ls.ListingID 
			from @TblListingCategory ls
				inner join dbo.tfChildPropertyAttributeNumericMissing(61) b on ls.ListingID = b.ListingID
			where 1 = @IncMissingSqFt

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblMaxPriceSqFt) b on a.ListingID = b.ListingID
		end
	
	-- MaxMthlyExp
	if @MaxMthlyExp is not null 
		begin
			declare @TblMaxMthlyExp table (ListingID int index ix1 clustered)

			insert into @TblMaxMthlyExp 
			select ls.ListingID 
			from @TblListingCategory ls
				inner join dbo.tfPropertyAttributeNumeric(60) b on ls.ListingID = b.ListingID 
			where AttributeValue >= @MaxPriceSqFt

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblMaxMthlyExp) b on a.ListingID = b.ListingID
		end
	
	-- Neighborhood
	if @Neighborhood is not null 
		begin
			declare @TblNeighborhood table (ListingID int index ix1 clustered)

			insert into @TblNeighborhood 
			select ls.ListingID
			from @TblListingCategory ls
				inner join dbo.tbListing l on ls.ListingID = l.ListingID
				inner join dbo.tbProperty p on l.PropertyID = p.PropertyID
				inner join dbo.tbPropertyAttribute pa on p.ParentPropertyID = pa.PropertyID or p.PropertyID = pa.PropertyID
				inner join dbo.tfCommaListToTable(@Neighborhood) t on pa.AttributeValueID = t.ListItem

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblNeighborhood) b on a.ListingID = b.ListingID
		end

	-- BuildingPersonnel
	if @BuildingPersonnel is not null
		begin
			declare @TblBuildingPersonnel table (ListingID int index ix1 clustered)

			insert into @TblBuildingPersonnel 
			select ls.ListingID
			from @TblListingCategory ls
				inner join dbo.tbListing l on ls.ListingID = l.ListingID
				inner join dbo.tbProperty p on l.PropertyID = p.PropertyID
				inner join dbo.tbPropertyAttribute pa on p.ParentPropertyID = pa.PropertyID or p.PropertyID = pa.PropertyID
				inner join (select ListingID, ListItem from @TblListingCategory cross apply dbo.tfCommaListToTable(@BuildingPersonnel)) t on pa.AttributeValueID = t.ListItem and l.ListingID = t.ListingID
			--	inner join dbo.tfCommaListToTable(@BuildingPersonnel) t on pa.AttributeValueID = t.ListItem
			--group by ls.ListingID
			--having count(ls.ListingID) = (select count(1) from dbo.tfCommaListToTable(@BuildingPersonnel))		-- Must have all

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblBuildingPersonnel) b on a.ListingID = b.ListingID
		end

	-- BuildingAllow
	if @BuildingAllow is not null
		begin
			declare @TblBuildingAllow table (ListingID int index ix1 clustered)

			insert into @TblBuildingAllow 
			select ls.ListingID
			from @TblListingCategory ls
				inner join dbo.tbListing l on ls.ListingID = l.ListingID
				inner join dbo.tbProperty p on l.PropertyID = p.PropertyID
				inner join dbo.tbPropertyAttribute pa on p.ParentPropertyID = pa.PropertyID or p.PropertyID = pa.PropertyID
				inner join dbo.tfCommaListToTable(@BuildingAllow) t on pa.AttributeValueID = t.ListItem

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblBuildingAllow) b on a.ListingID = b.ListingID
		end

	-- BuildingNotAllow
	if @BuildingNotAllow is not null
		begin
			declare @TblBuildingNotAllow table (ListingID int index ix1 clustered)

			insert into @TblBuildingNotAllow 
			select ls.ListingID
			from @TblListingCategory ls
				inner join dbo.tbListing l on ls.ListingID = l.ListingID
				inner join dbo.tbProperty p on l.PropertyID = p.PropertyID
				inner join dbo.tbPropertyAttribute pa on p.ParentPropertyID = pa.PropertyID or p.PropertyID = pa.PropertyID
				inner join dbo.tfCommaListToTable(@BuildingNotAllow) t on pa.AttributeValueID = t.ListItem

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblBuildingNotAllow) b on a.ListingID = b.ListingID
		end

	-- ListActivityStart and ListActivityEnd
	if (@ListActivityStart is not null or @ListActivityEnd is not null) 
		begin
			declare @TblListActivity table (ListingID int index ix1 clustered)

			insert into @TblListActivity 
			select ls.ListingID 
			from @TblListingCategory ls
				inner join dbo.vwCurrentListingStatus b on ls.ListingID = b.ListingID 
			where convert(date, StatusDateTime) between @ListActivityStart and @ListActivityEnd
				and AttributeValueID = isnull(@ListActivityStatus, AttributeValueID)

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblListActivity) b on a.ListingID = b.ListingID
		end

	-- OutdoorSpace
	if @OutdoorSpace is not null
		begin
			declare @TblOutdoorSpace table (ListingID int index ix1 clustered)

			insert into @TblOutdoorSpace 
			select ls.ListingID
			from @TblListingCategory ls
				inner join dbo.tbListing l on ls.ListingID = l.ListingID
				inner join dbo.tbProperty p on l.PropertyID = p.PropertyID
				inner join dbo.tbPropertyAttribute pa on p.ParentPropertyID = pa.PropertyID or p.PropertyID = pa.PropertyID
				inner join dbo.tfCommaListToTable(@OutdoorSpace) t on pa.AttributeValueID = t.ListItem

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblOutdoorSpace) b on a.ListingID = b.ListingID
		end

	-- Views
	if @Views is not null
		begin
			declare @TblViews table (ListingID int index ix1 clustered)

			insert into @TblViews 
			select ls.ListingID
			from @TblListingCategory ls
				inner join dbo.tbListing l on ls.ListingID = l.ListingID
				inner join dbo.tbProperty p on l.PropertyID = p.PropertyID
				inner join dbo.tbPropertyAttribute pa on p.ParentPropertyID = pa.PropertyID or p.PropertyID = pa.PropertyID
				inner join dbo.tfCommaListToTable(@Views) t on pa.AttributeValueID = t.ListItem

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblViews) b on a.ListingID = b.ListingID
		end

	-- Exposure
	if @Exposure is not null
		begin
			declare @TblExposure table (ListingID int index ix1 clustered)

			insert into @TblExposure 
			select ls.ListingID
			from @TblListingCategory ls
				inner join dbo.tbListing l on ls.ListingID = l.ListingID
				inner join dbo.tbProperty p on l.PropertyID = p.PropertyID
				inner join dbo.tbPropertyAttribute pa on p.ParentPropertyID = pa.PropertyID or p.PropertyID = pa.PropertyID
				inner join dbo.tfCommaListToTable(@Exposure) t on pa.AttributeValueID = t.ListItem

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblExposure) b on a.ListingID = b.ListingID
		end

	-- ListingType
	if @ListingType is not null
		begin
			declare @TblListingType table (ListingID int index ix1 clustered)

			insert into @TblListingType 
			select ls.ListingID
			from @TblListingCategory ls
				inner join dbo.tbListing l on ls.ListingID = l.ListingID
				inner join dbo.tfCommaListToTable(@ListingType) t on l.ListingTypeID = t.ListItem

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblListingType) b on a.ListingID = b.ListingID
		end

	-- ListingStatus
	if @ListingStatus is not null 
		begin
			declare @TblStatus table (ListingID int index ix1 clustered)

			insert into @TblStatus 
			select a.ListingID 
			from @TblListingCategory ls
				inner join dbo.tbListing a on ls.ListingID = a.ListingID
				inner join dbo.tbListingStatus s on a.CurrentStatusID = s.ListingStatusID
				inner join (select ListItem from dbo.tfCommaListToTable(@ListingStatus)) b on s.StatusID = b.ListItem

			delete @TblListingCategory
			from @TblListingCategory a
				inner join (
					select ListingID from @TblListingCategory
					except
					select ListingID from @TblStatus) b on a.ListingID = b.ListingID
		end

	-- Return data
	select distinct
		l.ListingID
		, p.PropertyID
		, p.ParentPropertyID
		, dbo.sfGetAttributeValue(l.ListingProviderID) 'ListingProvider'
		, dbo.sfGetAttributeValue(l.ListingCategoryID) 'ListingCategory'
		, dbo.sfGetAttributeValue(l.ListingTypeID) 'ListingType'
		, l.InitialPrice
		, l.CurrentPrice
		, lan.ClosingPrice
		, l.CurrentStatus
		, lan.BuyerCommission
		, lan.SellerCommission
		, lan.TotalCommission
		, lat.ListingNumber
		, lat.ListingNumberReference
		, l.ListDate
		--, dbo.sfGetAttributeValue(p.PropertyClassID) 'PropertyClass'
		--, ot.ValueName 'OwnershipType'
		--, nh.ValueName 'Neighborhood'
		--, pt.ValueName 'Pets'
		--, dm.ValueName 'Personnel'
		----, coalesce(p.Line1, bld.Line1) 'Line1'
		, isnull(pat.UnitIdentifier, ppat.UnitIdentifier) UnitIdentifier
		----, coalesce(p.CityTown, bld.CityTown) 'CityTown'
		----, coalesce(p.StateProvince, bld.StateProvince) 'StateProvince'
		----, coalesce(p.PostalCode, bld.PostalCode) 'PostalCode'
		, isnull(pan.AssociationFee, ppan.AssociationFee) AssociationFee
		, isnull(pan.Bathrooms, ppan.Bathrooms) Bathrooms
		, isnull(pan.Bedrooms, ppan.Bedrooms) Bedrooms
		, isnull(pan.HalfBathrooms, ppan.HalfBathrooms) HalfBathrooms
		, isnull(pan.Rooms, ppan.Rooms) Rooms
		, isnull(pan.SquareFootage, ppan.SquareFootage) SquareFootage
		, isnull(pan.Elevators, ppan.Elevators) Elevators
		, isnull(pan.FinanceAllowedPercent, ppan.FinanceAllowedPercent) FinanceAllowedPercent
		, isnull(pat.FlipTax, ppat.FlipTax) FlipTax
		, isnull(pan.Floors, ppan.Floors) Floors
		, isnull(pan.Shares, ppan.Shares) Shares
		, isnull(pan.TaxDeductPct, ppan.TaxDeductPct) TaxDeductPct
		, isnull(pan.Units, ppan.Units) Units
		, isnull(pan.YearBuilt, ppan.YearBuilt) YearBuilt
		, isnull(pan.YearIncorporated, ppan.YearIncorporated) YearIncorporated
		, l.UpdateDateTime 'ListingUpdated'
		, l.CreateDateTime 'ListingCreated'
	from @TblListingCategory ls
		inner join dbo.tbListing l on ls.ListingID = l.ListingID
		left join dbo.tbProperty p on l.PropertyID = p.PropertyID
		left join dbo.vwPropertyAttributeNumeric pan on p.PropertyID = pan.PropertyID
		left join dbo.vwPropertyAttributeNumeric ppan on p.ParentPropertyID = ppan.PropertyID
		left join dbo.vwPropertyAttributeText pat on p.PropertyID = pat.PropertyID
		left join dbo.vwPropertyAttributeText ppat on p.ParentPropertyID = ppat.PropertyID
		left join dbo.vwListingAttributeNumeric lan on ls.ListingID = lan.ListingID
		left join dbo.vwListingAttributeText lat on ls.ListingID = lat.ListingID
		--left join dbo.tfPropertyAttribute(72) ot on  p.PropertyID = ot.PropertyID or p.ParentPropertyID = ot.PropertyID
		--left join dbo.tfPropertyAttribute(67) nh on  p.PropertyID = nh.PropertyID or p.ParentPropertyID = nh.PropertyID
		--left join dbo.tfPropertyAttribute(12) pt on  (p.PropertyID = pt.PropertyID or p.ParentPropertyID = pt.PropertyID) and pt.AttributeValueID = 175
		--left join dbo.tfPropertyAttribute(17) dm on  (p.PropertyID = dm.PropertyID or p.ParentPropertyID = dm.PropertyID) and dm.AttributeValueID = 188
		--left join dbo.vwCurrentListingStatus cls on l.ListingID = cls.ListingID
		--left join dbo.vwCurrentListingPrice clp on l.ListingID = clp.ListingID

end
go
grant exec on dbo.upGetListingBySearchOpt to web