use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetDetail') and type in (N'P', N'PC'))
drop procedure dbo.upGetDetail
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160325
-- ============================================================
create procedure dbo.upGetDetail
	@DetailID int = null
	, @DetailTypeID int = null
	, @ShowDeleted bit = 0
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		DetailID
		, dbo.sfGetAttributeValue(DetailTypeID) 'Type'
		, DetailText
		, IsDeleted
		, CreateDateTime
		, dbo.sfGetPersonFullName(CreateID) 'CreatedBy'
		, UpdateDateTime
		, dbo.sfGetPersonFullName(UpdateID) 'UpdatedBy'
	from dbo.tbDetail
	where DetailID = isnull(@DetailID, DetailID)
		and DetailTypeID = isnull(@DetailTypeID, DetailTypeID)
		and IsDeleted = @ShowDeleted

end
go
grant exec on dbo.upGetDetail to web