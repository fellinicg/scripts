use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddPropertyAddress') and type in (N'P', N'PC'))
drop procedure dbo.upAddPropertyAddress
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddPropertyAddress
(
	@PropertyID int
	, @PostalAddressID int
	, @IsPrimary bit
	, @East int = null
	, @West int = null
	, @North int = null
	, @South int = null
	, @CreateDateTime datetime
	, @CreateID int
	, @PropertyAddressID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbPropertyAddress
			(PropertyID, PostalAddressID, IsPrimary, East, West, North, South, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@PropertyID, @PostalAddressID, @IsPrimary, @East, @West, @North, @South, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @PropertyAddressID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddPropertyAddress - PropertyAddress was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddPropertyAddress to web