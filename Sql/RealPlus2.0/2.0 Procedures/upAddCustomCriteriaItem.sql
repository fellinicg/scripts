use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddCustomCriteriaItem') and type in (N'P', N'PC'))
drop procedure dbo.upAddCustomCriteriaItem
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160328
-- ============================================================
create procedure dbo.upAddCustomCriteriaItem
(
	@CustomCriteriaSectionID int
	, @CompanyID int
	, @LongLabel varchar(100)
	, @CreateID int
	, @AttributeValueID int = null
	, @ShortLabel varchar(20) = null
	, @NumericValue decimal(12,2) = null
	, @SortOrder tinyint = 0
	, @Display bit = 1
	, @CustomCriteriaItemID int output
	, @Result char(1) = 'F' output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Insert record, if the AttributeValueName does NOT already exist
	insert into dbo.tbCustomCriteriaItem (CustomCriteriaSectionID, AttributeValueID, CompanyID, ShortLabel, LongLabel, NumericValue, SortOrder, Display, CreateID, UpdateID)
	Values (@CustomCriteriaSectionID, @AttributeValueID, @CompanyID, @ShortLabel, @LongLabel, @NumericValue, @SortOrder, @Display,  @CreateID, @CreateID)

	-- Check for success
	if (@@error = 0) and (@@rowcount > 0)
		begin
			set @CustomCriteriaItemID = @@identity
		end
	else
		raiserror('upAddCustomCriteriaItem - Custom criteria item was not successfully added', 16, 1)

end
go
grant exec on dbo.upAddCustomCriteriaItem to web