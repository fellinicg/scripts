use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateAgentStatus') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateAgentStatus
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upUpdateAgentStatus
(
	@AgentStatusID int
	, @IsDeleted bit
	, @UpdateDateTime datetime2
	, @UpdateID int
	,  @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbAgentStatus
			set IsDeleted = @IsDeleted
			, UpdateDateTime = @UpdateDateTime
			, UpdateID = @UpdateID
			where AgentStatusID = @AgentStatusID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateAgentStatus - AgentStatus was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateAgentStatus to web