use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateCompany') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateCompany
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160324
-- ============================================================
create procedure dbo.upUpdateCompany
(
	@CompanyID int
	, @CompanyName varchar(50) = null
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Only take action if at least one optional parameter is NOT null
	if @CompanyName is not null or @IsDeleted is not null
		begin
			-- Update record
			update dbo.tbCompany set 
				CompanyName = isnull(@CompanyName, CompanyName)
				, IsDeleted = isnull(@IsDeleted, IsDeleted)
				, UpdateID = @UpdateID
				, UpdateDateTime = getdate()
			where CompanyID = @CompanyID

			-- Check for success
			if (@@error = 0) and (@@rowcount > 0)
				begin
					set @Result = 'S'
				end
			else
				raiserror('upUpdateCompany - Company was not successfully Updated', 16, 1)
		end
end
go
grant exec on dbo.upUpdateCompany to web