use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateListingContactEmail') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateListingContactEmail
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upUpdateListingContactEmail
(
	@ListingContactEmailID int
	, @ListingContactTypeID int = null
	, @PersonEmailID int = null
	, @IsDeleted bit = null
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbListingContactEmail
			set ListingContactTypeID = isnull(@ListingContactTypeID, ListingContactTypeID)
			, PersonEmailID = isnull(@PersonEmailID, PersonEmailID)
			, IsDeleted = isnull(@IsDeleted, IsDeleted)
		where ListingContactEmailID = @ListingContactEmailID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateListingContactEmail - ListingContactEmail was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateListingContactEmail to web