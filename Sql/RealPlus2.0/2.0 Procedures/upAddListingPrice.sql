use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddListingPrice') and type in (N'P', N'PC'))
drop procedure dbo.upAddListingPrice
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddListingPrice
(
	@ListingID int
	, @ListingPrice money
	, @PriceDateTime datetime2
	, @CreateDateTime datetime2
	, @CreateID int
	, @ListingPriceID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbListingPrice
			(ListingID, ListingPrice, PriceDateTime, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@ListingID, @ListingPrice, @PriceDateTime, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @ListingPriceID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddListingPrice - ListingPrice was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddListingPrice to web