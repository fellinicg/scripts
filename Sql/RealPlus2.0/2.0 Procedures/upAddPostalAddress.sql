use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddPostalAddress') and type in (N'P', N'PC'))
drop procedure dbo.upAddPostalAddress
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160316
-- ============================================================
create procedure dbo.upAddPostalAddress
(
	 @Line1 varchar(80)
	, @CreateID int
	, @Line2 varchar(80) = null
	, @Line3 varchar(80) = null
	, @Line4 varchar(80) = null
	, @CityTown varchar(40) = null
	, @StateProvince varchar(40) = null
	, @PostalCode varchar(20) = null
	, @Country char(2) = null
	, @Latitude float = null
	, @Longitude float = null
	, @PostalAddressID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Insert record
	insert into dbo.tbPostalAddress(Line1, Line2, Line3, Line4, CityTown, StateProvince, PostalCode, Country, Latitude, Longitude, CreateID, UpdateID)
	values (@Line1, @Line2, @Line3, @Line4, @CityTown, @StateProvince, @PostalCode, @Country, @Latitude, @Longitude, @CreateID, @CreateID)

	-- Check for success
	if (@@error = 0) and (@@rowcount > 0)
		begin
			set @PostalAddressID = @@identity
			set @Result = 'S'
		end
	else
		raiserror('upAddPostalAddress - Address was not successfully added', 16, 1)
end
go
grant exec on dbo.upAddPostalAddress to web