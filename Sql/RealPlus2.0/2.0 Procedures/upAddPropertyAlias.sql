use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddPropertyAlias') and type in (N'P', N'PC'))
drop procedure dbo.upAddPropertyAlias
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddPropertyAlias
(
	@PropertyID int
	, @PropertyAliasName varchar(100)
	, @CreateDateTime datetime2
	, @CreateID int
	, @PropertyAliasID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbPropertyAlias
			(PropertyID, PropertyAliasName, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@PropertyID, @PropertyAliasName, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @PropertyAliasID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddPropertyAlias - PropertyAlias was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddPropertyAlias to web