use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetListingOpenHouses') and type in (N'P', N'PC'))
drop procedure dbo.upGetListingOpenHouses
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160405
-- ============================================================
create procedure dbo.upGetListingOpenHouses
	@ListingID int
	, @IncludeHistory bit = 0
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		loh.ListingOpenHouseID
		, dbo.sfGetPersonFullName(AgentID) 'Agent'
		, o.OfficeCode
		, o.Detail 'OfficeDetail'
		, loh.StartDateTime
		, loh.EndDateTime
		, loh.Comment
		, loh.AppointmentOnly
	from dbo.tbListingOpenHouse loh
		inner join dbo.tbOffice o on loh.OfficeID = o.OfficeID
	where loh.ListingID = @ListingID
		and loh.IsDeleted = 0
	order by loh.StartDateTime desc, loh.EndDateTime

end
go
grant exec on dbo.upGetListingOpenHouses to web