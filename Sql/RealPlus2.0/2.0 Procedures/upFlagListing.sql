use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upFlagListing') and type in (N'P', N'PC'))
drop procedure dbo.upFlagListing
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160315
-- ============================================================
create procedure dbo.upFlagListing
(
	@ListingID varchar(max)
	, @AgentID int
	, @CustomerID varchar(max) = null
	, @PickListed bit
	, @Liked bit
	, @Disiked bit
	, @Visited bit
	, @AllMyCustomers bit = 0
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @tblCustomerID table (CustomerID int null index ix1 clustered)

	-- Initialize variables
	set @Result = 'F'

	-- Check for AllMyCustomers flag
	if @AllMyCustomers = 1
		begin
			insert into @tblCustomerID
			select distinct
				c.CustomerID
			from dbo.tbCustomer c
				inner join dbo.tbPerson p on c.CustomerID = p.PersonID
			where c.AgentID = @AgentID
				and c.IsDeleted = 0
				and p.IsDeleted = 0
		end
	else
		begin
			insert into @tblCustomerID
			select ListItem from dbo.tfCommaListToTable(@CustomerID)

			if exists (select * from @tblCustomerID where CustomerID is not null)
				begin
					-- Check that CustomerIDs belong to AgentID
					if exists (select 1 from dbo.tbCustomer a right join @tblCustomerID b on a.AgentID = @AgentID and a.CustomerID = b.CustomerID where a.CustomerID is null)
						begin
							raiserror('upFlagListing - invalid agent and/or customer', 16, 1)
							return
						end
				end
		end

	-- Merge record
	merge dbo.tbAgentListingPin as dest
		using (
				select 
					l.ListItem as ListingID
					, @AgentID as AgentID
					, c.CustomerID as CustomerID
					, @PickListed as PickListed
					, @Liked as Liked
					, @Disiked as Disiked
					, @Visited as Visited 
				from dbo.tfCommaListToTable(@ListingID) l, @tblCustomerID c
		) as row on dest.ListingID = row.ListingID and dest.AgentID = row.AgentID and isnull(dest.CustomerID, 0) = isnull(row.CustomerID, 0)
    when not matched then
		insert(ListingID, AgentID, CustomerID, PickListed, Liked, Disiked, Visited, CreateID, UpdateID)
		values(row.ListingID, row.AgentID, row.CustomerID, row.PickListed, row.Liked, row.Disiked, row.Visited, row.AgentID, row.AgentID)
    when matched then
		update set dest.PickListed = row.PickListed, dest.Liked = row.Liked, dest.Disiked = row.Disiked, dest.Visited = row.Visited, dest.UpdateID = row.AgentID, dest.UpdateDateTime = getdate();

	-- Check for success
	if (@@error = 0) and (@@rowcount > 0)
		begin
			set @Result = 'S'
		end
	else
		raiserror('upFlagListing - listing was not successfully flagged', 16, 1)
end
go
grant exec on dbo.upFlagListing to web