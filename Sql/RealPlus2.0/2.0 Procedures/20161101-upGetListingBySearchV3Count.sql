USE [Jaguar_v2_Prototype2_BHS_2]
GO
/****** Object:  StoredProcedure [dbo].[upGetListingBySearchV3Count]    Script Date: 11/1/2016 7:55:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Kalpesh Patel
-- OrigCreateDate: 20160330
-- Last Modified By: Maunish Shah
-- Last Modified On: 09Aug2016
-- ============================================================
ALTER procedure [dbo].[upGetListingBySearchV3Count]
	  @ListingCategory int
	, @ActiveInactive varchar(3) = '0,1'				-- Deprecated
	, @ListingStatus varchar(max) = null				-- Quick, Advanced/Essentials, Advanced/Listing Details, Open House
	, @WebOrListingNum varchar(max) = null				-- Advanced/Essentials, Address, Open House, Sold
	, @PriceMin int = null								-- Quick, Advanced/Essentials, Open House, Sold
	, @PriceMax int = null								-- Quick, Advanced/Essentials, Open House, Sold
	, @Bedrooms float = null							-- Quick, Advanced/Essentials, Open House, Sold
	, @BedroomsPlus bit = 0								-- Quick, Advanced/Essentials, Open House, Sold
	, @Bathrooms float = null							-- Quick, Advanced/Essentials, Open House, Sold
	, @BathroomsPlus bit = 0							-- Quick, Advanced/Essentials, Open House, Sold
	, @HalfBathrooms float = null						-- Advanced/Essentials, Open House, Sold
	, @Rooms float = null								-- Advanced/Essentials, Open House, Sold
	, @RoomsPlus bit = 0								-- Advanced/Essentials, Open House, Sold
	, @MaxMthlyExp int = null							-- Advanced/Essentials, Open House, Sold
	, @OwnershipType varchar(max) = null				-- Advanced/Essentials, Advanced/Building Details, Open House, Sold
	, @ListDateStart varchar(20) = null					-- Advanced/Essentials
	, @ListDateEnd varchar(20) = null					-- Advanced/Essentials
	, @UpdateDateStart varchar(20) = null				-- Advanced/Essentials
	, @UpdateDateEnd varchar(20) = null					-- Advanced/Essentials
	, @ListUpdateDateStart varchar(20) = null			-- Advanced/Essentials
	, @ListUpdateDateEnd varchar(20) = null				-- Advanced/Essentials
	, @OpenHouseStart varchar(20) = null				-- Advanced/Essentials, Open House
	, @OpenHouseEnd varchar(20) = null					-- Advanced/Essentials, Open House
	, @Neighborhood varchar(max) = null					-- Advanced/Essentials, Advanced/Location Details, Address, Open House, Sold
	, @BuildingPeriod varchar(max) = null				-- Advanced/Essentials, Advanced/Building Details, Open House, Sold
	, @MiscEssentials varchar(max) = null				-- Advanced/Essentials, Advanced/Building Details, Open House, Sold
	, @MinSqFt int = null								-- Advanced/Apartment Details, Advanced/Townhouse-Home Details, Open House, Sold
	, @MaxPriceSqFt int = null							-- Advanced/Apartment Details, Advanced/Townhouse-Home Details, Open House, Sold
	, @IncMissingSqFt bit = null						-- Advanced/Apartment Details, Advanced/Townhouse-Home Details, Open House, Sold
	, @ApartmentType varchar(max) = null				-- Advanced/Apartment Details
	, @OutdoorSpace varchar(max) = null					-- Advanced/Apartment Details	
	, @Views varchar(max) = null						-- Advanced/Apartment Details
	, @Exposure varchar(max) = null						-- Advanced/Apartment Details
	, @MultiFloor varchar(max) = null					-- Advanced/Apartment Details
	, @TownhouseType varchar(max) = null				-- Advanced/Townhouse-Home Details
	, @THFeatures varchar(max) = null					-- Advanced/Townhouse-Home Details
	, @HomeType varchar(max) = null						-- Advanced/Townhouse-Home Details
	, @HFeatures varchar(max) = null					-- Advanced/Townhouse-Home Details
	, @ListActivityStatus varchar(max) = null			-- Advanced/Listing Details
	, @ListActivityStart varchar(20) = null				-- Advanced/Listing Details
	, @ListActivityEnd varchar(20) = null				-- Advanced/Listing Details
	, @ListingType varchar(max) = null					-- Advanced/Listing Details
	, @BuildingType varchar(max) = null					-- Advanced/Building Details
	, @BuildingPersonnel varchar(max) = null			-- Advanced/Building Details
	, @BuildingAmenity varchar(max) = null				-- Advanced/Building Details
	, @BuildingAllow varchar(max) = null				-- Advanced/Building Details
	, @BuildingNotAllow varchar(max) = null				-- Advanced/Building Details
	, @MinYearBuilt int = null							-- Advanced/Building Details, Advanced/Townhouse-Home Details
	, @ManagedBuildings varchar(max) = null				-- Advanced/Building Details
	, @ManagingAgent varchar(max) = null				-- Advanced/Building Details
	, @GridNorth1 varchar(max) = null							-- Advanced/Location Details
	, @GridNorth2 varchar(max) = null							-- Advanced/Location Details
	, @GridEast1 varchar(max) = null								-- Advanced/Location Details
	, @GridEast2 varchar(max) = null								-- Advanced/Location Details
	, @GridWest1 varchar(max) = null								-- Advanced/Location Details
	, @GridWest2 varchar(max) = null								-- Advanced/Location Details
	, @GridSouth1 varchar(max) = null							-- Advanced/Location Details
	, @GridSouth2 varchar(max) = null							-- Advanced/Location Details
	, @Zipcode varchar(max) = null						-- Advanced/Location Details
	, @FurnishedOpts varchar(max) = null				-- Rented
	, @Fee varchar(max) = null							-- Rented
	, @AddressID varchar(max) = null
	, @NewDevelopment varchar(10) = null	
	, @FinancingAllowed varchar(10) = null

	--- Added for Map related criteria and additional parameters given for UI changes while demo by dev team
	,@MapSelectedShapes varchar(max) = NULL-- map related
	,@circleRadius varchar(max) = null-- map related
	,@MinWidth float = null
	,@MinUnits float = null
	,@MinLeastTerm int = null
	,@MaxLeaseTerm int = null
	,@ListingCreditType varchar(max) = null
	,@LeaseTypeRental varchar(max) = null
	,@KeywordSearch varchar(max) = null
	,@OmniSearchHeader varchar(max) = NULL
	,@selectedNeighborhood_map varchar(max) = null-- map related
	,@MiscDetails varchar(max) = null	-- Advanced/Apartment Details
	,@shouldReturnList bit = 0 -- flag to identify whether to return the count or listingids column
	,@agensavedcriteriaid int = 0 -- internal for development
	,@AgentID  int = null-- internal for development
	,@Detail varchar(200) = null-- internal for development
	,@CustomerID int = null-- internal for development

	-- if any new parameters to be added, please add below this line
	, @FloorMin varchar(30) = null								-- Advanced/Apartment Details
	, @FloorMax varchar(30) = null								-- Advanced/Apartment Details
	, @IncPenthouses bit = 0									-- Advanced/Apartment Details
	, @searchByExclusiveAgent varchar(max) = null          -- ExclusiveAgent Name 
	, @searchByExclusiveFirm varchar(max) = null             -- ExclusiveAgent Firm
	, @UnitIdentifier varchar(max) = null					-- Unit # (NOT NUMERIC)
	, @ListAndActivityStatus varchar(max) = null-- JIRA 1490 start	
	, @SoldOrContractSignedDateStart varchar(20) = null
	, @SoldOrContractSignedDateEnd varchar(20) = null-- JIRA 1490 end
	, @loggedinUserID int = 0
as
begin
	
	/* internal by development team start*/
	-- Needed for Entity Framework to detect the integer value returned.
	SET FMTONLY OFF
	-- Initialize settings
	set nocount on
	set @MiscDetails=Replace(@MiscDetails,'[' ,'')
	set @MiscDetails=Replace(@MiscDetails,']' ,'')
	declare @intMapTotalRecordCount int =0
	-- parameter to check if Geo parameters set or not
	declare @blnGeoParameter bit=0
	declare @blnProcessMapShapes int=0
	declare @strGeoSepSQL varchar(max)=null
	

	declare @companyid int = 0
	set @companyid = isnull((SELECT tc.CompanyID
		FROM dbo.tbCompany tc
		INNER JOIN dbo.tbOffice [to] ON [to].CompanyID = tc.CompanyID
		INNER JOIN dbo.tbAgentOffice tao ON tao.OfficeID = [to].OfficeID
		WHERE tao.AgentID = @loggedinUserID),0)

	-- Mandatory to send parameters not selected as null section
	SELECT  @ListingCategory=CASE WHEN LTRIM(@ListingCategory) = 0 THEN NULL ELSE  LTRIM(@ListingCategory) END,
			@ActiveInactive=CASE WHEN LTRIM(@ActiveInactive) = '' THEN NULL ELSE  LTRIM(@ActiveInactive) END,
			@ListingStatus=CASE WHEN LTRIM(@ListingStatus) = '' THEN NULL ELSE  LTRIM(@ListingStatus) END,
			@WebOrListingNum=CASE WHEN LTRIM(@WebOrListingNum) = '' or  LTRIM(@WebOrListingNum)  = '0' THEN NULL ELSE  LTRIM(@WebOrListingNum) END,
			@PriceMin=CASE WHEN @PriceMin = 0 THEN NULL ELSE  @PriceMin END,
			@PriceMax=CASE WHEN @PriceMax = 0 THEN NULL ELSE  @PriceMax END,
			@Bedrooms= (CASE when @Bedrooms = -1 then 0 WHEN @Bedrooms = 0 THEN NULL ELSE @Bedrooms END),
			@Bathrooms=CASE WHEN @Bathrooms = 0 THEN NULL ELSE @Bathrooms END,
			@HalfBathrooms=CASE WHEN @HalfBathrooms = 0 THEN NULL ELSE  @HalfBathrooms END,
			@Rooms=CASE WHEN @Rooms = 0 THEN NULL ELSE  @Rooms END,
			@OwnershipType=CASE WHEN LTRIM(@OwnershipType) = ''  THEN NULL ELSE  @OwnershipType END,
			@ApartmentType=CASE WHEN LTRIM(@ApartmentType) = ''  THEN NULL ELSE  @ApartmentType END,
			@ListDateStart=CASE WHEN LTRIM(@ListDateStart) = ''  OR  LTRIM(@ListDateStart) = '1/1/0001 12:00:00 AM'  OR  LTRIM(@ListDateStart) = '01/01/0001 12:00:00 AM'  OR  LTRIM(@ListDateStart) = '01/01/0001 12:00:00'  THEN NULL ELSE  @ListDateStart END ,
			@ListDateEnd=CASE WHEN LTRIM(@ListDateEnd) = ''  OR  LTRIM(@ListDateEnd) = '1/1/0001 12:00:00 AM'  OR  LTRIM(@ListDateEnd) = '01/01/0001 12:00:00 AM'   OR  LTRIM(@ListDateEnd) = '01/01/0001 12:00:00'  THEN NULL ELSE  @ListDateEnd END ,
			@UpdateDateStart=CASE WHEN LTRIM(@UpdateDateStart) = ''  OR  LTRIM(@UpdateDateStart) = '1/1/0001 12:00:00 AM'  OR  LTRIM(@UpdateDateStart) = '01/01/0001 12:00:00 AM'   OR  LTRIM(@UpdateDateStart) = '01/01/0001 12:00:00'  THEN NULL ELSE  @UpdateDateStart END ,
			@UpdateDateEnd=CASE WHEN LTRIM(@UpdateDateEnd) = ''  OR  LTRIM(@UpdateDateEnd) = '1/1/0001 12:00:00 AM'  OR  LTRIM(@UpdateDateEnd) = '01/01/0001 12:00:00 AM'   OR  LTRIM(@UpdateDateEnd) = '01/01/0001 12:00:00'  THEN NULL ELSE  @UpdateDateEnd END ,
			@ListUpdateDateStart= CASE WHEN LTRIM(@ListUpdateDateStart) = ''  OR  @ListUpdateDateStart  = '1/1/0001 12:00:00 AM'   OR  LTRIM(@ListUpdateDateStart) = '01/01/0001 12:00:00 AM' OR  LTRIM(@ListUpdateDateStart) = '01/01/0001 12:00:00'   THEN NULL ELSE dbo.sfGetDatePart(@ListUpdateDateStart) END,
			@ListUpdateDateEnd= CASE WHEN LTRIM(@ListUpdateDateEnd) = ''  OR  @ListUpdateDateEnd  = '1/1/0001 12:00:00 AM'   OR  LTRIM(@ListUpdateDateEnd) = '01/01/0001 12:00:00 AM' OR  LTRIM(@ListUpdateDateEnd) = '01/01/0001 12:00:00'   THEN NULL ELSE dbo.sfGetDatePart(@ListUpdateDateEnd) END,
			@OpenHouseStart= CASE WHEN LTRIM(@OpenHouseStart) = ''  OR  @OpenHouseStart  = '1/1/0001 12:00:00 AM'   OR  LTRIM(@OpenHouseStart) = '01/01/0001 12:00:00 AM' OR  LTRIM(@OpenHouseStart) = '01/01/0001 12:00:00'   THEN NULL ELSE  dbo.sfGetDatePart(@OpenHouseStart) END,
			@OpenHouseEnd=  CASE WHEN LTRIM(@OpenHouseEnd) = ''  OR  @OpenHouseEnd  = '1/1/0001 12:00:00 AM'    OR  LTRIM(@OpenHouseEnd) = '01/01/0001 12:00:00 AM' OR  LTRIM(@OpenHouseEnd) = '01/01/0001 12:00:00'  THEN NULL ELSE  dbo.sfGetDatePart(@OpenHouseEnd) END,
			@ListActivityStart=  CASE WHEN LTRIM(@ListActivityStart) = ''  OR  @ListActivityStart  = '1/1/0001 12:00:00 AM'    OR  LTRIM(@ListActivityStart) = '01/01/0001 12:00:00 AM' OR  LTRIM(@ListActivityStart) = '01/01/0001 12:00:00'  THEN NULL ELSE  dbo.sfGetDatePart(@ListActivityStart) END,
			@ListActivityEnd=  CASE WHEN LTRIM(@ListActivityEnd) = ''  OR  @ListActivityEnd  = '1/1/0001 12:00:00 AM'    OR  LTRIM(@ListActivityEnd) = '01/01/0001 12:00:00 AM' OR  LTRIM(@ListActivityEnd) = '01/01/0001 12:00:00'  THEN NULL ELSE  dbo.sfGetDatePart(@ListActivityEnd) END,
			@BuildingPersonnel=CASE WHEN LTRIM(@BuildingPersonnel) = ''  THEN NULL ELSE  @BuildingPersonnel END,
			@MinSqFt= CASE  WHEN @MinSqFt = 0 THEN NULL ELSE @MinSqFt END,
			@MaxPriceSqFt=CASE WHEN LTRIM(@MaxPriceSqFt) = 0  THEN NULL ELSE  @MaxPriceSqFt END,
			@IncMissingSqFt=isnull(@IncMissingSqFt, 0),--CASE  WHEN @IncMissingSqFt = 0 THEN 0 ELSE @IncMissingSqFt END,
			@MaxMthlyExp=CASE WHEN LTRIM(@MaxMthlyExp) = 0  THEN NULL ELSE  @MaxMthlyExp END,
			@Neighborhood=CASE WHEN LTRIM(@Neighborhood) = ''  THEN NULL ELSE  @Neighborhood END,
			@BuildingPeriod=CASE WHEN LTRIM(@BuildingPeriod) = ''  THEN NULL ELSE  @BuildingPeriod END,
			@MiscEssentials=CASE WHEN LTRIM(@MiscEssentials) = ''  THEN NULL ELSE  @MiscEssentials END,
			--@MinSqFt=CASE WHEN LTRIM(@MinSqFt) = 0  THEN NULL ELSE  @MinSqFt END,
			@BuildingAmenity=CASE WHEN LTRIM(@BuildingAmenity) = ''  THEN NULL ELSE  @BuildingAmenity END,			
			@BuildingAllow=CASE WHEN LTRIM(@BuildingAllow) = ''  THEN NULL ELSE  @BuildingAllow END,
			@BuildingNotAllow=CASE WHEN LTRIM(@BuildingNotAllow) = ''  THEN NULL ELSE  @BuildingNotAllow END,			
			@ListActivityStatus=CASE WHEN LTRIM(@ListActivityStatus) = ''  THEN NULL ELSE  @ListActivityStatus END,
			@OutdoorSpace=CASE WHEN LTRIM(@OutdoorSpace) = ''  THEN NULL ELSE  @OutdoorSpace END,
			@Views=CASE WHEN LTRIM(@Views) = ''  THEN NULL ELSE  @Views END,
			@Exposure=CASE WHEN LTRIM(@Exposure) = ''  THEN NULL ELSE  @Exposure END,
			@MultiFloor=CASE WHEN LTRIM(@MultiFloor) = ''  THEN NULL ELSE  @MultiFloor END,
			@TownhouseType=CASE WHEN LTRIM(@TownhouseType) = ''  THEN NULL ELSE  @TownhouseType END,
			@THFeatures =CASE WHEN LTRIM(@THFeatures) = ''  THEN NULL ELSE  @THFeatures END,
			@HomeType=CASE WHEN LTRIM(@HomeType) = ''  THEN NULL ELSE  @HomeType END,
			@HFeatures=CASE WHEN LTRIM(@HFeatures) = ''  THEN NULL ELSE  @HFeatures END,
			@MinYearBuilt=CASE WHEN LTRIM(@MinYearBuilt) = 0  THEN NULL ELSE  @MinYearBuilt END,
			@ListingType=CASE WHEN LTRIM(@ListingType) = ''  THEN NULL ELSE  @ListingType END,
			@AddressID=CASE WHEN LTRIM(@AddressID) = ''  THEN NULL ELSE  @AddressID END,
			@bedroomsPlus  = CASE  WHEN @bedroomsPlus = 0 THEN 0 ELSE @bedroomsPlus END,
			@bathroomsPlus  = CASE  WHEN @bathroomsPlus = 0 THEN 0 ELSE @bathroomsPlus END,
			@roomsPlus  = CASE  WHEN @roomsPlus = 0 THEN 0 ELSE @roomsPlus END,
			@GridNorth1  = CASE  WHEN @GridNorth1 = '' or @GridNorth1 = '0'  THEN NULL ELSE  @GridNorth1  END,
			@GridNorth2  = CASE  WHEN @GridNorth2 = '' or @GridNorth2 = '0'  THEN NULL ELSE @GridNorth2 END,
			@GridEast1  = CASE  WHEN @GridEast1 = '' or @GridEast1 = '0' THEN NULL ELSE @GridEast1 END,
			@GridEast2  = CASE  WHEN @GridEast2 = '' or @GridEast2 = '0' THEN NULL ELSE @GridEast2 END,
			@GridWest1  = CASE  WHEN @GridWest1 = '' or @GridWest1 = '0' THEN NULL ELSE @GridWest1 END,
			@GridWest2  = CASE  WHEN @GridWest2 = '' or @GridWest2 = '0' THEN NULL ELSE @GridWest2 END,
			@GridSouth1  = CASE  WHEN @GridSouth1 = '' or @GridSouth1 = '0' THEN NULL ELSE @GridSouth1 END,
			@GridSouth2  = CASE  WHEN @GridSouth2 = '' or @GridSouth2 = '0' THEN NULL ELSE @GridSouth2 END,
			@MinWidth= CASE  WHEN @MinWidth = 0 THEN NULL ELSE @MinWidth END,
			@MinUnits= CASE  WHEN @MinUnits = 0 THEN NULL ELSE @MinUnits END,
			@MinLeastTerm= CASE  WHEN @MinLeastTerm = 0 THEN NULL ELSE @MinLeastTerm END,
			@MaxLeaseTerm= CASE  WHEN @MaxLeaseTerm = 0 THEN NULL ELSE @MaxLeaseTerm END,
			@BuildingType=CASE WHEN LTRIM(@BuildingType) = ''  THEN NULL ELSE  @BuildingType END,
			@ManagedBuildings=CASE WHEN LTRIM(@ManagedBuildings) = ''  THEN NULL ELSE  @ManagedBuildings END,
			@ManagingAgent=CASE WHEN LTRIM(@ManagingAgent) = ''  THEN NULL ELSE  @ManagingAgent END,
			@Zipcode=CASE WHEN LTRIM(@Zipcode) = ''  THEN NULL ELSE  @Zipcode END,
			@FurnishedOpts=CASE WHEN LTRIM(@FurnishedOpts) = ''  THEN NULL ELSE  @FurnishedOpts END,
			@Fee=CASE WHEN LTRIM(@Fee) = ''  THEN NULL ELSE  @Fee END,
			@NewDevelopment=CASE WHEN LTRIM(@NewDevelopment) = ''  THEN NULL ELSE  @NewDevelopment END,
			@FinancingAllowed=CASE WHEN LTRIM(@FinancingAllowed) = ''  THEN NULL ELSE  @FinancingAllowed END,
			@ListingCreditType=CASE WHEN LTRIM(@ListingCreditType) = ''  THEN NULL ELSE  @FinancingAllowed END,
			@LeaseTypeRental=CASE WHEN LTRIM(@LeaseTypeRental) = ''  THEN NULL ELSE  @LeaseTypeRental END,
			@KeywordSearch=CASE WHEN LTRIM(@KeywordSearch) = ''  THEN NULL ELSE  @KeywordSearch END,
			@OmniSearchHeader=CASE WHEN LTRIM(@OmniSearchHeader) = ''  THEN NULL ELSE  @OmniSearchHeader END,
			@MapSelectedShapes=CASE WHEN LTRIM(@MapSelectedShapes) = ''  THEN NULL ELSE  @MapSelectedShapes END,
			@circleRadius=CASE WHEN LTRIM(@circleRadius) = ''  THEN NULL ELSE  @circleRadius END,
			@MiscDetails=CASE WHEN LTRIM(@MiscDetails) = ''  THEN NULL ELSE  @MiscDetails END,
			@searchByExclusiveAgent=CASE WHEN LTRIM(@searchByExclusiveAgent) = ''  THEN NULL ELSE  @searchByExclusiveAgent END,
			@searchByExclusiveFirm=CASE WHEN LTRIM(@searchByExclusiveFirm) = ''  THEN NULL ELSE  @searchByExclusiveFirm END,
			@FloorMin  = CASE  WHEN @FloorMin = 0 THEN NULL ELSE @FloorMin END,
			@FloorMax  = CASE  WHEN @FloorMax = 0 THEN NULL ELSE @FloorMax END,
			@UnitIdentifier = case when ltrim(@UnitIdentifier) = '' then null else @UnitIdentifier end,
			@ListAndActivityStatus =CASE WHEN LTRIM(@ListAndActivityStatus) = ''  THEN NULL ELSE  @ListAndActivityStatus END,-- JIRA 1490 start	
			@SoldOrContractSignedDateStart=CASE WHEN LTRIM(@SoldOrContractSignedDateStart) = ''  OR  LTRIM(@SoldOrContractSignedDateStart) = '1/1/0001 12:00:00 AM'  OR  LTRIM(@SoldOrContractSignedDateStart) = '01/01/0001 12:00:00 AM'   OR  LTRIM(@SoldOrContractSignedDateStart) = '01/01/0001 12:00:00'  THEN NULL ELSE  @SoldOrContractSignedDateStart END ,
			@SoldOrContractSignedDateEnd=CASE WHEN LTRIM(@SoldOrContractSignedDateEnd) = ''  OR  LTRIM(@SoldOrContractSignedDateEnd) = '1/1/0001 12:00:00 AM'  OR  LTRIM(@SoldOrContractSignedDateEnd) = '01/01/0001 12:00:00 AM'   OR  LTRIM(@SoldOrContractSignedDateEnd) = '01/01/0001 12:00:00'  THEN NULL ELSE  @SoldOrContractSignedDateEnd END-- JIRA 1490 end	
			 								
	
	SET @Zipcode = REPLACE(@Zipcode,'$$',',')
	SET @AddressID = REPLACE(@AddressID,'$$',',')
	SET @ManagingAgent = REPLACE(@ManagingAgent,'$$',',')
	SET @ManagingAgent = REPLACE(@ManagingAgent,'-',' ')
	SET @searchByExclusiveFirm = REPLACE(@searchByExclusiveFirm,'$$',',')
	SET @WebOrListingNum=REPLACE(@WebOrListingNum,'Web:','')
	SET @WebOrListingNum=REPLACE(@WebOrListingNum,'Listing:','')
	SET @WebOrListingNum=REPLACE(@WebOrListingNum,'$$',',')
	set @UnitIdentifier = replace(@UnitIdentifier, '$$',',')
	set @Neighborhood=replace(@Neighborhood, '$$',',')
	set @searchByExclusiveAgent = REPLACE(@searchByExclusiveAgent,'$$',',')
	
	-- New code  to check balcony existing on the outdoorspace or not if existing adding terrace attribute as default. 

	--if @OutdoorSpace is not null
	--begin 
	--print charindex('208',@OutdoorSpace)
	--	if charindex('208',@OutdoorSpace)>0 
	--	begin
	--	 set @OutdoorSpace=@OutdoorSpace +',213'
	--	end 
	--	print @OutdoorSpace
	--end 
	
			


	DECLARE @WorkInProgress XML
	-- Make non selected parameters to null section ends
	/* internal by development team ends*/
	
	
	/*(For Todd to update/add search related logic) Starts here*/

	-- Declarations



	-- new code to get all children neibhourhood. 

/*if @Neighborhood is not null 
begin 
	--declare @childNeighborhood as varchar(max) 
	--declare @childNeighborhood2 as varchar(max) 
	
	--	 (select @childNeighborhood=COALESCE(@childNeighborhood + ',','') + cast(outtav.attributevalueid as varchar(20)) 
	--	from tbvalue tv
	--	 inner join 
	--	 tbattributevalue outtav 
	--	 on outtav.valueid=tv.valueid 
	--	 inner join (
	--	select  tav.valueid as innervalue
	--	from 
	--	 tbattributevalue tav 
	--	 inner join dbo.tfcommalisttotable(@Neighborhood)  tbn
	--	 on tav.attributevalueid =tbn.ListItem
	--	where tav.attributeid=1215 and  tav.isdeleted=0) as innertb
	--	on tv.valuecode=innertb.innervalue where tv.isdeleted=0 and tv.valuecode is not null)
		
	--	if @childNeighborhood <>''
	--	begin 
 --        (select @childNeighborhood2=COALESCE(@childNeighborhood2 + ',','') + cast(outtav.attributevalueid as varchar(20)) 
	--	from tbvalue tv
	--	 inner join 
	--	 tbattributevalue outtav 
	--	 on outtav.valueid=tv.valueid 
	--	 inner join (
	--	select  tav.valueid as innervalue
	--	from 
	--	 tbattributevalue tav 
	--	 inner join dbo.tfcommalisttotable(@childNeighborhood)  tbn
	--	 on tav.attributevalueid =tbn.ListItem
	--	where tav.attributeid=1215 and  tav.isdeleted=0) as innertb
	--	on tv.valuecode=innertb.innervalue where tv.isdeleted=0 and tv.valuecode is not null)

	--	if @childNeighborhood2 <>''
	--	begin 
	--	set @Neighborhood= @Neighborhood +','+ @childNeighborhood +','+@childNeighborhood2
	--	end 
	--	else
	--	begin
	--	set @Neighborhood= @Neighborhood +','+ @childNeighborhood
	--	end 
	--	end 
	--	print @Neighborhood

	--declare @flag bit
	--declare @level int
	--declare @childNeighborhood as varchar(max) 
	--declare @tblNH_list table (
	--	  uniq int identity(1,1)
	--	, ID int
	--	, ParentID int
	--	, ValueName varchar(max)
	--	, ValueID  int
	--	, orderNum int)

	--set @flag = 1
	--set @level = 0

	--insert into @tblNH_list(ID,ParentID,ValueName,ValueID, OrderNum)
	--select AttributeValueID as ID
	--		, cast(V.ValueCode as int) as ParentID
	--		, V.ValueName
	--		, V.ValueID
	--		, 0
	--from tbAttribute A
	--	inner join tbAttributeValue AV
	--		on AV.AttributeId = A.AttributeId
	--	inner join tbValue V
	--		on V.ValueID = AV.ValueID
	--	inner join dbo.tfcommalisttotable(@Neighborhood)  tbn
	--	 on av.attributevalueid =tbn.ListItem
	--where A.AttributeID = 1215 
	--	and V.IsDeleted = 0 
	--order by V.ValueCode

	--WHILE (@flag = 1)
	--	begin
	--	set @level = @level + 1
		
	--	insert into @tblNH_list(ID,ParentID,ValueName,ValueID,orderNum)
	--	select	  av.AttributeValueID as ID
	--			, PAV.AttributeValueID as ParentID
	--			, V.ValueName
	--			, V.ValueID
	--			, @level
	--	from tbAttribute A
	--		inner join tbAttributeValue AV
	--			on AV.AttributeId = A.AttributeId					
	--		inner join tbValue V
	--			on V.ValueID = AV.ValueID
	--		inner join tbAttributeValue PAV
	--			on PAV.ValueID = cast(V.ValueCode as int)
	--		left join @tblNH_list sr
	--			on sr.ID = av.AttributeValueID
	--	where A.AttributeID = 1215 
	--		and sr.ID is null
	--		and V.IsDeleted = 0 
	--		and cast(V.ValueCode as int) in (select ValueID from @tblNH_list)
	--	order by case when @level = 1 then CAST(av.AttributeValueID as varchar)  else v.ValueName end

	--	if (select	count(*)
	--		from tbAttribute A
	--			inner join tbAttributeValue AV
	--				on AV.AttributeId = A.AttributeId
	--			inner join tbValue V
	--				on V.ValueID = AV.ValueID
	--			left join @tblNH_list sr
	--				on sr.ID = av.AttributeValueID
	--		where A.AttributeID = 1215 
	--			and sr.ID is null
	--			and V.IsDeleted = 0 
	--			and cast(V.ValueCode as int) in (select ValueID from @tblNH_list)) = 0
	--		set @flag = 0
	--	end
		
	--	set @childNeighborhood = ''
	--	select @childNeighborhood=COALESCE(@childNeighborhood + ',','') + cast(ID as varchar(20)) 
	--	from @tblNH_list as l
	--		inner join  dbo.tfcommalisttotable(@Neighborhood) as n
	--			on  CAST(n.ListItem AS INT) <> l.id
	--	if isnull(@childNeighborhood, '') <> ''
	--		set @Neighborhood= @Neighborhood + @childNeighborhood

end */
-- end new code for neighboorhood

	declare @sql varchar(max) = ''
	declare @sqltmp varchar(max) = ''
	declare @th_sql varchar(max) = ''
	declare @geog_sql varchar(max) = ''

	declare @tblresult table (ListingID int index ix1 clustered)
	declare @listingCount int = 0
			declare @locpos as int
	IF (@GridNorth1 is not null)
	BEGIN 
		set @locpos =charindex('_',@gridNorth1)
		if (@locpos>0) set @gridNorth1=substring(@gridNorth1,1,@locpos-1)
	 END 

	IF (@GridNorth2 is not null)
	BEGIN 
		set @locpos =charindex('_',@GridNorth2)
		if (@locpos>0) set @GridNorth2=substring(@GridNorth2,1,@locpos-1)
	END 
	
	IF (@GridEast1 is not null)
	BEGIN 
		set @locpos =charindex('_',@GridEast1)
		if (@locpos>0) set @GridEast1=substring(@GridEast1,1,@locpos-1)
	END 

	IF (@GridEast2 is not null)
	BEGIN 
		set @locpos =charindex('_',@GridEast2)
		if (@locpos>0) set @GridEast2=substring(@GridEast2,1,@locpos-1)
	END 		
	IF (@GridWest1 is not null)
	BEGIN 
		set @locpos =charindex('_',@GridWest1)
		if (@locpos>0) set @GridWest1=substring(@GridWest1,1,@locpos-1)
	END	
	IF (@GridWest2 is not null)
	BEGIN 
		set @locpos =charindex('_',@GridWest2)
		if (@locpos>0) set @GridWest2=substring(@GridWest2,1,@locpos-1)
	END
	IF (@GridSouth1 is not null)
	BEGIN 
		set @locpos =charindex('_',@GridSouth1)
		if (@locpos>0) set @GridSouth1=substring(@GridSouth1,1,@locpos-1)
	END	
	
	IF (@GridSouth2 is not null)
	BEGIN 
		set @locpos =charindex('_',@GridSouth2)
		if (@locpos>0) set @GridSouth2=substring(@GridSouth2,1,@locpos-1)
	END		

	-- Grid
	if (@GridNorth1 is not null or @GridSouth1 is not null or @GridEast1 is not null or @GridWest1 is not null)
		begin
		-- Set Geo Parameter to 1 for Geo Parameter
		set @blnGeoParameter=1
			set @geog_sql =  @geog_sql + '
						(
						select l.ListingID
						from (
								select PropertyID
								from tbPropertyAttributeNumeric
								where AttributeID = 1211 and AttributeValue <= ' + cast(isnull(@GridNorth1, 1000000) as varchar(10)) + '
								intersect
								select PropertyID
								from tbPropertyAttributeNumeric
								where AttributeID = 1212 and AttributeValue >= ' + cast(isnull(@GridSouth1, 0) as varchar(10)) + '
								intersect
								select PropertyID
								from tbPropertyAttributeNumeric
								where AttributeID = 1213 and AttributeValue >= ' + cast(isnull(@GridEast1, 1000000) as varchar(10)) + '
								intersect
								select PropertyID
								from dbo.tbPropertyAttributeNumeric
								where AttributeID = 1214 and AttributeValue <= ' + cast(isnull(@GridWest1, 0) as varchar(10)) + '
							) pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where l.IsDeleted = 0'
				
			if (@GridNorth2 is not null or @GridSouth2 is not null or @GridEast2 is not null or @GridWest2 is not null)
				-- Set Geo Parameter to 1 for Geo Parameter
				set @blnGeoParameter=1
				set @geog_sql = @geog_sql + '
						union
						select l.ListingID
						from (
								select PropertyID
								from tbPropertyAttributeNumeric
								where AttributeID = 1211 and AttributeValue <= ' + cast(isnull(@GridNorth2, 1000000) as varchar(10)) + '
								intersect
								select PropertyID
								from tbPropertyAttributeNumeric
								where AttributeID = 1212 and AttributeValue >= ' + cast(isnull(@GridSouth2, 0) as varchar(10)) + '
								intersect
								select PropertyID
								from tbPropertyAttributeNumeric
								where AttributeID = 1213 and AttributeValue >= ' + cast(isnull(@GridEast2, 1000000) as varchar(10)) + '
								intersect
								select PropertyID
								from dbo.tbPropertyAttributeNumeric
								where AttributeID = 1214 and AttributeValue <= ' + cast(isnull(@GridWest2, 0) as varchar(10)) + '
							) pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where l.IsDeleted = 0'
			set @geog_sql = @geog_sql + '
			            ) union'
		end

	-- AddressID
	if @AddressID is not null 
		begin
		-- Set Geo Parameter to 1 for Geo Parameter
		set @blnGeoParameter=1
			set @geog_sql =  @geog_sql + '
						(
						select l.ListingID
						from dbo.properties pa
							inner join dbo.tfcommalisttotable(' + char(39) + @AddressID + char(39) + ') a on pa.rp_bin = a.ListItem
							inner join dbo.tbProperty p on pa.propertyId = p.EtlID and p.PropertyClassID = 1
							inner join dbo.tbProperty pp on p.PropertyID = pp.ParentPropertyID
							inner join dbo.tbListing l on pp.PropertyID = l.PropertyID
						) union'
		end

	-- Zipcode
	if @Zipcode is not null 
		begin
		-- Set Geo Parameter to 1 for Geo Parameter
		set @blnGeoParameter=1
			set @geog_sql =  @geog_sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeText pat 
							inner join dbo.tfCommaListToTable(' + char(39) + @Zipcode + char(39) + ') on AttributeValue = ListItem
							inner join dbo.tbListing l on pat.PropertyID = l.PropertyID
						where pat.IsDeleted = 0
							and pat.AttributeID in (1259)
						) union'
		end

	-- Neighborhood
	if @Neighborhood is not null 
		begin
		-- Set Geo Parameter to 1 for Geo Parameter
		set @blnGeoParameter=1
			set @geog_sql =  @geog_sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							--inner join dbo.tfCommaListToTable(' + char(39) + @Neighborhood + char(39) + ') t on pa.AttributeValueID = cast(t.ListItem as int)
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						where pa.AttributeValueID in ('  + @Neighborhood +  ')
						union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							--inner join dbo.tfCommaListToTable(' + char(39) + @Neighborhood + char(39) + ') t on pa.AttributeValueID = cast(t.ListItem as int)
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pa.AttributeValueID in ('  + @Neighborhood +  ')
						) union'
		end

	-- UnitIdentifier
	if @UnitIdentifier is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeText pat 
							inner join dbo.tfCommaListToTable(' + char(39) + @UnitIdentifier + char(39) + ') on AttributeValue = ListItem
							inner join dbo.tbListing l on pat.PropertyID = l.PropertyID
						where pat.IsDeleted = 0
							and pat.AttributeID in (100)
						) intersect'
		end

	-- WebOrListingNum
	if @WebOrListingNum is not null 
		begin
			--set @sql = @sql + '
			--			(
			--			select distinct lat.ListingID
			--			from dbo.tbListingAttributeText lat 
			--				inner join dbo.tfCommaListToTable(' + char(39) + @WebOrListingNum + char(39) + ') on AttributeValue = ListItem
			--			where lat.IsDeleted = 0
			--				and lat.AttributeID in (54,53)
			--			) intersect'

			set @sql = @sql + '
						(
						select distinct lat.ListingID
						from dbo.tbListingAttributeText lat 							
						inner join listings ll on ll.listingid = lat.listingid
						where lat.IsDeleted = 0 and lat.AttributeID in (54,53) and lat.AttributeValue in ('+ @WebOrListingNum +')  and ( secure_listings_flag = ''N'' or secure_listings_flag is null)

						union
						select tl.listingid
						from dbo.tbListing tl
						inner join listings ll on ll.listingid = tl.listingid
						left join [who_can_see_it] wcs on wcs.recordid = tl.listingid and wcs.recordtype = ''L''
						left join secure_listings sl on sl.listingid = tl.listingid
						where sl.listingnumber in ' + cast(@WebOrListingNum as varchar) + ' and secure_listings_flag = ''Y'' and (
						SL.secure_group in (select securegroup from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ') or 
														(SL.secure_group in (select parentgroupname from tbsecuregroups where companyid =  ' + cast( @companyid as varchar)  + ')))
														) intersect'
		end

	-- added by Dev Team on Jen's req
	-- PriceMin
	if (@PriceMin is not null and @PriceMax is null) 
		begin
			--set @sql = @sql + '
			--			(
			--			select ListingID
			--			from dbo.tbListing 
			--			where CurrentPrice > ' + cast(@PriceMin as varchar) + '
			--			) intersect'

			set @sql = @sql + '
			(select tl.ListingID
			from dbo.tbListing tl
			inner join listings ll on ll.listingid = tl.listingid
			where originalprice > ' + cast(@PriceMin as varchar) + ' and ( secure_listings_flag = ''N'' or secure_listings_flag is null)
			union
			select tl.listingid
			from dbo.tbListing tl
			inner join listings ll on ll.listingid = tl.listingid
			left join [who_can_see_it] wcs on wcs.recordid = tl.listingid and wcs.recordtype = ''L''
			left join secure_listings sl on sl.listingid = tl.listingid
			where sl.originalprice > ' + cast(@PriceMin as varchar) + ' and secure_listings_flag = ''Y'' and (
			SL.secure_group in (select securegroup from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ') or 
											(SL.secure_group in (select parentgroupname from tbsecuregroups where companyid =  ' + cast( @companyid as varchar)  + ')))
											) intersect'

		end

	-- added by Dev Team on Jen's req
		-- PriceMax
	if (@PriceMin is null and @PriceMax is not null) 
		begin
			--set @sql = @sql + '
			--			(
			--			select ListingID
			--			from dbo.tbListing 
			--			where CurrentPrice < ' + cast(@PriceMax as varchar) + '
			--			) intersect'
			set @sql = @sql + '
			(select tl.ListingID
			from dbo.tbListing tl
			inner join listings ll on ll.listingid = tl.listingid
			where CurrentPrice < ' + cast(@PriceMax as varchar) + ' and ( secure_listings_flag = ''N'' or secure_listings_flag is null)
			union
			select tl.listingid
			from dbo.tbListing tl
			inner join listings ll on ll.listingid = tl.listingid
			left join [who_can_see_it] wcs on wcs.recordid = tl.listingid and wcs.recordtype = ''L''
			left join secure_listings sl on sl.listingid = tl.listingid
			where sl.originalprice < ' + cast(@PriceMax as varchar) + ' and secure_listings_flag = ''Y'' and (
			SL.secure_group in (select securegroup from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ') or 
											(SL.secure_group in (select parentgroupname from tbsecuregroups where companyid =  ' + cast( @companyid as varchar)  + ')))
											) intersect'
		end

		-- PriceMin and PriceMax
	if (@PriceMin is not null and @PriceMax is not null) 
		begin
			--set @sql = @sql + '
			--			(
			--			select ListingID
			--			from dbo.tbListing 
			--			where CurrentPrice between ' + cast(@PriceMin as varchar) + ' and ' + cast(@PriceMax as varchar) + '
			--			) intersect'

			set @sql = @sql + '(select tl.ListingID
								from dbo.tbListing tl
								inner join listings ll on ll.listingid = tl.listingid
								where CurrentPrice between ' + cast(@PriceMin as varchar) + ' and ' + cast(@PriceMax as varchar) + ' and ( secure_listings_flag = ''N'' or secure_listings_flag is null)
								union
								select tl.listingid
								from dbo.tbListing tl
								inner join listings ll on ll.listingid = tl.listingid
								left join [who_can_see_it] wcs on wcs.recordid = tl.listingid and wcs.recordtype = ''L''
								left join secure_listings sl on sl.listingid = tl.listingid
								where sl.originalprice between ' + cast(@PriceMin as varchar) + ' and ' + cast(@PriceMax as varchar) + ' and secure_listings_flag = ''Y'' and (
								SL.secure_group in (select securegroup from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ') or 
								(SL.secure_group in (select parentgroupname from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ')))) intersect'

		end

	-- @FloorMin and @FloorMax
	if (@FloorMin is not null or @FloorMax is not null)
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.AttributeID = 111 
							and pan.AttributeValue between isnull(' + @FloorMin + ', 0) and isnull(' + @FloorMax + ', 10000000000)'
			if @IncPenthouses = 1
				set @sql = @sql + '
						union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tbProperty p on pa.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pa.AttributeValueID = 298) intersect'
			else
				set @sql = @sql + ') intersect'
		end

	-- MinYearBuilt
	if @MinYearBuilt is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 106 
							and AttributeValue >= ' + cast(@MinYearBuilt as varchar) + '
						union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 106 
							and AttributeValue >= ' + cast(@MinYearBuilt as varchar) + '
						) intersect'
		end

	-- ManagingAgent
	if @ManagingAgent is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeText pat 
							inner join dbo.tbProperty p on pat.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pat.IsDeleted = 0
							and pat.AttributeID = 1184
							and charindex(' + char(39) + @ManagingAgent + char(39) + ', AttributeValue) > 0
						) intersect'
		end

	-- Bedrooms
	if @Bedrooms is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 11 
							and AttributeValue ' + case when isnull(@BedroomsPlus, 0) = 1 then '>' else '' end + '= ' + cast(@Bedrooms as varchar) + '
						union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 11 
							and AttributeValue ' + case when isnull(@BedroomsPlus, 0) = 1 then '>' else '' end + '= ' + cast(@Bedrooms as varchar) + '
						) intersect'
		end
	
	-- Bathrooms
	if @Bathrooms is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 10 
							and AttributeValue ' + case when isnull(@BathroomsPlus, 0) = 1 then '>' else '' end + '= ' + cast(@Bathrooms as varchar) + '
						union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 10 
							and AttributeValue ' + case when isnull(@BathroomsPlus, 0) = 1 then '>' else '' end + '= ' + cast(@Bathrooms as varchar) + '
						) intersect'
		end

	-- HalfBathrooms
	if @HalfBathrooms is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 41 
							and AttributeValue >= ' + cast(@HalfBathrooms as varchar) + '
						union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 41 
							and AttributeValue >= ' + cast(@HalfBathrooms as varchar) + '
						) intersect'
		end

	-- Rooms
	if @Rooms is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 85 
							and AttributeValue ' + case when isnull(@RoomsPlus, 0) = 1 then '>' else '' end + '= ' + cast(@Rooms as varchar) + '
						union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 85 
							and AttributeValue ' + case when isnull(@RoomsPlus, 0) = 1 then '>' else '' end + '= ' + cast(@Rooms as varchar) + '
						) intersect'
		end

	-- MinSqFt
	if @MinSqFt is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 91 
							and AttributeValue >= ' + cast(@MinSqFt as varchar) + '
						union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 91 
							and AttributeValue >= ' + cast(@MinSqFt as varchar)
			if @IncMissingSqFt = 1
				set @sql = @sql + '
						union
						select l.ListingID
						from dbo.tbListing l
							left join dbo.tbPropertyAttributeNumeric pan on l.PropertyID = pan.PropertyID and pan.AttributeID = 91
						where pan.PropertyAttributeNumericID is null or pan.AttributeValue = 0 ) intersect'
			else
				set @sql = @sql + ') intersect'
		end

	-- MaxPriceSqFt
	if @MaxPriceSqFt is not null 
		begin
			set @sql = @sql + '
						(
						select lan.ListingID
						from dbo.tbListingAttributeNumeric lan
						where lan.IsDeleted = 0 
							and lan.AttributeID = 79
							and AttributeValue <= ' + cast(@MaxPriceSqFt as varchar) + '
						) intersect'
		END

	-- MinLeaseTerm
	if @MinLeastTerm is not null and @MaxLeaseTerm is null
		begin
			set @sql = @sql + '
						(
						select lan.ListingID
						from dbo.tbListingAttributeNumeric lan
						where lan.IsDeleted = 0 
							and lan.AttributeID = 1251
							and AttributeValue >= ' + cast(@MinLeastTerm as varchar) + '
						) intersect'
		end
	
	-- MaxLeaseTerm
	if @MaxLeaseTerm is not null and @MinLeastTerm is null
		begin
			set @sql = @sql + '
						(
						select lan.ListingID
						from dbo.tbListingAttributeNumeric lan
						where lan.IsDeleted = 0 
							and lan.AttributeID = 1252
							and lan.AttributeValue <= ' + cast(@MaxLeaseTerm as varchar) + '
						) intersect'
		end
	
	-- added by development team
	if @MinLeastTerm is not null and @MaxLeaseTerm is not null
	begin
	set @sql = @sql + '
						(
						select lan.ListingID
						from dbo.tbListingAttributeNumeric lan
						where lan.IsDeleted = 0 
							and lan.AttributeID = 1252
							and lan.AttributeValue between ' + cast(@MinLeastTerm as varchar) + ' and ' + cast(@MaxLeaseTerm as varchar) + '
						) intersect'
	end
	
	-- MaxMthlyExp
	if @MaxMthlyExp is not null 
		begin
			set @sql = @sql + '
						(
						select lan.ListingID
						from dbo.tbListingAttributeNumeric lan
						where lan.IsDeleted = 0 
							and lan.AttributeID = 76
							and lan.AttributeValue <= ' + cast(@MaxMthlyExp as varchar) + '
						) intersect'
		end
	
	-- ListDateStart and ListDateEnd
	if (@ListDateStart is not null or @ListDateEnd is not null) 
		begin
			--set @sql = @sql + '
			--			(
			--			select ListingID
			--			from dbo.tbListingActivity
			--			where convert(date, ActivityDateTime) between ' + char(39) + @ListDateStart + char(39) + ' and ' + char(39) + @ListDateEnd + char(39) + '
			--				and ActivityID = 2549
			--			) intersect'

			set @sql = @sql + '
						(
							select LA.ListingID
							from dbo.tbListingActivity LA
							inner join listings ll on ll.listingid = LA.listingid
							where convert(date, ActivityDateTime) between ' + char(39) + @ListDateStart + char(39) + ' and ' + char(39) + @ListDateEnd + char(39) + '
							and ActivityID = 2549  and  ( secure_listings_flag = ''N'' or secure_listings_flag is null)

							union

							select tl.listingid
							from dbo.tbListing tl
							inner join listings ll on ll.listingid = tl.listingid
							left join [who_can_see_it] wcs on wcs.recordid = tl.listingid and wcs.recordtype = ''L''
							left join secure_listings sl on sl.listingid = tl.listingid
							where datelisted between ' + char(39) + @ListDateStart + char(39) + ' and ' + char(39) + @ListDateEnd + char(39) + ' and secure_listings_flag = ''Y'' and (
							SL.secure_group in (select securegroup from tbsecuregroups where companyid = ' + cast( @companyid as varchar) + ' ) or 
															(SL.secure_group in (select parentgroupname from tbsecuregroups where companyid = ' + cast( @companyid as varchar) + ')))

						) intersect'
		end

	-- UpdateDateStart and UpdateDateEnd
	if (@UpdateDateStart is not null or @UpdateDateEnd is not null) 
		begin
			
			-- commented as the logic seems incorrect based on JIRA - 1600
			--set @sql = @sql + '
			--			(
			--			select listingid
			--			from dbo.tbListing 
			--			where IsDeleted = 0
			--				and convert(date, UpdateDateTime) between ' + char(39) + @UpdateDateStart + char(39) + ' and ' + char(39) + @UpdateDateEnd + char(39) + ' 
			--			) intersect'
			
			--set @sql = @sql + '
			--			(
			--			select listingid
			--			from listings bbl
			--			where 
			--			--IsDeleted = 0
			--				--and 
			--				convert(date, updted) between ' + char(39) + @UpdateDateStart + char(39) + ' and ' + char(39) + @UpdateDateEnd + char(39) + ' 
			--			) intersect'

			set @sql = @sql + '
						(
							select listingid
							from listings bbl
							where convert(date, updted) between ' + char(39) + @UpdateDateStart + char(39) + ' and ' + char(39) + @UpdateDateEnd + char(39) + ' and ( secure_listings_flag = ''N'' or secure_listings_flag is null)

							union

							select ll.listingid
							from listings ll
							left join [who_can_see_it] wcs on wcs.recordid = ll.listingid and wcs.recordtype = ''L''
							left join secure_listings sl on sl.listingid = ll.listingid
							where datelisted between ' + char(39) + @ListDateStart + char(39) + ' and ' + char(39) + @ListDateEnd + char(39) + ' and secure_listings_flag = ''Y'' and (
							SL.secure_group in (select securegroup from tbsecuregroups where companyid = ' + cast( @companyid as varchar) + ' ) or 
															(SL.secure_group in (select parentgroupname from tbsecuregroups where companyid = ' + cast( @companyid as varchar) + ')))
						) intersect'

		end

	-- ListUpdateDateStart and ListUpdateDateEnd
	
	if (@ListUpdateDateStart is not null or @ListUpdateDateEnd is not null) 
		begin
			set @sql = @sql + '
						(
						select ListingID
						from dbo.tbListingActivity
						where convert(date, ActivityDateTime) between ' + char(39) + @ListUpdateDateStart + char(39) + ' and ' + char(39) + @ListUpdateDateEnd + char(39) + '
							and ActivityID = 2549
						union
						select ListingID
						from dbo.tbListing 
						where IsDeleted = 0
							and convert(date, UpdateDateTime) between ' + char(39) + @ListUpdateDateStart + char(39) + ' and ' + char(39) + @ListUpdateDateEnd + char(39) + ' 
						) intersect'
			
				
		end

	-- OpenHouseStart and OpenHouseEnd
	if (@OpenHouseStart is not null or @OpenHouseEnd is not null) 
		begin

			-- modified Todd's logic and added distinct keyword to fetch unique listings
			set @sql = @sql + '
						(
						select distinct ListingID
						from dbo.tbListingOpenHouse 
						where IsDeleted = 0
							and (convert(date, StartDateTime) between ' + char(39) + @OpenHouseStart + char(39) + ' and ' + char(39) + @OpenHouseEnd + char(39) + ' 
								or convert(date, EndDateTime) between ' + char(39) + @OpenHouseStart + char(39) + ' and ' + char(39) + @OpenHouseEnd + char(39) + ')
						) intersect'
		end

	-- ListActivityStart and ListActivityEnd
	if (@ListActivityStart is not null and @ListActivityEnd is not null and @ListActivityStatus is not null) 
		begin
			set @sql = @sql + '
						(
						select distinct ListingID
						from dbo.tbListingActivity
						where convert(date, ActivityDateTime) between ' + char(39) + @ListActivityStart + char(39) + ' and ' + char(39) + @ListActivityEnd + char(39) + '
							and ActivityID in (' + @ListActivityStatus + ')
						) intersect'
		end

	---- JIRA 1490 Start
	---- @SoldOrContractSignedDateStart and @SoldOrContractSignedDateEnd -- sold
	--if (@SoldOrContractSignedDateStart is not null and @SoldOrContractSignedDateEnd is not null and @ListAndActivityStatus is not null and @ListAndActivityStatus = 3698) 
	--	begin
	--		set @sql = @sql + '
	--					(
	--					select ListingID
	--					from dbo.Listings 
	--					where convert(date, closingdate) between ' + char(39) + @SoldOrContractSignedDateStart + char(39) + ' and ' + char(39) + @SoldOrContractSignedDateEnd + char(39) + ' 
	--					) intersect'
	--	end

	---- ListActivityStart and ListActivityEnd -- contract signed
	--if (@SoldOrContractSignedDateStart is not null and @SoldOrContractSignedDateEnd is not null and @ListAndActivityStatus is not null and @ListAndActivityStatus = 3697) 
	--	begin
	--		set @sql = @sql + '
	--					(
	--					select ListingID
	--					from dbo.Listings 
	--					where convert(date, vecontractsigneddate) between ' + char(39) + @SoldOrContractSignedDateStart + char(39) + ' and ' + char(39) + @SoldOrContractSignedDateEnd + char(39) + ' 
	--					) intersect'
	--	end
	---- JIRA 1490 End



	-- NewDevelopment
	if @NewDevelopment is not null
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @NewDevelopment + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @NewDevelopment + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- BuildingPeriod
	if @BuildingPeriod is not null
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @BuildingPeriod + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingPeriod + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- MiscEssentials
	if @MiscEssentials is not null
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from (
								select PropertyID 
								from dbo.tbPropertyAttribute  
									inner join dbo.tfCommaListToTable(' + char(39) + @MiscEssentials + char(39) + ') on AttributeValueID = ListItem 
								group by PropertyID
								having count(PropertyID) = (select count(1) from dbo.tfCommaListToTable(' + char(39) + @MiscEssentials + char(39) + '))
							 ) pa
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						) intersect'
		end

	---- MiscDetails
	if @MiscDetails is not null
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from (
								select PropertyID 
								from dbo.tbPropertyAttribute  
									inner join dbo.tfCommaListToTable(' + char(39) + @MiscDetails + char(39) + ') on AttributeValueID = ListItem 
								group by PropertyID
								having count(PropertyID) = (select count(1) from dbo.tfCommaListToTable(' + char(39) + @MiscDetails + char(39) + '))
							 ) pa
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						) intersect'
		end

	-- FurnishedOpts
	if @FurnishedOpts is not null
		begin
			set @sql = @sql + '
						(
						select distinct ListingID
						from dbo.tbListingAttribute
							inner join dbo.tfCommaListToTable(' + char(39) + @FurnishedOpts + char(39) + ') on AttributeValueID = ListItem
						where IsDeleted = 0
						) intersect'
		end

	-- Fee
	if @Fee is not null
		begin
			set @sql = @sql + '
						(
						select distinct ListingID
						from dbo.tbListingAttribute
							inner join dbo.tfCommaListToTable(' + char(39) + @Fee + char(39) + ') on AttributeValueID = ListItem
						where IsDeleted = 0
						) intersect'
		end

	-- BuildingType
	if @BuildingType is not null
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @BuildingType + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingType + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- BuildingAmenity
	if @BuildingAmenity is not null
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from (
								select PropertyID 
								from dbo.tbPropertyAttribute  
									inner join dbo.tfCommaListToTable(' + char(39) + @BuildingAmenity + char(39) + ') on AttributeValueID = ListItem 
								group by PropertyID
								having count(PropertyID) = (select count(1) from dbo.tfCommaListToTable(' + char(39) + @BuildingAmenity + char(39) + '))
							 ) pa
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- BuildingPersonnel
	if @BuildingPersonnel is not null
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @BuildingPersonnel + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingPersonnel + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- BuildingAllow
	if @BuildingAllow is not null
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from (
								select PropertyID 
								from dbo.tbPropertyAttribute  
									inner join dbo.tfCommaListToTable(' + char(39) + @BuildingAllow + char(39) + ') on AttributeValueID = ListItem 
								group by PropertyID
								having count(PropertyID) = (select count(1) from dbo.tfCommaListToTable(' + char(39) + @BuildingAllow + char(39) + '))
							 ) pa
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- ManagedBuildings
	if @ManagedBuildings is not null
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @ManagedBuildings + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @ManagedBuildings + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- BuildingNotAllow
	if @BuildingNotAllow is not null
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from (
								select PropertyID 
								from dbo.tbPropertyAttribute  
									inner join dbo.tfCommaListToTable(' + char(39) + @BuildingNotAllow + char(39) + ') on AttributeValueID = ListItem 
								group by PropertyID
								having count(PropertyID) = (select count(1) from dbo.tfCommaListToTable(' + char(39) + @BuildingNotAllow + char(39) + '))
							 ) pa
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- OutdoorSpace
		
	if @OutdoorSpace is not null  
		begin
		
			if charindex('208',@OutdoorSpace)>0 
				begin
				
					set @sql = @sql + '
								((
								select l.ListingID
								from (
										select PropertyID 
										from dbo.tbPropertyAttribute  
											inner join dbo.tfCommaListToTable(' + char(39) + @OutdoorSpace + char(39) + ') on AttributeValueID = cast(ListItem  as int)
										group by PropertyID
										having count(PropertyID) = (select count(1) from dbo.tfCommaListToTable(' + char(39) + @OutdoorSpace + char(39) + '))
									 ) pa
									inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
								) union'

							set @OutdoorSpace=replace(@OutdoorSpace,'208','213')

							set @sql = @sql + '
								(
								select l.ListingID
								from (
										select PropertyID 
										from dbo.tbPropertyAttribute  
											inner join dbo.tfCommaListToTable(' + char(39) + @OutdoorSpace + char(39) + ') on AttributeValueID = cast(ListItem  as int) 
										group by PropertyID
										having count(PropertyID) = (select count(1) from dbo.tfCommaListToTable(' + char(39) + @OutdoorSpace + char(39) + '))
									 ) pa
									inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
								)'
							set @sql = @sql + ' ) intersect'

				end 
			else
				begin 
					set @sql = @sql + '
								(
								select l.ListingID
								from (
										select PropertyID 
										from dbo.tbPropertyAttribute  
											inner join dbo.tfCommaListToTable(' + char(39) + @OutdoorSpace + char(39) + ') on AttributeValueID = ListItem 
										group by PropertyID
										having count(PropertyID) = (select count(1) from dbo.tfCommaListToTable(' + char(39) + @OutdoorSpace + char(39) + '))
									 ) pa
									inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
								) intersect'
				end
		end
	-- Views
	if @Views is not null
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from (
								select PropertyID 
								from dbo.tbPropertyAttribute  
									inner join dbo.tfCommaListToTable(' + char(39) + @Views + char(39) + ') on AttributeValueID = ListItem 
								group by PropertyID
								having count(PropertyID) = (select count(1) from dbo.tfCommaListToTable(' + char(39) + @Views + char(39) + '))
							 ) pa
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						) intersect'
		end

	-- Exposure
	if @Exposure is not null
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from (
								select PropertyID 
								from dbo.tbPropertyAttribute  
									inner join dbo.tfCommaListToTable(' + char(39) + @Exposure + char(39) + ') on AttributeValueID = ListItem 
								group by PropertyID
								having count(PropertyID) = (select count(1) from dbo.tfCommaListToTable(' + char(39) + @Exposure + char(39) + '))
							 ) pa
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						) intersect'
		end

	-- MultiFloor
	if @MultiFloor is not null
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @MultiFloor + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @MultiFloor + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- OwnershipType
	if @OwnershipType is not null 
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @OwnershipType + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							--inner join dbo.tfCommaListToTable(' + char(39) + @OwnershipType + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pa.AttributeValueID in ('  + @OwnershipType +  ')
						) intersect'
		end

	-- ApartmentType
	if @ApartmentType is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from (
								select PropertyID 
								from dbo.tbPropertyAttribute  
									inner join dbo.tfCommaListToTable(' + char(39) + @ApartmentType + char(39) + ') on AttributeValueID = ListItem 
								group by PropertyID
								having count(PropertyID) = (select count(1) from dbo.tfCommaListToTable(' + char(39) + @ApartmentType + char(39) + '))
							 ) pa
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						) intersect'
		end

	-- Only process if ownershiptype includes TOWNHOUSE (225)
	if exists(select 1 from dbo.tfCommaListToTable(@Ownershiptype) where listitem = 225)
	begin
		-- TownhouseType
		if @TownhouseType is not null 
			begin
				set @th_sql = @th_sql + '
							(
							select l.ListingID
							from (
									select PropertyID 
									from dbo.tbPropertyAttribute  
										inner join dbo.tfCommaListToTable(' + char(39) + @TownhouseType + char(39) + ') on AttributeValueID = ListItem 
									group by PropertyID
									having count(PropertyID) = (select count(1) from dbo.tfCommaListToTable(' + char(39) + @TownhouseType + char(39) + '))
									) pa
								inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
								inner join dbo.tbListing l on p.PropertyID = l.PropertyID
							) intersect'
			end

		-- THFeatures
		if @THFeatures is not null 
			begin
				set @th_sql = @th_sql + '
							(
							select l.ListingID
							from (
									select PropertyID 
									from dbo.tbPropertyAttribute  
										inner join dbo.tfCommaListToTable(' + char(39) + @THFeatures + char(39) + ') on AttributeValueID = ListItem 
									group by PropertyID
									having count(PropertyID) = (select count(1) from dbo.tfCommaListToTable(' + char(39) + @THFeatures + char(39) + '))
									) pa
								inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
							) intersect'
			end

		-- MinUnits
		if @MinUnits is not null 
			begin
				set @th_sql = @th_sql + '
							(
							select l.ListingID
							from dbo.tbPropertyAttributeNumeric pan
								inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
								inner join dbo.tbListing l on p.PropertyID = l.PropertyID
							where pan.IsDeleted = 0 
								and l.IsDeleted = 0
								and pan.AttributeID = 102 
								and AttributeValue >= ' + cast(@MinUnits as varchar) + '
							) intersect'
			end

		-- MinWidth
		if @MinWidth is not null 
			begin
				set @th_sql = @th_sql + '
							(
							select l.ListingID
							from dbo.tbPropertyAttributeNumeric pan
								inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
								inner join dbo.tbListing l on p.PropertyID = l.PropertyID
							where pan.IsDeleted = 0 
								and l.IsDeleted = 0
								and pan.AttributeID = 1128 
								and AttributeValue >= ' + cast(@MinWidth as varchar) + '
							) intersect'
			end

		-- Button up sql
		if len(@th_sql) > 0
		begin
			-- Add townhouse ownershiptype only
			set @th_sql = @th_sql + '
							(
							select l.ListingID
							from dbo.tbPropertyAttribute pa
								inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
								inner join dbo.tbListing l on p.PropertyID = l.PropertyID
							where pa.AttributeValueID = 225
							) '
			-- Add union
			set @th_sql = '
						union
						(' +
						@th_sql +
						')'
		end

	end

	-- HomeType
	if @HomeType is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @HomeType + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @HomeType + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
						--	inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- HFeatures
	if @HFeatures is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @HFeatures + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @HFeatures + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
						--	inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- FinancingAllowed
	if @FinancingAllowed is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 36 
							and AttributeValue > 0
						--union
						--select l.ListingID
						--from dbo.tbPropertyAttributeNumeric pan
						--	inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
						--	inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						--where pan.IsDeleted = 0 
						--	and l.IsDeleted = 0
						--	and pan.AttributeID = 36 
						--	and AttributeValue > 0
						) intersect'
		end

	-- ListingStatus
	if @ListingStatus is not null 
		begin
			--set @sql = @sql + '
			--			(
			--			select l.ListingID 
			--			from dbo.tbListing l 
			--				inner join dbo.tbListingStatus ls on l.CurrentStatusID = ls.ListingStatusID
			--				inner join (select ListItem from dbo.tfCommaListToTable(' + char(39) + @ListingStatus + char(39) + ' )) b on ls.StatusID = b.ListItem
			--			where l.IsDeleted = 0
			--			) intersect'

			--set @sql = @sql + '
			--			(
			--			select l.ListingID 
			--			from dbo.tbListing l 
			--				inner join dbo.tbListingStatus ls on l.CurrentStatusID = ls.ListingStatusID and ls.statusid in (' + @ListingStatus + ' )
			--				--inner join (select ListItem from dbo.tfCommaListToTable(' + char(39) + @ListingStatus + char(39) + ' )) b on ls.StatusID = b.ListItem
			--			where l.IsDeleted = 0
			--			) intersect'

			set @sql = @sql + '
						(
							select l.ListingID 
							from dbo.tbListing l 
							inner join listings ll on ll.listingid = l.listingid
							inner join dbo.tbListingStatus ls on l.CurrentStatusID = ls.ListingStatusID and ls.statusid in (' + @ListingStatus + ' )
							where l.IsDeleted = 0 and  ( secure_listings_flag = ''N'' or secure_listings_flag is null)

							union

							select tl.listingid
							from dbo.tbListing tl
							inner join listings ll on ll.listingid = tl.listingid
							left join [who_can_see_it] wcs on wcs.recordid = tl.listingid and wcs.recordtype = ''L''
							left join secure_listings sl on sl.listingid = tl.listingid
							where sl.status in  (' + @ListingStatus + ' ) and secure_listings_flag = ''Y'' and (
							SL.secure_group in (select securegroup from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ') or 
															(SL.secure_group in (select parentgroupname from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ')))

						) intersect'

		end

	-- ListingType
	if @ListingType is not null
		begin
			--set @sql = @sql + '
			--			(
			--			select l.ListingID
			--			from dbo.tbListing l 
			--				inner join dbo.tfCommaListToTable(' + char(39) + @ListingType + char(39) + ') t on l.ListingTypeID = t.ListItem
			--			where l.IsDeleted = 0
			--			) intersect'

			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbListing l 						
						inner join listings ll on ll.listingid = l.listingid
						where l.IsDeleted = 0 and  ( secure_listings_flag = ''N'' or secure_listings_flag is null) and l.ListingTypeID in (' +@ListingType + ')

						union

						select tl.listingid
						from dbo.tbListing tl
						inner join listings ll on ll.listingid = tl.listingid
						left join [who_can_see_it] wcs on wcs.recordid = tl.listingid and wcs.recordtype = ''L''
						left join secure_listings sl on sl.listingid = tl.listingid
						where sl.listingtype in  (' + @ListingType + ' ) and secure_listings_flag = ''Y'' and (
						SL.secure_group in (select securegroup from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ') or 
														(SL.secure_group in (select parentgroupname from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ')))

						) intersect'

		end

	-- LeaseTypeRental
	if @LeaseTypeRental is not null 
		begin
			set @sql = @sql + '
						(
						select la.ListingID
						from dbo.tbListingAttribute la
							inner join dbo.tfCommaListToTable(' + char(39) + @LeaseTypeRental + char(39) + ') t on la.AttributeValueID = t.ListItem
						) intersect'
		end

	-- ListingCreditType
	if @ListingCreditType is not null
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbListingCredit l 
							inner join dbo.tfCommaListToTable(' + char(39) + @ListingCreditType + char(39) + ') t on l.ListingCreditTypeID = t.ListItem
						where l.IsDeleted = 0
						) intersect'
		end

	-- Keyword
	if @KeywordSearch is not null
		begin
			set @sql = @sql + '
						(
						select l.recordid
						from dbo.big_text2 l 
						where contains(l.note, ' + char(39) + dbo.sfConvertToFtsContains(@KeywordSearch, 'or') + char(39) + ')  and l.recordid is not null
						union
						select lc.ListingID
						from dbo.tbListingComment lc
							inner join dbo.tbDetail d on lc.DetailID = d.DetailID
						where contains(d.DetailText,' + char(39) + dbo.sfConvertToFtsContains(@KeywordSearch, 'or') + char(39) + ')
						) intersect'
			--set @sql = @sql + '
			--			(
			--			select l.recordid
			--			from dbo.big_text l 
			--			where charindex(' + char(39) + @KeywordSearch + char(39) + ', note) > 0
			--			union
			--			select lc.ListingID
			--			from dbo.tbListingComment lc
			--				inner join dbo.tbDetail d on lc.DetailID = d.DetailID
			--			where charindex(' + char(39) + @KeywordSearch + char(39) + ', d.DetailText) > 0
			--			) intersect'
		end


-- New code from  dev team

	--searchByExclusiveAgent
	if @searchByExclusiveAgent is not null
		begin
			set @sql = @sql + '
						(
						select distinct c.recordid
						from  dbo.contacts c
							inner join (select ListItem from dbo.tfCommaListToTable(' + char(39) + @searchByExclusiveAgent + char(39) + ' )) b on (c.firstname+'+char(39)+ char(32) +char(39)+ '+c.lastname) = b.ListItem
						where recordtype=''L'' and contacttype=''E''
						) intersect'
		end


	--searchByExclusiveFirm

	if @searchByExclusiveFirm is not null
		begin
			set @sql = @sql + '
						(
						select distinct c.recordid
						from  dbo.contacts c
							inner join (select ListItem from dbo.tfCommaListToTable(' + char(39) + @searchByExclusiveFirm + char(39) + ' )) b on c.companyname = b.ListItem
							where contacttype=''C''					
						) intersect'
		end

	
	IF ((@MapSelectedShapes IS NOT null) AND (@MapSelectedShapes <> ''))
	begin
		-- Cleanup @geog_sql
		if len(@geog_sql) > 1
			begin
				-- Remove last union
				set @geog_sql = left(@geog_sql, len(@geog_sql) - 5)

				-- Surround with parenthesis
				set @geog_sql = '
							(' +
							@geog_sql + '
							)'
			end

		-- Combine non-geo with geo sql
		set @strGeoSepSQL = @sql + @geog_sql
		
		-- Remove last intersect if it exists
		if right(@sql, 9) = 'intersect' set @sql = left(@sql, len(@sql) - 9)
		if right(@strGeoSepSQL, 9) = 'intersect' set @strGeoSepSQL = left(@strGeoSepSQL, len(@strGeoSepSQL) - 9)

		-- Combine sql
		set @sql = @sql + @th_sql
		set @strGeoSepSQL=@strGeoSepSQL + @th_sql

		--print 'Geo SQL'
		--print @strGeoSepSQL
		 --print 'end GEO SQL'
	end
	else
	begin
		-- Cleanup @geog_sql
		if len(@geog_sql) > 1
			begin
				-- Remove last union
				set @geog_sql = left(@geog_sql, len(@geog_sql) - 5)

				-- Surround with parenthesis
				set @geog_sql = '
							(' +
							@geog_sql + '
							)'
			end

		-- Combine non-geo with geo sql
		set @sql = @sql + @geog_sql

		-- Remove last intersect if it exists
		if right(@sql, 9) = 'intersect' set @sql = left(@sql, len(@sql) - 9)

		-- Combine sql
		set @sql = @sql + @th_sql
	end



	-- Check if ListingCategory was the only parameter (BAD)
	declare @tempsql varchar(max), @finalsql varchar(max)			
	if isnull(@sql,'') = ''
		begin
			-- Execute final sql statement to insert ListingIDs into table variable
			
			IF ((@MapSelectedShapes IS NOT null) AND (@MapSelectedShapes <> ''))
			begin
			-- First take count of the Listings based on ListingcategoryID
				set @intMapTotalRecordCount =(select count(l.ListingID)
				from dbo.tblisting l				
				where l.ListingCategoryID = @ListingCategory)
				set @finalsql = '(select l.ListingID from dbo.tblisting l where l.ListingCategoryID ='+ cast(@ListingCategory as varchar(max)) + ') 
								intersect (select SL.listingid
								from secure_listings SL
								inner join listings LL on LL.listingid=SL.listingid
								where (SL.secure_group in (select securegroup from tbsecuregroups where companyid = ' + cast(@companyid as varchar) + ')) or 
								(SL.secure_group in (select parentgroupname from tbsecuregroups where companyid = ' + cast(@companyid as varchar) + ')))'				
			end
			else
			begin
				insert into @tblresult
				select distinct top 1000 l.ListingID
				from dbo.tbListing l
				inner join dbo.listings bbl on l.ListingID = bbl.listingid
				inner join dbo.units bbu on bbl.unitid = bbu.unitid
				inner join dbo.properties bbp on bbl.propertyid = bbp.propertyid
				inner join dbo.addresses bba on bbp.addressid = bba.addressid				
				where l.ListingCategoryID = @ListingCategory and l.listingid in (
						select SL.listingid
						from secure_listings SL
						inner join listings LL on LL.listingid=SL.listingid
						where (SL.secure_group in (select securegroup from tbsecuregroups where companyid = @companyid)) or
						(SL.secure_group in (select parentgroupname from tbsecuregroups where companyid = @companyid)))
			end

			
		end
	else
		begin
			-- Execute final sql statement to insert ListingIDs into table variable			
			DECLARE @CountResults TABLE (CountReturned INT)
			IF ((@MapSelectedShapes IS NOT null) AND (@MapSelectedShapes <> ''))
			begin

			--print 'blnGeoParameter:'
			--print @blnGeoParameter
				-- First take count of the Listings based on ListingcategoryID
				if (@blnGeoParameter =1 or 1=1)
				begin
								set @tempsql = ' select count(*) from ( (select l.ListingID from dbo.tbListing l where l.ListingCategoryID = ' + cast(@ListingCategory as varchar(max)) + ' ) intersect 
								(
									select l.ListingID
									from dbo.tbListing l 						
									inner join listings ll on ll.listingid = l.listingid
									where l.IsDeleted = 0 and  ( secure_listings_flag = ''N'' or secure_listings_flag is null)

									union

									select tl.listingid
									from dbo.tbListing tl
									inner join listings ll on ll.listingid = tl.listingid
									left join [who_can_see_it] wcs on wcs.recordid = tl.listingid and wcs.recordtype = ''L''
									left join secure_listings sl on sl.listingid = tl.listingid
									where secure_listings_flag = ''Y'' and (
									SL.secure_group in (select securegroup from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ') or 
									(SL.secure_group in (select parentgroupname from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ')))
								) intersect'

								set @tempsql=@tempsql + @strGeoSepSQL + ' ) I'
								insert into @CountResults 
								exec(@tempsql)
								set @intMapTotalRecordCount = (SELECT CountReturned FROM @CountResults)
								--print 'withGEOCount----'
								--print @intMapTotalRecordCount
		 
								if(@intMapTotalRecordCount>=1000)
								begin 
									set @tempsql = '(select Li.ListingID from dbo.tbListing Li where Li.ListingCategoryID = ' + cast(@ListingCategory as varchar(max)) + '  and Li.isdeleted=0) intersect 
									(
										select l.ListingID
										from dbo.tbListing l 						
										inner join listings ll on ll.listingid = l.listingid
										where l.IsDeleted = 0 and  ( secure_listings_flag = ''N'' or secure_listings_flag is null)

										union

										select tl.listingid
										from dbo.tbListing tl
										inner join listings ll on ll.listingid = tl.listingid
										left join [who_can_see_it] wcs on wcs.recordid = tl.listingid and wcs.recordtype = ''L''
										left join secure_listings sl on sl.listingid = tl.listingid
										where secure_listings_flag = ''Y'' and (
										SL.secure_group in (select securegroup from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ') or 
										(SL.secure_group in (select parentgroupname from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ')))
									) intersect'

									set @finalsql = 'select distinct top 1000 p.ListingID from ('+ @tempsql + @strGeoSepSQL+')p inner join dbo.tbListing l on l.ListingID=p.ListingID inner join dbo.listings bbl on l.ListingID = bbl.listingid inner join dbo.units bbu on bbl.unitid = bbu.unitid inner join dbo.properties bbp on bbl.propertyid = bbp.propertyid inner join dbo.addresses bba on bbp.addressid = bba.addressid'
													insert into @tblresult
													exec(@finalsql)
									
									set @intMapTotalRecordCount = (select count(ListingID) from @tblresult)
									if(@intMapTotalRecordCount>=1000)
									begin
										set @blnProcessMapShapes=1
									end 
								end 
														--print 'blnProcessMapShapes:'
														--print @blnProcessMapShapes
		

								if(@blnProcessMapShapes<>1)
								begin 
								
									set @blnProcessMapShapes=2
									-- Insert Data into Template based on Geo Parameter First. . 
									set @tempsql = '(select Li.ListingID from dbo.tbListing Li where Li.ListingCategoryID = ' + cast(@ListingCategory as varchar(max)) + '  and Li.isdeleted=0) intersect 
									(
										select l.ListingID
										from dbo.tbListing l 						
										inner join listings ll on ll.listingid = l.listingid
										where l.IsDeleted = 0 and  ( secure_listings_flag = ''N'' or secure_listings_flag is null)

										union

										select tl.listingid
										from dbo.tbListing tl
										inner join listings ll on ll.listingid = tl.listingid
										left join [who_can_see_it] wcs on wcs.recordid = tl.listingid and wcs.recordtype = ''L''
										left join secure_listings sl on sl.listingid = tl.listingid
										where secure_listings_flag = ''Y'' and (
										SL.secure_group in (select securegroup from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ') or 
										(SL.secure_group in (select parentgroupname from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ')))
									) intersect'
									set @finalsql = 'select distinct top 1000 p.ListingID from ('+ @tempsql + @strGeoSepSQL+')p inner join dbo.tbListing l on l.ListingID=p.ListingID inner join dbo.listings bbl on l.ListingID = bbl.listingid inner join dbo.units bbu on bbl.unitid = bbu.unitid inner join dbo.properties bbp on bbl.propertyid = bbp.propertyid inner join dbo.addresses bba on bbp.addressid = bba.addressid'
									insert into @tblresult
									exec(@finalsql)

									-- Check count without GEO Parameters 
									set @tempsql = ' select count(*) from ( (select l.ListingID from dbo.tbListing l where l.ListingCategoryID = ' + cast(@ListingCategory as varchar(max)) + ' ) intersect 
									(
											select l.ListingID
											from dbo.tbListing l 						
											inner join listings ll on ll.listingid = l.listingid
											where l.IsDeleted = 0 and  ( secure_listings_flag = ''N'' or secure_listings_flag is null)

											union

											select tl.listingid
											from dbo.tbListing tl
											inner join listings ll on ll.listingid = tl.listingid
											left join [who_can_see_it] wcs on wcs.recordid = tl.listingid and wcs.recordtype = ''L''
											left join secure_listings sl on sl.listingid = tl.listingid
											where secure_listings_flag = ''Y'' and (
											SL.secure_group in (select securegroup from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ') or 
											(SL.secure_group in (select parentgroupname from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ')))
									) intersect'
									set @tempsql=@tempsql + @sql + ' ) I'
									delete from @CountResults
									insert into @CountResults 
									exec(@tempsql)
									set @intMapTotalRecordCount = (SELECT CountReturned FROM @CountResults)
										--print 'withoutGEOCOUNT'
										--print @intMapTotalRecordCount
		
									set @tempsql = '(select l.ListingID from dbo.tbListing l where l.ListingCategoryID = ' + cast(@ListingCategory as varchar(max)) + ' ) intersect 
									(
										select l.ListingID
										from dbo.tbListing l 						
										inner join listings ll on ll.listingid = l.listingid
										where l.IsDeleted = 0 and  ( secure_listings_flag = ''N'' or secure_listings_flag is null)

										union

										select tl.listingid
										from dbo.tbListing tl
										inner join listings ll on ll.listingid = tl.listingid
										left join [who_can_see_it] wcs on wcs.recordid = tl.listingid and wcs.recordtype = ''L''
										left join secure_listings sl on sl.listingid = tl.listingid
										where secure_listings_flag = ''Y'' and (
										SL.secure_group in (select securegroup from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ') or 
										(SL.secure_group in (select parentgroupname from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ')))
									) intersect'
									set @finalsql = @tempsql + @sql
								end 
								------- if Geo Parameter set for along with Map Shapes than do Or Conditon with Maps. 
				end
				else
				begin 
						--- End Code for Or Condition 
					set @tempsql = ' select count(*) from ( (select l.ListingID from dbo.tbListing l where l.ListingCategoryID = ' + cast(@ListingCategory as varchar(max)) + ' ) intersect 
					(
						select l.ListingID
						from dbo.tbListing l 						
						inner join listings ll on ll.listingid = l.listingid
						where l.IsDeleted = 0 and  ( secure_listings_flag = ''N'' or secure_listings_flag is null)

						union

						select tl.listingid
						from dbo.tbListing tl
						inner join listings ll on ll.listingid = tl.listingid
						left join [who_can_see_it] wcs on wcs.recordid = tl.listingid and wcs.recordtype = ''L''
						left join secure_listings sl on sl.listingid = tl.listingid
						where secure_listings_flag = ''Y'' and (
						SL.secure_group in (select securegroup from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ') or 
						(SL.secure_group in (select parentgroupname from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ')))
					) intersect'

					set @tempsql=@tempsql + @sql + ' ) I'
			
				
				
					delete from @CountResults
				
					insert into @CountResults 
					exec(@tempsql)
				
						

					set @intMapTotalRecordCount = (SELECT CountReturned FROM @CountResults)
				
					set @tempsql = '(select l.ListingID from dbo.tbListing l where l.ListingCategoryID = ' + cast(@ListingCategory as varchar(max)) + ' ) intersect 
					(
						select l.ListingID
						from dbo.tbListing l 						
						inner join listings ll on ll.listingid = l.listingid
						where l.IsDeleted = 0 and  ( secure_listings_flag = ''N'' or secure_listings_flag is null)

						union

						select tl.listingid
						from dbo.tbListing tl
						inner join listings ll on ll.listingid = tl.listingid
						left join [who_can_see_it] wcs on wcs.recordid = tl.listingid and wcs.recordtype = ''L''
						left join secure_listings sl on sl.listingid = tl.listingid
						where secure_listings_flag = ''Y'' and (
						SL.secure_group in (select securegroup from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ') or 
						(SL.secure_group in (select parentgroupname from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ')))
					) intersect'
					set @finalsql = @tempsql + @sql

					print @finalsql
				end 
			end
			else
			begin
				--set @tempsql = '(select Li.ListingID from dbo.tbListing Li where Li.ListingCategoryID = ' + cast(@ListingCategory as varchar(max)) + '  and Li.isdeleted=0) intersect 
				--(select SL.listingid
				--from secure_listings SL
				--inner join listings LL on LL.listingid=SL.listingid
				--where (SL.secure_group in (select securegroup from tbsecuregroups where companyid = ' + cast(@companyid as varchar) + ')) 
				--			or (SL.secure_group in (select parentgroupname from tbsecuregroups where companyid = ' + cast(@companyid as varchar) + '))) intersect'

				set @tempsql = '(select Li.ListingID from dbo.tbListing Li where Li.ListingCategoryID = ' + cast(@ListingCategory as varchar(max)) + '  and Li.isdeleted=0) intersect 
				(
					select l.ListingID
					from dbo.tbListing l 						
					inner join listings ll on ll.listingid = l.listingid
					where l.IsDeleted = 0 and  ( secure_listings_flag = ''N'' or secure_listings_flag is null)

					union

					select tl.listingid
					from dbo.tbListing tl
					inner join listings ll on ll.listingid = tl.listingid
					left join [who_can_see_it] wcs on wcs.recordid = tl.listingid and wcs.recordtype = ''L''
					left join secure_listings sl on sl.listingid = tl.listingid
					where secure_listings_flag = ''Y'' and (
					SL.secure_group in (select securegroup from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ') or 
					(SL.secure_group in (select parentgroupname from tbsecuregroups where companyid = ' + cast( @companyid as varchar)  + ')))
				) intersect'

				set @finalsql = 'select distinct top 1000 p.ListingID from ('+ @tempsql + @sql+')p inner join dbo.tbListing l 
				on l.ListingID=p.ListingID inner join dbo.listings bbl on l.ListingID = bbl.listingid 
				inner join dbo.units bbu on bbl.unitid = bbu.unitid 
				inner join dbo.properties bbp on bbl.propertyid = bbp.propertyid 
				inner join dbo.addresses bba on bbp.addressid = bba.addressid'
				--set @finalsql =  @tempsql + @sql

				--set @tempsql = '(select distinct top 1000 l.ListingID from dbo.vwListingBySearch l	where l.ListingCategoryID = ' + cast(@ListingCategory as varchar(max)) + ' ) intersect'				
				--set @finalsql = ' select distinct top 1000  listingid from (' + @tempsql + @sql + ' ) I'
				
				insert into @tblresult
				exec(@finalsql)

				print 'no map selected. only other criteria available'
				print @finalsql
			end
		END

		

		
		


/*(For Todd to update/add search related logic) Ends here*/
		
/* internal by development team starts*/
-- sample map shapes string for testing purpose.
--SET @MapSelectedShapes= '-74.00115966796875,40.77469185382073;-74.00115966796875,40.78762523112725;-73.95421028137207,40.78762523112725;-73.95421028137207,40.77469185382073;-74.00115966796875,40.77469185382073;:-73.97884368896484,40.792758888618756;-73.97884368896484,40.792758888618756;-73.97772789001463,40.78372598556362;-73.97772789001463,40.78372598556362;-73.94116401672363,40.784505852991366;-73.94116401672363,40.784505852991366;-73.97103309631348,40.79236900450889;-73.97103309631348,40.79236900450889;-73.97232055664062,40.786845400329625;-73.97232055664062,40.786845400329625;-73.97884368896484,40.792758888618756;:'
--print 'intMapTotalRecordCount'
	--		print @intMapTotalRecordCount
		--	print 'blnProcessMapShapes'
			--print @blnProcessMapShapes

IF ((@MapSelectedShapes IS NOT null) AND (@MapSelectedShapes <> '') AND @intMapTotalRecordCount>0 And @blnProcessMapShapes<>1)
	BEGIN

			declare @tblcircleRadius table (indexnumber int IDENTITY(1,1) index ix1 clustered,radiusvalues decimal(30,15))
			declare @tblresult_copy table (indexnumber int IDENTITY(1,1) index ix1 clustered,ListingID int index ix1ListingID nonclustered, latitude decimal(18,15) , longitude decimal(18,15)  )
			declare @tblresultFull_copy table (indexnumber int IDENTITY(1,1) index ix1 clustered,ListingID int index ix1ListingID nonclustered, latitude decimal(18,15) , longitude decimal(18,15))
			declare @tblresult_distinct table (indexnumber int IDENTITY(1,1) index ix1 clustered,ListingID int index ix1ListingID nonclustered)
			declare @tblFinalOutput table (listingid int)


			
			declare @intCurrentCount INT =0
			declare @intFinalOutputCount INT =0
			declare @strTempSql varchar(max)
			--- Logic to first push data based on other Geo Location and after execute other Location to get data from it. 
			if(@blnProcessMapShapes=2)
			begin 
				insert INTO @tblFinalOutput
				select distinct listingid from @tblresult
				set @intFinalOutputCount=(Select count(listingId) from @tblFinalOutput)
			end 
			--print 'blnGeoParameter:'
			--print @intFinalOutputCount
			--print 'intMapTotalRecordCount'
			--print @intMapTotalRecordCount
			--- End the code for it. 

			-- Loop to check total record in finaloutcount and 
			while ((@intFinalOutputCount<1000) AND (@intcurrentcount<@intMapTotalRecordCount))
			begin 
				
				
				set @strTempSql=@finalsql + ' order by listingid OFFSET ' + cast(@intCurrentCount as varchar(max))  + ' ROWS FETCH NEXT 10000 ROWS ONLY'

				delete from @tblresult
				insert into @tblresult
				exec(@strTempSql)


				--print @strTempSql

				set @intCurrentCount=@intCurrentCount + 10000

				INSERT INTO @tblresult_copy (listingid,latitude,longitude)
				select T.ListingID, bba.Latitude, bba.Longitude 
				from dbo.tbListing l
				inner join @tblresult T on T.ListingID = l.ListingID
				inner join dbo.listings bbl on l.ListingID = bbl.listingid		
				inner join dbo.properties bbp on bbl.propertyid = bbp.propertyid
				inner join dbo.addresses bba on bbp.addressid = bba.addressid		
				left join dbo.tbProperty p on l.PropertyID = p.PropertyID		
				where bba.latitude is not null and bba.longitude is not null

				if (@circleRadius <> '' and (@circleRadius is not null))
				begin
				INSERT INTO @tblcircleRadius
				SELECT ListItem FROM [dbo].[tfDelimeterListToTable](@circleRadius,';')
				end

			
				declare @tblresult_AfterMap table (ListingID int index ix1 clustered)
				declare @tblresult_circle table (ListingID int index ix1 clustered)
				declare @tblresult_line table (ListingID int index ix1 clustered)
				declare @tblresult_copydistinct table (ListingID int index ix1 clustered)
			
			
				declare @shapes varchar(max)
				set @shapes = @MapSelectedShapes			
				declare @tblMapShapes table (indexnumber int index ix1 clustered , shapecoordinates varchar(max), isforCircle bit null, isforLine bit null)
			
				delete from @tblMapShapes
				INSERT INTO @tblMapShapes
				SELECT indexnumber, ListItem,null,null  FROM [dbo].[tfDelimeterListToTable](@shapes,':')	
			
				DELETE FROM @tblMapShapes WHERE [@tblMapShapes].shapecoordinates = ':'
			
				declare @shapeCount int
				set @shapeCount = (SELECT count(*) FROM @tblMapShapes)
						
				declare @shapeCounter int = 0
				declare @shapeNumber int= 0
				declare @shapestr varchar(max)
			
				declare @tblMapShapeCoordinate table (indexnumber int  index ix1 clustered, shapecoordinates varchar(max))
			
				-- for indentifying the polygon, square, rectangle, triangle, circle, line	   
				IF (@shapeCount > 0)
				  BEGIN
					WHILE (@shapeCounter < @shapeCount)
					   BEGIN			
							DELETE FROM @tblMapShapeCoordinate
						
							set @shapestr = (SELECT shapecoordinates FROM @tblMapShapes tms WHERE tms.indexnumber = @shapeCounter + 1)		
						
							DECLARE @shapestrForCircle int

							SET @shapestrForCircle = LEN(@shapestr) - LEN(REPLACE(@shapestr, ';', ''))

							IF (@shapestrForCircle = 1)
							BEGIN
									-- print 'shapestr' + @shapestr + ' - is for circle'
									UPDATE @tblMapShapes SET isforCircle = 1 WHERE [@tblMapShapes].indexnumber = @shapeCounter + 1
							END

							IF (@shapestrForCircle = 2)
							BEGIN
									-- print 'shapestr' + @shapestr + ' - is for line'
									UPDATE @tblMapShapes SET isforLine = 1 WHERE [@tblMapShapes].indexnumber = @shapeCounter + 1
							END

							SET @shapeCounter = @shapeCounter + 1
					   END
				  end

				SET @shapeCounter = 0			   
			
				-- for polygon, square, rectangle, triangle	   
				IF (@shapeCount > 0)
				  BEGIN
					WHILE (@shapeCounter < @shapeCount)
					   BEGIN			
							DELETE FROM @tblMapShapeCoordinate
						
							set @shapestr = (SELECT shapecoordinates FROM @tblMapShapes tms WHERE tms.indexnumber = @shapeCounter + 1 AND tms.isforCircle IS NULL AND isforline IS null)														
							
							set @shapestr = replace (@shapestr, ',',' ')
							set @shapestr = replace (@shapestr, ';',',')
							set @shapestr = replace (@shapestr, ':','')
							set @shapestr = 'POLYGON((' + @shapestr + '))'

							set @shapestr  = replace(@shapestr, ',))','))' )
												
							-- print '*******************************************************************************************************'
							insert INTO @tblMapShapeCoordinate
							SELECT indexnumber, ListItem FROM [dbo].[tfDelimeterListToTable](@shapestr,';')									
							DELETE FROM @tblMapShapeCoordinate WHERE shapecoordinates = ';'		
							UPDATE @tblMapShapeCoordinate SET shapecoordinates = shapecoordinates + ';'	
						

							DECLARE @geom GEOMETRY = @shapestr
							DECLARE @geog GEOGRAPHY = @geom.MakeValid().STUnion(@geom.STStartPoint()).STAsText()



							DECLARE @g geography;
							DECLARE @h geography;
							
							SET @g = geography::STGeomFromText(@shapestr, 4326);

							

							insert into @tblresult_AfterMap
							SELECT top 1000 rc.listingid
							FROM @tblresult_copy rc 
							where @geog.STContains(geography::Point(rc.latitude, rc.longitude , 4326)) = 1
							--where @g.STContains(geography::Point(rc.latitude, rc.longitude , 4326)) = 1
							--where (@g.STIntersects(geography::Point(rc.latitude, rc.longitude , 4326))) = 1
							--where (@g.STWithin(geography::Point(rc.latitude, rc.longitude , 4326))) = 1

						set @shapeCounter = @shapeCounter + 1
					END	
				  end
				-- for circle
				IF (@shapeCount > 0)
				  BEGIN
					  DECLARE @circleshapescount int = 0
					  DECLARE @circleshapescounter int = 0
					  DECLARE @coordinateltforcircle  decimal(18,15)
					  DECLARE @coordinatelgforcircle  decimal(18,15)
					  DECLARE @coordinatesforcircle varchar(max)
					  DECLARE @dist int
					  SET @dist=1000
					  SET @circleshapescount = (SELECT count(*) FROM @tblMapShapes WHERE isforCircle = 1)
			  
					  DECLARE @radiuscount int
					  SET @radiuscount = (SELECT count(*) FROM @tblcircleRadius tr)

					  declare @tblMapCircle table (indexnumber int IDENTITY(1,1) , shapecoordinates varchar(max))
					  INSERT INTO @tblMapCircle
					  SELECT shapecoordinates FROM @tblMapShapes WHERE isforCircle = 1 AND isforLine IS null
			  			  
					  WHILE (@circleshapescounter < @circleshapescount)
					   BEGIN
							-- print 'inside begin for circle'						
						
							IF (@radiuscount > 0)
							BEGIN
								SET @dist=(SELECT cast(radiusvalues AS decimal(30,15)) FROM @tblcircleRadius tcr WHERE tcr.indexnumber = @circleshapescounter + 1) 
								-- print @dist
							end


							SET @dist=(SELECT cast(radiusvalues AS decimal(30,15)) FROM @tblcircleRadius tcr WHERE tcr.indexnumber = @circleshapescounter + 1) 
							-- print @dist

		  					SET @coordinatesforcircle = (SELECT shapecoordinates FROM @tblMapCircle tms WHERE tms.indexnumber = @circleshapescounter + 1)
										
							SET @coordinatesforcircle = REPLACE(@coordinatesforcircle,';','')		
				
							SET @coordinateltforcircle = cast(substring(@coordinatesforcircle,charindex(',',@coordinatesforcircle)+1,len(@coordinatesforcircle)) AS decimal(18,15))
							SET @coordinatelgforcircle = substring(@coordinatesforcircle,1,charindex(',',@coordinatesforcircle)-1)
												
							declare @tmp table (listingid int, distance float)					
								insert into @tmp
							SELECT listingid,isnull((((acos(sin((@coordinateltforcircle*pi()/180)) * sin((dest.latitude*pi()/180))+cos((@coordinateltforcircle*pi()/180))*cos((dest.latitude*pi()/180))*cos(((@coordinatelgforcircle-dest.longitude)*pi()/180))))*180/pi())*60*1.1515*1609.344),0) as distance						
							from @tblresult_copy AS dest 

							insert INTO @tblresult_circle
							SELECT DISTINCT listingid FROM @tmp WHERE distance < @dist

							SET @circleshapescounter = @circleshapescounter + 1
					   END
				  end
		
				-- for line
				IF (@shapeCount > 0)
				  BEGIN
					  declare @shapeCoordinateCount int
					  set @shapeCoordinateCount = (SELECT count(t.indexnumber) FROM @tblMapShapeCoordinate t)

					  declare @shapeCoordinateCounter int = 0	
						DECLARE @i int = 0
						DECLARE @c bit = 0
						SET @i = 0
						
						DECLARE @coordinateltfori decimal(18,15)
						DECLARE @coordinatelgfori decimal(18,15)
						DECLARE @coordinateltforj decimal(18,15)
						DECLARE @coordinatelgforj decimal(18,15)
						DECLARE @coordinatesfori varchar(max)
						DECLARE @coordinatesforj varchar(max)
						DECLARE @lt decimal(18,15)
						DECLARE @lg decimal(18,15)
						DECLARE @resultcopycount int
						DECLARE @resultcopycounter int = 0
						DECLARE @tblMapShapeCoordinateCount int = 0
						SET @tblMapShapeCoordinateCount = (SELECT count(*) FROM @tblMapShapeCoordinate)									
						DECLARE @tblMapShapeCoordinateCounter int = 0	
					  DECLARE @lineshapescount int = 0
					  DECLARE @lineshapescounter int = 0
					  DECLARE @coordinateltforline  decimal(18,15)
					  DECLARE @coordinatelgforline  decimal(18,15)
					  DECLARE @coordinatesforline varchar(max)
					  DECLARE @coordinatesforlinep1 varchar(max)
					  DECLARE @coordinateltforlinep1  decimal(18,15)
					  DECLARE @coordinatelgforlinep1  decimal(18,15)
					  DECLARE @coordinatesforlinep2 varchar(max)
					  DECLARE @coordinateltforlinep2  decimal(18,15)
					  DECLARE @coordinatelgforlinep2  decimal(18,15)
					  DECLARE @shapelinestr varchar(max)
					  DECLARE @bottomleftlt decimal(18,15)
					  DECLARE @bottomleftlg decimal(18,15)
					  DECLARE @topleftlt decimal(18,15)
					  DECLARE @topleftlg decimal(18,15)
					  DECLARE @toprightlt decimal(18,15)
					  DECLARE @toprightlg decimal(18,15)				  
					  DECLARE @bottomrightlt decimal(18,15)
					  DECLARE @bottomrightlg decimal(18,15)

					  DECLARE @radius int
					  SET @radius=1000

					  SET @lineshapescount = (SELECT count(*) FROM @tblMapShapes WHERE isforLine = 1)
			  
					  declare @tblMapLine table (indexnumber int IDENTITY(1,1) , shapecoordinates varchar(max))
					  INSERT INTO @tblMapLine
					  SELECT shapecoordinates FROM @tblMapShapes WHERE isforLine = 1 AND isforCircle IS null
			  	
					  WHILE (@lineshapescounter < @lineshapescount)
					   BEGIN
							-- print 'inside begin for line'												
		  					SET @coordinatesforline = (SELECT shapecoordinates FROM @tblMapline tms WHERE tms.indexnumber = @lineshapescounter + 1)						
							SET @coordinatesforline = substring(@coordinatesforline, 1, (len(@coordinatesforline) - 1))
							-- print @coordinatesforline
							SET @coordinatesforlinep1 = substring(@coordinatesforline,1,charindex(';',@coordinatesforline)-1)
							SET @coordinatesforlinep2 = substring(@coordinatesforline,charindex(';',@coordinatesforline)+1,len(@coordinatesforline))
						
							SET @coordinatesforlinep1 = REPLACE(@coordinatesforlinep1,';','')
							SET @coordinateltforlinep1 = cast(substring(@coordinatesforlinep1,charindex(',',@coordinatesforlinep1)+1,len(@coordinatesforlinep1)) AS decimal(18,15))
							SET @coordinatelgforlinep1 = substring(@coordinatesforlinep1,1,charindex(',',@coordinatesforlinep1)-1)

							SET @coordinatesforlinep2 = REPLACE(@coordinatesforlinep2,';','')
							SET @coordinateltforlinep2 = cast(substring(@coordinatesforlinep2,charindex(',',@coordinatesforlinep2)+1,len(@coordinatesforlinep2)) AS decimal(18,15))
							SET @coordinatelgforlinep2 = substring(@coordinatesforlinep2,1,charindex(',',@coordinatesforlinep2)-1)

							-- logic to create 4 sets of latlongs from 2 line points. i.e BottomLeft TopLeft TopRight BottomRight

							DECLARE @startltp1 decimal(18,15)
							DECLARE @startlgp1 decimal(18,15)
							DECLARE @startltp2 decimal(18,15)
							DECLARE @startlgp2 decimal(18,15)
							SET @startltp1 = @coordinateltforlinep1
							SET @startlgp1 = @coordinatelgforlinep1
							SET @startltp2 = @coordinateltforlinep2
							SET @startlgp2 = @coordinatelgforlinep2
							DECLARE @dx decimal(18,15)
							DECLARE @dy decimal(18,15)
							DECLARE @deltaltp1 decimal(18,15)
							DECLARE @deltalgp1 decimal(18,15)
							DECLARE @deltaltp2 decimal(18,15)
							DECLARE @deltalgp2 decimal(18,15)
							DECLARE @finalltp1 decimal(18,15)
							DECLARE @finallgp1 decimal(18,15)
							DECLARE @finalltp2 decimal(18,15)
							DECLARE @finallgp2 decimal(18,15)
							DECLARE @R int
							SET @R = 30.48 -- 1Ft = 0.3048 m .. we need 100Ft
							DECLARE @theta decimal(18,15)
							SET @theta = 135

							SET @dx = @R*cos(@theta)
							SET @dy = @R*sin(@theta)

							SET @deltalgp1 = @dx/(111320*cos(degrees(41.88592)))
							SET @deltaltp1 = @dy/110540

							SET @deltalgp2 = @dx/(111320*cos(degrees(41.88592)))
							SET @deltaltp2 = @dy/110540

							SET @finallgp1 = @startlgp1 + @deltalgp1
							SET @finalltp1 = @startltp1 + @deltaltp1

							SET @finallgp2 = @startlgp2 + @deltalgp2
							SET @finalltp2 = @startltp2 + @deltaltp2


							-- logic to create 4 sets ends here

							DELETE FROM @tblMapShapeCoordinate
						
							set @shapelinestr = cast(@finallgp1 AS varchar) + ',' + cast(@finalltp1 AS varchar) + ';' + cast(@finallgp2 AS varchar) + ',' + cast(@finalltp2 AS varchar)
						

							DECLARE @g_line geography;
							DECLARE @h_line geography;
							--SET @g = geography::STGeomFromText('POLYGON((-73.98267924785614 40.73544168255703,-73.98311913013458 40.734852286139734,-73.98084461688995 40.7338848517358, -73.9804208278656 40.73447832156747, -73.98267924785614 40.73544168255703))', 4326);
							SET @g_line = geography::STGeomFromText(@shapelinestr, 4326);

						
							insert into @tblresult_line
							SELECT top 1000 rc.listingid
							FROM @tblresult_copy rc 
							where @g.STContains(geography::Point(rc.latitude, rc.longitude , 4326)) = 1
							--where (@g_line.STIntersects(geography::Point(rc.latitude, rc.longitude , 4326))) = 1
							--where (@g_line.STWithin(geography::Point(rc.latitude, rc.longitude , 4326))) = 1

							SET @lineshapescounter = @lineshapescounter + 1
					   END
				  end			
		
				-- Return data
				INSERT INTO @tblresult_copydistinct
				SELECT DISTINCT listingid FROM @tblresult_AfterMap

				INSERT INTO @tblresult_copydistinct
				SELECT DISTINCT listingid FROM @tblresult_circle

				INSERT INTO @tblresult_copydistinct
				SELECT DISTINCT listingid FROM @tblresult_line
			
			
				insert INTO @tblFinalOutput
				select distinct listingid from @tblresult_copydistinct

				set @intFinalOutputCount=(Select count(listingId) from @tblFinalOutput)
			end
			--- Loop for Map completed. 
			


			declare @tblfinalcount int
			set @tblfinalcount = (select count(*) from @tblFinalOutput)
			

			if (@shouldReturnList = 1)
			begin
				
				--select * into tmpTableMaunish from @tblFinalOutput

				select @WorkInProgress = ISNULL((
									SELECT distinct top 1000 listing.ListingID as id, 
									0 checked, 
									0 removed
									from dbo.tbListing listing
									inner join dbo.listings bbl on listing.ListingID = bbl.listingid
									inner join dbo.units bbu on bbl.unitid = bbu.unitid
									inner join dbo.properties bbp on bbl.propertyid = bbp.propertyid
									inner join dbo.addresses bba on bbp.addressid = bba.addressid				
									inner join @tblFinalOutput r on listing.ListingID = r.ListingID
									where listing.ListingCategoryID = @ListingCategory
									FOR XML AUTO, BINARY BASE64,root('wip')) , '')				
				
			end
			else
			begin

				

				set @listingCount  = (select count(*) as listingCount
				from dbo.tbListing listing
									inner join dbo.listings bbl on listing.ListingID = bbl.listingid
									inner join dbo.units bbu on bbl.unitid = bbu.unitid
									inner join dbo.properties bbp on bbl.propertyid = bbp.propertyid
									inner join dbo.addresses bba on bbp.addressid = bba.addressid
				inner join @tblFinalOutput r on listing.ListingID = r.ListingID			
				where listing.ListingCategoryID = @ListingCategory)

				
				
				if (@listingCount > 1000)
				 select 1000 as listingCount
				 else
				 select @listingCount as listingCount

				
				--select count(1) as listingCount
				--from dbo.tblisting l
				--inner join @tblFinalOutput r on l.ListingID = r.ListingID			
				--where l.ListingCategoryID = @ListingCategory and l.isdeleted=0

			end
			--select @WorkInProgress as text1 into tmpMaunish
		END
else
begin	
	---- Return data
	if (@shouldReturnList = 1)
			begin			

			
				select @WorkInProgress = ISNULL((
									SELECT distinct top 1000 listing.ListingID as id, 
									0 checked, 
									0 removed
									from dbo.tbListing listing
									inner join dbo.listings bbl on listing.ListingID = bbl.listingid
									inner join dbo.units bbu on bbl.unitid = bbu.unitid
									inner join dbo.properties bbp on bbl.propertyid = bbp.propertyid
									inner join dbo.addresses bba on bbp.addressid = bba.addressid		
									inner join @tblresult r on listing.ListingID = r.ListingID
									where listing.ListingCategoryID = @ListingCategory
									FOR XML AUTO, BINARY BASE64,root('wip')) , '')
				
			end
			else
			begin
				

				set @listingCount  = (select count(*) as listingCount
				from dbo.tbListing listing
									inner join dbo.listings bbl on listing.ListingID = bbl.listingid
									inner join dbo.units bbu on bbl.unitid = bbu.unitid
									inner join dbo.properties bbp on bbl.propertyid = bbp.propertyid
									inner join dbo.addresses bba on bbp.addressid = bba.addressid		
				inner join @tblresult r on listing.ListingID = r.ListingID		
				where listing.ListingCategoryID = @ListingCategory)

				
				declare @cc int
				set @cc = (select count(*) from  @tblresult)
				
				
				if (@listingCount > 1000)
				begin
				
				 select 1000 as listingCount
				 end
				 else
				 select @listingCount as listingCount

				--select count(1) as listingCount
				--from dbo.tblisting l
				--inner join @tblresult r on l.ListingID = r.ListingID		
				--where l.ListingCategoryID = @ListingCategory and l.isdeleted=0
			end
			
			
end

if (@shouldReturnList = 1)
begin
declare @AgentWorkInProgressID int
	
	select top 1 @AgentWorkInProgressID = AgentWorkInProgressID
	from tbAgentWorkInProgress
	where AgentSavedCriteriaID = @agensavedcriteriaid
	if (isnull(@CustomerID, 0) = 0)
		set @CustomerID = NULL

	declare @newlisting int	
	
	
	if (isnull(@AgentWorkInProgressID,0) > 0 )
	  begin

	  select @newlisting = count(*) from
	  (select Tbl.Col.value('@id', 'int') as ListingID
	   from @WorkInProgress.nodes('//wip/listing') Tbl(Col)) N
	   
	  
	   
	   if ( @newlisting > 0)
	   begin
		declare @CurrentWorkInProgress xml 

		select top 1 @CurrentWorkInProgress = WorkInProgress 
		from tbAgentWorkInProgress 
		where AgentWorkInProgressID = @AgentWorkInProgressID
			
		

		if (@CurrentWorkInProgress IS NOT NULL)
		select @WorkInProgress = 
			(select distinct top 1000 ListingID  as id,  checked,  removed
			from (select N.ListingID,
						isnull(C.Checked, N.Checked) as Checked,
						isnull(C.Removed, N.Removed) as Removed 
					from (select Tbl.Col.value('@id', 'int') as ListingID,
							Tbl.Col.value('@checked', 'int') as Checked,
							Tbl.Col.value('@removed', 'int')	as Removed
					from @WorkInProgress.nodes('//wip/listing') Tbl(Col) ) as N
				LEFT JOIN 
				(select Tbl.Col.value('@id', 'int')			as ListingID,
						Tbl.Col.value('@checked', 'int') as Checked,
						Tbl.Col.value('@removed', 'int')	as Removed
					from @CurrentWorkInProgress.nodes('//wip/listing') Tbl(Col) ) as C
				on C.ListingID = N.ListingID) as listing 
			for xml auto, binary base64,root('wip'))
			
		
		end
		update tbAgentWorkInProgress
		set AgentID = @AgentID,
			CustomerID= @CustomerID,
			Detail= @Detail,
			WorkInProgress= @WorkInProgress,
			IsDeleted=0,
			UpdateDateTime=getdate(),
			UpdateID = @AgentID
		where AgentWorkInProgressID = @AgentWorkInProgressID
	  end
	else 
	  begin
		insert into tbAgentWorkInProgress (AgentID,
											CustomerID,
											Detail,
											WorkInProgress,
											AgentSavedCriteriaID,
											SortBy,
											Bookmark,
											IsDeleted,
											CreateDateTime,
											CreateID) 
										values (@AgentID,
											@CustomerID,
											@Detail,
											@WorkInProgress,
											@agensavedcriteriaid,
											0,
											null,
											0,
											getdate(),
											@AgentID)
	  end
end

/* internal by development team ends*/
end
