use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdatePostalAddress') and type in (N'P', N'PC'))
drop procedure dbo.upUpdatePostalAddress
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160412
-- ============================================================
create procedure dbo.upUpdatePostalAddress
(
	@PostalAddressID int
	, @Line1 varchar(80) = null
	, @Line2 varchar(80) = null
	, @Line3 varchar(80) = null
	, @Line4 varchar(80) = null
	, @CityTown varchar(40) = null
	, @StateProvince varchar(40) = null
	, @PostalCode varchar(20) = null
	, @Country char(2) = null
	, @Latitude float = null
	, @Longitude float = null
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbPostalAddress
			set Line1 = isnull(@Line1, Line1)
			, Line2 = isnull(@Line2, Line2)
			, Line3 = isnull(@Line3, Line3)
			, Line4 = isnull(@Line4, Line4)
			, CityTown = isnull(@CityTown, CityTown)
			, StateProvince = isnull(@StateProvince, StateProvince)
			, PostalCode = isnull(@PostalCode, PostalCode)
			, Country = isnull(@Country, Country)
			, Latitude = isnull(@Latitude, Latitude)
			, Longitude = isnull(@Longitude, Longitude)
			, IsDeleted = isnull(@IsDeleted, IsDeleted)
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
		where PostalAddressID = @PostalAddressID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdatePostalAddress - PostalAddress was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdatePostalAddress to web