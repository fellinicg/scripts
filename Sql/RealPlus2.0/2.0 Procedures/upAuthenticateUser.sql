use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAuthenticateUser') and type in (N'P', N'PC'))
drop procedure dbo.upAuthenticateUser
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160313
-- ============================================================
create procedure dbo.upAuthenticateUser
(
	@AccountName varchar(20), 
	@PasswordCrypted nvarchar(200),
	@FullName varchar(200) output,
	@AccountStatus varchar(50) output,
	@PersonID int output,
	@RoleID tinyint output,
	@CompanyID int = 'F' output,
	@Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @AccountStatus = 'AuthFailed'

	-- Get record
	select 
		@FullName = c.FullName
		, @AccountStatus = dbo.sfGetAttributeValue(UserAccountStatusID)
		, @PersonID = a.UserAccountID
		, @RoleID = d.RoleID
		, @CompanyID = e.CompanyID
	from dbo.tbUserAccount a 
		inner join dbo.tbPerson c on a.UserAccountID = c.PersonID
		inner join dbo.tbUserRole d on a.UserAccountID = d.PersonID and d.IsDeleted = 0
		inner join dbo.vwAgentInfo e on a.UserAccountID = e.AgentID
	where a.AccountName = @AccountName
		and a.PasswordEncrypted = @PasswordCrypted

	-- Check for success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upAuthenticateUser - user was not successfully authenticated', 16, 1)
end
go
grant exec on dbo.upAuthenticateUser to web