use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetListingAgents') and type in (N'P', N'PC'))
drop procedure dbo.upGetListingAgents
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160420
-- ============================================================
create procedure dbo.upGetListingAgents
	@ListingID int
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		c.firstname + ' ' + c.lastname FullName
		, c.CompanyName
		, c.officephone FullPhoneNumber
		, c.email EmailAddress
		, c.address Line1
		, c.city CityTown
		, c.state StateProvince
		, c.postalcode PostalCode
	from dbo.contacts c
	where c.recordid = @ListingID

	-- ORIGINAL
	--select distinct
	--	p.FullName
	--	, c.CompanyName
	--	, pp.FullPhoneNumber
	--	, pe.EmailAddress
	--	, a.Line1
	--	, a.CityTown
	--	, a.StateProvince
	--	, a.PostalCode
	--	--, *
	--from dbo.tbAgentListing al
	--	inner join dbo.tbPerson p on al.AgentID = p.PersonID
	--	left join dbo.tbAgentOffice ao on al.AgentID = ao.AgentID
	--	left join dbo.tbOffice o on ao.OfficeID = o.OfficeID
	--	left join dbo.tbCompany c on o.CompanyID = c.CompanyID
	--	left join dbo.tbPersonPhone pp on p.PersonID = pp.PersonID
	--	left join dbo.tbPersonAddress pa on p.PersonID = pa.PersonID
	--	left join dbo.tbPostalAddress a on pa.PostalAddressID = a.PostalAddressID
	--	left join dbo.tbPersonEmail pe on p.PersonID = pe.PersonID
	--where
	--	al.ListingID = @ListingID

end
go
grant exec on dbo.upGetListingAgents to web