use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetSearchCategories') and type in (N'P', N'PC'))
drop procedure dbo.upGetSearchCategories
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160324
-- ============================================================
create procedure dbo.upGetSearchCategories
	@ShowDeleted bit = 0
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		AttributeValueID
		, ValueName 
	from dbo.tfAttributeValue() 
	where 
		AttributeID = dbo.sfGetAttributeID('ListingCategory')
		and
		IsDeleted = @ShowDeleted

end
go
grant exec on dbo.upGetSearchCategories to web