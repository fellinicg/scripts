use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetSectionItems') and type in (N'P', N'PC'))
drop procedure dbo.upGetSectionItems
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160322
-- ============================================================
create procedure dbo.upGetSectionItems
	@SectionID int
	, @CompanyID int = 0
	, @Hide bit = 1
	, @ShowDeleted bit = 0
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		CustomCriteriaItemID
		, CustomCriteriaSectionID
		, AttributeValueID
		, CompanyID
		, ShortLabel
		, LongLabel
		, NumericValue
		, SortOrder
		, Display
		, IsDeleted
	from dbo.tbCustomCriteriaItem
	where CustomCriteriaSectionID = @SectionID
		and CompanyID = @CompanyID
		and Display = @Hide
		and IsDeleted = @ShowDeleted
	order by SortOrder

end
go
grant exec on dbo.upGetSectionItems to web