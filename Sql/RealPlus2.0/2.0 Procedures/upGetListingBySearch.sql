use Jaguar_v2_Prototype2_Dev
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetListingBySearchV3') and type in (N'P', N'PC'))
drop procedure dbo.upGetListingBySearchV3
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160330
-- ============================================================
create procedure dbo.upGetListingBySearchV3
	@ListingCategory int
	, @ActiveInactive varchar(3) = '0,1'				-- Deprecated
	, @ListingStatus varchar(max) = null				-- Quick, Advanced/Essentials, Advanced/Listing Details, Open House
	, @WebOrListingNum varchar(max) = null				-- Advanced/Essentials, Address, Open House, Sold
	, @PriceMin int = null								-- Quick, Advanced/Essentials, Open House, Sold
	, @PriceMax int = null								-- Quick, Advanced/Essentials, Open House, Sold
	, @Bedrooms float = null							-- Quick, Advanced/Essentials, Open House, Sold
	, @BedroomsPlus bit = 0								-- Quick, Advanced/Essentials, Open House, Sold
	, @Bathrooms float = null							-- Quick, Advanced/Essentials, Open House, Sold
	, @BathroomsPlus bit = 0							-- Quick, Advanced/Essentials, Open House, Sold
	, @HalfBathrooms float = null						-- Advanced/Essentials, Open House, Sold
	, @Rooms float = null								-- Advanced/Essentials, Open House, Sold
	, @RoomsPlus bit = 0								-- Advanced/Essentials, Open House, Sold
	, @MaxMthlyExp int = null							-- Advanced/Essentials, Open House, Sold
	, @OwnershipType varchar(max) = null				-- Advanced/Essentials, Advanced/Building Details, Open House, Sold
	, @ListDateStart varchar(20) = null					-- Advanced/Essentials
	, @ListDateEnd varchar(20) = null					-- Advanced/Essentials
	, @UpdateDateStart varchar(20) = null				-- Advanced/Essentials
	, @UpdateDateEnd varchar(20) = null					-- Advanced/Essentials
	, @ListUpdateDateStart varchar(20) = null			-- Advanced/Essentials
	, @ListUpdateDateEnd varchar(20) = null				-- Advanced/Essentials
	, @OpenHouseStart varchar(20) = null				-- Advanced/Essentials, Open House
	, @OpenHouseEnd varchar(20) = null					-- Advanced/Essentials, Open House
	, @MinLeaseTerm varchar(20) = null					-- Advanced/Essentials, Open House
	, @MaxLeaseTerm varchar(20) = null					-- Advanced/Essentials, Open House
	, @Neighborhood varchar(max) = null					-- Advanced/Essentials, Advanced/Location Details, Address, Open House, Sold
	, @BuildingPeriod varchar(max) = null				-- Advanced/Essentials, Advanced/Building Details, Open House, Sold
	, @MiscEssentials varchar(max) = null				-- Advanced/Essentials, Advanced/Building Details, Open House, Sold
	, @MinSqFt int = null								-- Advanced/Apartment Details, Advanced/Townhouse-Home Details, Open House, Sold
	, @MaxPriceSqFt int = null							-- Advanced/Apartment Details, Advanced/Townhouse-Home Details, Open House, Sold
	, @IncMissingSqFt bit = null						-- Advanced/Apartment Details, Advanced/Townhouse-Home Details, Open House, Sold
	, @ApartmentType varchar(max) = null				-- Advanced/Apartment Details
	, @MiscDetails varchar(max) = null					-- Advanced/Apartment Details
	, @OutdoorSpace varchar(max) = null					-- Advanced/Apartment Details	
	, @Views varchar(max) = null						-- Advanced/Apartment Details
	, @Exposure varchar(max) = null						-- Advanced/Apartment Details
	, @MultiFloor varchar(max) = null					-- Advanced/Apartment Details
	, @MinUnits varchar(10) = null						-- Advanced/Townhouse-Home Details
	, @MinWidth varchar(10) = null						-- Advanced/Townhouse-Home Details
	, @TownhouseType varchar(max) = null				-- Advanced/Townhouse-Home Details
	, @THFeatures varchar(max) = null					-- Advanced/Townhouse-Home Details
	, @HomeType varchar(max) = null						-- Advanced/Townhouse-Home Details
	, @HFeatures varchar(max) = null					-- Advanced/Townhouse-Home Details
	, @ListActivityStatus varchar(max) = null			-- Advanced/Listing Details
	, @ListActivityStart varchar(20) = null				-- Advanced/Listing Details
	, @ListActivityEnd varchar(20) = null				-- Advanced/Listing Details
	, @ListingType varchar(max) = null					-- Advanced/Listing Details
	, @ListingCreditType varchar(max) = null			-- Advanced/Listing Details
	, @BuildingType varchar(max) = null					-- Advanced/Building Details
	, @BuildingPersonnel varchar(max) = null			-- Advanced/Building Details
	, @BuildingAmenity varchar(max) = null				-- Advanced/Building Details
	, @BuildingAllow varchar(max) = null				-- Advanced/Building Details
	, @BuildingNotAllow varchar(max) = null				-- Advanced/Building Details
	, @MinYearBuilt int = null							-- Advanced/Building Details, Advanced/Townhouse-Home Details
	, @ManagedBuildings varchar(max) = null				-- Advanced/Building Details
	, @ManagingAgent varchar(max) = null				-- Advanced/Building Details
	, @GridNorth1 int = null							-- Advanced/Location Details
	, @GridNorth2 int = null							-- Advanced/Location Details
	, @GridEast1 int = null								-- Advanced/Location Details
	, @GridEast2 int = null								-- Advanced/Location Details
	, @GridWest1 int = null								-- Advanced/Location Details
	, @GridWest2 int = null								-- Advanced/Location Details
	, @GridSouth1 int = null							-- Advanced/Location Details
	, @GridSouth2 int = null							-- Advanced/Location Details
	, @Zipcode varchar(max) = null						-- Advanced/Location Details
	, @FurnishedOpts varchar(max) = null				-- Rented
	, @Fee varchar(max) = null							-- Rented
	, @AddressID varchar(max) = null
	, @NewDevelopment varchar(10) = null
	, @FinancingAllowed varchar(10) = null
	, @Keyword varchar(max) = null
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @sql varchar(max) = ''
	declare @tblresult table (ListingID int index ix1 clustered)

	-- Grid
	if @GridNorth1 is not null
		begin
			set @sql =  @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAddress pa
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where isnull(North, -1) = coalesce(' + cast(@GridNorth1 as varchar) + ', North, -1)
							and isnull(East, -1) = coalesce(' + cast(@GridEast1 as varchar) + ', East, -1)
							and isnull(West, -1) = coalesce(' + cast(@GridWest1 as varchar) + ', West, -1)
							and isnull(South, -1) = coalesce(' + cast(@GridSouth1 as varchar) + ', South, -1)
						union
						select l.ListingID
						from dbo.tbPropertyAddress pa
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where isnull(North, -1) = coalesce(' + cast(@GridNorth2 as varchar) + ', North, -1)
							and isnull(East, -1) = coalesce(' + cast(@GridEast2 as varchar) + ', East, -1)
							and isnull(West, -1) = coalesce(' + cast(@GridWest2 as varchar) + ', West, -1)
							and isnull(South, -1) = coalesce(' + cast(@GridSouth2 as varchar) + ', South, -1)
						) intersect'
		end

	-- AddressID
	if @AddressID is not null 
		begin
			set @sql =  @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAddress pa
							inner join dbo.tfcommalisttotable(' + char(39) + @AddressID + char(39) + ') a on pa.PropertyAddressID = a.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pa.IsDeleted = 0
						union
						select l.ListingID
						from dbo.tbPropertyAddress pa
							inner join dbo.tfcommalisttotable(' + char(39) + @AddressID + char(39) + ') a on pa.PropertyAddressID = a.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
									where pa.IsDeleted = 0
						) intersect'
		end

	-- Zipcode
	if @Zipcode is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeText pat 
							inner join dbo.tfCommaListToTable(' + char(39) + @Zipcode + char(39) + ') on AttributeValue = ListItem
							inner join dbo.tbListing l on pat.PropertyID = l.PropertyID
						where pat.IsDeleted = 0
							and pat.AttributeID in (1259)
						) intersect'
		end

	-- WebOrListingNum
	if @WebOrListingNum is not null 
		begin
			set @sql = @sql + '
						(
						select lat.ListingID
						from dbo.tbListingAttributeText lat 
							inner join dbo.tfCommaListToTable(' + char(39) + @WebOrListingNum + char(39) + ') on AttributeValue = ListItem
						where lat.IsDeleted = 0
							and lat.AttributeID in (54,53)
						) intersect'
		end

	-- PriceMin and PriceMax
	if (@PriceMin is not null and @PriceMax is not null) 
		begin
			set @sql = @sql + '
						(
						select ListingID
						from dbo.tbListing 
						where CurrentPrice between ' + cast(@PriceMin as varchar) + ' and ' + cast(@PriceMax as varchar) + '
						) intersect'
		end

	-- MinUnits
	if @MinUnits is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 102 
							and AttributeValue >= ' + cast(@MinUnits as varchar) + '
						) intersect'
		end

	-- MinWidth
	if @MinWidth is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 1128 
							and AttributeValue >= ' + cast(@MinWidth as varchar) + '
						) intersect'
		end

	-- MinYearBuilt
	if @MinYearBuilt is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 106 
							and AttributeValue >= ' + cast(@MinYearBuilt as varchar) + '
						union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 106 
							and AttributeValue >= ' + cast(@MinYearBuilt as varchar) + '
						) intersect'
		end

	-- ManagingAgent
	if @ManagingAgent is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 1184 
							and charindex(' + char(39) + @ManagingAgent + char(39) + ', AttributeValue > 0
						union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 1184 
							and charindex(' + char(39) + @ManagingAgent + char(39) + ', AttributeValue > 0
						) intersect'
		end

	-- Bedrooms
	if @Bedrooms is not null 
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttributeNumeric pan
						--	inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
						--	inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						--where pan.IsDeleted = 0 
						--	and l.IsDeleted = 0
						--	and pan.AttributeID = 11 
						--	and AttributeValue ' + case when isnull(@BedroomsPlus, 0) = 1 then '>' else '' end + '= ' + cast(@Bedrooms as varchar) + '
						--union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 11 
							and AttributeValue ' + case when isnull(@BedroomsPlus, 0) = 1 then '>' else '' end + '= ' + cast(@Bedrooms as varchar) + '
						) intersect'
		end
	
	-- Bathrooms
	if @Bathrooms is not null 
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttributeNumeric pan
						--	inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
						--	inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						--where pan.IsDeleted = 0 
						--	and l.IsDeleted = 0
						--	and pan.AttributeID = 10 
						--	and AttributeValue ' + case when isnull(@BathroomsPlus, 0) = 1 then '>' else '' end + '= ' + cast(@Bathrooms as varchar) + '
						--union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 10 
							and AttributeValue ' + case when isnull(@BathroomsPlus, 0) = 1 then '>' else '' end + '= ' + cast(@Bathrooms as varchar) + '
						) intersect'
		end

	-- HalfBathrooms
	if @HalfBathrooms is not null 
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttributeNumeric pan
						--	inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
						--	inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						--where pan.IsDeleted = 0 
						--	and l.IsDeleted = 0
						--	and pan.AttributeID = 41 
						--	and AttributeValue >= ' + cast(@HalfBathrooms as varchar) + '
						--union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 41 
							and AttributeValue >= ' + cast(@HalfBathrooms as varchar) + '
						) intersect'
		end

	-- Rooms
	if @Rooms is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 85 
							and AttributeValue ' + case when isnull(@RoomsPlus, 0) = 1 then '>' else '' end + '= ' + cast(@Rooms as varchar) + '
						union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 85 
							and AttributeValue ' + case when isnull(@RoomsPlus, 0) = 1 then '>' else '' end + '= ' + cast(@Rooms as varchar) + '
						) intersect'
		end

	-- MinSqFt
	if @MinSqFt is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 91 
							and AttributeValue >= ' + cast(@MinSqFt as varchar) + '
						union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 91 
							and AttributeValue >= ' + cast(@MinSqFt as varchar)
			if @IncMissingSqFt = 1
				set @sql = @sql + '
						union
						select l.ListingID
						from dbo.tbListing l
							left join dbo.tbPropertyAttributeNumeric pan on l.PropertyID = pan.PropertyID and pan.AttributeID = 91
						where pan.PropertyAttributeNumericID is null'
			else
				set @sql = @sql + ') intersect'
		end

	-- MaxPriceSqFt
	if @MaxPriceSqFt is not null 
		begin
			set @sql = @sql + '
						(
						select lan.ListingID
						from dbo.tbListingAttributeNumeric lan
						where lan.IsDeleted = 0 
							and lan.AttributeID = 79
							and AttributeValue <= ' + cast(@MaxPriceSqFt as varchar) + '
						) intersect'
		end
	
	-- MinLeaseTerm
	if @MinLeaseTerm is not null 
		begin
			set @sql = @sql + '
						(
						select lan.ListingID
						from dbo.tbListingAttributeNumeric lan
						where lan.IsDeleted = 0 
							and lan.AttributeID = 1251
							and AttributeValue = ' + @MinLeaseTerm + '
						) intersect'
		end
	
	-- MaxLeaseTerm
	if @MaxLeaseTerm is not null 
		begin
			set @sql = @sql + '
						(
						select lan.ListingID
						from dbo.tbListingAttributeNumeric lan
						where lan.IsDeleted = 0 
							and lan.AttributeID = 1252
							and AttributeValue = ' + @MaxLeaseTerm + '
						) intersect'
		end
	
	-- MaxMthlyExp
	if @MaxMthlyExp is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 76
							and AttributeValue <= ' + cast(@MaxMthlyExp as varchar) + '
						union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 76
							and AttributeValue <= ' + cast(@MaxMthlyExp as varchar) + '
						) intersect'
		end
	
	-- ListDateStart and ListDateEnd
	if (@ListDateStart is not null or @ListDateEnd is not null) 
		begin
			set @sql = @sql + '
						(
						select ListingID
						from dbo.tbListing 
						where IsDeleted = 0
							and ListDate between ' + char(39) + @ListDateStart + char(39) + ' and ' + char(39) + @ListDateEnd + char(39) + ' 
						) intersect'
		end

	-- UpdateDateStart and UpdateDateEnd
	if (@UpdateDateStart is not null or @UpdateDateEnd is not null) 
		begin
			set @sql = @sql + '
						(
						select ListingID
						from dbo.tbListing 
						where IsDeleted = 0
							and convert(date, UpdateDateTime) between ' + char(39) + @UpdateDateStart + char(39) + ' and ' + char(39) + @UpdateDateEnd + char(39) + ' 
						) intersect'
		end

	-- ListUpdateDateStart and ListUpdateDateEnd
	if (@ListUpdateDateStart is not null or @ListUpdateDateEnd is not null) 
		begin
			set @sql = @sql + '
						(
						select ListingID
						from dbo.tbListing 
						where IsDeleted = 0
							and ListDate between ' + char(39) + @ListDateStart + char(39) + ' and ' + char(39) + @ListDateEnd + char(39) + ' 
						union
						select ListingID
						from dbo.tbListing 
						where IsDeleted = 0
							and convert(date, UpdateDateTime) between ' + char(39) + @UpdateDateStart + char(39) + ' and ' + char(39) + @UpdateDateEnd + char(39) + ' 
						) intersect'
		end

	-- OpenHouseStart and OpenHouseEnd
	if (@OpenHouseStart is not null or @OpenHouseEnd is not null) 
		begin
			set @sql = @sql + '
						(
						select ListingID
						from dbo.tbListingOpenHouse 
						where IsDeleted = 0
							and (convert(date, StartDateTime) between ' + char(39) + @OpenHouseStart + char(39) + ' and ' + char(39) + @OpenHouseEnd + char(39) + ' 
								or convert(date, EndDateTime) between ' + char(39) + @OpenHouseStart + char(39) + ' and ' + char(39) + @OpenHouseEnd + char(39) + ')
						) intersect'
		end

	-- ListActivityStart and ListActivityEnd
	if (@ListActivityStart is not null or @ListActivityEnd is not null) 
		begin
			set @sql = @sql + '
						(
						select distinct ListingID
						from dbo.tbListingActivity
						where convert(date, ActivityDateTime) between ' + char(39) + @ListActivityStart + char(39) + ' and ' + char(39) + @ListActivityEnd + char(39) + '
							and ActivityID = isnull(' + @ListActivityStatus + ', ActivityID)
						) intersect'
		end

	-- NewDevelopment
	if @NewDevelopment is not null
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @NewDevelopment + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @NewDevelopment + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- Neighborhood
	if @Neighborhood is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @Neighborhood + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @Neighborhood + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- BuildingPeriod
	if @BuildingPeriod is not null
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @BuildingPeriod + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingPeriod + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- MiscEssentials
	if @MiscEssentials is not null
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @MiscEssentials + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @MiscEssentials + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- MiscDetails
	if @MiscDetails is not null
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @MiscDetails + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @MiscDetails + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- FurnishedOpts
	if @FurnishedOpts is not null
		begin
			set @sql = @sql + '
						(
						select distinct ListingID
						from dbo.tbListingAttribute
							inner join dbo.tfCommaListToTable(' + char(39) + @FurnishedOpts + char(39) + ') on AttributeID = ListItem
						where IsDeleted = 0
						) intersect'
		end

	-- Fee
	if @Fee is not null
		begin
			set @sql = @sql + '
						(
						select distinct ListingID
						from dbo.tbListingAttribute
							inner join dbo.tfCommaListToTable(' + char(39) + @Fee + char(39) + ') on AttributeID = ListItem
						where IsDeleted = 0
						) intersect'
		end

	-- BuildingType
	if @BuildingType is not null
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @BuildingType + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingType + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- BuildingAmenity
	if @BuildingAmenity is not null
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @BuildingAmenity + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingAmenity + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- BuildingPersonnel
	if @BuildingPersonnel is not null
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @BuildingPersonnel + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingPersonnel + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- BuildingAllow
	if @BuildingAllow is not null
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @BuildingAllow + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingAllow + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- ManagedBuildings
	if @ManagedBuildings is not null
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @ManagedBuildings + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @ManagedBuildings + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- BuildingNotAllow
	if @BuildingNotAllow is not null
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @BuildingNotAllow + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingNotAllow + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- OutdoorSpace
	if @OutdoorSpace is not null
		begin
			set @sql = @sql + '
						(select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @OutdoorSpace + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @OutdoorSpace + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- Views
	if @Views is not null
		begin
			set @sql = @sql + '
						(select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @Views + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @Views + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- Exposure
	if @Exposure is not null
		begin
			set @sql = @sql + '
						(select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @Exposure + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @Exposure + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- MultiFloor
	if @MultiFloor is not null
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @MultiFloor + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @MultiFloor + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- OwnershipType
	if @OwnershipType is not null 
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @OwnershipType + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @OwnershipType + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- ApartmentType
	if @ApartmentType is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @ApartmentType + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @ApartmentType + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
						--	inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- TownhouseType
	if @TownhouseType is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @TownhouseType + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @TownhouseType + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
						--	inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- THFeatures
	if @THFeatures is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @THFeatures + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @THFeatures + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
						--	inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- HomeType
	if @HomeType is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @HomeType + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @HomeType + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
						--	inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- HFeatures
	if @HFeatures is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @HFeatures + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @HFeatures + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
						--	inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						) intersect'
		end

	-- FinancingAllowed
	if @FinancingAllowed is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 36 
							and AttributeValue > 0
						--union
						--select l.ListingID
						--from dbo.tbPropertyAttributeNumeric pan
						--	inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
						--	inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						--where pan.IsDeleted = 0 
						--	and l.IsDeleted = 0
						--	and pan.AttributeID = 36 
						--	and AttributeValue > 0
						) intersect'
		end

	-- ListingStatus
	if @ListingStatus is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID 
						from dbo.tbListing l 
							inner join dbo.tbListingStatus ls on l.CurrentStatusID = ls.ListingStatusID
							inner join (select ListItem from dbo.tfCommaListToTable(' + char(39) + @ListingStatus + char(39) + ' )) b on ls.StatusID = b.ListItem
						where l.IsDeleted = 0
						) intersect'
		end

	-- ListingType
	if @ListingType is not null
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbListing l 
							inner join dbo.tfCommaListToTable(' + char(39) + @ListingType + char(39) + ') t on l.ListingTypeID = t.ListItem
						where l.IsDeleted = 0
						) intersect'
		end

	-- ListingCreditType
	if @ListingCreditType is not null
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbListingCredit l 
							inner join dbo.tfCommaListToTable(' + char(39) + @ListingCreditType + char(39) + ') t on l.ListingCreditTypeID = t.ListItem
						where l.IsDeleted = 0
						) intersect'
		end

	-- Keyword
	if @Keyword is not null
		begin
			set @sql = @sql + '
						(
						select l.recordid
						from dbo.big_text l 
						where charindex(' + char(39) + @Keyword + char(39) + ', note) > 0
						) intersect'
		end

	-- Remove last intersect if it exists
	if right(@sql, 9) = 'intersect' set @sql = left(@sql, len(@sql) - 9)
	
	-- Check if ListingCategory was the only parameter (BAD)
	if isnull(@sql,'') = ''
		begin
			-- Execute final sql statement to insert ListingIDs into table variable
			insert into @tblresult
			select ListingID
			from dbo.tbListing
			where ListingCategoryID = @ListingCategory and IsDeleted = 0
		end
	else
		begin
			-- Execute final sql statement to insert ListingIDs into table variable
			insert into @tblresult
			exec(@sql)
			print @sql
		end

	-- Return data
	select 
		l.*
	from dbo.vwListingBySearchV3 l
		inner join @tblresult r on l.ListingID = r.ListingID
	where l.ListingCategoryID = @ListingCategory

end
go
grant exec on dbo.upGetListingBySearchV3 to web