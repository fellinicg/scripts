use Jaguar_v2_Prototype2_Dev
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetBuildingBySearchTMF') and type in (N'P', N'PC'))
drop procedure dbo.upGetBuildingBySearchTMF
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160716
-- ============================================================
create procedure dbo.upGetBuildingBySearchTMF
	@AddressOrBuildingName varchar(max) = null		-- Brian's plugin????
	, @UnitIdentifier varchar(max) = null			-- Brian's plugin????
	, @GridNorth1 int = null
	, @GridNorth2 int = null
	, @GridEast1 int = null
	, @GridEast2 int = null
	, @GridWest1 int = null
	, @GridWest2 int = null
	, @GridSouth1 int = null
	, @GridSouth2 int = null
	, @AreaOrNeighborhood varchar(max) = null
	, @Zipcode varchar(max) = null
	, @OwnershipType varchar(max) = null				
	, @BuildingPeriod varchar(max) = null				
	, @BuildingType varchar(max) = null
	, @BuildingPersonnel varchar(max) = null
	, @BuildingAmenity varchar(max) = null
	, @BuildingAllow varchar(max) = null
	, @BuildingNotAllow varchar(max) = null
	, @ManagedBuildings varchar(max) = null				
	, @MinYearBuilt int = null
	, @ManagingAgent varchar(max) = null				
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @sql varchar(max) = ''
	declare @tblresult table (PropertyID int index ix1 clustered)

	-- Grid
	if @GridNorth1 is not null
		begin
			set @sql =  @sql + '
						(
						select pa.PropertyID
						from dbo.tbPropertyAddress pa
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
						where isnull(North, -1) = coalesce(' + cast(@GridNorth1 as varchar) + ', North, -1)
							and isnull(East, -1) = coalesce(' + cast(@GridEast1 as varchar) + ', East, -1)
							and isnull(West, -1) = coalesce(' + cast(@GridWest1 as varchar) + ', West, -1)
							and isnull(South, -1) = coalesce(' + cast(@GridSouth1 as varchar) + ', South, -1)
						union
						select pa.PropertyID
						from dbo.tbPropertyAddress pa
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
						where isnull(North, -1) = coalesce(' + cast(@GridNorth2 as varchar) + ', North, -1)
							and isnull(East, -1) = coalesce(' + cast(@GridEast2 as varchar) + ', East, -1)
							and isnull(West, -1) = coalesce(' + cast(@GridWest2 as varchar) + ', West, -1)
							and isnull(South, -1) = coalesce(' + cast(@GridSouth2 as varchar) + ', South, -1)
						) intersect'
		end

	---- AddressID
	--if @AddressID is not null 
	--	begin
	--		set @sql =  @sql + '
	--					(
	--					select l.ListingID
	--					from dbo.tbPropertyAddress pa
	--						inner join dbo.tfcommalisttotable(' + char(39) + @AddressID + char(39) + ') a on pa.PropertyAddressID = a.ListItem
	--						inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
	--						inner join dbo.tbListing l on p.PropertyID = l.PropertyID
	--					where pa.IsDeleted = 0
	--					union
	--					select l.ListingID
	--					from dbo.tbPropertyAddress pa
	--						inner join dbo.tfcommalisttotable(' + char(39) + @AddressID + char(39) + ') a on pa.PropertyAddressID = a.ListItem
	--						inner join dbo.tbProperty p on pa.PropertyID = p.PropertyID
	--						inner join dbo.tbListing l on p.PropertyID = l.PropertyID
	--								where pa.IsDeleted = 0
	--					) intersect'
	--	end

	-- MinYearBuilt
	if @MinYearBuilt is not null 
		begin
			set @sql = @sql + '
						(
						select pan.PropertyID
						from dbo.tbPropertyAttributeNumeric pan
						where pan.IsDeleted = 0 
							and pan.AttributeID = 106 
							and AttributeValue >= ' + cast(@MinYearBuilt as varchar) + '
						) intersect'
		end

	-- AreaOrNeighborhood
	if @AreaOrNeighborhood is not null 
		begin
			set @sql = @sql + '
						(
						select pa.PropertyID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @AreaOrNeighborhood + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
						) intersect'
		end

	-- BuildingType
	if @BuildingType is not null
		begin
			set @sql = @sql + '
						(
						select pa.PropertyID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingType + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.PropertyID and p.PropertyClassID = 1
						) intersect'
		end

	-- BuildingAmenity
	if @BuildingAmenity is not null
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingAmenity + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.PropertyID and p.PropertyClassID = 1
						) intersect'
		end

	-- BuildingPersonnel
	if @BuildingPersonnel is not null
		begin
			set @sql = @sql + '
						(
						select pa.PropertyID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingPersonnel + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.PropertyID and p.PropertyClassID = 1
						) intersect'
		end

	-- BuildingAllow
	if @BuildingAllow is not null
		begin
			set @sql = @sql + '
						(
						select pa.PropertyID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingAllow + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.PropertyID and p.PropertyClassID = 1
						) intersect'
		end

	-- BuildingNotAllow
	if @BuildingNotAllow is not null
		begin
			set @sql = @sql + '
						(
						select pa.PropertyID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingNotAllow + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.PropertyID and p.PropertyClassID = 1
						) intersect'
		end

	-- OwnershipType
	if @OwnershipType is not null 
		begin
			set @sql = @sql + '
						(
						select pa.PropertyID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @OwnershipType + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.PropertyID and p.PropertyClassID = 1
						) intersect'
		end

	-- Remove last intersect if it exists
	if right(@sql, 9) = 'intersect' set @sql = left(@sql, len(@sql) - 9)

	-- Check for no parameters (BAD)
	if isnull(@sql,'') = ''
		begin
			-- Execute final sql statement to insert PropertyIDs into table variable
			insert into @tblresult
			select PropertyID
			from dbo.tbProperty
			where PropertyClassID = 1 and IsDeleted = 0
		end
	else
		begin
			-- Execute final sql statement to insert ListingIDs into table variable
			insert into @tblresult
			exec(@sql)
		end

	-- Return data
	select distinct
		p.PropertyID
		, bba.displayaddress Line1
		, null Line2
		, null Line3
		, null Line4
		, bba.city CityTown
		, bba.state StateProvince
		, bba.postalcode PostalCode
		, dbo.sfGetPropertyAttribute(p.PropertyID, 72) 'OwnershipType'
		, dbo.sfGetPropertyAttribute(p.PropertyID, 1118) 'BuildingPeriod'
		, convert(float, bbp.elevators) Elevators
		, convert(float, bbp.financingallowedpercent) FinanceAllowedPercent
		, convert(float, bbp.totalfloors) Floors
		, dbo.sfGetPropertyAttribute(p.PropertyID, 45) 'HasFireplace'
		, dbo.sfGetPropertyAttribute(p.PropertyID, 43) 'HasAC'
		, dbo.sfGetPropertyAttribute(p.PropertyID, 49) 'HasWashDry'
		, dbo.sfGetPropertyAttribute(p.PropertyID, 7) 'ApartmentType'
		, dbo.sfGetPropertyAttribute(p.PropertyID, 33) 'Exposure'
		, dbo.sfGetPropertyAttribute(p.PropertyID, 105) 'Views'
	from @tblresult r
		inner join dbo.tbProperty p on r.PropertyID = p.PropertyID
		inner join dbo.properties bbp on p.EtlID = bbp.propertyid
		inner join dbo.addresses bba on bbp.addressid = bba.addressid
	where p.IsDeleted = 0

end
go
grant exec on dbo.upGetBuildingBySearchTMF to web