use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upDeleteAttribute') and type in (N'P', N'PC'))
drop procedure dbo.upDeleteAttribute
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160324
-- ============================================================
create procedure dbo.upDeleteAttribute
(
	@AttributeID int
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Delete record
	update dbo.tbAttribute set IsDeleted = 1, UpdateID = @UpdateID, UpdateDateTime = getdate()
	where AttributeID = @AttributeID

	-- Check for success
	if (@@error = 0) and (@@rowcount > 0)
		begin
			set @Result = 'S'
		end
	else
		raiserror('upDeleteAttribute - Attribute was not successfully Deleteed', 16, 1)
end
go
grant exec on dbo.upDeleteAttribute to web