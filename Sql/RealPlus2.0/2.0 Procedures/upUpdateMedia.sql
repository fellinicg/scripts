use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateMedia') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateMedia
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160412
-- ============================================================
create procedure dbo.upUpdateMedia
(
	@MediaID int
	, @MediaTypeID int = null
	, @Link varchar(500) = null
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbMedia
			set MediaTypeID = isnull(@MediaTypeID, MediaTypeID)
			, Link = isnull(@Link, Link)
			, IsDeleted = isnull(@IsDeleted, IsDeleted)
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
		where MediaID = @MediaID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateMedia - Media was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateMedia to web