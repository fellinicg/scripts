use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdatePerson') and type in (N'P', N'PC'))
drop procedure dbo.upUpdatePerson
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160412
-- ============================================================
create procedure dbo.upUpdatePerson
(
	@PersonID int
	, @NamePrefix varchar(20) = null
	, @FirstGivenName varchar(40) = null
	, @SecondGivenName varchar(40) = null
	, @FamilyName varchar(60) = null
	, @NameSuffix varchar(20) = null
	, @FullName varchar(200) = null
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbPerson
			set NamePrefix = isnull(@NamePrefix, NamePrefix)
			, FirstGivenName = isnull(@FirstGivenName, FirstGivenName)
			, SecondGivenName = isnull(@SecondGivenName, SecondGivenName)
			, FamilyName = isnull(@FamilyName, FamilyName)
			, NameSuffix = isnull(@NameSuffix, NameSuffix)
			, FullName = isnull(@FullName, FullName)
			, IsDeleted = isnull(@IsDeleted, IsDeleted)
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
		where PersonID = @PersonID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdatePerson - Person was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdatePerson to web