use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddPersonAttribute') and type in (N'P', N'PC'))
drop procedure dbo.upAddPersonAttribute
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddPersonAttribute
(
	@PersonID int
	, @AttributeValueID int
	, @CreateDateTime datetime2
	, @CreateID int
	, @PersonAttributeID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbPersonAttribute
			(PersonID, AttributeValueID, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@PersonID, @AttributeValueID, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @PersonAttributeID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddPersonAttribute - PersonAttribute was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddPersonAttribute to web