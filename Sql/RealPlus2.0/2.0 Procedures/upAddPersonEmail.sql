use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddPersonEmail') and type in (N'P', N'PC'))
drop procedure dbo.upAddPersonEmail
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160316
-- ============================================================
create procedure dbo.upAddPersonEmail
(
	@PersonID int
	, @EmailTypeID int
	, @EmailAddress nvarchar(254)
	, @CreateID int
	, @IsPreferred bit = 0
	, @PersonEmailID int = null output
	, @Result char(1) output
)
as
begin
	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Insert record
	insert into dbo.tbPersonEmail (PersonID, EmailTypeID, EmailAddress, IsPreferred, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
	values (@PersonID, @EmailTypeID, @EmailAddress, @IsPreferred, getdate(), @CreateID, getdate(), @CreateID)

	-- Check for success
	if (@@error = 0) and (@@rowcount > 0)
		begin
			set @PersonEmailID = @@identity
			set @Result = 'S'
		end
	else
		raiserror('upAddPersonEmail - Email was not successfully added', 16, 1)
end
go
grant exec on dbo.upAddPersonEmail to web