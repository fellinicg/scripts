use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddAgentWorkInProgress') and type in (N'P', N'PC'))
drop procedure dbo.upAddAgentWorkInProgress
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160316
-- ============================================================
create procedure dbo.upAddAgentWorkInProgress
(
	@AgentID int
	, @Detail varchar(200)
	, @WorkInProgress xml
	, @CustomerID int = null
	, @AgentSavedCriteriaID int = null
	, @SortBy bit = 0
	, @Bookmark int = null
	, @CreateID int = null
	, @AgentWorkInProgressID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	if @CreateID is null set @CreateID = @AgentID

	-- Check that this agent did not already use the description (detail)
	if exists(select 1 from dbo.tbAgentWorkInProgress where AgentID = @AgentID and Detail = @Detail)
		begin
			set @Result = 'X'
			raiserror('upAddAgentWorkInProgress - A work in progress with this description already exists', 20, 1)
			return
		end

	-- Insert record
	insert into dbo.tbAgentWorkInProgress(AgentID, Detail, CustomerID, WorkInProgress, AgentSavedCriteriaID, SortBy, Bookmark, CreateID, UpdateID)
	values (@AgentID, @Detail, @CustomerID, @WorkInProgress, @AgentSavedCriteriaID, @SortBy, @Bookmark, @CreateID, @CreateID)

	-- Check for success
	if (@@error = 0) and (@@rowcount > 0)
		begin
			set @AgentWorkInProgressID = @@identity
			set @Result = 'S'
		end
	else
		raiserror('upAddAgentWorkInProgress - Work in progress was not successfully saved', 16, 1)
end
go
grant exec on dbo.upAddAgentWorkInProgress to web