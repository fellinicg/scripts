use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddOffice') and type in (N'P', N'PC'))
drop procedure dbo.upAddOffice
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddOffice
(
	@CompanyID int
	, @OfficeCode varchar(10) = null
	, @Detail varchar(50)
	, @PostalAddressID int = null
	, @PhoneNumber varchar(20) = null
	, @Fax varchar(20) = null
	, @Email varchar(50) = null
	, @CreateDateTime datetime2
	, @CreateID int
	, @OfficeID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbOffice
			(CompanyID, OfficeCode, Detail, PostalAddressID, PhoneNumber, Fax, Email, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@CompanyID, @OfficeCode, @Detail, @PostalAddressID, @PhoneNumber, @Fax, @Email, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @OfficeID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddOffice - Office was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddOffice to web