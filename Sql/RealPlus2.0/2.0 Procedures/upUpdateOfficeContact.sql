use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateOfficeContact') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateOfficeContact
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160412
-- ============================================================
create procedure dbo.upUpdateOfficeContact
(
	@OfficeID int
	, @PersonID int
	, @IsPrimary bit = null
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbOfficeContact
			set IsPrimary = isnull(@IsPrimary, IsPrimary)
			, IsDeleted = isnull(@IsDeleted, IsDeleted)
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
		where OfficeID = @OfficeID 
				and PersonID = @PersonID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateOfficeContact - OfficeContact was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateOfficeContact to web