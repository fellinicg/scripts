use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateListingOpenHouse') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateListingOpenHouse
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160412
-- ============================================================
create procedure dbo.upUpdateListingOpenHouse
(
	@ListingOpenHouseID int
	, @StartDateTime datetime2 = null
	, @EndDateTime datetime2 = null
	, @Comment varchar(200) = null
	, @AppointmentOnly bit = null
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbListingOpenHouse
			set StartDateTime = isnull(@StartDateTime, StartDateTime)
			, EndDateTime = isnull(@EndDateTime, EndDateTime)
			, Comment = isnull(@Comment, Comment)
			, AppointmentOnly = isnull(@AppointmentOnly, AppointmentOnly)
			, IsDeleted = isnull(@IsDeleted, IsDeleted)
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
			where ListingOpenHouseID = @ListingOpenHouseID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateListingOpenHouse - ListingOpenHouse was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateListingOpenHouse to web