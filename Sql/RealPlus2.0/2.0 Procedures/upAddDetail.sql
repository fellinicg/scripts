use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddDetail') and type in (N'P', N'PC'))
drop procedure dbo.upAddDetail
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddDetail
(
	@DetailTypeID int
	, @DetailText nvarchar(max)
	, @CreateDateTime datetime2
	, @CreateID int
	, @DetailID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbDetail
			(DetailTypeID, DetailText, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@DetailTypeID, @DetailText, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @DetailID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddDetail - Detail was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddDetail to web