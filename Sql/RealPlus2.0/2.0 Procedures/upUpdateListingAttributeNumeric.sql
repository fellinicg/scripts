use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateListingAttributeNumeric') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateListingAttributeNumeric
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upUpdateListingAttributeNumeric
(
	@ListingAttributeNumericID int
	, @AttributeValue float = null
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbListingAttributeNumeric
			set AttributeValue = isnull(@AttributeValue, AttributeValue)
			, IsDeleted = isnull(@IsDeleted, IsDeleted)
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
		where ListingAttributeNumericID = @ListingAttributeNumericID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateListingAttributeNumeric - ListingAttributeNumeric was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateListingAttributeNumeric to web