use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddCountry') and type in (N'P', N'PC'))
drop procedure dbo.upAddCountry
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddCountry
(
	@CountryCode char(2)
	, @CountryName varchar(45)
	, @FipsCode char(2)
	, @North varchar(30)
	, @South varchar(30)
	, @East varchar(30)
	, @West varchar(30)
	, @Capital varchar(30)
	, @ContinentName varchar(15)
	, @Continent char(2)
	, @AreaInSqKm varchar(20)
	, @GeonameId int
	, @CreateDateTime datetime2
	, @CountryID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbCountry
			(CountryCode, CountryName, FipsCode, North, South, East, West, Capital, ContinentName, Continent, AreaInSqKm, GeonameId, CreateDateTime, UpdateDateTime)
		values
			(@CountryCode, @CountryName, @FipsCode, @North, @South, @East, @West, @Capital, @ContinentName, @Continent, @AreaInSqKm, @GeonameId, @CreateDateTime, @CreateDateTime)

		set @CountryID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddCountry - Country was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddCountry to web