use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddListingAttributeText') and type in (N'P', N'PC'))
drop procedure dbo.upAddListingAttributeText
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddListingAttributeText
(
	@ListingID int
	, @AttributeID int
	, @AttributeValue varchar(200)
	, @CreateDateTime datetime2
	, @CreateID int
	, @ListingAttributeTextID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbListingAttributeText
			(ListingID, AttributeID, AttributeValue, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@ListingID, @AttributeID, @AttributeValue, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @ListingAttributeTextID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddListingAttributeText - ListingAttributeText was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddListingAttributeText to web