use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddAgentListingPin') and type in (N'P', N'PC'))
drop procedure dbo.upAddAgentListingPin
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddAgentListingPin
(
	@AgentID int
	, @ListingID int
	, @CustomerID int
	, @PickListed bit
	, @Liked bit
	, @Disiked bit
	, @Visited bit
	, @CreateDateTime datetime2
	, @CreateID int
	, @AgentListingPinID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbAgentListingPin
			(AgentID, ListingID, CustomerID, PickListed, Liked, Disiked, Visited, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@AgentID, @ListingID, @CustomerID, @PickListed, @Liked, @Disiked, @Visited, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @AgentListingPinID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddAgentListingPin - AgentListingPin was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddAgentListingPin to web