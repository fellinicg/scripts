use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateListingExcludeFromResult') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateListingExcludeFromResult
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160421
-- ============================================================
create procedure dbo.upUpdateListingExcludeFromResult
(
	@ListingExcludeFromResultID int
	, @IsDeleted bit
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbListingExcludeFromResult
			set IsDeleted = @IsDeleted
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
		where ListingExcludeFromResultID = @ListingExcludeFromResultID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateListingExcludeFromResult - record was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateListingExcludeFromResult to web