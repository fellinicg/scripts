use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetAllListingsForUnit') and type in (N'P', N'PC'))
drop procedure dbo.upGetAllListingsForUnit
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160413
-- ============================================================
create procedure dbo.upGetAllListingsForUnit
	@ListingID int
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		l.ListingID
		, dbo.sfGetListingAttribute(l.ListingID, 'UnitIdentifier') 'UnitIdentifier'
		, l.CurrentStatus
		, l.UpdateDateTime
		, l.CurrentPrice
		, l.ListDate
		, dbo.sfGetListingAttribute(l.ListingID, 'dom') 'DOM'
		, dbo.sfGetListingAttribute(l.ListingID, 'closingdate') 'Closed'
		, dbo.sfGetListingAttribute(l.ListingID, 'Bedrooms') 'Bedrooms'
		, dbo.sfGetListingAttribute(l.ListingID, 'Bathrooms') 'Bathrooms'
		, dbo.sfGetListingAttribute(l.ListingID, 'SquareFootage') 'SquareFootage'
	from dbo.tbListing l
		inner join dbo.tbProperty p on l.PropertyID = p.PropertyID
	where l.ListingID = @ListingID
		and l.IsDeleted = 0

end
go
grant exec on dbo.upGetAllListingsForUnit to web