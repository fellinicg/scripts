use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateListingAttribute') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateListingAttribute
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upUpdateListingAttribute
(
	@ListingAttributeID int
	, @IsDeleted bit
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbListingAttribute
			set IsDeleted = @IsDeleted
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
		where ListingAttributeID = @ListingAttributeID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateListingAttribute - ListingAttribute was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateListingAttribute to web