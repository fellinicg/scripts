use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateListingMedia') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateListingMedia
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upUpdateListingMedia
(
	@ListingMediaID int
	, @ListingMediaTypeID int = null
	, @Detail varchar(100) = null
	, @AdditionalDetail varchar(60) = null
	, @SortOrder tinyint = nulln
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbListingMedia
			set ListingMediaTypeID = isnull(@ListingMediaTypeID, ListingMediaTypeID)
			, Detail = isnull(@Detail, Detail)
			, AdditionalDetail = isnull(@AdditionalDetail, AdditionalDetail)
			, SortOrder = isnull(@SortOrder, SortOrder)
			, IsDeleted = isnull(@IsDeleted, IsDeleted)
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
			where ListingMediaID = @ListingMediaID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateListingMedia - ListingMedia was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateListingMedia to web