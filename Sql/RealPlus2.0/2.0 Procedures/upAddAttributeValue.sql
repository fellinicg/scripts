use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddAttributeValue') and type in (N'P', N'PC'))
drop procedure dbo.upAddAttributeValue
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160317
-- ============================================================
create procedure dbo.upAddAttributeValue
(
	@AttributeID int
	, @ValueID int
	, @CreateID int
	, @AttributeValueID int output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Check for existence of AttributeValueName
	select @AttributeValueID = AttributeValueID from dbo.tbAttributeValue where AttributeID = @AttributeID and ValueID = @ValueID

	-- If the AttributeValueName does NOT already exist
	if @@rowcount = 0
		begin
			-- Insert record, if the AttributeValueName does NOT already exist
			insert into dbo.tbAttributeValue (AttributeID, ValueID, CreateID, UpdateID)
			Values (@AttributeID, @ValueID,  @CreateID, @CreateID)

			-- Check for success
			if (@@error = 0) and (@@rowcount > 0)
				begin
					set @AttributeValueID = @@identity
				end
			else
				raiserror('upAddAttributeValue - AttributeValue was not successfully added', 16, 1)
		end
end
go
grant exec on dbo.upAddAttributeValue to web