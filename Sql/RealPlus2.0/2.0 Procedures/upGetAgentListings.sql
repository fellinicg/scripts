use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetAgentListings') and type in (N'P', N'PC'))
drop procedure dbo.upGetAgentListings
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160406
-- ============================================================
create procedure dbo.upGetAgentListings
	@AgentID int
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		a.AgentID
		, a.ListingID
		, dbo.sfGetAttributeValue(a.StatusID) 'Status'
		, a.IsDeleted 'CustomerDeleted'
		, b.IsDeleted 'PersonDeleted'
	from dbo.tbAgentListing a
		inner join dbo.tbListing l on a.ListingID = l.ListingID
		inner join dbo.tbPropertyAddress pa on l.PropertyID = pa.PropertyID and pa.IsPrimary = 1
		inner join dbo.tbPostalAddress adr on pa.PostalAddressID = adr.PostalAddressID
	where AgentID = @AgentID
		and a.IsDeleted = 0

end
go
grant exec on dbo.upGetAgentListings to web