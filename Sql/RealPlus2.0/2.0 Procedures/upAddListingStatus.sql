use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddListingStatus') and type in (N'P', N'PC'))
drop procedure dbo.upAddListingStatus
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddListingStatus
(
	@ListingID int
	, @StatusID int
	, @StatusDateTime datetime2
	, @CreateDateTime datetime2
	, @CreateID int
	, @ListingStatusID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbListingStatus
			(ListingID, StatusID, StatusDateTime, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@ListingID, @StatusID, @StatusDateTime, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @ListingStatusID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddListingStatus - ListingStatus was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddListingStatus to web