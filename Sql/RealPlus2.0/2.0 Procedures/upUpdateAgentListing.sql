use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateAgentListing') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateAgentListing
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upUpdateAgentListing
(
	@AgentListingID int
	, @AgentID int
	--, @ListingID int
	, @IsDeleted bit
	, @UpdateDateTime datetime2
	, @UpdateID int
	,  @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbAgentListing
			set IsDeleted = @IsDeleted
			--, ListingID = @ListingID
			, UpdateDateTime = @UpdateDateTime
			, UpdateID = @UpdateID
		where AgentListingID = @AgentListingID
			and AgentID = @AgentID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateAgentListing - AgentListing was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateAgentListing to web