use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetListingMedia') and type in (N'P', N'PC'))
drop procedure dbo.upGetListingMedia
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160331
-- ============================================================
create procedure dbo.upGetListingMedia
	@ListingID int
	, @MediaTypeID int = null
	, @ListingMediaTypeID int = null
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		lm.ListingMediaID
		, lm.ListingID
		, m.MediaTypeID
		, lm.ListingMediaTypeID
		, v.ValueName 'ListingMediaType'
		, m.Link
		, lm.Detail
		, lm.AdditionalDetail
		, lm.SortOrder
	from dbo.tbListingMedia lm
		inner join dbo.tbMedia m on lm.MediaID = m.MediaID
		inner join dbo.tbAttributeValue av on lm.ListingMediaTypeID = av.AttributeValueID
		inner join dbo.tbValue v on av.ValueID = v.ValueID
	where lm.ListingID = @ListingID
		and m.MediaTypeID = isnull(@MediaTypeID, m.MediaTypeID)
		and lm.ListingMediaTypeID = isnull(@ListingMediaTypeID, lm.ListingMediaTypeID)
		and lm.IsDeleted = 0
		and m.IsDeleted = 0
	order by lm.ListingMediaTypeID, lm.SortOrder

end
go
grant exec on dbo.upGetListingMedia to web