use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddRole') and type in (N'P', N'PC'))
drop procedure dbo.upAddRole
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddRole
(
	@RoleName varchar(50)
	, @CreateDateTime datetime2
	, @CreateID int
	, @RoleID tinyint output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbRole
			(RoleName, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@RoleName, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @RoleID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddRole - Role was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddRole to web