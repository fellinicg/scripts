use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddListingExcludeFromResult') and type in (N'P', N'PC'))
drop procedure dbo.upAddListingExcludeFromResult
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160421
-- ============================================================
create procedure dbo.upAddListingExcludeFromResult
(
	@ListingID int
	, @CompanyID int = null
	, @AgentID int = null
	, @CustomerID int = null
	, @CreateID int
	, @ListingExcludeFromResultID int output
	, @Result char(1) = 'F' output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Insert record
	insert into dbo.tbListingExcludeFromResult (ListingID, CompanyID, AgentID, CustomerID, CreateID, UpdateID)
	Values (@ListingID, @CompanyID, @AgentID, @CustomerID, @CreateID, @CreateID)

	-- Check for success
	if (@@error = 0) and (@@rowcount > 0)
		begin
			set @ListingExcludeFromResultID = @@identity
		end
	else
		raiserror('upAddListingExcludeFromResult - Listing was not successfully excluded', 16, 1)

end
go
grant exec on dbo.upAddListingExcludeFromResult to web