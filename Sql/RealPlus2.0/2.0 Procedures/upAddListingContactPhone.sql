use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddListingContactPhone') and type in (N'P', N'PC'))
drop procedure dbo.upAddListingContactPhone
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddListingContactPhone
(
	@ListingID int
	, @ListingContactTypeID int
	, @PersonPhoneID int
	, @ListingContactPhoneID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbListingContactPhone
			(ListingID, ListingContactTypeID, PersonPhoneID)
		values
			(@ListingID, @ListingContactTypeID, @PersonPhoneID)

		set @ListingContactPhoneID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddListingContactPhone - ListingContactPhone was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddListingContactPhone to web