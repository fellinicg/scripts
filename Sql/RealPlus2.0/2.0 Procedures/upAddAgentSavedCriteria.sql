use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddAgentSavedCriteria') and type in (N'P', N'PC'))
drop procedure dbo.upAddAgentSavedCriteria
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160315
-- ============================================================
create procedure dbo.upAddAgentSavedCriteria
(
	@AgentID int
	, @Detail varchar(200)
	, @Criteria xml
	, @CustomerID int = null
	, @CreateID int = null
	, @AgentSavedCriteriaID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	if @CreateID is null set @CreateID = @AgentID

	-- Check that this agent did not already use the description (detail)
	if exists(select 1 from dbo.tbAgentSavedCriteria where AgentID = @AgentID and Detail = @Detail)
		begin
			set @Result = 'X'
			raiserror('upAddAgentSavedCriteria - A saved criteria with this description already exists', 18, 1)
			return
		end

	-- Insert record
	insert into dbo.tbAgentSavedCriteria(AgentID, Detail, CustomerID, Criteria, CreateID, UpdateID)
	values (@AgentID, @Detail, @CustomerID, @Criteria, @CreateID, @CreateID)

	-- Check for success
	if (@@error = 0) and (@@rowcount > 0)
		begin
			set @AgentSavedCriteriaID = @@identity
			set @Result = 'S'
		end
	else
		raiserror('upAddAgentSavedCriteria - Criteria was not successfully saved', 16, 1)
end
go
grant exec on dbo.upAddAgentSavedCriteria to web