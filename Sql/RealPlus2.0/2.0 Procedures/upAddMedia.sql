use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddMedia') and type in (N'P', N'PC'))
drop procedure dbo.upAddMedia
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddMedia
(
	@MediaTypeID int
	, @Link varchar(500)
	, @CreateDateTime datetime2
	, @CreateID int
	, @MediaID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbMedia
			(MediaTypeID, Link, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@MediaTypeID, @Link, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @MediaID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddMedia - Media was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddMedia to web