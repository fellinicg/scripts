use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddCustomCriteriaSection') and type in (N'P', N'PC'))
drop procedure dbo.upAddCustomCriteriaSection
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160328
-- ============================================================
create procedure dbo.upAddCustomCriteriaSection
(
	@LongLabel varchar(100)
	, @CreateID int
	, @ShortLabel varchar(20) = null
	, @CustomCriteriaSectionID int = null output
	, @Result char(1) = 'F' output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Check for existence of CustomCriteriaSectionName
	select @CustomCriteriaSectionID = CustomCriteriaSectionID from dbo.tbCustomCriteriaSection where LongLabel = @LongLabel

	-- If the CustomCriteriaSectionName does NOT already exist
	if @@rowcount = 0
		begin
			-- Insert record, if the CustomCriteriaSectionName does NOT already exist
			insert into dbo.tbCustomCriteriaSection (ShortLabel, LongLabel, CreateID, UpdateID)
			values (@ShortLabel, @LongLabel,  @CreateID, @CreateID)

			-- Check for success
			if (@@error = 0) and (@@rowcount > 0)
				begin
					set @CustomCriteriaSectionID = @@identity
					set @Result = 'S'
			end
			else
				raiserror('upAddCustomCriteriaSection - Custom criteria section was not successfully added', 16, 1)
		end
end
go
grant exec on dbo.upAddCustomCriteriaSection to web