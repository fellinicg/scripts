use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddSharedCustomer') and type in (N'P', N'PC'))
drop procedure dbo.upAddSharedCustomer
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddSharedCustomer
(
	@CustomerID int
	, @AgentID int
	, @PermissionID char(1)
	, @CreateDateTime datetime2
	, @CreateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbSharedCustomer
			(CustomerID, AgentID,  PermissionID, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@CustomerID, @AgentID, @PermissionID, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddSharedCustomer - SharedCustomer was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddSharedCustomer to web