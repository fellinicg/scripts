use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetListingByCompany') and type in (N'P', N'PC'))
drop procedure dbo.upGetListingByCompany
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160415
-- ============================================================
create procedure dbo.upGetListingByCompany
	@Omni varchar(max)
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @TblResult table (ListingID int index ix1 clustered)

	-- Insert ListingIDs into table variable
	insert into @TblResult
	select ListingID from dbo.tfGetListingByCompany(@Omni)

	-- Return data
	select *
	from dbo.vwListingBySearch l
		inner join @tblresult r on l.ListingID = r.ListingID

end
go
grant exec on dbo.upGetListingByCompany to web