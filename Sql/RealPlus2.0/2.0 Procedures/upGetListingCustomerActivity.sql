use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetListingCustomerActivity') and type in (N'P', N'PC'))
drop procedure dbo.upGetListingCustomerActivity
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160420
-- ============================================================
create procedure dbo.upGetListingCustomerActivity
(
	@AgentID int
	, @ListingID int
)
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select p.FullName, a.Activity, a.UpdateDateTime
	from (select CustomerID, Activity, UpdateDateTime
			from dbo.tbAgentListingPin alp
				unpivot (value for Activity in (PickListed, Liked, Visited, Disliked)) up
			where ListingID = @ListingID
				and AgentID = @AgentID
				and CustomerID is not null
				and Value = 1) a
		inner join dbo.tbPerson p on a.CustomerID = p.PersonID

end
go
grant exec on dbo.upGetListingCustomerActivity to web


