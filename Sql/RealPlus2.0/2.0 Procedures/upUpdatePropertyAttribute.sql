use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdatePropertyAttribute') and type in (N'P', N'PC'))
drop procedure dbo.upUpdatePropertyAttribute
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160412
-- ============================================================
create procedure dbo.upUpdatePropertyAttribute
(
	@PropertyAttributeID int
	, @IsDeleted bit
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbPropertyAttribute
			set IsDeleted = isnull(@IsDeleted, IsDeleted)
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
		where PropertyAttributeID = @PropertyAttributeID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdatePropertyAttribute - PropertyAttribute was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdatePropertyAttribute to web