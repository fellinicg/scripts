use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddPropertyAttributeNumeric') and type in (N'P', N'PC'))
drop procedure dbo.upAddPropertyAttributeNumeric
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddPropertyAttributeNumeric
(
	@PropertyID int
	, @AttributeID int
	, @AttributeValue float
	, @CreateDateTime datetime2
	, @CreateID int
	, @PropertyAttributeNumericID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbPropertyAttributeNumeric
			(PropertyID, AttributeID, AttributeValue, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@PropertyID, @AttributeID, @AttributeValue, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @PropertyAttributeNumericID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddPropertyAttributeNumeric - PropertyAttributeNumeric was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddPropertyAttributeNumeric to web