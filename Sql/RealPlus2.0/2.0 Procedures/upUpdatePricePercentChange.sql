use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdatePricePercentChange') and type in (N'P', N'PC'))
drop procedure dbo.upUpdatePricePercentChange
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160324
-- ============================================================
create procedure dbo.upUpdatePricePercentChange
as
begin

	-- Initialize settings
	set nocount on

	-- Update PercentChange in tbListingPrice
	update dbo.tbListingPrice set PercentChange = b.pctdiff
	from dbo.tbListingPrice a
	inner join (
		select 
			ListingPriceID
			, cast(round(((coalesce(lag(ListingPrice) over (partition by ListingID order by ListingPriceID), ListingPrice) - ListingPrice) / nullif(ListingPrice, 0)) * -100, 0) as bigint) as pctdiff 
		from dbo.tbListingPrice
	) b on a.ListingPriceID = b.ListingPriceID

end