use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateCustomer') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateCustomer
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upUpdateCustomer
(
	@CustomerID int
	, @AgentID int
	, @StatusID int = null
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbCustomer
			set StatusID = isnull(@StatusID, StatusID)
			, IsDeleted = isnull(@IsDeleted, IsDeleted)
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
		where CustomerID = @CustomerID
			and AgentID = @AgentID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateCustomer - Customer was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateCustomer to web