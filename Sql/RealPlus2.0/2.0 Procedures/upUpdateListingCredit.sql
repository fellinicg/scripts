use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateListingCredit') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateListingCredit
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upUpdateListingCredit
(
	@ListingCreditID int
	, @ListingCreditTypeID int = null
	, @CreditPercent float = null
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbListingCredit
			set ListingCreditTypeID = isnull(@ListingCreditTypeID, ListingCreditTypeID)
			, CreditPercent = isnull(@CreditPercent, CreditPercent)
			, IsDeleted = isnull(@IsDeleted, IsDeleted)
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
			where ListingCreditID = @ListingCreditID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateListingCredit - ListingCredit was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateListingCredit to web