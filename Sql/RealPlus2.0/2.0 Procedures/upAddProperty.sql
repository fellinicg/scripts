use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddProperty') and type in (N'P', N'PC'))
drop procedure dbo.upAddProperty
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddProperty
(
	@PropertyClassID int
	, @ParentPropertyID int = null
	, @Block varchar(10) = null
	, @Lot varchar(10) = null
	, @Section varchar(10) = null
	, @Latitude varchar(20) = null
	, @Longitude varchar(20) = null
	, @CreateDateTime datetime
	, @CreateID int
	, @PropertyID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbProperty
			(PropertyClassID, ParentPropertyID, Block, Lot, Section, Latitude, Longitude, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@PropertyClassID, @ParentPropertyID, @Block, @Lot, @Section, @Latitude, @Longitude, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @PropertyID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddProperty - Property was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddProperty to web