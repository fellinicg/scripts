use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateCustomCriteriaItem') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateCustomCriteriaItem
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160328
-- ============================================================
create procedure dbo.upUpdateCustomCriteriaItem
(
	@CustomCriteriaItemID int
	, @AttributeValueID int = null
	, @LongLabel varchar(100) = null
	, @ShortLabel varchar(20) = null
	, @NumericValue decimal(12,2) = null
	, @SortOrder tinyint = null
	, @Display bit = null
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) = 'F' output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Update record
	update dbo.tbCustomCriteriaItem set 
		AttributeValueID = isnull(@AttributeValueID, AttributeValueID)
		, LongLabel = isnull(@LongLabel, LongLabel)
		, ShortLabel = isnull(@ShortLabel, ShortLabel)
		, NumericValue = isnull(@NumericValue, NumericValue)
		, SortOrder = isnull(@SortOrder, SortOrder)
		, Display = isnull(@Display, Display)
		, IsDeleted = isnull(@IsDeleted, IsDeleted)
		, UpdateID = @UpdateID
		, UpdateDateTime = getdate()
	where CustomCriteriaItemID = @CustomCriteriaItemID

	-- Check for success
	if (@@error = 0) and (@@rowcount > 0)
		begin
			set @Result = 'S'
		end
	else
		raiserror('upUpdateCustomCriteriaItem - Custom criteria item was not successfully Updated', 16, 1)

end
go
grant exec on dbo.upUpdateCustomCriteriaItem to web