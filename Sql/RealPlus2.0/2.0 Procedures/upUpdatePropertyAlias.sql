use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdatePropertyAlias') and type in (N'P', N'PC'))
drop procedure dbo.upUpdatePropertyAlias
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160412
-- ============================================================
create procedure dbo.upUpdatePropertyAlias
(
	@PropertyAliasID int
	, @PropertyAliasName varchar(100) = null
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbPropertyAlias
			set PropertyAliasName = isnull(@PropertyAliasName, PropertyAliasName)
			, IsDeleted = isnull(@IsDeleted, IsDeleted)
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
		where PropertyAliasID = @PropertyAliasID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdatePropertyAlias - PropertyAlias was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdatePropertyAlias to web