use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetListingBySearchOmni') and type in (N'P', N'PC'))
drop procedure dbo.upGetListingBySearchOmni
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160330
-- ============================================================
create procedure dbo.upGetListingBySearchOmni
	@ListingCategory int
	, @ActiveInactive varchar(3) = '0,1'
	, @ListingStatus varchar(max) = null
	, @WebOrListingNum varchar(max) = null
	, @PriceMin int = null
	, @PriceMax int = null
	, @Bedrooms float = null
	, @Bathrooms float = null
	, @HalfBathrooms float = null
	, @Rooms float = null
	, @OwnershipType varchar(max) = null
	, @ApartmentType varchar(max) = null
	, @ListDateStart varchar(20) = null
	, @ListDateEnd varchar(20) = null
	, @UpdateDateStart varchar(20) = null
	, @UpdateDateEnd varchar(20) = null
	, @ListUpdateDateStart varchar(20) = null
	, @ListUpdateDateEnd varchar(20) = null
	, @OpenHouseStart varchar(20) = null
	, @OpenHouseEnd varchar(20) = null
	, @MinSqFt int = null
	, @MaxPriceSqFt int = null
	, @IncMissingSqFt bit = null
	, @MaxMthlyExp int = null
	, @Neighborhood varchar(max) = null
	, @BuildingAmenity varchar(max) = null
	, @BuildingPersonnel varchar(max) = null
	, @BuildingAllow varchar(max) = null
	, @BuildingNotAllow varchar(max) = null
	, @ListActivityStart varchar(20) = null
	, @ListActivityEnd varchar(20) = null
	, @ListActivityStatus varchar(8) = null
	, @OutdoorSpace varchar(max) = null
	, @Views varchar(max) = null
	, @Exposure varchar(max) = null
	, @MultiFloor varchar(max) = null
	, @MinYearBuilt int = null
	, @ListingType varchar(max) = null
	, @AddressID varchar(max) = null
	, @NewDevelopment varchar(10) = null
	, @FinancingAllowed varchar(10) = null
	, @Omni varchar(max) = null
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @sql varchar(max) = ''
	declare @tblresult table (ListingID int index ix1 clustered)

	-- AddressID
	if @AddressID is not null 
		begin
			set @sql =  @sql + '
						(select l.ListingID
						from dbo.tbPropertyAddress pa
							inner join dbo.tfcommalisttotable(' + char(39) + @AddressID + char(39) + ') a on pa.PropertyAddressID = a.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pa.IsDeleted = 0
						union
						select l.ListingID
						from dbo.tbPropertyAddress pa
							inner join dbo.tfcommalisttotable(' + char(39) + @AddressID + char(39) + ') a on pa.PropertyAddressID = a.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
									where pa.IsDeleted = 0)
						intersect'
		end

	-- WebOrListingNum
	if @WebOrListingNum is not null 
		begin
			set @sql = @sql + '
						select distinct lat.ListingID
						from dbo.tbListingAttributeText lat 
							inner join dbo.tfCommaListToTable(' + char(39) + @WebOrListingNum + char(39) + ') on AttributeValue = ListItem
						where lat.IsDeleted = 0
							and lat.AttributeID in (54,53)
						intersect'
		end

	-- PriceMin and PriceMax
	if (@PriceMin is not null and @PriceMax is not null) 
		begin
			set @sql = @sql + '
						select ListingID
						from dbo.tbListing 
						where CurrentPrice between ' + cast(@PriceMin as varchar) + ' and ' + cast(@PriceMax as varchar) + '
						intersect'
		end

	-- MinYearBuilt
	if @MinYearBuilt is not null 
		begin
			set @sql = @sql + '
						(select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 106 
							and AttributeValue >= ' + cast(@MinYearBuilt as varchar) + '
						union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 106 
							and AttributeValue >= ' + cast(@MinYearBuilt as varchar) + ')
						intersect'
		end

	-- Bedrooms
	if @Bedrooms is not null 
		begin
			set @sql = @sql + '
						(select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 11 
							and AttributeValue >= ' + cast(@Bedrooms as varchar) + '
						union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 11 
							and AttributeValue >= ' + cast(@Bedrooms as varchar) + ')
						intersect'
		end
	
	-- Bathrooms
	if @Bathrooms is not null 
		begin
			set @sql = @sql + '
						(select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 10 
							and AttributeValue >= ' + cast(@Bathrooms as varchar) + '
						union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 10 
							and AttributeValue >= ' + cast(@Bathrooms as varchar) + ')
						intersect'
		end

	-- HalfBathrooms
	if @HalfBathrooms is not null 
		begin
			set @sql = @sql + '
						(select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 41 
							and AttributeValue >= ' + cast(@HalfBathrooms as varchar) + '
						union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 41 
							and AttributeValue >= ' + cast(@HalfBathrooms as varchar) + ')
						intersect'
		end

	-- Rooms
	if @Rooms is not null 
		begin
			set @sql = @sql + '
						(select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 85 
							and AttributeValue >= ' + cast(@Rooms as varchar) + '
						union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 85 
							and AttributeValue >= ' + cast(@Rooms as varchar) + ')
						intersect'
		end

	-- MinSqFt
	if @MinSqFt is not null 
		begin
			set @sql = @sql + '
						(select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 91 
							and AttributeValue >= ' + cast(@MinSqFt as varchar) + '
						union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 91 
							and AttributeValue >= ' + cast(@MinSqFt as varchar)
			if @IncMissingSqFt = 1
				set @sql = @sql + '
						union
						select l.ListingID
						from dbo.tbListing l
							left join dbo.tbPropertyAttributeNumeric pan on l.PropertyID = pan.PropertyID and pan.AttributeID = 91
						where pan.PropertyAttributeNumericID is null)'
			else
				set @sql = @sql + ')intersect'
		end

	-- MaxPriceSqFt
	if @MaxPriceSqFt is not null 
		begin
			set @sql = @sql + '
						(select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 79
							and AttributeValue <= ' + cast(@MaxPriceSqFt as varchar) + '
						union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 79
							and AttributeValue <= ' + cast(@MaxPriceSqFt as varchar) + ')
						intersect'
		end
	
	-- MaxMthlyExp
	if @MaxMthlyExp is not null 
		begin
			set @sql = @sql + '
						(select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 76
							and AttributeValue <= ' + cast(@MaxMthlyExp as varchar) + '
						union
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 76
							and AttributeValue <= ' + cast(@MaxMthlyExp as varchar) + ')
						intersect'
		end
	
	-- ListDateStart and ListDateEnd
	if (@ListDateStart is not null or @ListDateEnd is not null) 
		begin
			set @sql = @sql + '
						(select ListingID
						from dbo.tbListing 
						where IsDeleted = 0
							and ListDate between ' + char(39) + @ListDateStart + char(39) + ' and ' + char(39) + @ListDateEnd + char(39) + ' 
						)intersect'
		end

	-- UpdateDateStart and UpdateDateEnd
	if (@UpdateDateStart is not null or @UpdateDateEnd is not null) 
		begin
			set @sql = @sql + '
						(select ListingID
						from dbo.tbListing 
						where IsDeleted = 0
							and convert(date, UpdateDateTime) between ' + char(39) + @UpdateDateStart + char(39) + ' and ' + char(39) + @UpdateDateEnd + char(39) + ' 
						)intersect'
		end

	-- ListUpdateDateStart and ListUpdateDateEnd
	if (@ListUpdateDateStart is not null or @ListUpdateDateEnd is not null) 
		begin
			set @sql = @sql + '
						(select ListingID
						from dbo.tbListing 
						where IsDeleted = 0
							and ListDate between ' + char(39) + @ListDateStart + char(39) + ' and ' + char(39) + @ListDateEnd + char(39) + ' 
						union
						select ListingID
						from dbo.tbListing 
						where IsDeleted = 0
							and convert(date, UpdateDateTime) between ' + char(39) + @UpdateDateStart + char(39) + ' and ' + char(39) + @UpdateDateEnd + char(39) + ' 
						)intersect'
		end

	-- OpenHouseStart and OpenHouseEnd
	if (@OpenHouseStart is not null or @OpenHouseEnd is not null) 
		begin
			set @sql = @sql + '
						(select ListingID
						from dbo.tbListingOpenHouse 
						where IsDeleted = 0
							and (convert(date, StartDateTime) between ' + char(39) + @OpenHouseStart + char(39) + ' and ' + char(39) + @OpenHouseEnd + char(39) + ' 
								or convert(date, EndDateTime) between ' + char(39) + @OpenHouseStart + char(39) + ' and ' + char(39) + @OpenHouseEnd + char(39) + ')
						)intersect'
		end

	-- ListActivityStart and ListActivityEnd
	if (@ListActivityStart is not null or @ListActivityEnd is not null) 
		begin
			set @sql = @sql + '
						(select distinct ListingID
						from dbo.tbListingStatus
						where convert(date, StatusDateTime) between ' + char(39) + @ListActivityStart + char(39) + ' and ' + char(39) + @ListActivityEnd + char(39) + '
							and StatusID = isnull(' + @ListActivityStatus + ', StatusID)
						)intersect'
		end

	-- NewDevelopment
	if @NewDevelopment is not null
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @NewDevelopment + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @NewDevelopment + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID)
						intersect'
		end

	-- Neighborhood
	if @Neighborhood is not null 
		begin
			set @sql = @sql + '
						(select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @Neighborhood + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @Neighborhood + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID)
						intersect'
		end

	-- BuildingAmenity
	if @BuildingAmenity is not null
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @BuildingAmenity + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingAmenity + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID)
						intersect'
		end

	-- BuildingPersonnel
	if @BuildingPersonnel is not null
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @BuildingPersonnel + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingPersonnel + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID)
						intersect'
		end

	-- BuildingAllow
	if @BuildingAllow is not null
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @BuildingAllow + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingAllow + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID)
						intersect'
		end

	-- BuildingNotAllow
	if @BuildingNotAllow is not null
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @BuildingNotAllow + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @BuildingNotAllow + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID)
						intersect'
		end

	-- OutdoorSpace
	if @OutdoorSpace is not null
		begin
			set @sql = @sql + '
						(select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @OutdoorSpace + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @OutdoorSpace + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID)
						intersect'
		end

	-- Views
	if @Views is not null
		begin
			set @sql = @sql + '
						(select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @Views + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @Views + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID)
						intersect'
		end

	-- Exposure
	if @Exposure is not null
		begin
			set @sql = @sql + '
						(select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @Exposure + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @Exposure + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID)
						intersect'
		end

	-- MultiFloor
	if @MultiFloor is not null
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @MultiFloor + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @MultiFloor + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						)intersect'
		end

	-- OwnershipType
	if @OwnershipType is not null 
		begin
			set @sql = @sql + '
						(
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @OwnershipType + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @OwnershipType + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID)
						intersect'
		end

	-- ApartmentType
	if @ApartmentType is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttribute pa
							inner join dbo.tfCommaListToTable(' + char(39) + @ApartmentType + char(39) + ') t on pa.AttributeValueID = t.ListItem
							inner join dbo.tbListing l on pa.PropertyID = l.PropertyID
						--union
						--select l.ListingID
						--from dbo.tbPropertyAttribute pa
						--	inner join dbo.tfCommaListToTable(' + char(39) + @ApartmentType + char(39) + ') t on pa.AttributeValueID = t.ListItem
						--	inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
						--	inner join dbo.tbListing l on p.PropertyID = l.PropertyID
							)
						intersect'
		end

	-- FinancingAllowed
	if @FinancingAllowed is not null 
		begin
			set @sql = @sql + '
						(
						select l.ListingID
						from dbo.tbPropertyAttributeNumeric pan
							inner join dbo.tbProperty p on pan.PropertyID = p.ParentPropertyID
							inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						where pan.IsDeleted = 0 
							and l.IsDeleted = 0
							and pan.AttributeID = 36 
							and AttributeValue > 0
						--union
						--select l.ListingID
						--from dbo.tbPropertyAttributeNumeric pan
						--	inner join dbo.tbProperty p on pan.PropertyID = p.PropertyID
						--	inner join dbo.tbListing l on p.PropertyID = l.PropertyID
						--where pan.IsDeleted = 0 
						--	and l.IsDeleted = 0
						--	and pan.AttributeID = 36 
						--	and AttributeValue > 0
						)intersect'
		end

	-- ListingStatus
	if @ListingStatus is not null 
		begin
			set @sql = @sql + '
						(select l.ListingID 
						from dbo.tbListing l 
							inner join dbo.tbListingStatus ls on l.CurrentStatusID = ls.ListingStatusID
							inner join (select ListItem from dbo.tfCommaListToTable(' + char(39) + @ListingStatus + char(39) + ' )) b on ls.StatusID = b.ListItem
						where l.IsDeleted = 0) 
						intersect'
		end

	-- ListingType
	if @ListingType is not null
		begin
			set @sql = @sql + '
						(select l.ListingID
						from dbo.tbListing l 
							inner join dbo.tfCommaListToTable(' + char(39) + @ListingType + char(39) + ') t on l.ListingTypeID = t.ListItem
						where l.IsDeleted = 0)
						intersect'
		end

	-- Omni
	if @Omni is not null 
		begin
			set @sql =  @sql + '
						(select ListingID
						from dbo.tfGetListingByOmni(' + char(39) + @Omni + char(39) + '))
						intersect'
		end

	-- Remove last intersect if it exists
	if right(@sql, 9) = 'intersect' set @sql = left(@sql, len(@sql) - 9)

	-- Check if ListingCategory was the only parameter (BAD)
	if isnull(@sql,'') = ''
		begin
			-- Execute final sql statement to insert ListingIDs into table variable
			insert into @tblresult
			select ListingID
			from dbo.tbListing
			where ListingCategoryID = @ListingCategory and IsDeleted = 0
		end
	else
		begin
			-- Execute final sql statement to insert ListingIDs into table variable
			insert into @tblresult
			exec(@sql)
		end

	-- Return data
	select *
	from dbo.vwListingBySearch l
		inner join @tblresult r on l.ListingID = r.ListingID
	where l.ListingCategoryID = @ListingCategory

end
go
grant exec on dbo.upGetListingBySearchOmni to web