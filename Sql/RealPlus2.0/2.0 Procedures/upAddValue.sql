use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddValue') and type in (N'P', N'PC'))
drop procedure dbo.upAddValue
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160324
-- ============================================================
create procedure dbo.upAddValue
(
	@ValueName varchar(100)
	, @CreateID int
	, @ValueCode varchar(10) = null
	, @ValueID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Check for existence of ValueName
	select @ValueID = ValueID from dbo.tbValue where ValueName = @ValueName

	-- If the ValueName does NOT already exist
	if @@rowcount = 0
		begin
			-- Insert record, if the ValueName does NOT already exist
			insert into dbo.tbValue (ValueName, ValueCode, CreateID, UpdateID)
			values (@ValueName, @ValueCode,  @CreateID, @CreateID)

			-- Check for success
			if (@@error = 0) and (@@rowcount > 0)
				begin
					set @ValueID = @@identity
					set @Result = 'S'
				end
			else
				raiserror('upAddValue - Value was not successfully added', 16, 1)
		end
end
go
grant exec on dbo.upAddValue to web