use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upCreateParams') and type in (N'P', N'PC'))
drop procedure dbo.upCreateParams
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160316
-- ============================================================
create procedure dbo.upCreateParams
	@TableName varchar(200)
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select ',@' + 
		COLUMN_NAME + 
		' ' + 
		DATA_TYPE + 
		case 
			when CHARACTER_MAXIMUM_LENGTH is not null 
			then '(' + cast(CHARACTER_MAXIMUM_LENGTH as varchar) + ')'
			else ''
		end +
		case 
			when COLUMN_DEFAULT is not null 
			then ' = ' + COLUMN_DEFAULT 
			else ''
		end
	from INFORMATION_SCHEMA.COLUMNS
	where TABLE_NAME = @TableName
	order by ORDINAL_POSITION

end
go
