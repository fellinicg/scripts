use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateCustomCriteriaSection') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateCustomCriteriaSection
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160328
-- ============================================================
create procedure dbo.upUpdateCustomCriteriaSection
(
	@CustomCriteriaSectionID int
	, @LongLabel varchar(100) = null
	, @ShortLabel varchar(20) = null
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) = 'F' output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Update record
	update dbo.tbCustomCriteriaSection set 
		LongLabel = isnull(@LongLabel, LongLabel)
		, ShortLabel = isnull(@ShortLabel, ShortLabel)
		, IsDeleted = isnull(@IsDeleted, IsDeleted)
		, UpdateID = @UpdateID
		, UpdateDateTime = getdate()
	where CustomCriteriaSectionID = @CustomCriteriaSectionID

	-- Check for success
	if (@@error = 0) and (@@rowcount > 0)
		begin
			set @Result = 'S'
		end
	else
		raiserror('upUpdateCustomCriteriaSection - Custom criteria section was not successfully Updated', 16, 1)
end
go
grant exec on dbo.upUpdateCustomCriteriaSection to web