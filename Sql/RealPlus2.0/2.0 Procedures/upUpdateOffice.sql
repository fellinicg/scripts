use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateOffice') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateOffice
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160412
-- ============================================================
create procedure dbo.upUpdateOffice
(
	@OfficeID int
	, @OfficeCode varchar(10) = null
	, @Detail varchar(50) = null
	, @PostalAddressID int = null
	, @PhoneNumber varchar(20) = null
	, @Fax varchar(20) = null
	, @Email varchar(50) = null
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbOffice
			set OfficeCode = isnull(@OfficeCode, OfficeCode)
			, Detail = isnull(@Detail, Detail)
			, PostalAddressID = isnull(@PostalAddressID, PostalAddressID)
			, PhoneNumber = isnull(@PhoneNumber, PhoneNumber)
			, Fax = isnull(@Fax, Fax)
			, Email = isnull(@Email, Email)
			, IsDeleted = isnull(@IsDeleted, IsDeleted)
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
		where OfficeID = @OfficeID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateOffice - Office was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateOffice to web