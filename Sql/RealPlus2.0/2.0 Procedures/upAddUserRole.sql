use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddUserRole') and type in (N'P', N'PC'))
drop procedure dbo.upAddUserRole
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddUserRole
(
	@PersonID int 
	, @RoleID tinyint 
	, @CreateDateTime datetime2
	, @CreateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbUserRole
			(PersonID, RoleID, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@PersonID, @RoleID, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddUserRole - UserRole was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddUserRole to web