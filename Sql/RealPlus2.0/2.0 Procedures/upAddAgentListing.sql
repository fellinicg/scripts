use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddAgentListing') and type in (N'P', N'PC'))
drop procedure dbo.upAddAgentListing
go


-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddAgentListing
(
	@AgentID int
	, @ListingID int
	, @CreateDateTime datetime2
	, @CreateID int
	, @AgentListingID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbAgentListing
			(AgentID, ListingID, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@AgentID, @ListingID, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @AgentListingID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddAgentListing - AgentListing was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddAgentListing to web