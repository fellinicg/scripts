use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetRecentMatchSettings') and type in (N'P', N'PC'))
drop procedure dbo.upGetRecentMatchSettings
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160315
-- ============================================================
create procedure dbo.upGetRecentMatchSettings
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select AttributeValueID, AttributeName, ValueName, IsDeleted
	from dbo.vwAttributeValues
	where AttributeName = 'recent match settings'

end
go
grant exec on dbo.upGetRecentMatchSettings to web