use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddListingComment') and type in (N'P', N'PC'))
drop procedure dbo.upAddListingComment
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddListingComment
(
	@ListingID int
	, @DetailID int
	, @CompanyID int
	, @PersonID int
	, @ListingCommentID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbListingComment
			(ListingID, DetailID, CompanyID, PersonID)
		values
			(@ListingID, @DetailID, @CompanyID, @PersonID)

		set @ListingCommentID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddListingComment - ListingComment was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddListingComment to web