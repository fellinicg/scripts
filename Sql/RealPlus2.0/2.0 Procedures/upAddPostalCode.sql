use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddPostalCode') and type in (N'P', N'PC'))
drop procedure dbo.upAddPostalCode
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddPostalCode
(
	@PostalCode char(5)
	, @CityTown varchar(40)
	, @StateProvince char(2)
	, @Country char(2) = null
	, @CreateDateTime datetime2
	, @PostalCodeID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbPostalCode
			(PostalCode, CityTown, StateProvince, Country, CreateDateTime)
		values
			(@PostalCode, @CityTown, @StateProvince, @Country, @CreateDateTime)

		set @PostalCodeID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddPostalCode - PostalCode was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddPostalCode to web