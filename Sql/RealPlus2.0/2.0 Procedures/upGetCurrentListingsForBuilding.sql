use Jaguar_v2_Prototype2_BHS_2
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetCurrentListingsForBuilding') and type in (N'P', N'PC'))
drop procedure dbo.upGetCurrentListingsForBuilding
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160413
-- ============================================================
create procedure dbo.upGetCurrentListingsForBuilding
	@ListingID int = null
	, @PropertyID int = null
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		l.ListingID
		, dbo.sfGetListingAttribute(l.ListingID, 'UnitIdentifier') 'UnitIdentifier'
		, l.CurrentStatus
		, l.UpdateDateTime
		, l.CurrentPrice
		, dbo.sfGetListingActivity (l.ListingID, 2549) 'ListDate'
		, dbo.sfGetListingAttribute(l.ListingID, 'dom') 'DOM'
		, dbo.sfGetListingActivity (l.ListingID, 2552) 'Closed'
		, dbo.sfGetListingAttribute(l.ListingID, 'Bedrooms') 'Bedrooms'
		, dbo.sfGetListingAttribute(l.ListingID, 'Bathrooms') 'Bathrooms'
		, dbo.sfGetListingAttribute(l.ListingID, 'SquareFootage') 'SquareFootage'
	from dbo.tbProperty p
		inner join dbo.tbListing l on p.PropertyID = l.PropertyID
		inner join dbo.tbListingStatus ls on l.CurrentStatusID = ls.ListingStatusID
	where p.ParentPropertyID = coalesce(@PropertyID, dbo.sfGetListingParentPropertyID(@ListingID))
		and l.IsDeleted = 0
		and ls.StatusID in (163, 3593, 3594, 3595, 3596, 3599, 3600, 3601)

end
go
grant exec on dbo.upGetCurrentListingsForBuilding to web