use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetListingAttributes') and type in (N'P', N'PC'))
drop procedure dbo.upGetListingAttributes
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160405
-- ============================================================
create procedure dbo.upGetListingAttributes
	@ListingID int
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		pa.ListingID
		, a.AttributeID
		, a.AttributeCode
		, a.AttributeName
		, v.ValueName AttributeValue
	from dbo.tbListingAttribute pa 
		inner join dbo.tbAttributeValue av on pa.AttributeValueID = av.AttributeValueID and av.Isdeleted = 0
		inner join dbo.tbAttribute a on av.AttributeID = a.AttributeID and a.IsDeleted = 0
		inner join dbo.tbValue v on av.ValueID = v.ValueID and v.Isdeleted = 0
	where 
		pa.IsDeleted = 0
		and pa.ListingID = @ListingID
	union
	select 
		pan.ListingID
		, a.AttributeID
		, a.AttributeCode
		, a.AttributeName
		, format(pan.AttributeValue, N'#.##############################')
	from dbo.tbListingAttributeNumeric pan 
		inner join dbo.tbAttribute a on pan.AttributeID = a.AttributeID and a.IsDeleted = 0
	where 
		pan.IsDeleted = 0 
		and pan.ListingID = @ListingID
	union
	select 
		pat.ListingID
		, a.AttributeID
		, a.AttributeCode
		, a.AttributeName
		, pat.AttributeValue
	from dbo.tbListingAttributeText pat 
		inner join dbo.tbAttribute a on pat.AttributeID = a.AttributeID and a.IsDeleted = 0
	where 
		pat.IsDeleted = 0 
		and pat.ListingID = @ListingID
	union
	select 
		l.ListingID
		, a.AttributeID
		, a.AttributeCode
		, a.AttributeName
		, v.ValueName AttributeValue
	from dbo.tbListing l
		inner join dbo.tbPropertyAttribute pa on l.PropertyID = pa.PropertyID
		inner join dbo.tbAttributeValue av on pa.AttributeValueID = av.AttributeValueID and av.Isdeleted = 0
		inner join dbo.tbAttribute a on av.AttributeID = a.AttributeID and a.IsDeleted = 0
		inner join dbo.tbValue v on av.ValueID = v.ValueID and v.Isdeleted = 0
	where 
		pa.IsDeleted = 0
		and l.ListingID = @ListingID
	union
	select 
		l.ListingID
		, a.AttributeID
		, a.AttributeCode
		, a.AttributeName
		, v.ValueName AttributeValue
	from dbo.tbListing l
		inner join dbo.tbProperty p on l.PropertyID = p.PropertyID
		inner join dbo.tbPropertyAttribute pa on p.ParentPropertyID = pa.PropertyID
		inner join dbo.tbAttributeValue av on pa.AttributeValueID = av.AttributeValueID and av.Isdeleted = 0
		inner join dbo.tbAttribute a on av.AttributeID = a.AttributeID and a.IsDeleted = 0
		inner join dbo.tbValue v on av.ValueID = v.ValueID and v.Isdeleted = 0
	where 
		pa.IsDeleted = 0
		and l.ListingID = @ListingID
	union
	select 
		l.ListingID
		, a.AttributeID
		, a.AttributeCode
		, a.AttributeName
		, convert(varchar, pan.AttributeValue) AttributeValue
	from dbo.tbListing l
		inner join dbo.tbPropertyAttributeNumeric pan on l.PropertyID = pan.PropertyID
		inner join dbo.tbAttribute a on pan.AttributeID = a.AttributeID and a.IsDeleted = 0
	where 
		pan.IsDeleted = 0 
		and l.ListingID = @ListingID
	union
	select 
		l.ListingID
		, a.AttributeID
		, a.AttributeCode
		, a.AttributeName
		, convert(varchar, pan.AttributeValue) AttributeValue
	from dbo.tbListing l
		inner join dbo.tbProperty p on l.PropertyID = p.PropertyID
		inner join dbo.tbPropertyAttributeNumeric pan on p.ParentPropertyID = pan.PropertyID
		inner join dbo.tbAttribute a on pan.AttributeID = a.AttributeID and a.IsDeleted = 0
	where 
		pan.IsDeleted = 0 
		and l.ListingID = @ListingID
	union
	select 
		l.ListingID
		, a.AttributeID
		, a.AttributeCode
		, a.AttributeName
		, convert(varchar, pat.AttributeValue) AttributeValue
	from dbo.tbListing l
		inner join dbo.tbPropertyAttributeText pat on l.PropertyID = pat.PropertyID
		inner join dbo.tbAttribute a on pat.AttributeID = a.AttributeID and a.IsDeleted = 0
	where 
		pat.IsDeleted = 0 
		and l.ListingID = @ListingID
	union
	select 
		l.ListingID
		, a.AttributeID
		, a.AttributeCode
		, a.AttributeName
		, convert(varchar, pat.AttributeValue) AttributeValue
	from dbo.tbListing l
		inner join dbo.tbProperty p on l.PropertyID = p.PropertyID
		inner join dbo.tbPropertyAttributeText pat on p.ParentPropertyID = pat.PropertyID
		inner join dbo.tbAttribute a on pat.AttributeID = a.AttributeID and a.IsDeleted = 0
	where 
		pat.IsDeleted = 0 
		and l.ListingID = @ListingID
	union
	select 
		l.ListingID
		, -1
		, null
		, 'zipcode'
		, ppa.Postalcode
	from dbo.tbListing l
		left join dbo.tbProperty p on l.PropertyID = p.PropertyID
		left join dbo.tbProperty pp on p.ParentPropertyID = pp.PropertyID
		left join dbo.tbPropertyAddress pa on pp.PropertyID = pa.PropertyID and pa.IsPrimary = 1
		left join dbo.tbPostalAddress ppa on pa.PostalAddressID = ppa.PostalAddressID
	where 
		ppa.IsDeleted = 0 
		and l.ListingID = @ListingID
	union
	select 
		l.ListingID
		, -1
		, null
		, 'listingcompany'
		, c.CompanyName
	from dbo.tbListing l
		inner join dbo.tbListingOffice lo on l.ListingID = lo.ListingID
		inner join dbo.tbOffice o on lo.OfficeID = o.OfficeID
		inner join dbo.tbCompany c on o.CompanyID = c.CompanyID
	where 
		c.IsDeleted = 0 
		and l.ListingID = @ListingID
	union
	select 
		l.ListingID
		, -1
		, null
		, 'listingtype'
		, dbo.sfGetAttributeValue(l.ListingTypeID)
	from dbo.tbListing l
	where 
		l.ListingID = @ListingID
	order by AttributeName

end
go
grant exec on dbo.upGetListingAttributes to web