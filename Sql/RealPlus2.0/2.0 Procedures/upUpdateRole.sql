use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateRole') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateRole
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160412
-- ============================================================
create procedure dbo.upUpdateRole
(
	@RoleID tinyint
	, @RoleName varchar(50) = null
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbRole
			set RoleName = isnull(@RoleName, RoleName)
			, IsDeleted = isnull(@IsDeleted, IsDeleted)
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
		where RoleID = @RoleID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateRole - Role was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateRole to web