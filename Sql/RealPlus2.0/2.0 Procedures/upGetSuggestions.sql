use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetSuggestions') and type in (N'P', N'PC'))
drop procedure dbo.upGetSuggestions
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160417
-- ============================================================
create procedure dbo.upGetSuggestions
	@Omni varchar(max)
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select
		'Agent' 'FoundIn'
		, p.FullName 'Result'
	from dbo.tbPerson p
		inner join dbo.tbAgentListing al on p.PersonID = al.AgentID
	where charindex(@Omni, p.FullName) > 0
	union
	select 
		'Company'
		, CompanyName
	from dbo.tbCompany
	where charindex(@Omni, CompanyName) > 0
	union
	select 
		'Address'
		, Detail
	from dbo.tbOffice
	where charindex(@Omni, Detail) > 0
	union
	select 
		'Address'
		, Line1
	from dbo.tbPostalAddress
	where charindex(@Omni, Line1) > 0
	union
	select 
		'Address'
		, Line2
	from dbo.tbPostalAddress
	where charindex(@Omni, Line2) > 0
	union
	select 
		'Address'
		, Line3
	from dbo.tbPostalAddress
	where charindex(@Omni, Line3) > 0
	union
	select 
		'Address'
		, Line4
	from dbo.tbPostalAddress
	where charindex(@Omni, Line4) > 0
	union
	select 
		'Address'
		, CityTown
	from dbo.tbPostalAddress
	where charindex(@Omni, CityTown) > 0
	union
	select 
		'Address'
		, StateProvince
	from dbo.tbPostalAddress
	where charindex(@Omni, StateProvince) > 0
	union
	select 
		'Address'
		, PostalCode
	from dbo.tbPostalAddress
	where charindex(@Omni, PostalCode) > 0
	union
	select 
		'ListingID'
		, AttributeValue
	from dbo.tbListingAttributeText  
	where AttributeID = 54	
		and charindex(@Omni, AttributeValue) > 0
	union
	select 
		'ListingID'
		, AttributeValue
	from dbo.tbListingAttributeText  
	where AttributeID = 53
		and charindex(@Omni, AttributeValue) > 0
	order by 'FoundIn', 'Result'


end
go
grant exec on dbo.upGetSuggestions to web