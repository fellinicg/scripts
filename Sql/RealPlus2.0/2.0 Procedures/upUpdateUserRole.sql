use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateUserRole') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateUserRole
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160412
-- ============================================================
create procedure dbo.upUpdateUserRole
(
	@PersonID int
	, @RoleID tinyint
	, @IsDeleted bit
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbUserRole
			set IsDeleted = @IsDeleted
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
		where PersonID = @PersonID 
				and RoleID = @RoleID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateUserRole - UserRole was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateUserRole to web