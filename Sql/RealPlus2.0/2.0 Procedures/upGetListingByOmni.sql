use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetListingByOmni') and type in (N'P', N'PC'))
drop procedure dbo.upGetListingByOmni
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160415
-- ============================================================
create procedure dbo.upGetListingByOmni
	@Omni varchar(max)
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @TblResult table (ListingID int index ix1 clustered)

	-- Insert ListingIDs into table variable
	insert into @TblResult
	select ListingID from dbo.tfGetListingByOmni(@Omni)

	-- Return data
	select *
	from dbo.vwListingBySearch l
		inner join @tblresult r on l.ListingID = r.ListingID

end
go
grant exec on dbo.upGetListingByOmni to web