use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddPersonPhone') and type in (N'P', N'PC'))
drop procedure dbo.upAddPersonPhone
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160316
-- ============================================================
create procedure dbo.upAddPersonPhone
(
	 @PersonID int
	, @PhoneTypeID int
	, @FullPhoneNumber varchar(30)
	, @CreateID int
	, @AreaCode tinyint = null
	, @PhoneNumber int = null
	, @Extension smallint = null
	, @TxtMsgCapable bit = 0
	, @IsPreferred bit = 0
	, @PersonPhoneID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Insert record
	insert into dbo.tbPersonPhone (PersonID, PhoneTypeID, FullPhoneNumber, AreaCode, PhoneNumber, Extension, TxtMsgCapable, IsPreferred, CreateID, UpdateID)
	values (@PersonID, @PhoneTypeID, @FullPhoneNumber, @AreaCode, @PhoneNumber, @Extension, @TxtMsgCapable, @IsPreferred, @CreateID, @CreateID)

	-- Check for success
	if (@@error = 0) and (@@rowcount > 0)
		begin
			set @PersonPhoneID = @@identity
			set @Result = 'S'
		end
	else
		raiserror('upAddPersonPhone - Phone was not successfully added', 16, 1)
end
go
grant exec on dbo.upAddPersonPhone to web