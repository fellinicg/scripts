use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddOfficeContact') and type in (N'P', N'PC'))
drop procedure dbo.upAddOfficeContact
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddOfficeContact
(
	@OfficeID int
	, @PersonID int
	, @IsPrimary bit
	, @CreateDateTime datetime2
	, @CreateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbOfficeContact
			(OfficeID, PersonID, IsPrimary, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@OfficeID, @PersonID, @IsPrimary, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddOfficeContact - OfficeContact was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddOfficeContact to web