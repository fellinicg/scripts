use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddUserAccount') and type in (N'P', N'PC'))
drop procedure dbo.upAddUserAccount
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddUserAccount
(
	@AccountName varchar(20)
	, @PasswordEncrypted varchar(200)
	, @UserAccountStatusID int
	, @CreateDateTime datetime2
	, @CreateID int
	, @UserAccountID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbUserAccount
			(AccountName, PasswordEncrypted, UserAccountStatusID, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@AccountName, @PasswordEncrypted, @UserAccountStatusID, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @UserAccountID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddUserAccount - UserAccount was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddUserAccount to web