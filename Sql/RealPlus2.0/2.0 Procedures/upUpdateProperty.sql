use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateProperty') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateProperty
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160412
-- ============================================================
create procedure dbo.upUpdateProperty
(
	@PropertyID int
	, @PropertyClassID int = null
	, @ParentPropertyID int = null
	, @Block varchar(10) = null
	, @Lot varchar(10) = null
	, @Section varchar(10) = null
	, @Latitude varchar(20) = null
	, @Longitude varchar(20) = null
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbProperty
			set PropertyClassID = isnull(@PropertyClassID, PropertyClassID)
			, ParentPropertyID = isnull(@ParentPropertyID, ParentPropertyID)
			, Block = isnull(@Block, Block)
			, Lot = isnull(@Lot, Lot)
			, Section = isnull(@Section, Section)
			, Latitude = isnull(@Latitude, Latitude)
			, Longitude = isnull(@Longitude, Longitude)
			, IsDeleted = isnull(@IsDeleted, IsDeleted)
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
		where PropertyID = @PropertyID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateProperty - Property was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateProperty to web