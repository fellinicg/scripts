use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetListingCredits') and type in (N'P', N'PC'))
drop procedure dbo.upGetListingCredits
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160406
-- ============================================================
create procedure dbo.upGetListingCredits
	@ListingID int
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select
		ListingCreditID
		, AgentID
		, dbo.sfGetPersonFullName(AgentID) 'Agent'
		, dbo.sfGetAttributeValue(ListingCreditTypeID) 'ListingCreditType'
		, CreditPercent
		, CreditDateTime
		, CreateDateTime
		, dbo.sfGetPersonFullName(CreateID) 'CreatedBy'
		, UpdateDateTime
		, dbo.sfGetPersonFullName(UpdateID) 'UpdatedBy'
	from dbo.tbListingCredit
	where ListingID = @ListingID
		and isDeleted = 0
	order by CreditDateTime desc

end
go
grant exec on dbo.upGetListingCredits to web