use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetWorkInProgressListings') and type in (N'P', N'PC'))
drop procedure dbo.upGetWorkInProgressListings
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160316
-- ============================================================
create procedure dbo.upGetWorkInProgressListings
	@AgentID int
	, @AgentWorkInProgressID int
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @xml as xml = (select WorkinProgress from dbo.tbAgentWorkInProgress where AgentID = @AgentID and AgentWorkInProgressID = @AgentWorkInProgressID)

	-- Return data
	select 
		wip.l.value('./@id', 'int') ListingID
		, a.*
		, wip.l.value('./@checked', 'bit') IsChecked
		, wip.l.value('./@removed', 'bit') IsRemoved
	from @xml.nodes('/wip/listing') wip(l)
		inner join dbo.tbListingFull a on a.ListingID = wip.l.value('./@id', 'int')

end
go
grant exec on dbo.upGetWorkInProgressListings to web