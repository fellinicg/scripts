use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddPerson') and type in (N'P', N'PC'))
drop procedure dbo.upAddPerson
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160315
-- ============================================================
create procedure dbo.upAddPerson
(
	@FullName varchar(200)
	, @CreateID int
	, @NamePrefix varchar(20) = null
	, @FirstGivenName varchar(40) = null
	, @SecondGivenName varchar(40) = null
	, @FamilyName varchar(60) = null
	, @NameSuffix varchar(40) = null
	, @PersonID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Insert record
	insert into dbo.tbPerson (NamePrefix, FirstGivenName, SecondGivenName, FamilyName, NameSuffix, FullName, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
	values (@NamePrefix, @FirstGivenName, @SecondGivenName, @FamilyName, @NameSuffix, @FullName, getdate(), @CreateID, getdate(), @CreateID)

	-- Check for success
	if (@@error = 0) and (@@rowcount > 0)
		begin
			set @PersonID = @@identity
			set @Result = 'S'
		end
	else
		raiserror('upAddPerson - person was not successfully added', 16, 1)
end
go
grant exec on dbo.upAddPerson to web