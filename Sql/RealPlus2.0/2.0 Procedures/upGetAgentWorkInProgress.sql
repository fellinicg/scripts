use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetAgentWorkInProgress') and type in (N'P', N'PC'))
drop procedure dbo.upGetAgentWorkInProgress
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160316
-- ============================================================
create procedure dbo.upGetAgentWorkInProgress
	@AgentID int
	, @AgentWorkInProgressID int
	, @IncludeDeleted bit = 0
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		a.CustomerID
		, a.Detail
		, a.WorkInProgress
		, a.SortBy
		, a.Bookmark
		, a.AgentSavedCriteriaID
		, b.Detail 'SearchCriteriaDetail'
		, b.Criteria
	from dbo.tbAgentWorkInProgress a
		left join dbo.tbAgentSavedCriteria b on a.AgentID = b.agentid and isnull(a.AgentSavedCriteriaID, 0) = b.AgentSavedCriteriaID
	where 
		a.AgentID = @AgentID 
		and 
		a.AgentWorkInProgressID = @AgentWorkInProgressID
		and
		(a.IsDeleted = 0 or a.IsDeleted = @IncludeDeleted)
end
go
grant exec on dbo.upGetAgentWorkInProgress to web