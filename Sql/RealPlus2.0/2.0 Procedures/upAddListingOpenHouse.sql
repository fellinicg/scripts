use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddListingOpenHouse') and type in (N'P', N'PC'))
drop procedure dbo.upAddListingOpenHouse
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddListingOpenHouse
(
	@ListingID int
	, @AgentID int
	, @OfficeID int
	, @StartDateTime datetime2
	, @EndDateTime datetime2
	, @Comment varchar(200) = null
	, @AppointmentOnly bit
	, @CreateDateTime datetime2
	, @CreateID int
	, @ListingOpenHouseID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbListingOpenHouse
			(ListingID, AgentID, OfficeID, StartDateTime, EndDateTime, Comment, AppointmentOnly, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@ListingID, @AgentID, @OfficeID, @StartDateTime, @EndDateTime, @Comment, @AppointmentOnly, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @ListingOpenHouseID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddListingOpenHouse - ListingOpenHouse was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddListingOpenHouse to web