use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddListingAttribute') and type in (N'P', N'PC'))
drop procedure dbo.upAddListingAttribute
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddListingAttribute
(
	@ListingID int
	, @AttributeValueID int
	, @CreateDateTime datetime2
	, @CreateID int
	, @ListingAttributeID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbListingAttribute
			(ListingID, AttributeValueID, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@ListingID, @AttributeValueID, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @ListingAttributeID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddListingAttribute - ListingAttribute was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddListingAttribute to web