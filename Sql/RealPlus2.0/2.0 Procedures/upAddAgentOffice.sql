use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddAgentOffice') and type in (N'P', N'PC'))
drop procedure dbo.upAddAgentOffice
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160315
-- ============================================================
create procedure dbo.upAddAgentOffice
(
	@AgentID int
	, @OfficeID int
	, @CreateID int
	, @AgentOfficeID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Check for existence of agent/office
	select @AgentOfficeID = AgentOfficeID from dbo.tbAgentOffice where AgentID = @AgentID and OfficeID = @OfficeID

	-- If the agent/office does NOT already exist
	if @@rowcount = 0
		begin
			-- Insert record
			insert into dbo.tbAgentOffice (AgentID, OfficeID, CreateID, UpdateID)
			values (@AgentID, @OfficeID, @CreateID, @CreateID)

			-- Check for success
			if (@@error = 0) and (@@rowcount > 0)
				begin
					set @AgentOfficeID = @@identity
					set @Result = 'S'
				end
			else
				raiserror('upAddAgentOffice - AgentOffice was not successfully added', 16, 1)
		end

end
go
grant exec on dbo.upAddAgentOffice to web