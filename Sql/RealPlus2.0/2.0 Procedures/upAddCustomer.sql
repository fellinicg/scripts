use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddCustomer') and type in (N'P', N'PC'))
drop procedure dbo.upAddCustomer
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160315
-- ============================================================
create procedure dbo.upAddCustomer
(
	@FullName varchar(200)
	, @EmailAddress nvarchar(254)
	, @AgentID int
	, @RecentMatchSettingID varchar(200) = null
	, @EmailTypeID int = null
	, @EmailAddressIsPreferred bit = 0
	, @NamePrefix varchar(20) = null
	, @FirstGivenName varchar(40) = null
	, @SecondGivenName varchar(40) = null
	, @FamilyName varchar(60) = null
	, @NameSuffix varchar(40) = null
	, @CustomerID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	if @EmailTypeID is null set @EmailTypeID = dbo.sfGetAttributeValueID('EmailType', 'Home')

	-- Begin transaction
	begin tran

	-- Add record to tbPerson
	exec dbo.upAddPerson 
		@FullName = @FullName
		, @CreateID = @AgentID
		, @NamePrefix = @NamePrefix
		, @FirstGivenName = @FirstGivenName
		, @SecondGivenName = @SecondGivenName
		, @FamilyName = @FamilyName
		, @NameSuffix = @NameSuffix
		, @PersonID = @CustomerID output
		, @Result = @Result output

	-- Check for success
	if (@@error <> 0) or (@@rowcount = 0)
		begin
			rollback tran
			raiserror('upAddCustomer - Customer was not successfully added', 16, 1)
			set @CustomerID = null
			return
		end

	-- Add record to tbPersonEmail
	exec dbo.upAddPersonEmail 
		@PersonID = @CustomerID
		, @EmailTypeID = @EmailTypeID
		, @EmailAddress = @EmailAddress
		, @CreateID = @AgentID
		, @IsPreferred = @EmailAddressIsPreferred
		, @Result = @Result output

	-- Check for success
	set @Result = 'F'
	if (@@error <> 0) or (@@rowcount = 0)
		begin
			rollback tran
			raiserror('upAddCustomer - Customer email was not successfully added', 16, 1)
			set @CustomerID = null
			return
		end

	-- Insert tbCustomer record
	insert into dbo.tbCustomer (CustomerID, AgentID, StatusID, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
	values (@CustomerID, @AgentID, dbo.sfGetAttributeValueID('Status','Active'), getdate(), @AgentID, getdate(), @AgentID)

	-- Check for success
	set @Result = 'F'
	if (@@error <> 0) or (@@rowcount = 0)
		begin
			rollback tran
			raiserror('upAddCustomer - Customer email was not successfully added', 16, 1)
			set @CustomerID = null
			return
		end

	-- Add Recent Match Settings, if necessary
	if @RecentMatchSettingID is not null
		exec dbo.upAddRecentMatchSettings 
			@RecentMatchSettingID = @RecentMatchSettingID
			, @CustomerID = @CustomerID
			, @CreateID = @AgentID
			, @Result = @Result output

	-- Check for success
	if (@@error = 0) and (@@rowcount > 0)
		begin
			set @Result = 'S'
			commit tran
		end
	else
		begin
			rollback tran
			raiserror('upAddCustomer - Customer was not successfully added', 16, 1)
		end
end
go
grant exec on dbo.upAddCustomer to web