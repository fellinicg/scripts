use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddPersonAddress') and type in (N'P', N'PC'))
drop procedure dbo.upAddPersonAddress
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160316
-- ============================================================
create procedure dbo.upAddPersonAddress
(
	@PersonID int
	, @AddressTypeID int
	, @CreateID int
	, @Line1 varchar(80)
	, @Line2 varchar(80) = null
	, @Line3 varchar(80) = null
	, @Line4 varchar(80) = null
	, @CityTown varchar(40) = null
	, @StateProvince varchar(40) = null
	, @PostalCode varchar(20) = null
	, @Country char(2) = null
	, @Latitude float = null
	, @Longitude float = null
	, @IsPreferred bit = 0
	, @PersonAddressID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Add record to tbPerson
	exec dbo.upAddPostalAddress
		@Line1 = @Line1
		, @CreateID = @CreateID
		, @Line2 = @Line2
		, @Line3 = @Line3
		, @Line4 = @Line4
		, @CityTown = @CityTown
		, @StateProvince = null
		, @PostalCode = null
		, @Country = null
		, @Latitude = null
		, @Longitude = null
		, @PostalAddressID = @PersonAddressID output
		, @Result = @Result output

	-- Check for success
	if (@@error <> 0) or (@@rowcount = 0) or ( @PersonAddressID = -1)
		begin
			raiserror('upAddPersonAddress - address was not successfully added', 16, 1)
			return
		end

	-- Insert record
	insert into dbo.tbPersonAddress(PersonID, PostalAddressID, AddressTypeID, IsPreferred, CreateID, UpdateID)
	values (@PersonID, @PersonAddressID, @AddressTypeID, @IsPreferred, @CreateID, @CreateID)

	-- Check for success
	if (@@error = 0) and (@@rowcount > 0)
		begin
			set @PersonAddressID = @@identity
			set @Result = 'S'
		end
	else
		begin
			raiserror('upAddPersonAddress - address was not successfully added', 16, 1)
			set @PersonAddressID = null
			set @Result = 'F'
		end

end
go
grant exec on dbo.upAddPersonAddress to web