use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddAgentStatus') and type in (N'P', N'PC'))
drop procedure dbo.upAddAgentStatus
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddAgentStatus
(
	@AgentID int
	, @StatusID int
	, @CreateDateTime datetime2
	, @CreateID int
	, @AgentStatusID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbAgentStatus
			(AgentID, StatusID, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@AgentID, @StatusID, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @AgentStatusID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddAgentStatus - AgentStatus was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddAgentStatus to web