use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddCustomerRecentMatchSettings') and type in (N'P', N'PC'))
drop procedure dbo.upAddCustomerRecentMatchSettings
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160317
-- ============================================================
create procedure dbo.upAddCustomerRecentMatchSettings
(
	@RecentMatchSettingID varchar(200)
	, @CustomerID int
	, @CreateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Delete RecentMatchSettings
	delete from dbo.tbPersonAttribute
	from dbo.vwAttributeValues av
		inner join dbo.tbPersonAttribute pa on av.AttributeValueID = pa.AttributeValueID
	where av.AttributeName = 'recent match settings'
		and pa.PersonID = @CustomerID

	-- Insert record, if the RecentMatchSettingsName does NOT already exist
	insert into dbo.tbPersonAttribute (PersonID, AttributeValueID, CreateID, UpdateID)
	select @CustomerID, ListItem, @CreateID, @CreateID 
	from dbo.tfCommaListToTable(@RecentMatchSettingID)

	-- Check for success
	if (@@error = 0) and (@@rowcount > 0)
		begin
			set @Result = 'S'
		end
	else
		raiserror('upAddCustomerRecentMatchSettings - Recent match settings were not successfully added', 16, 1)

end
go
grant exec on dbo.upAddCustomerRecentMatchSettings to web