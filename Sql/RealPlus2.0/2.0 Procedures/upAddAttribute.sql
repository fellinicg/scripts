use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddAttribute') and type in (N'P', N'PC'))
drop procedure dbo.upAddAttribute
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160324
-- ============================================================
create procedure dbo.upAddAttribute
(
	@AttributeName varchar(100)
	, @CreateID int
	, @AttributeCode varchar(10) = null
	, @AttributeID int = null output
	, @Result char(1) = 'F' output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Check for existence of AttributeName
	select @AttributeID = AttributeID from dbo.tbAttribute where AttributeName = @AttributeName

	-- If the AttributeName does NOT already exist
	if @@rowcount = 0
		begin
			-- Insert record, if the AttributeName does NOT already exist
			insert into dbo.tbAttribute (AttributeName, AttributeCode, CreateID, UpdateID)
			values (@AttributeName, @AttributeCode,  @CreateID, @CreateID)

			-- Check for success
			if (@@error = 0) and (@@rowcount > 0)
				begin
					set @AttributeID = @@identity
					set @Result = 'S'
			end
			else
				raiserror('upAddAttribute - Attribute was not successfully added', 16, 1)
		end
end
go
grant exec on dbo.upAddAttribute to web