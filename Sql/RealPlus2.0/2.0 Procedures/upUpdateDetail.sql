use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateDetail') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateDetail
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upUpdateDetail
(
	@DetailID int
	, @DetailText nvarchar(max) = null
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbDetail
			set DetailText = isnull(@DetailText, DetailText)
			, IsDeleted = isnull(@IsDeleted, IsDeleted)
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
		where DetailID = @DetailID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateDetail - Detail was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateDetail to web