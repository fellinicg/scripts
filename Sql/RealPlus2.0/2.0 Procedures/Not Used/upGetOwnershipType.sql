use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetOwnershipType') and type in (N'P', N'PC'))
drop procedure dbo.upGetOwnershipType
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160314
-- ============================================================
create procedure dbo.upGetOwnershipType
	@CompanyID int = null
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	--exec dbo.upGetAttributeValues 14
	exec dbo.upGetAttributeValues @AttributeName='OwnershipType', @CompanyID = @CompanyID

end
go
grant exec on dbo.upGetOwnershipType to web