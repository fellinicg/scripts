use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetPriceMax') and type in (N'P', N'PC'))
drop procedure dbo.upGetPriceMax
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160314
-- ============================================================
create procedure dbo.upGetPriceMax
	@CompanyID int = null
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	--exec dbo.upGetAttributeValues 14
	exec dbo.upGetAttributeValues @AttributeName='PriceMax', @CompanyID = @CompanyID

end
go
grant exec on dbo.upGetPriceMax to web