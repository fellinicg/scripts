use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetAttributeValues') and type in (N'P', N'PC'))
drop procedure dbo.upGetAttributeValues
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160221
-- ============================================================
create procedure dbo.upGetAttributeValues
	@AttributeID int = null
	, @AttributeName varchar(100) = null
	, @CompanyID int = null
	, @GlobalIsDeleted bit = 0
	, @GlobalDisplay bit = 1
	, @CommpanyIsDeleted bit = 0
	, @CompanyDisplay bit = 1
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select a.AttributeValueID, coalesce(b.CustomLabel, a.ValueName) 'Label', a.NumericValue--, a.SortOrder, b.SortOrder --IsDeleted, Display, 
	from dbo.vwAttributeValues a
		left join tbCompanySearchProperyPreference b on a.AttributeValueID = isnull(b.AttributeValueID, 0) and b.CompanyID = isnull(@CompanyID, 0)
	where (a.AttributeID = @AttributeID or a.AttributeName = @AttributeName)
		and a.IsDeleted = @GlobalIsDeleted
		and a.Display = @GlobalDisplay
		and isnull(b.IsDeleted, @CommpanyIsDeleted) = @CommpanyIsDeleted
		and isnull(b.Display, @CompanyDisplay) = @CompanyDisplay
	order by coalesce(b.SortOrder, a.SortOrder), a.ValueName

	--select AttributeValueID, ValueName 
	--	from dbo.tbAttributeValue a
	--	inner join dbo.tbValue b on a.ValueID = b.ValueID
	--where a.AttributeID = @AttributeID
	--	and a.IsDeleted = 0
	--	and a.Display = 1
	--order by b.ValueName

end
