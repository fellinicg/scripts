use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetOutdoorSpace') and type in (N'P', N'PC'))
drop procedure dbo.upGetOutdoorSpace
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160221
-- ============================================================
create procedure dbo.upGetOutdoorSpace
	@CompanyID int = null
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	--exec dbo.upGetAttributeValues 11
	exec dbo.upGetAttributeValues @AttributeName='OutdoorSpace', @CompanyID = @CompanyID

end
go
grant exec on dbo.upGetOutdoorSpace to web