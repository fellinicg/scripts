use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetRentMax') and type in (N'P', N'PC'))
drop procedure dbo.upGetRentMax
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160315
-- ============================================================
create procedure dbo.upGetRentMax
	@CompanyID int = null
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	--exec dbo.upGetAttributeValues 14
	exec dbo.upGetAttributeValues @AttributeName='RentMax', @CompanyID = @CompanyID

end
go
grant exec on dbo.upGetRentMax to web