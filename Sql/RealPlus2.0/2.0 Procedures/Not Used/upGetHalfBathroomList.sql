use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetHalfBathroomList') and type in (N'P', N'PC'))
drop procedure dbo.upGetHalfBathroomList
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160311
-- ============================================================
create procedure dbo.upGetHalfBathroomList
	@CompanyID int = null
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	--exec dbo.upGetAttributeValues 5
	exec dbo.upGetAttributeValues @AttributeName='HalfBathrooms', @CompanyID = @CompanyID

end
go
grant exec on dbo.upGetHalfBathroomList to web