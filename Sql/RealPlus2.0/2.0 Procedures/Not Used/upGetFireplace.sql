use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetFireplace') and type in (N'P', N'PC'))
drop procedure dbo.upGetFireplace
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160221
-- ============================================================
create procedure dbo.upGetFireplace
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	exec dbo.upGetAttributeValues 13

end
