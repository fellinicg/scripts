use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetPriceMin') and type in (N'P', N'PC'))
drop procedure dbo.upGetPriceMin
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160314
-- ============================================================
create procedure dbo.upGetPriceMin
	@CompanyID int = null
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	--exec dbo.upGetAttributeValues 14
	exec dbo.upGetAttributeValues @AttributeName='PriceMin', @CompanyID = @CompanyID

end
go
grant exec on dbo.upGetPriceMin to web