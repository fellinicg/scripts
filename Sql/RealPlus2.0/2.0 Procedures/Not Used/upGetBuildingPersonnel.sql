use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetBuildingPersonnel') and type in (N'P', N'PC'))
drop procedure dbo.upGetBuildingPersonnel
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160221
-- ============================================================
create procedure dbo.upGetBuildingPersonnel
	@CompanyID int = null
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	--exec dbo.upGetAttributeValues 3
	exec dbo.upGetAttributeValues @AttributeName='BuildingPersonnel', @CompanyID = @CompanyID

end
go
grant exec on dbo.upGetBuildingPersonnel to web