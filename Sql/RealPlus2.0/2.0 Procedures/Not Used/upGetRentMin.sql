use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetRentMin') and type in (N'P', N'PC'))
drop procedure dbo.upGetRentMin
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160315
-- ============================================================
create procedure dbo.upGetRentMin
	@CompanyID int = null
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	--exec dbo.upGetAttributeValues 14
	exec dbo.upGetAttributeValues @AttributeName='RentMin', @CompanyID = @CompanyID

end
go
grant exec on dbo.upGetRentMin to web