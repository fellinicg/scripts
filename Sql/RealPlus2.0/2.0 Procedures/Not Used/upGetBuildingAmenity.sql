use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetBuildingAmenity') and type in (N'P', N'PC'))
drop procedure dbo.upGetBuildingAmenity
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160221
-- ============================================================
create procedure dbo.upGetBuildingAmenity
	@CompanyID int = null
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	--exec dbo.upGetAttributeValues 6
	exec dbo.upGetAttributeValues @AttributeName='BuildingAmenity', @CompanyID = @CompanyID

end
go
grant exec on dbo.upGetBuildingAmenity to web