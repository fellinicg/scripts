use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetRentalStatus') and type in (N'P', N'PC'))
drop procedure dbo.upGetRentalStatus
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160221
-- ============================================================
create procedure dbo.upGetRentalStatus
	@CompanyID int = null
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	--exec dbo.upGetAttributeValues 9
	--select a.AttributeTypeID, b.AttributeName 
	--from dbo.tbAttributeType a
	--	inner join dbo.tbAttribute b on a.AttributeID = b.AttributeID
	--where a.TypeID = 9
	--	and a.IsDeleted = 0
	--order by b.AttributeName
	exec dbo.upGetAttributeValues @AttributeName='RentalStatus', @CompanyID = @CompanyID

end
go
grant exec on dbo.upGetRentalStatus to web