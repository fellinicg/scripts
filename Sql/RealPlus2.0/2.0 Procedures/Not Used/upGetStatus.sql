use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetStatus') and type in (N'P', N'PC'))
drop procedure dbo.upGetStatus
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160310
-- ============================================================
create procedure dbo.upGetStatus
	@CompanyID int = null
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	--exec dbo.upGetAttributeValues 5
	exec dbo.upGetAttributeValues @AttributeName='Status', @CompanyID = @CompanyID

end
go
grant exec on dbo.upGetStatus to web