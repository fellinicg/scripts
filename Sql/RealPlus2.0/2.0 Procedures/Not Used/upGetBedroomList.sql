use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetBedroomList') and type in (N'P', N'PC'))
drop procedure dbo.upGetBedroomList
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160311
-- ============================================================
create procedure dbo.upGetBedroomList
	@CompanyID int = null
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	--exec dbo.upGetAttributeValues 5
	exec dbo.upGetAttributeValues @AttributeName='Bedrooms', @CompanyID = @CompanyID

end
go
grant exec on dbo.upGetBedroomList to web