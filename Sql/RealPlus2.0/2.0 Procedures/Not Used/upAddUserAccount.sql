use Jaguar_v2_Prototype
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddUserAccount') and type in (N'P', N'PC'))
drop procedure dbo.upAddUserAccount
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20151130
-- ============================================================
create procedure dbo.upAddUserAccount
	@AccountType char(1)
	,@AccountName varchar(20)
	,@FirstGivenName varchar(40)
	,@FamilyName varchar(60)
	,@AccountStatus  char(1)
	,@NamePrefix varchar(20) = null
	,@SecondGivenName varchar(40) = null
	,@NameSuffix varchar(20) = null
	,@UserAccountID int = -1 output
as
begin

	-- Initialize settings
	set nocount on

	-- Nullify empty strings supplied
	set @NamePrefix = nullif(@NamePrefix, '')
	set @SecondGivenName = nullif(@SecondGivenName, '')
	set @NameSuffix = nullif(@NameSuffix, '')


	-- Insert new record into tbUserAccount
	insert into dbo.tbUserAccount (
		AccountType,
		AccountName,
		NamePrefix,
		FirstGivenName,
		SecondGivenName,
		FamilyName,
		NameSuffix,
		FullName,
		AccountStatus,
		CreateDateTime,
		UpdateDateTime)
	values (
		upper(@AccountType),
		@AccountName,
		@NamePrefix,
		@FirstGivenName,
		@SecondGivenName,
		@FamilyName,
		@NameSuffix,
		dbo.sfTrim(@FirstGivenName) + ' ' + dbo.sfTrim(@FamilyName),
		upper(@AccountStatus),
		current_timestamp,
		current_timestamp)

	set @UserAccountID = @@identity

	if (isnull(@UserAccountID, 0) = 0)
		raiserror('upAddUserAccount - user account was NOT successfully added', 16, 1)

end
