use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetMinSqFt') and type in (N'P', N'PC'))
drop procedure dbo.upGetMinSqFt
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160316
-- ============================================================
create procedure dbo.upGetMinSqFt
	@CompanyID int = null
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	--exec dbo.upGetAttributeValues 5
	exec dbo.upGetAttributeValues @AttributeName='MinSqFt', @CompanyID = @CompanyID

end
go
grant exec on dbo.upGetMinSqFt to web