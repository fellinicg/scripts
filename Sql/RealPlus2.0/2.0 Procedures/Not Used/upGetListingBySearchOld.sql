use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetListingBySearchOld') and type in (N'P', N'PC'))
drop procedure dbo.upGetListingBySearchOld
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160321
-- ============================================================
create procedure dbo.upGetListingBySearchOld
	@ListingCategory varchar(20)
	, @ListingStatus varchar(max) = '163,156'
	, @PriceMin int = 0
	, @PriceMax int = 100000
	, @Bedrooms float = 1
	, @Bathrooms float = 1
	, @OwnershipType varchar(max) = '26,27,28'
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select l.ListingID, l.ListingProvider, l.ListingCategory, l.ListingType, l.InitialPrice, l.CurrentPrice, l.ClosingPrice
		, l.CurrentStatus, l.BuyerCommission, l.SellerCommission, l.TotalCommission, l.ListingNumber, l.ListingNumberReference
		, l.ListDate, l.PropertyClass, l.Line1, l.UnitIdentifier, l.CityTown, l.StateProvince, l.PostalCode, l.AssociationFee, l.Bathrooms
		, l.Bedrooms, l.HalfBathrooms, l.Rooms, l.SquareFootage, l.Elevators, l.FinanceAllowedPercent, l.FlipTax, l.Floors, l.Shares
		, l.TaxDeductPct, l.Units, l.YearBuilt, l.YearIncorporated, l.ListingUpdated, l.ListingCreated
	from dbo.vwListing l
	inner join (
		select l.ListingID, l.PropertyID
		from dbo.tbListing l
			inner join
			(
				select ListingID from dbo.vwCurrentListingStatusID a inner join (select ListItem from dbo.tfCommaListToTable(@ListingStatus)) b on a.AttributeValueID = b.ListItem
				intersect
				select ListingID from dbo.vwCurrentListingPrice where ListingPrice between @PriceMin and @PriceMax
			) ls on l.ListingID = ls.ListingID
		where
			l.ListingCategoryID = dbo.sfGetAttributeValueID('ListingCategory',@ListingCategory)
	
		intersect

		select l.ListingID, l.PropertyID
		from dbo.tbListing l
			inner join
			(
				select PropertyID from dbo.tfPropertyAttribute('Bedrooms') where AttributeValue >= @Bedrooms
				intersect
				select PropertyID from dbo.tfPropertyAttribute('Bathrooms') where AttributeValue >= @Bathrooms
				intersect
				select PropertyID from dbo.tbProperty where ParentPropertyID in (select PropertyID from dbo.tbPropertyAttribute a inner join (select ListItem from dbo.tfCommaListToTable(@OwnershipType)) b on a.AttributeValueID = b.ListItem)
			) ps on l.PropertyID = ps.PropertyID
		where
			l.ListingCategoryID = dbo.sfGetAttributeValueID('ListingCategory',@ListingCategory)
	) f on l.ListingID = f.ListingID

end
go
grant exec on dbo.upGetListingBySearchOld to web