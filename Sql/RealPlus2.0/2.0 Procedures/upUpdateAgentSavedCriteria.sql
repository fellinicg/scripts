use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateAgentSavedCriteria') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateAgentSavedCriteria
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160316
-- ============================================================
create procedure dbo.upUpdateAgentSavedCriteria
(
	@AgentSavedCriteriaID int	
	, @AgentID int
	, @CustomerID int  = null
	, @Detail varchar(200) = null
	, @Criteria xml = null
	, @IsDeleted bit = null
	, @Result char(1) = 'F' output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Update record
	update dbo.tbAgentSavedCriteria
	set 
		Detail = isnull(@Detail, Detail)
		, Criteria = isnull(@Criteria, Criteria)
		, IsDeleted = isnull(@IsDeleted, IsDeleted)
		, UpdateID = @AgentID
		, UpdateDateTime = getdate()	
	where 
		AgentSavedCriteriaID = @AgentSavedCriteriaID
		and
		AgentID = @AgentID
		and
		CustomerID = isnull(@CustomerID, CustomerID)

	-- Check for success
	if (@@error = 0) and (@@rowcount > 0)
		begin
			set @Result = 'S'
		end
	else
		raiserror('upUpdateAgentSavedCriteria - Criteria was not successfully updated', 16, 1)
end
go
grant exec on dbo.upUpdateAgentSavedCriteria to web