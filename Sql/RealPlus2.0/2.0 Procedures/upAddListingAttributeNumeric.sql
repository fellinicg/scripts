use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddListingAttributeNumeric') and type in (N'P', N'PC'))
drop procedure dbo.upAddListingAttributeNumeric
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddListingAttributeNumeric
(
	@ListingID int
	, @AttributeID int
	, @AttributeValue float
	, @CreateDateTime datetime2
	, @CreateID int
	, @ListingAttributeNumericID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbListingAttributeNumeric
			(ListingID, AttributeID, AttributeValue, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@ListingID, @AttributeID, @AttributeValue, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @ListingAttributeNumericID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddListingAttributeNumeric - ListingAttributeNumeric was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddListingAttributeNumeric to web