use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateSharedCustomer') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateSharedCustomer
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160412
-- ============================================================
create procedure dbo.upUpdateSharedCustomer
(
	@CustomerID int
	, @AgentID int
	, @PermissionID char(1) = null
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbSharedCustomer
			set PermissionID = isnull(@PermissionID, PermissionID)
			, IsDeleted = isnull(@IsDeleted, IsDeleted)
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
		where CustomerID = @CustomerID 
				and AgentID = @AgentID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateSharedCustomer - SharedCustomer was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateSharedCustomer to web