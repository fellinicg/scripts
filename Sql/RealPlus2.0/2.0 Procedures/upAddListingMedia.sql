use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddListingMedia') and type in (N'P', N'PC'))
drop procedure dbo.upAddListingMedia
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddListingMedia
(
	@ListingID int
	, @MediaID int
	, @ListingMediaTypeID int
	, @Detail varchar(100) = null
	, @AdditionalDetail varchar(60) = null
	, @SortOrder tinyint
	, @CreateDateTime datetime2
	, @CreateID int
	, @ListingMediaID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbListingMedia
			(ListingID, MediaID, ListingMediaTypeID, Detail, AdditionalDetail, SortOrder, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
		values
			(@ListingID, @MediaID, @ListingMediaTypeID, @Detail, @AdditionalDetail, @SortOrder, @CreateDateTime, @CreateID, @CreateDateTime, @CreateID)

		set @ListingMediaID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddListingMedia - ListingMedia was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddListingMedia to web