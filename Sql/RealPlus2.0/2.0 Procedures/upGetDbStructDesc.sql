use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetDbStructDesc') and type in (N'P', N'PC'))
drop procedure dbo.upGetDbStructDesc
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- ============================================================
create procedure dbo.upGetDbStructDesc
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select TableName, ColumnName, Description
	from (
		select b.name TableName, '' ColumnName, a.value Description, a.minor_id
		from sys.extended_properties a
			inner join sys.tables b on a.major_id = b.object_id
		where a.name = 'Description' and a.minor_id = 0
		union 
		select b.Name, c.name, a.value, a.minor_id
		from sys.extended_properties a
			inner join sys.tables b on a.major_id = b.object_id
			inner join sys.columns c on a.major_id = c.object_id and a.minor_id = c.column_id
		where a.name = 'Description' and a.minor_id > 0) a
	order by TableName, minor_id
end
go
grant exec on dbo.upGetDbStructDesc to viewdefinition