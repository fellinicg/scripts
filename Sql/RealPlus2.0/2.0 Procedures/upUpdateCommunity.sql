use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateCommunity') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateCommunity
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160324
-- ============================================================
create procedure dbo.upUpdateCommunity
(
	@CommunityID int
	, @CommunityTypeID int = null
	, @CommunityName varchar(100) = null
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Only take action if at least one optional parameter is NOT null
	if @CommunityName is not null or @IsDeleted is not null
		begin
			-- Update record
			update dbo.tbCommunity set 
				CommunityTypeID = isnull(@CommunityTypeID, CommunityTypeID)
				, CommunityName = isnull(@CommunityName, CommunityName)
				, IsDeleted = isnull(@IsDeleted, IsDeleted)
				, UpdateID = @UpdateID
				, UpdateDateTime = getdate()
			where CommunityID = @CommunityID

			-- Check for success
			if (@@error = 0) and (@@rowcount > 0)
				begin
					set @Result = 'S'
				end
			else
				raiserror('upUpdateCommunity - Community was not successfully Updated', 16, 1)
		end
end
go
grant exec on dbo.upUpdateCommunity to web