use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateListingPrice') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateListingPrice
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160412
-- ============================================================
create procedure dbo.upUpdateListingPrice
(
	@ListingPriceID int
	, @IsDeleted bit = null
	, @PriceDateTime datetime2 = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbListingPrice
			set IsDeleted = isnull(@IsDeleted, IsDeleted)
			, PriceDateTime = isnull(@PriceDateTime, PriceDateTime)
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
			where ListingPriceID = @ListingPriceID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateListingPrice - ListingPrice was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateListingPrice to web