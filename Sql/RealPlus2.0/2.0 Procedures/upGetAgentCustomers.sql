use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetAgentCustomers') and type in (N'P', N'PC'))
drop procedure dbo.upGetAgentCustomers
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160315
-- ============================================================
create procedure dbo.upGetAgentCustomers
	@AgentID int
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		a.CustomerID
		, b.FullName
		, dbo.sfGetAttributeValue(a.StatusID) 'Status'
		, a.IsDeleted 'CustomerDeleted'
		, b.IsDeleted 'PersonDeleted'
	from dbo.tbCustomer a
		inner join dbo.tbPerson b on a.CustomerID = b.PersonID
	where AgentID = @AgentID

end
go
grant exec on dbo.upGetAgentCustomers to web