use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetCompanyOffices') and type in (N'P', N'PC'))
drop procedure dbo.upGetCompanyOffices
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160405
-- ============================================================
create procedure dbo.upGetCompanyOffices
	@CompanyID int
	, @ShowDeleted bit = 0
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		c.CompanyName
		, c.IsDeleted 'CompanyIsDeleted'
		, o.OfficeID
		, o.OfficeCode
		, o.Detail
		, o.PhoneNumber
		, o.Fax
		, o.Email
		, o.IsDeleted 'OfficeIsDeleted'
		, o.CreateDateTime
		, dbo.sfGetPersonFullName(o.CreateID) 'CreatedBy'
		, o.UpdateDateTime
		, dbo.sfGetPersonFullName(o.UpdateID) 'UpdatedBy'
		, a.Line1
		, a.Line2
		, a.Line3
		, a.Line4
		, a.CityTown
		, a.StateProvince
		, a.PostalCode
	from dbo.tbCompany c
		inner join dbo.tbOffice o on c.CompanyID = o.CompanyID
		left join dbo.tbPostalAddress a on o.PostalAddressID = a.PostalAddressID
	where c.CompanyID = @CompanyID
		and c.IsDeleted = @ShowDeleted
		and o.IsDeleted = @ShowDeleted

end
go
grant exec on dbo.upGetCompanyOffices to web