use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetListingChanges') and type in (N'P', N'PC'))
drop procedure dbo.upGetListingChanges
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160412
-- ============================================================
create procedure dbo.upGetListingChanges
	@ListingID int
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 'Status' 'ActivityType', dbo.sfGetAttributeValue(StatusID) 'Activity', StatusDateTime 'DateTime'
	from dbo.tbListingStatus
	where ListingID = @ListingID
	union
	select 'Price', convert(varchar, ListingPrice), PriceDateTime
	from dbo.tbListingPrice
	where ListingID = @ListingID
	order by DateTime

end
go
grant exec on dbo.upGetListingChanges to web