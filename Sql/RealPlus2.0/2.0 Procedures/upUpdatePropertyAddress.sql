use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdatePropertyAddress') and type in (N'P', N'PC'))
drop procedure dbo.upUpdatePropertyAddress
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160412
-- ============================================================
create procedure dbo.upUpdatePropertyAddress
(
	@PropertyAddressID int
	, @PostalAddressID int = null
	, @IsPrimary bit = null
	, @East int = null
	, @West int = null
	, @North int = null
	, @South int = null
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbPropertyAddress
			set PostalAddressID = isnull(@PostalAddressID, PostalAddressID)
			, IsPrimary = isnull(@IsPrimary, IsPrimary)
			, East = isnull(@East, East)
			, West = isnull(@West, West)
			, North = isnull(@North, North)
			, South = isnull(@South, South)
			, IsDeleted = isnull(@IsDeleted, IsDeleted)
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
		where PropertyAddressID = @PropertyAddressID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdatePropertyAddress - PropertyAddress was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdatePropertyAddress to web