use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateUserAccount') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateUserAccount
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160412
-- ============================================================
create procedure dbo.upUpdateUserAccount
(
	@UserAccountID int
	, @AccountName varchar(20) = null
	, @PasswordEncrypted varchar(200) = null
	, @UserAccountStatusID int = null
	, @IsDeleted bit = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbUserAccount
			set AccountName = isnull(@AccountName, AccountName)
			, PasswordEncrypted = isnull(@PasswordEncrypted, PasswordEncrypted)
			, UserAccountStatusID = isnull(@UserAccountStatusID, UserAccountStatusID)
			, IsDeleted = isnull(@IsDeleted, IsDeleted)
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
		where UserAccountID = @UserAccountID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateUserAccount - UserAccount was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateUserAccount to web