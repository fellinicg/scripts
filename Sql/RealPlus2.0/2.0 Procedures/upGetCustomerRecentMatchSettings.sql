use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetCustomerRecentMatchSettings') and type in (N'P', N'PC'))
drop procedure dbo.upGetCustomerRecentMatchSettings
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160324
-- ============================================================
create procedure dbo.upGetCustomerRecentMatchSettings
(
	@CustomerID int
)
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select av.AttributeValueID, av.ValueName
	from dbo.vwAttributeValues av
		inner join dbo.tbPersonAttribute pa on av.AttributeValueID = pa.AttributeValueID
	where av.AttributeName = 'recent match settings'
		and pa.PersonID = @CustomerID

end
go
grant exec on dbo.upGetCustomerRecentMatchSettings to web