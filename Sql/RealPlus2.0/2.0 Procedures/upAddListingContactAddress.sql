use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddListingContactAddress') and type in (N'P', N'PC'))
drop procedure dbo.upAddListingContactAddress
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddListingContactAddress
(
	@ListingID int
	, @ListingContactTypeID int
	, @PersonAddressID int
	, @ListingContactAddressID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbListingContactAddress
			(ListingID, ListingContactTypeID, PersonAddressID)
		values
			(@ListingID, @ListingContactTypeID, @PersonAddressID)

		set @ListingContactAddressID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddListingContactAddress - ListingContactAddress was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddListingContactAddress to web