use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateListingContactPhone') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateListingContactPhone
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upUpdateListingContactPhone
(
	@ListingContactPhoneID int
	, @ListingContactTypeID int
	, @PersonPhoneID int
	, @IsDeleted bit
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbListingContactPhone
			set ListingContactTypeID = isnull(@ListingContactTypeID, ListingContactTypeID)
			, PersonPhoneID = isnull(@PersonPhoneID, PersonPhoneID)
			, IsDeleted = isnull(@IsDeleted, IsDeleted)
		where ListingContactPhoneID = @ListingContactPhoneID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateListingContactPhone - ListingContactPhone was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateListingContactPhone to web