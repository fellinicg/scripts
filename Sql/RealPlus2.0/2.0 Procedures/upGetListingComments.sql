use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetListingComments') and type in (N'P', N'PC'))
drop procedure dbo.upGetListingComments
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160405
-- ============================================================
create procedure dbo.upGetListingComments
	@ListingID int
	, @CompanyID int = -1
	, @PersonID int = -1
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		lc.ListingCommentID
		, lc.CompanyID
		, lc.PersonID
		, d.DetailID
		, v.ValueName 'Type'
		, d.DetailText
		, d.UpdateDateTime
		, dbo.sfGetPersonFullName(d.UpdateID)
	from dbo.tbListingComment lc
		inner join dbo.tbDetail d on lc.DetailID = d.DetailID
		inner join dbo.tbAttributeValue av on d.DetailTypeID = av.AttributeValueID
		inner join dbo.tbValue v on av.ValueID = v.ValueID
	where lc.listingID = @ListingID
		and isnull(lc.CompanyID, -1) = @CompanyID
		and isnull(lc.PersonID, -1) = @PersonID
		and lc.IsDeleted = 0

end
go
grant exec on dbo.upGetListingComments to web