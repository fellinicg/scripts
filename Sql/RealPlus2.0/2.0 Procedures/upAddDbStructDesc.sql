use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddDbStructDesc') and type in (N'P', N'PC'))
drop procedure dbo.upAddDbStructDesc
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- ============================================================
create procedure dbo.upAddDbStructDesc
	@Description varchar(1000)
	,@Table varchar(100)
	,@Column varchar(100) = null
as
begin

	-- Initialize settings
	set nocount on

	-- Check optional parameter
	if @Column is null 
	begin
		-- Table description
		exec sp_addextendedproperty
			@name = N'Description', @value = @Description,
			@level0type = N'Schema', @level0name = 'dbo',
			@level1type = N'Table',  @level1name = @Table
	end
	else
	begin
		-- Column description
		exec sp_addextendedproperty
			@name = N'Description', @value = @Description,
			@level0type = N'Schema', @level0name = 'dbo',
			@level1type = N'Table',  @level1name = @Table,
			@level2type = N'Column',  @level2name = @Column
	end
end
