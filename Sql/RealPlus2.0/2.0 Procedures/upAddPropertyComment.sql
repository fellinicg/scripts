use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddPropertyComment') and type in (N'P', N'PC'))
drop procedure dbo.upAddPropertyComment
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160411
-- ============================================================
create procedure dbo.upAddPropertyComment
(
	@PropertyID int
	, @DetailID int
	, @CompanyID int
	, @PersonID int
	, @PropertyCommentID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		insert into dbo.tbPropertyComment
			(PropertyID, DetailID, CompanyID, PersonID)
		values
			(@PropertyID, @DetailID, @CompanyID, @PersonID)

		set @PropertyCommentID = @@identity
		set @Result = 'S'

	end try
	begin catch

		raiserror('upAddPropertyComment - PropertyComment was not successfully added',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upAddPropertyComment to web