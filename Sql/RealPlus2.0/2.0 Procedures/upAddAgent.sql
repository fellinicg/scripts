use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddAgent') and type in (N'P', N'PC'))
drop procedure dbo.upAddAgent
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160315
-- ============================================================
create procedure dbo.upAddAgent
(
	@AgentID int
	, @AgentNumber varchar(20)
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Insert record
	insert into dbo.tbAgent (AgentID, AgentNumber, CreateDateTime, CreateID, UpdateDateTime, UpdateID)
	values (@AgentID, @AgentNumber, getdate(), @AgentID, getdate(), @AgentID)

	-- Check for success
	if (@@error = 0) and (@@rowcount > 0)
		begin
			set @Result = 'S'
		end
	else
		raiserror('upAddAgent - Agent was not successfully added', 16, 1)
end
go
grant exec on dbo.upAddAgent to web