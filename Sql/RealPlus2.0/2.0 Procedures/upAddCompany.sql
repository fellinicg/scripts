use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddCompany') and type in (N'P', N'PC'))
drop procedure dbo.upAddCompany
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160406
-- ============================================================
create procedure dbo.upAddCompany
(
	@CompanyName varchar(50)
	, @CreateID int
	, @CompanyID int output
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Check for existence of Company by name
	if exists(select 1 from dbo.tbCompany where CompanyName = @CompanyName)
		begin
			set @Result = 'X'
			raiserror('upAddCompany - A company with this name already exists', 18, 1)
			return
		end

	-- Begin transaction
	begin tran

	-- Insert record into tbCompany
	insert into dbo.tbCompany (CompanyName, CreateID, UpdateID)
	values (@CompanyName, @CreateID, @CreateID)

	-- Check for success
	if (@@error <> 0) or (@@rowcount = 0)
		begin
			rollback tran
			raiserror('upAddCompany - Company was not successfully added', 16, 1)
			set @CompanyID = null
			return
		end
	else
		set @CompanyID = @@identity
		
	-- Copy master company records to new company in tbCustomCriteriaItem
	insert into dbo.tbCustomCriteriaItem (CustomCriteriaSectionID, AttributeValueID, CompanyID, ShortLabel, LongLabel, NumericValue, SortOrder, Display, IsDeleted, CreateID, UpdateID)
	select CustomCriteriaSectionID, AttributeValueID, @CompanyID, ShortLabel, LongLabel, NumericValue, SortOrder, Display, IsDeleted, @CreateID, @CreateID
	from dbo.tbCustomCriteriaItem
	where CompanyID = 0

		-- Check for success
	if (@@error = 0) and (@@rowcount > 0)
		begin
			set @Result = 'S'
			commit tran
		end
	else
		begin
			rollback tran
			raiserror('upAddCompany - Company was not successfully added', 16, 1)
		end

end
go
grant exec on dbo.upAddCompany to web