use Jaguar_v2_Prototype2_BHS_2
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetBuildingRentalCount') and type in (N'P', N'PC'))
drop procedure dbo.upGetBuildingRentalCount
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160810
-- ============================================================
create procedure dbo.upGetBuildingRentalCount
	@PropertyID int
	, @MinPrice money
	, @MaxPrice money
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select count (1)
	from dbo.tbProperty p
		inner join dbo.tbListing l on p.PropertyID = l.PropertyID
		inner join dbo.tbListingStatus ls on l.CurrentStatusID = ls.ListingStatusID
	where ls.StatusID in (220)
		and l.ListingCategoryID = 7
		and CurrentPrice between @MinPrice and @MaxPrice
		and p.ParentPropertyID = @PropertyID	

end
go
grant exec on dbo.upGetBuildingRentalCount to web