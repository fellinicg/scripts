use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateListingStatus') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateListingStatus
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160412
-- ============================================================
create procedure dbo.upUpdateListingStatus
(
	@ListingStatusID int
	, @IsDeleted bit = null
	, @StatusDateTime datetime2 = null
	, @UpdateID int
	, @Result char(1) output
)
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	
	begin try

		update dbo.tbListingStatus
			set IsDeleted = isnull(@IsDeleted, IsDeleted)
			, StatusDateTime = isnull(@StatusDateTime, StatusDateTime)
			, UpdateDateTime = getdate()
			, UpdateID = @UpdateID
			where ListingStatusID = @ListingStatusID

		set @Result = 'S'

	end try
	begin catch

		raiserror('upUpdateListingStatus - ListingStatus was not successfully updated',  16,  1) 
								
	end catch		
end

go
grant exec on dbo.upUpdateListingStatus to web