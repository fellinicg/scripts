use Jaguar_v2_WiP
use TMF
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.triggers where object_id = object_id(N'dbo.trgInsertPropertyUnit'))
drop trigger dbo.trgInsertPropertyUnit
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160511
-- ============================================================
create trigger dbo.trgInsertPropertyUnit
on dbo.properties_units
for insert
as 
begin
	-- Initialize settings
	set nocount on

	-- Declarations
	declare @AttributeID int

	-- Insert building (parent) into tbProperty, if they do not exist
	insert into dbo.tbProperty (PropertyClassID, EtlID)
	select 1, i.propertyid
	from Inserted i
		left join dbo.tbProperty p on i.propertyid = p.EtlID and p.PropertyClassID = 1
	where p.PropertyID is null

	-- Insert unit into tbProperty, if they do not exist
	insert into dbo.tbProperty (PropertyClassID, ParentPropertyID, EtlID)
	select 6, p.PropertyID, i.unitid
	from Inserted i
		inner join dbo.tbProperty p on i.propertyid = p.EtlID and p.PropertyClassID = 1
		left join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
	where u.PropertyID is null

	-- Update or Insert buildingtype => tbPropertyAttribute
	set @AttributeID = 19

	update dbo.tbPropertyAttribute set AttributeValueID = av.AttributeValueID, UpdateDateTime = getdate()
	from Inserted i
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		inner join vwAttributeValues av on i.buildingtype = av.ValueName and av.AttributeID = @AttributeID
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and av.AttributeValueID = pa.AttributeValueID
	where i.buildingtype is not null

	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, av.AttributeValueID
	from Inserted i
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		inner join vwAttributeValues av on i.buildingtype = av.ValueName and av.AttributeID = @AttributeID
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and av.AttributeValueID = pa.AttributeValueID
	where i.buildingtype is not null
		and pa.PropertyAttributeID is null

	-- Update or Insert unitid => tbPropertyAttributeText
	set @AttributeID = 100

	update dbo.tbPropertyAttributeText set AttributeValue = i.unitnumber_full, UpdateDateTime = getdate()
	from Inserted i
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		inner join dbo.tbPropertyAttributeText pat on u.PropertyID = pat.PropertyID and pat.AttributeID = @AttributeID
	where i.unitnumber_full is not null

	insert into dbo.tbPropertyAttributeText (PropertyID, AttributeID, AttributeValue)
	select u.PropertyID, @AttributeID, i.unitnumber_full
	from Inserted i
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		left join dbo.tbPropertyAttributeText pat on u.PropertyID = pat.PropertyID and pat.AttributeID = @AttributeID
	where i.unitnumber_full is not null
		and pat.PropertyAttributeTextID is null

	-- Update or Insert buildingname => tbPropertyAttributeText
	set @AttributeID = 14

	update dbo.tbPropertyAttributeText set AttributeValue = i.buildingname, UpdateDateTime = getdate()
	from Inserted i
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		inner join dbo.tbPropertyAttributeText pat on u.PropertyID = pat.PropertyID and pat.AttributeID = @AttributeID
	where i.buildingname is not null

	insert into dbo.tbPropertyAttributeText (PropertyID, AttributeID, AttributeValue)
	select u.PropertyID, @AttributeID, i.buildingname
	from Inserted i
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		left join dbo.tbPropertyAttributeText pat on u.PropertyID = pat.PropertyID and pat.AttributeID = @AttributeID
	where i.buildingname is not null
		and pat.PropertyAttributeTextID is null

	-- Update or Insert buildingname => tbPropertyAttributeText
	set @AttributeID = 14

	update dbo.tbPropertyAttributeText set AttributeValue = i.buildingname, UpdateDateTime = getdate()
	from Inserted i
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		inner join dbo.tbPropertyAttributeText pat on u.PropertyID = pat.PropertyID and pat.AttributeID = @AttributeID
	where i.buildingname is not null

	insert into dbo.tbPropertyAttributeText (PropertyID, AttributeID, AttributeValue)
	select u.PropertyID, @AttributeID, i.buildingname
	from Inserted i
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		left join dbo.tbPropertyAttributeText pat on u.PropertyID = pat.PropertyID and pat.AttributeID = @AttributeID
	where i.buildingname is not null
		and pat.PropertyAttributeTextID is null

	-- Update or Insert => tbPropertyAttributeNumeric

	update dbo.tbPropertyAttributeNumeric set AttributeValue = i.AttributeValue, UpdateDateTime = getdate()
	from (
		select 
			up.EtlID
			, case up.AttributeID
				when 'bedrooms' then 10
				when 'bathrooms' then 11
				when 'halfbaths' then 41
				when 'totalrooms_unit' then 85
				end 'AttributeID'
			, up.AttributeValue
		from (
			select 
				convert(int, unitid) EtlID
				, convert(int, bedrooms) bedrooms
				, convert(int, bathrooms) bathrooms
				, convert(int, halfbaths) halfbaths
				, convert(int, totalrooms_unit) totalrooms_unit
			from Inserted) i
			unpivot (AttributeValue for AttributeID in (bedrooms, bathrooms, halfbaths, totalrooms_unit)
		) up
	) i
	inner join dbo.tbProperty u on i.EtlID = u.EtlID and u.PropertyClassID = 6
	inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and i.AttributeID = pan.AttributeID

	insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
	select u.PropertyID, i.AttributeID, i.AttributeValue
	from (
		select 
			up.EtlID
			, case up.AttributeID
				when 'bedrooms' then 10
				when 'bathrooms' then 11
				when 'halfbaths' then 41
				when 'totalrooms_unit' then 85
				end 'AttributeID'
			, up.AttributeValue
		from (
			select 
				convert(int, unitid) EtlID
				, convert(int, bedrooms) bedrooms
				, convert(int, bathrooms) bathrooms
				, convert(int, halfbaths) halfbaths
				, convert(int, totalrooms_unit) totalrooms_unit
			from Inserted) i
			unpivot (AttributeValue for AttributeID in (bedrooms, bathrooms, halfbaths, totalrooms_unit)
		) up
	) i
	inner join dbo.tbProperty u on i.EtlID = u.EtlID and u.PropertyClassID = 6
	left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and i.AttributeID = pan.AttributeID
	where pan.PropertyAttributeNumericID is null

end
go