use Jaguar_v2_Prototype2_BHS_2
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.triggers where object_id = object_id(N'dbo.trgInsertUnit'))
drop trigger dbo.trgInsertUnit
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160511
-- ============================================================
create trigger dbo.trgInsertUnit
on dbo.units
for insert
as 
begin
	-- Initialize settings
	set nocount on

	-- Declarations
	declare @AttributeID int

	-- Insert building (parent) into tbProperty, if they do not exist
	insert into dbo.tbProperty (PropertyClassID, EtlID)
	select distinct 1, i.propertyid
	from Inserted i
		left join dbo.tbProperty p on i.propertyid = p.EtlID and p.PropertyClassID = 1
	where p.PropertyID is null

	-- Insert unit into tbProperty, if they do not exist
	insert into dbo.tbProperty (PropertyClassID, ParentPropertyID, EtlID)
	select 6, p.PropertyID, i.unitid
	from Inserted i
		inner join dbo.tbProperty p on i.propertyid = p.EtlID and p.PropertyClassID = 1
		left join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
	where u.PropertyID is null

	-- Insert advaptmiscr => tbPropertyAttribute (1-n)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, l.ListItem
	from Inserted i 
		cross apply dbo.tfpipelisttotable(i.advaptmiscr) l
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
	where i.advaptmiscr is not null
		and pa.PropertyAttributeID is null

	-- Insert advaptmiscs => tbPropertyAttribute (1-n)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, l.ListItem
	from Inserted i 
		cross apply dbo.tfpipelisttotable(i.advaptmiscs) l
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
	where i.advaptmiscs is not null
		and pa.PropertyAttributeID is null

	-- Insert viewattributes => tbPropertyAttribute (1-n)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, l.ListItem
	from Inserted i 
		cross apply dbo.tfpipelisttotable(i.viewattributes) l
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
	where i.viewattributes is not null
		and pa.PropertyAttributeID is null

	-- Insert outdoorspace => tbPropertyAttribute (1-n)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, l.ListItem
	from Inserted i 
		cross apply dbo.tfpipelisttotable(i.outdoorspace) l
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
	where i.outdoorspace is not null
		and pa.PropertyAttributeID is null

	-- Insert homestyle => tbPropertyAttribute (1-n)
	--insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	--select u.PropertyID, l.ListItem
	--from Inserted i 
	--	cross apply dbo.tfpipelisttotable(i.homestyle) l
	--	inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
	--	left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
	--where i.homestyle is not null
	--	and pa.PropertyAttributeID is null

	-- Insert townhousefeatures => tbPropertyAttribute (1-n)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, l.ListItem
	from Inserted i 
		cross apply dbo.tfpipelisttotable(i.townhousefeatures) l
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
	where i.townhousefeatures is not null
		and pa.PropertyAttributeID is null

	-- Insert exposureattribute => tbPropertyAttribute (1-n)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, l.ListItem
	from Inserted i 
		cross apply dbo.tfpipelisttotable(i.exposureattribute) l
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
	where i.exposureattribute is not null
		and pa.PropertyAttributeID is null

	-- Insert ispenthouse => tbPropertyAttribute (1-n)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, l.ListItem
	from Inserted i 
		cross apply dbo.tfpipelisttotable(i.ispenthouse) l
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
	where i.ispenthouse = 298
		and pa.PropertyAttributeID is null
				
	-- Insert apttype => tbPropertyAttribute (1-n)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, l.ListItem
	from Inserted i 
		cross apply dbo.tfpipelisttotable(i.apttype) l
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
	where i.apttype is not null
		and pa.PropertyAttributeID is null

	-- Insert multifloor => tbPropertyAttribute (1-n)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, l.ListItem
	from Inserted i 
		cross apply dbo.tfpipelisttotable(i.multifloor) l
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
	where i.multifloor is not null
		and pa.PropertyAttributeID is null

	-- Insert miscessentials => tbPropertyAttribute (1-n)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, l.ListItem
	from Inserted i 
		cross apply dbo.tfpipelisttotable(i.miscessentials) l
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
	where i.miscessentials is not null
		and pa.PropertyAttributeID is null 
				
	-- Insert unitid => tbPropertyAttributeText
	set @AttributeID = 100
	
	insert into dbo.tbPropertyAttributeText (PropertyID, AttributeID, AttributeValue)
	select u.PropertyID, @AttributeID, i.unitnumber_full
	from Inserted i
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		left join dbo.tbPropertyAttributeText pat on u.PropertyID = pat.PropertyID and pat.AttributeID = @AttributeID
	where i.unitnumber_full is not null
		and pat.PropertyAttributeTextID is null

	-- Insert postalcode => tbPropertyAttributeText
	set @AttributeID = 1259

	insert into dbo.tbPropertyAttributeText (PropertyID, AttributeID, AttributeValue)
	select u.PropertyID, @AttributeID, a.postalcode
	from Inserted i
		inner join properties p on i.propertyid = p.propertyid
		inner join addresses a on p.addressid = a.addressid
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		left join dbo.tbPropertyAttributeText pat on  u.PropertyID = pat.PropertyID and pat.AttributeID = @AttributeID
	where a.postalcode is not null
		and pat.PropertyAttributeTextID is null

	-- Insert => tbPropertyAttributeNumeric
	insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
	select u.PropertyID, i.AttributeID, i.AttributeValue
	from (
		select 
			up.EtlID
			, case up.AttributeID
				when 'bedrooms' then 11
				when 'bathrooms' then 10
				when 'halfbaths' then 41
				when 'totalrooms' then 85
				when 'totalsqft' then 91
			  end 'AttributeID'
			, up.AttributeValue
		from (
			select 
				convert(int, unitid) EtlID
				, convert(float, bedrooms) bedrooms
				, convert(float, bathrooms) bathrooms
				, convert(float, halfbaths) halfbaths
				, convert(float, totalrooms) totalrooms
				, convert(float, totalsqft) totalsqft
			from Inserted) i
			unpivot (AttributeValue for AttributeID in (bedrooms, bathrooms, halfbaths, totalrooms, totalsqft)
		) up
	) i
	inner join dbo.tbProperty u on i.EtlID = u.EtlID and u.PropertyClassID = 6
	left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and i.AttributeID = pan.AttributeID
	where pan.PropertyAttributeNumericID is null

	-- Insert floornum => tbPropertyAttributeNumeric
	set @AttributeID = 111

	insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
	select u.PropertyID, @AttributeID, i.floornum
	from Inserted i
		inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
		left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
	where i.floornum is not null
		and pan.PropertyAttributeNumericID is null
		
	---- Insert bedrooms => tbPropertyAttributeNumeric
	--set @AttributeID = 11

	--insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
	--select u.PropertyID, @AttributeID, i.bedrooms
	--from Inserted i
	--	inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
	--	left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
	--where i.bedrooms is not null
	--	and pan.PropertyAttributeNumericID is null

	---- Insert bathrooms => tbPropertyAttributeNumeric
	--set @AttributeID = 10

	--insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
	--select u.PropertyID, @AttributeID, i.bathrooms
	--from Inserted i
	--	inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
	--	left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
	--where i.bathrooms is not null
	--	and pan.PropertyAttributeNumericID is null

	---- Insert halfbaths => tbPropertyAttributeNumeric
	--set @AttributeID = 41

	--insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
	--select u.PropertyID, @AttributeID, i.halfbaths
	--from Inserted i
	--	inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
	--	left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
	--where i.halfbaths is not null
	--	and pan.PropertyAttributeNumericID is null

	---- Insert totalrooms => tbPropertyAttributeNumeric
	--set @AttributeID = 85

	--insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
	--select u.PropertyID, @AttributeID, i.totalrooms
	--from Inserted i
	--	inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
	--	left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
	--where i.totalrooms is not null
	--	and pan.PropertyAttributeNumericID is null

	---- Insert totalsqft => tbPropertyAttributeNumeric
	--set @AttributeID = 91

	--insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
	--select u.PropertyID, @AttributeID, i.totalsqft
	--from Inserted i
	--	inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
	--	left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
	--where i.totalsqft is not null
	--	and pan.PropertyAttributeNumericID is null

end
go