use Jaguar_v2_Prototype2_BHS_2
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.triggers where object_id = object_id(N'dbo.trgInsertProperty'))
drop trigger dbo.trgInsertProperty
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160511
-- ============================================================
create trigger dbo.trgInsertProperty
on dbo.properties
for insert
as 
begin
	-- Initialize settings
	set nocount on

	-- Declarations
	declare @AttributeID int

	-- Insert building into tbProperty, if they do not exist
	insert into dbo.tbProperty (PropertyClassID, EtlID)
	select 1, i.propertyid
	from Inserted i
		left join dbo.tbProperty p on i.propertyid = p.EtlID and p.PropertyClassID = 1
	where p.PropertyID is null

	-- Insert buildingtype => tbPropertyAttribute (1-1)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, i.buildingtype
	from Inserted i
		inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
		--inner join vwAttributeValues av on i.buildingtype = av.ValueName and av.AttributeID = @AttributeID
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and i.buildingtype = pa.AttributeValueID
	where i.buildingtype is not null
		and pa.PropertyAttributeID is null

	-- Insert ownershiptype => tbPropertyAttribute (1-1)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, i.ownershiptype
	from Inserted i
		inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and i.ownershiptype = pa.AttributeValueID
	where i.ownershiptype is not null
		and pa.PropertyAttributeID is null

	-- Insert postwar => tbPropertyAttribute (1-1)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, i.postwar
	from Inserted i
		inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and i.postwar = pa.AttributeValueID
	where i.postwar is not null
		and pa.PropertyAttributeID is null

	-- Insert neighborhoodattribute => tbPropertyAttribute (1-1)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, a.neighborhoodattribute
	from Inserted i
		inner join dbo.addresses a on i.addressid = a.addressid
		inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and a.neighborhoodattribute = pa.AttributeValueID
	where a.neighborhoodattribute is not null
		and pa.PropertyAttributeID is null

	-- Insert terrahld => tbPropertyAttribute (1-1)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, i.terrahld
	from Inserted i
		inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and i.terrahld = pa.AttributeValueID
	where i.terrahld is not null
		and pa.PropertyAttributeID is null

	-- Insert buildingallowed => tbPropertyAttribute (1-n)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, l.ListItem
	from Inserted i 
		cross apply dbo.tfpipelisttotable(i.buildingallowed) l
		inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
	where i.buildingallowed is not null
		and pa.PropertyAttributeID is null

	-- Insert buildingnotallowed => tbPropertyAttribute (1-n)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, l.ListItem
	from Inserted i 
		cross apply dbo.tfpipelisttotable(i.buildingnotallowed) l
		inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
	where i.buildingnotallowed is not null
		and pa.PropertyAttributeID is null

	-- Insert buildingamenities => tbPropertyAttribute (1-n)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, l.ListItem
	from Inserted i 
		cross apply dbo.tfpipelisttotable(i.buildingamenities) l
		inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
	where i.buildingamenities is not null
		and pa.PropertyAttributeID is null

	-- Insert buildingpersonnel => tbPropertyAttribute (1-n)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, l.ListItem
	from Inserted i 
		cross apply dbo.tfpipelisttotable(i.buildingpersonnel) l
		inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
	where i.buildingpersonnel is not null
		and pa.PropertyAttributeID is null

	-- Insert facade => tbPropertyAttribute (1-n)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, l.ListItem
	from Inserted i 
		cross apply dbo.tfpipelisttotable(i.facade) l
		inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
	where i.facade is not null
		and pa.PropertyAttributeID is null
		
	-- Insert housestyle => tbPropertyAttribute (1-n)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, l.ListItem
	from Inserted i 
		cross apply dbo.tfpipelisttotable(i.housestyle) l
		inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
	where i.housestyle is not null
		and pa.PropertyAttributeID is null

	-- Insert housetype => tbPropertyAttribute (1-n)
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, l.ListItem
	from Inserted i 
		cross apply dbo.tfpipelisttotable(i.housetype) l
		inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
	where i.housetype is not null
		and pa.PropertyAttributeID is null

	-- Insert townhousefeatures => tbPropertyAttribute
	insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
	select u.PropertyID, l.ListItem
	from Inserted i 
		cross apply dbo.tfpipelisttotable(i.townhousefeatures) l
		inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
		left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
	where i.townhousefeatures is not null
		and pa.PropertyAttributeID is null

	-- Insert buildingname => tbPropertyAttributeText
	set @AttributeID = 14

	insert into dbo.tbPropertyAttributeText (PropertyID, AttributeID, AttributeValue)
	select u.PropertyID, @AttributeID, i.buildingname
	from Inserted i
		inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
		left join dbo.tbPropertyAttributeText pat on u.PropertyID = pat.PropertyID and pat.AttributeID = @AttributeID
	where i.buildingname is not null
		and pat.PropertyAttributeTextID is null

	-- Insert managed_by => tbPropertyAttributeText
	set @AttributeID = 1184

	insert into dbo.tbPropertyAttributeText (PropertyID, AttributeID, AttributeValue)
	select u.PropertyID, @AttributeID, i.managed_by
	from Inserted i
		inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
		left join dbo.tbPropertyAttributeText pat on u.PropertyID = pat.PropertyID and pat.AttributeID = @AttributeID
	where i.managed_by is not null
		and pat.PropertyAttributeTextID is null

	-- Insert yearbuilt => tbPropertyAttributeNumeric
	set @AttributeID = 106

	insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
	select u.PropertyID, @AttributeID, i.yearbuilt
	from Inserted i
		inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
		left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
	where i.yearbuilt is not null
		and pan.PropertyAttributeNumericID is null

	-- Insert financingallowedpercent => tbPropertyAttributeNumeric
	set @AttributeID = 36

	insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
	select u.PropertyID, @AttributeID, i.financingallowedpercent
	from Inserted i
		inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
		left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
	where i.financingallowedpercent is not null
		and pan.PropertyAttributeNumericID is null		

	-- Insert totalunits => tbPropertyAttributeNumeric
	set @AttributeID = 102

	insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
	select u.PropertyID, @AttributeID, i.totalunits
	from Inserted i
		inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
		left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
	where i.totalunits is not null
		and pan.PropertyAttributeNumericID is null

	-- Insert frontfootage => tbPropertyAttributeNumeric
	set @AttributeID = 1128

	insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
	select u.PropertyID, @AttributeID, i.frontfootage
	from Inserted i
		inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
		left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
	where i.frontfootage is not null
		and pan.PropertyAttributeNumericID is null

	-- Insert grid_north => tbPropertyAttributeNumeric
	set @AttributeID = 1211

	insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
	select distinct p.PropertyID, @AttributeID, a.grid_north
	from Inserted i
		inner join dbo.addresses a on i.addressid = a.addressid
		inner join dbo.tbProperty p on i.propertyid = p.EtlID and p.PropertyClassID = 1
		left join dbo.tbPropertyAttributeNumeric pan on p.propertyid = pan.PropertyID and pan.AttributeID = @AttributeID
	where a.grid_north is not null
		and pan.PropertyAttributeNumericID is null

	-- Insert grid_south => tbPropertyAttributeNumeric
	set @AttributeID = 1212

	insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
	select distinct p.PropertyID, @AttributeID, a.grid_south
	from Inserted i
		inner join dbo.addresses a on i.addressid = a.addressid
		inner join dbo.tbProperty p on i.propertyid = p.EtlID and p.PropertyClassID = 1
		left join dbo.tbPropertyAttributeNumeric pan on p.propertyid = pan.PropertyID and pan.AttributeID = @AttributeID
	where a.grid_south is not null
		and pan.PropertyAttributeNumericID is null
	
	-- Insert grid_east => tbPropertyAttributeNumeric
	set @AttributeID = 1213

	insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
	select distinct p.PropertyID, @AttributeID, a.grid_east
	from Inserted i
		inner join dbo.addresses a on i.addressid = a.addressid
		inner join dbo.tbProperty p on i.propertyid = p.EtlID and p.PropertyClassID = 1
		left join dbo.tbPropertyAttributeNumeric pan on p.propertyid = pan.PropertyID and pan.AttributeID = @AttributeID
	where a.grid_east is not null
		and pan.PropertyAttributeNumericID is null

	-- Insert grid_west => tbPropertyAttributeNumeric
	set @AttributeID = 1214

	insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
	select distinct p.PropertyID, @AttributeID, a.grid_west
	from Inserted i
		inner join dbo.addresses a on i.addressid = a.addressid
		inner join dbo.tbProperty p on i.propertyid = p.EtlID and p.PropertyClassID = 1
		left join dbo.tbPropertyAttributeNumeric pan on p.propertyid = pan.PropertyID and pan.AttributeID = @AttributeID
	where a.grid_west is not null
		and pan.PropertyAttributeNumericID is null

	-- Insert rp_bin => tbPropertyAttributeNumeric
	set @AttributeID = 1260

	insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
	select u.PropertyID, @AttributeID, i.rp_bin
	from Inserted i
		inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
		left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
	where i.rp_bin is not null
		and pan.PropertyAttributeNumericID is null

end
go