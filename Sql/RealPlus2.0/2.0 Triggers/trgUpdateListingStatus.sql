use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.triggers where object_id = object_id(N'dbo.trgUpdateListingStatus'))
drop trigger dbo.trgUpdateListingStatus
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160405
-- ============================================================
create trigger dbo.trgUpdateListingStatus
on dbo.tbListingStatus
for insert
as 
begin
	-- Initialize settings
	set nocount on

	-- Handle multiple record inserts
	update dbo.tbListing set CurrentStatusID = i.ListingStatusID, CurrentStatus = v.ValueName
	from Inserted i
		inner join dbo.tbListing l on i.ListingID = l.ListingID
		inner join dbo.tbAttributeValue av on i.StatusID = av.AttributeValueID
		inner join dbo.tbValue v on av.ValueID = v.ValueID
end
go