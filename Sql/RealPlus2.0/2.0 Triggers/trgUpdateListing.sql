use Jaguar_v2_Prototype2_BHS_2
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.triggers where object_id = object_id(N'dbo.trgUpdateListing'))
drop trigger dbo.trgUpdateListing
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160511
-- ============================================================
create trigger dbo.trgUpdateListing
on dbo.listings
for update
as 
begin
	-- Initialize settings
	set nocount on

	-- Declarations
	declare @AttributeID int

	-- Update unitid (PropertyID)
	if update(unitid)
	begin
		-- Insert building (parent) into tbProperty, if they do not exist
		insert into dbo.tbProperty (PropertyClassID, EtlID)
		select distinct 1, bbp.propertyid
		from Inserted i
			inner join dbo.units bbu on i.unitid = bbu.unitid
			inner join dbo.properties bbp on bbu.propertyid = bbp.propertyid
			left join dbo.tbProperty p on bbp.propertyid = p.EtlID and p.PropertyClassID = 1
		where p.PropertyID is null

		-- Insert unit into tbProperty, if they do not exist
		insert into dbo.tbProperty (PropertyClassID, EtlID)
		select distinct 1, i.unitid
		from Inserted i
			left join dbo.tbProperty p on i.unitid = p.EtlID and p.PropertyClassID = 6
		where p.PropertyID is null

		-- Update
		update dbo.tbListing set PropertyID = p.PropertyID, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbProperty p on i.unitid = p.EtlID and p.PropertyClassID = 6
	end

	-- Update leasetype (1-1)
	if update(leasetype)
	begin
		set @AttributeID = 52

		-- Update
		update dbo.tbListingAttribute set AttributeValueID = i.leasetype, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbListingAttribute la on i.listingid = la.ListingID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and la.AttributeValueID = av.AttributeValueID
		where i.leasetype is not null

		-- Insert
		insert into dbo.tbListingAttribute (ListingID, AttributeValueID)
		select l.ListingID, i.leasetype
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			left join dbo.tbListingAttribute la on l.ListingID = la.ListingID and i.leasetype = la.AttributeValueID
		where i.leasetype is not null
			and la.ListingAttributeID is null

		-- Delete
		delete from dbo.tbListingAttribute
		from Inserted i
			inner join dbo.tbListingAttribute la on i.listingid = la.ListingID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and la.AttributeValueID = av.AttributeValueID
		where i.leasetype is null
	end

	-- Update feepaid (1-1)
	if update(feepaid)
	begin
		set @AttributeID = 116

		-- Update
		update dbo.tbListingAttribute set AttributeValueID = i.feepaid, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbListingAttribute la on i.listingid = la.ListingID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and la.AttributeValueID = av.AttributeValueID
		where i.feepaid is not null

		-- Insert
		insert into dbo.tbListingAttribute (ListingID, AttributeValueID)
		select l.ListingID, i.feepaid
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			left join dbo.tbListingAttribute la on l.ListingID = la.ListingID and i.feepaid = la.AttributeValueID
		where i.feepaid is not null
			and la.ListingAttributeID is null

		-- Delete
		delete from dbo.tbListingAttribute
		from Inserted i
			inner join dbo.tbListingAttribute la on i.listingid = la.ListingID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and la.AttributeValueID = av.AttributeValueID
		where i.feepaid is null
	end

	-- Update webid
	if update(webid)
	begin
		set @AttributeID = 54

		-- Update
		update dbo.tbListingAttributeText set AttributeValue = i.webid, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			inner join dbo.tbListingAttributeText lat on l.ListingID = lat.ListingID and lat.AttributeID = @AttributeID
		where i.webid is not null

		-- Insert
		insert into dbo.tbListingAttributeText (ListingID, AttributeID, AttributeValue)
		select l.ListingID, @AttributeID, i.webid
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			left join dbo.tbListingAttributeText lat on l.ListingID = lat.ListingID and lat.AttributeID = @AttributeID
		where i.webid is not null
			and lat.ListingAttributeTextID is null

		-- Delete
		delete from dbo.tbListingAttributeText
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			inner join dbo.tbListingAttributeText lat on l.ListingID = lat.ListingID and lat.AttributeID = @AttributeID
		where i.webid is null
	end

	-- Update listingnumber
	if update(listingnumber)
	begin
		set @AttributeID = 53

		-- Update
		update dbo.tbListingAttributeText set AttributeValue = i.listingnumber, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			inner join dbo.tbListingAttributeText lat on l.ListingID = lat.ListingID and lat.AttributeID = @AttributeID
		where i.listingnumber is not null

		-- Insert
		insert into dbo.tbListingAttributeText (ListingID, AttributeID, AttributeValue)
		select l.ListingID, @AttributeID, i.listingnumber
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			left join dbo.tbListingAttributeText lat on l.ListingID = lat.ListingID and lat.AttributeID = @AttributeID
		where i.listingnumber is not null
			and lat.ListingAttributeTextID is null

		-- Delete
		delete from dbo.tbListingAttributeText
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			inner join dbo.tbListingAttributeText lat on l.ListingID = lat.ListingID and lat.AttributeID = @AttributeID
		where i.listingnumber is null
	end

	-- Update pricepersqft
	if update(pricepersqft)
	begin
		set @AttributeID = 53

		-- Update
		update dbo.tbListingAttributeNumeric set AttributeValue = i.pricepersqft, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			inner join dbo.tbListingAttributeNumeric lan on l.ListingID = lan.ListingID and lan.AttributeID = @AttributeID
		where i.pricepersqft is not null

		-- Insert
		insert into dbo.tbListingAttributeNumeric (ListingID, AttributeID, AttributeValue)
		select l.ListingID, @AttributeID, i.pricepersqft
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			left join dbo.tbListingAttributeNumeric lan on l.ListingID = lan.ListingID and lan.AttributeID = @AttributeID
		where i.pricepersqft is not null
			and lan.ListingAttributeNumericID is null

		-- Delete
		delete from dbo.tbListingAttributeNumeric
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			inner join dbo.tbListingAttributeNumeric lan on l.ListingID = lan.ListingID and lan.AttributeID = @AttributeID
		where i.pricepersqft is null
	end

	-- Update leasetermmin
	if update(leasetermmin)
	begin
		set @AttributeID = 1251

		-- Update
		update dbo.tbListingAttributeNumeric set AttributeValue = i.leasetermmin, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			inner join dbo.tbListingAttributeNumeric lan on l.ListingID = lan.ListingID and lan.AttributeID = @AttributeID
		where i.leasetermmin is not null

		-- Insert
		insert into dbo.tbListingAttributeNumeric (ListingID, AttributeID, AttributeValue)
		select l.ListingID, @AttributeID, i.leasetermmin
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			left join dbo.tbListingAttributeNumeric lan on l.ListingID = lan.ListingID and lan.AttributeID = @AttributeID
		where i.leasetermmin is not null
			and lan.ListingAttributeNumericID is null

		-- Delete
		delete from dbo.tbListingAttributeNumeric
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			inner join dbo.tbListingAttributeNumeric lan on l.ListingID = lan.ListingID and lan.AttributeID = @AttributeID
		where i.leasetermmin is null
	end

	-- Update leasetermmax
	if update(leasetermmax)
	begin
		set @AttributeID = 1252

		-- Update
		update dbo.tbListingAttributeNumeric set AttributeValue = i.leasetermmax, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			inner join dbo.tbListingAttributeNumeric lan on l.ListingID = lan.ListingID and lan.AttributeID = @AttributeID
		where i.leasetermmax is not null

		-- Insert
		insert into dbo.tbListingAttributeNumeric (ListingID, AttributeID, AttributeValue)
		select l.ListingID, @AttributeID, i.leasetermmax
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			left join dbo.tbListingAttributeNumeric lan on l.ListingID = lan.ListingID and lan.AttributeID = @AttributeID
		where i.leasetermmax is not null
			and lan.ListingAttributeNumericID is null

		-- Delete
		delete from dbo.tbListingAttributeNumeric
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			inner join dbo.tbListingAttributeNumeric lan on l.ListingID = lan.ListingID and lan.AttributeID = @AttributeID
		where i.leasetermmax is null
	end

	-- Update totalmonthlyfees
	if update(totalmonthlyfees)
	begin
		set @AttributeID = 76

		-- Update
		update dbo.tbListingAttributeNumeric set AttributeValue = i.totalmonthlyfees, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			inner join dbo.tbListingAttributeNumeric lan on l.ListingID = lan.ListingID and lan.AttributeID = @AttributeID
		where i.totalmonthlyfees is not null

		-- Insert
		insert into dbo.tbListingAttributeNumeric (ListingID, AttributeID, AttributeValue)
		select l.ListingID, @AttributeID, i.totalmonthlyfees
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			left join dbo.tbListingAttributeNumeric lan on l.ListingID = lan.ListingID and lan.AttributeID = @AttributeID
		where i.totalmonthlyfees is not null
			and lan.ListingAttributeNumericID is null

		-- Delete
		delete from dbo.tbListingAttributeNumeric
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			inner join dbo.tbListingAttributeNumeric lan on l.ListingID = lan.ListingID and lan.AttributeID = @AttributeID
		where i.totalmonthlyfees is null
	end

	-- Update status
	if update(status)
	begin
		-- Insert
		insert into dbo.tbListingStatus (ListingID, StatusID, StatusDateTime)
		select l.ListingID, i.status, i.modifieddate
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			left join dbo.tbListingStatus dst on i.listingid = dst.ListingID and i.status = dst.StatusID and i.modifieddate = dst.StatusDateTime
		where i.status is not null
			and dst.ListingStatusID is null
	end

	-- Update price
	if update(price)
	begin
		-- Insert
		insert into dbo.tbListingPrice (ListingID, ListingPrice, PriceDateTime)
		select l.ListingID, i.price, i.modifieddate
		from Inserted i
			inner join dbo.tbListing l on i.listingid = l.ListingID
			left join dbo.tbListingPrice dst on i.listingid = dst.ListingID and i.price = dst.ListingPrice and i.modifieddate = dst.PriceDateTime
		where i.price is not null
			and dst.ListingPriceID is null
	end

end
go