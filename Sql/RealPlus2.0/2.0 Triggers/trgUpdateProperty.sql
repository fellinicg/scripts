use Jaguar_v2_Prototype2_Dev
use Jaguar_v2_Prototype2_BHS_2
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.triggers where object_id = object_id(N'dbo.trgUpdateProperty'))
drop trigger dbo.trgUpdateProperty
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160511
-- ============================================================
create trigger dbo.trgUpdateProperty
on dbo.properties
for update
as 
begin
	-- Initialize settings
	set nocount on

	-- Declarations
	declare @AttributeID int

	-- Update buildingtype (1-1)
	if update(buildingtype)
	begin
		set @AttributeID = 19

		-- Update
		update dbo.tbPropertyAttribute set AttributeValueID = i.buildingtype, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID
		where i.buildingtype is not null

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, i.buildingtype
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and i.buildingtype = pa.AttributeValueID
		where i.buildingtype is not null
			and pa.PropertyAttributeID is null

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID
		where i.buildingtype is null
	end
	
	-- Update ownershiptype (1-1)
	if update(ownershiptype)
	begin
		set @AttributeID = 72

		-- Update
		update dbo.tbPropertyAttribute set AttributeValueID = i.ownershiptype, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID
		where i.ownershiptype is not null

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, i.ownershiptype
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and i.ownershiptype = pa.AttributeValueID
		where i.ownershiptype is not null
			and pa.PropertyAttributeID is null

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID
		where i.ownershiptype is null
	end

	-- Update postwar (1-1)
	if update(postwar)
	begin
		set @AttributeID = 1118

		-- Update
		update dbo.tbPropertyAttribute set AttributeValueID = i.postwar, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID
		where i.postwar is not null

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, i.postwar
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and i.postwar = pa.AttributeValueID
		where i.postwar is not null
			and pa.PropertyAttributeID is null

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID
		where i.postwar is null
	end

	-- Update buildingallowed (1-n)
	if update(buildingallowed)
	begin
		set @AttributeID = 12

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID
		where i.buildingallowed is null

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, l.ListItem
		from Inserted i 
			cross apply dbo.tfpipelisttotable(i.buildingallowed) l
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
		where i.buildingallowed is not null
			and pa.PropertyAttributeID is null
	end
	
	-- Update buildingnotallowed (1-n)
	if update(buildingnotallowed)
	begin
		set @AttributeID = 16

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID
		where i.buildingnotallowed is null

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, l.ListItem
		from Inserted i 
			cross apply dbo.tfpipelisttotable(i.buildingnotallowed) l
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
		where i.buildingnotallowed is not null
			and pa.PropertyAttributeID is null
	end

	-- Update buildingamenities (1-n)
	if update(buildingamenities)
	begin
		set @AttributeID = 13

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, l.ListItem
		from Inserted i 
			cross apply dbo.tfpipelisttotable(i.buildingamenities) l
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
		where i.buildingamenities is not null
			and pa.PropertyAttributeID is null
	end

	-- Update buildingpersonnel (1-n)
	if update(buildingpersonnel)
	begin
		set @AttributeID = 17

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, l.ListItem
		from Inserted i 
			cross apply dbo.tfpipelisttotable(i.buildingpersonnel) l
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
		where i.buildingpersonnel is not null
			and pa.PropertyAttributeID is null
	end

	-- Update facade (1-n)
	if update(facade)
	begin
		set @AttributeID = 34

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, l.ListItem
		from Inserted i 
			cross apply dbo.tfpipelisttotable(i.facade) l
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
		where i.facade is not null
			and pa.PropertyAttributeID is null
	end

	-- Update housestyle (1-n)
	if update(housestyle)
	begin
		set @AttributeID = 114

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, l.ListItem
		from Inserted i 
			cross apply dbo.tfpipelisttotable(i.housestyle) l
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
		where i.housestyle is not null
			and pa.PropertyAttributeID is null
	end

	-- Update housetype (1-n)
	if update(housetype)
	begin
		set @AttributeID = 1130

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, l.ListItem
		from Inserted i 
			cross apply dbo.tfpipelisttotable(i.housetype) l
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
		where i.housetype is not null
			and pa.PropertyAttributeID is null
	end

	-- Update townhousefeatures (1-n)
	if update(townhousefeatures)
	begin
		set @AttributeID = 1131

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, l.ListItem
		from Inserted i 
			cross apply dbo.tfpipelisttotable(i.townhousefeatures) l
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
		where i.townhousefeatures is not null
			and pa.PropertyAttributeID is null
	end

	-- Update buildingname
	if update(buildingname)
	begin
		set @AttributeID = 14

		-- Update
		update dbo.tbPropertyAttributeText set AttributeValue = i.buildingname, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttributeText pat on u.PropertyID = pat.PropertyID and pat.AttributeID = @AttributeID
		where i.buildingname is not null

		-- Insert
		insert into dbo.tbPropertyAttributeText (PropertyID, AttributeID, AttributeValue)
		select u.PropertyID, @AttributeID, i.buildingname
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			left join dbo.tbPropertyAttributeText pat on u.PropertyID = pat.PropertyID and pat.AttributeID = @AttributeID
		where i.buildingname is not null
			and pat.PropertyAttributeTextID is null

		-- Delete
		delete from dbo.tbPropertyAttributeText
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttributeText pat on u.PropertyID = pat.PropertyID and pat.AttributeID = @AttributeID
		where i.buildingname is null
	end

	-- Update addressid (postalcode)
	if update(addressid)
	begin
		set @AttributeID = 1259

		-- Update
		update dbo.tbPropertyAttributeText set AttributeValue = a.postalcode, UpdateDateTime = getdate()
		from Inserted i
			inner join units u on i.propertyid = u.propertyid
			inner join addresses a on i.addressid = a.addressid
			inner join dbo.tbProperty p on u.unitid = p.EtlID and p.PropertyClassID = 6
			inner join dbo.tbPropertyAttributeText pat on p.PropertyID = pat.PropertyID and pat.AttributeID = @AttributeID
		where a.postalcode is not null

		-- Insert
		insert into dbo.tbPropertyAttributeText (PropertyID, AttributeID, AttributeValue)
		select distinct p.PropertyID, @AttributeID, a.postalcode
		from Inserted i
			inner join units u on i.propertyid = u.propertyid
			inner join addresses a on i.addressid = a.addressid
			inner join dbo.tbProperty p on u.unitid = p.EtlID and p.PropertyClassID = 6
			left join dbo.tbPropertyAttributeText pat on u.unitid = pat.PropertyID and pat.AttributeID = @AttributeID
		where a.postalcode is not null
			and pat.PropertyAttributeTextID is null

		-- Delete
		delete from dbo.tbPropertyAttributeText
		from Inserted i
			inner join units u on i.propertyid = u.propertyid
			inner join dbo.tbProperty p on u.unitid = p.EtlID and p.PropertyClassID = 6
			inner join dbo.tbPropertyAttributeText pat on p.PropertyID = pat.PropertyID and pat.AttributeID = @AttributeID
		where i.addressid is null
	end

	-- Update yearbuilt
	if update(yearbuilt)
	begin
		set @AttributeID = 106

		-- Update
		update dbo.tbPropertyAttributeNumeric set AttributeValue = i.yearbuilt, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.yearbuilt is not null

		-- Insert
		insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
		select u.PropertyID, @AttributeID, i.yearbuilt
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.yearbuilt is not null
			and pan.PropertyAttributeNumericID is null

		-- Delete
		delete from dbo.tbPropertyAttributeText
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.yearbuilt is null
	end

	-- Update financingallowedpercent
	if update(financingallowedpercent)
	begin
		set @AttributeID = 36

		-- Update
		update dbo.tbPropertyAttributeNumeric set AttributeValue = i.financingallowedpercent, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.financingallowedpercent is not null

		-- Insert
		insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
		select u.PropertyID, @AttributeID, i.financingallowedpercent
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.financingallowedpercent is not null
			and pan.PropertyAttributeNumericID is null

		-- Delete
		delete from dbo.tbPropertyAttributeText
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.financingallowedpercent is null
	end

	-- Update totalunits
	if update(totalunits)
	begin
		set @AttributeID = 36

		-- Update
		update dbo.tbPropertyAttributeNumeric set AttributeValue = i.totalunits, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.totalunits is not null

		-- Insert
		insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
		select u.PropertyID, @AttributeID, i.totalunits
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.totalunits is not null
			and pan.PropertyAttributeNumericID is null

		-- Delete
		delete from dbo.tbPropertyAttributeText
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.totalunits is null
	end

	-- Update frontfootage
	if update(frontfootage)
	begin
		set @AttributeID = 1128

		-- Update
		update dbo.tbPropertyAttributeNumeric set AttributeValue = i.frontfootage, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.frontfootage is not null

		-- Insert
		insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
		select u.PropertyID, @AttributeID, i.frontfootage
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.frontfootage is not null
			and pan.PropertyAttributeNumericID is null

		-- Delete
		delete from dbo.tbPropertyAttributeText
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.frontfootage is null
	end

	-- Update rp_bin
	if update(rp_bin)
	begin
		set @AttributeID = 1260

		-- Update
		update dbo.tbPropertyAttributeNumeric set AttributeValue = i.rp_bin, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.rp_bin is not null

		-- Insert
		insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
		select u.PropertyID, @AttributeID, i.rp_bin
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.rp_bin is not null
			and pan.PropertyAttributeNumericID is null

		-- Delete
		delete from dbo.tbPropertyAttributeText
		from Inserted i
			inner join dbo.tbProperty u on i.propertyid = u.EtlID and u.PropertyClassID = 1
			inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.rp_bin is null
	end

end
go