use Jaguar_v2_Prototype2_Dev
use Jaguar_v2_Prototype2_BHS_2
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.triggers where object_id = object_id(N'dbo.trgInsertOpenHouse'))
drop trigger dbo.trgInsertOpenHouse
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160711
-- ============================================================
create trigger dbo.trgInsertOpenHouse
on dbo.openhouse
for insert
as 
begin
	-- Initialize settings
	set nocount on

	-- Insert into tbListingOpenHouse

	insert into dbo.tbListingOpenHouse (ListingOpenHouseID, ListingID, StartDateTime, EndDateTime, Comment)
	select i.openhouseid, i.recordid, convert(datetime2, i.openhousedate + ' ' + i.starttime), convert(datetime2, i.openhousedate + ' ' + i.endtime), remarks
	from Inserted i
		left join dbo.tbListingOpenHouse dst on i.openhouseid = dst.ListingOpenHouseID
	where isdate(i.openhousedate) = 1
		and isdate(i.starttime) = 1
		and isdate(i.endtime) = 1
		and i.recordtype = 'L'
		and dst.ListingOpenHouseID is null

end
go