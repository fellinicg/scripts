use Jaguar_v2_WiP
use TMF
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.triggers where object_id = object_id(N'dbo.trgUpdatePropertyUnit'))
drop trigger dbo.trgUpdatePropertyUnit
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160511
-- ============================================================
create trigger dbo.trgUpdatePropertyUnit
on dbo.properties_units
for update
as 
begin
	-- Initialize settings
	set nocount on

	-- Handle multiple record updates
	update dbo.tbProperty set Block = 1000
	from Inserted i
		inner join dbo.tbProperty p on i.unitid = p.EtlID and p.PropertyClassID = 6
end
go