use Jaguar_v2_Prototype2_BHS_2
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.triggers where object_id = object_id(N'dbo.trgInsertListing'))
drop trigger dbo.trgInsertListing
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160511
-- ============================================================
create trigger dbo.trgInsertListing
on dbo.listings
for insert
as 
begin
	-- Initialize settings
	set nocount on

	-- Declarations
	declare @AttributeID int

	-- Insert unit into tbProperty, if they do not exist
	--insert into dbo.tbProperty (PropertyClassID, EtlID)
	--select 6, i.unitid
	--from Inserted i
	--	left join dbo.tbProperty p on i.unitid = p.EtlID and p.PropertyClassID = 6
	--where p.PropertyID is null

	-- Insert listing into tbListing
--	insert into dbo.tbListing (ListingID, ListingCategoryID, PropertyID, ListingTypeID, ListingProviderID, InitialPrice, CurrentPrice, CurrentStatus, ListDate)
	insert into dbo.tbListing (ListingID, ListingCategoryID, PropertyID, ListingTypeID, ListingProviderID, InitialPrice)
	--select i.listingid, i.saleorrental, p.PropertyID, i.listingtype, i.source, i.originalprice, i.price, av.ValueName, i.datelisted
	select i.listingid, i.saleorrental, p.PropertyID, i.listingtype, i.source, i.originalprice
	from Inserted i
		inner join dbo.tbProperty p on i.unitid = p.EtlID and p.PropertyClassID = 6
		--left join vwAttributeValues av on i.status = AttributeValueID
		left join dbo.tbListing l on i.listingid = l.ListingID
	where l.ListingID is null

	-- Insert leasetype => tbListingAttribute (1-1)
	insert into dbo.tbListingAttribute (ListingID, AttributeValueID)
	select l.ListingID, i.leasetype
	from Inserted i
		inner join dbo.tbListing l on  i.listingid = l.ListingID
		left join dbo.tbListingAttribute la on l.ListingID = la.ListingID and i.leasetype = la.AttributeValueID
	where i.leasetype is not null
		and la.ListingAttributeID is null
		
	-- Insert feepaid => tbListingAttribute (1-1)
	insert into dbo.tbListingAttribute (ListingID, AttributeValueID)
	select l.ListingID, i.feepaid
	from Inserted i
		inner join dbo.tbListing l on  i.listingid = l.ListingID
		left join dbo.tbListingAttribute la on l.ListingID = la.ListingID and i.feepaid = la.AttributeValueID
	where i.feepaid is not null
		and la.ListingAttributeID is null
		
	-- Insert webid => tbListingAttributeText
	set @AttributeID = 54
	
	insert into dbo.tbListingAttributeText (ListingID, AttributeID, AttributeValue)
	select l.ListingID, @AttributeID, i.webid
	from Inserted i
		inner join dbo.tbListing l on i.listingid = l.ListingID
		left join dbo.tbListingAttributeText lat on l.ListingID = lat.ListingID and lat.AttributeID = @AttributeID
	where i.webid is not null
		and lat.ListingAttributeTextID is null

	-- Insert listingnumber => tbListingAttributeText
	set @AttributeID = 53

	insert into dbo.tbListingAttributeText (ListingID, AttributeID, AttributeValue)
	select l.ListingID, @AttributeID, i.listingnumber
	from Inserted i
		inner join dbo.tbListing l on i.listingid = l.ListingID
		left join dbo.tbListingAttributeText lat on l.ListingID = lat.ListingID and lat.AttributeID = @AttributeID
	where i.listingnumber is not null
		and lat.ListingAttributeTextID is null

	-- Insert pricepersqft => tbListingAttributeNumeric
	set @AttributeID = 79

	insert into dbo.tbListingAttributeNumeric (ListingID, AttributeID, AttributeValue)
	select l.ListingID, @AttributeID, i.pricepersqft
	from Inserted i
		inner join dbo.tbListing l on i.listingid = l.ListingID
		left join dbo.tbListingAttributeNumeric lan on l.ListingID = lan.ListingID and lan.AttributeID = @AttributeID
	where i.pricepersqft is not null
		and lan.ListingAttributeNumericID is null

	-- Insert leasetermmin => tbListingAttributeNumeric
	set @AttributeID = 1251

	insert into dbo.tbListingAttributeNumeric (ListingID, AttributeID, AttributeValue)
	select distinct l.ListingID, @AttributeID, i.leasetermmin
	from Inserted i
		inner join dbo.tbListing l on i.listingid = l.ListingID
		left join dbo.tbListingAttributeNumeric lan on l.ListingID = lan.ListingID and lan.AttributeID = @AttributeID
	where i.leasetermmin is not null
		and lan.ListingAttributeNumericID is null

	-- Insert leasetermmax => tbListingAttributeNumeric
	set @AttributeID = 1252

	insert into dbo.tbListingAttributeNumeric (ListingID, AttributeID, AttributeValue)
	select distinct l.ListingID, @AttributeID, i.leasetermmax
	from Inserted i
		inner join dbo.tbListing l on i.listingid = l.ListingID
		left join dbo.tbListingAttributeNumeric lan on l.ListingID = lan.ListingID and lan.AttributeID = @AttributeID
	where i.leasetermmax is not null
		and lan.ListingAttributeNumericID is null

	-- Insert totalmonthlyfees => tbListingAttributeNumeric
	set @AttributeID = 76

	insert into dbo.tbListingAttributeNumeric (ListingID, AttributeID, AttributeValue)
	select distinct l.ListingID, @AttributeID, i.totalmonthlyfees
	from Inserted i
		inner join dbo.tbListing l on i.listingid = l.ListingID
		left join dbo.tbListingAttributeNumeric lan on l.ListingID = lan.ListingID and lan.AttributeID = @AttributeID
	where i.totalmonthlyfees is not null
		and lan.ListingAttributeNumericID is null

	-- Insert status into tbListingStatus
	insert into dbo.tbListingStatus (ListingID, StatusID, StatusDateTime)
	select l.ListingID, i.status, isnull(i.updted, getdate())
	from Inserted i
		inner join dbo.tbListing l on i.listingid = l.ListingID
		left join dbo.tbListingStatus dst on i.listingid = dst.ListingID and i.status = dst.StatusID and i.updted = dst.StatusDateTime
	where i.status is not null
		and dst.ListingStatusID is null

	-- Insert price into tbListingPrice
	insert into dbo.tbListingPrice (ListingID, ListingPrice, PriceDateTime)
	select l.ListingID, i.price, isnull(i.modifieddate, getdate())
	from Inserted i
		inner join dbo.tbListing l on i.listingid = l.ListingID
		left join dbo.tbListingPrice dst on i.listingid = dst.ListingID and i.price = dst.ListingPrice and i.modifieddate = dst.PriceDateTime
	where i.price is not null
		and dst.ListingPriceID is null

	-- Insert price into tbListingActivity
	insert into dbo.tbListingActivity (ListingID, ActivityID, ActivityDateTime)
	select l.ListingID, i.AttributeID, i.AttributeValue
	from (
		select 
			up.listingid
			, case up.AttributeID
				when 'availabledate' then 2553
				when 'closingdate' then 2552
				when 'contractsigndate' then 2551
				when 'datelisted' then 2549
			  end 'AttributeID'
			, up.AttributeValue
		from (
			select 
				listingid
				, availabledate
				, closingdate
				, contractsigndate
				, datelisted
			from Inserted) i
			unpivot (AttributeValue for AttributeID in (availabledate, closingdate, contractsigndate, datelisted)
		) up
	) i
	inner join dbo.tbListing l on i.listingid = l.ListingID
	left join dbo.tbListingActivity la on l.ListingID = la.ListingID and i.AttributeID = la.ActivityID
	where la.ListingActivityID is null

end
go