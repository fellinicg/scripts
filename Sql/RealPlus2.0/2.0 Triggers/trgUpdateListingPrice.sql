use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.triggers where object_id = object_id(N'dbo.trgUpdateListingPrice'))
drop trigger dbo.trgUpdateListingPrice
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160405
-- ============================================================
create trigger dbo.trgUpdateListingPrice
on dbo.tbListingPrice
for insert
as 
begin
	-- Initialize settings
	set nocount on

	-- Handle multiple record inserts
	update dbo.tbListing set CurrentPriceID = i.ListingPriceID, CurrentPrice = i.ListingPrice
	from Inserted i
		inner join dbo.tbListing l on i.ListingID = l.ListingID
end
go