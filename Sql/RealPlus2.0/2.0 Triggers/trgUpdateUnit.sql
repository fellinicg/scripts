use Jaguar_v2_Prototype2_BHS_2
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.triggers where object_id = object_id(N'dbo.trgUpdateUnit'))
drop trigger dbo.trgUpdateUnit
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160511
-- ============================================================
create trigger dbo.trgUpdateUnit
on dbo.units
for update
as 
begin
	-- Initialize settings
	set nocount on

	-- Declarations
	declare @AttributeID int

	-- Update propertyid (ParentPropertyID)
	if update(propertyid)
	begin
		-- Insert building (parent) into tbProperty, if they do not exist
		insert into dbo.tbProperty (PropertyClassID, EtlID)
		select distinct 1, i.propertyid
		from Inserted i
			left join dbo.tbProperty p on i.propertyid = p.EtlID and p.PropertyClassID = 1
		where p.PropertyID is null

		-- Update
		update dbo.tbProperty set ParentPropertyID = p.ParentPropertyID, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbProperty p on i.propertyid = p.EtlID and p.PropertyClassID = 1
	end

	-- Update advaptmiscr (1-n)
	if update(advaptmiscr)
	begin
		set @AttributeID = 1127

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, l.ListItem
		from Inserted i 
			cross apply dbo.tfpipelisttotable(i.advaptmiscr) l
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
		where i.advaptmiscr is not null
			and pa.PropertyAttributeID is null
	end

	-- Update advaptmiscs (1-n)
	if update(advaptmiscs)
	begin
		set @AttributeID = 1126

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, l.ListItem
		from Inserted i 
			cross apply dbo.tfpipelisttotable(i.advaptmiscs) l
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
		where i.advaptmiscs is not null
			and pa.PropertyAttributeID is null
	end

	-- Update viewattributes (1-n)
	if update(viewattributes)
	begin
		set @AttributeID = 105

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, l.ListItem
		from Inserted i 
			cross apply dbo.tfpipelisttotable(i.viewattributes) l
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
		where i.viewattributes is not null
			and pa.PropertyAttributeID is null
	end

	-- Update outdoorspace (1-n)
	if update(outdoorspace)
	begin
		set @AttributeID = 71

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, l.ListItem
		from Inserted i 
			cross apply dbo.tfpipelisttotable(i.outdoorspace) l
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
		where i.outdoorspace is not null
			and pa.PropertyAttributeID is null
	end
	
	-- Update townhousefeatures (1-n)
	if update(townhousefeatures)
	begin
		set @AttributeID = 1131

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, l.ListItem
		from Inserted i 
			cross apply dbo.tfpipelisttotable(i.townhousefeatures) l
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
		where i.townhousefeatures is not null
			and pa.PropertyAttributeID is null
	end

	-- Update exposureattribute (1-n)
	if update(exposureattribute)
	begin
		set @AttributeID = 33

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, l.ListItem
		from Inserted i 
			cross apply dbo.tfpipelisttotable(i.exposureattribute) l
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
		where i.exposureattribute is not null
			and pa.PropertyAttributeID is null
	end
		
	-- Update ispenthouse (1-n)
	if update(ispenthouse)
	begin
		set @AttributeID = 74

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, l.ListItem
		from Inserted i 
			cross apply dbo.tfpipelisttotable(i.ispenthouse) l
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
		where i.ispenthouse = 298
			and pa.PropertyAttributeID is null
	end
				
	-- Update apttype (1-n)
	if update(apttype)
	begin
		set @AttributeID = 7

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, l.ListItem
		from Inserted i 
			cross apply dbo.tfpipelisttotable(i.apttype) l
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
		where i.apttype is not null
	end

	-- Update multifloor (1-n)
	if update(multifloor)
	begin
		set @AttributeID = 112

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, l.ListItem
		from Inserted i 
			cross apply dbo.tfpipelisttotable(i.multifloor) l
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
		where i.multifloor is not null
	end

	-- Update miscessentials (1-n)
	if update(miscessentials)
	begin
		set @AttributeID = 1265

		-- Delete
		delete from dbo.tbPropertyAttribute
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID
			inner join dbo.tbAttributeValue av on av.AttributeID = @AttributeID and pa.AttributeValueID = av.AttributeValueID

		-- Insert
		insert into dbo.tbPropertyAttribute (PropertyID, AttributeValueID)
		select u.PropertyID, l.ListItem
		from Inserted i 
			cross apply dbo.tfpipelisttotable(i.miscessentials) l
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			left join dbo.tbPropertyAttribute pa on u.PropertyID = pa.PropertyID and l.ListItem = pa.AttributeValueID
		where i.miscessentials is not null
	end

	-- Update unitnumber_full
	if update(unitnumber_full)
	begin
		set @AttributeID = 100

		-- Update
		update dbo.tbPropertyAttributeText set AttributeValue = i.unitnumber_full, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttributeText pat on u.PropertyID = pat.PropertyID and pat.AttributeID = @AttributeID
		where i.unitnumber_full is not null

		-- Insert
		insert into dbo.tbPropertyAttributeText (PropertyID, AttributeID, AttributeValue)
		select u.PropertyID, @AttributeID, i.unitnumber_full
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			left join dbo.tbPropertyAttributeText pat on u.PropertyID = pat.PropertyID and pat.AttributeID = @AttributeID
		where i.unitnumber_full is not null
			and pat.PropertyAttributeTextID is null

		-- Delete
		delete from dbo.tbPropertyAttributeText
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttributeText pat on u.PropertyID = pat.PropertyID and pat.AttributeID = @AttributeID
		where i.unitnumber_full is null
	end

	-- Update bedrooms
	if update(bedrooms)
	begin
		set @AttributeID = 11

		-- Update
		update dbo.tbPropertyAttributeNumeric set AttributeValue = i.bedrooms, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.bedrooms is not null

		-- Insert
		insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
		select u.PropertyID, @AttributeID, i.bedrooms
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.bedrooms is not null
			and pan.PropertyAttributeNumericID is null

		-- Delete
		delete from dbo.tbPropertyAttributeNumeric
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.bedrooms is null
	end

	-- Update bathrooms
	if update(bathrooms)
	begin
		set @AttributeID = 10

		-- Update
		update dbo.tbPropertyAttributeNumeric set AttributeValue = i.bathrooms, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.bathrooms is not null

		-- Insert
		insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
		select u.PropertyID, @AttributeID, i.bathrooms
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.bathrooms is not null
			and pan.PropertyAttributeNumericID is null

		-- Delete
		delete from dbo.tbPropertyAttributeNumeric
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.bathrooms is null
	end

	-- Update halfbaths
	if update(halfbaths)
	begin
		set @AttributeID = 41

		-- Update
		update dbo.tbPropertyAttributeNumeric set AttributeValue = i.halfbaths, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.halfbaths is not null

		-- Insert
		insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
		select u.PropertyID, @AttributeID, i.halfbaths
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.halfbaths is not null
			and pan.PropertyAttributeNumericID is null

		-- Delete
		delete from dbo.tbPropertyAttributeNumeric
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.halfbaths is null
	end

	-- Update totalrooms
	if update(totalrooms)
	begin
		set @AttributeID = 85

		-- Update
		update dbo.tbPropertyAttributeNumeric set AttributeValue = i.totalrooms, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.totalrooms is not null

		-- Insert
		insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
		select u.PropertyID, @AttributeID, i.totalrooms
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.totalrooms is not null
			and pan.PropertyAttributeNumericID is null

		-- Delete
		delete from dbo.tbPropertyAttributeNumeric
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.totalrooms is null
	end

	-- Update totalsqft
	if update(totalsqft)
	begin
		set @AttributeID = 91

		-- Update
		update dbo.tbPropertyAttributeNumeric set AttributeValue = i.totalsqft, UpdateDateTime = getdate()
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.totalsqft is not null

		-- Insert
		insert into dbo.tbPropertyAttributeNumeric (PropertyID, AttributeID, AttributeValue)
		select u.PropertyID, @AttributeID, i.totalsqft
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			left join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.totalsqft is not null
			and pan.PropertyAttributeNumericID is null

		-- Delete
		delete from dbo.tbPropertyAttributeNumeric
		from Inserted i
			inner join dbo.tbProperty u on i.unitid = u.EtlID and u.PropertyClassID = 6
			inner join dbo.tbPropertyAttributeNumeric pan on u.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
		where i.totalsqft is null
	end

end
go