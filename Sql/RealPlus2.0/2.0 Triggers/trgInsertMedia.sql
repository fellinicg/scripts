use Jaguar_v2_Prototype2_Dev
use Jaguar_v2_Prototype2_BHS_2
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.triggers where object_id = object_id(N'dbo.trgInsertMedia'))
drop trigger dbo.trgInsertMedia
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160711
-- ============================================================
create trigger dbo.trgInsertMedia
on dbo.media
for insert
as 
begin
	-- Initialize settings
	set nocount on

	-- Declarations
	declare @AttributeID int

	-- Insert Media into tbMedia (Floorplans and Photos)
	insert into dbo.tbMedia (MediaID, MediaTypeID, Link)
	select i.Mediaid, i.mediatype, i.imageurl
	from Inserted i
		left join dbo.tbMedia m on i.Mediaid = m.MediaID
	where i.filetype in ('F', 'P')
		and i.imageurl is not null
		and m.MediaID is null

	-- Insert into tbListingMedia
	insert into dbo.tbListingMedia (ListingID, MediaID, ListingMediaTypeID, Detail, SortOrder)
	select i.recordid, i.mediaid, listingmediatype, description, imageorder
	from Inserted i
		inner join dbo.tbMedia m on i.Mediaid = m.MediaID
		left join dbo.tbListingMedia dst on i.MediaID = dst.MediaID and i.recordid = dst.ListingID
	where dst.ListingMediaID is null

end
go