use Jaguar_v2_WiP
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfGetAttributeValue') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfGetAttributeValue
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160322
-- ============================================================
go
create function dbo.sfGetAttributeValue (@AttributeValueID int)
returns varchar(100)
as
begin
	return (select v.ValueName from dbo.tbAttributeValue av inner join dbo.tbValue v on av.ValueID = v.ValueID where av.AttributeValueID = @AttributeValueID)
end