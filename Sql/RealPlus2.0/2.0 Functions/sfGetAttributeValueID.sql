use Jaguar_v2_WiP
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfGetAttributeValueID') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfGetAttributeValueID
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160314
-- ============================================================
go
create function dbo.sfGetAttributeValueID(@AttributeName varchar(100), @ValueName varchar(100))
returns int
as
begin
   return (select AttributeValueID from dbo.vwAttributeValues where AttributeName = @AttributeName and ValueName = @ValueName)
end