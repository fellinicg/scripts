use Jaguar_v2_WiP
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfGetListingParentPropertyID') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfGetListingParentPropertyID
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160414
-- ============================================================
go
create function dbo.sfGetListingParentPropertyID(@ListingID int)
returns int
as
begin
   return (select p.ParentPropertyID from dbo.tbListing l join dbo.tbProperty p on l.PropertyID = p.PropertyID where l.ListingID = @ListingID)
end