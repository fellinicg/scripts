use Jaguar_v2_WiP
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.BeginsWith') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.BeginsWith
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- ============================================================
go
create function dbo.BeginsWith (@SearchIn varchar(max), @SearchFor varchar(50))
returns bit
as
begin
   return (case charindex(@SearchFor, @SearchIn) when 1 then 1 else 0 end)
end