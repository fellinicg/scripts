use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
create function dbo.ufEmailAddressIsValid (@EmailAddress varchar(120))
returns bit
as
begin
declare
  @IsValid bit
  
  if (
     -- No embedded spaces
     charindex(' ',ltrim(rtrim(@EmailAddress))) = 0
     -- '@' can't be the first character of an email address
     and left(ltrim(@EmailAddress),1) <> '@'
     -- '.' can't be the last character of an email address
     and RIGHT(rtrim(@EmailAddress),1) <> '.'
     -- There must be a '.' after '@'
     and charindex('.',@EmailAddress,charindex('@',@EmailAddress)) - charindex('@',@EmailAddress) > 1
     -- Only one '@' sign is allowed
     and len(ltrim(rtrim(@EmailAddress))) - len(replace(ltrim(rtrim(@EmailAddress)),'@','')) = 1
     -- Domain name should end with at least 2 character extension
     and charindex('.',reverse(ltrim(rtrim(@EmailAddress)))) >= 3
     -- can't have patterns like '.@' and '..'
     and (charindex('.@',@EmailAddress) = 0 and charindex('..',@EmailAddress) = 0)
     )
    set @IsValid = 1
  else
    set @IsValid = 0

  return(@IsValid)

end