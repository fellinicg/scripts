use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tfChildPropertyAttributeNumericMissing') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.tfChildPropertyAttributeNumericMissing
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160326
-- ============================================================
create function dbo.tfChildPropertyAttributeNumericMissing (@AttributeID int)
returns table 
as
return 

	select distinct l.ListingID
	from dbo.tbListing l
		left join dbo.tbPropertyAttributeNumeric pan on l.PropertyID = pan.PropertyID and pan.AttributeID = @AttributeID
	where pan.PropertyAttributeNumericID is null