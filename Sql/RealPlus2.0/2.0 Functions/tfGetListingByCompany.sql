use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tfGetListingByCompany') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.tfGetListingByCompany
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160415
-- ============================================================
create function dbo.tfGetListingByCompany (@Omni varchar(max))
returns table
as
return
	select lo.ListingID
	from dbo.tbCompany c
	inner join dbo.tbOffice o on c.CompanyID = o.CompanyID
	inner join dbo.tbListingOffice lo on o.OfficeID = lo.OfficeID
	where charindex(@Omni, c.CompanyName) > 0
	or charindex(@Omni, o.Detail) > 0
	or charindex(@Omni, o.OfficeCode) > 0