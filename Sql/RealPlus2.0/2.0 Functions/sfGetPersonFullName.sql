use Jaguar_v2_WiP
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfGetPersonFullName') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfGetPersonFullName
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160325
-- ============================================================
go
create function dbo.sfGetPersonFullName (@PersonID int)
returns varchar(200)
as
begin
	return (select FullName from dbo.tbPerson where PersonID = @PersonID)
end