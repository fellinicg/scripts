use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tfListingAttribute') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.tfListingAttribute
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160321
-- ============================================================
create function dbo.tfListingAttribute (@AttributeName varchar(200))
returns table
as
return
	select lan.ListingID, a.AttributeCode, a.AttributeName, convert(varchar(200), lan.AttributeValue) 'AttributeValue'
	from dbo.tbListingAttributeNumeric lan 
		inner join dbo.tbAttribute a on lan.AttributeID = a.AttributeID
	where lan.IsDeleted = 0 
		and a.IsDeleted = 0
		and AttributeName = @AttributeName
	union
	select lat.ListingID, a.AttributeCode, a.AttributeName, lat.AttributeValue
	from dbo.tbListingAttributeText lat 
		inner join dbo.tbAttribute a on lat.AttributeID = a.AttributeID
	where 
		lat.IsDeleted = 0 
		and a.IsDeleted = 0
		and AttributeName = @AttributeName