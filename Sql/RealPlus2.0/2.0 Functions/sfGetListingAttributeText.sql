use Jaguar_v2_WiP
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfGetListingAttributeText') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfGetListingAttributeText
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160322
-- ============================================================
go
create function dbo.sfGetListingAttributeText (@ListingID int, @AttributeID int)
returns float
as
begin
   return (select AttributeValue from dbo.tbListingAttributeText where ListingID = @ListingID and AttributeID = @AttributeID)
end