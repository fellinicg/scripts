use Jaguar_v2_Prototype2_Dev
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfGetExclusiveAgentEmail') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfGetExclusiveAgentEmail
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 2016719
-- ============================================================
go
create function dbo.sfGetExclusiveAgentEmail (@ListingID int)
returns varchar(200)
as
begin
	return (select top 1 email from dbo.contacts where recordid = @ListingID and recordtype = 'L' and contacttype = 'E')
end