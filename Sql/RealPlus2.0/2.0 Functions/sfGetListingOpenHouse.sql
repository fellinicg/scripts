use Jaguar_v2_Prototype2_BHS_2
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfGetListingOpenHouse') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfGetListingOpenHouse
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160804
-- ============================================================
go
create function dbo.sfGetListingOpenHouse(@ListingID int)
returns varchar(100)
as
begin
   return (select top 1 format(StartDateTime, 'MM/dd/yyyy hh:mm tt') + ' to ' + format(EndDateTime, 'MM/dd/yyyy hh:mm tt') from dbo.tbListingOpenhouse where ListingID = @ListingID order by StartDateTime desc)
end