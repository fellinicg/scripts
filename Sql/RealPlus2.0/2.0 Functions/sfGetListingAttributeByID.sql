use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfGetListingAttributeByID') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfGetListingAttributeByID
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160422
-- ============================================================
go
create function dbo.sfGetListingAttributeByID (@ListingID int, @AttributeID int)
returns varchar(max)
as
begin
	return (
   		select stuff((select ',' + convert(varchar(max), AttributeValue)
		from (
			select v.ValueName AttributeValue
			from dbo.tbListingAttribute la
				inner join dbo.tbAttributeValue av on la.AttributeValueID = av.AttributeValueID and av.Isdeleted = 0
				inner join dbo.tbAttribute a on av.AttributeID = a.AttributeID and a.IsDeleted = 0
				inner join dbo.tbValue v on av.ValueID = v.ValueID and v.Isdeleted = 0
			where 
				la.IsDeleted = 0
				and la.ListingID = @ListingID
				and a.AttributeID = @AttributeID
			union
			select convert(varchar, lan.AttributeValue) AttributeValue
			from dbo.tbListingAttributeNumeric lan 
				inner join dbo.tbAttribute a on lan.AttributeID = a.AttributeID and a.IsDeleted = 0
			where 
				lan.IsDeleted = 0 
				and lan.ListingID = @ListingID
				and a.AttributeID = @AttributeID
			union
			select lat.AttributeValue
			from dbo.tbListingAttributeText lat 
				inner join dbo.tbAttribute a on lat.AttributeID = a.AttributeID and a.IsDeleted = 0
			where 
				lat.IsDeleted = 0 
				and lat.ListingID = @ListingID
				and a.AttributeID = @AttributeID
			union
			select v.ValueName
			from dbo.tbListing l
				inner join dbo.tbPropertyAttribute pa on l.PropertyID = pa.PropertyID
				inner join dbo.tbAttributeValue av on pa.AttributeValueID = av.AttributeValueID and av.Isdeleted = 0
				inner join dbo.tbAttribute a on av.AttributeID = a.AttributeID and a.IsDeleted = 0
				inner join dbo.tbValue v on av.ValueID = v.ValueID and v.Isdeleted = 0
			where 
				pa.IsDeleted = 0
				and l.ListingID = @ListingID
				and a.AttributeID = @AttributeID
			union
			select v.ValueName AttributeValue
			from dbo.tbListing l
				inner join dbo.tbProperty p on l.PropertyID = p.PropertyID
				inner join dbo.tbPropertyAttribute pa on p.ParentPropertyID = pa.PropertyID
				inner join dbo.tbAttributeValue av on pa.AttributeValueID = av.AttributeValueID and av.Isdeleted = 0
				inner join dbo.tbAttribute a on av.AttributeID = a.AttributeID and a.IsDeleted = 0
				inner join dbo.tbValue v on av.ValueID = v.ValueID and v.Isdeleted = 0
			where 
				pa.IsDeleted = 0
				and l.ListingID = @ListingID
				and a.AttributeID = @AttributeID
			union
			select convert(varchar, pan.AttributeValue)
			from dbo.tbListing l
				inner join dbo.tbPropertyAttributeNumeric pan on l.PropertyID = pan.PropertyID
				inner join dbo.tbAttribute a on pan.AttributeID = a.AttributeID and a.IsDeleted = 0
			where 
				pan.IsDeleted = 0 
				and l.ListingID = @ListingID
				and a.AttributeID = @AttributeID
			union
			select convert(varchar, pan.AttributeValue)
			from dbo.tbListing l
				inner join dbo.tbProperty p on l.PropertyID = p.PropertyID
				inner join dbo.tbPropertyAttributeNumeric pan on p.ParentPropertyID = pan.PropertyID
				inner join dbo.tbAttribute a on pan.AttributeID = a.AttributeID and a.IsDeleted = 0
			where 
				pan.IsDeleted = 0 
				and l.ListingID = @ListingID
				and a.AttributeID = @AttributeID
			union
			select convert(varchar, pat.AttributeValue)
			from dbo.tbListing l
				inner join dbo.tbPropertyAttributeText pat on l.PropertyID = pat.PropertyID
				inner join dbo.tbAttribute a on pat.AttributeID = a.AttributeID and a.IsDeleted = 0
			where 
				pat.IsDeleted = 0 
				and l.ListingID = @ListingID
				and a.AttributeID = @AttributeID
			union
			select convert(varchar, pat.AttributeValue)
			from dbo.tbListing l
				inner join dbo.tbProperty p on l.PropertyID = p.PropertyID
				inner join dbo.tbPropertyAttributeText pat on p.ParentPropertyID = pat.PropertyID
				inner join dbo.tbAttribute a on pat.AttributeID = a.AttributeID and a.IsDeleted = 0
			where 
				pat.IsDeleted = 0 
				and l.ListingID = @ListingID
				and a.AttributeID = @AttributeID) a 
		for xml path('')),1,1,'') as AttributeValue   
	)
end