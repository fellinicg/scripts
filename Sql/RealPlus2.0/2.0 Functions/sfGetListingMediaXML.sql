use Jaguar_v2_WiP
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfGetListingMediaXML') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfGetListingMediaXML
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160409
-- ============================================================
go
create function dbo.sfGetListingMediaXML (@ListingID int)
returns xml
as
begin
	return (
			select lm.ListingMediaID, lm.ListingMediaTypeID, v.ValueName as ListingMediaType, m.Link, lm.AdditionalDetail, lm.SortOrder
			from dbo.tbListingMedia lm
				inner join dbo.tbMedia m on lm.MediaID = m.MediaID
				inner join dbo.tbAttributeValue av on lm.ListingMediaTypeID = av.AttributeValueID
				inner join dbo.tbValue v on av.ValueID = v.ValueID
            where lm.ListingID = @ListingID and lm.IsDeleted = 0 
			for xml raw
			--for xml path('ListingMedia'), type
	)
end
go
grant exec on dbo.sfGetListingMediaXML to web