use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetCustomerCustomerActivity') and type in (N'P', N'PC'))
drop procedure dbo.upGetCustomerCustomerActivity
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160413
-- ============================================================
create procedure dbo.upGetCustomerCustomerActivity
(
	@AgentID int
	, @CustomerID int
)
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select ListingID, name, value, UpdateDateTime
	from dbo.tbAgentListingPin
		unpivot (value for name in (PickListed, Liked, Visited, Disliked)) up
	where AgentID = @AgentID
		and CustomerID = @CustomerID

end
go
grant exec on dbo.upGetCustomerCustomerActivity to web


