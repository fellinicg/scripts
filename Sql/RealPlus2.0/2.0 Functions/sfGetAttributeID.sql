use Jaguar_v2_WiP
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfGetAttributeID') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfGetAttributeID
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160314
-- ============================================================
go
create function dbo.sfGetAttributeID(@AttributeName varchar(100))
returns int
as
begin
   return (select AttributeID from dbo.tbAttribute where AttributeName = @AttributeName)
end