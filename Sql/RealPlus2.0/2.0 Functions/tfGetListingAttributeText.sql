use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tfGetListingAttributeText') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.tfGetListingAttributeText
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160326
-- ============================================================
create function dbo.tfGetListingAttributeText (@AttributeID int)
returns table
as
return
	select lat.ListingID, lat.AttributeValue
	from dbo.tbListingAttributeText lat 
		inner join dbo.tbListing l on lat.ListingID = l.ListingID
	where lat.AttributeID = @AttributeID
		and l.IsDeleted = 0
		and lat.IsDeleted = 0