use Jaguar_v2_WiP
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfGetPropertyAttribute') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfGetPropertyAttribute
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160325
-- ============================================================
go
create function dbo.sfGetPropertyAttribute (@PropertyID int, @AttributeID int)
returns varchar(max)
as
begin
   return (
   		select stuff((select ',' + convert(varchar(max), AttributeValue)
		from (
			select v.ValueName AttributeValue
			from dbo.tbPropertyAttribute pa
				inner join dbo.tbAttributeValue av on pa.AttributeValueID = av.AttributeValueID
				inner join dbo.tbAttribute a on av.AttributeID = a.AttributeID
				inner join dbo.tbValue v on av.ValueID = v.ValueID
			where 
				pa.PropertyID = @PropertyID 
				and av.AttributeID = @AttributeID
				and pa.IsDeleted = 0
				and av.IsDeleted = 0
				--and a.IsDeleted = 0
				--and v.IsDeleted = 0
			union
			select convert(varchar, pan.AttributeValue)
			from dbo.tbPropertyAttributeNumeric pan 
			where 
				pan.PropertyID = @PropertyID 
				and pan.AttributeID = @AttributeID
				and pan.IsDeleted = 0 
			union
			select pat.AttributeValue
			from dbo.tbPropertyAttributeText pat
			where 
				pat.PropertyID = @PropertyID 
				and pat.AttributeID = @AttributeID
				and pat.IsDeleted = 0
		) a
		for xml path('')),1,1,'') as AttributeValue   
			--select stuff((select ',' + convert(varchar(max), v.ValueName)
			--	from dbo.tbPropertyAttribute pa
			--		inner join dbo.tbAttributeValue av on pa.AttributeValueID = av.AttributeValueID
			--		inner join dbo.tbAttribute a on av.AttributeID = a.AttributeID
			--		inner join dbo.tbValue v on av.ValueID = v.ValueID
			--	where pa.PropertyID = @PropertyID 
			--		and av.AttributeID = @AttributeID
			--		and pa.IsDeleted = 0
			--		and av.IsDeleted = 0
			--		and a.IsDeleted = 0
			--		and v.IsDeleted = 0
			--	for xml path('')), 1, 1, '')
	)
end