use Jaguar_v2_Prototype2_BHS_2
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfGetListingActivity') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfGetListingActivity
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160803
-- ============================================================
go
create function dbo.sfGetListingActivity (@ListingID int, @ActivityID int)
returns date
as
begin
	return (select ActivityDateTime from dbo.tbListingActivity where ListingID = @ListingID and ActivityID = @ActivityID)
end