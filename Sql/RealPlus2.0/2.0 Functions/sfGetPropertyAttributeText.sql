use Jaguar_v2_WiP
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfGetPropertyAttributeText') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfGetPropertyAttributeText
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160322
-- ============================================================
go
create function dbo.sfGetPropertyAttributeText (@PropertyID int, @AttributeID int)
returns varchar(100)
as
begin
   return (select AttributeValue from dbo.tbPropertyAttributeText where PropertyID = @PropertyID and AttributeID = @AttributeID)
end