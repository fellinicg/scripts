set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfConvertToFtsContains') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfConvertToFtsContains
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20161026
-- ============================================================
go
create function dbo.sfConvertToFtsContains (@SearchCondition varchar(max), @Operator varchar(10))
returns varchar(max)
as
begin
	return ('"' + replace(@SearchCondition, ' ', '" ' + @Operator + ' "') + '"')
end