use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tfGetListingByOmni') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.tfGetListingByOmni
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160415
-- ============================================================
create function dbo.tfGetListingByOmni (@Omni varchar(max))
returns @TblReturn table (ListingID int)
as
begin
	-- Declarations
	declare @TblPerson table (PersonID int index ix1 clustered
							, PersonAddressID int index ix2 nonclustered
							, PersonEmailID int index ix3 nonclustered
							, PersonPhoneID int index ix4 nonclustered)

	-- Search tbPerson and return PersonID list
	insert into @TblPerson
	select 
		p.PersonID
		, ap.PersonAddressID
		, ep.PersonEmailID
		, pp.PersonPhoneID
	from dbo.tbPerson p
		left join dbo.tbPersonAddress ap on p.PersonID = ap.PersonID
		left join dbo.tbPersonEmail ep on p.PersonID = ep.PersonID
		left join dbo.tbPersonPhone pp on p.PersonID = pp.PersonID
	where charindex(@Omni, p.FullName) > 0
		or charindex(@Omni, p.FirstGivenName) > 0
		or charindex(@Omni, p.FamilyName) > 0
		or charindex(@Omni, p.SecondGivenName) > 0

	-- Return table
	insert into @TblReturn
	-- Search Agent
	select al.ListingID
	from @TblPerson p
		inner join dbo.tbAgentListing al on p.PersonID = al.AgentID
	union
	-- Search Listing Address Contacts
	select lca.ListingID
	from @TblPerson p
		inner join dbo.tbListingContactAddress lca on p.PersonEmailID = lca.PersonAddressID
	union
	-- Search Listing Email Contacts
	select lce.ListingID
	from @TblPerson p
		inner join dbo.tbListingContactEmail lce on p.PersonEmailID = lce.PersonEmailID
	union
	-- Search Listing Phone Contacts
	select lcp.ListingID
	from @TblPerson p
		inner join dbo.tbListingContactPhone lcp on p.PersonEmailID = lcp.PersonPhoneID
	union
	-- Search Company
	select ListingID
	from dbo.tfGetListingByCompany(@Omni)

	return
end