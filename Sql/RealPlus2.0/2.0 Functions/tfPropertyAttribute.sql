use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tfPropertyAttribute') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.tfPropertyAttribute
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160323
-- ============================================================
create function dbo.tfPropertyAttribute (@AttributeID int)
returns table 
as
return 
	select pa.PropertyID, v.ValueName, av.AttributeValueID
	from dbo.tbAttributeValue av
		inner join dbo.tbValue v on av.ValueID = v.ValueID
		inner join dbo.tbPropertyAttribute pa on av.AttributeValueID = pa.AttributeValueID
	where av.AttributeID = @AttributeID