use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tfPropertyAttribute') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.tfPropertyAttribute
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160321
-- ============================================================
create function dbo.tfPropertyAttribute (@AttributeName varchar(200))
returns @ResultTable table (
								PropertyID int
								, AttributeCode varchar(50)
								, AttributeName varchar(100)
								, AttributeValue varchar(200)
							)
with schemabinding
as
begin

	insert @ResultTable(PropertyID, AttributeCode, AttributeName, AttributeValue)
	select pan.PropertyID, a.AttributeCode, a.AttributeName, convert(varchar(200), pan.AttributeValue)
	from dbo.tbPropertyAttributeNumeric pan 
	inner join dbo.tbAttribute a on pan.AttributeID = a.AttributeID
	where pan.IsDeleted = 0 and a.IsDeleted = 0
	and a.AttributeName = @AttributeName
	union
	select pat.PropertyID, a.AttributeCode, a.AttributeName, pat.AttributeValue
	from dbo.tbPropertyAttributeText pat 
	inner join dbo.tbAttribute a on pat.AttributeID = a.AttributeID
	where pat.IsDeleted = 0 and a.IsDeleted = 0
	and a.AttributeName = @AttributeName

	return

end
go