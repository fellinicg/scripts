set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfToCSV') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfToCSV
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160329
-- ============================================================
go
create function dbo.sfToCSV (@xml xml)
returns varchar(max)
as
begin
	declare @csv varchar(max)

	select @csv = coalesce(@csv + ',', '') + c.value('@value[1]', 'varchar(max)') from @xml.nodes('/row') as tbl(c)

	return @csv
end