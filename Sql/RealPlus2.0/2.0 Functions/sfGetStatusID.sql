use Jaguar_v2_WiP
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfGetStatusID') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfGetStatusID
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160315
-- ============================================================
go
create function dbo.sfGetStatusID(@StatusName varchar(100))
returns int
as
begin
   return (select AttributeValueID from dbo.vwAttributeValues where AttributeName = 'Status' and ValueName = @StatusName)
end