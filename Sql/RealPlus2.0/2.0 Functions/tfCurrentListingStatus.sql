use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tfCurrentListingStatus') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.tfCurrentListingStatus
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160321
-- ============================================================
create function dbo.tfCurrentListingStatus ()
returns table 
as
return
	select distinct ListingID, max(StatusDateTime) StatusDateTime 
	from dbo.tbListingStatus 
	group by ListingID