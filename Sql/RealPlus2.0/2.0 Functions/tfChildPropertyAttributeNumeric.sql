use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tfChildPropertyAttributeNumeric') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.tfChildPropertyAttributeNumeric
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160322
-- ============================================================
create function dbo.tfChildPropertyAttributeNumeric (@AttributeID int)
returns table 
as
return 
	select l.ListingID, pan.AttributeValue
	from dbo.tbPropertyAttributeNumeric pan
		inner join dbo.tbListing l on pan.PropertyID = l.PropertyID
	where pan.AttributeID = @AttributeID
		and l.IsDeleted = 0
		and pan.IsDeleted = 0
