use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfGetListingSoldPrice') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfGetListingSoldPrice
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160421
-- ============================================================
go
create function dbo.sfGetListingSoldPrice (@ListingID int)
returns float
as
begin
   return (select case CurrentStatus when 'sold' then CurrentPrice else null end 'SoldPrice'
			from dbo.tbListing
			where ListingID = @ListingID)
end