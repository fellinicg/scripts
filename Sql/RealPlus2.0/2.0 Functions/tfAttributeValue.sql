use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tfAttributeValue') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.tfAttributeValue
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160321
-- ============================================================
create function dbo.tfAttributeValue ()
returns table 
as
return

	select av.AttributeValueID, av.AttributeID, v.ValueName, av.IsDeleted
	from dbo.tbAttributeValue av
		inner join dbo.tbValue v on av.ValueID = v.ValueID