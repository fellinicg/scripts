use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tfGetListingByAgent') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.tfGetListingByAgent
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160415
-- ============================================================
create function dbo.tfGetListingByAgent (@Omni varchar(max))
returns table
as
return
	select al.ListingID
	from dbo.tbPerson p
		inner join dbo.tbAgentListing al on p.PersonID = al.AgentID
	where charindex(@Omni, p.FullName) > 0
		or charindex(@Omni, p.FirstGivenName) > 0
		or charindex(@Omni, p.FamilyName) > 0
		or charindex(@Omni, p.SecondGivenName) > 0