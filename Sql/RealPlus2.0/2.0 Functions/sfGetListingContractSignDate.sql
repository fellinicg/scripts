use Jaguar_v2_WiP
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfGetListingContractSignDate') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfGetListingContractSignDate
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160421
-- ============================================================
go
create function dbo.sfGetListingContractSignDate (@ListingID int)
returns datetime2
as
begin
   return (	select top 1 StatusDateTime
			from dbo.tbListingStatus
			where ListingID = @ListingID
			order by StatusDateTime desc)
end