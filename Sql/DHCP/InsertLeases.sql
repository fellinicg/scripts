truncate table dbo.tbDhcpLease

insert into dbo.tbDhcpLease(ScopeID, HostName, IPAddress, MacAddress)
select 
	b.ScopeID
	, a.HostName
	, a.Address
	, replace(a.MAC_Address, '1,6,', '') MAC_Address
from openrowset('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=C:\Feeds\DHCP Leases.xlsx', 'select * from [sheet1$]') a
inner join dbo.tbDhcpScope b on a.Scope = b.ScopeName
