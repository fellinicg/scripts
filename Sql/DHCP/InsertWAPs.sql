insert into dbo.tbWirelessAccessPoint(WirelessAccessPointName, MacAddress, IPAddress, ScopeID)
select 
	a.[AP Name]
	, a.[Ethernet MAC Address]
	, a.[IP Address/DNS]
	, b.ScopeID
from  openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [WirelessAccessPoints.csv]') a
left join dbo.tbdhcpscope b on b.Octet1 = parsename(a.[IP Address/DNS], 4)
	and b.Octet2 = parsename(a.[IP Address/DNS], 3)
	and parsename(a.[IP Address/DNS], 2) between b.LowerOctet3 and b.UpperOctet3
	and parsename(a.[IP Address/DNS], 1) between b.LowerOctet4 and b.UpperOctet4
left join dbo.tbWirelessAccessPoint c on a.[Ethernet MAC Address] = c.MacAddress
where c.MacAddress is null