insert into dbo.tbDhcpScope (ScopeName, Subnet, Groups, Octet1, Octet2, LowerOctet3, UpperOctet3, LowerOctet4, UpperOctet4)
select 
	a.ScopeName
	, a.Subnet
	, a.Groups
	, a.Octet1
	, a.Octet2
	, a.Octet3 LowerOctet3
	, case 
		when a.Groups < 24 then
			case a.Groups
				when 23 then a.Octet3 + 1
				when 22 then a.Octet3 + 3
				when 21 then a.Octet3 + 7
			end 
		else Octet3
	  end UpperOctet3
	, a.Octet4 + 1 LowerOctet4
	, case a.Groups
		when 22 then 254
		when 23 then 254
		when 24 then 254
		when 25 then a.Octet4 + 126
		when 26 then a.Octet4 + 62
		when 27 then a.Octet4 + 30
		when 28 then a.Octet4 + 14
		when 29 then a.Octet4 + 6
		when 30 then a.Octet4 + 2
	  end UpperOctet4
from (	
		select 
			replace(Name, char(160), '') ScopeName
			, replace(Subnet, char(160), '') Subnet
			, right(replace(Subnet, char(160), ''), 2) Groups
			, parsename(Subnet, 4) + '.' + parsename(Subnet, 3) + '.' + parsename(Subnet, 2) Network
			, parsename(Subnet, 4) Octet1
			, parsename(Subnet, 3) Octet2
			, parsename(Subnet, 2) Octet3
			, left(parsename(Subnet, 1), charindex('/', parsename(Subnet, 1), 1) - 1) Octet4
		from openrowset('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=C:\Feeds\DHCP Subnets.xlsx', 'select * from [sheet1$]') a
	  ) a
left join dbo.tbDhcpScope b on a.ScopeName = b.ScopeName and a.Subnet = b.Subnet
where b.ScopeID is null