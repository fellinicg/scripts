select *
from  openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [WirelessAccessPoints.csv]')

--select *
--from openrowset('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=C:\Feeds\DHCP Leases.xlsx', 'select * from [sheet1$]')

--select 
--	Name
--	, Subnet
--	, Groups
--	--, Network
--	, Octet1
--	, Octet2
--	, Octet3 LowerOctet3
--	, case 
--		when Groups < 24 then
--			case Groups
--				when 23 then Octet3 + 1
--				when 22 then Octet3 + 3
--				when 21 then Octet3 + 7
--			end 
--		else Octet3
--	  end UpperOctet3
--	, Octet4 + 1 LowerOctet4
--	, case Groups
--		when 22 then 254
--		when 23 then 254
--		when 24 then 254
--		when 25 then Octet4 + 126
--		when 26 then Octet4 + 62
--		when 27 then Octet4 + 30
--		when 28 then Octet4 + 14
--		when 29 then Octet4 + 6
--		when 30 then Octet4 + 2
--	  end
--from (	select 
--			Name
--			, replace(Subnet, char(160), '') Subnet
--			, right(replace(Subnet, char(160), ''), 2) Groups
--			, parsename(Subnet, 4) + '.' + parsename(Subnet, 3) + '.' + parsename(Subnet, 2) Network
--			, parsename(Subnet, 4) Octet1
--			, parsename(Subnet, 3) Octet2
--			, parsename(Subnet, 2) Octet3
--			, left(parsename(Subnet, 1), charindex('/', parsename(Subnet, 1), 1) - 1) Octet4
--			--, left(parsename(Subnet, 1), charindex('/', parsename(Subnet, 1), 1) - 1) + 1 LowerHost
--		from openrowset('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=C:\Feeds\DHCP Subnets.xlsx', 'select * from [sheet1$]') a
--	  ) a

--order by groups

--cat 20170803-Leases.txt | sed 's/|/","/g' | sed 's/^/"/g' | sed 's/$/"/g' > Leases.txt
-- | sed -e 's/  */ /gp' | sed -e 's/\s/,/g'
--  /opt/nwreg2/local/usrbin/nrcmd -N tfellini scope listbrief | sed '/100 Ok/d' | sed '/---/d' | sed 's/[[:space:]]\+/ /g' | sed -e 's/\s/","/g' | sed 's/^/"/g' | sed 's/$/"/g' > scopes.csv
-- /opt/nwreg2/local/usrbin/nrcmd -N tfellini scope listbrief | sed '/100 Ok/d' | sed '/---/d' | sed 's/./,/33' | sed 's/./,/54' > scopes.csv

select IP
from (
	select F4 IP from  openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds;HDR=NO;', 'select * from [20170803-Leases.csv]')
	union
	select deviceIpAddress from  openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds;', 'select * from [20170803-ClientIPs.csv]')
	union
	select ipAddress from  openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds;', 'select * from [20170803-ClientIPs.csv]')
	) a
where charindex('::', IP) = 0
order by a.IP