USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071018
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071018
-- ============================================================
CREATE TRIGGER dbo.trgUpdateItineraryStatus ON  dbo.tbParticipantItineraryStatus 
	FOR INSERT 
AS 
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

  -- Update ItineraryStatus in tbParticipant
	UPDATE dbo.tbParticipant SET ItineraryStatus = Inserted.ParticipantItineraryStatusID
	FROM Inserted 
		INNER JOIN dbo.tbParticipant ON  Inserted.ParticipantID = dbo.tbParticipant.ParticipantID

END
GO
