use GHVMLS
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upExportHeader') and type in (N'P', N'PC'))
drop procedure dbo.upExportHeader
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20110805
-- ============================================================
create procedure dbo.upExportHeader
as
begin

	-- Initialize settings
	set nocount on

	-- Return header
	select dbo.sfDelimColumn('ColumnHeader', char(9)) --dbo.sfTabDelimFileHeader()
	
end