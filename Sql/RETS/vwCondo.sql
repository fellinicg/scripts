use GHVMLS
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.views where object_id = object_id(N'dbo.vwCondo'))
drop view dbo.vwCondo
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20110805
-- ============================================================
CREATE VIEW dbo.vwCondo
AS
	select 
		sysid
		,Property_1
--		,ZipCode_10
--		,StatusC_18
--		,null 'Col1'
--		,BookVol_22
--		,Area_40
--		,BA_44
--		,Bedroom_54
--		,CountyI_67
--		,Cumulat_68
--		,Contrac_74
--		,CloseDa_75
--		,StreetD_89
--		,DaysOnM_90
--		,EntryDa_98
--		,TermsO_114
--		,ImageC_134
--		,AgentN_146
--		,LastTr_149
--		,LastTr_150
--		,LAV_152
--		,Listin_193
--		,ListDa_194
--		,Listin_199
--		,ListPr_201
--		,Listin_203
--		,MLNumb_217
--		,Origin_234
--		,PhotoI_238
--		,Previo_253
--		,RDFSet_257
--		,Record_259
--		,SalePr_269
--		,SDESG_274
--		,Sellin_276
--		,Sellin_277
--		,SortPr_282
--		,Status_290
--		,Street_292
--		,Street_293
--		,Region_294
--		,TaxID_302
--		,YRBT_311
--		,AC_1_317
--		,ACCESS_318
--		,ADTLRM_320
--		,ADTLRM_321
--		,ADTLRM_322
--		,ADTLRM_323
--		,ADTLRM_324
--		,AMEN_1_325
--		,APPEAR_326
--		,APPTPH_328
--		,APT_329
--		,ASMT_D_333
--		,ATTIC__334
--		,BAC_335
--		,BACA2_337
--		,BATH1_339
--		,BATH2_340
--		,BATH3_341
--		,BATHL_342
--		,BATHO_343
--		,BEDPOS_344
	from dbo.tbCondo