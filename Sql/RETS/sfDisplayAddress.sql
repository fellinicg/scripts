use GHVMLS
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfDisplayAddress') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfDisplayAddress
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20110811
-- ============================================================
create function dbo.sfDisplayAddress(@sysid int)
returns varchar(max)
as
begin
	-- declare variables.
	declare 
		@result varchar(max)

	-- Initialize variables.
	set @result = ''

	-- Populate result variable.
	select @result = dbo.sfNullToEmpty(Street_293) + ' ' + dbo.sfNullToEmpty(Street_292)
		+ ' ' + dbo.sfNullToEmpty(SDESG_274)
	from dbo.vwAllProperty

	-- Return the result of the function
	return @result

end