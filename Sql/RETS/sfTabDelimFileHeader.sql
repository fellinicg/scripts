use GHVMLS
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfTabDelimFileHeader') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfTabDelimFileHeader
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20110805
-- ============================================================
create function dbo.sfTabDelimFileHeader()
returns varchar(max)
as
begin
	-- declare variables.
	declare @result varchar(max)
	declare @header table (field varchar(40))

	-- Initialize variables.
	set @result = ''

	-- Insert column headers into local table variable.
	insert into @header
	select ColumnHeader
	from dbo.tbOutputField
	order by Position

	-- Populate result variable.
	while (select count(field) from @header) > 0
		begin
			-- Assign value concatenated with tab to result variable.
			select top 1 @result = @result + field + char(9) 
			from @header
			
			-- Remove top row from table variable.
			delete from @header
			where field = (select top 1 field from @header)
		END

	-- Remove final tab.
	set @result = substring(@result, 1, len(@result)-1)

	-- Return the result of the function
	return @result

end
go

