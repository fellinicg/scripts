use GHVMLS
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfDelimColumn') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfDelimColumn
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20110808
-- ============================================================
create function dbo.sfDelimColumn
(
	@Column varchar(20)
	,@Delimiter char(1)
)
returns varchar(max)
as
begin
	-- declare variables.
	declare 
		@result varchar(max)
		,@idx int
	declare @temp table (idx int identity(1,1), col varchar(200))

	-- Initialize variables.
	set @result = ''

	-- Insert data into local table variable.
	if (@Column = 'ColumnHeader')
		insert into @temp
		select ColumnHeader	from dbo.tbOutputField order by Position
	if (@Column = 'CondoField')
		insert into @temp
		select CondoField	from dbo.tbOutputField order by Position
	if (@Column = 'ResField')
		insert into @temp
		select ResField	from dbo.tbOutputField order by Position
	if (@Column = 'MultiField')
		insert into @temp
		select MultiField	from dbo.tbOutputField order by Position
	
	-- Populate result variable.
	while exists (select 1 from @temp)
		begin
			-- Assign value concatenated with tab to result variable.
			select top 1 @result = @result + isnull(col, 'dbo.sfNullToEmpty(null) as ' + quotename(convert(varchar, idx), char(39))) + @Delimiter 
			from @temp order by idx
			
			-- Remove top row from table variable.
			delete from @temp
			where idx = (select top 1 idx from @temp order by idx)
		end

	-- Remove final tab.
	set @result = substring(@result, 1, len(@result)-len(@Delimiter))

	-- Return the result of the function
	return @result

end
go

