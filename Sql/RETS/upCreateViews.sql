use GHVMLS
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upCreateViews') and type in (N'P', N'PC'))
drop procedure dbo.upCreateViews
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20110808
-- ============================================================
create procedure dbo.upCreateViews
as
begin

	-- Declare variables
	declare @sql varchar(max)

	-- Initialize settings
	set nocount on

	-- Drop views
	if  exists (select * from sys.views where object_id = object_id(N'dbo.vwCondo'))
	drop view dbo.vwCondo
	if  exists (select * from sys.views where object_id = object_id(N'dbo.vwRes'))
	drop view dbo.vwRes
	if  exists (select * from sys.views where object_id = object_id(N'dbo.vwMulti'))
	drop view dbo.vwMulti
	
	-- Create vwCondo
	set @sql = 'create view dbo.vwCondo as select ' + dbo.sfDelimColumn('CondoField', ',') + ' from dbo.tbCondo'
	exec(@sql)

	-- Create vwRes
	set @sql = 'create view dbo.vwRes as select ' + dbo.sfDelimColumn('ResField', ',') + ' from dbo.tbRes'
	exec(@sql)

	-- Create vwMulti
	set @sql = 'create view dbo.vwMulti as select ' + dbo.sfDelimColumn('MultiField', ',') + ' from dbo.tbMulti'
	exec(@sql)
	
end