use Warren

declare 
	@dropcmd varchar(max)
	,@cmd varchar(max)
	,@file varchar(50)

set @dropcmd = 'if  exists (select * from sys.objects where object_id = object_id(N''dbo.tb$TABLE$'') and type in (N''U'')) drop table dbo.tb$TABLE$'
set @file = 'Temp'

set @cmd = replace(@dropcmd, '$TABLE$', @file)
exec (@cmd)