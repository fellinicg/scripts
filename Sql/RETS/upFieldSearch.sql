use GHVMLS
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upFieldSearch') and type in (N'P', N'PC'))
drop procedure dbo.upFieldSearch
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20110805
-- ============================================================
create procedure dbo.upFieldSearch(@SearchFor varchar(max))
as
begin

	-- Initialize settings
	set nocount on

	-- Return header
	select t.name, c.name, m.LongName
	from sys.columns c
		left join sys.tables t on c.object_id = t.object_id
		left join dbo.tbMasterField m on c.name = m.Field
	where c.name like '%' + @SearchFor + '%'
		or m.LongName like '%' + @SearchFor + '%'
	
end