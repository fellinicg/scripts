use GHVMLS
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfNullToZero') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfNullToZero
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20110811
-- ============================================================
go
create function dbo.sfNullToZero (@Input varchar(max))
returns int
as
begin
  
  return(isnull(@Input, 0))

end