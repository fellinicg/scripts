use GHVMLS
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.views where object_id = object_id(N'dbo.vwAllProperty'))
drop view dbo.vwAllProperty
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20110808
-- ============================================================
CREATE VIEW dbo.vwAllProperty
AS
	select * from dbo.vwCondo
	union
	select * from dbo.vwRes
	union
	select * from dbo.vwMulti		