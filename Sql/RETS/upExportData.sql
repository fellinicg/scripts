use GHVMLS
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upExportData') and type in (N'P', N'PC'))
drop procedure dbo.upExportData
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20110808
-- ============================================================
create procedure dbo.upExportData
as
begin

	-- Return data
	select * from dbo.vwAllProperty
	
end