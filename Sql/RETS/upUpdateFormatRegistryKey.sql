use GHVMLS
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upUpdateFormatRegistryKey') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateFormatRegistryKey
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20110805
-- ============================================================
create procedure dbo.upUpdateFormatRegistryKey
	@Value varchar(20)  -- CSVDelimited,TabDelimited
as
begin

	-- Initialize settings
	set nocount on

	-- Update registry
	exec master.sys.xp_instance_regwrite
		@rootkey = N'HKEY_LOCAL_MACHINE'
	 ,@key = N'Software\Microsoft\Jet\4.0\Engines\Text'
	 ,@value_name = N'Format'
	 ,@type = N'REG_SZ'
	 ,@value = @Value

end