use GHVMLS
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upImportData') and type in (N'P', N'PC'))
drop procedure dbo.upImportData
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20110805
-- ============================================================
create procedure dbo.upImportData
as
begin

	-- Initialize settings
	set nocount on
	
	-- Declarations
	declare @files table (FileID tinyint, InputFile varchar(100), Location varchar(500), Tbl varchar(100))
	declare
		@fileid tinyint
		,@file varchar(100)
		,@location varchar(500)
		,@table varchar(100)
		,@dropcmd varchar(max)
		,@inscmd varchar(max)
		,@cmd varchar(max)

	-- Initialize variables
	set @dropcmd = 'if exists(select * from sys.objects 
		where object_id = object_id(N''dbo.$TABLE$'') 
		and type in (N''U'')) 
		drop table dbo.$TABLE$'

	set @inscmd = 'select * into $TABLE$
		from (select * from openrowset(''Microsoft.Jet.OLEDB.4.0'',
		''Text;HDR=YES;FMT=Delimited;Database=$LOC$'',   
		''select * from $FILE$'')) as a'
		
	-- Set format to tab delimited
	exec dbo.upUpdateFormatRegistryKey 'TabDelimited'

	-- Insert a list of input files into table var
	insert into @files
	select FileID, Name + Extension, Location, 'tb' + Name
	from dbo.tbFile
	where Type = 'I'

	-- Loop over list of files
	while exists(select 1 from @files)
	begin
		-- Initialize
		select top 1 @fileid = FileID, @file = InputFile, @location = Location, @table = Tbl from @files
		-- Drop table
		set @cmd = replace(@dropcmd, '$TABLE$', @table)
		exec (@cmd)
		-- Insert data into table
		set @cmd = replace(replace(replace(@inscmd, '$TABLE$', @table),'$LOC$', @location),'$FILE$', @file)
		exec (@cmd)
		-- Clean up
		delete from @files where FileID = @fileid
	end

	-- Set format to comma delimited
	exec dbo.upUpdateFormatRegistryKey 'CSVDelimited'
	
end