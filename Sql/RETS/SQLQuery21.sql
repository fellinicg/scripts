select distinct AMEN_1_325 
from dbo.tbCondo
where AMEN_1_325 is not null
order by AMEN_1_325

select 
AMEN_1_325,
case when AMEN_1_325 = 'Elevators' then 'Yes' else 'No' end,
charindex('Elevators', AMEN_1_325, 1),
case when isnull(charindex('Elevators', AMEN_1_325, 1), 0) > 0 then 'Yes' else 'No' end as Elevator
from tbCondo