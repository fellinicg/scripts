use GHVMLS
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfTrim') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfTrim
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20110811
-- ============================================================
go
create function dbo.sfTrim (@Input varchar(4000))
returns varchar(max)
as
begin
  
  return(ltrim(rtrim(@Input)))

end