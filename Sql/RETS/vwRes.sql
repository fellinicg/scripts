use GHVMLS
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.views where object_id = object_id(N'dbo.vwRes'))
drop view dbo.vwRes
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20110805
-- ============================================================
CREATE VIEW dbo.vwRes
AS
	select 
		sysid
		,Property_1
--		,AC_1_2
--		,ZipCode_10
--		,StatusC_18
--		,Selling_20
--		,BookVol_22
--		,ACCESS__24
--		,ADTLRM1_26
--		,ADTLRM2_27
--		,ADTLRM3_28
--		,ADTLRML_29
--		,ADTLRMO_30
--		,APPEAR_33
--		,APPOINT_34
--		,APPTPH2_35
--		,APT_36
--		,Area_40
--		,ASMT_DO_41
--		,ATTIC_1_42
--		,BA_44
--		,BAC_45
--		,BACA2_47
--		,BATH1_49
--		,BATH2_50
--		,BATH3_51
--		,BATHL_52
--		,BATHO_53
--		,Bedroom_54
--		,BEDPOS_55
--		,BEG_LP_56
--		,BKR_57
--		,BKRPH_58
--		,BLK_59
--		,BR1_60
--		,BR2_61
--		,BR3_62
--		,BRL_63
--		,BRO_64
--		,BSMTD_1_66
--		,CountyI_67
--		,Cumulat_68
--		,COMP_TR_69
--		,CONCESS_70
--		,CONST_1_72
--		,CONT_73
--		,Contrac_74
--		,CloseDa_75
--		,DENOFF1_76
--		,DENOFF2_77
--		,DENOFF3_78
--		,DENOFFL_79
--		,DENOFFO_80
--		,DEP_81
--		,DINE_1_82
--		,DINING1_83
--		,DINING2_84
--		,DINING3_85
--		,DININGL_86
--		,DININGO_87
--		,DIR_88
--		,StreetD_89
--		,DaysOnM_90
--		,DP1_91
--		,DP2_92
	from dbo.tbRes