use GHVMLS

truncate table dbo.tbMasterField

insert into dbo.tbMasterField
select distinct a.*
from openrowset('Microsoft.Jet.OLEDB.4.0', 
	'Excel 8.0;Database=C:\Documents and Settings\tfellini\My Documents\RETS\masterfields.xls', 
	'SELECT * FROM [condo$]') as a
where a.field is not null
union 
select distinct a.*
from openrowset('Microsoft.Jet.OLEDB.4.0', 
	'Excel 8.0;Database=C:\Documents and Settings\tfellini\My Documents\RETS\masterfields.xls', 
	'SELECT * FROM [res$]') as a
where a.field is not null
union 
select distinct a.*
from openrowset('Microsoft.Jet.OLEDB.4.0', 
	'Excel 8.0;Database=C:\Documents and Settings\tfellini\My Documents\RETS\masterfields.xls', 
	'SELECT * FROM [multi$]') as a
where a.field is not null
order by Field
