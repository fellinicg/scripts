USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071018
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071018
-- ============================================================
ALTER TRIGGER [trgInitializeItineraryStatus] ON [dbo].[tbParticipant]
	FOR INSERT 
AS
BEGIN

		-- Declarations
	DECLARE @CurrID int
	DECLARE @ParticipantItineraryStatusID int

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @ParticipantItineraryStatusID = 0

  -- Handle multiple record inserts
  SELECT @CurrID = MIN(ParticipantID) FROM Inserted
  WHILE (ISNULL(@CurrID, 0) <> 0)
		BEGIN

			-- If the pax already has a record store its ID
			SELECT
				@ParticipantItineraryStatusID = MAX(ParticipantItineraryStatusID)
			FROM dbo.tbParticipantItineraryStatus 
			WHERE ParticipantID = @CurrID

			-- Either insert or update
			IF (ISNULL(@ParticipantItineraryStatusID, 0) = 0)
				INSERT INTO dbo.tbParticipantItineraryStatus (ParticipantID, ItineraryStatusCode)
				VALUES (@CurrID, 'P')
			ELSE
				UPDATE dbo.tbParticipant SET ItineraryStatus = @ParticipantItineraryStatusID
				WHERE ParticipantID = @CurrID
				
			-- Get next candidate
			SELECT @CurrID = MIN(ParticipantID) FROM Inserted WHERE ParticipantID > @CurrID
	    
		END

END
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

