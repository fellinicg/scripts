select n.Caption, n.MachineType, ah.TimeStamp
from SolarWindsOrion.dbo.AlertHistory ah
	inner join SolarWindsOrion.dbo.AlertObjects ao on ah.AlertObjectID = ao.AlertObjectID
	inner join SolarWindsOrion.dbo.Nodes n on ao.RelatedNodeId = n.NodeID
where ah.EventType = 0
	and charindex('Offline Node - Testing for reporting purposes ONLY', ah.Message) > 0
	and charindex('2003', n.MachineType) > 0
	--and (charindex('Pomeroy - Server - Reboot', ah.Message) > 0 or charindex('Pomeroy - Server - Offline Node - PANYNJ - IBM Director', ah.Message) > 0 or charindex('Offline Node - Testing for reporting purposes ONLY', ah.Message) > 0)
	--and ah.TimeStamp > '3-1-2016'
order by n.Caption, ah.Message, ah.TimeStamp

select n.Caption, n.MachineType, count(n.NodeID) 'Issues Since 3/1/2016'
from SolarWindsOrion.dbo.AlertHistory ah
	inner join SolarWindsOrion.dbo.AlertObjects ao on ah.AlertObjectID = ao.AlertObjectID
	inner join SolarWindsOrion.dbo.Nodes n on ao.RelatedNodeId = n.NodeID
where ah.EventType = 0
	and charindex('Offline Node - Testing for reporting purposes ONLY', ah.Message) > 0
	and charindex('2003', n.MachineType) > 0
	--and (charindex('Pomeroy - Server - Reboot', ah.Message) > 0 or charindex('Pomeroy - Server - Offline Node - PANYNJ - IBM Director', ah.Message) > 0 or charindex('Offline Node - Testing for reporting purposes ONLY', ah.Message) > 0)
	--and ah.TimeStamp > '3-1-2016'
group by n.Caption, n.MachineType
order by n.Caption