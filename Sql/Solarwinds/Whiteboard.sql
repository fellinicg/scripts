select 
s.*
from dbo.vwSwitchPort s
inner join dbo.vwSolarwindsIP i on s.Sysname = i.Name
where IPAddress in (
'12.1.49.9'
,'12.1.50.9'
,'206.137.235.189'
,'206.137.235.126'
,'65.254.128.40'
)
and connecteddevice is not null
order by s.Sysname



select
	i.IPAddress
	, isnull(i.SubnetMask, '') SubnetMask
	, isnull(n.Slash, '') Slash
from TELEDB48.SolarWindsOrion.dbo.NodeIPAddresses i
left join openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\netmask.xlsx', 'select * from [Sheet1$]') n on i.SubnetMask = n.NetMask
where 
	charindex('::', i.IPAddress) = 0
	and
	charindex('169.', i.IPAddress) <> 1
order by i.IPAddress


--select ipAddress from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds\', 'select * from 20170803-ClientIPs.csv')
--union
--select deviceIpAddress from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds\', 'select * from 20170803-ClientIPs.csv')
--union
--select F4 from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds\;HDR=No', 'select * from 20170803-Leases.csv')


--select * from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\netmask.xlsx', 'select * from [Sheet1$]') n