use AD_EventLog
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects WHERE object_id = OBJECT_ID(N'dbo.upImportMachineLog') AND type in (N'P', N'PC'))
drop procedure dbo.upImportMachineLog
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20130129
-- ============================================================
create procedure dbo.upImportMachineLog
as
begin

	-- Declare variables
	declare @tbServers table (ServerName varchar(50))
	declare @currServer varchar(50)
	declare @sql varchar(max)
	declare @union varchar(max)
	declare @select varchar(max)
	declare @path varchar(50)
	declare @ts varchar(15)
	declare @result int
	declare @file varchar(200)
	
	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @sql = 'select *, current_timestamp CreateDateTime into tbMachineImport from ($TEMP$) a'
	set @path = 'D:\EventLogOutput' --'D:\EventLogOutput\Archive'
	set @ts = '' --'2013013011-'
	set @select = 'select distinct replace(replace(UserName, ''PANYNJ\'', ''''), ''$'', '''') UserName, IP, case when charindex(''$'', UserName, 1) > 0 then ''M'' else ''U'' end Type, 
					TimeGenerated from openrowset(''Microsoft.ACE.OLEDB.12.0'',
					''Text;Database=' + @path + ''',''select UserName, TimeGenerated, IP from [' + @ts + '$SERVER$-machine.log]'')'
	set @union = ''	
	insert into @tbServers select * from dbo.tfDelimListToTable('PATCIS22,PATCIS23,TELEIS29,TELEIS30', ',')

	-- Drop tbMachineImport
	If  exists (select * from sys.objects where object_id = object_id(N'dbo.tbMachineImport') and type in (N'U'))
	drop table dbo.tbMachineImport

	-- Create SQL
	while exists(select 1 from @tbServers)
	begin
		select top 1 @currServer = ServerName from @tbServers order by ServerName
		set @file = @path + '\' + @ts + @currServer + '-machine.log'
		exec master.dbo.xp_fileexist @file, @result output
		if @result = 1
		begin
			set @union = @union + replace(@select, '$SERVER$', @currServer)
			if (select count(1) from @tbServers) > 1 set @union = @union + ' union '
		end
		delete from @tbServers where ServerName = @currServer
	end
		
	-- Execut query to insert data into tbMachineImport
	set @sql = replace(@sql, '$TEMP$', @union)
	exec(@sql)
			
	-- Insert new data into tbMachine
	insert into dbo.tbMachine
	select distinct src.UserName, src.TimeGenerated, src.CreateDateTime
	from tbMachineImport src
	left join dbo.tbMachine dest on src.UserName = dest.Machine
	where src.Type = 'M' and src.UserName is not null and dest.Machine is null
	
	-- Insert new data into tbMachineIP
	insert into dbo.tbMachineIP
	select distinct src.UserName, src.IP, src.CreateDateTime
	from dbo.tbMachineImport src
	left join dbo.tbMachineIP dest on src.UserName = dest.Machine and src.IP = dest.IPAddress
	where src.Type = 'M' and src.IP <> '-' and src.UserName is not null and dest.Machine is null
	
	-- Insert new data into tbUserIP
	insert into dbo.tbUserIP
	select distinct src.UserName, src.IP, src.CreateDateTime
	from dbo.tbMachineImport src
	left join dbo.tbUserIP dest on src.UserName = dest.UserName and src.IP = dest.IPAddress
	where src.Type = 'U' and src.IP <> '-' and src.UserName is not null and dest.UserName is null

	-- Update LastActivity field in tbMachine
	update dbo.tbMachine set LastActivity = convert(datetime, src.TimeGenerated)
	from dbo.tbMachineImport src
	inner join dbo.tbMachine dest on src.UserName = dest.Machine
	where src.TimeGenerated > dest.LastActivity

end
go