select distinct a.Name, isnull(a.IPAddress, a.Name) IPAddress
from openrowset('Microsoft.ACE.OLEDB.12.0','Text;Database=d:\EventLogOutput','select * from [expDB.csv]') a
left join AN6.dbo.Computers b on a.Name = case charindex('.', b.Name) when 0 then b.Name else left(b.Name, charindex('.', b.Name) - 1) end 
	or 'D-' + a.Name = case charindex('.', b.Name) when 0 then b.Name else left(b.Name, charindex('.', b.Name) - 1) end 
left join dbo.tbMachine c on a.Name = c.Machine
where b.Name is null and c.Machine is null