use AD_EventLog
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfPossibleMatches') and type in (N'fn', N'if', N'tf', N'fs', N'ft'))
drop function dbo.sfPossibleMatches
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20130219
-- ============================================================
create function dbo.sfPossibleMatches (@name varchar(100))
returns varchar(max)
as
begin

declare @tbl table (col varchar(100))
declare @ret varchar(max) = ''
declare @col varchar(100) = ''

if charindex('-', @name) = 2 set @name = substring(@name, 3, 97)

insert into @tbl
select Name
from AN6.dbo.Computers
where charindex(@name, Name) > 0 

while exists (select 1 from @tbl)
begin
	select top 1 @col = col from @tbl
	if (len(@ret) = 0) set @ret = @col
	else set @ret = @ret + ', ' + @col
	delete from @tbl where col = @col
end

return @ret

end