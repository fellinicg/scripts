use AD_EventLog
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.views where object_id = object_id(N'dbo.vwMissingInAlloy2'))
drop view dbo.vwMissingInAlloy2
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20130219
-- ============================================================
create view dbo.vwMissingInAlloy2
as
	select distinct 
		b.Machine
		,b.LastActivity
		,b.CreateDateTime
	from AN6.dbo.Computers a
	right join dbo.tbMachine b on 
		case charindex('.', a.Name) when 0 then a.Name else left(a.Name, charindex('.', a.Name) - 1) end = b.Machine
		or case charindex('.', a.Name) when 0 then a.Name else left(a.Name, charindex('.', a.Name) - 1) end = case charindex('-', b.Machine) when 2 then substring(b.Machine, 3, 100) else b.Machine end
	where a.Name is null