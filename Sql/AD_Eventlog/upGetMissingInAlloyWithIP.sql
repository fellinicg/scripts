use AD_EventLog
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetMissingInAlloyWithIP') and type in (N'P', N'PC'))
drop procedure dbo.upGetMissingInAlloyWithIP
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20130130
-- ============================================================
create procedure dbo.upGetMissingInAlloyWithIP
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select distinct
		a.Machine
		,b.IPAddress
		,a.LastActivity
		,a.CreateDateTime
		,'ping -n 1 ' + a.Machine
		,'ping -n 1 ' + a.Machine + ' >> edis.log'
	from dbo.vwMissingInAlloy a
	left join dbo.tbMachineIP b on a.Machine = b.Machine
	order by a.Machine

end
