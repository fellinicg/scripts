use AD_EventLog
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects WHERE object_id = OBJECT_ID(N'dbo.upImportDnsInfo') AND type in (N'P', N'PC'))
drop procedure dbo.upImportDnsInfo
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20130207
-- ============================================================
create procedure dbo.upImportDnsInfo
as
begin

	-- Declare variables
	
	-- Initialize settings
	set nocount on

	-- Initialize variables

	-- Truncate tbDnsInfo
	truncate table dbo.tbDnsInfo

	-- Insert new data into tbDnsInfo
	insert into dbo.tbDnsInfo
	select distinct a.entry, a.zone,a.ip, t.mac, a.ts, t.ts
	from openrowset('Microsoft.ACE.OLEDB.12.0','Text;Database=D:\EventLogOutput','select * from [dns-a.txt]') a
	left join openrowset('Microsoft.ACE.OLEDB.12.0','Text;Database=D:\EventLogOutput','select * from [dns-txt.txt]') t
		on a.entry = t.entry
	where isnull(a.entry, '') <> ''

end
go