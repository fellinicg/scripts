use AD_EventLog
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects WHERE object_id = OBJECT_ID(N'dbo.upImportUserMachineLog') AND type in (N'P', N'PC'))
drop procedure dbo.upImportUserMachineLog
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20130129
-- ============================================================
create procedure dbo.upImportUserMachineLog
as
begin

	-- Declare variables
	declare @tbServers table (ServerName varchar(50))
	declare @currServer varchar(50)
	declare @sql varchar(max)
	declare @union varchar(max)
	declare @select varchar(max)
	declare @path varchar(50)
	declare @ts varchar(15)
	declare @result int
	declare @file varchar(200)
	
	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @sql = 'select *, current_timestamp CreateDateTime into tbUserMachineImport from ($TEMP$) a'
	set @path = 'D:\EventLogOutput' --\Archive'
	set @ts = '' --'2013013014-'
	set @select = 'select distinct UserName, Machine, TimeGenerated from openrowset(''Microsoft.ACE.OLEDB.12.0'',
					''Text;Database=' + @path + ''',''select * from [' + @ts + '$SERVER$-user.log]'')'
	set @union = ''	
	insert into @tbServers select * from dbo.tfDelimListToTable('PATCIS22,PATCIS23,TELEIS29,TELEIS30', ',')
	
	-- Drop tbUserMachineImport
	If  exists (select * from sys.objects where object_id = object_id(N'dbo.tbUserMachineImport') and type in (N'U'))
	drop table dbo.tbUserMachineImport

	-- Create SQL
	while exists(select 1 from @tbServers)
	begin
		select top 1 @currServer = ServerName from @tbServers order by ServerName
		set @file = @path + '\' + @ts + @currServer + '-user.log'
		exec master.dbo.xp_fileexist @file, @result output
		if @result = 1
		begin
			set @union = @union + replace(@select, '$SERVER$', @currServer)
			if (select count(1) from @tbServers) > 1 set @union = @union + ' union '
		end
		delete from @tbServers where ServerName = @currServer
	end
		
	-- Execut query to insert data into tbUserMachineImport
	set @sql = replace(@sql, '$TEMP$', @union)
	exec(@sql)

	-- Insert new data into tbUserMachine
	insert into dbo.tbUserMachine
	select distinct replace(src.UserName, 'PANYNJ\', ''), isnull(src.Machine, ''), src.TimeGenerated, src.CreateDateTime
	from tbUserMachineImport src
	left join dbo.tbUserMachine dest on replace(src.UserName, 'PANYNJ\', '') = dest.UserName and isnull(src.Machine, '') = isnull(dest.Machine, '')
	where dest.UserName is null and charindex('$', src.UserName, 1) = 0
	
	-- Update LastActivity field in tbUserMachine
	update dbo.tbUserMachine set LastActivity = convert(datetime, src.TimeGenerated)
	from tbUserMachineImport src
	inner join dbo.tbUserMachine dest on src.UserName = dest.UserName and isnull(src.Machine, '') = isnull(dest.Machine, '')
	where src.TimeGenerated > dest.LastActivity

end
go