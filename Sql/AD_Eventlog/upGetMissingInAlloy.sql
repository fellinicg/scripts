use AD_EventLog
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetMissingInAlloy') and type in (N'P', N'PC'))
drop procedure dbo.upGetMissingInAlloy
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20130130
-- ============================================================
create procedure dbo.upGetMissingInAlloy
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select distinct
		Machine
		,dbo.sfPossibleMatches(Machine) 'Possible Match'
		,LastActivity
		,CreateDateTime
		,'call pinghosts ' + Machine
		,'ping -n 1 ' + Machine + ' >> edis.log'
	from dbo.vwMissingInAlloy
	order by CreateDateTime desc

end
