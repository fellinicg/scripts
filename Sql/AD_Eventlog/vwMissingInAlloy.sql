use AD_EventLog
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.views where object_id = object_id(N'dbo.vwMissingInAlloy'))
drop view dbo.vwMissingInAlloy
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20130130
-- ============================================================
create view dbo.vwMissingInAlloy
as
	select distinct 
		b.Machine
		,b.LastActivity
		,b.CreateDateTime
	from AN6.dbo.Computers a
	right join dbo.tbMachine b on case charindex('.', a.Name) when 0 then a.Name else left(a.Name, charindex('.', a.Name) - 1) end = b.Machine
	where a.Name is null
