-- Clean Description field
--

-- Copy Description to UDF_Support_Status
--
begin tran
update dbo.Computers set UDF_Support_Status = Description
where 
	charindex('PMRY', Description collate Latin1_General_CS_AS) > 0
	or
	charindex('Decomm', Description collate Latin1_General_CS_AS) > 0
--where Type_ID in (
--	'629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3' --Server
----	,'E00622AC-074D-4F49-9654-8CEA4FC56C4E' --Server ICS
--	,'B37E1387-CF14-4562-930D-DD48D5F1837A' --VM Server
--	)
--and Description is not null
commit tran

-- Clear Description field
--
begin tran
update dbo.Computers set Description = null
where 
	charindex('PMRY', Description collate Latin1_General_CS_AS) > 0
	or
	charindex('Decomm', Description collate Latin1_General_CS_AS) > 0
--where Type_ID in (
--	'629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3' --Server
----	,'E00622AC-074D-4F49-9654-8CEA4FC56C4E' --Server ICS
--	,'B37E1387-CF14-4562-930D-DD48D5F1837A' --VM Server
--	)
--and Description is not null
commit tran

-- Copy UDF_MajorApplications to Description
--
begin tran
update dbo.Computers set Description = UDF_MajorApplications
where 
--	Type_ID in (
--	'629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3' --Server
----	,'E00622AC-074D-4F49-9654-8CEA4FC56C4E' --Server ICS
--	,'B37E1387-CF14-4562-930D-DD48D5F1837A' --VM Server
--	)
--and 
	Description is null
and 
	UDF_MajorApplications is not null 
commit tran

-- Check
--
select 
	Description
	, UDF_Support_Status
	, UDF_MajorApplications
from dbo.Computers
where Type_ID in (
	'629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3' --Server
--	,'E00622AC-074D-4F49-9654-8CEA4FC56C4E' --Server ICS
	,'B37E1387-CF14-4562-930D-DD48D5F1837A' --VM Server
	)
--and Description is null
and UDF_MajorApplications is not null 