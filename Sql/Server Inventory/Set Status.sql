-- Development
--
select 
	Name
	, Description
	, Status_ID
	, sTATUS
	, UDF_ICS
from dbo.Computers
inner join dbo.Status on Status_ID = dbo.Status.ID
where
	Type_ID = '629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3' --Server
	and
	charindex('dev', Description) > 0
	and
	charindex('device', Description) = 0
	and
	Status_ID <> '3417FB44-7DF7-4C9F-9A25-AAB4CC4C43E6'


-- Test
--
select 
	Name
	, Description
	, Status_ID
	, sTATUS
	, UDF_ICS
from dbo.Computers
inner join dbo.Status on Status_ID = dbo.Status.ID
begin tran
update dbo.Computers set Status_ID = '7355EBAF-25B4-48F0-9937-F0DE1F131A47'
where
	Type_ID = '629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3' --Server
	and
	charindex('test', Description) > 0
	--and
	--charindex('device', Description) = 0
	and
	Status_ID <> '3417FB44-7DF7-4C9F-9A25-AAB4CC4C43E6'
--commit tran