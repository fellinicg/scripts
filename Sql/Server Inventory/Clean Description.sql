-- Remove VM from Description field
--
begin tran
update dbo.Computers set Description = replace(Description, 'VM ', '')
where Type_ID in (
	'629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3' --Server
	,'B37E1387-CF14-4562-930D-DD48D5F1837A' --VM Server
	)
and Description is not null
and charindex('VM ', Description collate Latin1_General_CS_AS) > 0
--commit tran

-- Check
--
select 
	Description
	, replace(Description, 'VM ', '')
from dbo.Computers
where Type_ID in (
	'629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3' --Server
--	,'E00622AC-074D-4F49-9654-8CEA4FC56C4E' --Server ICS
	,'B37E1387-CF14-4562-930D-DD48D5F1837A' --VM Server
	)
and Description is not null
and charindex('VM ', Description collate Latin1_General_CS_AS) > 0