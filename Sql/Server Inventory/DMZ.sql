select 
--distinct ConnectedDevice
*
from (
		select 
			s.NodeID
			, s.Sysname
			, s.Switchport
			, s.InterfaceName
			, s.StatusLED
			, s.ConnectedMAC
			, s.ConnectedDevice
		from dbo.vwSwitchPort s
		inner join dbo.vwSolarwindsIP i on s.Sysname = i.Name
		where IPAddress in (
		'12.1.49.9'
		,'12.1.50.9'
		,'206.137.235.189'
		,'206.137.235.126'
		,'65.254.128.40'
		)
		and ConnectedDevice is not null
		union
		select distinct
			s.NodeID
			, s.Sysname
			, s.Switchport
			, s.InterfaceName
			, s.StatusLED
			, s.ConnectedMAC
			, m.Machine
		from dbo.vwAllMacs m
		right join (
					select s.*
					from dbo.vwSwitchPort s
					inner join dbo.vwSolarwindsIP i on s.Sysname = i.Name
					where IPAddress in (
					'12.1.49.9'
					,'12.1.50.9'
					,'206.137.235.189'
					,'206.137.235.126'
					,'65.254.128.40'
					)
					and ConnectedDevice is null) s on m.MACAddress = s.ConnectedMAC
) a
--where a.ConnectedDevice is not null
--order by a.Sysname, a.ConnectedMAC