begin tran
update dbo.Computers set UDF_ICS = 'Yes'
from dbo.Computers c
	inner join dbo.Object_Type o on c.Type_ID = o.ID
where
	charindex('ICS', o.Type collate Latin1_General_CS_AS) > 0
commit tran

select 
	c.UDF_ICS
	, o.Type
	,*
from dbo.Computers c
	inner join dbo.Object_Type o on c.Type_ID = o.ID
where
	charindex('ICS', o.Type collate Latin1_General_CS_AS) > 0