-- Check
--
--select 
--	UDF_PMRY_Support
--	, UDF_Support_Status
select 
	distinct UDF_Support_Status
from dbo.Computers
where
	UDF_Support_Status is not null

-- Server
--
begin tran
update dbo.Computers set UDF_PMRY_Support = 'Server'
where
	UDF_Support_Status in (
	'PMRY-Unx'
	, 'PMRY-P'
	, 'PMRY-VM'
	)
--commit tran

-- Hardware Only
--
select 
	UDF_PMRY_Support
	, UDF_Support_Status
from dbo.Computers
begin tran
update dbo.Computers set UDF_PMRY_Support = 'Hardware Only'
where
	UDF_Support_Status in (
	'PMRY-P-HWO'
	)
--commit tran

-- None
--
select 
Name
	, UDF_PMRY_Support
	, UDF_Support_Status
from dbo.Computers
begin tran
update dbo.Computers set UDF_PMRY_Support = 'None'
where
	charindex('Non-PMRY', UDF_Support_Status collate Latin1_General_CS_AS) > 0
	and
	UDF_PMRY_Support is null
--commit tran



