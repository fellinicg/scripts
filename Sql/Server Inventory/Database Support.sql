-- Check
--
select
	a.ServerName
	, c.Name
	, c.UDF_PMRY_Support
from 
(select distinct case when charindex('\', [Instance Name]) > 0 then left([Instance Name], charindex('\', [Instance Name]) - 1) else [Instance Name] end 'ServerName' from openrowset('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=C:\Feeds\databases.xlsx', 'select * from [sheet1$]') a) a
inner join dbo.Computers c on a.ServerName = c.Name
where UDF_PMRY_Support = 'Server'

select
	a.ServerName
	, c.Name
	, c.UDF_PMRY_Support
	, c.UDF_Support_Status
from (
		select srv1 'ServerName' from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [xcdb.txt]') a
		union
		select srv2 from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [xcdb.txt]') a
) a
left join dbo.Computers c on a.ServerName = c.Name
where c.Status_ID <> '3417FB44-7DF7-4C9F-9A25-AAB4CC4C43E6'

-- Database Only
--
begin tran
update dbo.Computers set UDF_PMRY_Support = 'Database Only'
from 
(select distinct case when charindex('\', [Instance Name]) > 0 then left([Instance Name], charindex('\', [Instance Name]) - 1) else [Instance Name] end 'ServerName' from openrowset('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=C:\Feeds\databases.xlsx', 'select * from [sheet1$]') a) a
inner join dbo.Computers c on a.ServerName = c.Name
where UDF_PMRY_Support = 'None'
--commit tran

-- Database & Server
--
begin tran
update dbo.Computers set UDF_PMRY_Support = 'Database & Server'
from 
(select distinct case when charindex('\', [Instance Name]) > 0 then left([Instance Name], charindex('\', [Instance Name]) - 1) else [Instance Name] end 'ServerName' from openrowset('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=C:\Feeds\databases.xlsx', 'select * from [sheet1$]') a) a
inner join dbo.Computers c on a.ServerName = c.Name
where UDF_PMRY_Support = 'Server'
--commit tran

--ExpressCluster Database & Hardware
--
select
	a.ServerName
	, c.Name
	, c.UDF_PMRY_Support
	, c.UDF_Support_Status
begin tran
update dbo.Computers set UDF_PMRY_Support = 'Database & Hardware'
from (
		select srv1 'ServerName' from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [xcdb.txt]') a
		union
		select srv2 from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [xcdb.txt]') a
) a
left join dbo.Computers c on a.ServerName = c.Name
where c.Status_ID <> '3417FB44-7DF7-4C9F-9A25-AAB4CC4C43E6'
	and
	UDF_PMRY_Support = 'Hardware Only'

--ExpressCluster Database & Server
--
select
	a.ServerName
	, c.Name
	, c.UDF_PMRY_Support
	, c.UDF_Support_Status
begin tran
update dbo.Computers set UDF_PMRY_Support = 'Database & Server'
from (
		select srv1 'ServerName' from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [xcdb.txt]') a
		union
		select srv2 from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [xcdb.txt]') a
) a
left join dbo.Computers c on a.ServerName = c.Name
where c.Status_ID <> '3417FB44-7DF7-4C9F-9A25-AAB4CC4C43E6'
	and
	UDF_PMRY_Support = 'Server'