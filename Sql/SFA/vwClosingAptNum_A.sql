use loho
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.vwClosingAptNum_A') and type in (N'V'))
drop view dbo.vwClosingAptNum_A
go
create view dbo.vwClosingAptNum_A
as
select a.*
from dbo.tbStage1_A a
inner join (
select ClosingDate, AptNumber
from dbo.tbStage1_A
group by ClosingDate, AptNumber
having count(1) > 1) b on a.ClosingDate=b.ClosingDate and a.AptNumber=b.AptNumber