--insert into dbo.tbStage1_A (
--NumberOfAptsSold, AptNumber, ClosingDate, Size, ShareHoldersName, SellingPrice, FlipTax, 
--NumberOfShares, BaseMaintenance, Note, Broker, src_file
--)
--SELECT [NUMBER OF APTS# SOLD],[APT# NUMBER],[CLOSING DATE ],[SIZE],[SHAREHOLDER'S NAME],[SELLING PRICE]
--,[FLIP TAX],[NO# OF SHARES],[BASE MAINT#],[NOTE],[Broker],src_file
--FROM dbo.tbComp_Seward_xls_APT_A_1
--union 
--SELECT [NUMBER OF APTS# SOLD],[APT# NUMBER],[CLOSING DATE ],[SIZE],[SHAREHOLDER'S NAME],[SELLING PRICE]
--,[FLIP TAX],[NO# OF SHARES],[BASE MAINT#],[NOTE],[Broker],src_file
--FROM dbo.tbComp_Seward_xls_Date_A_1

--update uid and tab
update dbo.tbStage1_A set src_tab=b.src_tab, uid=b.uid
from dbo.tbStage1_A a
inner join dbo.tbComp_Seward_xls_APT_A_1 b on
isnull(ClosingDate, '')=isnull([CLOSING DATE ], '') and 
isnull(AptNumber, '')=isnull([APT# NUMBER], '') and isnull(a.Broker, '')=isnull(b.Broker, '') and 
isnull(ShareHoldersName, '')=isnull([SHAREHOLDER'S NAME], '') and 
isnull(a.Note, '')=isnull(b.Note, '') and isnull(a.Size, '')=isnull(b.Size, '')
where a.uid is null

update dbo.tbStage1_A set src_tab=b.src_tab, uid=b.uid
from dbo.tbStage1_A a
inner join dbo.tbComp_Seward_xls_Date_A_1 b on
isnull(ClosingDate, '')=isnull([CLOSING DATE ], '') and 
isnull(AptNumber, '')=isnull([APT# NUMBER], '') and isnull(a.Broker, '')=isnull(b.Broker, '') and 
isnull(ShareHoldersName, '')=isnull([SHAREHOLDER'S NAME], '') and 
isnull(a.Note, '')=isnull(b.Note, '') and isnull(a.Size, '')=isnull(b.Size, '')
where a.uid is null

