--select distinct
--ClosingDate, ShareHoldersName, AptNumber, Size, SellingPrice, BaseMaintenance, NumberOfShares, FlipTax, Buyer, Broker, NumberOfAptsSold, Note
--from dbo.tbStage1_A

select a.*
from dbo.tbStage1_A a
inner join (
select ClosingDate, AptNumber--, ShareHoldersName--, Size--, SellingPrice, BaseMaintenance, NumberOfShares, FlipTax--, Buyer, Broker--, NumberOfAptsSold--, Note
from dbo.tbStage1_A
group by ClosingDate, AptNumber--, ShareHoldersName--, Size--, SellingPrice, BaseMaintenance, NumberOfShares, FlipTax, Buyer, Broker--, NumberOfAptsSold--, Note
having count(1) > 1) b on a.ClosingDate=b.ClosingDate and a.AptNumber=b.AptNumber
order by a.AptNumber, a.ClosingDate


--select distinct
--AptNumber
--from dbo.tbStage1_A
--where AptNumber is not null