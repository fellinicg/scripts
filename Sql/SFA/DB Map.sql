use SFA

select 
	a.TABLE_NAME 'Table'
	,a.COLUMN_NAME 'Column'
	,a.IS_NULLABLE 'Nullable'
	,a.DATA_TYPE + case when a.CHARACTER_MAXIMUM_LENGTH is not null then '(' + convert(varchar, a.CHARACTER_MAXIMUM_LENGTH) + ')' else '' end 'Data Type'
from INFORMATION_SCHEMA.COLUMNS a
inner join INFORMATION_SCHEMA.TABLES b on a.TABLE_NAME = b.TABLE_NAME
where b.TABLE_TYPE = 'BASE TABLE'
order by a.TABLE_NAME, a.ORDINAL_POSITION