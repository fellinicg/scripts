use loho
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.vwDiffBroker_A') and type in (N'V'))
drop view dbo.vwDiffBroker_A
go
create view dbo.vwDiffBroker_A
as
select a.*
from dbo.tbStage1_A a
inner join (
select ClosingDate, AptNumber, ShareHoldersName, Size, SellingPrice, BaseMaintenance, NumberOfShares, FlipTax, Buyer--, Broker--, NumberOfAptsSold--, Note
from dbo.tbStage1_A
group by ClosingDate, ShareHoldersName, AptNumber, Size, SellingPrice, BaseMaintenance, NumberOfShares, FlipTax, Buyer--, Broker--, NumberOfAptsSold--, Note
having count(1) > 1) b on a.ClosingDate=b.ClosingDate and a.AptNumber=b.AptNumber