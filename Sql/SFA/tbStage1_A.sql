use loho
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[tbStage1_A]') and type in (N'U'))
drop table dbo.tbStage1_A
go
create table dbo.tbStage1_A(
	Stage1_A_ID int identity(1,1) not null,
	ClosingDate datetime null,
	ShareHoldersName varchar(255) null,
	AptNumber varchar(255) null,
	Size varchar(255) null,
	SellingPrice varchar(255) null,
	BaseMaintenance varchar(255) null,
	NumberOfShares varchar(255) null,
	FlipTax varchar(255) null,
	Buyer varchar(255) null,
	Broker varchar(255) null,
	NumberOfAptsSold varchar(255) null,
	Note varchar(255) null,
	uid uniqueidentifier null,
	src_file varchar(19) null,
	src_tab varchar(6) null,
	Duplicate bit constraint dftbStage1_ADuplicate default 0 
) on [primary]

--alter table dbo.tbStage1_A add constraint ucUID unique (uid)
