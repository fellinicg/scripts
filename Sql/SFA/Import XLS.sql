use Loho

declare 
	@path varchar(500)
	,@file varchar(100)
	,@tab varchar(100)
	,@table varchar(100)
	,@group char(1)
	
--set @path = 'C:\Documents and Settings\tfellini\My Documents\Miscellaneous\SFA\Loho\Comps\'
set @path = 'C:\Documents and Settings\tfellini\My Documents\Miscellaneous\SFA\Loho\Potential Sellers Lists\'
--set @path = 'C:\Documents and Settings\tfellini\My Documents\Miscellaneous\SFA\Loho\SalesForce Startup\'
set @file = 'Potential sellers.xls'
set @tab = 'Jacobs list'
set @group = 'B'
set @table = 'tb' + replace(replace(@file, ' ' , '_'), '.', '_') + '_' + replace(replace(@tab, ' ' , '_'), '''', '_') + '_' + @group

exec('if  exists (select * from sys.objects where object_id = OBJECT_ID(N''[dbo].[' + @table + ']'') 
			and type in (N''U'')) drop table [dbo].[' + @table + ']')

exec('SELECT distinct a.*, ''' + @file + ''' as src_file, ''' + @tab + ''' as src_tab into '+ @table + ' FROM OPENROWSET(''Microsoft.Jet.OLEDB.4.0'', 
   ''Excel 8.0;Database=' + @path + @file + ''', ''SELECT * FROM [' + @tab + '$]'') AS a')


