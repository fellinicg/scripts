use loho
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.tbStage1_B') and type in (N'U'))
drop table dbo.tbStage1_B
go
create table dbo.tbStage1_B(
	Stage1_B_ID int identity(1,1) not null,
	Date datetime null,
	LastUpdated varchar(20) null,
	CreateDate varchar(20) null,
	Name varchar(255) null,
	LastName varchar(255) null,
	FirstName  varchar(255) null,
	Owner2LastName varchar(255) null,
	Owner2FirstName varchar(255) null,
	Seller varchar(255) null,
	CoopAddress varchar(255) null,
	OwnerAddress varchar(255) null,
	Apartment varchar(255) null,
	Address varchar(255) null,
	Street varchar(255) null,
	City varchar(255) null,
	State varchar(255) null,
	Zip varchar(255) null,
	CityState varchar(255) null,
	AptForSale varchar(255) null,
	Phone varchar(255) null,
	Cell varchar(255) null,
	Cell2 varchar(255) null,
	HomePhone varchar(255) null,
	WorkPhone varchar(255) null,
	Email varchar(255) null,
	Email2 varchar(255) null,
	Fax varchar(25) null,
	WantsOpenhouseMailings varchar(3) null,
	WantsNewListingsMailings varchar(3) null,
	DesiredNumBedrooms varchar(10) null,
	DesiredNumBathrooms varchar(10) null,
	MaxNumPrice varchar(3) null,
	LookingToBuy varchar(3) null,
	LookingToRent varchar(45) null,
	DesirableCoOps varchar(255) null,
	Guide varchar(255) null,
	Notes varchar(500) null,
	uid uniqueidentifier null,
	src_file varchar(50) null,
	src_tab varchar(25) null,
	Duplicate bit constraint dftbStage1_BDuplicate default 0 
)

--alter table dbo.tbStage1_B add constraint ucUID unique (uid)
