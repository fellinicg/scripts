--JacobsList
insert into dbo.tbStage1_B (Date, Name, Apartment, Phone, Notes, src_file, src_tab, uid)
select Date, Name, Apartment, [Phone #], Note, src_file, src_tab, uid
from dbo.tbPotential_sellers_xls_Jacobs_List_B

--TerrisList
insert into dbo.tbStage1_B (Name, AptForSale, Address, City, State, Zip, src_file, src_tab, uid)
select distinct Name, [Apt for sell], [address], [city], state, zip, src_file, src_tab, uid
from dbo.tbPotential_sellers_xls_Terris_List_B

--GSNList
insert into dbo.tbStage1_B (Name, Street, City, State, Zip, src_file, src_tab, uid)
select Name, Street, City, State, Zip, src_file, src_tab, uid
from dbo.tbPotential_sellers_xls_GSN_LIst_B