USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[upAddUserAccount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[upAddUserAccount]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090518
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090518
-- ============================================================
CREATE PROCEDURE dbo.upAddUserAccount
	@AccountType char(1)
	,@AccountName varchar(20)
	,@AccountKey varchar(200)
	,@FirstGivenName varchar(40)
	,@FamilyName varchar(60)
	,@AccountStatus  char(1)
	,@NamePrefix varchar(20) = null
	,@SecondGivenName varchar(40) = null
	,@NameSuffix varchar(20) = null
	,@GovtIDType char(1) = null
	,@GovtIDCode varchar(200) = null
	,@IsRegistered bit = 0
	,@UserAccountID int = -1 OUTPUT
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Nullify empty strings supplied
	SET @NamePrefix = NULLIF(@NamePrefix, '')
	SET @SecondGivenName = NULLIF(@SecondGivenName, '')
	SET @NameSuffix = NULLIF(@NameSuffix, '')
	SET @GovtIDType = NULLIF(@GovtIDType, '')
	SET @GovtIDCode = NULLIF(@GovtIDCode, '')


	-- Insert new record into tbUserAccount
	INSERT INTO dbo.tbUserAccount (
		AccountType,
		AccountName,
		AccountKey,
		NamePrefix,
		FirstGivenName,
		SecondGivenName,
		FamilyName,
		NameSuffix,
		FullName,
		GovtIDType,
		GovtIDCode,
		AccountStatus,
		CreateDateTime,
		UpdateDateTime)
	VALUES (
		UPPER(@AccountType),
		@AccountName,
		@AccountKey,
		@NamePrefix,
		@FirstGivenName,
		@SecondGivenName,
		@FamilyName,
		@NameSuffix,
		Tools.dbo.sfTrim(@FirstGivenName) + ' ' + Tools.dbo.sfTrim(@FamilyName),
		@GovtIDType,
		@GovtIDCode,
		UPPER(@AccountStatus),
		CURRENT_TIMESTAMP,
		CURRENT_TIMESTAMP)

	SET @UserAccountID = @@IDENTITY

	IF (ISNULL(@UserAccountID, 0) = 0)
		RAISERROR('upAddUserAccount - user account was NOT successfully added', 16, 1)

END
