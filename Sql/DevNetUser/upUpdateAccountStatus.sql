USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[upUpdateAccountStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[upUpdateAccountStatus]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090519
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090519
-- ============================================================
CREATE PROCEDURE dbo.upUpdateAccountStatus
	@AccountStatus int
	,@Description	varchar(40)
	,@Result char(1) OUTPUT -- (S)uccess or (F)ailure
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'

	-- Update record in tbAccountStatus
	UPDATE dbo.tbAccountStatus SET Description = @Description
	WHERE AccountStatus = @AccountStatus

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upUpdateAccountStatus - account status was NOT successfully updated', 16, 1)

END