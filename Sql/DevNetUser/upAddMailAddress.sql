USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[upAddMailAddress]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[upAddMailAddress]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090518
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090518
-- ============================================================
CREATE PROCEDURE dbo.upAddMailAddress 
	@UserAccountID int
	,@MailAddressType char(1)
	,@Line1 varchar(80) = null
	,@Line2 varchar(80) = null
	,@Line3 varchar(80) = null
	,@Line4 varchar(80) = null
	,@CityTown varchar(40) = null
	,@StateProvince varchar(40) = null
	,@PostalCode varchar(20) = null
	,@Country varchar(80) = null
	,@IsPreferred bit
	,@MailAddressID int OUTPUT
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- ***** Check Inputs *****
	-- 
	-- Nullify empty strings supplied
	SET @Line1 = NULLIF(@Line1, '')
	SET @Line2 = NULLIF(@Line2, '')
	SET @Line3 = NULLIF(@Line3, '')
	SET @Line4 = NULLIF(@Line4, '')
	SET @CityTown = NULLIF(@CityTown, '')
	SET @StateProvince = NULLIF(@StateProvince, '')
	SET @PostalCode = NULLIF(@PostalCode, '')
	SET @Country = NULLIF(@Country, '')

	-- If IsPreferred, then reset all other mail addresses to not preferred
	IF (@IsPreferred = 1)
		UPDATE dbo.tbMailAddress SET IsPreferred = 0 WHERE UserAccountID = @UserAccountID

	-- Insert new record into tbMailAddress
	INSERT INTO dbo.tbMailAddress (
		UserAccountID,
		MailAddressType,
		Line1,
		Line2,
		Line3,
		Line4,
		CityTown,
		StateProvince,
		PostalCode,
		Country,
		IsPreferred,
		CreateDateTime, 
		UpdateDateTime)
	VALUES (
		@UserAccountID,
		UPPER(@MailAddressType),
		@Line1,
		@Line2,
		@Line3,
		@Line4,
		@CityTown,
		@StateProvince,
		@PostalCode,
		@Country,
		@IsPreferred,
		CURRENT_TIMESTAMP,
		CURRENT_TIMESTAMP)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@IDENTITY > 0)
		SET @MailAddressID = @@IDENTITY
	ELSE
		RAISERROR('upAddMailAddress - mail address was NOT successfully added', 16, 1)

END