USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufMailAddressTypes]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ufMailAddressTypes]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090518
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090518
-- ============================================================
CREATE FUNCTION dbo.ufMailAddressTypes ()
RETURNS TABLE 
AS
RETURN 
(
	SELECT 'H' AS Type, 'Home' AS Description
	UNION
	SELECT 'W', 'Work'
	UNION
	SELECT 'O', 'Other'
)
