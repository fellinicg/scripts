USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUnregisterUser') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUnregisterUser
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090520
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090520
-- ============================================================
CREATE PROCEDURE dbo.upUnregisterUser
	@UserAccountID int
	,@Result char(1) = 'F' OUTPUT -- (S)uccess or (F)ailure
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Update record 
	UPDATE dbo.tbUserAccount SET IsRegistered = 0, UpdateDateTime = CURRENT_TIMESTAMP
	WHERE UserAccountID = @UserAccountID

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upUnregisterUser - user account was NOT successfully unregistered', 16, 1)

END