USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[upGetAccountPersonNames]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[upGetAccountPersonNames]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090518
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090518
-- ============================================================
CREATE PROCEDURE dbo.upGetAccountPersonNames
	@UserAccountID int
	,@FirstName varchar(40) OUTPUT
	,@MiddleName varchar(40) OUTPUT
	,@LastName varchar(60) OUTPUT
	,@FullNameForward varchar(100) OUTPUT
	,@FullNameReverse varchar(100) OUTPUT
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	SELECT
		@FirstName = FirstGivenName,
		@MiddleName = SecondGivenName,
		@LastName = FamilyName,
		@FullNameForward = FullName
	FROM tbUserAccount
	WHERE UserAccountID = @UserAccountID

	SET @FullNameReverse = @LastName + ', ' + @FirstName + CASE WHEN @MiddleName IS NOT NULL THEN ' ' + @MiddleName ELSE '' END

END