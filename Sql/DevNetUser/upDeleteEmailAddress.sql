USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[upDeleteEmailAddress]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[upDeleteEmailAddress]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090518
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090518
-- ============================================================
CREATE PROCEDURE dbo.upDeleteEmailAddress
	@EmailAddressID int
	,@UserAccountID int
	,@Result char(1) OUTPUT -- (S)uccess or (F)ailure
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'

	-- Delete record in tbEmailAddress
	DELETE FROM dbo.tbEmailAddress
	WHERE EmailAddressID = @EmailAddressID
	AND UserAccountID = @UserAccountID

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upDeleteEmailAddress - email address was NOT successfully deleted', 16, 1)

END