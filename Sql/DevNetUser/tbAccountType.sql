USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fkAccountType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUserAccount]'))
ALTER TABLE [dbo].[tbUserAccount] DROP CONSTRAINT [fkAccountType]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbAccountType]') AND type in (N'U'))
DROP TABLE [dbo].[tbAccountType]
GO
CREATE TABLE [dbo].[tbAccountType](
	[AccountType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [pkAccountType] PRIMARY KEY CLUSTERED 
(
	[AccountType] ASC
) ON [PRIMARY],
 CONSTRAINT [akAccountType] UNIQUE NONCLUSTERED 
(
	[Description] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbUserAccount]  WITH CHECK ADD  CONSTRAINT [fkAccountType] FOREIGN KEY([AccountType])
REFERENCES [dbo].[tbAccountType] ([AccountType])