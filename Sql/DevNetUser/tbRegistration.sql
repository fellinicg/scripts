USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbRegistration]') AND type in (N'U'))
DROP TABLE [dbo].[tbRegistration]
GO
CREATE TABLE [dbo].[tbRegistration](
	[RegistrationID] [uniqueidentifier] NOT NULL,
	[UserAccountID] [int] NOT NULL
) ON [PRIMARY]
