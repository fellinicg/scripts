USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateMailAddress') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateMailAddress
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090519
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090519
-- ============================================================
CREATE PROCEDURE dbo.upUpdateMailAddress
	@MailAddressID int
	,@UserAccountID int
	,@MailAddressType char(1)
	,@Line1 varchar(80)
	,@Line2 varchar(80) = null
	,@Line3 varchar(80) = null
	,@Line4 varchar(80) = null
	,@CityTown varchar(40)
	,@StateProvince varchar(40)
	,@PostalCode varchar(20)
	,@Country varchar(80) = null
	,@IsPreferred bit
	,@Result char(1) = 'F' OUTPUT -- (S)uccess or (F)ailure
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Update record
	UPDATE dbo.tbMailAddress SET MailAddressType = @MailAddressType, 
		Line1 = @Line1, Line2 = @Line2, Line3 = @Line3, Line4 = @Line4, CityTown = @CityTown, 
		StateProvince = @StateProvince, PostalCode = @PostalCode, Country = @Country, 
		IsPreferred = @IsPreferred, UpdateDateTime = CURRENT_TIMESTAMP
	WHERE MailAddressID = @MailAddressID AND UserAccountID = @UserAccountID

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upUpdateMailAddress - mail address was NOT successfully updated', 16, 1)

END