USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[upDeleteUsersEmailAddresses]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[upDeleteUsersEmailAddresses]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090518
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090518
-- ============================================================
CREATE PROCEDURE dbo.upDeleteUsersEmailAddresses
	@UserAccountID int
	,@EMailAddressType char(1) = '%'
	,@Result char(1) OUTPUT -- (S)uccess or (F)ailure
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'

	-- Delete records in tbEmailAddress, if there are any
	IF EXISTS (SELECT 1	FROM dbo.tbEmailAddress WHERE UserAccountID = @UserAccountID 
						AND EMailAddressType LIKE @EMailAddressType)
		BEGIN
			DELETE FROM dbo.tbEmailAddress
			WHERE UserAccountID = @UserAccountID
			AND EMailAddressType LIKE @EMailAddressType

			-- Verify success
			IF (@@ROWCOUNT <> 0)
				SET @Result = 'S'
			ELSE
				RAISERROR('upDeleteUsersEmailAddresses - email addresses were NOT successfully deleted', 16, 1)
		END
	ELSE SET @Result = 'S'

END
