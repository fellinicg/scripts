USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufContactNumTypes]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ufContactNumTypes]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090518
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090518
-- ============================================================
CREATE FUNCTION dbo.ufContactNumTypes ()
RETURNS TABLE 
AS
RETURN 
(
	SELECT 'HF' AS Type, 'Home Fax' AS Description
	UNION
	SELECT 'HP', 'Home Phone'
	UNION
	SELECT 'MP', 'Mobile Phone'
	UNION
	SELECT 'WF', 'Work Fax'
	UNION
	SELECT 'WP', 'Work Phone'
	UNION
	SELECT 'OF', 'Other Fax'
	UNION
	SELECT 'OP', 'Other Phone'
)