USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateEmailAddress') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateEmailAddress
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090519
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090519
-- ============================================================
CREATE PROCEDURE dbo.upUpdateEmailAddress 
	@EmailAddressID int
	,@UserAccountID int
	,@EMailAddressType char(1)
	,@EmailAddress	varchar(120)
	,@IsPreferred bit
	,@Result char(1) = 'F' OUTPUT -- (S)uccess or (F)ailure
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Update record
	UPDATE dbo.tbEmailAddress SET EMailAddressType = @EMailAddressType, 
		EmailAddress = @EmailAddress, IsPreferred = @IsPreferred, 
		UpdateDateTime = CURRENT_TIMESTAMP
	WHERE EmailAddressID = @EmailAddressID AND UserAccountID = @UserAccountID
	
	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upUpdateEmailAddress - email address was NOT successfully updated', 16, 1)

END
