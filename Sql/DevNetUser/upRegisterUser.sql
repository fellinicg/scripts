USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upRegisterUser') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upRegisterUser
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090520
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090520
-- ============================================================
CREATE PROCEDURE dbo.upRegisterUser
	@RegistrationID uniqueidentifier
	,@Result char(1) = 'F' OUTPUT -- (S)uccess or (F)ailure
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Update record 
	UPDATE dbo.tbUserAccount SET IsRegistered = 1
	FROM dbo.tbUserAccount u INNER JOIN dbo.tbRegistration r ON u.UserAccountID = r.UserAccountID
	WHERE r.RegistrationID = @RegistrationID

	-- Verify success
	IF (@@ROWCOUNT = 1)
		BEGIN
			SET @Result = 'S'
			DELETE FROM dbo.tbRegistration WHERE RegistrationID = @RegistrationID
		END
	ELSE
		RAISERROR('upRegisterUser - user account was NOT successfully registered', 16, 1)

END