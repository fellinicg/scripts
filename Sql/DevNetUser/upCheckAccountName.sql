USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upCheckAccountName') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upCheckAccountName
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090520
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090520
-- ============================================================
CREATE PROCEDURE dbo.upCheckAccountName
	@AccountName varchar(20)
	,@Exists char(1) OUTPUT
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Check for existence of accountname
	IF EXISTS (SELECT 1 FROM dbo.tbUserAccount WHERE AccountName = @AccountName)
		SET @Exists = 'T'
	ELSE
		SET @Exists = 'F'		

END