USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upWipeUserAccount') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upWipeUserAccount
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090519
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090519
-- ============================================================
CREATE PROCEDURE dbo.upWipeUserAccount
	@UserAccountID int
	,@Result char(1) OUTPUT
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Begin transaction
	BEGIN TRAN

	-- Delete records in tbEmailAddress
	DELETE FROM dbo.tbEmailAddress
	WHERE UserAccountID = @UserAccountID

	-- Delete records in tbMailAddress
	DELETE FROM dbo.tbMailAddress
	WHERE UserAccountID = @UserAccountID

	-- Delete records in tbContactNum
	DELETE FROM dbo.tbContactNum
	WHERE UserAccountID = @UserAccountID

	-- Disable trigger so we can delete record
	ALTER TABLE tbUserAccount DISABLE TRIGGER tdUserAccount

	-- Delete record in tbUserAccount
	DELETE FROM dbo.tbUserAccount
	WHERE UserAccountID = @UserAccountID

	-- Verify success
	IF (@@ROWCOUNT = 1)
		BEGIN
			SET @Result = 'S'
			COMMIT TRAN
		END
	ELSE
		BEGIN
			ROLLBACK TRAN
			SET @Result = 'F'
			RAISERROR('upWipeUserAccount - user account was NOT successfully wiped', 16, 1)
		END

	-- Enable trigger so noone else can delete record
	ALTER TABLE tbUserAccount ENABLE TRIGGER tdUserAccount

END