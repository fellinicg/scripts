USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[upAddEmailAddress]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[upAddEmailAddress]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090518
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090518
-- ============================================================
CREATE PROCEDURE dbo.upAddEmailAddress
	@UserAccountID int
	,@EMailAddressType char(1)
	,@EmailAddress	varchar(120)
	,@IsPreferred bit
	,@EmailAddressID int OUTPUT
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Insert new record into tbEmailAddress
	INSERT INTO dbo.tbEmailAddress (
		UserAccountID,
		EMailAddressType, 
		EmailAddress, 
		IsPreferred, 
		CreateDateTime, 
		UpdateDateTime)
	VALUES (
		@UserAccountID,
		UPPER(@EMailAddressType),
		@EmailAddress,
		@IsPreferred,
		CURRENT_TIMESTAMP,
		CURRENT_TIMESTAMP)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@IDENTITY > 0)
		SET @EmailAddressID = @@IDENTITY
	ELSE
		RAISERROR('upAddEmailAddress - email address was NOT successfully added', 16, 1)

END