USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddRegistrationID') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddRegistrationID
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090520
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090520
-- ============================================================
CREATE PROCEDURE dbo.upAddRegistrationID
	@UserAccountID int
	,@RegistrationID char(36) OUTPUT
	,@Result char(1) = 'F' OUTPUT -- (S)uccess or (F)ailure
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @RegistrationID = NEWID()
	-- Insert record 
	INSERT INTO dbo.tbRegistration (RegistrationID, UserAccountID)
	VALUES (@RegistrationID, @UserAccountID)

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		BEGIN
			SET @RegistrationID = null
			RAISERROR('upAddRegistrationID - registration id was NOT successfully added', 16, 1)
		END

END