USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upDeleteUserAccount') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upDeleteUserAccount
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090518
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090519
-- ============================================================
CREATE PROCEDURE dbo.upDeleteUserAccount
	@UserAccountID int
	,@Result char(1) = 'F' OUTPUT
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Delete record in tbUserAccount
	UPDATE dbo.tbUserAccount SET IsDeleted = 1
	WHERE UserAccountID = @UserAccountID

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'

END