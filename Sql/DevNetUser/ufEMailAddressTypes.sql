USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufEMailAddressTypes]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ufEMailAddressTypes]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090518
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090518
-- ============================================================
CREATE FUNCTION dbo.ufEMailAddressTypes ()
RETURNS TABLE 
AS
RETURN 
(
	SELECT 'H' AS Type, 'Home' AS Description
	UNION
	SELECT 'W', 'Work'
	UNION
	SELECT 'M', 'Mobile'
	UNION
	SELECT 'O', 'Other'
)