USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fkGovtIDType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUserAccount]'))
ALTER TABLE [dbo].[tbUserAccount] DROP CONSTRAINT [fkGovtIDType]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbGovtIDType]') AND type in (N'U'))
DROP TABLE [dbo].[tbGovtIDType]
GO
CREATE TABLE [dbo].[tbGovtIDType](
	[GovtIDType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [pkGovtIDType] PRIMARY KEY CLUSTERED 
(
	[GovtIDType] ASC
) ON [PRIMARY],
 CONSTRAINT [akGovtIDType] UNIQUE NONCLUSTERED 
(
	[Description] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbUserAccount]  WITH CHECK ADD  CONSTRAINT [fkGovtIDType] FOREIGN KEY([GovtIDType])
REFERENCES [dbo].[tbGovtIDType] ([GovtIDType])
