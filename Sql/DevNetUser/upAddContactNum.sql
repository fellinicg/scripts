USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[upAddContactNum]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[upAddContactNum]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090518
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090518
-- ============================================================
CREATE PROCEDURE dbo.upAddContactNum
	@UserAccountID int
	,@ContactNumType char(2)
	,@CityAreaCode varchar(10) = null
	,@ContactNum varchar(20) = null
	,@Extension varchar(10) = null
	,@TxtMsgCapable bit = 0
	,@IsPreferred bit = 0
	,@ContactNumID int OUTPUT
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- ***** Check Inputs *****
	-- 
	-- Nullify empty strings supplied
	SET @CityAreaCode = NULLIF(@CityAreaCode, '')
	SET @ContactNum = NULLIF(@ContactNum, '')
	SET @Extension = NULLIF(@Extension, '')

	-- If IsPreferred, then reset all other Contact numbers to not preferred
	IF (@IsPreferred = 1)
		UPDATE dbo.tbContactNum SET IsPreferred = 0 WHERE UserAccountID = @UserAccountID

	-- Insert new record into tbContactNum
	INSERT INTO dbo.tbContactNum (
		UserAccountID,
		ContactNumType,
		CityAreaCode,
		ContactNum,
		Extension,
		TxtMsgCapable,
		IsPreferred,
		CreateDateTime, 
		UpdateDateTime)
	VALUES (
		@UserAccountID,
		UPPER(@ContactNumType),
		@CityAreaCode,
		@ContactNum,
		@Extension,
		@TxtMsgCapable,
		@IsPreferred,
		CURRENT_TIMESTAMP,
		CURRENT_TIMESTAMP)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@IDENTITY > 0)
		SET @ContactNumID = @@IDENTITY
	ELSE
		RAISERROR('upAddContactNum - Contact number was NOT successfully added', 16, 1)

END