USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[upGetAccountPreferredEmailAddr]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[upGetAccountPreferredEmailAddr]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090518
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090518
-- ============================================================
CREATE PROCEDURE dbo.upGetAccountPreferredEmailAddr 
	@UserAccountID int
	,@EmailAddress varchar(120) OUTPUT
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	
	SELECT @EmailAddress = EmailAddress
	FROM tbEmailAddress 
	WHERE UserAccountID = @UserAccountID
		AND IsPreferred = 1

END