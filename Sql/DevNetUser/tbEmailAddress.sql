USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fkEmailAddressAccount]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbEmailAddress]'))
ALTER TABLE [dbo].[tbEmailAddress] DROP CONSTRAINT [fkEmailAddressAccount]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbEmailAddress]') AND type in (N'U'))
DROP TABLE [dbo].[tbEmailAddress]
GO
CREATE TABLE [dbo].[tbEmailAddress](
	[EmailAddressID] [int] IDENTITY(1,1) NOT NULL,
	[UserAccountID] [int] NOT NULL,
	[EMailAddressType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[EmailAddress] [varchar](120) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsValidFormat]  AS ([dbo].[ufEmailAddressIsValid]([EmailAddress])),
	[IsPreferred] [bit] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [dfEmailAddressCreateDate]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [dfEmailAddressUpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [pkEmailAddress] PRIMARY KEY CLUSTERED 
(
	[EmailAddressID] ASC
) ON [PRIMARY],
 CONSTRAINT [akEmailAddress] UNIQUE NONCLUSTERED 
(
	[UserAccountID] ASC,
	[EmailAddress] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbEmailAddress]  WITH CHECK ADD  CONSTRAINT [fkEmailAddressAccount] FOREIGN KEY([UserAccountID])
REFERENCES [dbo].[tbUserAccount] ([UserAccountID])
GO
ALTER TABLE [dbo].[tbEmailAddress]  WITH NOCHECK ADD  CONSTRAINT [tcEmailAddressType] CHECK  (([EMailAddressType] = 'H' or ([EMailAddressType] = 'O' or ([EMailAddressType] = 'M' or [EMailAddressType] = 'W'))))
GO
ALTER TABLE [dbo].[tbEmailAddress] CHECK CONSTRAINT [tcEmailAddressType]
GO
CREATE TRIGGER [tiEmailAddress] ON [dbo].[tbEmailAddress] FOR INSERT
AS
BEGIN

	IF (@@ROWCOUNT = 0)
		RETURN

	-- ACCOUNTS MAY HAVE ONLY ONE PREFERRED EMAIL ADDRESS
	IF (EXISTS (SELECT 1 FROM inserted WHERE IsPreferred = 1))
		UPDATE tbEmailAddress SET IsPreferred = 0
		FROM tbEmailAddress e INNER JOIN inserted i ON i.UserAccountID = e.UserAccountID 
				AND i.EmailAddressID <> e.EmailAddressID AND i.IsPreferred = 1

END
GO
CREATE TRIGGER [tuEmailAddress] ON [dbo].[tbEmailAddress] FOR UPDATE
AS
BEGIN

	IF (@@ROWCOUNT = 0)
		RETURN

	-- ACCOUNTS MAY HAVE ONLY ONE PREFERRED EMAIL ADDRESS
	IF (UPDATE(IsPreferred))
		IF (EXISTS (SELECT 1 FROM inserted WHERE IsPreferred = 1))
			UPDATE tbEmailAddress SET IsPreferred = 0
			FROM tbEmailAddress e INNER JOIN inserted i ON i.UserAccountID = e.UserAccountID 
				AND i.EmailAddressID <> e.EmailAddressID AND i.IsPreferred = 1

END