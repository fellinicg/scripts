USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateContactNum') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateContactNum
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090519
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20090519
-- ============================================================
CREATE PROCEDURE dbo.upUpdateContactNum
	@ContactNumID int
	,@UserAccountID int
	,@ContactNumType char(2)
	,@CityAreaCode varchar(10)
	,@ContactNum varchar(20)
	,@Extension varchar(10) = null
	,@TxtMsgCapable bit = 0
	,@IsPreferred bit = 0
	,@Result char(1) = 'F' OUTPUT -- (S)uccess or (F)ailure
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Update record 
	UPDATE dbo.tbContactNum SET ContactNumType = @ContactNumType, CityAreaCode = @CityAreaCode, 
		ContactNum = @ContactNum, Extension = @Extension, TxtMsgCapable = @TxtMsgCapable, 
		IsPreferred = @IsPreferred, UpdateDateTime = CURRENT_TIMESTAMP
	WHERE ContactNumID = @ContactNumID AND UserAccountID = @UserAccountID


	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upUpdateContactNum - contact number was NOT successfully updated', 16, 1)

END