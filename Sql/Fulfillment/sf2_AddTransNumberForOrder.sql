SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: October 11, 2005
-- Description:	Add a credit card transaction number to an order
-- =============================================
CREATE PROCEDURE sf2_AddTransNumberForOrder 
	@OrderID int,
	@UserID int,
	@TransactionNumber varchar(100)
AS
BEGIN

	SET NOCOUNT ON

	UPDATE dbo.OrderHeader SET TransactionNumber = @TransactionNumber
	WHERE OrderID = @OrderID
	AND UserID = @UserID

END
GO
