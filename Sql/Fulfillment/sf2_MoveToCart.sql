USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: October 20, 2006
-- Description:	Move item to cart from wish list
-- =============================================
CREATE PROCEDURE dbo.sf2_MoveToCart 
@CartID INT,
@Userid VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	-- Make sure the Cart belongs to the user
	IF EXISTS (SELECT 1 FROM dbo.ShoppingCart WHERE CartID = @CartID AND UserID = @Userid)
		BEGIN
			/* Move item. */
			UPDATE dbo.ShoppingCart SET WishList = 0
			WHERE CartID = @CartID
		END

END
GO
