USE Fulfilment
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Todd Fellini
-- Create date: September 6, 2005
-- Description:	Cart Checkout
-- =============================================
ALTER PROCEDURE sf2_Checkout 
	@p_ID INT, 
	@u_ID INT,
  @ShipName VARCHAR(100),
	@ShipAddress1 VARCHAR(100), 
	@ShipAddress2 VARCHAR(100)='', 
	@ShipCity VARCHAR(100), 
	@ShipState VARCHAR(100), 
	@ShipPostal VARCHAR(100), 
	@Email VARCHAR(100), 
	@ShipPhone VARCHAR(100),
	@ShipPhone2 VARCHAR(100)='',
	@CardType VARCHAR(20),
	@Country VARCHAR(100),
	@PointBalance INT,
	@GCShippingCharge INT,
	@OrderID INT OUTPUT
AS
BEGIN

DECLARE @rc INT
DECLARE @pCode VARCHAR(10)
DECLARE @clientid INT
DECLARE @token VARCHAR(50)
DECLARE @time DATETIME
DECLARE @DKNo INT

SET NOCOUNT ON
SET XACT_ABORT ON

SET @orderid = 0
SET @time = getdate()

BEGIN TRAN

-- Get item info for checkout
EXEC @rc = SQL2KPROD.Catalog.dbo.sf2_GetCartItemsForCheckout @p_ID, @u_ID, @token = @token OUTPUT

-- Check return value
IF @rc = 0 
BEGIN
	ROLLBACK TRAN
	RETURN @orderid
END

-- Get extraneous program info
SELECT @pCode = p_Code, @clientid = p_cl_ID, @DKNo = p_DKNo
FROM SQL2KPROD.Catalog.dbo.Programs
WHERE p_ID = @p_ID

-- Insert info into OrderHeader table
INSERT INTO OrderHeader (
	OrderDate, 
	ProjectNumber, 
	UserID, 
	ShipName, 
	ShipAddress1, 
	ShipAddress2, 
	ShipCity, 
	ShipState, 
	ShipCountry, 
	ShipPostal, 
	ShipPhone, 
	ShipPhone2, 
	Email, 
	ProgramCode, 
	ClientID, 
	YTOPoints_Levels,
	CardType)
VALUES (
	GETDATE(), 
	@DKNo, 
	@u_ID, 
	@Shipname, 
	@ShipAddress1, 
	@ShipAddress2, 
	@ShipCity, 
	@ShipState,
	@Country, 
	@ShipPostal, 
	@ShipPhone, 
	@ShipPhone2, 
	@Email, 
	@Pcode, 
	@clientid, 
	@PointBalance,
	@CardType)

-- Error check
IF @@Error <> 0 OR @@Rowcount = 0 
BEGIN
	ROLLBACK TRAN
	RETURN 0
END

-- Store orderid
SET @orderid = @@Identity

-- Insert info into AwardLine table
INSERT INTO dbo.AwardLine(AwardID, OrderID, AwardTitle, Quantity, UnitPoints, TotalPoints, CartID)
SELECT 
	sc.AwardID, 
	@orderid, 
	scc.ItemDesc, 
	sc.Quantity, 
	scc.Price, 
	CASE scc.ItemType WHEN 'G' THEN ((sc.Quantity * scc.Price) + ISNULL(@GCShippingCharge, 0)) ELSE (sc.Quantity * scc.Price) END, 
	sc.CartID
FROM dbo.ShoppingCart sc
INNER JOIN dbo.ShoppingCartCheckout scc ON sc.CartID = scc.CartID
WHERE sc.ProgramID = @p_ID
AND sc.UserID = @u_ID

-- Error check
IF @@Error <> 0 OR @@Rowcount = 0 
BEGIN
	ROLLBACK TRAN
	SET @orderid = 0
	RETURN 0
END

-- Insert info into ItemLine table for non-packaged items
INSERT INTO dbo.ItemLine(AwardLineID, ItemType, ItemID, VendorID, ItemTitle, 
ItemOptions, ItemModel, CostPerUnit, ShippingPerUnit, TaxPerUnit, TotalCost, 
TotalShipping, TotalTax, ShippingFlag)
SELECT al.AwardLineID, scc.ItemType, scc.MasterCatalogId, scc.VendorNum, scc.ItemDesc, 
ISNULL(uf.OptionList, ''), scc.Model, scc.Cost, scc.Shipping, CONVERT(SMALLMONEY, CONVERT(VARCHAR, (scc.Cost * scc.TaxRate), 0)), (sc.Quantity * scc.Cost),
CASE scc.ShipFlag WHEN 'Q' THEN (sc.Quantity * scc.Shipping) ELSE scc.Shipping END,
(sc.Quantity * CONVERT(SMALLMONEY, CONVERT(VARCHAR, (scc.Cost * scc.TaxRate), 0))),
scc.ShipFlag
FROM dbo.ShoppingCart sc
INNER JOIN dbo.ShoppingCartCheckout scc ON sc.CartID = scc.CartID
INNER JOIN dbo.AwardLine al ON al.CartID = sc.CartID AND al.OrderID = @orderid
LEFT JOIN dbo.sf2_ufGetOptionsForCartItems(@p_ID, @u_ID) uf ON sc.CartID = uf.CartID
WHERE sc.ProgramID = @p_ID
AND sc.UserID = @u_ID
AND scc.ItemsInPkg = 0
ORDER BY AwardLineID

-- Error check
IF @@Error <> 0
BEGIN
	ROLLBACK TRAN
	SET @orderid = 0
	RETURN 0
END

-- Insert info into ItemLine table for packaged items
INSERT INTO dbo.ItemLine(AwardLineID, ItemType, ItemID, VendorID, ItemTitle, 
ItemOptions, ItemModel, CostPerUnit, ShippingPerUnit, TaxPerUnit, TotalCost, 
TotalShipping, TotalTax, ShippingFlag)
SELECT al.AwardLineID, gp.it_SDesc, gp.mc_ID, gp.mc_v_Number, gp.mc_SDesc, '', gp.mc_Model,
gp.ip_Cost, gp.isi_ShipCost, CONVERT(SMALLMONEY, CONVERT(VARCHAR, (gp.ip_Cost * scc.TaxRate), 0)),
(sc.Quantity * gp.ip_Cost), CASE gp.isi_ShipFlag WHEN 'Q' THEN (sc.Quantity * gp.isi_ShipCost) ELSE gp.isi_ShipCost END,
(sc.Quantity * CONVERT(SMALLMONEY, CONVERT(VARCHAR, (gp.ip_Cost * scc.TaxRate), 0))),
gp.isi_ShipFlag
FROM dbo.ShoppingCart sc
INNER JOIN dbo.ShoppingCartCheckout scc ON sc.CartID = scc.CartID
INNER JOIN dbo.AwardLine al ON al.CartID = sc.CartID AND al.OrderID = @orderid
INNER JOIN dbo.sf2_ufGetPackage(@time) gp ON scc.MasterCatalogId = gp.pc_mc_ID
WHERE sc.ProgramID = @p_ID
AND sc.UserID = @u_ID
AND scc.ItemsInPkg <> 0
ORDER BY AwardLineID

-- Error check
IF @@Error <> 0 
BEGIN
	ROLLBACK TRAN
	SET @orderid = 0
	RETURN 0
END

-- Clear users cart for program
--EXEC dbo.sf2_ClearUsersCartByProgram @UserID = @u_ID, @ProgramID = @p_ID

-- Clear CartID field in Awardline
UPDATE dbo.Awardline SET CartID = NULL
FROM dbo.ShoppingCart sc
INNER JOIN dbo.ShoppingCartCheckout scc ON sc.CartID = scc.CartID
INNER JOIN dbo.AwardLine al ON al.CartID = sc.CartID AND al.OrderID = @orderid
WHERE sc.ProgramID = @p_ID
AND sc.UserID = @u_ID
AND scc.ItemsInPkg = 0

-- Clear working table
DELETE 
FROM dbo.ShoppingCartCheckout
WHERE Token = @token

-- Commit transaction
COMMIT TRAN

RETURN 1

END
GO
