USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Todd Fellini
-- Create date: August 31, 2005
-- Description:	Get list of accepted credit cards
-- =============================================
ALTER PROCEDURE sf2_GetAcceptedCC 
AS
BEGIN

SET NOCOUNT ON

SELECT CreditCardAbbrev, CreditCard
FROM dbo.CreditCards
WHERE Accepted = 1

END
GO
