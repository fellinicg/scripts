USE Fulfilment

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: October 5, 2005
-- Description:	Delete user's order
-- =============================================
ALTER PROCEDURE dbo.sf2_DeleteOrder 
	@OrderID INT,
	@UserID INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM dbo.OrderHeader
	WHERE OrderID = @OrderID
	AND UserID = @UserID

END
GO
