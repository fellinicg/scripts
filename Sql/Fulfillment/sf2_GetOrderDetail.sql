SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: September 9, 2005
-- Description:	Retrieve order detail information
-- =============================================
ALTER PROCEDURE dbo.sf2_GetOrderDetail
	@p_ID INT, 
	@u_ID INT, 
	@OrderID INT
AS
BEGIN

	DECLARE @pCode VARCHAR(10)
	DECLARE @clientid INT
	DECLARE @DKNo INT

	SET NOCOUNT ON

	-- Get extraneous program info
	SELECT @pCode = p_Code, @clientid = p_cl_ID, @DKNo = p_DKNo
	FROM SQL2KPROD.Catalog.dbo.Programs
	WHERE p_ID = @p_ID

	SELECT DISTINCT oh.OrderID, CONVERT(VARCHAR, oh.OrderDate, 101) 'OrderDate', 
	ShipName,
	CASE LEN(ISNULL(ShipAddress2, '')) 
		WHEN 0 THEN ShipAddress1
		ELSE ShipAddress1 + CHAR(13) + ShipAddress2
  END 'ShipAddress',
	ShipCity,
	ShipState,
	ShipPostal,
	dbo.sf2_ufFormatPhoneNum(ShipPhone, 'p') 'ShipPhone'
	FROM dbo.OrderHeader oh
	INNER JOIN dbo.AwardLine al ON oh.OrderID = al.OrderID
	WHERE oh.UserID = @u_ID
	AND oh.ProjectNumber = @DKNo
	AND oh.OrderID = @orderid

	SELECT DISTINCT oh.OrderID, al.AwardLineID, al.AwardTitle, al.UnitPoints, al.Quantity, al.TotalPoints, 
	CASE al.Void 
		WHEN 1 THEN 'Canceled'
		ELSE 
			CASE al.Processed	
				WHEN 0 THEN 'Processing' 
				ELSE 'Processed'
			END 
	END 'Status',
	il.ItemOptions,
	al.Void,
	il.ItemType
	FROM dbo.OrderHeader oh
	INNER JOIN dbo.AwardLine al ON oh.OrderID = al.OrderID
	INNER JOIN dbo.ItemLine il ON al.AwardLineID = il.AwardLineID
	WHERE oh.UserID = @u_ID
	AND oh.ProjectNumber = @DKNo
	AND oh.OrderID = @orderid
	ORDER BY al.AwardLineID

END
GO
