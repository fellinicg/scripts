USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: October 6, 2005
-- Description:	Remove all user's items from cart for a specific program
-- =============================================
ALTER PROCEDURE sf2_ClearUsersCartByProgram 
	@UserID varchar(100),
	@ProgramID int
AS
BEGIN
	SET NOCOUNT ON

	-- Clear user's cart for program
	DELETE 
	FROM dbo.ShoppingCart 
	WHERE ProgramID = @ProgramID
	AND UserID = @UserID

END
GO
