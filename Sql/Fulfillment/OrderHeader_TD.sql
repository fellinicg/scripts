SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Todd Fellini
-- Create date: October 5, 2005
-- Description:	Delete Trigger
-- =============================================
CREATE TRIGGER dbo.OrderHeader_TD 
   ON  dbo.OrderHeader 
   FOR DELETE
AS 
BEGIN

	DECLARE 
		@OrderID INT,
	  @CurrID INT


	SET NOCOUNT ON

  -- HANDLE MULTIPLE RECORD DELETES
  SELECT @CurrID = MIN(OrderID) FROM deleted
  WHILE (ISNULL(@CurrID, 0) <> 0)
  BEGIN

    -- DELETE RECORDS IN ITEMLINE TABLE
		DELETE
		FROM dbo.ItemLine
		WHERE ItemLineID IN (
													SELECT il.ItemLineID
													FROM dbo.ItemLine il
													INNER JOIN dbo.AwardLine al ON al.AwardLineID = il.AwardLineID
													WHERE al.OrderID = @CurrID)
    
    -- DELETE RECORDS IN AWARDLINE TABLE
		DELETE
		FROM dbo.AwardLine
		WHERE OrderID = @CurrID
    
    -- GET NEXT CANDIDATE
    SELECT @CurrID = MIN(OrderID) FROM deleted WHERE OrderID > @CurrID
    
  END
  
  SET NOCOUNT OFF

END
GO
