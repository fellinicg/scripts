USE [Fulfilment]
GO
/****** Object:  Table [dbo].[AwardLine]    Script Date: 08/31/2005 20:07:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[CreditCards](
	[CreditCardAbbrev] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CreditCard] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Accepted] [bit] NOT NULL DEFAULT (1),
 CONSTRAINT [PK_CreditCards] PRIMARY KEY CLUSTERED 
(
	[CreditCardAbbrev]ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF

GO

INSERT INTO [Fulfilment].[dbo].[CreditCards]
           ([CreditCardAbbrev]
           ,[CreditCard])
     VALUES
           ('amex'
           ,'American Express')
GO
INSERT INTO [Fulfilment].[dbo].[CreditCards]
           ([CreditCardAbbrev]
           ,[CreditCard])
     VALUES
           ('visa'
           ,'Visa')
GO
INSERT INTO [Fulfilment].[dbo].[CreditCards]
           ([CreditCardAbbrev]
           ,[CreditCard])
     VALUES
           ('mc'
           ,'MasterCard')
GO
INSERT INTO [Fulfilment].[dbo].[CreditCards]
           ([CreditCardAbbrev]
           ,[CreditCard])
     VALUES
           ('discover'
           ,'Discover')