SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: September 22, 2005
-- Description:	Return information for token
-- =============================================
ALTER PROCEDURE dbo.sf2_GetTokenInfo
	@Token varchar(36),
	@ProgramID int OUTPUT, 
	@UserId int OUTPUT
AS
BEGIN

	SET NOCOUNT ON

	-- Retrieve information associated with token.
	--
	SELECT @ProgramID = PID, @UserId = UID
	FROM dbo.StoreFrontTokens
  WHERE TokenID = @Token

	-- Delete token.
	--
	DELETE FROM dbo.StoreFrontTokens
  WHERE TokenID = @Token

END
GO
