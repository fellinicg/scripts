USE [Fulfilment]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: September 8, 2005
-- Description:	Returns option list for cart items in one field per cart.
-- =============================================
CREATE FUNCTION dbo.sf2_ufGetOptionsForCartItems 
(
	@p_ID INT,
	@u_ID INT
)
RETURNS 
@Results TABLE 
(
	CartID int, 
	OptionList varchar(5000)
)
AS
BEGIN

	DECLARE @OptList VARCHAR(5000)
	DECLARE @cartid INT
	DECLARE @TempTable TABLE (SCOID INT, CartID INT, Opt VARCHAR(4000))

	SET @OptList = ''

	INSERT INTO @TempTable (SCOID, CartID, Opt)
	SELECT SCOID, CartID, ot_Desc + ': ' + o_Desc
	FROM dbo.sf2_ufGetCart(@p_ID, @u_ID)
	INNER JOIN SQL2KPROD.Catalog.dbo.ItemOptions ON OptionValue = io_ID
	INNER JOIN SQL2KPROD.Catalog.dbo.OptionTypes ON io_ot_ID = ot_ID
	INNER JOIN SQL2KPROD.Catalog.dbo.Options ON io_o_ID = o_ID
	ORDER BY CartID, SCOID

	SELECT @Cartid = MIN(CartID) FROM @TempTable

	WHILE (SELECT COUNT(SCOID) FROM @TempTable) > 0
	BEGIN
		IF (SELECT TOP 1 CartID FROM @TempTable ORDER BY CartID, SCOID) <> @cartid
			BEGIN
				INSERT INTO @Results (CartID, OptionList)
				VALUES (@cartid, @Optlist)
				SET @Optlist = ''
				SELECT @Cartid =MIN(CartID) FROM @TempTable
			END
		SELECT TOP 1 @Optlist = @Optlist + Opt + char(13)
		FROM @TempTable
		ORDER BY CartID, SCOID
		DELETE 
		FROM @TempTable
		WHERE SCOID = (SELECT MIN(SCOID) FROM @TempTable)
	END

	IF ((LEN(@cartid) > 0) AND (LEN(@Optlist) > 0))
		INSERT INTO @Results (CartID, OptionList)
		VALUES (@Cartid, @Optlist)
	
	RETURN 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
