USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: August 31, 2005
-- Description:	Get a list of states
-- =============================================
CREATE PROCEDURE dbo.sf2_GetStates
AS
BEGIN
SET NOCOUNT ON

SELECT Name, StateCode
FROM dbo.States
ORDER BY StateCode

END
GO
