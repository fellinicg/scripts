USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061012
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upPurchasePoints_Delete (@PurchasePointOrderId int)
AS

	SET NOCOUNT ON

	DELETE FROM tbPurchasePointOrders
	WHERE PurchasePointOrderID = @PurchasePointOrderId

	SET NOCOUNT OFF



