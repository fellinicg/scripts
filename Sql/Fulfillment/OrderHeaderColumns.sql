USE Fulfilment

-- If necessary, add a ShipPhone2 column to OrderHeader
IF NOT EXISTS (SELECT 1 
							FROM sysobjects
							INNER JOIN syscolumns ON sysobjects.id = syscolumns.id
							WHERE sysobjects.name = 'OrderHeader'
							AND sysobjects.type = 'U'
							AND syscolumns.name = 'ShipPhone2')
ALTER TABLE dbo.OrderHeader
	ADD ShipPhone2 varchar(100) NULL

-- If necessary, add a CardType column to OrderHeader
IF NOT EXISTS (SELECT 1 
							FROM sysobjects
							INNER JOIN syscolumns ON sysobjects.id = syscolumns.id
							WHERE sysobjects.name = 'OrderHeader'
							AND sysobjects.type = 'U'
							AND syscolumns.name = 'CardType')
ALTER TABLE dbo.OrderHeader
	ADD CardType varchar(20) NULL

-- If necessary, add a TransactionNumber column to OrderHeader
IF NOT EXISTS (SELECT 1 
							FROM sysobjects
							INNER JOIN syscolumns ON sysobjects.id = syscolumns.id
							WHERE sysobjects.name = 'OrderHeader'
							AND sysobjects.type = 'U'
							AND syscolumns.name = 'TransactionNumber')
ALTER TABLE dbo.OrderHeader
	ADD TransactionNumber varchar(100) NULL
