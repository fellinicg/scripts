USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE dbo.sf2_RemoveFromWishList
@CartID INT,
@Userid VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	-- Make sure the Cart belongs to the user
	IF EXISTS (SELECT 1 FROM dbo.ShoppingCart WHERE CartID = @CartID AND UserID = @Userid)
		BEGIN
			/* Remove options. */
			DELETE FROM dbo.ShoppingCartOptions
			WHERE CartID = @CartID

			/* Remove item. */
			DELETE FROM dbo.ShoppingCart
			WHERE CartID = @CartID
		END

END
GO
