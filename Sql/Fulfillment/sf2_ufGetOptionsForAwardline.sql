SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: September 9, 2005
-- Description:	Returns option list for award items in one field.
-- =============================================
CREATE FUNCTION dbo.sf2_ufGetOptionsForAwardline 
(
	@Awardline INT
)
RETURNS VARCHAR(5000)
AS
BEGIN

	DECLARE @OptList VARCHAR(5000)
	DECLARE @TempTable TABLE (Itemline INT, Opt VARCHAR(5000))

	SET @OptList = ''

	INSERT INTO @TempTable (ItemLine, Opt)
	SELECT ItemLineID, ItemOptions
	FROM Fulfilment.dbo.ItemLine
	WHERE AwardLineID = @Awardline
	ORDER BY ItemLineID

	SELECT @Cartid = MIN(CartID) FROM @TempTable

	WHILE (SELECT COUNT(SCOID) FROM @TempTable) > 0
	BEGIN
		IF (SELECT TOP 1 CartID FROM @TempTable ORDER BY CartID, SCOID) <> @cartid
			BEGIN
				INSERT INTO @Results (CartID, OptionList)
				VALUES (@cartid, @Optlist)
				SET @Optlist = ''
				SELECT @Cartid =MIN(CartID) FROM @TempTable
			END
		SELECT TOP 1 @Optlist = @Optlist + Opt + char(13)
		FROM @TempTable
		ORDER BY CartID, SCOID
		DELETE 
		FROM @TempTable
		WHERE SCOID = (SELECT MIN(SCOID) FROM @TempTable)
	END

	IF ((LEN(@cartid) > 0) AND (LEN(@Optlist) > 0))
		INSERT INTO @Results (CartID, OptionList)
		VALUES (@Cartid, @Optlist)
	
	RETURN 
END
GO