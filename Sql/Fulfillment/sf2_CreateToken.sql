SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Todd Fellini
-- Create date: September 22, 2005
-- Description:	Return a token to pass to Storefront for authentication
-- =============================================
ALTER PROCEDURE dbo.sf2_CreateToken 
	@ProgramID int, 
	@UserId int,
	@Token varchar(36) OUTPUT
AS
BEGIN
	SET NOCOUNT ON

	SET @Token = NEWID()

	INSERT INTO dbo.StoreFrontTokens (TokenID, PID, UID)
	VALUES (@Token, @ProgramID, @UserId)

END
GO
