SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: September 9, 2005
-- Description:	Format phone number
-- =============================================
ALTER FUNCTION dbo.sf2_ufFormatPhoneNum 
(
	@PhoneNum VARCHAR(20),
	@Style CHAR(1)
)
RETURNS VARCHAR(20)
AS
BEGIN
	-- if LEN(@PhoneNum) 0-6 do nothing, 7 = add one hyphen, 8,9 = do nothing, 10 = add two hyphens
  -- if NULL given, return empty string
  
  SET @PhoneNum = ISNULL(@PhoneNum, '')
	SET @Style = UPPER(@Style)

  IF LEN(@PhoneNum) = 7
    SET @PhoneNum = STUFF(@PhoneNum, 4, 0, '-')
 
  IF LEN(@PhoneNum) = 10
		BEGIN
			IF @Style = 'D'
				SET @PhoneNum = STUFF(STUFF(@PhoneNum, 7, 0, '-'), 4, 0, '-')
			IF @Style = 'P'
				SET @PhoneNum = STUFF(STUFF(STUFF(@PhoneNum, 7, 0, '-'), 4, 0, ') '), 1, 0, '(')
		END
 
  IF LEN(@PhoneNum) = 11
		BEGIN
			IF @Style = 'D'
				SET @PhoneNum = STUFF(STUFF(STUFF(@PhoneNum, 8, 0, '-'), 5, 0, '-'), 2, 0, '-')
			IF @Style = 'P'
				SET @PhoneNum = STUFF(STUFF(STUFF(@PhoneNum, 8, 0, '-'), 5, 0, ') '), 2, 0, ' (')
		END
 
  RETURN(@PhoneNum)

END
GO

