set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

ALTER TRIGGER [dbo].[ItemLine_TI] ON [dbo].[ItemLine] FOR INSERT
AS
DECLARE
  @OrderID int,
  @UserID varchar(100),
  @EarnedPoints int,
  @ProjectNumber int,
  @SpentPoints int,
  @oldSBTStatus varchar(50),
  @newSBTStatus varchar(50),
  @ProgramCode  varchar(25),
  @CurrID int

  SET NOCOUNT ON

  -- HANDLE MULTIPLE RECORD INSERTS
  SELECT @CurrID = MIN(ItemLineID) FROM inserted
  WHILE (ISNULL(@CurrID, 0) <> 0)
  BEGIN

    -- GET INFO FOR CURR RECORD
    SELECT
      @OrderID = oh.OrderID,
      @UserID = oh.UserID,
      @EarnedPoints = oh.YTOPoints_Levels,
      @ProjectNumber = oh.ProjectNumber,
      @ProgramCode = oh.ProgramCode
    FROM OrderHeader oh
      INNER JOIN AwardLine al ON (oh.OrderID = al.OrderID)
      INNER JOIN ItemLine il ON (al.AwardLineID = il.AwardLineID)
    WHERE il.ItemLineID = @CurrID

    SELECT @SpentPoints = SUM(TotalPoints)
    FROM OrderHeader oh
      INNER JOIN AwardLine al ON (oh.OrderID = al.OrderID)
    WHERE oh.UserID = @UserID
      AND oh.ProgramCode = @ProgramCode
      AND Void <> 1
      AND al.AwardTitle not LIKE 'AMEX Gift Certificate%'

    --DELETE THE ORDER IF NEGATIVE BALANCE IS DETECTED
    IF ((ISNULL(@EarnedPoints,0) - ISNULL(@SpentPoints,0)) < 0 
           AND @ProjectNumber NOT IN 
             (5558,5759,5779,5780,5808,5809,5779,5780,5759,5872,4137,5899,5910,5911,5908,5909,8023,8033,8048))
    BEGIN
      UPDATE AwardLine SET 
        Void = 1, 
        VoidedBy = 'Voided by the ALEROS System: NB001',
        VoidDate = CURRENT_TIMESTAMP
      WHERE OrderID = @OrderID 
        AND AwardTitle NOT LIKE 'AMEX Gift Certificate%'
    END
    
    -- GET NEXT CANDIDATE
    SELECT @CurrID = MIN(ItemLineID) FROM inserted WHERE ItemLineID > @CurrID
    
  END
  
  SET NOCOUNT OFF
