USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ShoppingCartCheckout](
	[CartID] [int] NOT NULL,
	[ProgramItemId] [int] NOT NULL,
	[MasterCatalogId] [int] NOT NULL,
	[ItemDesc] [varchar](500) NOT NULL,
	[ItemType] [char](1) NOT NULL,
	[VendorNum] [int] NOT NULL,
	[Model] [varchar](50) NOT NULL,
	[Price] [smallmoney] NOT NULL,
	[Cost] [smallmoney] NOT NULL,
	[Shipping] [smallmoney] NOT NULL,
	[TaxRate] [smallmoney] NOT NULL,
	[ShipFlag] [char](1) NOT NULL,
	[ItemsInPkg] [tinyint] NOT NULL,
	[Token] [varchar](50) NOT NULL,
	[Added] [datetime] NOT NULL CONSTRAINT [DF_ShoppingCartCheckout_Added]  DEFAULT (getdate()),
 CONSTRAINT [PK_ShoppingCartCheckout] PRIMARY KEY CLUSTERED 
(
	[CartID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF