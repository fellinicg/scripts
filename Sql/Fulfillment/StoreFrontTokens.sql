USE [Fulfilment]
GO
/****** Object:  Table [dbo].[StoreFrontTokens]    Script Date: 09/22/2005 15:09:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StoreFrontTokens](
	[TokenID] [uniqueidentifier] NOT NULL,
	[UID] [int] NOT NULL,
	[PID] [int] NOT NULL,
	[CreatedOn] [smalldatetime] NOT NULL CONSTRAINT [DF_StoreFrontTokens_CreatedOn]  DEFAULT (getdate())
) ON [PRIMARY]

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF