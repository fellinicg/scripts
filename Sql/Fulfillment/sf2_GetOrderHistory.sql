USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: September 9, 2005
-- Description:	Order history for user for program
-- =============================================
ALTER PROCEDURE dbo.sf2_GetOrderHistory 
	@p_ID int, 
	@u_ID int 
AS
BEGIN
	DECLARE @pCode VARCHAR(10)
	DECLARE @clientid INT
	DECLARE @DKNo INT

	SET NOCOUNT ON

	-- Get extraneous program info
	SELECT @pCode = p_Code, @clientid = p_cl_ID, @DKNo = p_DKNo
	FROM SQL2KPROD.Catalog.dbo.Programs
	WHERE p_ID = @p_ID

	SELECT oh.OrderID, CONVERT(VARCHAR, oh.OrderDate, 0) 'OrderDate', SUM(al.TotalPoints) 'TotalPoints', 
	SUM(al.Quantity) 'Status'
	FROM dbo.OrderHeader oh
	INNER JOIN dbo.AwardLine al ON oh.OrderID = al.OrderID
	WHERE oh.UserID = @u_ID
	AND oh.ProjectNumber = @DKNo
	AND al.Void = 0
	GROUP BY oh.OrderID, OrderDate
	ORDER BY oh.OrderID DESC

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF