USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071004
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071004
-- ============================================================
CREATE PROCEDURE dbo.upAddStatus
(
	@StatusID char(1)
	,@Description varchar(30) = null
	,@Result char(1) output
)
	
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Add record
  INSERT INTO dbo.tbStatus (StatusID, Description)
  VALUES (@StatusID, @Description)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@ERROR = 0)
		SET @Result = 'S'

END