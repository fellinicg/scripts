USE [OnvantageReports]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[upDeleteProgramReport]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[upDeleteProgramReport]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080904
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080904
-- ============================================================
CREATE PROCEDURE dbo.upDeleteProgramReport
(
	@ExecProc varchar(40)
	,@DKNumber varchar(20)
	,@Result char(1) output
)
	
AS
BEGIN

	-- Declarations
	DECLARE @Sql varchar(500)
	DECLARE @ReportTable varchar(40)

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Initialize Variables
	SELECT @ReportTable = ReportTable
	FROM dbo.tbReport
	WHERE ExecProc = @ExecProc

	-- Create sql statement
	SET @Sql = 'DELETE FROM ' + @ReportTable + ' WHERE DKNumber = ''' + @DKNumber + ''''

	-- Delete records
	EXECUTE (@Sql)

	-- Verify success
	IF (@@ERROR = 0)
		SET @Result = 'S'

END
GO
GRANT EXECUTE
ON dbo.upDeleteProgramReport
TO adm_airconfirm