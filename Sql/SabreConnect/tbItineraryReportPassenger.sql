USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbItineraryReportPassenger]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbItineraryReportPassenger]
GO
CREATE TABLE [dbo].[tbItineraryReportPassenger](
	[ItineraryRepPassengerID] [int] IDENTITY(1,1) NOT NULL,
	[ReportLoadID] [int] NOT NULL,
	[BookingLocator] [char](6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[PersonNumber] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FirstName] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastName] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_tbItineraryReportPassenger] PRIMARY KEY CLUSTERED 
(
	[ItineraryRepPassengerID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF