USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[upAddItineraryReportPassenger]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[upAddItineraryReportPassenger]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080310
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080310
-- ============================================================
CREATE PROCEDURE dbo.upAddItineraryReportPassenger
(
	@ReportLoadID int
	,@BookingLocator char(6)
	,@PersonNumber varchar(10)
	,@FirstName varchar(100)
	,@LastName varchar(100)
	,@Result char(1) output
)
	
AS
BEGIN
	-- Declarations
	DECLARE @CreateDate datetime
	

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'
	SET @CreateDate = CURRENT_TIMESTAMP

	-- Add record
  INSERT INTO dbo.tbItineraryReportPassenger (ReportLoadID, BookingLocator, PersonNumber, 
		FirstName, LastName, CreateDate)
  VALUES (@ReportLoadID, @BookingLocator, @PersonNumber, @FirstName, @LastName, @CreateDate)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@ERROR = 0)
		SET @Result = 'S'

END
GO
GRANT EXECUTE
ON dbo.upAddItineraryReportPassenger
TO system