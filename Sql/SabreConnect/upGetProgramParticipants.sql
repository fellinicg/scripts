USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071015
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071015
-- ============================================================
ALTER PROCEDURE dbo.upGetProgramParticipants 
(	
	@ProgramID int
	,@Status char(1)
	,@UserID int
	,@IsAdmin bit = 0
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Return data
	SELECT p.ParticipantID
				,(p.FirstName + ' ' + p.LastName) FullName
				,g.GuestID
				,(g.FirstName + ' ' + g.LastName) GuestName
				, dbo.sfNumNotes(p.ParticipantID) NumNotes
				,p.Email
				,p.ProgramID
				,s.Description Status
				,i.ItineraryStatusCode ItinStatusCode
				,CASE ISNULL(i.AgentID, 0) WHEN 0 THEN null ELSE a.PersonBuiltFullName END Agent				,c.Description ItinStatus
				,CONVERT(varchar, i.CreateDate, 100) ItinStatusDate
				,LTRIM(p.BookingLocator) BookingLocator
				,LTRIM(g.BookingLocator) GuestBookingLocator
				,p.IsLocked
				,ISNULL(CASE p.LockedBy WHEN @UserID THEN 0 ELSE p.LockedBy END, 0) LockedBy
				,CASE ISNULL(p.LockedBy, 0) WHEN 0 THEN null ELSE l.PersonBuiltFullName END LockedByName
				,@IsAdmin IsAdmin
				,p.IsTicketed
				,g.IsTicketed GuestIsTicketed
				,@Status StatusCode
		FROM dbo.tbProgram pg
			INNER JOIN dbo.tbParticipant p ON pg.ProgramID = p.ProgramID AND pg.IsDeleted = 0
			LEFT JOIN dbo.tbGuest g ON p.ParticipantID = g.ParticipantID
			INNER JOIN dbo.tbParticipantStatusCode s ON p.Status = s.ParticipantStatusCode
			LEFT JOIN dbo.tbParticipantItineraryStatus i ON p.ItineraryStatus = i.ParticipantItineraryStatusID
			LEFT JOIN dbo.tbItineraryStatusCode c ON i.ItineraryStatusCode = c.ItineraryStatusCode
			LEFT JOIN dbo.vwMadisonEmployee a ON i.AgentID = a.UserAccountID
			LEFT JOIN dbo.vwMadisonEmployee l ON p.LockedBy = l.UserAccountID
		WHERE p.Status = @Status
			AND pg.ProgramID = @ProgramID
			AND p.IsDeleted = 0
		ORDER BY p.LastName, p.FirstName

END