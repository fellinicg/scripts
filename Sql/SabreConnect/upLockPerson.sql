USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071004
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071004
-- ============================================================
ALTER PROCEDURE dbo.upLockPerson
(
	@ParticipantID bigint
	,@LockedBy int
	,@Result char(1) output
)
	
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Make sure participant record is not locked
	IF (dbo.sfIsLocked(@ParticipantID, @LockedBy) = 1)
		BEGIN
			SET @Result = 'L'
			RETURN
		END

	-- Update record
	UPDATE dbo.tbParticipant SET IsLocked = 1, LockedBy = @LockedBy
	WHERE ParticipantID = @ParticipantID

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@ERROR = 0)
		SET @Result = 'S'

END