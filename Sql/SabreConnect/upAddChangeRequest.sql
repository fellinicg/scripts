USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071214
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071214
-- ============================================================
ALTER PROCEDURE dbo.upAddChangeRequest
(
	@ParticipantID bigint
	,@ProjectNumber char(10)
	,@Request varchar(2000)
	,@Result char(1) output
)
	
AS
BEGIN

	-- Declarations
	DECLARE @AddNoteResult char(1)

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'
	SET @Request = 'Change Request - ' + @Request


	-- Open transaction
	BEGIN TRAN

	-- Add record to tbNote
	EXECUTE dbo.upAddNote 
		 @ParticipantID
		,@Request
		,-1
		,@AddNoteResult OUTPUT

	-- Verify success
	IF (@AddNoteResult = 'S')
		SET @Result = 'S'
	ELSE
		GOTO CleanUp

	-- Update participants status
	UPDATE dbo.tbParticipant SET Status = 'Q'
	WHERE ParticipantID = @ParticipantID
		AND ProjectNumber = @ProjectNumber

	-- Verify success
	IF (@@ROWCOUNT <> 1 OR @@ERROR <> 0)
		BEGIN
			SET @Result = 'F'
			GOTO CleanUp
		END

	-- Add record to tbParticipantItineraryStatus
  INSERT INTO dbo.tbParticipantItineraryStatus (ParticipantID, ItineraryStatusCode)
  VALUES (@ParticipantID, 'P')

	-- Verify success
	IF (@@ROWCOUNT <> 1 OR @@ERROR <> 0)
		SET @Result = 'F'

CleanUp:
	-- Close transaction
	IF (@Result = 'S')
		COMMIT TRAN
	ELSE ROLLBACK TRAN

END
GO
GRANT EXECUTE
ON dbo.upAddChangeRequest
TO system