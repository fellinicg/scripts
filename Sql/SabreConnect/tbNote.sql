USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbNote](
	[NoteID] [int] IDENTITY(1,1) NOT NULL,
	[ParticipantID] [bigint] NOT NULL,
	[Note] [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AuthorID] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL CONSTRAINT [DF_tbNote_CreateDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbNote] PRIMARY KEY CLUSTERED 
(
	[NoteID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF