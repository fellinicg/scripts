USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071004
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071004
-- ============================================================
CREATE PROCEDURE dbo.upUpdateProgram
(
	@ProgramID int
	,@DKNumber int = null
	,@Description varchar(200) = null
	,@Result char(1) output
)
	
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Update record
  UPDATE dbo.tbProgram SET DKNumber = @DKNumber, Description = @Description
	WHERE ProgramID = @ProgramID

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@ERROR = 0)
		SET @Result = 'S'

END