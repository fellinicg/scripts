USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[upAddItineraryReportData]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[upAddItineraryReportData]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080306
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080307
-- ============================================================
CREATE PROCEDURE dbo.upAddItineraryReportData
(
	@ReportLoadID int
	,@BookingLocator char(6)
	,@SegmentID tinyint
	,@DepartureDate datetime
	,@DepartureTime varchar(11)
	,@DepartureAirportCode varchar(10)
	,@DepartureAirport varchar(200) 
	,@ArrivalDate datetime
	,@ArrivalTime varchar(11)
	,@ArrivalAirportCode varchar(10)
	,@ArrivalAirport varchar(200)
	,@CarrierCode varchar(5)
	,@Carrier varchar(100)
	,@FlightNumber varchar(50)
	,@Status varchar(20)
	,@StatusCode varchar(5)
	,@ConnectionInd varchar(5)
	,@Result char(1) output
)
	
AS
BEGIN

	-- Declarations
	DECLARE @CreateDate datetime

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'
	SET @CreateDate = CURRENT_TIMESTAMP

	-- Add record
  INSERT INTO dbo.tbItineraryReport (ReportLoadID, BookingLocator, SegmentID, DepartureDate, 
		DepartureTime, DepartureAirportCode, DepartureAirport, ArrivalDate, ArrivalTime, ArrivalAirportCode, 
		ArrivalAirport, CarrierCode, Carrier, FlightNumber, Status, StatusCode, ConnectionInd, CreateDate)
  VALUES (@ReportLoadID, @BookingLocator, @SegmentID, @DepartureDate, @DepartureTime, 
		@DepartureAirportCode, @DepartureAirport, @ArrivalDate, @ArrivalTime, @ArrivalAirportCode, 
		@ArrivalAirport, @CarrierCode, @Carrier, @FlightNumber, @Status, @StatusCode, @ConnectionInd, @CreateDate)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@ERROR = 0)
		SET @Result = 'S'

END
GO
GRANT EXECUTE
ON dbo.upAddItineraryReportData
TO system