USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[upGetBookingLocatorsForProgram]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[upGetBookingLocatorsForProgram]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080305
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080305
-- ============================================================
CREATE PROCEDURE dbo.upGetBookingLocatorsForProgram
(
	@ProgramID int
)
	
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Return data
	SELECT 
		p.BookingLocator
	FROM dbo.tbParticipant p
	WHERE ProgramID = @ProgramID
		AND p.BookingLocator IS NOT NULL
	UNION 
	SELECT 
		g.BookingLocator
	FROM dbo.tbParticipant p
		INNER JOIN dbo.tbGuest g ON p.ParticipantID = g.ParticipantID
	WHERE ProgramID = @ProgramID
		AND g.BookingLocator IS NOT NULL

END
GO
GRANT EXECUTE
ON dbo.upGetBookingLocatorsForProgram
TO system