USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071004
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071004
-- ============================================================
ALTER PROCEDURE dbo.upAddNote
(
	@ParticipantID bigint
	,@Note varchar(2000)
	,@AuthorID int
	,@Result char(1) output
)
	
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Add record
  INSERT INTO dbo.tbNote (ParticipantID, Note, AuthorID)
  VALUES (@ParticipantID, @Note, @AuthorID)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@ERROR = 0)
		SET @Result = 'S'

END