USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080117
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080117
-- ============================================================
ALTER PROCEDURE dbo.upAuthenticateTravelRegUser
(
	@ProgramID int
	,@TravelRegLogin varchar(100)
)
	
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Return data
	SELECT 
		pax.ParticipantID
		,pax.FirstName
		,pax.LastName
		,pax.InvitationURL
		,REPLACE(pax.InvitationURL, 'http://', 'https://') SecureInvitationURL
	FROM dbo.tbParticipant pax
	WHERE pax.TravelRegLogin = @TravelRegLogin
		AND pax.ProgramID = @ProgramID

END
GO
GRANT EXECUTE
ON dbo.upAuthenticateTravelRegUser
TO system, travelreg