USE [SabreConnect]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbAirline](
	[AirlineID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [char](2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OrgDesc] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbAirline_IsDeleted]  DEFAULT (0)
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF