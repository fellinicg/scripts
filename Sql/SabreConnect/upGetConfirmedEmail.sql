USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080109
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080109
-- ============================================================
ALTER PROCEDURE dbo.upGetConfirmedEmail
(
	@ParticipantID bigint
	,@UserID int
	,@OverrideLock bit = 0
)
	
AS
BEGIN

	-- Declarations
	DECLARE @Result char(1)

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'S'

	-- Make sure participant record is not locked
	IF (@OverrideLock = 0)
		IF (dbo.sfIsLocked(@ParticipantID, @UserID) = 1)
			SET @Result = 'L'

	-- Return data
	SELECT 
		pax.FirstName + ' ' + pax.LastName + ' [' + pax.Email + ']' EmailTo
		,prg.ReplyToEmail EmailFrom
		,prg.ConfirmedEmailSubject Subject
		,prg.ConfirmedEmailText Message
		,ISNULL(pax.BookingLocator, '') BookingLocator
		,ISNULL(gst.BookingLocator, '') GuestBookingLocator
		,REVERSE(CONVERT(varchar, pax.ProgramID) + '-' + CONVERT(varchar, pax.ParticipantID) + '-' + pax.ProjectNumber) Token
		,@Result Result
	FROM dbo.tbParticipant pax
		INNER JOIN dbo.tbProgram prg ON pax.ProgramID = prg.ProgramID
		LEFT JOIN dbo.tbGuest gst ON pax.ParticipantID = gst.ParticipantID
	WHERE pax.ParticipantID = @ParticipantID

END
GO
GRANT EXECUTE
ON dbo.upGetConfirmedEmail
TO system