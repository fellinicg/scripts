USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[upDeleteProgram]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[upDeleteProgram]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080218
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080903
-- ============================================================
CREATE PROCEDURE dbo.upDeleteProgram
(
	@ProgramID bigint
	,@Result char(1) output
)
	
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Open transaction
	BEGIN TRAN

	-- Delete guests associated with participants of the program to be deleted
	DELETE FROM dbo.tbGuest
	FROM dbo.tbGuest g
	INNER JOIN dbo.tbParticipant p ON g.ParticipantID = p.ParticipantID
	WHERE p.ProgramID = @ProgramID

	-- Verify success
	IF (@@ERROR <> 0)
		GOTO CleanUp

	-- Delete notes associated with participants of the program to be deleted
	DELETE FROM dbo.tbNote
	FROM dbo.tbNote n
	INNER JOIN dbo.tbParticipant p ON n.ParticipantID = p.ParticipantID
	WHERE p.ProgramID = @ProgramID

	-- Verify success
	IF (@@ERROR <> 0)
		GOTO CleanUp

	-- Delete itinerary statuses associated with participants of the program to be deleted
	DELETE FROM dbo.tbParticipantItineraryStatus
	FROM dbo.tbParticipantItineraryStatus i
	INNER JOIN dbo.tbParticipant p ON i.ParticipantID = p.ParticipantID
	WHERE p.ProgramID = @ProgramID

	-- Verify success
	IF (@@ERROR <> 0)
		GOTO CleanUp

	-- Delete participants of the program to be deleted
	DELETE FROM dbo.tbParticipant
	FROM dbo.tbParticipant p 
	WHERE p.ProgramID = @ProgramID

	-- Verify success
	IF (@@ERROR <> 0)
		GOTO CleanUp

	-- Delete program
	DELETE FROM dbo.tbProgram
	WHERE ProgramID = @ProgramID

	-- Verify success
	IF (@@ERROR <> 0)
		GOTO CleanUp

	-- Delete Air Reports for program
	EXECUTE dbo.upDeleteItineraryReportForProgram	 @ProgramID, @Result OUTPUT

CleanUp:
	-- Close transaction
	IF (@Result = 'S')
		COMMIT TRAN
	ELSE ROLLBACK TRAN

END
GO
GRANT EXECUTE
ON dbo.upDeleteProgram
TO adm_airconfirm