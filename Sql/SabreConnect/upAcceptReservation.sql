USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071214
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071214
-- ============================================================
ALTER PROCEDURE dbo.upAcceptReservation
(
	@ParticipantID bigint
	,@ProjectNumber char(10)
	,@Result char(1) output
)
	
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Open transaction
	BEGIN TRAN

	-- Update participants status
	UPDATE dbo.tbParticipant SET Status = 'C'
	WHERE ParticipantID = @ParticipantID
		AND ProjectNumber = @ProjectNumber

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@ERROR = 0)
		SET @Result = 'S'
	ELSE
		GOTO CleanUp

	-- Add record to tbParticipantItineraryStatus
  INSERT INTO dbo.tbParticipantItineraryStatus (ParticipantID, ItineraryStatusCode)
  VALUES (@ParticipantID, 'C')

	-- Verify success
	IF (@@ROWCOUNT <> 1 OR @@ERROR <> 0)
		SET @Result = 'F'

CleanUp:
	-- Close transaction
	IF (@Result = 'S')
		COMMIT TRAN
	ELSE ROLLBACK TRAN

END
GO
GRANT EXECUTE
ON dbo.upAcceptReservation
TO system