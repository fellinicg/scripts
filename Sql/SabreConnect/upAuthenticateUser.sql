USE AirConfirm
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071011
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071011
-- ============================================================
ALTER PROCEDURE [dbo].[upAuthenticateUser]
(
	@LoginName varchar(20)
	,@Password varchar(20)
	,@UserID int output
	,@UserType char(1) output
	,@FirstName varchar(40) output
	,@LastName varchar(60) output
	,@Email varchar(120) output
	,@IsAdmin bit output
	,@GroupID tinyint output
	,@Result char(1) output
)
	
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Authenticate login name and password
	SELECT 
		@UserID = v.u_ID 
		,@UserType = v.u_Type 
		,@FirstName = v.u_FirstName 
		,@LastName = v.u_LastName 
		,@Email = ISNULL(v.em_Address , '')
		,@IsAdmin = CASE ug.ug_gr_ID WHEN 36 THEN 1 ELSE 0 END
		,@GroupID = ug.ug_gr_ID
	FROM OPENQUERY(SQL2KPROD, 'SELECT u_ID,u_Type,u_FirstName,u_LastName,em_Address,u_UserName,gu_rs_ID
															FROM MadisonPortal.dbo.vwUsers') v
		INNER JOIN OPENQUERY(SQL2KPROD, 'SELECT u_ID, Master.dbo.ud_CCDecrypt(u_Password) u_Password FROM MadisonPortal.dbo.Users') u ON v.u_ID = u.u_ID
		INNER JOIN OPENQUERY(SQL2KPROD, 'SELECT ug_gr_ID, ug_u_ID FROM MadisonPortal.dbo.UserGroups') ug ON u.u_ID = ug.ug_u_ID
	WHERE v.u_UserName = @LoginName
		AND u.u_Password = @Password
		AND v.gu_rs_ID = 0
		AND ug.ug_gr_ID IN (35,36,38)

	IF (@@ROWCOUNT > 0)
		SET @Result = 'S'

END
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

