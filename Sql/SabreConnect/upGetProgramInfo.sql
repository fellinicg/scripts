USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071018
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071018
-- ============================================================
ALTER PROCEDURE dbo.upGetProgramInfo (@ProgramID int)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Return data
	SELECT ProgramID, DKNumber, Description, ReplyToEmail, ConfirmationEmailSubject,
		ConfirmationEmailText, ConfirmedEmailSubject, ConfirmedEmailText,
		IsDeleted, CreateDate
	FROM dbo.tbProgram
	WHERE ProgramID = @ProgramID

END