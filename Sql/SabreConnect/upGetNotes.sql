USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071026
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080109
-- ============================================================
ALTER PROCEDURE dbo.upGetNotes
(
	@ParticipantID bigint
)
	
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Return data
	SELECT n.ParticipantID
		,n.Note
		,CASE n.Authorid WHEN -1 THEN p.FirstName + ' ' + p.LastName ELSE dbo.sfGetUsersFullName(AuthorID) END Author
		,CONVERT(varchar, n.CreateDate, 100) AddedOn
	FROM dbo.tbNote n
		LEFT JOIN dbo.tbParticipant p ON n.ParticipantID = p.ParticipantID
	WHERE n.ParticipantID = @ParticipantID
	ORDER BY n.CreateDate DESC

END