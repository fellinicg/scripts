USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbReportLoad]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbReportLoad]
GO
CREATE TABLE [dbo].[tbReportLoad](
	[ReportLoadID] [int] IDENTITY(1,1) NOT NULL,
	[ProgramID] [int] NOT NULL,
	[ProjectNumber] [char](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Type] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Status] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[UserID] [int] NOT NULL,
	[SendToEmail] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CreateDate] [datetime] NOT NULL CONSTRAINT [DF_tbReportLoad_CreateDate]  DEFAULT (getdate()),
	[LastUpdate] [datetime] NULL,
 CONSTRAINT [PK_tbReportLoad] PRIMARY KEY CLUSTERED 
(
	[ReportLoadID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbReportLoad] ADD CONSTRAINT [CK_Status] CHECK  (([Status] = 'C' or ([Status] = 'P' or [Status] = 'U')))

GO
ALTER TABLE [dbo].[tbReportLoad] ADD CONSTRAINT [CK_Type] CHECK  ([Type] = 'I')
