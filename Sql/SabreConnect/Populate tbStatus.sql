INSERT INTO dbo.tbStatus (StatusID, Description) VALUES ('C', 'Cancelled')GO
INSERT INTO dbo.tbStatus (StatusID, Description) VALUES ('G', 'Registered / Non-Confirmed')GO
INSERT INTO dbo.tbStatus (StatusID, Description) VALUES ('N', 'Non-Registered')GO
INSERT INTO dbo.tbStatus (StatusID, Description) VALUES ('Q', 'Change Request')GO
INSERT INTO dbo.tbStatus (StatusID, Description) VALUES ('R', 'Registered / Confirmed')