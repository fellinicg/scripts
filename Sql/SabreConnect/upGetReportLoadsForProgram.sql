USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[upGetReportLoadsForProgram]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[upGetReportLoadsForProgram]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080312
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080312
-- ============================================================
CREATE PROCEDURE dbo.upGetReportLoadsForProgram
(
	@ProgramID int
	,@Status char(1)
)
	
AS
BEGIN
	-- Initialize settings
	SET NOCOUNT ON

	-- Check @Status
	IF @Status = 'A'
		-- Return all data
		SELECT ReportLoadID, Type, Status, CONVERT(varchar, LastUpdate, 100) LastUpdate
		FROM dbo.tbReportLoad
		WHERE ProgramID = @ProgramID
		ORDER BY CONVERT(datetime, LastUpdate) DESC
	ELSE
		-- Return data for status
		SELECT ReportLoadID, Type, Status, CONVERT(varchar, LastUpdate, 100) LastUpdate
		FROM dbo.tbReportLoad
		WHERE ProgramID = @ProgramID
			AND Status = @Status
		ORDER BY CONVERT(datetime, LastUpdate) DESC

END
GO
GRANT EXECUTE
ON dbo.upGetReportLoadsForProgram
TO system