USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbParticipant](
	[ParticipantID] [bigint] NOT NULL,
	[FirstName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LastName] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Email] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ProgramID] [int] NOT NULL,
	[Status] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ItineraryStatus] [int] NULL,
	[TravelRegLogin] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InvitationURL] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[BookingLocator] [char](6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsTicketed] [bit] NOT NULL CONSTRAINT [DF_tbParticipant_IsTicketed]  DEFAULT (0),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbParticipant_IsDeleted]  DEFAULT (0),
	[IsLocked] [bit] NOT NULL CONSTRAINT [DF_tbParticipant_IsLocked]  DEFAULT (0),
	[LockedBy] [int] NULL,
	[CreateDate] [datetime] NOT NULL CONSTRAINT [DF_tbParticipant_CreateDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbParticipant] PRIMARY KEY CLUSTERED 
(
	[ParticipantID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF