USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[upGetManifestItineraryReport]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[upGetManifestItineraryReport]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080313
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080313
-- ============================================================
CREATE PROCEDURE dbo.upGetManifestItineraryReport
(
	@ReportLoadID int
)
	
AS
BEGIN
	-- Declarations
	DECLARE @ProjectNumber char(10)

	-- Initialize settings
	SET NOCOUNT ON

	-- Get Project Number
	SELECT @ProjectNumber = ProjectNumber
	FROM dbo.tbReportLoad
	WHERE ReportLoadID = @ReportLoadID

	-- Return data
	SELECT @ProjectNumber ProjectNumber, irp.PersonNumber, irp.FirstName, irp.LastName, ir.BookingLocator, 
		ir.CarrierCode, ir.Carrier, ir.FlightNumber, ir.DepartureDate, ir.DepartureTime, ir.DepartureAirport, 
		ir.ArrivalAirport, ir.ArrivalTime, ir.Status
	FROM dbo.tbItineraryReport ir
	INNER JOIN dbo.tbItineraryReportPassenger irp ON ir.ReportLoadID = irp.ReportLoadID 
		AND ir.BookingLocator = irp.BookingLocator
	WHERE ir.ReportLoadID = @ReportLoadID
	ORDER BY irp.LastName, irp.FirstName, ir.DepartureDate

END
GO
GRANT EXECUTE
ON dbo.upGetManifestItineraryReport
TO system