USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[upGetArrivalAirports]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[upGetArrivalAirports]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080404
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080404
-- ============================================================
CREATE PROCEDURE dbo.upGetArrivalAirports
(
	@ReportLoadID int
	,@FromDate varchar(10)
	,@ToDate varchar(10)
)
	
AS
BEGIN
	-- Initialize settings
	SET NOCOUNT ON

	-- Return data
	SELECT DISTINCT ArrivalAirportCode AirportCode, ArrivalAirport Airport
	FROM dbo.tbItineraryReport
	WHERE ReportLoadID = @ReportLoadID
		AND DepartureDate BETWEEN @FromDate AND @ToDate
	ORDER BY ArrivalAirportCode

END
GO
GRANT EXECUTE
ON dbo.upGetArrivalAirports
TO system