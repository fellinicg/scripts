USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080218
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080218
-- ============================================================
ALTER PROCEDURE dbo.upDeleteParticipant
(
	@ParticipantID bigint
	,@Result char(1) output
)
	
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Open transaction
	BEGIN TRAN

	-- Delete guest associated with the participant
	DELETE FROM dbo.tbGuest
	WHERE ParticipantID = @ParticipantID

	-- Verify success
	IF (@@ERROR <> 0)
		GOTO CleanUp

	-- Delete notes associated with participant
	DELETE FROM dbo.tbNote
	WHERE ParticipantID = @ParticipantID

	-- Verify success
	IF (@@ERROR <> 0)
		GOTO CleanUp

	-- Delete itinerary statuses associated with participant
	DELETE FROM dbo.tbParticipantItineraryStatus
	WHERE ParticipantID = @ParticipantID

	-- Verify success
	IF (@@ERROR <> 0)
		GOTO CleanUp

	-- Delete participant
	DELETE FROM dbo.tbParticipant
	WHERE ParticipantID = @ParticipantID

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@ERROR = 0)
		SET @Result = 'S'

CleanUp:
	-- Close transaction
	IF (@Result = 'S')
		COMMIT TRAN
	ELSE ROLLBACK TRAN

END
GO
GRANT EXECUTE
ON dbo.upDeleteParticipant
TO adm_airconfirm