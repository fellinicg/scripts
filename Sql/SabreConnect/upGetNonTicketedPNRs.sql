USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071212
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071212
-- ============================================================
ALTER PROCEDURE dbo.upGetNonTicketedPNRs
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Return data
	SELECT DISTINCT p.BookingLocator
	FROM dbo.tbParticipant p
		INNER JOIN dbo.tbParticipantItineraryStatus i ON p.ItineraryStatus = i.ParticipantItineraryStatusID
	WHERE p.IsDeleted = 0
		AND p.BookingLocator IS NOT NULL
		AND p.IsTicketed = 0
		AND p.Status NOT IN ('X')
		AND i.ItineraryStatusCode NOT IN ('B')
	UNION
	SELECT g.BookingLocator
	FROM dbo.tbParticipant p
		INNER JOIN dbo.tbParticipantItineraryStatus i ON p.ItineraryStatus = i.ParticipantItineraryStatusID
		INNER JOIN dbo.tbGuest g ON p.ParticipantID = g.ParticipantID
	WHERE p.IsDeleted = 0
		AND g.BookingLocator IS NOT NULL
		AND g.IsTicketed = 0
		AND p.Status NOT IN ('X')
		AND i.ItineraryStatusCode NOT IN ('B')
END
GO
GRANT EXECUTE
ON dbo.upGetNonTicketedPNRs
TO system