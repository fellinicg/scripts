USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE dbo.tbChangeRequest(
	[ChangeRequestID] [int] IDENTITY(1,1) NOT NULL,
	[ParticipantID] [bigint] NOT NULL,
	[ProjectNumber] [char](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ParticipantRequest] [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CreateDate] [datetime] NOT NULL CONSTRAINT [DF_tbChangeRequest_CreateDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbChangeRequest] PRIMARY KEY CLUSTERED 
(
	[ChangeRequestID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF