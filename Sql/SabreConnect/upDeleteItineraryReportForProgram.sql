USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[upDeleteItineraryReportForProgram]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[upDeleteItineraryReportForProgram]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080903
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080903
-- ============================================================
CREATE PROCEDURE dbo.upDeleteItineraryReportForProgram
(
	@ProgramID int
	,@Result char(1) output
)
	
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Delete records
	DELETE FROM dbo.tbItineraryReportPassenger
	FROM dbo.tbReportLoad rl
	LEFT JOIN dbo.tbItineraryReportPassenger irp ON rl.ReportLoadID = irp.ReportLoadID
	WHERE ProgramID = @ProgramID

	-- Verify success
	IF (@@ERROR <> 0)
		RETURN

	-- Delete records
	DELETE FROM dbo.tbItineraryReport
	FROM dbo.tbReportLoad rl
	LEFT JOIN dbo.tbItineraryReport ir ON rl.ReportLoadID = ir.ReportLoadID
	WHERE ProgramID = @ProgramID

	-- Verify success
	IF (@@ERROR <> 0)
		RETURN

	-- Delete records
  DELETE FROM dbo.tbReportLoad
	WHERE ProgramID = @ProgramID

	-- Verify success
	IF (@@ERROR = 0)
		SET @Result = 'S'

END
GO
GRANT EXECUTE
ON dbo.upDeleteItineraryReportForProgram
TO adm_airconfirm