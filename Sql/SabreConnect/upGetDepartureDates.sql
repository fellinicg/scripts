USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[upGetDepartureDates]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[upGetDepartureDates]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080404
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080404
-- ============================================================
CREATE PROCEDURE dbo.upGetDepartureDates
(
	@ReportLoadID int
	,@FromDate varchar(10)
)
	
AS
BEGIN
	-- Initialize settings
	SET NOCOUNT ON

	-- Set FromDate if BLANK
	IF LEN(LTRIM(RTRIM(@FromDate))) = 0
		SET @FromDate = '1/1/1900'

	-- Return data
	SELECT DISTINCT CONVERT(varchar, DepartureDate, 101) Dates
	FROM dbo.tbItineraryReport
	WHERE ReportLoadID = @ReportLoadID
		AND DepartureDate >= @FromDate
	ORDER BY Dates

END
GO
GRANT EXECUTE
ON dbo.upGetDepartureDates
TO system