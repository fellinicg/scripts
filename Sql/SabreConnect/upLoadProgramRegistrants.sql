USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071031
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071031
-- ============================================================
ALTER PROCEDURE dbo.upLoadProgramRegistrants 
(	
	@InputFile varchar(50)
	, @Development bit = 0
	, @RecordsUpdated int output
	, @RecordsAdded int output
	, @Result char(1) output
)
AS
BEGIN

	-- Declarations
	DECLARE @bcpcmd varchar(5000)
	DECLARE @timestamp varchar(20)
	DECLARE @tab varchar(50)
	DECLARE @ParticipantStatus char(1)

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'
	SET @RecordsAdded = 0
	SET @RecordsUpdated = 0
	SET @timestamp = CONVERT(VARCHAR, CURRENT_TIMESTAMP, 112) + '_' + 
		CONVERT(VARCHAR, DATEPART(hh, CURRENT_TIMESTAMP)) + 'h' + 
		CONVERT(VARCHAR, DATEPART(mi, CURRENT_TIMESTAMP)) + 'm' + 
		CONVERT(VARCHAR, DATEPART(ss, CURRENT_TIMESTAMP)) + 's'
	SET @tab = RTRIM(LEFT(@InputFile, CHARINDEX('.XLS', @InputFile) - 1))
	SET @ParticipantStatus = 'R'

	-- Map drive
	IF (@Development = 1)
		SET @bcpcmd = 'net use z: /delete&net use z: \\' + @@SERVERNAME + '\AirConfirmUpload$'
	ELSE
		SET @bcpcmd = 'net use z: /delete&net use z: \\web2003prod5a\AirConfirmUpload$'
	EXEC master.dbo.xp_cmdshell @bcpcmd

	-- Copy file to SQL server
	SET @bcpcmd = 'copy z:\' + @InputFile + ' d:\' + @InputFile
	EXEC master.dbo.xp_cmdshell @bcpcmd

	-- Create temporary table sowe don't need to constantly hit the XLS file
	CREATE TABLE #Onvantage
	(
		PrimaryID bigint,
		PersonID bigint,
		FirstName varchar(50),
		LastName varchar(100),
		Email varchar(100),
		PrimaryReg bit
	)

	-- Populate temporary table
	EXEC ('INSERT INTO #Onvantage
				SELECT [Primary ID],
					 [Participant ID], [First Name], [Last Name], [Business Email], 
					 CASE [Primary Registrant] WHEN ''yes'' THEN 1 ELSE 0 END
--					 CASE [Primary Registrant] WHEN ''Primary'' THEN 1 ELSE 0 END
				FROM OPENROWSET(''Microsoft.Jet.OLEDB.4.0'', 
					 ''Excel 8.0;Database=D:\' + @InputFile + ''', ''SELECT [Primary ID],
					 [Participant ID], [First Name], [Last Name], [Business Email], 
					 [Primary Registrant] FROM [' + @tab + '$]'')
					 AS a')

	-- Update PENDING participants
	UPDATE dbo.tbParticipant SET
		FirstName = a.FirstName
		,LastName = a.LastName
		,Email = a.Email
		,Status = @ParticipantStatus 
	FROM #Onvantage a
	INNER JOIN dbo.tbParticipant p ON a.PersonID = p.ParticipantID
	WHERE a.PrimaryReg = 1
		AND p.Status IN ('P')

	-- Store number of records added
	SET @RecordsUpdated = @@ROWCOUNT

	-- Check for error
	IF (@@ERROR <> 0)	
		GOTO CleanUp

	-- Insert records for guests
	INSERT INTO dbo.tbGuest (GuestID, FirstName, LastName, ParticipantID, ProjectNumber)
	SELECT a.PersonID, a.FirstName, a.LastName, p.ParticipantID, p.ProjectNumber
	FROM #Onvantage a
	INNER JOIN dbo.tbParticipant p ON a.PrimaryID = p.ParticipantID
	LEFT JOIN dbo.tbGuest g ON a.PersonID = g.GuestID
	WHERE a.PrimaryReg = 0
		AND g.GuestID IS NULL

	-- Store number of records added
	SET @RecordsAdded = @@ROWCOUNT

	-- Check for error
	IF (@@ERROR <> 0)	
		GOTO CleanUp

	-- Archive files
	SET @bcpcmd = 'move z:\' + @InputFile + ' z:\Archive\' + @timestamp + '_' + @InputFile
	EXECUTE master.dbo.xp_cmdshell @bcpcmd
	
	-- Success
	SET @Result = 'S'

CleanUp:
	-- Delete temporary table
	IF (OBJECT_ID('tempdb..#Onvantage') IS NOT NULL)
		DROP TABLE #Onvantage

	-- Remove mapped drive
	EXECUTE master.dbo.xp_cmdshell 'net use z: /delete'
	
	-- Delete file from SQL server
	SET @bcpcmd = 'del d:\' + @InputFile
	EXECUTE master.dbo.xp_cmdshell @bcpcmd

END