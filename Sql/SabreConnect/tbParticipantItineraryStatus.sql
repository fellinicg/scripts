USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbParticipantItineraryStatus](
	[ParticipantItineraryStatusID] [int] IDENTITY(1,1) NOT NULL,
	[ParticipantID] [bigint] NOT NULL,
	[ItineraryStatusCode] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AgentID] [int] NULL,
	[CreateDate] [datetime] NOT NULL CONSTRAINT [DF_tbParticipantItineraryStatus_CreateDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbParticipantItineraryStatus] PRIMARY KEY CLUSTERED 
(
	[ParticipantItineraryStatusID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF