USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071031
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071031
-- ============================================================
ALTER PROCEDURE dbo.upAddParticipantItineraryStatus
(
	@ParticipantID bigint
	,@ItineraryStatusCode char(1)
	,@AgentID int = null
	,@Result char(1) output
)
	
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Make sure participant record is not locked
	IF (dbo.sfIsLocked(@ParticipantID, @AgentID) = 1)
		BEGIN
			SET @Result = 'L'
			RETURN
		END

	-- Add record
  INSERT INTO dbo.tbParticipantItineraryStatus (ParticipantID, ItineraryStatusCode, AgentID)
  VALUES (@ParticipantID, UPPER(@ItineraryStatusCode), @AgentID)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@ERROR = 0)
		SET @Result = 'S'

END
GO
GRANT EXECUTE
ON dbo.upAddParticipantItineraryStatus
TO system