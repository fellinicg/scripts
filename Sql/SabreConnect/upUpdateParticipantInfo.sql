USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071026
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080116
-- ============================================================
ALTER PROCEDURE dbo.upUpdateParticipantInfo 
(
	@ParticipantID bigint
	,@FirstName varchar(50)
	,@LastName varchar(100)
	,@Email varchar(100)
	,@TravelRegLogin varchar(100)
	,@BookingLocator char(6)
	,@IsTicketed bit
	,@IsDeleted bit
	,@Result char(1) output
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Initialize parameters
	IF (LEN(LTRIM(RTRIM(@BookingLocator))) = 0)
		SET @BookingLocator = null

	-- Update record
	UPDATE dbo.tbParticipant SET 
		FirstName = @FirstName
		,LastName = @LastName
		,Email = @Email
		,TravelRegLogin = @TravelRegLogin
		,BookingLocator = @BookingLocator
		,IsTicketed = @IsTicketed
		,IsDeleted = @IsDeleted
	WHERE ParticipantID = @ParticipantID

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@ERROR = 0)
		SET @Result = 'S'

END