USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE dbo.tbUserSetting(
	[UserAccountID] [int] NOT NULL,
	[AlwaysRefresh] [bit] NOT NULL CONSTRAINT [DF_tbUserSetting_AlwaysRefresh]  DEFAULT (1)
) ON [PRIMARY]
