USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071026
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071026
-- ============================================================
ALTER FUNCTION dbo.sfGetUsersFullName (@UserID int)
	RETURNS varchar(200) AS
BEGIN
	DECLARE @Ret varchar(200)

	SET @Ret = @UserID

	SELECT @Ret = u_FirstName + ' ' + u_LastName
	FROM OPENQUERY(SQL2KPROD, 'SELECT u_ID,u_FirstName,u_LastName FROM MadisonPortal.dbo.vwUsers')
	WHERE u_ID = @UserID

	RETURN(@Ret)
END
