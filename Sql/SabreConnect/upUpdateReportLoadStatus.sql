USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[upUpdateReportLoadStatus]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[upUpdateReportLoadStatus]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080307
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080307
-- ============================================================
CREATE PROCEDURE dbo.upUpdateReportLoadStatus
(
	@ReportLoadID int
	,@Status char(1)
	,@Result char(1) output
)
	
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Update record
	UPDATE dbo.tbReportLoad SET Status = UPPER(@Status), LastUpdate = CURRENT_TIMESTAMP
	WHERE ReportLoadID = @ReportLoadID

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@ERROR = 0)
		SET @Result = 'S'

END
GO
GRANT EXECUTE
ON dbo.upUpdateReportLoadStatus
TO system