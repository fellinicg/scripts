
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071226
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071226
-- ============================================================
CREATE VIEW dbo.vwMadisonEmployee
AS
	SELECT UserAccountID, PersonBuiltFullName
	FROM OPENQUERY(SQL2KPROD, 'SELECT UserAccountID, PersonBuiltFullName FROM GlobalUsers.dbo.tbUserAccount WHERE AccountType = ''M''')
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

