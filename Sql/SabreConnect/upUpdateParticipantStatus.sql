USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071026
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071031
-- ============================================================
ALTER PROCEDURE dbo.upUpdateParticipantStatus 
(
	@ParticipantID bigint
	,@Status char(1)
	,@UserID int
	,@Result char(1) output
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'
	SET @Status = UPPER(@Status)

	-- Make sure participant record is not locked
	IF (dbo.sfIsLocked(@ParticipantID, @UserID) = 1)
		BEGIN
			SET @Result = 'L'
			RETURN
		END

	-- Update record
	UPDATE dbo.tbParticipant SET Status = @Status
	WHERE ParticipantID = @ParticipantID

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@ERROR = 0)
		SET @Result = 'S'

	-- Add itnierary status on successful update
	IF (@Result = 'S')
		BEGIN
			IF (@Status IN ('P', 'Q', 'R'))
				EXEC dbo.upAddParticipantItineraryStatus @ParticipantID, 'P', @UserID, @Result OUTPUT
			IF (@Status = 'X')
				EXEC dbo.upAddParticipantItineraryStatus @ParticipantID, 'X', @UserID, @Result OUTPUT
		END
END