USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[upGetReportLoads]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[upGetReportLoads]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080307
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080312
-- ============================================================
CREATE PROCEDURE dbo.upGetReportLoads
(
	@Status char(1)
)
	
AS
BEGIN
	-- Initialize settings
	SET NOCOUNT ON

	-- Check @Status
	IF @Status = 'A'
		-- Return all data
		SELECT ReportLoadID, ProgramID, ProjectNumber, Type, Status, SendToEmail, LastUpdate
		FROM dbo.tbReportLoad
	ELSE
		-- Return data for status
		SELECT ReportLoadID, ProgramID, ProjectNumber, Type, Status, SendToEmail, LastUpdate
		FROM dbo.tbReportLoad
		WHERE Status = @Status

END
GO
GRANT EXECUTE
ON dbo.upGetReportLoads
TO system