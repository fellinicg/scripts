USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071113
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071113
-- ============================================================
ALTER PROCEDURE dbo.upUpdateGuestInfo 
(
	@GuestID bigint
	,@FirstName varchar(50)
	,@LastName varchar(100)
	,@BookingLocator char(6)
	,@IsTicketed bit
	,@Result char(1) output
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Initialize parameters
	IF (LEN(LTRIM(RTRIM(@BookingLocator))) = 0)
		SET @BookingLocator = null

	-- Update record
	UPDATE dbo.tbGuest SET 
		FirstName = @FirstName
		,LastName = @LastName
		,BookingLocator = @BookingLocator
		,IsTicketed = @IsTicketed
	WHERE GuestID = @GuestID

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@ERROR = 0)
		SET @Result = 'S'

END
GO
GRANT EXECUTE
ON dbo.upUpdateGuestInfo
TO system