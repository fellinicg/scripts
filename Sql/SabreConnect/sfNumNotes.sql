USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071015
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071015
-- ============================================================
CREATE FUNCTION dbo.sfNumNotes (@ParticipantID bigint)
	RETURNS int AS
BEGIN
	DECLARE @Ret int

	SET @Ret = 0

	SELECT @Ret = COUNT(NoteID) FROM dbo.tbNote WHERE ParticipantID = @ParticipantID

	RETURN(@Ret)
END
