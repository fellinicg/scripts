USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071105
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071105
-- ============================================================
ALTER PROCEDURE dbo.upGetConfirmationEmail
(
	@ParticipantID bigint
	,@UserID int
)
	
AS
BEGIN

	-- Declarations
	DECLARE @Result char(1)

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'S'

	-- Make sure participant record is not locked
	IF (dbo.sfIsLocked(@ParticipantID, @UserID) = 1)
		SET @Result = 'L'

	-- Return data
	SELECT 
		pax.FirstName + ' ' + pax.LastName + ' [' + pax.Email + ']' EmailTo
		,prg.ReplyToEmail EmailFrom
		,prg.ConfirmationEmailSubject Subject
		,prg.ConfirmationEmailText Message
		,ISNULL(pax.BookingLocator, '') BookingLocator
		,ISNULL(gst.BookingLocator, '') GuestBookingLocator
		,REVERSE(CONVERT(varchar, pax.ProgramID) + '-' + CONVERT(varchar, pax.ParticipantID) + '-' + pax.ProjectNumber) Token
		,@Result Result
	FROM dbo.tbParticipant pax
		INNER JOIN dbo.tbProgram prg ON pax.ProgramID = prg.ProgramID
		LEFT JOIN dbo.tbGuest gst ON pax.ParticipantID = gst.ParticipantID
	WHERE pax.ParticipantID = @ParticipantID

END
GO
GRANT EXECUTE
ON dbo.upGetConfirmationEmail
TO system