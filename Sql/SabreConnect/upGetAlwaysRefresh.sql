USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'dbo.upGetAlwaysRefresh') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE dbo.upGetAlwaysRefresh
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080912
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080912
-- ============================================================
CREATE PROCEDURE dbo.upGetAlwaysRefresh
(
	@UserAccountID int
	,@Result bit output
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 1

	-- Return data
	SELECT 
		@Result = AlwaysRefresh
	FROM dbo.tbUserSetting
	WHERE UserAccountID = @UserAccountID

END
GO
GRANT EXECUTE
ON dbo.upGetAlwaysRefresh
TO system