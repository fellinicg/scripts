USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[upUpdateArrivalDeparture]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[upUpdateArrivalDeparture]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080311
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080311
-- ============================================================
CREATE PROCEDURE dbo.upUpdateArrivalDeparture
(
	@ReportLoadID int
)
	
AS
BEGIN

	-- Declarations
	DECLARE 
		@NumBookingLoc int
		,@ArrivalAirportCode varchar(5)

	-- Initialize settings
	SET NOCOUNT ON

	SELECT @NumBookingLoc = COUNT(DISTINCT BookingLocator)
	FROM dbo.tbItineraryReport
	WHERE ReportLoadID = @ReportLoadID

	SELECT @ArrivalAirportCode = ArrivalAirportCode
	FROM dbo.tbItineraryReport
	GROUP BY ArrivalAirportCode
	HAVING COUNT(ArrivalAirportCode) > @NumBookingLoc

	UPDATE dbo.tbItineraryReport SET ArrivalDepartureInd = 'A'
	WHERE ArrivalAirportCode = @ArrivalAirportCode

	UPDATE dbo.tbItineraryReport SET ArrivalDepartureInd = 'D'
	WHERE DepartureAirportCode = @ArrivalAirportCode
	AND (ArrivalDepartureInd <> 'A' OR ArrivalDepartureInd IS NULL)

END
GO
GRANT EXECUTE
ON dbo.upUpdateArrivalDeparture
TO system