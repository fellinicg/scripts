USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071004
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071004
-- ============================================================
CREATE PROCEDURE dbo.upDeletePerson
(
	@PersonID bigint
	,@Result char(1) output
)
	
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Update record
	UPDATE dbo.tbPerson SET IsDeleted = 1
	WHERE PersonID = @PersonID

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@ERROR = 0)
		SET @Result = 'S'

END