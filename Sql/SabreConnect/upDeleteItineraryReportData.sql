USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[upDeleteItineraryReportData]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[upDeleteItineraryReportData]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080307
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080903
-- ============================================================
CREATE PROCEDURE dbo.upDeleteItineraryReportData
(
	@ReportLoadID int
	,@Result char(1) output
)
	
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Delete records
  DELETE FROM dbo.tbItineraryReportPassenger
	WHERE ReportLoadID = @ReportLoadID

	-- Verify success
	IF (@@ERROR <> 0)
		RETURN

	-- Delete records
  DELETE FROM dbo.tbItineraryReport
	WHERE ReportLoadID = @ReportLoadID

	-- Verify success
	IF (@@ERROR <> 0)
		RETURN

	-- Delete records
  DELETE FROM dbo.tbReportLoad
	WHERE ReportLoadID = @ReportLoadID

	-- Verify success
	IF (@@ERROR = 0)
		SET @Result = 'S'

END
GO
GRANT EXECUTE
ON dbo.upDeleteItineraryReportData
TO adm_airconfirm