USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071026
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080116
-- ============================================================
ALTER PROCEDURE dbo.upGetParticipantInfo (@ParticipantID bigint)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Return data
	SELECT p.ParticipantID, p.FirstName, p.LastName, p.Email, p.TravelRegLogin, 
		p.BookingLocator, p.IsDeleted, p.IsTicketed, p.InvitationURL, p.Status, 
		g.GuestID, g.FirstName GuestFirstName, g.LastName GuestLastName, 
		g.BookingLocator GuestBookingLocator, g.IsTicketed GuestIsTicketed
	FROM dbo.tbParticipant p
	LEFT JOIN dbo.tbGuest g ON p.ParticipantID = g.ParticipantID
	WHERE p.ParticipantID = @ParticipantID

END