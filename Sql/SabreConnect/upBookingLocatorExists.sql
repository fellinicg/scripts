USE AirConfirm
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071205
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071205
-- ============================================================
CREATE PROCEDURE dbo.upBookingLocatorExists
(
	@BookingLocator char(6)
	,@ProjectNumber char(10)
	,@Result char(1) output
)
	
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Check to see if this booking locator exists
	SELECT @Result = Result
	FROM (
		SELECT 'T' 'Result'
		FROM dbo.tbParticipant
		WHERE BookingLocator = @BookingLocator
			AND ProjectNumber = @ProjectNumber
		UNION ALL
		SELECT 'T' 
		FROM dbo.tbGuest
		WHERE BookingLocator = @BookingLocator
			AND ProjectNumber = @ProjectNumber
	) a

END
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upBookingLocatorExists
TO system
