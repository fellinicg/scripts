USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbGuest](
	[GuestID] [bigint] NOT NULL,
	[FirstName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LastName] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ParticipantID] [bigint] NOT NULL,
	[BookingLocator] [char](6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsTicketed] [bit] NOT NULL CONSTRAINT [DF_tbGuest_IsTicketed]  DEFAULT (0),
	[CreateDate] [datetime] NOT NULL CONSTRAINT [DF_tbGuest_CreateDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbGuest] PRIMARY KEY CLUSTERED 
(
	[GuestID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF