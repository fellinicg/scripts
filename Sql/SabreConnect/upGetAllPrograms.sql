USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071011
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071011
-- ============================================================
CREATE PROCEDURE dbo.upGetAllPrograms
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Return data
	SELECT ProgramID, DKNumber, Description, IsDeleted, CreateDate
	FROM dbo.tbProgram
	ORDER BY Description

END