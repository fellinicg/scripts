USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071218
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071218
-- ============================================================
ALTER PROCEDURE dbo.upGetChangeRequest
(
	@ParticipantID bigint
)
	
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Return data
	SELECT TOP 1 ParticipantRequest, CONVERT(varchar, CreateDate, 100) CreatedOn
	FROM dbo.tbChangeRequest
	WHERE ParticipantID = @ParticipantID
	ORDER BY CreateDate DESC

END
GO
GRANT EXECUTE
ON dbo.upGetChangeRequest
TO system