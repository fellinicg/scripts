--UPDATE dbo.tbAirline SET Description = RTRIM(LEFT(OrgDesc, charindex(' (', OrgDesc)))
--SELECT OrgDesc, RTRIM(LEFT(OrgDesc, charindex(' (', OrgDesc))), charindex(' (', OrgDesc)
--FROM 
--WHERE IsDeleted = 0



--WHERE Code IN (SELECT Code
--								FROM dbo.tbAirline
--								WHERE IsDeleted = 0
--								GROUP BY Code
--								HAVING COUNT(Code) > 1)


--	INSERT INTO tbAirport
--  SELECT DISTINCT *
--  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0', 
--     'Excel 8.0;Database=D:\Clients\Madison Performance Group\Sabre\Data\codes.xls', 'SELECT * FROM [airport$]')


--	INSERT INTO tbMeal
--  SELECT DISTINCT ISNULL(Code, ''), Description
--  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0', 
--     'Excel 8.0;Database=D:\Clients\Madison Performance Group\Sabre\Data\codes.xls', 'SELECT * FROM [meals$]')

--	INSERT INTO tbAirline (Code, OrgDesc)
--  SELECT DISTINCT ISNULL(Code, '') Code, Description
--  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0', 
--     'Excel 8.0;Database=D:\Clients\Madison Performance Group\Sabre\Data\codes.xls', 'SELECT * FROM [Airlines$]')
--	ORDER BY Code
--
--	INSERT INTO tbEquipment
--  SELECT DISTINCT ISNULL(Code, '') Code, Description
--  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0', 
--     'Excel 8.0;Database=D:\Clients\Madison Performance Group\Sabre\Data\codes.xls', 'SELECT * FROM [Equipment$]')
--	ORDER BY Code
--
--select * from dbo.tbEquipment
--where code in
--(select code from dbo.tbEquipment group by code having count(code) > 1)

--update dbo.tbEquipment set Description = RTRIM(SUBSTRING(Description, 1, LEN(Description) -3))
--where description like '%pax'

--update dbo.tbEquipment set Description = RTRIM(REPLACE(Description, ' all pax models', ''))
--where description like '%all pax models%'

SELECT '<?xml version="1.0"?><data>'
UNION ALL
SELECT '<row><code>' + Code + '</code><description>' + Description + '</description></row>'
FROM dbo.tbEquipment
UNION ALL
SELECT '</data>'

SELECT '<?xml version="1.0"?><data>'
UNION ALL
SELECT '<row><code>' + Code + '</code><airport>' + ISNULL(Airport, '') + 
'</airport><city>' + ISNULL(City, '') +
'</city><province>' + ISNULL(Province, '') +
'</province><country>' + ISNULL(Country, '') +
'</country?</row>'
FROM dbo.tbAirport
UNION ALL
SELECT '</data>'

insert into dbo.tbEquipment
select b.code, b.description, b.description
from dbo.tbEquipment a
right join (
  SELECT REPLACE(IATA, '''', '') Code, Description
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0', 
     'Excel 8.0;Database=D:\arctypes.xls', 'SELECT * FROM [arctypes$]')
) b ON a.Code = b.Code
where a.code is null