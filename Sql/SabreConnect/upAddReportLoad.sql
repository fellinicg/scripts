USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[upAddReportLoad]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[upAddReportLoad]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080307
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080312
-- ============================================================
CREATE PROCEDURE dbo.upAddReportLoad
(
	@ProgramID int
	,@Type char(1) 
	,@UserID int
	,@SendToEmail varchar(100)
	,@ReportLoadID int output 
	,@Result char(1) output
)
	
AS
BEGIN
	-- Declarations
	DECLARE 
		@ProjectNumber char(10)
		,@Status char(1)

	-- Initialize settings
	SET NOCOUNT ON
	SET @Status = 'U'
	SET @ReportLoadID = 0
	SET @Result = 'F'

	-- Get ProjectNumber
	SELECT @ProjectNumber = '026000' + CONVERT(varchar, DKNumber)
	FROM dbo.tbProgram
	WHERE ProgramID = @ProgramID

	-- Verify success
	IF (@@ROWCOUNT = 0 OR @@ERROR <> 0)
		RETURN

	-- Add record
  INSERT INTO dbo.tbReportLoad (ProgramID, ProjectNumber, Type, Status, UserID, SendToEmail)
  VALUES (@ProgramID, @ProjectNumber, UPPER(@Type), UPPER(@Status), @UserID, @SendToEmail)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@ERROR = 0)
		BEGIN
			SET @Result = 'S'
			SET @ReportLoadID = @@IDENTITY
		END

END
GO
GRANT EXECUTE
ON dbo.upAddReportLoad
TO system