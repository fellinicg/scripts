USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071214
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080204
-- ============================================================
ALTER PROCEDURE dbo.upGetBookingLocators
(
	@ParticipantID bigint
	,@BookingLocator char(10) output
	,@GuestBookingLocator char(10) output

)
	
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Return data
	SELECT 
		@BookingLocator = ISNULL(pax.BookingLocator, '') 
		,@GuestBookingLocator = CASE WHEN ISNULL(pax.BookingLocator, '') <> ISNULL(gst.BookingLocator, '') 
			THEN ISNULL(gst.BookingLocator, '') 
			ELSE '' END 
	FROM dbo.tbParticipant pax
		LEFT JOIN dbo.tbGuest gst ON pax.ParticipantID = gst.ParticipantID
	WHERE pax.ParticipantID = @ParticipantID

END
GO
GRANT EXECUTE
ON dbo.upGetBookingLocators
TO system