USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbItineraryReport]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbItineraryReport]
GO
CREATE TABLE [dbo].[tbItineraryReport](
	[ItineraryReportID] [int] IDENTITY(1,1) NOT NULL,
	[ReportLoadID] [int] NOT NULL,
	[BookingLocator] [char](6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SegmentID] [tinyint] NOT NULL,
	[DepartureDate] [datetime] NULL,
	[DepartureTime] [varchar](11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DepartureAirportCode] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DepartureAirport] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ArrivalDate] [datetime] NULL,
	[ArrivalTime] [varchar](11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ArrivalAirportCode] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ArrivalAirport] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CarrierCode] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Carrier] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FlightNumber] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StatusCode] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ConnectionInd] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ArrivalDepartureInd] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_tbItineraryReport] PRIMARY KEY CLUSTERED 
(
	[ItineraryReportID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF