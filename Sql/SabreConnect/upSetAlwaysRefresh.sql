USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'dbo.upSetAlwaysRefresh') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE dbo.upSetAlwaysRefresh
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080916
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080916
-- ============================================================
CREATE PROCEDURE dbo.upSetAlwaysRefresh
(
	@UserAccountID int
	,@AlwaysRefresh bit
	,@Result char(1) output
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Store data
	IF EXISTS (SELECT AlwaysRefresh FROM dbo.tbUserSetting WHERE UserAccountID = @UserAccountID)
		UPDATE dbo.tbUserSetting SET AlwaysRefresh = @AlwaysRefresh WHERE UserAccountID = @UserAccountID
	ELSE
		INSERT INTO dbo.tbUserSetting (UserAccountID, AlwaysRefresh) VALUES (@UserAccountID, @AlwaysRefresh)

	-- Verify success
	IF @@ROWCOUNT > 0 AND @@ERROR = 0
		SET @Result = 'S'

END
GO
GRANT EXECUTE
ON dbo.upSetAlwaysRefresh
TO system