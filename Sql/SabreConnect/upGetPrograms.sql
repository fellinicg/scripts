USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071011
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071011
-- ============================================================
ALTER PROCEDURE dbo.upGetPrograms (@Status char(1))
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Evaluate parameter
	SET @Status = UPPER(@Status)

	SET @Status = CASE @Status 
									WHEN 'O' THEN '0'
									WHEN 'D' THEN '1'
									ELSE 'A'
								END

	-- Return data
	IF (@Status = 'A')
		EXEC dbo.upGetAllPrograms
	ELSE
		SELECT ProgramID, DKNumber, Description, IsDeleted, CreateDate
		FROM dbo.tbProgram
		WHERE IsDeleted = @Status
		ORDER BY Description

END