USE [OnvantageReports]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[upTruncateTables]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[upTruncateTables]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080903
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080903
-- ============================================================
CREATE PROCEDURE dbo.upTruncateTables
(
	@Result char(1) output
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Truncate tables
	TRUNCATE TABLE dbo.tbActivityRoster
	TRUNCATE TABLE dbo.tbEventSchedule
	TRUNCATE TABLE dbo.tbGolf
	TRUNCATE TABLE dbo.tbPaxList
	TRUNCATE TABLE dbo.tbRoomList

	-- Verify success
	IF (@@ERROR = 0)
		SET @Result = 'S'

END
GO
GRANT EXECUTE
ON dbo.upTruncateTables
TO adm_airconfirm