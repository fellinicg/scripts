USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071026
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071026
-- ============================================================
ALTER FUNCTION dbo.sfIsLocked (@ParticipantID bigint, @UserID int)
	RETURNS bit AS
BEGIN
	DECLARE @Ret bit

	SET @Ret = 0

	SELECT @Ret = CASE ISNULL(LockedBy, @UserID) WHEN @UserID THEN 0 ELSE IsLocked END
	FROM dbo.tbParticipant 
	WHERE ParticipantID = @ParticipantID

	RETURN(@Ret)
END
