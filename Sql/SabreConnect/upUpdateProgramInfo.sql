USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071018
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071018
-- ============================================================
ALTER PROCEDURE dbo.upUpdateProgramInfo 
(
	@ProgramID int
	,@DKNumber int = null
	,@Description varchar(200)
	,@ReplyToEmail varchar(200)
	,@ConfirmationEmailSubject varchar(50)
	,@ConfirmationEmailText text
	,@ConfirmedEmailSubject varchar(50)
	,@ConfirmedEmailText text
	,@IsDeleted bit
	,@Result char(1) output
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Update record
	UPDATE dbo.tbProgram SET 
		DKNumber = @DKNumber
		,Description = @Description
		,ReplyToEmail = @ReplyToEmail
		,ConfirmationEmailSubject = @ConfirmationEmailSubject
		,ConfirmationEmailText = @ConfirmationEmailText
		,ConfirmedEmailSubject = @ConfirmedEmailSubject
		,ConfirmedEmailText = @ConfirmedEmailText
		,IsDeleted = @IsDeleted
	WHERE ProgramID = @ProgramID

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@ERROR = 0)
		SET @Result = 'S'

END