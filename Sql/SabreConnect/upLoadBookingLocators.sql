USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
USE [AirConfirm]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'dbo.upLoadBookingLocators') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE dbo.upLoadBookingLocators
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080911
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080911
-- ============================================================
CREATE PROCEDURE dbo.upLoadBookingLocators
(	
	@InputFile varchar(50)
	, @Development bit = 0
	, @RecordsUpdated int output
	, @Result char(1) output
)
AS
BEGIN

	-- Declarations
	DECLARE @bcpcmd varchar(5000)
	DECLARE @timestamp varchar(20)

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'
	SET @RecordsUpdated = 0
	SET @timestamp = CONVERT(VARCHAR, CURRENT_TIMESTAMP, 112) + '_' + 
		CONVERT(VARCHAR, DATEPART(hh, CURRENT_TIMESTAMP)) + 'h' + 
		CONVERT(VARCHAR, DATEPART(mi, CURRENT_TIMESTAMP)) + 'm' + 
		CONVERT(VARCHAR, DATEPART(ss, CURRENT_TIMESTAMP)) + 's'

	-- Map drive
	IF (@Development = 1)
		SET @bcpcmd = 'net use z: /delete&net use z: \\' + @@SERVERNAME + '\AirConfirmUpload$'
	ELSE
		SET @bcpcmd = 'net use z: /delete&net use z: \\web2003prod5a\AirConfirmUpload$'
	EXEC master.dbo.xp_cmdshell @bcpcmd

	-- Copy file to SQL server
	SET @bcpcmd = 'copy "z:\' + @InputFile + '" "d:\' + @InputFile + '"'
	EXEC master.dbo.xp_cmdshell @bcpcmd

	-- Update participants booking locators
	EXEC ('UPDATE dbo.tbParticipant SET BookingLocator = UPPER(xls.BookingLocator)
				FROM OPENROWSET(''Microsoft.Jet.OLEDB.4.0'', 
				''Excel 8.0;Database=D:\' + @InputFile + ''', ''SELECT [Participant ID] AS ParticipantID, 
				[Booking Locator] AS BookingLocator, [Program ID] AS ProgramID
				FROM [BOOKINGLOC$] WHERE [Participant ID] IS NOT NULL'') AS xls
				INNER JOIN dbo.tbParticipant p ON xls.ParticipantID = p.ParticipantID
					AND xls.ProgramID = p.ProgramID')

	-- Store number of records added
	SET @RecordsUpdated = @@ROWCOUNT

	-- Check for error
	IF (@@ERROR <> 0)	
		GOTO CleanUp

	-- Update guests booking locators
	EXEC ('UPDATE dbo.tbGuest SET BookingLocator = UPPER(xls.BookingLocator)
				FROM OPENROWSET(''Microsoft.Jet.OLEDB.4.0'', 
				''Excel 8.0;Database=D:\' + @InputFile + ''', ''SELECT [Participant ID] AS ParticipantID, 
				[Booking Locator] AS BookingLocator, [Program ID] AS ProgramID
				FROM [BOOKINGLOC$] WHERE [Participant ID] IS NOT NULL'') AS xls
				INNER JOIN dbo.tbGuest g ON xls.ParticipantID = g.GuestID
				INNER JOIN dbo.tbParticipant p ON g.ParticipantID = p.ParticipantID
					AND xls.ProgramID = p.ProgramID')

	-- Store number of records added
	SET @RecordsUpdated = @@ROWCOUNT + @RecordsUpdated

	-- Check for error
	IF (@@ERROR <> 0)	
		GOTO CleanUp

	-- Archive files
	SET @bcpcmd = 'move "z:\' + @InputFile + '" "z:\Archive\' + @timestamp + '_' + @InputFile + '"'
	EXECUTE master.dbo.xp_cmdshell @bcpcmd
	
	-- Success
	SET @Result = 'S'

CleanUp:
	-- Remove mapped drive
	EXECUTE master.dbo.xp_cmdshell 'net use z: /delete'
	
	-- Delete file from SQL server
	SET @bcpcmd = 'del "d:\' + @InputFile + '"'
	EXECUTE master.dbo.xp_cmdshell @bcpcmd

END
GO
GRANT EXECUTE
ON dbo.upDeleteItineraryReportForProgram
TO adm_airconfirm