USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbProgram](
	[ProgramID] [int] NOT NULL,
	[DKNumber] [int] NULL,
	[Description] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ReplyToEmail] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PreConfirmEmailSubject] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PreConfirmEmailText] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PostConfirmEmailSubject] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PostConfirmEmailText] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbProgram_IsDeleted]  DEFAULT (0),
	[CreateDate] [datetime] NOT NULL CONSTRAINT [DF_tbProgram_CreateDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbProgram] PRIMARY KEY CLUSTERED 
(
	[ProgramID] ASC
) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF