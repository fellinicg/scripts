USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[upGetArrivalItineraryReport]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[upGetArrivalItineraryReport]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080312
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080312
-- ============================================================
CREATE PROCEDURE dbo.upGetArrivalItineraryReport
(
	@ReportLoadID varchar(30)
	,@FromDate varchar(10)
	,@ToDate varchar(10)
	,@AirportCodes varchar(500)
)
	
AS
BEGIN
	-- Initialize settings
	SET NOCOUNT ON

	-- Return data
	SELECT DISTINCT ir.ArrivalDate, ir.ArrivalTime, irp.FirstName, irp.LastName, ir.Carrier, 
		ir.FlightNumber, 
		CASE LEN(ir.DepartureAirport) WHEN 0 THEN ir.DepartureAirportCode ELSE ir.DepartureAirport END DepartureAirport, 
		CASE LEN(ir.ArrivalAirport) WHEN 0 THEN ir.ArrivalAirportCode ELSE ir.ArrivalAirport END ArrivalAirport, 
		ir.DepartureTime
	FROM dbo.tbItineraryReport ir
		INNER JOIN dbo.tbItineraryReportPassenger irp ON ir.ReportLoadID = irp.ReportLoadID AND ir.BookingLocator = irp.BookingLocator
		INNER JOIN dbo.tfSplit(@AirportCodes, ',') ac ON ir.ArrivalAirportCode = ac.Items
	WHERE ir.ReportLoadID = @ReportLoadID
		AND ir.ArrivalDate BETWEEN @FromDate AND @ToDate
	ORDER BY ir.ArrivalDate, ir.ArrivalTime, irp.LastName, irp.FirstName

END
GO
GRANT EXECUTE
ON dbo.upGetArrivalItineraryReport
TO system