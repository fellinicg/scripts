USE [AirConfirm]
DELETE FROM dbo.tbGuest
DELETE FROM dbo.tbNote
DELETE FROM dbo.tbParticipantItineraryStatus
DELETE FROM dbo.tbParticipant
DELETE FROM dbo.tbProgram

-- tbProgram
INSERT INTO dbo.tbProgram
SELECT *
FROM AirConfirmData.dbo.tbProgram

-- tbParticipant
ALTER TABLE dbo.tbParticipant DISABLE TRIGGER trgInitializeItineraryStatus

INSERT INTO dbo.tbParticipant
SELECT *
FROM AirConfirmData.dbo.tbParticipant

ALTER TABLE dbo.tbParticipant ENABLE TRIGGER trgInitializeItineraryStatus

-- tbParticipantItineraryStatus
ALTER TABLE dbo.tbParticipantItineraryStatus DISABLE TRIGGER trgUpdateItineraryStatus

SET IDENTITY_INSERT dbo.tbParticipantItineraryStatus ON

INSERT INTO dbo.tbParticipantItineraryStatus (ParticipantItineraryStatusID, ParticipantID, ItineraryStatusCode, AgentID, CreateDate)
SELECT ParticipantItineraryStatusID, ParticipantID, ItineraryStatusCode, AgentID, CreateDate
FROM AirConfirmData.dbo.tbParticipantItineraryStatus

SET IDENTITY_INSERT dbo.tbParticipantItineraryStatus OFF

ALTER TABLE dbo.tbParticipantItineraryStatus ENABLE TRIGGER trgUpdateItineraryStatus

-- tbGuest
INSERT INTO dbo.tbGuest
SELECT *
FROM AirConfirmData.dbo.tbGuest

-- tbParticipantItineraryStatus
SET IDENTITY_INSERT dbo.tbNote ON

INSERT INTO dbo.tbNote (NoteID, ParticipantID, Note, AuthorID, CreateDate)
SELECT NoteID, ParticipantID, Note, AuthorID, CreateDate
FROM AirConfirmData.dbo.tbNote

SET IDENTITY_INSERT dbo.tbNote OFF
