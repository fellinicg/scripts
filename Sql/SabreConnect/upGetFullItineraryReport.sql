USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[upGetFullItineraryReport]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[upGetFullItineraryReport]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20080306
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080312
-- ============================================================
CREATE PROCEDURE dbo.upGetFullItineraryReport
(
	@ReportLoadID int
)
	
AS
BEGIN
	-- Initialize settings
	SET NOCOUNT ON

	-- Return data
	SELECT ir.DepartureDate, ir.DepartureTime, ir.ArrivalDate, ir.ArrivalTime, irp.FirstName, irp.LastName, 
		ir.Carrier, ir.FlightNumber, ir.DepartureAirport, ir.ArrivalAirport, ir.Status, ir.ConnectionInd,
		ir.CarrierCode, ir.DepartureAirportCode, ir.ArrivalAirportCode
	FROM dbo.tbItineraryReport ir
	INNER JOIN dbo.tbItineraryReportPassenger irp ON ir.ReportLoadID = irp.ReportLoadID 
		AND ir.BookingLocator = irp.BookingLocator
	WHERE ir.ReportLoadID = @ReportLoadID
	ORDER BY ir.BookingLocator, irp.LastName, irp.FirstName, ir.SegmentID

END
GO
GRANT EXECUTE
ON dbo.upGetFullItineraryReport
TO system