USE [AirConfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071017
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080116
-- ============================================================
ALTER PROCEDURE dbo.upLoadProgramParticipants 
(	
	@InputFile varchar(50)
	, @Development bit = 0
	, @ProgramAdded bit output
	, @RecordsAdded int output
	, @Result char(1) output
)
AS
BEGIN

	-- Declarations
	DECLARE @bcpcmd varchar(5000)
	DECLARE @timestamp varchar(20)
	DECLARE @tab varchar(50)
	DECLARE @ParticipantStatus char(1)

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'
	SET @RecordsAdded = 0
	SET @ProgramAdded = 0
	SET @timestamp = CONVERT(VARCHAR, CURRENT_TIMESTAMP, 112) + '_' + 
		CONVERT(VARCHAR, DATEPART(hh, CURRENT_TIMESTAMP)) + 'h' + 
		CONVERT(VARCHAR, DATEPART(mi, CURRENT_TIMESTAMP)) + 'm' + 
		CONVERT(VARCHAR, DATEPART(ss, CURRENT_TIMESTAMP)) + 's'
	SET @tab = RTRIM(LEFT(@InputFile, CHARINDEX('.XLS', @InputFile) - 1))
	SET @ParticipantStatus = 'P'

	-- Map drive
	IF (@Development = 1)
		SET @bcpcmd = 'net use z: /delete&net use z: \\' + @@SERVERNAME + '\AirConfirmUpload$'
	ELSE
		SET @bcpcmd = 'net use z: /delete&net use z: \\web2003prod5a\AirConfirmUpload$'
	EXEC master.dbo.xp_cmdshell @bcpcmd

	-- Copy file to SQL server
	SET @bcpcmd = 'copy z:\' + @InputFile + ' d:\' + @InputFile
	EXEC master.dbo.xp_cmdshell @bcpcmd

	-- Add ProgramID if it doesn't exist
	EXEC ('INSERT INTO dbo.tbProgram (ProgramID, DKNumber)
				SELECT a.ProgramID, RIGHT(a.Project#, 4)
				FROM OPENROWSET(''Microsoft.Jet.OLEDB.4.0'', 
					 ''Excel 8.0;Database=D:\' + @InputFile + ''', ''SELECT DISTINCT [Event ID] AS ProgramID, [Project#] FROM [' + @tab + '$] WHERE [Event ID] IS NOT NULL'') 
					 AS a
				LEFT JOIN dbo.tbProgram p ON a.ProgramID = p.ProgramID
				WHERE p.ProgramID IS NULL')

	-- Store number of records added
	SET @ProgramAdded = @@ROWCOUNT

	-- Check for error
	IF (@@ERROR <> 0)	
		GOTO CleanUp

	-- Insert records into table
	EXEC ('INSERT INTO dbo.tbParticipant (ParticipantID, FirstName, LastName, Email, 
				ProgramID, ProjectNumber, TravelRegLogin, InvitationURL, Status)
				SELECT [Participant Id], [First Name], [Last Name], [Email Address], [Event ID], [Project#],
				CASE LEN(ISNULL([Business Name], '''')) WHEN 0 THEN [Email Address] ELSE [Business Name] END, 
				REPLACE([Invitation ID#], ''/event/'', ''/register/''), ''' + @ParticipantStatus + '''
				FROM OPENROWSET(''Microsoft.Jet.OLEDB.4.0'', 
					 ''Excel 8.0;Database=D:\' + @InputFile + ''', ''SELECT * FROM [' + @tab + '$] WHERE [Participant Id] IS NOT NULL'') 
					 AS a
				LEFT JOIN dbo.tbParticipant p ON a.[Participant Id] = p.ParticipantID
				WHERE p.ParticipantID IS NULL')

	-- Store number of records added
	SET @RecordsAdded = @@ROWCOUNT

	-- Check for error
	IF (@@ERROR <> 0)	
		GOTO CleanUp

	-- Archive files
	SET @bcpcmd = 'move z:\' + @InputFile + ' z:\Archive\' + @timestamp + '_' + @InputFile
	EXECUTE master.dbo.xp_cmdshell @bcpcmd
	
	-- Success
	SET @Result = 'S'

CleanUp:
	-- Remove mapped drive
	EXECUTE master.dbo.xp_cmdshell 'net use z: /delete'
	
	-- Delete file from SQL server
	SET @bcpcmd = 'del d:\' + @InputFile
	EXECUTE master.dbo.xp_cmdshell @bcpcmd

END