USE AirConfirm
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071205
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071205
-- ============================================================
ALTER PROCEDURE dbo.upUpdateBookingInfo
(
	@PersonID bigint
	,@IsTicketed bit = 0	
	,@ProjectNumber char(10)
	,@BookingLocator char(6)
	,@Result char(1) output
)
	
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Check BookingLocator
	IF (LEN(LTRIM(RTRIM(@BookingLocator))) = 0)
		BEGIN
			SET @BookingLocator = null
			SET @IsTicketed = 0
		END
		
	-- Try an update on tbParticipant
	UPDATE dbo.tbParticipant SET 
		BookingLocator = @BookingLocator
		,IsTicketed = @IsTicketed
	WHERE ParticipantID = @PersonID
		AND ProjectNumber = @ProjectNumber

	-- Verify success
	IF (@@ROWCOUNT > 0 AND @@ERROR = 0)
		BEGIN
			SET @Result = 'S'
			RETURN
		END

	-- Try an update on tbGuest
	UPDATE dbo.tbGuest SET 
		BookingLocator = @BookingLocator
		,IsTicketed = @IsTicketed
	WHERE GuestID = @PersonID
		AND ProjectNumber = @ProjectNumber
	
	-- Verify success
	IF (@@ROWCOUNT > 0 AND @@ERROR = 0)
		SET @Result = 'S'

END
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upUpdateBookingInfo
TO system