USE [OnvantageReports]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'dbo.upGetReportList') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE dbo.upGetReportList
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060915
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20080904
-- ============================================================
CREATE PROCEDURE dbo.upGetReportList
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Return data
	SELECT 
		ReportID, 
		ReportName,
		ReportTable,
		LoadProc,
		ExecProc
	FROM dbo.tbReport
	WHERE IsDeleted = 0
	ORDER BY ReportName

END
GO
GRANT EXECUTE
ON dbo.upGetReportList
TO system