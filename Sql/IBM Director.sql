select distinct a.ServerID, a.ServerName
from tbServer a
left join dbo.tbServer_MngObj b on a.ServerID = b.ServerID
where b.ServerID is null
join
(select distinct a.ServerID, a.ServerName
from tbServer a
left join dbo.tbServer_WinNetID b on a.ServerID = b.ServerID
where b.ServerID is null) b
on a.ServerID <>


-- Appear in tbServer, dbo.TWG_MANAGED_OBJECT, dbo.TWG_WINDOWS_NETWORK_ID with DUPES
select *
from dbo.tbServer_MngObj a
join dbo.tbServer_WinNetID b on a.ServerID = b.ServerID and a.ObjID = b.ObjID
left join (select a.ServerID, case count(a.ServerID) when 1 then '' else 'DUP' end 'Dup'
					from dbo.tbServer_MngObj a
					join dbo.tbServer_WinNetID b on a.ServerID = b.ServerID and a.ObjID = b.ObjID
					group by a.ServerID) c on a.ServerID = c.ServerID

-- Appear in tbServer_MngObj, but not 
--select a.*, c.*
--from dbo.tbServer_MngObj a
--left join dbo.tbServer_WinNetID b on a.ServerID = b.ServerID 
--left join (select a.ServerID, case count(a.ServerID) when 1 then '' else 'DUP' end 'Dup'
--					from dbo.tbServer_MngObj a
--					left join dbo.tbServer_WinNetID b on a.ServerID = b.ServerID 
--					where b.ServerID is null
--					group by a.ServerID) c on a.ServerID = c.ServerID
--where b.ServerID is null

--Same OBJID different names
select *
from dbo.tbServer_MngObj a
join dbo.tbServer_WinNetID b on a.ObjID = b.ObjID and a.ServerID <> b.ServerID

-- Appear in tbServer but no logical match in IBM Director
select distinct a.serverid, a.servername
from tbServer a
full join dbo.tbServer_MngObj b on a.ServerID = b.ServerID
full join dbo.tbServer_WinNetID c on a.ServerID = c.ServerID
where b.serverid is null and c.serverid is null

select a.managed_obj_id, a.label, b.computer_name, b.managed_obj_id, a.State
from teleas22.IBMDirector.dbo.TWG_MANAGED_OBJECT a
full join teleas22.IBMDirector.dbo.TWG_WINDOWS_NETWORK_ID b
	on a.managed_obj_id = b.managed_obj_id
order by b.computer_name

--select *
--from teleas22.IBMDirector.dbo.TWG_MANAGED_OBJECT
--where managed_obj_id = 1779

select ServerName, LABEL, IP_INDEX, IP_ADDRESS, State, s.MANAGED_OBJ_ID
from dbo.tbServer
inner join teleas22.IBMDirector.dbo.TWG_MANAGED_OBJECT s on ServerName = left(label, charindex(' ', label) - 1)
left join teleas22.IBMDirector.dbo.TWG_IP_ADDRESS i on s.MANAGED_OBJ_ID = i.MANAGED_OBJ_ID and IP_INDEX = 1
order by servername

--inner join teleas22.IBMDirector.dbo.TWG_MANAGED_OBJECT s on ServerName = LABEL


--select distinct ServerName, computer_name, MANAGED_OBJ_ID
--from dbo.tbServer
--left join teleas22.IBMDirector.dbo.TWG_WINDOWS_NETWORK_ID on ServerName = computer_name


--select label, left(label, charindex(' ', label) - 1)
--from teleas22.IBMDirector.dbo.TWG_MANAGED_OBJECT s
--order by label

--select *
--from teleas22.IBMDirector.dbo.TWG_MANAGED_OBJECT a
--right join (
--								select managed_obj_id
--								from teleas22.IBMDirector.dbo.TWG_WINDOWS_NETWORK_ID
--								where computer_name in (select computer_name
--								from teleas22.IBMDirector.dbo.TWG_WINDOWS_NETWORK_ID
--								group by computer_name
--								having count(computer_name) > 1)) b on a.MANAGED_OBJ_ID = b.MANAGED_OBJ_ID 
--order by label
--
truncate table dbo.tbServer_MngObj

insert into dbo.tbServer_MngObj
select ServerID, ServerName, s.MANAGED_OBJ_ID, LABEL, State
from dbo.tbServer
inner join teleas22.IBMDirector.dbo.TWG_MANAGED_OBJECT s on ServerName = left(label, charindex(' ', label) - 1)
order by servername

truncate table dbo.tbServer_WinNetID

insert into dbo.tbServer_WinNetID
select ServerID, ServerName, s.MANAGED_OBJ_ID, s.COMPUTER_NAME
from dbo.tbServer
inner join teleas22.IBMDirector.dbo.TWG_WINDOWS_NETWORK_ID s on ServerName = s.COMPUTER_NAME
order by servername

select [Major OS], [Deployment Status], [Server Name], ServerName, x.label, HW, warranty,  x.*
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=c:\temp\dec servers.xls', 'select * from [Sheet1$]')
left join dbo.tbServer on [Server Name] = ServerName
left join (select obj.MANAGED_OBJ_ID, Processors, OSVer, convert(bigint, physical_memory_kb)*1024 'MemoryBytes', 
	Product, SERIAL_NUMBER, IP_ADDRESS, 
	case charindex('(', obj.label) 
		when 0 then rtrim(ltrim(obj.label)) 
		else rtrim(ltrim(left(label, charindex('(', obj.label)-1))) end 'ServerNm', label
from teleas22.IBMDirector.dbo.TWG_MANAGED_OBJECT obj
left join (select MANAGED_OBJ_ID, count(MANAGED_OBJ_ID) 'Processors' 
						from teleas22.IBMDirector.dbo.TWG_PROCESSOR
						group by MANAGED_OBJ_ID) a on obj.MANAGED_OBJ_ID = a.MANAGED_OBJ_ID
left join (select MANAGED_OBJ_ID, dbo.sfTrim(i.OP_SYS_VERSION) +	case isnull(i.OP_SYS_REVISION, '') when '' then ''
							else case charindex(',', i.OP_SYS_REVISION) when 0 then '.' + i.OP_SYS_REVISION 
								else '.' + left(i.OP_SYS_REVISION, charindex(',', i.OP_SYS_REVISION)-1) end end 'OSVer'
						from teleas22.IBMDirector.dbo.TWG_OPERATING_SYSTEM i) b on obj.MANAGED_OBJ_ID = b.MANAGED_OBJ_ID
left join teleas22.IBMDirector.dbo.TWG_INSTALLED_MEMORY c on obj.MANAGED_OBJ_ID = c.MANAGED_OBJ_ID
left join teleas22.IBMDirector.dbo.TWG_COMPONENT_ID d on obj.MANAGED_OBJ_ID = d.MANAGED_OBJ_ID
left join teleas22.IBMDirector.dbo.TWG_IP_ADDRESS e on obj.MANAGED_OBJ_ID = e.MANAGED_OBJ_ID and e.IP_INDEX = 1
) x on [Server Name] = ServerNm
where [Server Name] is not null and ServerName is null

	select a.ServerID, a.IBMDirectorID, a.UpdFromIBMDir, a.IBMDirDateCreated
	from dbo.tbServerIBMDir a
	inner join (select IBMDirectorID, max(IBMDirDateCreated) 'IBMDirDateCreated'
							from dbo.tbServerIBMDir
							group by IBMDirectorID) b on a.IBMDirectorID = b.IBMDirectorID
	order by a.ServerID