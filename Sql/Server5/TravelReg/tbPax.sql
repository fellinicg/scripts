USE TravelReg
GO
CREATE TABLE [tbPax] (
	[TransactionNum] [varchar] (100) NULL ,
	[PaxID] [varchar] (100) NULL ,
	[FirstName] [varchar] (100) NULL ,
	[LastName] [varchar] (100) NULL ,
	[GuestList] [varchar] (100) NULL ,
	[GuestOf] [varchar] (100) NULL ,
	[PrimaryReg] [varchar] (100) NULL ,
	[RSVPStatus] [varchar] (100) NULL ,
	[BusEmail] [varchar] (100) NULL 
) ON [PRIMARY]
GO
