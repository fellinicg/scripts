SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE  PROCEDURE dbo.upLoadOnvantageInvites AS

	DECLARE @RC INT
	DECLARE @bcpcmd VARCHAR(5000)
	DECLARE @timestamp VARCHAR(20)
	DECLARE @mtgid INT
	
	-- Set timestamp
	SET @timestamp = CONVERT(VARCHAR, DATEPART(hh, CURRENT_TIMESTAMP)) + 'h' + CONVERT(VARCHAR, DATEPART(mi, CURRENT_TIMESTAMP)) + 'm' + CONVERT(VARCHAR, DATEPART(ss, CURRENT_TIMESTAMP)) + 's'
	
	-- Map drive
	SET @bcpcmd = 'net use z: /delete&net use z: \\mpg-fs\onvantage'
	EXECUTE master.dbo.xp_cmdshell @bcpcmd
	
	-- Get meeting id
	SET @bcpcmd = 'master.dbo.xp_cmdshell "type z:\meeting.txt"'
	CREATE TABLE #TMP (info varchar(200)) 
	INSERT #TMP EXEC (@bcpcmd)
	SET @mtgid = (SELECT TOP 1 info FROM #TMP)
	DROP TABLE #TMP
	
	-- Clear table
	TRUNCATE TABLE dbo.tbUrl
	
	-- Import file
	SET @bcpcmd = 'bcp "Travelreg.dbo.tbUrl" in "z:\invites.csv" -SSERVER5 -T -w -F2'
	EXECUTE master.dbo.xp_cmdshell @bcpcmd
	
	-- Archive files
	SET @bcpcmd = 'move z:\invites.csv z:\Archive\' + @timestamp + '_' + 'invites.csv'
	SET @bcpcmd = @bcpcmd + '&move z:\invites.csv z:\Archive\' + @timestamp + '_' + 'meeting.txt'
	EXECUTE master.dbo.xp_cmdshell @bcpcmd
	
	-- Clean data
	UPDATE dbo.tbUrl SET 
	FirstName = REPLACE(FirstName, '"', ''),
	LastName = REPLACE(LastName, '"', ''),
	BusEmail = REPLACE(BusEmail, '"', ''),
	Url = REPLACE(REPLACE(Url, '"', ''), '/event/', '/register/')
	
	-- Remove mapped drive
	EXECUTE master.dbo.xp_cmdshell 'net use z: /delete'
	
	-- Insert records into Users table
	INSERT INTO dbo.Users (u_FullName, u_LoginName, u_InvURL, u_DivisionID, u_ConfStatus)
	SELECT 
	FirstName + ' ' + LastName,
	BusEmail,
	Url,
 	FROM dbo.tbUrl
	INNER JOIN dbo.Divisions ON d_MtgId = @mtgid
	LEFT JOIN dbo.Users ON BusEmail = u_LoginName AND u_DivisionID = d_ID
	WHERE LEN(RTRIM(LTRIM(BusEmail))) > 0
	AND u_ID IS NULL



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

