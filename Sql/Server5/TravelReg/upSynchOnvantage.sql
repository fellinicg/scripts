SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

ALTER PROCEDURE dbo.upSynchOnvantage AS

	DECLARE @RC INT
	DECLARE @bcpcmd VARCHAR(5000)
	DECLARE @timestamp VARCHAR(20)
	DECLARE @mtgid INT
	DECLARE @divid INT
	DECLARE @wavenum INT
	DECLARE @groupnum INT
	DECLARE @compsitenum INT
	DECLARE @PaxID INT
	DECLARE @PersonNum INT
	DECLARE @RegNum INT
	DECLARE @PrimaryNum INT
	
	-- Set timestamp
	SET @timestamp = CONVERT(VARCHAR, DATEPART(hh, CURRENT_TIMESTAMP)) + 'h' + CONVERT(VARCHAR, DATEPART(mi, CURRENT_TIMESTAMP)) + 'm' + CONVERT(VARCHAR, DATEPART(ss, CURRENT_TIMESTAMP)) + 's'
	
	-- Map drive
	SET @bcpcmd = 'net use z: /delete&net use z: \\mpg-fs\onvantage'
	EXECUTE master.dbo.xp_cmdshell @bcpcmd
	
	-- Get meeting id from file
	SET @bcpcmd = 'master.dbo.xp_cmdshell "type z:\meeting.txt"'
	CREATE TABLE #TMP (info varchar(200)) 
	INSERT #TMP EXEC (@bcpcmd)
	SET @mtgid = (SELECT TOP 1 info FROM #TMP)
	DROP TABLE #TMP

	-- Store Divisions ID, Wave Number, Group Number and Comp Site Number in local vars
	SELECT @divid = d_ID, @wavenum = d_WaveNum, @groupnum = d_GroupNum, @compsitenum = CompSiteNum
	FROM dbo.Divisions
	INNER JOIN GoldCS.dbo.MtgMain ON d_mtgid = MtgId
	WHERE d_MtgId = @mtgid
	
	-- Clear table
	TRUNCATE TABLE dbo.tbPax

	-- Import file
	SET @bcpcmd = 'bcp "Travelreg.dbo.tbPax" in "z:\responses.csv" -SGOLDENEYE -T -w -F2'
	EXECUTE master.dbo.xp_cmdshell @bcpcmd
	
	-- Archive files
	SET @bcpcmd = 'move z:\responses.csv z:\Archive\' + @timestamp + '_' + 'responses.csv'
	SET @bcpcmd = @bcpcmd + '&move z:\meeting.txt z:\Archive\' + @timestamp + '_' + 'meeting.txt'
	EXECUTE master.dbo.xp_cmdshell @bcpcmd
	
	-- Clean data imported from Onvantage file
	UPDATE dbo.tbPax SET 
		TransactionNum = REPLACE(TransactionNum, '"', ''),
		PaxID = REPLACE(PaxID, '"', ''),
		FirstName = REPLACE(FirstName, '"', ''),
		LastName = REPLACE(LastName, '"', ''),
		GuestList = REPLACE(GuestList, '"', ''),
		GuestOf = REPLACE(GuestOf, '"', ''),
		PrimaryReg = REPLACE(PrimaryReg, '"', ''),
		RSVPStatus = REPLACE(RSVPStatus, '"', ''),
		BusEmail = REPLACE(BusEmail, '"', '')
	
	-- Remove mapped drive
	EXECUTE master.dbo.xp_cmdshell 'net use z: /delete'

--GOTO SKIPCHECK

	-- Check to see if any Primary Registrants are not in the Users table for the specified Meeting/Division
	IF EXISTS (SELECT 1
						FROM tbPax
						LEFT JOIN Users ON BusEmail = u_LoginName AND u_DivisionID = @divid
						WHERE PrimaryReg = 'Yes'
						AND u_ID IS NULL)
		BEGIN
			SELECT 'Please check the email addresses below for mismatches with entries in the Users table:'

			SELECT tbPax.*
			FROM tbPax
			LEFT JOIN Users ON BusEmail = u_LoginName AND u_DivisionID = @divid
			WHERE PrimaryReg = 'Yes'
			AND u_ID IS NULL

			RETURN
		END

SKIPCHECK:
	
	-- Update Onvantage ID for Primary Registrants
	UPDATE Users SET u_ONVID = PaxID
	FROM tbPax
	INNER JOIN Users ON BusEmail = u_LoginName AND u_DivisionID = @divid
	WHERE PrimaryReg = 'Yes'
	AND u_ONVID IS NULL

	-- Update Guest Onvantage ID for Primary Registrants
	UPDATE Users SET u_GONVID = GONVID
	FROM tbPax
	INNER JOIN Users ON BusEmail = u_LoginName AND u_DivisionID = @divid
	INNER JOIN (
		SELECT DISTINCT Guest.PaxID GONVID, Pax.PaxID ONVID
		FROM tbPax Guest
		INNER JOIN tbPax Pax ON Guest.TransactionNum = Pax.TransactionNum AND Pax.PrimaryReg = 'Yes'
		WHERE Guest.PrimaryReg = 'No'
	) Guests ON u_ONVID = ONVID
	WHERE PrimaryReg = 'Yes'
	AND u_GONVID IS NULL

	-- Insert Primary Registrant who have accepted into a temporary table for processing
	SELECT PaxID, FirstName, LastName, BusEmail
	INTO #Primary
	FROM dbo.tbPax
	INNER JOIN dbo.Users ON PaxID = u_ONVID AND u_PersonNum = 0
	WHERE RSVPStatus = 'Accepted'
	AND PrimaryReg = 'Yes'

	-- Process Primary registrants
	WHILE EXISTS (SELECT 1 FROM #Primary)
		BEGIN
			-- Get next personnum and regnum from GoldCS
			EXEC @PersonNum = GoldCS.dbo.sps_NextPKValue 'PplMain'
			EXEC @RegNum = GoldCS.dbo.sps_NextRegNum @mtgid
	
			-- Ensure we received a valid number
			IF (@PersonNum < 1) OR (@RegNum < 1) GOTO NextPrimary
	
			-- Get Pax to process
			SELECT TOP 1 @PaxID = PaxID FROM #Primary

			-- Begin transaction
			BEGIN TRAN
	
			-- Add Pax to GoldCS
			INSERT INTO GoldCS.dbo.PplMain (PersonNum, FirstName, LastName, CmpEMail, CompSiteNum)
			SELECT @PersonNum, FirstName, LastName, BusEmail, @compsitenum
			FROM #Primary
			WHERE PaxID = @PaxID

			-- Ensure record was added
			IF @@ROWCOUNT <> 1
				BEGIN
					ROLLBACK TRAN
					GOTO NextPrimary
				END
	
			-- Add Pax to Meeting
			INSERT INTO GoldCS.dbo.MtgReg (MtgID, RegNum, PersonNum, WaveNum, ATypeNum, GroupNum, PartyMember, AStatusNum, MailTo)
			VALUES (@mtgid, @RegNum, @PersonNum, @wavenum, 1, @groupnum, 'P', 1, 2)
	
			-- Ensure record was added
			IF @@ROWCOUNT <> 1
				BEGIN
					ROLLBACK TRAN
					GOTO NextPrimary
				END
	
			-- Update PersonNum and RegNum for Pax in Users table
			UPDATE Users SET u_PersonNum = @PersonNum, u_RegNum = @RegNum
			WHERE u_ONVID = @PaxID
			AND u_DivisionID = @divid

			-- Ensure record was added
			IF @@ROWCOUNT <> 1
				BEGIN
					ROLLBACK TRAN
					GOTO NextPrimary
				END
	
			-- Commit transaction
			COMMIT TRAN
	
	NextPrimary:
			-- Delete processed Pax from temporary table
			DELETE FROM #Primary WHERE PaxID = @PaxID
	
			-- Clear Vars
			SET @PersonNum = 0
			SET @PaxID = 0
	
		END

	-- Drop temporary table	
	DROP TABLE #Primary

	-- Insert Guest Registrants who have accepted into a temporary table for processing
	SELECT PaxID 'GuestID', FirstName, LastName, BusEmail, u_ONVID, u_PersonNum
	INTO #Guest
	FROM dbo.tbPax
	INNER JOIN dbo.Users ON PaxID = u_GONVID AND u_GuestRegNum = 0
	WHERE RSVPStatus = 'Accepted'
	AND PrimaryReg = 'No'

	-- Process Guest registrants
	WHILE EXISTS (SELECT 1 FROM #Guest)
		BEGIN
			-- Get next personnum and regnum from GoldCS
			EXEC @PersonNum = GoldCS.dbo.sps_NextPKValue 'PplMain'
			EXEC @RegNum = GoldCS.dbo.sps_NextRegNum @mtgid
	
			-- Ensure we received a valid number
			IF (@PersonNum < 1) OR (@RegNum < 1) GOTO NextGuest
	
			-- Get Guest to process
			SELECT TOP 1 @PaxID = GuestID, @PrimaryNum = u_PersonNum FROM #Guest

			-- Begin transaction
			BEGIN TRAN
	
			-- Add Guest to GoldCS
			INSERT INTO GoldCS.dbo.PplMain (PersonNum, FirstName, LastName, CmpEMail, CompSiteNum)
			SELECT @PersonNum, FirstName, LastName, BusEmail, @compsitenum
			FROM #Guest
			WHERE GuestID = @PaxID

			-- Ensure record was added
			IF @@ROWCOUNT <> 1
				BEGIN
					ROLLBACK TRAN
					GOTO NextGuest
				END
	
			-- Add Guest to Meeting
			INSERT INTO GoldCS.dbo.MtgReg (MtgID, RegNum, PersonNum, WaveNum, ATypeNum, GuestOfNum, GroupNum, PartyMember, AStatusNum, MailTo)
			VALUES (@mtgid, @RegNum, @PersonNum, @wavenum, 4, @PrimaryNum, @groupnum, 'G', 1, 2)
	
			-- Ensure record was added
			IF @@ROWCOUNT <> 1
				BEGIN
					ROLLBACK TRAN
					GOTO NextGuest
				END
	
			-- Update GuestRegNum for Primary in Users table
			UPDATE Users SET u_GuestRegNum = @RegNum
			WHERE u_GONVID = @PaxID
			AND u_DivisionID = @divid

			-- Ensure record was added
			IF @@ROWCOUNT <> 1
				BEGIN
					ROLLBACK TRAN
					GOTO NextGuest
				END
	
			-- Commit transaction
			COMMIT TRAN
	
	NextGuest:
			-- Delete processed Guest from temporary table
			DELETE FROM #Guest WHERE GuestID = @PaxID
	
			-- Clear Vars
			SET @PersonNum = 0
			SET @PaxID = 0
	
		END
	
	DROP TABLE #Guest

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

