-- Employee search
select --top 20
	emp.FIRSTNAME
	,emp.LASTNAME
	,evt.EVDESCR
	,rdr.READERDESC
	,convert(varchar, dateadd(second, datediff(second, getdate(), getutcdate()) * -1, evts.EVENT_TIME_UTC), 100) 'DateTimeStamp'
from dbo.EMP emp
inner join dbo.EVENTS evts on emp.ID = evts.EMPID
inner join dbo.EVENT evt on evts.EVENTID = evt.EVENTID
inner join dbo.READER rdr on evts.DEVID = rdr.READERID
where emp.ID in (
1861
,940
,1972
,1966
,1974
,1973
,2031
,2002
,967
,2009
)
and evts.EVENT_TIME_UTC between '5/1/2013' and '7/6/2013'
order by emp.LASTNAME, evts.EVENT_TIME_UTC desc

--Dan Schiff 1861
--Ramon Alequin 940
--Ramya Dama 1972
--Jaocb Airov 1966
--Geoff Walano 1974
--John Tepe 1973
--Delfin Reyes
--Manoja Addala 2031
--Destiny Baskerville 2002
--Maria Canhoto 967
--Isiah Kearney 2009

--select *
--from dbo.EMP
--where LASTNAME in (
--'Schiff'
--,'Alequin'
--,'Dama'
--,'Airov'
--,'Walano'
--,'Tepe'
--,'Reyes'
--,'Addala'
--,'Baskerville'
--,'Canhoto'
--,'Kearney'
--)
--order by LASTNAME

--select *
--from dbo.EMP
--where FIRSTNAME like 'de%'