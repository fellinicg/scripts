USE [MadisonPortal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE dbo.GetVendorsForPortalAccess
(
@VendorType int
)
AS

/* &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
gets all vendors with no portal access, active portal access, and terminated portal access;
depending on input parameter
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */

IF @VendorType = 0
BEGIN
	--get all vendors from SBT with Ip that have not yet been added to the Portal
	SELECT vendno AS VendID, company + ' - ' + vendno AS Vendor
	FROM Catalog.dbo.vwVendors
	WHERE vendno NOT IN (SELECT u_AltID FROM MadisonPortal.dbo.Users WHERE u_Type = 'V')
	ORDER BY company
END
IF @VendorType = 1
BEGIN
	--get all active vendors from the portal
	SELECT u_AltID AS VendID, company + ' - ' + vendno AS Vendor
	FROM MadisonPortal.dbo.Users
	INNER JOIN Catalog.dbo.vwVendors ON u_AltID = vendno
	WHERE u_Type = 'V'
	AND u_RecState = 0
	ORDER BY company
END
IF @VendorType = 2
BEGIN
	--get all terminated vendors from the portal that still have a SBT status of IP
	SELECT u_AltID AS VendID, company + ' - ' + vendno AS Vendor
	FROM MadisonPortal.dbo.Users
	INNER JOIN Catalog.dbo.vwVendors ON u_AltID = vendno
	WHERE u_Type = 'V'
	AND u_RecState = 2
	ORDER BY company
END
