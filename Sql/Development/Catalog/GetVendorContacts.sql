USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/*
Returns all vendor Contacts for one Vendor
in the Master Calaog
*/
ALTER  PROCEDURE dbo.GetVendorContacts
(
@vendno		varchar(25)
)
AS

SELECT agrtid id_col, telephone_7 contact
FROM SQL2KPROD2.Agresso.dbo.agladdress
WHERE dim_value = @vendno
	AND sequence_no > 0
	AND client = 'MP'
	AND address_type = '1'
