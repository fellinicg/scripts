USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER FUNCTION dbo.GetVendor  (@Value VARCHAR(6))
RETURNS VARCHAR(500)
AS
BEGIN

	DECLARE @RetVal VARCHAR(500)

	SELECT @Retval = company
	FROM dbo.vwVendors
	WHERE vendno = @Value

	RETURN(@RetVal)

END