USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/*
Updates an existing Vendor in the
Master Catalog based on id
*/
ALTER PROCEDURE dbo.UpdateExistingVendor
(
@vendno		varchar(25),
@phone		varchar(20),
@faxno		varchar(20),
@email		varchar(35),
@contact	varchar(20),
@ModifiedBy	int,
@phone2		varchar(20)
)
AS

	UPDATE SQL2KPROD2.Agresso.dbo.agladdress
	SET 
		telephone_1 = @phone,
		telephone_2 = @faxno,
		e_mail = @email,
		telephone_7 = @contact,
		telephone_3 = @phone2
	WHERE dim_value = @vendno
	AND sequence_no = 0
	AND client = 'MP'
	AND address_type = '1'
