USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
returns the provided vendors memo, its send to vendor id,
and its send to vendor's memo fields
*/
ALTER PROCEDURE dbo.GetVendorSendToMemos
(
@vst_vendno varchar(6)
)
AS

DECLARE @vst_sendto varchar(6)
DECLARE @sendmemo	varchar(5000)

SET @vst_sendto = 
(SELECT vst_sendto FROM dbo.VendorSendTo WHERE vst_vendno = @vst_vendno)

IF @vst_sendto IS NOT NULL
	BEGIN
		SET @sendmemo = 
		(SELECT CONVERT(VARCHAR(5000),vndmemo)
		FROM SQL2KPROD.MPGPRO02.dbo.apvend
		WHERE vendno = @vst_sendto)
	END

SELECT vndmemo, vst_sendto, @sendmemo as SendMemo
FROM SQL2KPROD.MPGPRO02.dbo.apvend
LEFT JOIN dbo.VendorSendTo ON vendno = vst_vendno
COLLATE SQL_Latin1_General_CP1_CI_AS
WHERE vendno = @vst_vendno
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF