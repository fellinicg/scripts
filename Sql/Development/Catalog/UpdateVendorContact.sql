USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Updates an existing Vendor Contact in the
Master Catalog based on id
*/
ALTER  PROCEDURE dbo.UpdateVendorContact
(
@id_col		bigint,
@contact	varchar(20),
@address1	varchar(30),
@address2	varchar(30),
@city		varchar(20),
@zip		varchar(10),
@faxno		varchar(20),
@phone	varchar(20),
@country	varchar(15),
@addrstate	varchar(10),
@email		varchar(35),
@notes		varchar(500),
@ModifiedBy	int,
@phone2	varchar(20)
)
AS

	UPDATE SQL2KPROD2.Agresso.dbo.agladdress
	SET 
		telephone_7 = @contact,
		address = CONVERT(char(40), @address1) + @address2,
		place = @city,
		zip_code = @zip,
		telephone_2 = @faxno,
		telephone_1 = @phone,
		country_code = @country,
		province = @addrstate,
		e_mail = @email,
--		notes = @notes,
		telephone_3 = @phone2
	WHERE agrtid = @id_col