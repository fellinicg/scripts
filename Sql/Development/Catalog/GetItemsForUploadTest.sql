USE [Catalog]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.GetItemsForUploadTest

AS

SELECT
mc_ID AS [Item#], 
mc_SDesc AS [Description],
rs_SDesc AS [Status],
mc_Model AS [Model],
dbo.DelimitOptions(mc_ID) AS [Options],
ip_MSRP AS [MSRP],
ip_Cost AS [Cost],
isi_ShipCost AS [Shipping]
FROM MasterCatalog
INNER JOIN ItemPricing ON mc_ip_ID = ip_ID
INNER JOIN ItemShippingInfo ON mc_isi_ID = isi_ID
INNER JOIN dbo.vwVendors ON mc_v_Number = vendno
INNER JOIN dbo.RecStatuses ON mc_rs_RecStatus = rs_RecStatus
WHERE mc_rs_RecStatus IN (0,1)
ORDER BY mc_rs_RecStatus, mc_SDesc