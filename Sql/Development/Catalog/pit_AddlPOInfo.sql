USE [Catalog]
GO
-- Add a new column to the table
ALTER TABLE Catalog.dbo.ProgramItemTypes
	ADD pit_AddlPOInfo varchar(500) NULL
GO
-- Update column
UPDATE Catalog.dbo.ProgramItemTypes SET pit_AddlPOInfo = '*PLEASE SHIP VIA USPS FIRST CLASS MAIL' 
WHERE pit_ProgramID IN (202, 182)
AND pit_ItemType = 'G'