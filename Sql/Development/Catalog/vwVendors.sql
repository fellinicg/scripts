USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER VIEW dbo.vwVendors
AS

	SELECT 
		vendno, company, contact, title, address1, address2, city, zip, country, phone, phone2, faxno, comment, 
		vndmemo, email, addrstate
	FROM OPENQUERY(SQL2KPROD2, 
		'SELECT ah.apar_id vendno, ah.apar_name company, ag.telephone_7 contact, ag.pos_title title, LEFT(ag.address, 40) address1, 
			RIGHT(ag.address, 120) address2, ag.place city, ag.zip_code zip, ag.country_code country, 
			ag.telephone_1 phone, ag.telephone_3 phone2, ag.telephone_2 faxno, SPACE(1) comment, SPACE(1) vndmemo, ag.e_mail email, 
			ag.province addrstate
		FROM SQL2KPROD2.Agresso.dbo.asuheader ah
		INNER JOIN SQL2KPROD2.Agresso.dbo.agladdress ag ON ah.apar_id = ag.dim_value AND ah.client = ag.client
		WHERE ah.apar_gr_id = ''S6''
			AND ah.client = ''MP''
			AND ag.attribute_id = ''A5''')

