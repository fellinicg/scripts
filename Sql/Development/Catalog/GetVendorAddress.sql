USE [MadisonPortal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE dbo.GetVendorAddress 
(
@vendno varchar(6),
@Indicator int = 0
)

AS

IF @Indicator = 0
	BEGIN
		SELECT 
		company, 
		address1, 
		address2, 
		city, 
		addrstate, 
		zip, 
		country
		FROM Catalog.dbo.vwVendors
		WHERE vendno = @vendno
	END
ELSE
	EXECUTE GetCheckRequestPayeeInfo @vendno

