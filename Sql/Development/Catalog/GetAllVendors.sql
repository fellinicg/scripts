USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/*
Returns list of all vendors
in the Master Calaog
*/
ALTER PROCEDURE dbo.GetAllVendors
AS

SELECT vendno AS 'v_ID', company AS 'v_Name'
FROM dbo.vwVendors
ORDER BY company
