USE [Catalog]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*
returns all items by vendors if the status of the item
is either available or back-ordered
*/
ALTER PROCEDURE dbo.RPTGetAllItemsByVendor

AS

SELECT
vst_sendto [Send To#], 
SEN.company AS [Send To],
mc_v_Number [Vendor#],
VEN.company AS [Vendor],
mc_ID AS [Item#], 
mc_SDesc AS [Description],
rs_SDesc AS [Status],
mc_Model AS [Model],
dbo.DelimitOptions(mc_ID) AS [Options],
ip_MSRP AS [MSRP],
ip_Cost AS [Cost],
isi_ShipCost AS [Shipping]
FROM MasterCatalog
INNER JOIN ItemPricing ON mc_ip_ID = ip_ID
INNER JOIN ItemShippingInfo ON mc_isi_ID = isi_ID
INNER JOIN dbo.vwVendors VEN ON mc_v_Number = VEN.vendno COLLATE SQL_Latin1_General_CP1_CI_AS
INNER JOIN dbo.RecStatuses ON mc_rs_RecStatus = rs_RecStatus
LEFT JOIN dbo.VendorSendTo ON mc_v_Number = vst_vendno
LEFT JOIN dbo.vwVendors SEN ON vst_sendto = SEN.vendno
WHERE mc_rs_RecStatus IN (0,1)
AND VEN.vendno <> ''
ORDER BY SEN.company, VEN.company, mc_v_Number, mc_rs_RecStatus, mc_SDesc