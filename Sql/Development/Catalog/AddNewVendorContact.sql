USE [Catalog]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*
Add a new Vendor Contact to the
Master Catalog 
*/
ALTER  PROCEDURE dbo.AddNewVendorContact
(
@vendno	varchar(25),
@contact	varchar(20),
@address1	varchar(30),
@address2	varchar(30),
@city		varchar(20),
@zip		varchar(10),
@faxno		varchar(20),
@phone	varchar(20),
@country	varchar(15),
@addrstate	varchar(10),
@email		varchar(35),
@notes		varchar(500),
@ModifiedBy	int,
@phone2	varchar(20)
)
AS

--DECLARE @address_id bigint
--
--SELECT @address_id = MAX(address_id) + 1
--FROM SQL2KPROD2.Agresso.dbo.agladdress
--WHERE client = 'MP'

--INSERT INTO MPGPRO02.dbo.apvadr
--	(vendno, contact, address1, address2, city,
--	zip, faxno, phone, country, addrstate, email,
--	notes, adduser, defaremit, phone2)
--VALUES	(@vendno, @contact, @address1, @address2, @city,
--	@zip, @faxno, @phone, @country, @addrstate, @email,
--	@notes, @ModifiedBy, 'N',@phone2)
--SELECT @@IDENTITY

SELECT 0