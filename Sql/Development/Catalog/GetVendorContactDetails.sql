USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/*
Returns all vendor Contact Details for one Vendor
in the Master Calaog
*/
ALTER  PROCEDURE dbo.GetVendorContactDetails
(
@id_col		bigint
)
AS

SELECT agrtid id_col, ag.telephone_7 contact, LEFT(address, 40) address1, RIGHT(address, 120) address2, 
	ag.place city, ag.zip_code zip, ag.telephone_2 faxno, ag.telephone_1 phone, ag.country_code country, 
	ag.province addrstate, ag.e_mail email, '' notes, ag.telephone_3 phone2
FROM SQL2KPROD2.Agresso.dbo.agladdress ag
WHERE agrtid = @id_col