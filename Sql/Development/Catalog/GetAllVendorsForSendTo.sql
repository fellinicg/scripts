USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
returns all vendors from SBT that are IP,
then:
attaches an -NA-  to all that have no match in the VendorSendTo table in the vst_sendto column
uses last column for sort purposes
or:
just returns all sorted by company

decision is based on retrieval for filed vendor or send to in application
*/
ALTER PROCEDURE dbo.GetAllVendorsForSendTo 
(
@indicator int = 0
)
AS

IF @indicator = 0 
	BEGIN
		SELECT RTRIM(vendno), CASE WHEN vst_sendto IS NULL THEN '-NA- ' + company ELSE company END AS company, CASE WHEN vst_sendto IS NULL THEN 0 ELSE 1 END AS 'match'
		FROM dbo.vwVendors
		LEFT JOIN dbo.VendorSendTo ON vendno = vst_vendno COLLATE SQL_Latin1_General_CP1_CI_AS
		ORDER BY match, company
	END 
IF @indicator = 1 
	BEGIN
		SELECT RTRIM(vendno), company
		FROM dbo.vwVendors
		ORDER BY company
	END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF