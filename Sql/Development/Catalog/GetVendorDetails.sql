USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/*
Returns all vendor details for one Vendor
in the Master Calaog
*/
ALTER PROCEDURE dbo.GetVendorDetails
(
@vendno		varchar(6)
)
AS

SELECT vendno, company, contact, address1, address2,
	city, zip, country, phone, faxno, email, addrstate, phone2,
	crpi_Bank, crpi_Bank_Addr1, crpi_Bank_Addr2, crpi_PayeeAcctNo, crpi_ABA
FROM dbo.vwVendors
LEFT JOIN MadisonPortal.dbo.CheckRequestPayeeInfo ON vendno = CONVERT(varchar,crpi_VendID)
WHERE vendno = @vendno