USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.RPTCrossTabOneProgram
(
@Program_ID	int
)
AS

DECLARE @RowCt INT
DECLARE @i INT
DECLARE @MerchCalc VARCHAR(500)
DECLARE @CardCalc VARCHAR(500)
DECLARE @TravelCalc VARCHAR(500)
DECLARE @p_ID VARCHAR(10)


SELECT
mc_ID, 	mc_SDesc, dbo.DelimitCategories(mc_ID) 'Cats',
mc_Model, it_SDesc, mc_v_Number, company 'Vendor',
ip_MSRP, ip_Cost, isi_ShipCost, 
ip_Cost * ISNULL(slt_Rate, 0) 'Tax',
ip_Cost + isi_ShipCost + ip_Cost * ISNULL(slt_Rate, 0) 'TotCost'
INTO #MASTER
FROM MasterCatalog
--NEW LINE
INNER JOIN dbo.ProgramCatalogs ON mc_ID = pc_mc_ID AND GETDATE() BETWEEN pc_StartDate AND pc_EndDate
LEFT JOIN ItemTypes ON mc_it_ID = it_ID
LEFT JOIN ItemPricing ON mc_ip_ID = ip_ID
LEFT JOIN ItemShippingInfo ON mc_isi_ID = isi_ID
LEFT JOIN SalesTax ON slt_State = 'NY' AND GETDATE() BETWEEN slt_StartDate AND slt_EndDate
LEFT JOIN dbo.vwVendors ON mc_v_Number = vendno
--NEW LINE
WHERE pc_p_ID = @Program_ID
--NEW INGO LINE 10-11-04
AND mc_rs_RecStatus IN (0,1)

SELECT p_ID, 
ISNULL( pm_AddlFunction, '') + dbo.PriceCalc('M', p_ID, CASE ISNULL(pm_MarkUpOn, 'C') WHEN 'C' THEN 'ip_Cost' ELSE 'ip_MSRP' END) AS 'Merch',
ISNULL( pm_AddlFunction, '') + dbo.PriceCalc('G', p_ID, CASE ISNULL(pm_MarkUpOn, 'C') WHEN 'C' THEN 'ip_Cost' ELSE 'ip_MSRP' END) AS 'Card',
ISNULL( pm_AddlFunction, '') + dbo.PriceCalc('T', p_ID, CASE ISNULL(pm_MarkUpOn, 'C') WHEN 'C' THEN 'ip_Cost' ELSE 'ip_MSRP' END) AS 'Travel',
p_SDesc
INTO #TEMP
FROM Programs
LEFT JOIN ProgramMarkups ON p_ID = pm_p_ID
WHERE (GETDATE() BETWEEN pm_StartDate AND pm_EndDate OR pm_StartDate IS NULL)
--NEW LINE
AND p_ID = @Program_ID
ORDER BY p_SDesc

SET @RowCt = @@ROWCOUNT
SET @i = 1


CREATE TABLE #TmpPts (tmp_p_ID INT NULL, tmp_mc_ID INT NULL, tmp_Points INT NULL)

WHILE @i <= @RowCt
 BEGIN
  SELECT TOP 1 @MerchCalc = Merch, @CardCalc = Card, @TravelCalc=Travel, @p_ID = p_ID FROM #TEMP ORDER BY p_SDesc
  EXEC ('ALTER TABLE #MASTER ADD prg' + @p_ID + ' INT NULL')
  EXEC('INSERT INTO #TmpPts
  SELECT p_ID, mc_ID,
  CASE mc_it_ID WHEN 3 THEN ' + @MerchCalc + '
  WHEN 4 THEN ' + @TravelCalc + '
  ELSE ' + @CardCalc +' END AS Points
  FROM Programs
  INNER JOIN ProgramCatalogs ON p_ID = pc_p_ID
  INNER JOIN MasterCatalog ON pc_mc_ID = mc_ID
  INNER JOIN ItemPricing ON mc_ip_ID = ip_ID
  LEFT JOIN ItemShippingInfo ON mc_isi_ID = isi_ID AND p_IncludeShipping = 1
  LEFT JOIN SalesTax ON GETDATE() BETWEEN slt_StartDate AND slt_EndDate AND p_IncludeTax = 1
  LEFT JOIN ProgramMarkups ON p_ID = pm_p_ID AND GETDATE() BETWEEN pm_StartDate AND pm_EndDate
  LEFT JOIN RecStatuses ON mc_rs_RecStatus = rs_RecStatus 
  LEFT JOIN ProgramItemMarkup ON pc_ID = pim_pc_ID AND GETDATE() BETWEEN pim_StartDate AND pim_EndDate 
  WHERE p_ID = ' + @p_ID + '
  AND GETDATE() BETWEEN pc_StartDate AND pc_EndDate
  AND mc_rs_RecStatus IN (0,1)
  ORDER BY rs_SDesc, mc_SDesc')
  DELETE FROM #TEMP WHERE p_ID = (SELECT TOP 1 p_ID FROM #TEMP ORDER BY p_SDesc)
  SET @i = @i + 1
 END

INSERT INTO #TEMP (p_ID)
SELECT DISTINCT tmp_p_ID
FROM #TmpPts
ORDER BY tmp_p_ID

SET @RowCt = @@ROWCOUNT
SET @i = 1

WHILE @i <= @RowCt
 BEGIN
  SELECT TOP 1 @p_ID = p_ID FROM #TEMP ORDER BY p_ID
  EXEC('UPDATE #MASTER SET prg' + @p_ID + ' = tmp_Points
  FROM #MASTER
  INNER JOIN #TmpPts ON mc_ID = tmp_mc_ID AND tmp_p_ID = ' + @p_ID)
  DELETE FROM #TEMP WHERE p_ID = (SELECT TOP 1 p_ID FROM #TEMP ORDER BY p_ID)
  SET @i = @i + 1
 END

CREATE TABLE #TmpHeader (Header VARCHAR(200))

INSERT INTO #TmpHeader
SELECT 'Item#'

INSERT INTO #TmpHeader
SELECT 'Description'

INSERT INTO #TmpHeader
SELECT 'Category'

INSERT INTO #TmpHeader
SELECT 'Model'

INSERT INTO #TmpHeader
SELECT 'Type'

INSERT INTO #TmpHeader
SELECT 'Vendor#'

INSERT INTO #TmpHeader
SELECT 'Vendor'

INSERT INTO #TmpHeader
SELECT 'MSRP'

INSERT INTO #TmpHeader
SELECT 'Cost'

INSERT INTO #TmpHeader
SELECT 'Shipping'

INSERT INTO #TmpHeader
SELECT 'Tax'

INSERT INTO #TmpHeader
SELECT 'Total Cost'

INSERT INTO #TmpHeader
SELECT p_SDesc
FROM Programs
--NEW LINE
WHERE p_ID = @Program_ID
ORDER BY p_SDesc

SELECT * FROM #TmpHeader

SELECT * FROM #MASTER ORDER BY mc_ID

DROP TABLE #TmpPts
DROP TABLE #MASTER
DROP TABLE #TEMP
DROP TABLE #TmpHeader