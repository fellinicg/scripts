USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.GetGroupsForUser
(
@u_ID	int
)
AS

	SELECT ug_gr_ID, gr_SDesc AS [Group], 'In' AS InOut
	FROM dbo.UserGroups
	INNER JOIN dbo.Groups ON ug_gr_ID = gr_ID
	INNER JOIN dbo.Users ON ug_u_ID = u_ID
	INNER JOIN GlobalUsers.dbo.tbUserAccount ON u_ID = UserAccountID
	WHERE AccountStatus = 'E' AND u_RecState = 0
	AND ug_u_ID = @u_ID
	UNION
	SELECT gr_ID, gr_SDesc AS [Group], 'Out' AS InOut
	FROM dbo.Groups
	WHERE gr_ID NOT IN (SELECT ug_gr_ID FROM dbo.UserGroups
	INNER JOIN dbo.Groups ON ug_gr_ID = gr_ID
	INNER JOIN dbo.Users ON ug_u_ID = u_ID
	INNER JOIN GlobalUsers.dbo.tbUserAccount ON u_ID = UserAccountID
	WHERE AccountStatus = 'E' AND u_RecState = 0
	AND ug_u_ID = @u_ID AND ug_gr_ID = gr_ID)

ORDER BY InOut, gr_SDesc
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.GetGroupsForUser
TO portaluser