USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.TestVendorPortalAccessStatus
(
@u_AltID varchar(50),
@AccessIndicator tinyint,
@RETURN varchar(50) Output
)
AS

/* First, get the u_ID */
DECLARE @gu_ID int
SET @gu_ID = (SELECT u_ID FROM dbo.Users WHERE u_AltID = @u_AltID)
IF @gu_ID IS NULL
	BEGIN
		SET @RETURN = 'XYZ'
		RETURN
	END


DECLARE @Counter int

SET @Counter = (SELECT u_ID 
			FROM dbo.Users 
			INNER JOIN GlobalUsers.dbo.tbUserAccount ON u_ID = UserAccountID
			WHERE u_ID = @gu_ID
			AND u_RecState = 0
			AND AccountStatus = 'E')

IF @Counter IS NULL
	BEGIN
		SET @RETURN = 'HasNoAccess'
		RETURN
	END
ELSE
	BEGIN
		/* test if acccess should be removed (0) */
		IF @AccessIndicator = 0
			BEGIN
				DECLARE @UpdResult char(1)

				/* has access, remove it */
				EXECUTE GlobalUsers.dbo.upUpdateUserAccount
					@Result = @UpdResult OUTPUT,
					@UserAccountID = @gu_ID,
					@UpdAccountStatus = 1,
					@AccountStatus = 'D'

				IF @@ROWCOUNT = 0 OR @@ERROR <> 0 OR @UpdResult = 'F'
					BEGIN
						SET @RETURN = 'XXX'
					  RETURN
					END

				UPDATE dbo.Users SET u_RecState = 2 WHERE u_ID = @gu_ID
				IF @@ROWCOUNT = 0 OR @@ERROR <> 0
					BEGIN
						SET @RETURN = 'XXX'
						EXECUTE GlobalUsers.dbo.upUpdateUserAccount
							@Result = @UpdResult OUTPUT,
							@UserAccountID = @gu_ID,
							@UpdAccountStatus = 1,
							@AccountStatus = 'E'
					  RETURN
					END
	
				SET @RETURN = 'AccessRemoved'
				COMMIT TRANSACTION
				RETURN
			END
		SET @RETURN = 'HasAccess'
	END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.TestVendorPortalAccessStatus
TO portaluser