USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

/*
add user to AxisPortal user table,
to global user table, and an e-mail, if provided
*/

ALTER PROCEDURE dbo.AddNewPortalUser
(
@gu_FirstName	varchar(50),
@gu_MiddleName	varchar(50) = NULL,
@gu_LastName	varchar(100),
@gu_SSN		varchar(9),
@gu_OldId	varchar(40) = '',
@gu_cl_ID	int = 0,
@u_Password	varchar(50),
@u_Type		char(1),
@EmailAddress	varchar(120) = '',
@u_StartDate	varchar(50) = '',
@u_RecState	int = 0,
@gu_rs_ID	int = 0,
@theReturn	varchar(200) = '' OUTPUT
)

AS

/* declare variables for this sp */
DECLARE @guID int
DECLARE @emID int
DECLARE @Count varchar(2)
DECLARE @UserName varchar(50)
DECLARE @IsPerson bit
DECLARE @AccountName varchar(80)
DECLARE @PersonGovtIDType char(1)
DECLARE @PersonGovtIDCodeTwoWayCrypted varchar(200)
DECLARE @AccountStatus  char(1)
DECLARE @PersonBuiltFullName varchar(200)
DECLARE @Result char(1)

/* set the defaults if not provided as real values */
SET @UserName = LOWER(LEFT(LTRIM(@gu_FirstName),1) + REPLACE(REPLACE(@gu_LastName, ' ', ''), '''', ''))
IF @u_StartDate = '' SET @u_StartDate = NULL
IF UPPER(@u_Type) = 'V' 
	BEGIN
		SET @IsPerson = 0
		SET @AccountName = LTRIM(RTRIM(@gu_FirstName)) + ' ' + LTRIM(RTRIM(@gu_LastName))
		SET @gu_FirstName = ''
		SET @gu_MiddleName = ''
		SET @gu_LastName = ''
		SET @PersonBuiltFullName = ''
	END
ELSE
	BEGIN
		SET @IsPerson = 1
		SET @PersonBuiltFullName = LTRIM(RTRIM(@gu_FirstName)) + ' ' + LTRIM(RTRIM(@gu_LastName))
		SET @AccountName = ''
	END

IF @u_Type = 'M' SET @gu_cl_ID = 5

IF LEN(LTRIM(RTRIM(@gu_SSN))) > 0 
	BEGIN
		SET @PersonGovtIDCodeTwoWayCrypted = master.dbo.ud_CCEncrypt(@gu_SSN)
		SET @PersonGovtIDType = 'S'
	END
ELSE 
	BEGIN
		SET @PersonGovtIDCodeTwoWayCrypted = ''
		SET @PersonGovtIDType = ''
	END
SET @AccountStatus = CASE @gu_rs_ID WHEN 0 THEN 'E' WHEN 2 THEN 'X' ELSE 'D' END

/* insert user into GlobalUser db */
EXECUTE  GlobalUsers.dbo.upAddUserAccount
	@AccountType = @u_Type,
	@IsPerson = @IsPerson,
	@PersonFirstGivenName = @gu_FirstName,
	@PersonSecondGivenName = @gu_MiddleName,
	@PersonFamilyName = @gu_LastName,
	@AccountName = @AccountName,
	@PersonBuiltFullName = @PersonBuiltFullName,
	@AltAccountCode= @gu_OldId,
	@ClientID = @gu_cl_ID,
	@PersonGovtIDType = @PersonGovtIDType,
	@PersonGovtIDCodeTwoWayCrypted = @PersonGovtIDCodeTwoWayCrypted,
	@AccountStatus = @AccountStatus,
	@UserAccountID = @guID OUTPUT
IF @@ERROR <> 0 OR @guID = 0
	BEGIN
		SET @theReturn = ''
		RETURN
	END

/* now add the e-mail, if provided */
IF LEN(LTRIM(RTRIM(@EmailAddress))) > 0
	BEGIN
		EXECUTE GlobalUsers.dbo.upAddEmailAddress
			@UserAccountID = @guID,
			@EMailAddressType = 'W',
			@EmailAddress = @EmailAddress,
			@IsPreferred = 1,
			@EmailAddressID =  @emID OUTPUT
		IF @@ERROR <> 0 OR @emID = 0
			BEGIN
				EXECUTE GlobalUsers.dbo.upDeleteUserAccount @Result OUTPUT, @guID
				SET @theReturn = ''
        RETURN
			END
	END

BEGIN TRANSACTION

/* 
add new user now to Users table
	First - detect if username combination of first letter
	first name plus last name (and numeric increment) already exists
*/ 

/* 
if exists, 
increment a counter until it does no longer exist
*/
SET @Count = ''
WHILE EXISTS (SELECT 1 FROM dbo.Users WHERE u_UserName = @UserName + @Count)
	BEGIN
		SET @Count = CASE @Count WHEN '' THEN 0 ELSE @Count END + 1
	END
SET @UserName = @UserName + @Count


/* now insert the new record */
INSERT INTO dbo.Users(
	u_ID,
	u_UserName,
	u_Password,
	u_Type,
	u_AltID,
	u_StartDate,
	u_RecState)
VALUES(
	@guID,
	LOWER(@UserName),
	master.dbo.ud_CCEncrypt(@u_Password),
	UPPER(@u_Type),
	@gu_OldId,
	@u_StartDate,
	@u_RecState)
IF @@ROWCOUNT = 0
	BEGIN
		ROLLBACK TRANSACTION
		EXECUTE GlobalUsers.dbo.upDeleteUserAccount @Result OUTPUT, @guID
		SET @theReturn = ''
    RETURN
	END

/* Add the default Group to the user if it is I or V*/
IF @u_Type = 'M'
	BEGIN
		EXECUTE AddUserToGroup 1, @guID
	END
IF @u_Type = 'V'
	BEGIN
		EXECUTE AddUserToGroup 2, @guID
	END

/* Now assign default home page content.*/
IF @u_Type = 'M'
	BEGIN
	INSERT INTO dbo.HomeContent (
		hc_Table, 
		hc_uc_ID, 
		hc_u_ID, 
		hc_SortOrder)
	SELECT DISTINCT 
		hc.hc_Table, 
		hc.hc_uc_ID, 
		@guID, 
		hc.hc_SortOrder
	FROM dbo.HomeContent hc
	LEFT JOIN dbo.HomeContent hc1 ON hc.hc_uc_ID = hc1.hc_uc_ID AND hc1.hc_u_ID = @guID
	WHERE hc.hc_u_ID = 1
	AND hc1.hc_uc_ID IS NULL
END 
IF @u_Type = 'V'
	BEGIN
	INSERT INTO dbo.HomeContent (
		hc_Table, 
		hc_uc_ID, 
		hc_u_ID, 
		hc_SortOrder)
	SELECT DISTINCT 
		hc.hc_Table, 
		hc.hc_uc_ID, 
		@guID, 
		hc.hc_SortOrder
	FROM dbo.HomeContent hc
	LEFT JOIN dbo.HomeContent hc1 ON hc.hc_uc_ID = hc1.hc_uc_ID AND hc1.hc_u_ID = @guID
	WHERE hc.hc_u_ID = 2
	AND hc1.hc_uc_ID IS NULL
END 

SET @theReturn =  @UserName

/* all done, let's commit */
COMMIT TRANSACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
GRANT EXECUTE
ON dbo.AddNewPortalUser
TO portaluser