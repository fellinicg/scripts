USE [MadisonPortal]
GO
/****** Object:  StoredProcedure [dbo].[GetGlobalUserPhone]    Script Date: 06/15/2006 15:58:41 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE dbo.GetGlobalUserPhone 
(
@cn_ID	int
)

AS

	SELECT 
		PhoneNumType,
		CASE WHEN LEN(ISNULL(CityAreaCode, '') + ISNULL(PhoneNum, '')) = 0 THEN ISNULL(OrigPhoneNum, '') ELSE ISNULL(CityAreaCode, '') + ISNULL(PhoneNum, '') END,
		Extension,
		IsPreferred
	FROM GlobalUsers.dbo.tbPhoneNum
	WHERE PhoneNumID = @cn_ID

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.GetGlobalUserPhone
TO portaluser