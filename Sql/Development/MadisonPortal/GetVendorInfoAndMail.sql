USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.GetVendorInfoAndMail
(
@v_ID int
)
AS

	/* First, get the real user id from the provided vendor id */
	DECLARE @gu_ID int
	SET @gu_ID = (SELECT u_ID FROM dbo.Users WHERE u_AltID = @v_ID)
	IF (@gu_ID IS NULL) RETURN

	SELECT u_UserName, master.dbo.ud_CCDecrypt(u_Password) AS Pwd, ua.AccountName AS gu_LastName, EmailAddress AS em_Address
	FROM dbo.Users
	INNER JOIN GlobalUsers.dbo.tbUserAccount ua ON u_ID = ua.UserAccountID
	LEFT JOIN GlobalUsers.dbo.tbEmailAddress ea ON ua.UserAccountID = ea.UserAccountID AND EMailAddressType = 'W'
	WHERE u_ID = @gu_ID
	AND AccountStatus = 'E'
	AND u_RecState = 0
	ORDER BY ea.IsPreferred DESC, EmailAddressID DESC

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.GetVendorInfoAndMail
TO portaluser