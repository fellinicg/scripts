USE [MadisonPortal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  PROCEDURE dbo.GetCSPrograms @u_ID INT =0 AS

DECLARE @CSFlag TINYINT

IF @u_ID > 0 SELECT @CSFlag = COUNT(pga_m_ID) FROM dbo.GetUserModules(@u_ID) WHERE pga_m_ID = 14
ELSE SET @CSFlag = 1

Select p_ID,
p_Code,
p_SDesc,
p_DKNo,
CASE @CSFLAG WHEN 0 THEN '' ELSE ISNULL(p_CustSvcSite, '') END AS p_CustSvcSite,
REPLACE(PersonFirstGivenName + ' ' + PersonFamilyName, 'System System', 'Internal') AS 'Manager',
'http://www.madisonpg.com/catalogdemo/login.asp?txtUserName=' + rtrim(p_Code) + '&txtPassword=rewards' AS 'ProdCat',
'http://dev2.madisonpg.com/shopping_Version2/shopstart.asp?progcode=' + rtrim(p_Code) + '&UID=123456789&earnedPoints=100&username=Administrator' AS 'DevCat'
FROM Catalog.dbo.Programs
LEFT JOIN GlobalUsers.dbo.tbUserAccount ON p_gu_ID = UserAccountID
WHERE p_rs_RecStatus = 0
ORDER BY p_SDesc
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.GetCSPrograms
TO portaluser