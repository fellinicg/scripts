USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.iv_getInvoiceComplex
(
@TwoWeeks bit,
@iv_ID int
)

AS

DECLARE @WHERE varchar(50)
SET @WHERE = ''

IF @TwoWeeks > 0
	SET @WHERE = ' WHERE iv_Date > GETDATE() - 14 '
IF @iv_ID > 0
	SET @WHERE = ' WHERE iv_ID = ' + CONVERT(varchar,@iv_ID) + ' '

EXECUTE ('SELECT iv_ID, iv_Date, PersonBuiltFullName AS [Name], iv_DK, description company, iv_Amount, iv_Comments, iv_Copy, iv_Processed, iv_Void, iv_DT
FROM dbo.Invoices
INNER JOIN GlobalUsers.dbo.tbUserAccount ON iv_User_ID = UserAccountID
INNER JOIN SQL2KPROD2.Agresso.dbo.atsproject ON CONVERT(varchar, iv_DK) = project'
+ @WHERE + '
ORDER BY iv_Date DESC')
