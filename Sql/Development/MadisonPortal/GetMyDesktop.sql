USE [MadisonPortal]
GO
/****** Object:  StoredProcedure [dbo].[GetMyDesktop]    Script Date: 06/16/2006 11:22:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER   PROCEDURE dbo.GetMyDesktop @Userid INT AS

SELECT DISTINCT
m_ID, m_SDesc, m_Desc, m_mt_ID, m_Image, ISNULL(pp_Path, '') + ISNULL(pp_Page, '') AS 'pp_Page', ISNULL(pp_Target, '_self') AS 'pp_Target', m_SortOrder
FROM dbo.Modules
INNER JOIN dbo.GetUserModules(@Userid) ON m_ID = pga_m_ID
LEFT JOIN dbo.PortalPages ON m_pp_ID = pp_ID
WHERE m_Parent = 55
ORDER BY m_mt_ID, m_SortOrder
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.GetMyDesktop
TO portaluser