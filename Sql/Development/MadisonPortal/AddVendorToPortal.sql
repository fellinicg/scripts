USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE [dbo].[AddVendorToPortal]
(
@gu_LastName varchar(100),
@gu_OldId int,
@UserName varchar(50) ,
@em_Address varchar(120),
@em_Address1 varchar(120) = '',
@em_Address2 varchar(120) = '',
@u_Password varchar(50) OUTPUT
)
AS

-- Local variable declarations
DECLARE @guID int
DECLARE @PW varchar(50) 
DECLARE @Random varchar(2)
DECLARE @emID int
DECLARE @Result char(1)

-- Test if vendor name already exists in db
IF EXISTS (SELECT 1 FROM dbo.Users WHERE u_UserName = @UserName)
	BEGIN
		SET @u_Password = 'XXX'
	  RETURN
	END

-- Generate the random User Password
SET @Random = (SELECT CONVERT(VARCHAR,ROUND(RAND()*100,0)))
IF LEN(@Random) = 1
	SET @Random = '0' + @Random
SET @PW = LEFT(@UserName,4) + @Random

-- Insert user into GlobalUser db
EXECUTE  GlobalUsers.dbo.upAddUserAccount
	@AccountType = 'V',
	@IsPerson = 0,
	@AccountName = @gu_LastName,
	@AltAccountCode= @gu_OldId,
	@ClientID = 0,
	@AccountStatus = 'E',
	@UserAccountID = @guID OUTPUT

IF @@ERROR <> 0 OR @guID = 0
	BEGIN
		SET @u_Password = 'YYY'
		RETURN
	END

-- Add the e-mail default contact
IF LEN(LTRIM(RTRIM(@em_Address))) > 0
	BEGIN
		EXECUTE GlobalUsers.dbo.upAddEmailAddress
			@UserAccountID = @guID,
			@EMailAddressType = 'W',
			@EmailAddress = @em_Address,
			@IsPreferred = 1,
			@EmailAddressID =  @emID OUTPUT
		IF @@ERROR <> 0 OR @emID = 0
			BEGIN
				EXECUTE GlobalUsers.dbo.upDeleteUserAccount @Result OUTPUT, @guID
				SET @u_Password = 'BBB'
        RETURN
			END
	END

-- Add additional e-mail addresses if necessary
IF LEN(LTRIM(RTRIM(@em_Address1))) > 0
	BEGIN
		EXECUTE GlobalUsers.dbo.upAddEmailAddress
			@UserAccountID = @guID,
			@EMailAddressType = 'W',
			@EmailAddress = @em_Address1,
			@IsPreferred = 0,
			@EmailAddressID =  @emID OUTPUT

		IF LEN(LTRIM(RTRIM(@em_Address2))) > 0
			EXECUTE GlobalUsers.dbo.upAddEmailAddress
				@UserAccountID = @guID,
				@EMailAddressType = 'W',
				@EmailAddress = @em_Address2,
				@IsPreferred = 0,
				@EmailAddressID =  @emID OUTPUT

	END

/* now insert the new record */
BEGIN TRANSACTION

INSERT INTO dbo.Users(
		u_ID,
		u_UserName,
		u_Password,
		u_Type,
		u_AltID,
		u_StartDate,
		u_RecState)
	VALUES(
		@guID,
		LOWER(@UserName),
		master.dbo.ud_CCEncrypt(@PW),
		'V',
		@gu_OldId,
		NULL,
		0)
	IF @@ROWCOUNT = 0 OR @@ERROR <> 0
		BEGIN
		SET @u_Password = 'ZZZ'
		EXECUTE GlobalUsers.dbo.upDeleteUserAccount @Result OUTPUT, @guID
		ROLLBACK TRANSACTION
	  RETURN
		END

/* Add the default Group */
EXECUTE AddUserToGroup 2, @guID

/* Now assign default home page content.*/
INSERT INTO dbo.HomeContent (
		hc_Table, 
		hc_uc_ID, 
		hc_u_ID, 
		hc_SortOrder)
	SELECT DISTINCT 
		hc.hc_Table, 
		hc.hc_uc_ID, 
		@guID, 
		hc.hc_SortOrder
	FROM dbo.HomeContent hc
	LEFT JOIN dbo.HomeContent hc1 ON hc.hc_uc_ID = hc1.hc_uc_ID AND hc1.hc_u_ID = @guID
	WHERE hc.hc_u_ID = 2
	AND hc1.hc_uc_ID IS NULL
	IF @@ROWCOUNT = 0 OR @@ERROR <> 0
		BEGIN
		SET @u_Password = 'AAA'
		EXECUTE GlobalUsers.dbo.upDeleteUserAccount @Result OUTPUT, @guID
		ROLLBACK TRANSACTION
	  RETURN
		END

SET @u_Password = @PW

COMMIT TRANSACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GRANT EXECUTE
ON dbo.AddVendorToPortal
TO portaluser