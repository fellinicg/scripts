USE [MadisonPortal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE dbo.UpdatePortalUser
(
@gu_ID		int,
@gu_FirstName	varchar(50),
@gu_MiddleName	varchar(50) = NULL,
@gu_LastName	varchar(100),
@gu_OldId	varchar(40) = '',
@gu_cl_ID	int = 0,
@u_Password	varchar(50)='',
@u_Type		char(1),
@u_StartDate	varchar(50) = '',
@u_RecState	int = 0,
@gu_rs_ID	int = 0,
@theReturn	varchar(200) = '' OUTPUT
)

AS

-- Local variable declarations
DECLARE @Result char(1)
DECLARE @IsPerson bit
DECLARE @AccountName varchar(80)
DECLARE @AccountStatus char(1)
DECLARE @PersonBuiltFullName varchar(200)

-- Initialize settings
SET @theReturn = ''
SET @Result = 0
SET @u_Type = UPPER(@u_Type)

IF @u_StartDate = '' SET @u_StartDate = NULL

IF LTRIM(RTRIM(@u_Password)) = '' SET @u_Password = (SELECT master.dbo.ud_CCDecrypt(u_Password)
					FROM dbo.Users WHERE u_ID = @gu_ID)

IF UPPER(@u_Type) = 'V' 
	BEGIN
		SET @IsPerson = 0
		SET @AccountName = LTRIM(RTRIM(@gu_FirstName)) + ' ' + LTRIM(RTRIM(@gu_LastName))
		SET @gu_FirstName = ''
		SET @gu_MiddleName = ''
		SET @gu_LastName = ''
		SET @PersonBuiltFullName = ''
	END
ELSE
	BEGIN
		SET @IsPerson = 1
		SET @PersonBuiltFullName = LTRIM(RTRIM(@gu_FirstName)) + ' ' + LTRIM(RTRIM(@gu_LastName))
		SET @AccountName = ''
	END

IF @u_Type = 'M' SET @gu_cl_ID = 5

SET @AccountStatus = CASE @gu_rs_ID WHEN 0 THEN 'E' WHEN 2 THEN 'X' ELSE 'D' END

/* update user in GlobalUser db */
EXECUTE  GlobalUsers.dbo.upUpdateUserAccount 
	@UserAccountID = @gu_ID,
	@UpdAccountType = 1,
	@AccountType = @u_Type,
	@UpdIsPerson = 1,
	@IsPerson = @IsPerson,
	@UpdClientID = 1,
	@ClientID = @gu_cl_ID,
	@UpdPersonFirstGivenName = 1,
	@PersonFirstGivenName = @gu_FirstName,
	@UpdPersonSecondGivenName = 1,
	@PersonSecondGivenName = @gu_MiddleName,
	@UpdPersonFamilyName = 1,
	@PersonFamilyName = @gu_LastName,
	@UpdPersonBuiltFullName = 1,
	@PersonBuiltFullName = @PersonBuiltFullName,
	@UpdAltAccountCode = 1,
	@AltAccountCode = @gu_OldId,
	@UpdAccountName = 1,
	@AccountName = @AccountName,
	@UpdAccountStatus = 1,
	@AccountStatus = @AccountStatus,
	@Result = @Result OUTPUT

IF (@@ERROR <> 0 OR @@ROWCOUNT = 0 OR @Result = 'F') RETURN

/* now update user in axis portal */
UPDATE dbo.Users
	SET u_Password = master.dbo.ud_CCEncrypt(@u_Password),
	u_Type = @u_Type,
	u_AltID = @gu_OldId,
	u_StartDate = @u_StartDate,
	u_RecState = @u_RecState
WHERE u_ID = @gu_ID

IF (@@ROWCOUNT = 0 OR @@ERROR <> 0) RETURN

SET @theReturn =  'SUCCESS'

GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
GRANT EXECUTE
ON dbo.UpdatePortalUser
TO portaluser