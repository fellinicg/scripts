USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE dbo.GetAllUserContacts
(
@gu_ID	int
)

AS

	SELECT 3 AS SORTER, EmailAddressID AS [Row], '3' AS ct_ID, EMailAddressType AS ct_Type,  Description AS ct_Desc
	FROM GlobalUsers.dbo.tbEmailAddress 
	LEFT JOIN GlobalUsers.dbo.ufEMailAddressTypes() ON EMailAddressType = Type
	WHERE UserAccountID = @gu_ID
	UNION
	SELECT 2, PhoneNumID, '2', PhoneNumType, Description
	FROM GlobalUsers.dbo.tbPhoneNum 
	LEFT JOIN GlobalUsers.dbo.ufPhoneNumTypes() ON PhoneNumType = Type
	WHERE UserAccountID = @gu_ID
	UNION
	SELECT 1, MailAddressID, '1', MailAddressType, Description
	FROM GlobalUsers.dbo.tbMailAddress 
	LEFT JOIN GlobalUsers.dbo.ufMailAddressTypes() ON MailAddressType = Type
	WHERE UserAccountID = @gu_ID
	UNION
	SELECT 4, EmergencyContactID, '4', 'E', 'Emergency'
	FROM GlobalUsers.dbo.tbEmergencyContact
	WHERE UserAccountID = @gu_ID

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.GetAllUserContacts
TO portaluser