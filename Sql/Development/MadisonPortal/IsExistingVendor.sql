USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE dbo.IsExistingVendor
(
@company varchar(100)
)

AS

SELECT Count(company)
FROM Catalog.dbo.vwVendors
WHERE company = @company
