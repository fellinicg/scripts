USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.RewardGetDepHeads

AS

DECLARE @DH int
SET @DH = 17

SELECT DISTINCT u_ID, dbo.ufGetPersonsLastFirstName(u_ID) AS [Name]
FROM dbo.Users
INNER JOIN dbo.UserGroups ON u_ID = ug_u_ID
WHERE ug_gr_ID = @DH
ORDER BY [Name]

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.RewardGetDepHeads
TO portaluser