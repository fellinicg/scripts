USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.trv_GetLatestModified 
(
@tm_ID int
)

AS

	SELECT tm_RecordDateTime AS LATESTDATE, dbo.ufGetPersonsFullName(tm_ModifiedBy) AS UserName
	FROM dbo.TravelMaster
	WHERE tm_ID = @tm_ID
	UNION
	SELECT TOP 1 RecordDateTime, dbo.ufGetPersonsFullName(ModifiedByID)
	FROM dbo.TravelDetail
	WHERE tm_ID = @tm_ID
	ORDER BY LATESTDATE DESC

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.trv_GetLatestModified
TO portaluser