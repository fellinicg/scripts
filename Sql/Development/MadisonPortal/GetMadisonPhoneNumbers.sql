USE [MadisonPortal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE dbo.GetMadisonPhoneNumbers
(
@First varchar(50) = '%',
@Last	varchar(100) = '%'
)

AS

	-- Add wildcard to end, if necessary
	IF (CHARINDEX('%', @First) <> LEN(@First) OR LEN(@First) = 0) SET @First = @First + '%'
	IF (CHARINDEX('%', @Last) <> LEN(@Last) OR LEN(@Last) = 0) SET @Last = @Last + '%'

	-- Get data
	SELECT 
		PersonFirstGivenName + ' ' + PersonFamilyName AS [Name], 
		PhoneNum AS [Number], 
		CASE WHEN Extension IS NULL THEN '' ELSE Extension END AS [Extension], 
		PhoneNumType AS [Phone Type] 
	FROM GlobalUsers.dbo.tbUserAccount ua
	INNER JOIN GlobalUsers.dbo.tbPhoneNum pn ON ua.UserAccountID = pn.UserAccountID AND pn.PhoneNumType = 'W'
	INNER JOIN dbo.Users ON ua.UserAccountID = u_ID
	WHERE PersonFirstGivenName LIKE @First
	AND PersonFamilyName LIKE @Last
	AND u_RecState = 0
	ORDER BY PersonFirstGivenName

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.GetMadisonPhoneNumbers
TO portaluser