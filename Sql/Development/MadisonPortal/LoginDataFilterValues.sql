USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.LoginDataFilterValues

AS

SELECT AccountType AS ut_SDesc, Description AS ut_Desc 
FROM GlobalUsers.dbo.tbAccountType
ORDER BY ut_Desc

SELECT DISTINCT UserAccountID AS gu_ID, dbo.ufGetPersonsLastFirstName(UserAccountID) AS [Name]
FROM GlobalUsers.dbo.tbUserAccount
INNER JOIN dbo.Users ON UserAccountID = u_ID
INNER JOIN dbo.AccessLog ON u_UserName = al_u_Username
ORDER BY [Name]

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.LoginDataFilterValues
TO portaluser