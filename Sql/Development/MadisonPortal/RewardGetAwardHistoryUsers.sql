USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.RewardGetAwardHistoryUsers

(
@u_ID int,
@p_PointsPeriod int
)

AS

--declare variables for the individual groups
DECLARE @APPROVER int
DECLARE @DH int
DECLARE @USER int
SET @APPROVER = 16
SET @DH = 17
SET @USER = 18


DECLARE @WHERE varchar(100)
SET @WHERE = ''

--in which group is the user?
--search top to bottom
DECLARE @GROUP int
	--approver
	SET @GROUP = (SELECT ug_gr_ID FROM dbo.UserGroups WHERE ug_u_ID = @u_ID AND ug_gr_ID = @APPROVER)
IF @GROUP IS NULL
	--not an approver
	SET @GROUP = (SELECT ug_gr_ID FROM dbo.UserGroups WHERE ug_u_ID = @u_ID AND ug_gr_ID = @DH)
IF @GROUP IS NULL
	--not a dh
	SET @GROUP = (SELECT ug_gr_ID FROM dbo.UserGroups WHERE ug_u_ID = @u_ID AND ug_gr_ID = @USER)
IF @GROUP IS NULL
	--does not have access at all
	RETURN

--if simple user, return itself
IF @GROUP = @USER
BEGIN
	IF @p_PointsPeriod > 0
		SET @WHERE = ' AND p_PointsPeriod = ' + CONVERT(varchar,@p_PointsPeriod)
	EXECUTE ('SELECT DISTINCT u_ID, dbo.ufGetPersonsLastFirstName(u_ID) AS [Name]
	FROM dbo.Users
	INNER JOIN Rewards.dbo.Points ON u_ID = p_UserID
	WHERE u_ID = ' + @u_ID + @WHERE + '')
	RETURN
END
--if is approver, return all users from users group
IF @GROUP = @APPROVER
BEGIN
	IF @p_PointsPeriod > 0
		SET @WHERE = ' WHERE p_PointsPeriod = ' + CONVERT(varchar,@p_PointsPeriod)
	EXECUTE ('SELECT DISTINCT u_ID, dbo.ufGetPersonsLastFirstName(u_ID) AS [Name]
	FROM dbo.Users
	INNER JOIN Rewards.dbo.Points ON u_ID = p_UserID ' +  @WHERE + '
	ORDER BY [Name]')
	RETURN
END
--if is dh, return users in the group for the department
IF @GROUP = @DH
BEGIN
	IF @p_PointsPeriod > 0
		SET @WHERE = ' p_PointsPeriod = ' + CONVERT(varchar,@p_PointsPeriod) + ' AND '
	EXECUTE ('SELECT DISTINCT u_ID, dbo.ufGetPersonsLastFirstName(u_ID) AS [Name]
	FROM dbo.Users
	INNER JOIN Rewards.dbo.Points ON u_ID = p_UserID
	WHERE ' + @WHERE + '
	u_ID IN (SELECT u_ID FROM dbo.tfRewardGetUsersInRYPDep(' + @u_ID + ',0))
	ORDER BY [Name]')
	RETURN
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.RewardGetAwardHistoryUsers
TO portaluser