USE [MadisonPortal]
GO
/****** Object:  StoredProcedure [dbo].[GetGlobalUserGenInfo]    Script Date: 06/14/2006 14:01:08 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE dbo.GetGlobalUserGenInfo
(
@gu_ID	int
)

AS

	SELECT 
		PersonFirstGivenName AS gu_FirstName, 
		PersonSecondGivenName AS gu_MiddleName, 
		PersonFamilyName AS gu_LastName, 
		CASE WHEN PersonGovtIDCodeTwoWayCrypted IS NULL THEN '' ELSE RIGHT(master.dbo.ud_CCDecrypt(PersonGovtIDCodeTwoWayCrypted),4) END AS gu_SSN,
		u_BirthDay, 
		u_Startdate 
	FROM GlobalUsers.dbo.tbUserAccount
	INNER JOIN dbo.Users ON UserAccountID = u_ID
	WHERE UserAccountID = @gu_ID

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.GetGlobalUserGenInfo
TO portaluser