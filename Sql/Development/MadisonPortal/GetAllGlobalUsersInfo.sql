USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.GetAllGlobalUsersInfo

AS

SELECT
	UserAccountID AS [ID],
	ISNULL(PersonFirstGivenName, '') AS [First Name],
	ISNULL(PersonSecondGivenName, '') AS [Middle Name], 
	ISNULL(PersonFamilyName, AccountName) AS [Last Name],
	ISNULL(AltAccountCode, 'NA') AS [Alt ID],
	ISNULL(cl_Desc, 'NA') AS [Client],
	CreateDateTime AS [Added],
	Description AS [Status]
FROM GlobalUsers.dbo.tbUserAccount ua
LEFT JOIN Catalog.dbo.Clients ON  ClientID= cl_ID
INNER JOIN GlobalUsers.dbo.tbAccountStatus astat ON ua.AccountStatus = astat.AccountStatus

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.GetAllGlobalUsersInfo
TO portaluser