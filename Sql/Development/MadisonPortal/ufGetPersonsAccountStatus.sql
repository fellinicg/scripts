USE [MadisonPortal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 19, 2005
-- Description:	Return AccountStatus name from GlobalUsers
-- =============================================
ALTER FUNCTION dbo.ufGetPersonsAccountStatus
(
	@UserAccountID int
)
RETURNS char(1)
AS
BEGIN
	DECLARE @Result char(1)
 
	SELECT @Result = AccountStatus
	FROM GlobalUsers.dbo.tbUserAccount
	WHERE UserAccountID = @UserAccountID

	RETURN @Result

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.ufGetPersonsAccountStatus
TO portaluser