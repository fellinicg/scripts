USE [MadisonPortal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.iv_GetInvoicesForMonthYear
(
@MonthReport int,
@YearReport int
)

AS

/*
the 20 is an indicator for how many rows should be returned per page, I know I am lazy and could have done this differently
*/

SELECT iv_ID, iv_Date, description company, iv_DK, iv_Amount, iv_Copy,  iv_DT, iv_Comments, PersonBuiltFullName AS [Name], 20
FROM dbo.Invoices
INNER JOIN GlobalUsers.dbo.tbUserAccount ON iv_User_ID = UserAccountID
INNER JOIN SQL2KPROD2.Agresso.dbo.atsproject ON CONVERT(varchar, iv_DK) = project
WHERE MONTH(iv_Date) = @MonthReport AND YEAR(iv_Date) = @YearReport
ORDER BY iv_Date ASC
