USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE dbo.cr_GetChecksForReviewByAdmin
(
@cr_u_ID 	INT,
@INDICATOR	varchar(50),
@VALUES	varchar(100)
)

AS
DECLARE @Where varchar(500)

SET @Where = CASE @INDICATOR WHEN 'All' THEN ''
				     WHEN '2Weeks' THEN 'WHERE cr_PostDate > ''' + convert(varchar,GETDATE() - 14,101) + ''''
				     WHEN 'Name' THEN 'WHERE gu_LastName LIKE ''' + @VALUES + '%'''
				     WHEN '#' THEN 'WHERE cr_ID = ' + @VALUES + ''
				     WHEN 'Acct' THEN 'WHERE cr_Acct_Approval = ' + @VALUES + ''
				     WHEN 'Corp' THEN 'WHERE cr_Corp_Approval = ' + @VALUES + ''
				     WHEN 'Void' THEN 'WHERE cr_Acct_Voided = ' + @VALUES + ''
				     ELSE '' END
EXECUTE ('SELECT cr_ID AS [Request #],
	dbo.ufGetPersonsFullName(cr_u_ID) AS [Name],
	CONVERT(VARCHAR,cr_PostDate,101) AS [Post Date], 
	CONVERT(VARCHAR,cr_PayDueDate,101) AS [Due Date],
	CONVERT(VARCHAR, cr_Amount, 1) AS [Amount], 
	cu_Desc AS Currency,
	cr_PayType AS [Type],
	cr_Acct_Approval AS [Acct Approve],
	cr_Corp_Approval AS [Corp Approve],
	cr_Acct_Voided AS [Void]
	FROM dbo.CheckRequests
	INNER JOIN dbo.Currencies ON cr_cu_ID = cu_ID '
	+ @Where + ' 
	ORDER BY cr_ID DESC')
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GRANT EXECUTE
ON dbo.cr_GetChecksForReviewByAdmin
TO portaluser

