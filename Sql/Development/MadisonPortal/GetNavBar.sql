USE [MadisonPortal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER   PROCEDURE dbo.GetNavBar @Userid INT, @Moduleid INT =0 AS

	SELECT DISTINCT
	m.m_ID, m.m_SDesc, m.m_Desc, m.m_mt_ID, m.m_Image, pp_Path, pp_Page, pp_Target, m.m_SortOrder, CASE m.m_ID WHEN @Moduleid THEN 1 ELSE 0 END AS 'CurrSel'
	FROM dbo.Modules m
	INNER JOIN dbo.Modules m1 ON m.m_ID = m1.m_Parent OR m.m_Parent = m1.m_ID OR m.m_ID = @Moduleid OR (m.m_Parent = m1.m_Parent AND m.m_mt_ID > 1)
	INNER JOIN dbo.GetUserModules(@Userid) ON m.m_ID = pga_m_ID
	LEFT JOIN dbo.PortalPages ON m.m_pp_ID = pp_ID
	WHERE m1.m_ID = @Moduleid
	--AND m.m_mt_ID BETWEEN 1 AND 3
	AND m.m_mt_ID IN (1,2)
	ORDER BY m.m_mt_ID, m.m_SortOrder

GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
GRANT EXECUTE
ON dbo.GetNavBar
TO portaluser