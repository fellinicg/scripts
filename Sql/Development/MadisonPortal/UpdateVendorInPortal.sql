USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.UpdateVendorInPortal
(
@gu_OldId varchar(50),
@gu_LastName varchar(100),
@em_Address varchar(200),
@em_Address1 varchar(200) = '',
@em_Address2 varchar(200) = '',
@RETURN varchar(50) OUTPUT
)

AS

-- Local variable declarations
DECLARE @Result char(1)
DECLARE @EmailAddressID int

-- First, get the gu_ID
DECLARE @gu_ID int
SET @gu_ID = (SELECT u_ID FROM dbo.Users WHERE u_AltID = @gu_OldId)
IF @gu_ID IS NULL
	BEGIN
		SET @RETURN = 'XYZ'
		RETURN
	END

-- Update the user last name
EXECUTE  GlobalUsers.dbo.upUpdateUserAccount 
	@UserAccountID = @gu_ID,
	@UpdAccountName = 1,
	@AccountName = @gu_LastName,
	@Result = @Result OUTPUT

IF (@@ERROR <> 0 OR @@ROWCOUNT = 0 OR @Result = 'F') 
	BEGIN
	SET @RETURN = 'XXX'
		RETURN
	END

-- Update the Email Data
--
-- First, clear all existing
EXECUTE GlobalUsers.dbo.upDeleteUsersEmailAddresses 
	@Result = @Result OUTPUT,
	@UserAccountID = @gu_ID,
	@EMailAddressType = 'W'

-- Next, add the new email(s)
EXECUTE GlobalUsers.dbo.upAddEmailAddress
	@UserAccountID = @gu_ID,
	@EMailAddressType = 'W',
	@EmailAddress = @em_Address,
	@IsPreferred = 1,
	@EmailAddressID =  @EmailAddressID OUTPUT
IF @@ERROR <> 0 OR @EmailAddressID = 0
	BEGIN
		SET @RETURN = 'YYY'
		RETURN
	END

-- Add additional e-mail addresses if necessary
IF LEN(LTRIM(RTRIM(@em_Address1))) > 0
	BEGIN
		EXECUTE GlobalUsers.dbo.upAddEmailAddress
			@UserAccountID = @gu_ID,
			@EMailAddressType = 'W',
			@EmailAddress = @em_Address1,
			@IsPreferred = 0,
			@EmailAddressID =  @EmailAddressID OUTPUT

		IF @@ERROR <> 0 OR @EmailAddressID = 0
			BEGIN
				SET @RETURN = 'ZZZ'
				RETURN
			END

		IF LEN(LTRIM(RTRIM(@em_Address2))) > 0
			EXECUTE GlobalUsers.dbo.upAddEmailAddress
				@UserAccountID = @gu_ID,
				@EMailAddressType = 'W',
				@EmailAddress = @em_Address2,
				@IsPreferred = 0,
				@EmailAddressID =  @EmailAddressID OUTPUT

		IF @@ERROR <> 0 OR @EmailAddressID = 0
			BEGIN
				SET @RETURN = 'ZZZ'
				RETURN
			END

	END

SET @RETURN = 'AAA'

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.AddVendorToPortal
TO portaluser