USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.LoginDataReport
(
@al_TimeStampSTART varchar(20),
@al_TimeStampEND varchar(20),
@al_Action int,
@u_ID int,
@u_Type varchar(3),
@al_Success int
)

AS

DECLARE @Where varchar(200)
SET @Where = ' WHERE al_Application = 0 '
IF @al_TimeStampSTART <> ''
	SET @Where = @Where + ' AND al_TimeStamp >= ''' + @al_TimeStampSTART + ' '' AND al_TimeStamp <= ''' + CONVERT(varchar, DATEADD(d,1,@al_TimeStampEND), 101) + ' '''
IF @al_Action = 1
	SET @Where = @Where + ' AND al_Action = ''Login attempt'''
IF @u_ID > 0
	SET @Where = @Where + ' AND ua.UserAccountID = ''' + CONVERT(varchar,@u_ID) + ''''
IF @u_Type <> ''
	SET @Where = @Where + ' AND at.AccountType = ''' + @u_Type + ''''
IF @al_Success <> 0
	SET @Where = @Where + ' AND al_Success = ''' + CONVERT(varchar,@al_Success) + ''''
print @Where

EXECUTE ('SELECT at.Description AS [Type],
		dbo.ufGetPersonsLastFirstName(u_ID) AS [Name],
		al_TimeStamp AS [Date],
		al_Action AS [Task],
		CASE WHEN al_Success=1 THEN ''Yes'' ELSE ''No'' END AS [Success]
		FROM dbo.AccessLog
		INNER JOIN dbo.Users ON al_u_Username = u_UserName
		INNER JOIN GlobalUsers.dbo.tbUserAccount ua ON u_ID = ua.UserAccountID
		INNER JOIN GlobalUsers.dbo.tbAccountType at ON ua.AccountType = at.AccountType' + @Where + '
		ORDER BY al_TimeStamp DESC')
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.LoginDataReport
TO portaluser