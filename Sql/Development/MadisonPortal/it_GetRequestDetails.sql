USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.it_GetRequestDetails
(
@hs_ID int
)

AS

SELECT PersonBuiltFullName AS [Name], hs_CreateDate, hs_Client, hs_Project
FROM dbo.HardwareSoftware
INNER JOIN GlobalUsers.dbo.tbUserAccount ON hs_u_ID = UserAccountID
WHERE hs_ID = @hs_ID

SELECT hsd_ID, hsd_Date, hsd_DK,  
	hsd_Product, hsd_Quantity, 
	CASE WHEN hsd_Billable = 0 THEN 'No' ELSE 'Yes' END AS hsd_Billable, 
	hsd_Descr, 
	hsd_Price_Admin, hsd_Comment_Admin, hsd_Approved
FROM dbo.HardwareSoftwareDetails
WHERE hsd_hs_ID = @hs_ID
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.it_GetRequestDetails
TO portaluser