USE [MadisonPortal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.GetCheckRequestPayeeInfo
@crpi_ID INT,
@GetVendID bit=0
AS

DECLARE @Identifier int
SET @Identifier=
(SELECT crpi_VendID FROM dbo.CheckRequestPayeeInfo WHERE crpi_ID = @crpi_ID)

IF @GetVendID = 1
	BEGIN
	SELECT @Identifier
	RETURN
	END

IF @Identifier IS NULL 
	BEGIN
		SELECT
		crpi_ID,
		crpi_Bank,
		crpi_Bank_Addr1,
		crpi_Bank_Addr2,
		crpi_Payee,
		crpi_Payee_Addr1,
		crpi_Payee_Addr2,
		crpi_PayTo_CSZ,
		crpi_PayeeAcctNo,
		crpi_ABA,
		'Man'

		FROM dbo.CheckRequestPayeeInfo
		WHERE crpi_ID = @crpi_ID
	END					
ELSE
	BEGIN
		SELECT
		crpi_ID,
		crpi_Bank,
		crpi_Bank_Addr1,
		crpi_Bank_Addr2,
		company,
		address1,
		address2,
		RTRIM(city) + ', ' + RTRIM(addrstate) + ' ' + RTRIM(zip),
		crpi_PayeeAcctNo,
		crpi_ABA,
		'Ven'
		FROM dbo.CheckRequestPayeeInfo
		INNER JOIN Catalog.dbo.vwVendors ON crpi_VendID = vendno
		WHERE crpi_ID = @crpi_ID
	END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
