USE [MadisonPortal]
GO
/****** Object:  View [dbo].[vwUsers]    Script Date: 06/13/2006 10:39:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW dbo.vwUsers
AS

	SELECT 
	u_ID, 
	u_UserName, 
	u_Type, 
	PersonFirstGivenName AS 'u_FirstName', 
	PersonFamilyName AS 'u_LastName', 
	EmailAddress AS 'em_Address', 
	u_AltID, 
	u_Added, 
	CASE AccountStatus WHEN 'E' THEN 0 WHEN 'X' THEN 2 ELSE 1 END AS 'gu_rs_ID'
	FROM dbo.Users u
	INNER JOIN GlobalUsers.dbo.tbUserAccount ua ON u.u_ID = ua.UserAccountID
	LEFT JOIN GlobalUsers.dbo.tbEmailAddress ea ON ua.UserAccountID = ea.UserAccountID AND ea.IsPreferred = 1

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT SELECT
ON dbo.vwUsers
TO portaluser