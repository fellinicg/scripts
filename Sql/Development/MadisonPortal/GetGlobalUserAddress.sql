USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE dbo.GetGlobalUserAddress
(
@ad_ID	int
)
AS

	SELECT 
		MailAddressType,
		Line1,
		Line2,
		Line3,
		Line4,
		CityTown,
		StateProvince,
		PostalCode,
		Country,
		IsPreferred
	FROM GlobalUsers.dbo.tbMailAddress
	WHERE MailAddressID = @ad_ID

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.GetGlobalUserAddress
TO portaluser