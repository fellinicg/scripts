USE [MadisonPortal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.GetPortalUser
(
@u_ID	int
)

AS

SELECT 
	PersonFirstGivenName AS gu_FirstName, 
	PersonSecondGivenName AS gu_MiddleName, 
	CASE IsPerson WHEN 0 THEN AccountName ELSE PersonFamilyName END AS gu_LastName, 
	AltAccountCode AS gu_OldId,
	CASE AccountStatus WHEN 'E' THEN 0 WHEN 'D' THEN 1 WHEN 'X' THEN 2 ELSE 99 END As gu_rs_ID,
	AccountType AS u_Type, 
	u_AltID, 
	u_StartDate, 
	u_RecState
FROM dbo.Users
INNER JOIN GlobalUsers.dbo.tbUserAccount ON u_ID = UserAccountID
WHERE u_ID = @u_ID

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.GetPortalUser
TO portaluser