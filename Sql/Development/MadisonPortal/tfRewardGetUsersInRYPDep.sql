USE [MadisonPortal]
GO
ALTER FUNCTION dbo.tfRewardGetUsersInRYPDep (@u_ID int, @ShowLocked int)  
RETURNS @theUsers TABLE (u_ID int NOT NULL, Name varchar(255) NOT NULL)
AS  
BEGIN
 
  IF @ShowLocked > 0
	BEGIN
  	INSERT @theUsers
  	SELECT u_ID, dbo.ufGetPersonsLastFirstName(u_ID)
  	FROM dbo.Users
    		INNER JOIN GlobalUsers.dbo.tbUserAccount ON (u_ID = UserAccountID)
    		INNER JOIN Rewards.dbo.DepartmentUsers ON (u_ID = depu_u_ID)
  	WHERE depu_dep_ID = (SELECT dpt_ID FROM Rewards.dbo.Departments WHERE dtp_DH_ID = @u_ID)
    		AND AccountStatus = 'E'
    		AND u_RecState = 0
    		AND depu_u_ID <> @u_ID
 	END
  ELSE
	BEGIN
	INSERT @theUsers
  	SELECT u_ID, dbo.ufGetPersonsLastFirstName(u_ID)
  	FROM dbo.Users
    		INNER JOIN Rewards.dbo.DepartmentUsers ON (u_ID = depu_u_ID)
  	WHERE depu_dep_ID = (SELECT dpt_ID FROM Rewards.dbo.Departments WHERE dtp_DH_ID = @u_ID)
    		AND depu_u_ID <> @u_ID
	END

  RETURN
 
END
GO
GRANT SELECT
ON dbo.tfRewardGetUsersInRYPDep
TO portaluser

