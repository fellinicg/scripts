USE [MadisonPortal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 19, 2005
-- Description:	Return last, first name from GlobalUsers
-- =============================================
ALTER FUNCTION dbo.ufGetPersonsLastFirstName 
(
	@UserAccountID int
)
RETURNS varchar(200)
AS
BEGIN
	DECLARE @Result varchar(200)
 
	SELECT @Result = ISNULL(PersonFamilyName + ', ' + PersonFirstGivenName, ISNULL(AccountName, ''))
	FROM GlobalUsers.dbo.tbUserAccount
	WHERE UserAccountID = @UserAccountID

	RETURN @Result

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.ufGetPersonsLastFirstName
TO portaluser