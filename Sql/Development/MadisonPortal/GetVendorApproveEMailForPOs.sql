USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE [dbo].[GetVendorApproveEMailForPOs]
(
@u_ID int,
@PONumber varchar(20),
@OUTPUT varchar(50)='' OUTPUT
)
AS

DECLARE @SENDER varchar(200)
DECLARE @RECEIVER varchar(200)
DECLARE @SUBJECT varchar(200)
DECLARE @BODY varchar(1000)
DECLARE @BCC varchar(200)

SET @OUTPUT = ''

/* First, get the real user id from the provided vendor id */
DECLARE @gu_ID int
SET @gu_ID = (
--SELECT u_ID FROM dbo.Users WHERE u_AltID = @u_ID
SELECT u_ID 
FROM dbo.Users 
INNER JOIN Catalog.dbo.VendorSendTo ON vst_vendno = @u_ID
WHERE u_AltID = vst_sendto
)
IF @gu_ID IS NULL
	BEGIN
	SET @OUTPUT = 'No User available'
	RETURN
	END

--get the sender
SET @SENDER = (SELECT pv_Value FROM dbo.PortalVariables WHERE pv_Desc = 'POAdminEmailAddress')
IF @SENDER IS NULL OR @SENDER = ''
	BEGIN
	SET @OUTPUT = 'No Sender available'
	RETURN
	END

--get the vendor e-mail address
SET @RECEIVER = (SELECT TOP 1 EmailAddress
									FROM GlobalUsers.dbo.tbEmailAddress
									WHERE UserAccountID = @gu_ID
									AND EMailAddressType = 'W'
									ORDER BY IsPreferred DESC)

IF @RECEIVER IS NULL or @RECEIVER = ''
	BEGIN
	SET @OUTPUT = 'No Vendor e-mail available'
	RETURN
	END

--set the subject
SET @SUBJECT = 'New Purchase Order, ' + @PONumber + ' available for download'

--set the body
SET @BODY = 'Dear Vendor, ' + CHAR(13) + CHAR(10)
SET @BODY = @BODY + CHAR(13) + CHAR(10)
SET @BODY = @BODY +'Purchase orders are now available to be downloaded. In order to access the PO Platform, please click on the link below or copy and paste onto your web browser.' + CHAR(13) + CHAR(10)
SET @BODY = @BODY + CHAR(13) + CHAR(10)
SET @BODY = @BODY + 'http://portal.madisonpg.com' + CHAR(13) + CHAR(10)
SET @BODY = @BODY + CHAR(13) + CHAR(10)
SET @BODY = @BODY + 'We ask that you please process the purchase order(s) immediately. If you have any questions, please contact Ava Milliner, Merchandising Administrator '
SET @BODY = @BODY + '212-339-2949 or e-mail amilliner@madisonpg.com. If you experience any technical issues while using this portal, send an e-mail to ischobert@madisonpg.com.' + CHAR(13) + CHAR(10)
SET @BODY = @BODY + CHAR(13) + CHAR(10)
SET @BODY = @BODY + 'Thank You, ' + CHAR(13) + CHAR(10)
SET @BODY = @BODY + CHAR(13) + CHAR(10)
SET @BODY = @BODY + 'Ava Milliner ' + CHAR(13) + CHAR(10)
SET @BODY = @BODY + CHAR(13) + CHAR(10)
SET @BODY = @BODY + 'Madison Performance Group'

--set the BCC
SET @BCC = (SELECT pv_Value FROM dbo.PortalVariables WHERE pv_Desc = 'POEmailBccAddress')
IF @BCC IS NULL OR @BCC = ''
	BEGIN
	SET @OUTPUT = 'No BCC available'
	END

--Set the output
SELECT @SENDER AS SENDER , EmailAddress AS RECEIVER, @SUBJECT AS SUBJECT, @BODY AS BODY, @BCC AS BCC
FROM GlobalUsers.dbo.tbEmailAddress
WHERE UserAccountID = @gu_ID
AND EMailAddressType = 'W'
ORDER BY IsPreferred DESC
