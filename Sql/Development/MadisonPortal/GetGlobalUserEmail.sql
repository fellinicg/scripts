USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE dbo.GetGlobalUserEmail
(
@em_ID	int
)

AS

	SELECT --em_ct_ID, em_Address, em_Default
		EMailAddressType,
		EmailAddress,
		IsPreferred
	FROM GlobalUsers.dbo.tbEmailAddress
	WHERE  EmailAddressID= @em_ID

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.GetGlobalUserEmail
TO portaluser