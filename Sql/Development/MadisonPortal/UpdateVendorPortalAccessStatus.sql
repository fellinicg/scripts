USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.UpdateVendorPortalAccessStatus
(
@u_RecState tinyint,
@u_AltID varchar(50),
@RETURN varchar(50) OUTPUT
)

AS

/* First, get the gu_ID */
DECLARE @gu_ID int
SET @gu_ID = (SELECT u_ID FROM dbo.Users WHERE u_AltID = @u_AltID)
IF @gu_ID IS NULL
	BEGIN
		SET @RETURN = 'XYZ'
		RETURN
	END

/* next, update status in global users */
DECLARE @UpdResult bit
DECLARE @OrgAccountStatus char(1)
DECLARE @AccountStatus char(1)
SET @OrgAccountStatus = dbo.ufGetPersonsAccountStatus(@gu_ID)
SET @AccountStatus = CASE @u_RecState WHEN 0 THEN 'E' WHEN 1 THEN 'D' WHEN 2 THEN 'X' END
EXECUTE GlobalUsers.dbo.upUpdateUserAccount
	@UpdResult = @UpdResult OUTPUT,
	@UserAccountID = @gu_ID,
	@UpdAccountStatus = 1,
	@AccountStatus = @AccountStatus

IF @@ROWCOUNT = 0 OR @@ERROR <> 0 OR @UpdResult <> 1
	BEGIN
		SET @RETURN = 'YYY'
		RETURN
	END

/*Next, update status in users */
UPDATE dbo.Users
	SET u_RecState = @u_RecState
	WHERE u_ID = @gu_ID
	IF @@ROWCOUNT = 0 OR @@ERROR <> 0
		BEGIN
			SET @RETURN = 'XXX'
			EXECUTE GlobalUsers.dbo.upUpdateUserAccount
				@UpdResult = @UpdResult OUTPUT,
				@UserAccountID = @gu_ID,
				@UpdAccountStatus = 1,
				@AccountStatus = @OrgAccountStatus
			RETURN
		END

SET @RETURN = 'AAA'

COMMIT TRANSACTION
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.UpdateVendorPortalAccessStatus
TO portaluser