USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.it_GetRequestEMailNotification
(
@hs_ID int,
@user varchar(200)
)

AS

DECLARE @Group int
DECLARE @BCC varchar(200)
DECLARE @BODY varchar(5000)
DECLARE @SUBJECT varchar(200)
DECLARE @SENDER varchar(400)

DECLARE @PROD varchar(4000)
DECLARE @CurrID int
DECLARE @COUNTER int
SET @PROD = ''
SET @COUNTER = 1

SELECT @CurrID = MIN(hsd_ID)
FROM dbo.HardwareSoftwareDetails
WHERE hsd_hs_ID = @hs_ID

WHILE ISNULL(@CurrID, 0) <>  0
	BEGIN		
		SET @PROD = @PROD +  CONVERT(VARCHAR,@COUNTER) + '. ' + 
				(SELECT hsd_Product FROM dbo.HardwareSoftwareDetails
				 WHERE hsd_ID = @CurrID) + CHAR(13) + CHAR(10)
		SELECT @CurrID = MIN(hsd_ID)
		FROM dbo.HardwareSoftwareDetails
		WHERE hsd_hs_ID = @hs_ID
		AND hsd_ID > @CurrID
		SET @COUNTER = @COUNTER + 1
	END


SET @SENDER = (SELECT TOP 1 EmailAddress 
								FROM dbo.HardwareSoftware
								INNER JOIN GlobalUsers.dbo.tbUserAccount ua ON hs_u_ID = ua.UserAccountID
								INNER JOIN GlobalUsers.dbo.tbEmailAddress ea ON ua.UserAccountID = ea.UserAccountID AND EMailAddressType = 'W'
								WHERE hs_ID = @hs_ID
								ORDER BY IsPreferred DESC)

SET @Group  = (SELECT pv_Value FROM dbo.PortalVariables WHERE pv_Desc = 'ITRequestGroupID')
SET @BCC  = (SELECT pv_Value FROM dbo.PortalVariables WHERE pv_Desc = 'ITRequestBCC')
SET @SUBJECT = 'New IT Requisition Received'

SET @BODY = 'Dear IT Leader, ' + CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10)
+ 'An IT Hardware / Software request has been received from ' 
+ @user + '.' + CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10)
+ 'Product Description(s):' + CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10)
+ @PROD
+ CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10)
+ 'Please go to http://portal.madisonpg.com and review the Request Number '
+ convert(varchar,@hs_ID) + '.'

SELECT @SENDER AS [SENDER], EmailAddress AS [RECEIVER], @SUBJECT AS [SUBJECT], @BODY AS [BODY], @BCC AS [BCC]
FROM GlobalUsers.dbo.tbEmailAddress
INNER JOIN dbo.UserGroups ON UserAccountID = ug_u_ID
INNER JOIN dbo.Users ON ug_u_ID = u_ID
WHERE ug_gr_ID = @Group
AND EMailAddressType = 'W'
AND u_RecState = 0

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.it_GetRequestEMailNotification
TO portaluser