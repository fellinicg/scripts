USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.RewardGetNominateAPeerUsers

(
@u_ID int
)

AS

--define which group is the PRY user group
DECLARE @USER int
SET @USER = 18

SELECT u_ID, dbo.ufGetPersonsLastFirstName(u_ID) AS [Name]
FROM dbo.Users
INNER JOIN GlobalUsers.dbo.tbUserAccount ON u_ID = UserAccountID
INNER JOIN dbo.UserGroups ON u_ID = ug_u_ID
WHERE ug_gr_ID = @USER
AND AccountStatus = 'E'
AND u_RecState = 0
AND u_ID NOT IN (SELECT u_ID FROM dbo.tfRewardGetUsersInRYPDep(@u_ID,1))
ORDER BY [Name]

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.RewardGetNominateAPeerUsers
TO portaluser