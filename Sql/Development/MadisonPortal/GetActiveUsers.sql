USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.GetActiveUsers

AS

	SELECT u_ID AS gu_ID, u_UserName AS [User]
	FROM dbo.Users
	INNER JOIN GlobalUsers.dbo.tbUserAccount ON u_ID = UserAccountID
	WHERE AccountStatus = 'E' AND u_RecState = 0
	ORDER BY u_UserName

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.GetActiveUsers
TO portaluser