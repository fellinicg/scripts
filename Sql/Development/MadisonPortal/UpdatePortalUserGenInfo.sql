USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.UpdatePortalUserGenInfo
(
@gu_ID		int,
@gu_FirstName		varchar(50),
@gu_MiddleName	varchar(50),
@gu_LastName		varchar(100),
@gu_SSN		varchar(9),
@u_BirthDay		varchar(5),
@u_Password		varchar(50),
@intReturn		int = 0 OUTPUT
)

AS

-- Local variable declarations
DECLARE @Result char(1)
DECLARE @UpdPersonGovtIDType bit
DECLARE @PersonGovtIDType char(1)
DECLARE @UpdPersonGovtIDCodeTwoWayCrypted bit
DECLARE @PersonGovtIDCodeTwoWayCrypted varchar(200)
DECLARE @PersonBuiltFullName varchar(200)

-- Initialize settings
IF @u_BirthDay = '' SET @u_BirthDay = NULL
IF @u_Password = '' SET @u_Password = NULL
SET @PersonBuiltFullName = LTRIM(RTRIM(@gu_FirstName)) + ' ' + LTRIM(RTRIM(@gu_LastName))

IF LEN(LTRIM(RTRIM(@gu_SSN))) > 0 
	BEGIN
		SET @UpdPersonGovtIDType = 1
		SET @UpdPersonGovtIDCodeTwoWayCrypted = 1
		SET @PersonGovtIDCodeTwoWayCrypted = master.dbo.ud_CCEncrypt(@gu_SSN)
		SET @PersonGovtIDType = 'S'
	END
ELSE 
	BEGIN
		SET @UpdPersonGovtIDType = 0
		SET @UpdPersonGovtIDCodeTwoWayCrypted = 0
		SET @PersonGovtIDCodeTwoWayCrypted = ''
		SET @PersonGovtIDType = ''
	END

/* update user in GlobalUser db */
EXECUTE  GlobalUsers.dbo.upUpdateUserAccount 
	@UserAccountID = @gu_ID,
	@UpdPersonFirstGivenName = 1,
	@PersonFirstGivenName = @gu_FirstName,
	@UpdPersonSecondGivenName = 1,
	@PersonSecondGivenName = @gu_MiddleName,
	@UpdPersonFamilyName = 1,
	@PersonFamilyName = @gu_LastName,
	@UpdPersonBuiltFullName = 1,
	@PersonBuiltFullName = @PersonBuiltFullName,
	@UpdPersonGovtIDType = @UpdPersonGovtIDType,
	@PersonGovtIDType = @PersonGovtIDType,
	@UpdPersonGovtIDCodeTwoWayCrypted = @UpdPersonGovtIDCodeTwoWayCrypted,
	@PersonGovtIDCodeTwoWayCrypted = @PersonGovtIDCodeTwoWayCrypted,
	@Result = @Result OUTPUT

IF (@@ERROR <> 0 OR @@ROWCOUNT = 0 OR @Result = 'F') RETURN

BEGIN TRANSACTION UpPortalUser

IF @u_BirthDay IS NOT NULL
	BEGIN
	UPDATE dbo.Users
	SET	u_BirthDay = @u_BirthDay
	WHERE u_ID = @gu_ID
	IF @@ROWCOUNT = 0 
		BEGIN
		SET @intReturn = 0
		ROLLBACK TRANSACTION UpPortalUser
		RETURN
		END
	END

IF @u_Password IS NOT NULL
	BEGIN
	UPDATE dbo.Users
	SET	u_Password = master.dbo.ud_CCEncrypt(@u_Password)
	WHERE u_ID = @gu_ID
	IF @@ROWCOUNT = 0 
		BEGIN
		SET @intReturn = 0
		ROLLBACK TRANSACTION UpPortalUser
		RETURN
		END
	END

SET @intReturn = 1

COMMIT TRANSACTION UpPortalUser
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.UpdatePortalUserGenInfo
TO portaluser