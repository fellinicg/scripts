USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE dbo.GetGlobalUserEmergencyContact 
(
@emc_ID	int
)

AS

	SELECT 
		'E',
		FullName,
		ISNULL(CityAreaCode, '') + ISNULL(PhoneNum, ''),
		Extension,
		RelationshipDesc
	FROM GlobalUsers.dbo.tbEmergencyContact
	WHERE EmergencyContactID = @emc_ID

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.GetGlobalUserEmergencyContact
TO portaluser