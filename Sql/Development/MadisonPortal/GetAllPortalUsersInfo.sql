USE [MadisonPortal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.GetAllPortalUsersInfo

AS

	SELECT 
		at.Description AS [Desc.], 
		u_UserName AS [UserName], 
		u_ID AS [ID], 
		CASE WHEN u_AltID IS NULL THEN 'NA' ELSE u_AltID END AS [Alt ID], 
		CASE WHEN u_BirthDay IS NULL THEN 'NA' ELSE u_BirthDay END AS [Birthday],
		CASE WHEN u_StartDate IS NULL THEN 'NA' ELSE convert(varchar,u_StartDate, 101) END AS [Start Date],
		CASE WHEN u_Added IS NULL THEN 'NA' ELSE convert(varchar,u_Added, 101) END AS [Added],
		astat.Description AS [Status]
	FROM dbo.Users
	INNER JOIN GlobalUsers.dbo.tbUserAccount ua ON u_ID = ua.UserAccountID
	INNER JOIN GlobalUsers.dbo.tbAccountStatus astat ON ua.AccountStatus = astat.AccountStatus
	INNER JOIN GlobalUsers.dbo.tbAccountType at ON ua.AccountType= at.AccountType
	ORDER BY astat.Description, u_UserName

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.GetAllPortalUsersInfo
TO portaluser