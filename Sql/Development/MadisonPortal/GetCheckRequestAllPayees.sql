USE [MadisonPortal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.GetCheckRequestAllPayees
(
@Limiter int
)
AS

IF @Limiter = 0
	BEGIN
		SELECT 
			vendno, company
		FROM OPENQUERY(SQL2KPROD2, 
			'SELECT ah.apar_id vendno, ah.apar_name company
			FROM SQL2KPROD2.Agresso.dbo.asuheader ah
			WHERE ah.apar_gr_id = ''S6''
				AND ah.client = ''MP''')
		ORDER BY company
	END
ELSE
	BEGIN
		SELECT 
			vendno, company
		FROM OPENQUERY(SQL2KPROD2, 
			'SELECT ah.apar_id vendno, ah.apar_name company
			FROM SQL2KPROD2.Agresso.dbo.asuheader ah
			WHERE ah.apar_gr_id = ''S6''
				AND ah.client = ''MP''')
		WHERE vendno NOT IN (SELECT CONVERT(varchar,crpi_VendID) FROM dbo.CheckRequestPayeeInfo WHERE crpi_VendID IS NOT NULL)
			ORDER BY company
	END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
