USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE dbo.PageAccess @Userid INT, @Page VARCHAR(500) AS

	SELECT COUNT(pp_ID) AS 'HasAccess'
	FROM dbo.PortalPages 
	INNER JOIN dbo.Modules ON pp_ID = m_pp_ID
	WHERE pp_Page LIKE '%' + @Page
	AND m_ID IN (
	SELECT * FROM  dbo.GetUserModules(@Userid)
	)

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.PageAccess
TO portaluser