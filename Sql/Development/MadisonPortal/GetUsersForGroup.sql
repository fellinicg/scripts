USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.GetUsersForGroup
(
@ug_gr_ID	int
)
AS


	SELECT u_ID, u_UserName AS [User], 'In' AS InOut
	FROM dbo.Users
	INNER JOIN dbo.UserGroups ON u_ID = ug_u_ID
	INNER JOIN GlobalUsers.dbo.tbUserAccount ON u_ID = UserAccountID
	WHERE AccountStatus = 'E' AND u_RecState = 0
	AND ug_gr_ID = @ug_gr_ID
	UNION
	SELECT u_ID, u_UserName AS [User], 'Out' AS InOut
	FROM dbo.Users
	INNER JOIN GlobalUsers.dbo.tbUserAccount ON u_ID = UserAccountID
	WHERE AccountStatus = 'E' AND u_RecState = 0
	AND u_ID NOT IN (SELECT u_ID FROM dbo.Users
	INNER JOIN dbo.UserGroups ON u_ID = ug_u_ID
	INNER JOIN GlobalUsers.dbo.tbUserAccount ON u_ID = UserAccountID
	WHERE AccountStatus = 'E' AND u_RecState = 0
	AND ug_gr_ID = @ug_gr_ID AND u_ID = UserAccountID)
	ORDER BY InOut, u_UserName

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.GetUsersForGroup
TO portaluser