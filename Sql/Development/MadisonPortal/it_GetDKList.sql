USE [MadisonPortal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE dbo.it_GetDKList AS

	SELECT project AS 'DK_Desc'
	FROM SQL2KPROD2.Agresso.dbo.atsproject
	WHERE status = 'N'
		AND invoice_code = 'STD'
	ORDER BY project
