USE [MadisonPortal]
GO
SET ANSI_NULLS on
GO
SET QUOTED_IDENTIFIER on
GO
ALTER PROCEDURE dbo.cr_GetDKList AS

	SELECT project + ' ' + description AS 'DK_Desc'
	FROM SQL2KPROD2.Agresso.dbo.atsproject
	WHERE status = 'N'
		AND invoice_code = 'STD'
	ORDER BY project + ' ' + description
