USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.cr_GetCheckRequest @cr_ID INT AS

SELECT
CONVERT(VARCHAR, cr_PostDate, 101) AS 'cr_PostDate',
dbo.ufGetPersonsFullName(cr_u_ID) AS 'RequestedBy',
cr_PayType,
CONVERT(VARCHAR, cr_Amount, 1) + ' ' + cu_Desc AS 'Amount',
cr_InvoiceNo,
DATENAME(mm, cr_PayDueDate) +  ' ' + DATENAME(d, cr_PayDueDate)  + ', '  +  DATENAME(yyyy, cr_PayDueDate) AS 'cr_PayDueDate',
cr_PayOption,
cr_PayTo,
cr_PayTo_Addr1,
cr_PayTo_Addr2,
cr_PayTo_CSZ,
cr_PayTo_Country,
cr_Bank,
cr_Bank_Addr1,
cr_Bank_Addr2,
cr_ABA,
cr_PayeeAcctNo,
cr_Reference,
cr_Notes,
cr_Vendor,
cr_Amount_USD,
cr_Created_Type,
cr_VendID
FROM dbo.CheckRequests
INNER JOIN dbo.Currencies ON cr_cu_ID = cu_ID
WHERE cr_ID = @cr_ID

SELECT
crd_ProgramInfo,
crd_SBT_Code,
CONVERT(VARCHAR, crd_Value, 1) AS 'crd_Value',
crd_Percentage
FROM dbo.CheckRequestDK
WHERE crd_cr_ID = @cr_ID
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.cr_GetCheckRequest
TO portaluser