USE [MadisonPortal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER VIEW dbo.vwVendors
AS

	SELECT 
		vendno, company
	FROM OPENQUERY(SQL2KPROD2, 
		'SELECT ah.apar_id vendno, ah.apar_name company
		FROM SQL2KPROD2.Agresso.dbo.asuheader ah
		WHERE ah.apar_gr_id = ''S6''
			AND ah.client = ''MP''')

