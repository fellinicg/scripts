USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE dbo.ForgotMyPassword
(
@u_UserName	varchar(50),
@em_Address	varchar(200),
@RETURN varchar(2000)  OUTPUT
)

AS
DECLARE @Valid int
DECLARE @ID int
DECLARE @RETURNTEMP varchar(50)
DECLARE @Failure varchar(50)
DECLARE @UserType varchar(1)

SET @RETURNTEMP = ''
SET @Valid = -1
SET @ID = -1
SET @Failure = '\n\nInvalid Username or E-Mail.'

--check if valid user
SET @ID = (SELECT u_ID 
FROM dbo.Users
WHERE u_UserName = @u_UserName)
IF @ID IS NULL OR @ID = -1
	BEGIN
	SET @RETURN = @Failure
	RETURN
	END

--check if valid default e-mail
SELECT u_ID 
FROM dbo.Users
INNER JOIN GlobalUsers.dbo.tbEmailAddress ON u_ID = UserAccountID
WHERE u_UserName = @u_UserName
AND EmailAddress = @em_Address
AND IsPreferred = 1
IF @@ROWCOUNT = 0 OR @@ERROR <> 0
	BEGIN
	SET @RETURN = @Failure
	RETURN
	END

--check if valid global user
SELECT UserAccountID
FROM GlobalUsers.dbo.tbUserAccount
WHERE AccountStatus = 'E'
AND UserAccountID = @ID
IF @@ROWCOUNT = 0 OR @@ERROR <> 0
	BEGIN
	SET @RETURN = @Failure
	RETURN
	END

--check if valid portal user
SELECT u_ID, u_Type
FROM dbo.Users
WHERE u_RecState = 0
AND u_ID = @ID
IF @@ROWCOUNT = 0 OR @@ERROR <> 0
	BEGIN
	SET @RETURN = @Failure
	RETURN
	END

--if we make it this far, then the user 
--is authenticated and authorized

--test if user is Internal, if so change the password
SET @UserType = (SELECT u_Type
			FROM dbo.Users
			WHERE u_ID = @ID)

IF @UserType = 'I'
	BEGIN
		--construct the new password
		SET @RETURNTEMP = CONVERT(VARCHAR,DATEPART(MINUTE,GETDATE())) 
		+ UPPER(LEFT(@u_UserName,2)) 
		+ CONVERT(VARCHAR,DATEPART(SECOND,GETDATE()))
		+ 'TP'
	
		--Now, set the new PW in the db
		UPDATE dbo.Users
		SET u_Password = master.dbo.ud_CCEncrypt(@RETURNTEMP)
		WHERE u_ID = @ID
		IF @@ROWCOUNT = 0 OR @@ERROR <> 0
			BEGIN
				SET @RETURN = 'ERROR DURING RESET'
				RETURN
			END
	END
ELSE
	BEGIN
		--return the existing password
		SET @RETURNTEMP =  (SELECT master.dbo.ud_CCDecrypt(u_Password) 
					FROM dbo.Users 
					WHERE u_ID = @ID)
		IF @RETURNTEMP = ''
			BEGIN
				SET @RETURN = 'ERROR DURING PASSWORD ACCESS'
				RETURN
			END 
	END


--Is a valid request, define and return the fields
DECLARE @THESENDER varchar(100)
DECLARE @THEBCC varchar(100)
DECLARE @THESUBJECT varchar(100)
DECLARE @THEBODY varchar(1500)
DECLARE @PORTALADMIN varchar(100)
DECLARE @SingleCRLF char(2)
DECLARE @DoubleCRLF char(4)

SET @THESENDER = (SELECT pv_Value FROM MadisonPortal.dbo.PortalVariables WHERE pv_Desc = 'PortalAdminEmailAddress')
SET @THEBCC = (SELECT pv_Value FROM MadisonPortal.dbo.PortalVariables WHERE pv_Desc = 'PortalEmailBccAddress')
SET @THESUBJECT = (SELECT pv_Value FROM MadisonPortal.dbo.PortalVariables WHERE pv_Desc = 'PortalPasswordSubjectLine')
SET @PORTALADMIN = (SELECT pv_Value FROM MadisonPortal.dbo.PortalVariables WHERE pv_Desc = 'PortalAdminName')

-- set vars
SET @SingleCRLF = char(13) + char(10)
SET @DoubleCRLF = char(13) + char(10) + char(13) + char(10)

IF @UserType = 'I'
	BEGIN
	SET @THEBODY = 'At your request, the password to access the Madison Portal has been reset.' + @SingleCRLF
	 + 'Your new password is: ' + @DoubleCRLF
	 + @RETURNTEMP  + @DoubleCRLF 
	 + 'Please remember to change your password once you have successfully logged into the portal by accessing My Desktop and selecting My Profile.'  + @SingleCRLF
	 + 'Please delete this e-mail as soon as possible to avoid misuse of your account.'  + @DoubleCRLF 
	 + @PORTALADMIN  + @SingleCRLF 
	 + 'Madison Performance Group'   + @SingleCRLF 
	 + 'Madison Portal Administrator'
	END
ELSE
	BEGIN
	SET @THEBODY = 'You requested to have your Madison Portal password sent to you.' + @SingleCRLF
	 + 'Your password is: ' + @DoubleCRLF  
	 + @RETURNTEMP  + @DoubleCRLF
	 + 'Please delete this e-mail as soon as possible to avoid misuse of your account.'  + @DoubleCRLF   
	 + @PORTALADMIN  + @SingleCRLF 
	 + 'Madison Performance Group'   + @SingleCRLF
	 + 'Madison Portal Administrator'
	END 

SET @RETURN = 'VALID' + @em_Address + '|||' + @THESENDER + '|||' + @THEBCC + '|||' + @THESUBJECT + '|||' + @THEBODY
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GRANT EXECUTE
ON dbo.ForgotMyPassword
TO portaluser