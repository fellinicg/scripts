USE [MadisonPortal]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.GetUsersForModule
(
@pua_m_ID	int
)
AS

	SELECT pua_u_ID, u_UserName AS [User], 'In' AS InOut
	FROM dbo.PortalUserAccess
	INNER JOIN dbo.Users ON pua_u_ID = u_ID
	INNER JOIN GlobalUsers.dbo.tbUserAccount ON u_ID = UserAccountID
	WHERE pua_m_ID = @pua_m_ID
	AND u_RecState = 0 AND AccountStatus = 'E'
	UNION
	SELECT u_ID, u_UserName AS [User], 'Out' AS InOut
	FROM dbo.Users
	INNER JOIN GlobalUsers.dbo.tbUserAccount ON u_ID = UserAccountID
	WHERE u_ID NOT IN (SELECT pua_u_ID FROM dbo.PortalUserAccess
	INNER JOIN dbo.Users ON pua_u_ID = u_ID
	WHERE pua_m_ID = @pua_m_ID AND pua_u_ID = u_ID)
	AND u_RecState = 0 AND AccountStatus = 'E'

ORDER BY InOut, u_UserName
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.GetUsersForModule
TO portaluser