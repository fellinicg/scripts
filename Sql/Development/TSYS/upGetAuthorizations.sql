USE [TSYS]
GO
/****** Object:  StoredProcedure [dbo].[upGetAuthorizations]    Script Date: 03/24/2006 10:00:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:			Todd Fellini
-- Create date: March 16, 2006
-- Description:	Download RYP transactions
-- =============================================
ALTER PROCEDURE dbo.upGetAuthorizations 
	@BeginDate DATETIME,			  -- Beginning of date range
	@EndDate DATETIME,					-- End of date range
	@SystemID VARCHAR (50),			-- SystemID for TSYS
	@SecretKey VARCHAR (50),		-- SecretKey for TSYS
	@UserID BIGINT,							-- UserId for TSYS
	@InterfaceVersion BIGINT,		-- InterfaceVersion for TSYS
	@APIUrl VARCHAR (50),				-- APIUrl for TSYS
	@ProgramID BIGINT,					-- ProgramID for TSYS
	@ShowResults BIT = 0				-- Show report of results?

AS
BEGIN

	SET NOCOUNT ON

	DECLARE @Object int
	DECLARE @Hresult int
	DECLARE @Error VARCHAR(8000)
	DECLARE @ErrorSource varchar (255)
	DECLARE @ErrorDesc varchar (255)
	DECLARE @Result varchar (8000)
	DECLARE @NumberOfAuths INT
	DECLARE @Counter INT
	DECLARE @RowCt INT

	--Create the object
	EXEC @Hresult = sp_OACreate 'TSYS.cInterface', @Object OUT 

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upGetAuthorizations - error occured while creating object: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set SystemID Property
	EXEC @Hresult = sp_OASetProperty @Object, 'SystemID', @SystemID

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upGetAuthorizations - error occured while setting SystemID: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set SecretKey Property
	EXEC @Hresult = sp_OASetProperty @Object, 'SecretKey', @SecretKey

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upGetAuthorizations - error occured while setting SecretKey: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set UserID Property
	EXEC @Hresult = sp_OASetProperty @Object, 'UserID', @UserID

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upGetAuthorizations - error occured while setting UserID: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set InterfaceVersion Property
	EXEC @Hresult = sp_OASetProperty @Object, 'InterfaceVersion', @InterfaceVersion

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upGetAuthorizations - error occured while setting InterfaceVersion: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set APIUrl Property
	EXEC @Hresult = sp_OASetProperty @Object, 'APIUrl', @APIUrl

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upGetAuthorizations - error occured while setting APIUrl: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set ProgramID Property
	EXEC @Hresult = sp_OASetProperty @Object, 'ProgramID', @ProgramID

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upGetAuthorizations - error occured while setting ProgramID: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Download authorizations
	EXEC @Hresult = sp_OAMethod @Object, 'GetAuthorizations', @Result OUT, @BeginDate, @EndDate

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upGetAuthorizations - error occured while downloading authorizations: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	IF @Result = 'SUCCESS'
		BEGIN

			--Get number of authorizations
			EXEC @Hresult = sp_OAGetProperty @Object, 'ReturnInfoCount', @NumberOfAuths OUT

			--Error check
			IF @Hresult <> 0
			BEGIN
				EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
				SET @Error = 'upGetAuthorizations - error occured while downloading number of authorizations: ' + @ErrorSource + ' ' + @ErrorDesc
				RAISERROR(@Error, 16, 1)
				GOTO CleanUp
			END

			--Create temporary table for transactions
			CREATE TABLE #authorizations (
				InternalKey BIGINT,
				LedgBal MONEY,
				AvailBal MONEY,
				TranAmount MONEY,
				MerchDescr VARCHAR(100),
				TranID BIGINT,
				AuthTime DATETIME,
				ResponseCode INT,
				AuthCode INT,
				MCC INT)

			--Loop and get all transactions
			SET @Counter = 1
			WHILE @Counter <= @NumberOfAuths
				BEGIN

					--Get transaction
					EXEC @Hresult = sp_OAGetProperty @Object, 'GetReturnInfo', @Result OUT, @Counter

					--Error check
					IF @Hresult <> 0
					BEGIN
						EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
						SET @Error = 'upGetAuthorizations - error occured while downloading an authorization: ' + @ErrorSource + ' ' + @ErrorDesc
						RAISERROR(@Error, 16, 1)
						GOTO CleanUp
					END

					--Put transaction into temporary table
					INSERT INTO #authorizations
					SELECT * FROM dbo.ufParseAuthorization(@Result)

					--Increment counter
					SET @Counter = @Counter + 1
	
				END

			--Insert transactions into Statements table
			INSERT INTO GOLDFINGER.ClarityCards.dbo.Authorizations (au_InternalKey, au_TranId, au_MCC, au_GenerateDate)
			SELECT InternalKey, TranID, MCC, AuthTime
			FROM #authorizations 
			LEFT JOIN GOLDFINGER.ClarityCards.dbo.Authorizations ON TranID = au_TranID AND InternalKey = au_InternalKey
			WHERE au_TranID IS NULL

			--Preserve row count
			SET @RowCt = @@ROWCOUNT

			--Show results, if necessary
			IF @ShowResults = 1 SELECT @ProgramID 'Program ID', @SystemID 'System ID', @NumberOfAuths '# Downloaded Auths', @RowCt '# Inserted Auths'

			--Drop temporary table
			DROP TABLE #authorizations

		END

CleanUp:

	--Destroy the object 
	EXEC @Hresult = sp_OADestroy @Object

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF