USE [TSYS]
GO
/****** Object:  StoredProcedure [dbo].[upRYPBalanceAdjustment]    Script Date: 03/24/2006 10:00:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:			Todd Fellini
-- Create date: March 17, 2006
-- Description:	Adjust users RYP balance
-- =============================================
CREATE PROCEDURE dbo.upRYPBalanceAdjustment
AS
BEGIN

	SET NOCOUNT ON

	DECLARE	@SystemID VARCHAR (50)			-- SystemID for TSYS
	DECLARE	@SecretKey VARCHAR (50)			-- SecretKey for TSYS
	DECLARE	@UserID BIGINT							-- UserId for TSYS
	DECLARE	@InterfaceVersion BIGINT		-- InterfaceVersion for TSYS
	DECLARE	@APIUrl VARCHAR (50)				-- APIUrl for TSYS
	DECLARE	@ProgramID BIGINT						-- ProgramID for TSYS
	DECLARE	@InternalKey BIGINT					-- Internalkey field
	DECLARE	@Amount INT									-- Signed amount to adjust in whole dollars
	DECLARE	@Description VARCHAR(50)		-- Description
	DECLARE	@PointID INT								-- Point ID
	DECLARE	@TransactionID BIGINT				-- TSYS Transaction ID

	--Get system parameters
	SELECT 
		@SystemID = p_SystemID, 
		@SecretKey = p_SecretKey, 
		@UserID = p_UserID, 
		@InterfaceVersion = p_API_IterfaceVers, 
		@APIUrl = p_API_Url, 
		@ProgramID = p_ProgID
	FROM SQL2KPROD.ClarityCards.dbo.Programs

	--Insert awards to be processed into a temporary table
	SELECT p_ID, ac_internalKey, p_Points, p_Category
	INTO #temp
	FROM SQL2KPROD.Rewards.dbo.Points
	INNER JOIN SQL2KPROD.ClarityCards.dbo.Users ON p_UserID = u_MpgProgUserID AND u_MPGProgCode = 'RYP'
	INNER JOIN SQL2KPROD.ClarityCards.dbo.ProgramUser ON u_ExternalKey = pu_Externalkey
	INNER JOIN SQL2KPROD.ClarityCards.dbo.Accounts ON pu_PUID = ac_PUID
	WHERE p_Tranid = 0 
	AND p_Status = 0 
	AND p_Description = 'CASH'
	AND ac_Status = 'ACCOUNT_OPEN'

	--Loop over table and process
	WHILE EXISTS (SELECT 1 FROM #temp)
		BEGIN
			--Store data in local variables
			SELECT TOP 1 
				@PointID = p_ID, 
				@InternalKey = ac_internalKey,
				@Amount = p_Points,
				@Description = ISNULL(p_Category, '')
			FROM #temp 

			--Adjust balance at TSYS
			EXECUTE dbo.upBalanceAdjustment 
				@InternalKey,
				@Amount,
				@Description,
				@SystemID,
				@SecretKey,
				@UserID,
				@InterfaceVersion,
				@APIUrl,
				@ProgramID,
				@TransactionID OUT

			--Update tables
			IF ISNUMERIC(@TransactionID) = 1
				BEGIN
					--Update transaction id in Points table in Rewards
					UPDATE SQL2KPROD.Rewards.dbo.Points SET p_Tranid = @TransactionID 
					WHERE p_ID = @PointID

					--Add record to Adjustments table in ClarityCards
					INSERT INTO SQL2KPROD.ClarityCards.dbo.Adjustments (a_InternalKey, a_TransID, a_Amount, a_TranDescription)
					SELECT ac_internalKey, p_tranid, p_points, p_category
					FROM SQL2KPROD.Rewards.dbo.Points
					INNER JOIN SQL2KPROD.ClarityCards.dbo.Users ON p_UserID = u_MpgProgUserID AND u_MPGProgCode = 'RYP'
					INNER JOIN SQL2KPROD.ClarityCards.dbo.ProgramUser ON u_ExternalKey = pu_Externalkey
					INNER JOIN SQL2KPROD.ClarityCards.dbo.Accounts ON pu_PUID = ac_PUID
					WHERE p_ID = @PointID
				END

			--Remove processed record from temporary table
			DELETE FROM #temp WHERE p_ID = @PointID
		END

	--Drop temporary table
	DROP TABLE #temp

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF