USE [TSYS]
GO
/****** Object:  StoredProcedure [dbo].[upBalanceAdjustment]    Script Date: 03/24/2006 09:59:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:			Todd Fellini
-- Create date: March 17, 2006
-- Description:	Adjust users debit card balance
-- =============================================
CREATE PROCEDURE dbo.upBalanceAdjustment
	@InternalKey BIGINT,				-- Internalkey field
	@Amount INT,								-- Signed amount to adjust in whole dollars
	@Description VARCHAR(50),		-- Description
	@SystemID VARCHAR (50),			-- SystemID for TSYS
	@SecretKey VARCHAR (50),		-- SecretKey for TSYS
	@UserID BIGINT,							-- UserId for TSYS
	@InterfaceVersion BIGINT,		-- InterfaceVersion for TSYS
	@APIUrl VARCHAR (50),				-- APIUrl for TSYS
	@ProgramID BIGINT,					-- ProgramID for TSYS
	@TransactionID BIGINT OUT
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @Object INT
	DECLARE @Hresult INT
	DECLARE @Error VARCHAR(8000)
	DECLARE @ErrorSource VARCHAR (255)
	DECLARE @ErrorDesc VARCHAR (255)
	DECLARE @Result VARCHAR (8000)
	DECLARE @ResponseCode VARCHAR (200)

	--Initialize variables
	SET @TransactionID = 0

	--Create the object
	EXEC @Hresult = sp_OACreate 'TSYS.cInterface', @Object OUT 

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upBalanceAdjustment - error occured while creating object: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set SystemID Property
	EXEC @Hresult = sp_OASetProperty @Object, 'SystemID', @SystemID

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upBalanceAdjustment - error occured while setting SystemID: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set SecretKey Property
	EXEC @Hresult = sp_OASetProperty @Object, 'SecretKey', @SecretKey

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upBalanceAdjustment - error occured while setting SecretKey: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set UserID Property
	EXEC @Hresult = sp_OASetProperty @Object, 'UserID', @UserID

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upBalanceAdjustment - error occured while setting UserID: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set InterfaceVersion Property
	EXEC @Hresult = sp_OASetProperty @Object, 'InterfaceVersion', @InterfaceVersion

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upBalanceAdjustment - error occured while setting InterfaceVersion: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set APIUrl Property
	EXEC @Hresult = sp_OASetProperty @Object, 'APIUrl', @APIUrl

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upBalanceAdjustment - error occured while setting APIUrl: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set ProgramID Property
	EXEC @Hresult = sp_OASetProperty @Object, 'ProgramID', @ProgramID

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upBalanceAdjustment - error occured while setting ProgramID: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Adjust balance
	EXEC @Hresult = sp_OAMethod @Object, 'BalanceAdjustment', @Result OUT, @InternalKey, @Amount, @Description

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upBalanceAdjustment - error occured while adjusting balance: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Get Response Code
	EXEC @Hresult = sp_OAGetProperty @Object, 'ResponseCode', @ResponseCode OUT

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upBalanceAdjustment - error occured while getting ResponseCode: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Check ResponseCode for success
	IF @ResponseCode = 'SUCCESS' SET @TransactionID = SUBSTRING(@Result,  1, CHARINDEX('||', @Result) - 1)

CleanUp:

	--Destroy the object 
	EXEC @Hresult = sp_OADestroy @Object

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF