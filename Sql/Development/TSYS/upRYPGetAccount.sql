USE [TSYS]
GO
/****** Object:  StoredProcedure [dbo].[upRYPGetAccount]    Script Date: 03/24/2006 10:01:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:			Todd Fellini
-- Create date: March 20, 2006
-- Description:	Adjust users RYP balance
-- =============================================
CREATE PROCEDURE dbo.upRYPGetAccount
	@MPGUserID INT
AS
BEGIN

	SET NOCOUNT ON

	DECLARE	@SystemID VARCHAR (50)			-- SystemID for TSYS
	DECLARE	@SecretKey VARCHAR (50)			-- SecretKey for TSYS
	DECLARE	@UserID BIGINT							-- UserId for TSYS
	DECLARE	@InterfaceVersion BIGINT		-- InterfaceVersion for TSYS
	DECLARE	@APIUrl VARCHAR (50)				-- APIUrl for TSYS
	DECLARE	@ProgramID BIGINT						-- ProgramID for TSYS
	DECLARE	@ExternalKey BIGINT					-- Internalkey field

	--Get system parameters
	SELECT 
		@SystemID = p_SystemID, 
		@SecretKey = p_SecretKey, 
		@UserID = p_UserID, 
		@InterfaceVersion = p_API_IterfaceVers, 
		@APIUrl = p_API_Url, 
		@ProgramID = p_ProgID
	FROM SQL2KPROD.ClarityCards.dbo.Programs

	--Get external key
	SELECT @ExternalKey = u_ExternalKey
	FROM dbo.ufGetTSYSUserKeys(@MPGUserID)

	--Get account from TSYS
	EXECUTE dbo.upGetAccount 
		@ExternalKey,
		@SystemID,
		@SecretKey,
		@UserID,
		@InterfaceVersion,
		@APIUrl,
		@ProgramID

END


GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF