USE [master]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION dbo.ufEmailAddressIsValid (@EmailAddress varchar(120))
RETURNS bit
AS
BEGIN
DECLARE
  @IsValid bit
  
  IF (
     -- No embedded spaces
     CHARINDEX(' ',LTRIM(RTRIM(@EmailAddress))) = 0
     -- '@' can't be the first character of an email address
     AND LEFT(LTRIM(@EmailAddress),1) <> '@'
     -- '.' can't be the last character of an email address
     AND RIGHT(RTRIM(@EmailAddress),1) <> '.'
     -- There must be a '.' after '@'
     AND CHARINDEX('.',@EmailAddress,CHARINDEX('@',@EmailAddress)) - CHARINDEX('@',@EmailAddress) > 1
     -- Only one '@' sign is allowed
     AND LEN(LTRIM(RTRIM(@EmailAddress))) - LEN(REPLACE(LTRIM(RTRIM(@EmailAddress)),'@','')) = 1
     -- Domain name should end with at least 2 character extension
     AND CHARINDEX('.',REVERSE(LTRIM(RTRIM(@EmailAddress)))) >= 3
     -- can't have patterns like '.@' and '..'
     AND (CHARINDEX('.@',@EmailAddress) = 0 AND CHARINDEX('..',@EmailAddress) = 0)
     )
    SET @IsValid = 1
  ELSE
    SET @IsValid = 0

  RETURN(@IsValid)

END
GO
GRANT EXECUTE
ON dbo.ufEmailAddressIsValid
TO sys_email