USE [master]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060824
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upOSSMTPSendEmail 
    @From varchar(120),
    @From_Name varchar(200) = '',
    @ReplyTo varchar(500) = '',
    @To varchar(8000),
    @CC varchar(8000) = '',
    @BCC varchar(8000) = '',
    @Priority varchar(6) = '',
    @Subject varchar(1000),
    @ContentType varchar(100),
    @Message varchar(8000),
    @Server varchar(100)
AS
BEGIN

	-- Local variable declarations
	DECLARE @Object int
	DECLARE @Hresult int
	DECLARE @Error varchar(8000)
	DECLARE @ErrorSource varchar(255)
	DECLARE @ErrorDesc varchar(255)
	DECLARE @Sender varchar(400)
	DECLARE @Result bit

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 0

	-- Check input parameters
	--
	-- @From
	IF (dbo.ufEmailAddressIsValid(@From) = 0)
		BEGIN
			SET @Error = 'Invalid email address specified for @From'
			RAISERROR(@Error, 16, 1)
			RETURN @Result
		END
	ELSE
		BEGIN
			IF (LEFT(@From, 1) <> '<') SET @From = '<' + @From
			IF (RIGHT(@From, 1) <> '>') SET @From = @From + '>'
		END

	-- @From_Name
	IF (dbo.ufStringLen(@From_Name) > 0) SET @Sender = @From_Name + ' ' + @From
	ELSE SET @Sender = @From

	-- @Priority
	SET @Priority = CASE @Priority 
										WHEN 'NORMAL' THEN 0 
										WHEN 'LOW' THEN 1 
										WHEN 'HIGH' THEN 2 
										ELSE 0 
									END

	-- @ContentType
	SET @ContentType = CASE @ContentType 
										WHEN 'text/plain' THEN 'text/plain'
										WHEN 'text/html' THEN 'text/html'
										ELSE 'text/plain'
									END

	--Create the object
	EXEC @Hresult = sp_OACreate 'OSSMTP.SMTPSession', @Object OUT 

	--Error check
	IF @Hresult <> 0
		BEGIN
			EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
			SET @Error = 'An error occured while creating object: ' + @ErrorSource + ' ' + @ErrorDesc
			RAISERROR(@Error, 16, 1)
			GOTO CleanUp
		END

	--Set Server Property
	EXEC @Hresult = sp_OASetProperty @Object, 'Server', @Server

	--Error check
	IF @Hresult <> 0
		BEGIN
			EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
			SET @Error = 'An error occured while setting Server property: ' + @ErrorSource + ' ' + @ErrorDesc
			RAISERROR(@Error, 16, 1)
			GOTO CleanUp
		END

	--Set MailFrom Property
	EXEC @Hresult = sp_OASetProperty @Object, 'MailFrom', @Sender

	--Error check
	IF @Hresult <> 0
		BEGIN
			EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
			SET @Error = 'An error occured while setting MailFrom property: ' + @ErrorSource + ' ' + @ErrorDesc
			RAISERROR(@Error, 16, 1)
			GOTO CleanUp
		END

	-- Only set ReplyTo property, if necessary
	IF (dbo.ufStringLen(@ReplyTo) > 0) 
		BEGIN
			--Set ReplyTo Property
			EXEC @Hresult = sp_OASetProperty @Object, 'ReplyTo', @ReplyTo

			--Error check
			IF @Hresult <> 0
				BEGIN
					EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
					SET @Error = 'An error occured while setting ReplyTo property: ' + @ErrorSource + ' ' + @ErrorDesc
					RAISERROR(@Error, 16, 1)
					GOTO CleanUp
				END
		END

	--Set SendTo Property
	EXEC @Hresult = sp_OASetProperty @Object, 'SendTo', @To

	--Error check
	IF @Hresult <> 0
		BEGIN
			EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
			SET @Error = 'An error occured while setting SendTo property: ' + @ErrorSource + ' ' + @ErrorDesc
			RAISERROR(@Error, 16, 1)
			GOTO CleanUp
		END

	-- Only set CC property, if necessary
	IF (dbo.ufStringLen(@CC) > 0) 
		BEGIN
			--Set CC Property
			EXEC @Hresult = sp_OASetProperty @Object, 'CC', @CC

			--Error check
			IF @Hresult <> 0
				BEGIN
					EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
					SET @Error = 'An error occured while setting CC property: ' + @ErrorSource + ' ' + @ErrorDesc
					RAISERROR(@Error, 16, 1)
					GOTO CleanUp
				END
		END

	-- Only set BCC property, if necessary
	IF (dbo.ufStringLen(@BCC) > 0) 
		BEGIN
			--Set BCC Property
			EXEC @Hresult = sp_OASetProperty @Object, 'BCC', @BCC

			--Error check
			IF @Hresult <> 0
				BEGIN
					EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
					SET @Error = 'An error occured while setting BCC property: ' + @ErrorSource + ' ' + @ErrorDesc
					RAISERROR(@Error, 16, 1)
					GOTO CleanUp
				END
		END

	--Set MessageSubject Property
	EXEC @Hresult = sp_OASetProperty @Object, 'MessageSubject', @Subject

	--Error check
	IF @Hresult <> 0
		BEGIN
			EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
			SET @Error = 'An error occured while setting MessageSubject property: ' + @ErrorSource + ' ' + @ErrorDesc
			RAISERROR(@Error, 16, 1)
			GOTO CleanUp
		END

	--Set ContentType Property
	EXEC @Hresult = sp_OASetProperty @Object, 'ContentType', @ContentType

	--Error check
	IF @Hresult <> 0
		BEGIN
			EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
			SET @Error = 'An error occured while setting ContentType property: ' + @ErrorSource + ' ' + @ErrorDesc
			RAISERROR(@Error, 16, 1)
			GOTO CleanUp
		END

	--Set MessageText Property
	EXEC @Hresult = sp_OASetProperty @Object, 'MessageText', @Message

	--Error check
	IF @Hresult <> 0
		BEGIN
			EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
			SET @Error = 'An error occured while setting MessageText property: ' + @ErrorSource + ' ' + @ErrorDesc
			RAISERROR(@Error, 16, 1)
			GOTO CleanUp
		END

	-- Only set Importance property, if necessary
	IF (@Priority <> 0) 
		BEGIN
			--Set Importance Property
			EXEC @Hresult = sp_OASetProperty @Object, 'Importance', @Priority

			--Error check
			IF @Hresult <> 0
				BEGIN
					EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
					SET @Error = 'An error occured while setting Importance property: ' + @ErrorSource + ' ' + @ErrorDesc
					RAISERROR(@Error, 16, 1)
					GOTO CleanUp
				END
		END

	--SendEmail
	EXEC @Hresult = sp_OAMethod @Object, 'SendEmail'

	--Error check
	IF @Hresult <> 0
		BEGIN
			EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
			SET @Error = 'An error occured while sending email: ' + @ErrorSource + ' ' + @ErrorDesc
			RAISERROR(@Error, 16, 1)
			GOTO CleanUp
		END

	-- Set return variable
	SET @Result = 1

CleanUp:

	--Destroy the object 
	EXEC @Hresult = sp_OADestroy @Object

	RETURN @Result

END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upOSSMTPSendEmail
TO sys_email