USE [master]
GO
/****** Object:  StoredProcedure [dbo].[upSearchSysCatalogObjects]    Script Date: 10/03/2006 15:14:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[upSearchSysCatalogObjects]
  @SearchString varchar(80),
	@Database varchar(40) = '',
	@SearchColumns bit = 0
AS
SET NOCOUNT ON

  /* ************************************************* */
  /* This procedure searches syscomments for the       */
  /* existence of the given search string and reports  */
  /* the objects that contain in and their type.       */
  /* ************************************************* */

EXEC ('SELECT DISTINCT
    CASE o.type 
			WHEN ''P'' THEN ''PROCEDURE'' 
			WHEN ''TF'' THEN ''TFUNC'' 
			WHEN ''FN'' THEN ''SFUNC'' 
			WHEN ''TR'' THEN ''TRIGGER'' 
			WHEN ''V'' THEN ''VIEW'' 
			WHEN ''IF'' THEN ''IFUNC''
		END AS ObjectType,
    o.Name AS ObjectName
		FROM ' + @Database + '.dbo.syscomments c
    INNER JOIN ' + @Database + '.dbo.sysobjects o ON (c.ID = o.ID AND o.type IN (''P'', ''FN'', ''TF'', ''V'', ''TR'', ''IF''))
		WHERE c.text LIKE ''%' + @SearchString + '%''
		ORDER BY ObjectType ASC, ObjectName ASC')

IF (@SearchColumns = 1)
	EXEC ('SELECT DISTINCT
			CASE o.type 
				WHEN ''FT'' THEN ''FUNC'' 
				WHEN ''U'' THEN ''UTABLE'' 
				WHEN ''TF'' THEN ''TFUNC'' 
				WHEN ''V'' THEN ''VIEW'' 
				WHEN ''IF'' THEN ''IFUNC''
			END AS ObjectType,
			o.Name AS ObjectName,
			c.Name AS ColumnName
			FROM ' + @Database + '.dbo.syscolumns c
			INNER JOIN ' + @Database + '.dbo.sysobjects o ON (c.ID = o.ID AND o.type IN (''FT'', ''U'', ''TF'', ''V'', ''IF''))
			WHERE c.name LIKE ''%' + @SearchString + '%''
			ORDER BY ObjectType ASC, ObjectName ASC, ColumnName ASC')

SET NOCOUNT OFF
