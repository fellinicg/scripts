USE [OutboundEmail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.upSendMailAdminMessage
  @MessageSubject varchar(120),
  @MessageBody varchar(7000),
  @IsSuccess bit OUTPUT
AS
DECLARE
  @MailAdminEmailAddress varchar(120),
  @SMTPServerName varchar(80),
  @SMTPErrorStatus int

  SET NOCOUNT ON
  
  -- set vars & constants
  SET @SMTPServerName = dbo.ufSMTPServerName() 
  SET @MailAdminEmailAddress = dbo.ufMailAdminEmailAddress() 
  
  -- send message
  EXEC @SMTPErrorStatus = master.dbo.upOSSMTPSendEmail
    @From = 'mpg-mailer-service@madisonpg.com',
    @From_Name = 'MPG Automated Email Service',
    @ReplyTo = 'noreply@madisonpg.com',
    @To = @MailAdminEmailAddress,
    @BCC = '',
    @Priority = 'HIGH',
    @Subject = @MessageSubject,
    @ContentType = 'text/plain',
    @Message = @MessageBody,
    @Server = @SMTPServerName

  IF @SMTPErrorStatus = 0
    SET @IsSuccess = 1
  ELSE
    SET @IsSuccess = 0
  
  SET NOCOUNT OFF
