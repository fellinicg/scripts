USE [OutboundEmail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.upSendMessage
	@EmailID int,
	@MessageSent bit OUTPUT
AS
DECLARE
	@ToEmailAddressList varchar(1024),
	@FromName varchar(120),
	@FromEmailAddress varchar(120),
	@ReplyToEmailAddress varchar(120),
	@CCEmailAddressList varchar(1024),
	@BCCEmailAddressList varchar(1024),
	@MessageSubject varchar(120),
	@MessageBody varchar(7000),
	@SMTPContentType varchar(20),	-- 'text/plain' | 'text/html'
	@SMTPPriority varchar(6),	-- 'LOW' | 'NORMAL' | 'HIGH'
	@RedirectMessage bit,
	@SMTPServerName varchar(80),
	@SMTPErrorStatus int,
	@Status char(1),
	@IsSuccess bit

	SET NOCOUNT ON

	SET @MessageSent = 0

	IF dbo.ufSuspendMessageSending() = 1
		RETURN

	-- lock record during send and subsequent status change
	BEGIN TRAN

	-- set vars & constants
	SET @SMTPServerName = dbo.ufSMTPServerName() 
	SELECT
		@ToEmailAddressList = ToEmailAddressList,
		@FromName = FromName,
		@FromEmailAddress = FromEmailAddress,
		@ReplyToEmailAddress = ReplyToEmailAddress,
		@CCEmailAddressList = CCEmailAddressList,
		@BCCEmailAddressList = BCCEmailAddressList,
		@MessageSubject = MessageSubject,
		@MessageBody = MessageBody,
		@SMTPContentType = CASE SMTPContentType WHEN 'P' THEN 'text/plain' WHEN 'H' THEN 'text/html' END,
		@SMTPPriority = CASE SMTPPriority WHEN 'L' THEN 'LOW' WHEN 'N' THEN 'NORMAL' WHEN 'H' THEN 'HIGH' END,
		@RedirectMessage = RedirectMessage
	FROM tbOutboundEmail (UPDLOCK HOLDLOCK)
	WHERE EmailID = @EmailID
		AND Status = 'P'	-- pending
		AND (EarliestSendDateTime IS NULL OR CURRENT_TIMESTAMP >= EarliestSendDateTime)	-- send no message before its time
	IF @@ROWCOUNT = 0
	BEGIN
		COMMIT TRAN
		RETURN
	END
	IF @RedirectMessage = 1
	BEGIN
		SET @ToEmailAddressList = dbo.ufRedirectEmailAddress()
		SET @ReplyToEmailAddress = dbo.ufRedirectEmailAddress()
		SET @CCEmailAddressList = NULL
		SET @BCCEmailAddressList = NULL
	END

	-- send message
	EXEC @SMTPErrorStatus = master.dbo.upOSSMTPSendEmail
		@From = @FromEmailAddress,
		@From_Name = @FromName,
		@ReplyTo = @ReplyToEmailAddress,
		@To = @ToEmailAddressList,
		@CC = @CCEmailAddressList,
		@BCC = @BCCEmailAddressList,
		@Priority = @SMTPPriority,
		@Subject = @MessageSubject,
		@ContentType = @SMTPContentType,
		@Message = @MessageBody,
		@Server = @SMTPServerName
	IF @SMTPErrorStatus = 0
		SET @MessageSent = 1
	ELSE
		BEGIN
			SET @MessageSubject = 'MPG Emailer System - Error'
			SET @MessageBody = 'upSendMessage call to master.dbo.upOSSMTPSendEmail returned error for EmailID(' + ISNULL(CONVERT(varchar(20), @EmailID), 'NULL') + '). Message NOT sent.'
			EXEC dbo.upSendDBAdminMessage @MessageSubject = @MessageSubject, @MessageBody = @MessageBody, @IsSuccess = NULL
		END

	SET @Status = CASE WHEN @MessageSent = 1 THEN 'S' ELSE 'F' END

	EXEC dbo.upUpdateMessageStatus @EmailID = @EmailID, @Status = @Status, @IsSuccess = @IsSuccess OUTPUT
	IF @IsSuccess = 0
	BEGIN
		SET @MessageSubject = 'MPG Emailer System - Error'
		SET @MessageBody = 'upSendMessage - call to upUpdateMessageStatus failed for EmailID(' + ISNULL(CONVERT(varchar(20), @EmailID), 'NULL') + '). Message WAS sent.'
		EXEC dbo.upSendDBAdminMessage @MessageSubject = @MessageSubject, @MessageBody = @MessageBody, @IsSuccess = NULL
	END

	COMMIT TRAN
	
	SET NOCOUNT OFF

