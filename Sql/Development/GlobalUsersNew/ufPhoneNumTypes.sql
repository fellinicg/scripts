USE [GlobalUsersNew]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 15, 2006
-- Description:	Return phone number types
-- =============================================
ALTER FUNCTION dbo.ufPhoneNumTypes ()
RETURNS TABLE 
AS
RETURN 
(
	SELECT 'H' AS Type, 'Home' AS Description
	UNION
	SELECT 'W', 'Work'
	UNION
	SELECT 'M', 'Mobile'
	UNION
	SELECT 'O', 'Other'
	UNION
	SELECT 'U', 'Unknown'
)

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF