USE [GlobalUsersNew]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 14, 2006
-- Description:	Return mail address type description
-- =============================================
ALTER FUNCTION dbo.ufMailAddressTypeDescription
(
	@MailAddressType char(1)
)
RETURNS varchar(40)
AS
BEGIN
	DECLARE @Result varchar(40)

	SELECT @Result = CASE UPPER(@MailAddressType)
										WHEN 'H' THEN 'Home'
										WHEN 'W' THEN 'Work'
										WHEN 'O' THEN 'Other'
										WHEN 'U' THEN 'Unknown' 
										ELSE '' END

	RETURN @Result

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF