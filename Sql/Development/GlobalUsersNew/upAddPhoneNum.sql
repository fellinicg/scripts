USE [GlobalUsersNew]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 9, 2006
-- Description:	Add a phone number for a user account
-- =============================================
ALTER PROCEDURE dbo.upAddPhoneNum 
	@PhoneNumID int OUTPUT,
	@UserAccountID int,
	@PhoneNumType char(1),  -- (U)nknown, (O)ther, (M)obile, (W)ork or (H)ome
	@IDDCode varchar(10) = '',
	@CityAreaCode varchar(10) = '',
	@PhoneNum varchar(20) = '',
	@Extension varchar(10) = '',
	@OrigPhoneNum varchar(20) = '',
	@TxtMsgCapable bit = 0,
	@IsPreferred bit = 0
AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp datetime

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CURRENT_TIMESTAMP

	-- ***** Check Inputs *****
	-- 
	-- Nullify IDDCode when blank
	IF (LEN(RTRIM(LTRIM(@IDDCode))) = 0) SET @IDDCode = null

	-- Nullify CityAreaCode when blank
	IF (LEN(RTRIM(LTRIM(@CityAreaCode))) = 0) SET @CityAreaCode = null

	-- Nullify PhoneNum when blank
	IF (LEN(LTRIM(RTRIM(@PhoneNum))) = 0) SET @PhoneNum = null

	-- Nullify Extension when blank
	IF (LEN(RTRIM(LTRIM(@Extension))) = 0) SET @Extension = null

	-- Nullify OrigPhoneNum when blank
	IF (LEN(RTRIM(LTRIM(@OrigPhoneNum))) = 0) SET @OrigPhoneNum = null

	-- If IsPreferred, then reset all other phone numbers to not preferred
	IF (@IsPreferred = 1)
		UPDATE dbo.tbPhoneNum SET IsPreferred = 0 WHERE UserAccountID = @UserAccountID

	-- Insert new record into tbPhoneNum
	INSERT INTO dbo.tbPhoneNum (
		UserAccountID,
		PhoneNumType,
		IDDCode,
		CityAreaCode,
		PhoneNum,
		Extension,
		OrigPhoneNum,
		TxtMsgCapable,
		IsPreferred,
		CreateDateTime, 
		UpdateDateTime)
	VALUES (
		@UserAccountID,
		UPPER(@PhoneNumType),
		@IDDCode,
		@CityAreaCode,
		@PhoneNum,
		@Extension,
		@OrigPhoneNum,
		@TxtMsgCapable,
		@IsPreferred,
		@DateTimeStamp,
		@DateTimeStamp)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@IDENTITY > 0)
		SET @PhoneNumID = @@IDENTITY
	ELSE
		RAISERROR('upAddPhoneNum - phone number was NOT successfully added', 16, 1)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF