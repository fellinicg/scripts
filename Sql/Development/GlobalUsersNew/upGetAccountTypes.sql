USE [GlobalUsersNew]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 13, 2006
-- Description:	Retrieve all account types
-- =============================================
ALTER PROCEDURE dbo.upGetAccountTypes
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Insert new record into tbAccountType
	SELECT AccountType, Description
	FROM dbo.tbAccountType
	ORDER BY Description

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF