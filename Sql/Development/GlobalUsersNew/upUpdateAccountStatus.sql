USE [GlobalUsersNew]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 9, 2006
-- Description:	Update an account status
-- =============================================
ALTER PROCEDURE dbo.upUpdateAccountStatus 
	@Result char(1) OUTPUT, -- (S)uccess or (F)ailure
	@AccountStatus int,
	@Description	varchar(40)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'

	-- Update record in tbAccountStatus
	UPDATE dbo.tbAccountStatus SET Description = @Description
	WHERE AccountStatus = @AccountStatus

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upUpdateAccountStatus - account status was NOT successfully updated', 16, 1)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF