USE [GlobalUsersNew]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 9, 2006
-- Description:	Delete a phone number for a user account
-- =============================================
ALTER PROCEDURE dbo.upDeletePhoneNum 
	@Result char(1) OUTPUT, -- (S)uccess or (F)ailure
	@PhoneNumID int,
	@UserAccountID int
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'

	-- Delete record in tbPhoneNum
	DELETE FROM dbo.tbPhoneNum
	WHERE PhoneNumID = @PhoneNumID
	AND UserAccountID = @UserAccountID

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upDeletePhoneNum - phone number was NOT successfully deleted', 16, 1)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF