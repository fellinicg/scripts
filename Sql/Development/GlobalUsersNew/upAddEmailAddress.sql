USE [GlobalUsersNew]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 8, 2006
-- Description:	Add an email address for a user account
-- =============================================
ALTER PROCEDURE dbo.upAddEmailAddress 
	@UserAccountID int,
	@EMailAddressType char(1),  -- (U)nknown, (O)ther, (M)obile, (W)ork or (H)ome
	@EmailAddress	varchar(120),
	@IsPreferred bit,
	@EmailAddressID int OUTPUT
AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp datetime

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CURRENT_TIMESTAMP

	-- If IsPreferred, then reset all other phone numbers to not preferred
	IF (@IsPreferred = 1)
		UPDATE dbo.tbEmailAddress SET IsPreferred = 0 WHERE UserAccountID = @UserAccountID

	-- Insert new record into tbEmailAddress
	INSERT INTO dbo.tbEmailAddress (
		UserAccountID,
		EMailAddressType, 
		EmailAddress, 
		IsPreferred, 
		CreateDateTime, 
		UpdateDateTime)
	VALUES (
		@UserAccountID,
		UPPER(@EMailAddressType),
		@EmailAddress,
		@IsPreferred,
		@DateTimeStamp,
		@DateTimeStamp)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@IDENTITY > 0)
		SET @EmailAddressID = @@IDENTITY
	ELSE
		RAISERROR('upAddEmailAddress - email address was NOT successfully added', 16, 1)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF