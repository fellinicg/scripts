USE [GlobalUsersNew]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 9, 2006
-- Description:	Add an account type
-- =============================================
ALTER PROCEDURE dbo.upAddAccountType 
	@Result char(1) OUTPUT, -- (S)uccess or (F)ailure
	@AccountType char(1),
	@Description varchar(40)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'

	-- Insert new record into tbAccountType
	INSERT INTO dbo.tbAccountType (AccountType, Description)
	VALUES (@AccountType, @Description)

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upAddAccountType - account type was NOT successfully added', 16, 1)

END


GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF