USE [GlobalUsersNew]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 9, 2006
-- Description:	Update a government id type
-- =============================================
ALTER PROCEDURE dbo.upUpdateGovtIDType 
	@Result char(1) OUTPUT, -- (S)uccess or (F)ailure
	@GovtIDType int,
	@Description	varchar(40)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'

	-- Update record in tbGovtIDType
	UPDATE dbo.tbGovtIDType SET Description = @Description
	WHERE GovtIDType = @GovtIDType

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upUpdateGovtIDType - government id type was NOT successfully updated', 16, 1)

END


GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF