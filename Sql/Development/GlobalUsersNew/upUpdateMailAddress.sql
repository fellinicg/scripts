USE [GlobalUsersNew]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 9, 2006
-- Description:	Update a mail address for a user account
-- =============================================
ALTER PROCEDURE dbo.upUpdateMailAddress 
	@Result char(1) OUTPUT, -- (S)uccess or (F)ailure
	@MailAddressID varchar(40),
	@UserAccountID varchar(40),
	@UpdMailAddressType bit = 0,
	@MailAddressType char(1) = '',  -- U)nknown, (O)ther, (W)ork or (H)ome
	@UpdLine1 bit = 0,
	@Line1 varchar(80) = '',
	@UpdLine2 bit = 0,
	@Line2 varchar(80) = '',
	@UpdLine3 bit = 0,
	@Line3 varchar(80) = '',
	@UpdLine4 bit = 0,
	@Line4 varchar(80) = '',
	@UpdCityTown bit = 0,
	@CityTown varchar(40) = '',
	@UpdStateProvince bit = 0,
	@StateProvince varchar(40) = '',
	@UpdPostalCode bit = 0,
	@PostalCode varchar(20) = '',
	@UpdCountry bit = 0,
	@Country varchar(80) = '',
	@UpdCityLine bit = 0,
	@CityLine varchar(120) = '',
	@UpdUseCityLine bit = 0,
	@UseCityLine bit = 0,
	@UpdIsPreferred bit = 0,
	@IsPreferred bit = 0
AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp varchar(40)
	DECLARE @FieldsToUpdate varchar(8000)

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CONVERT(VARCHAR, CURRENT_TIMESTAMP, 121)
	SET @FieldsToUpdate = ''
	SET @Result = 'F'

	-- ***** Check Inputs *****
	-- 
	-- MailAddressType
	IF (@UpdMailAddressType = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET MailAddressType = ' + CASE WHEN LEN(RTRIM(LTRIM(@MailAddressType))) = 0 THEN 'null' ELSE '''' + @MailAddressType + '''' END
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',MailAddressType = ' + CASE WHEN LEN(RTRIM(LTRIM(@MailAddressType))) = 0 THEN 'null' ELSE '''' + @MailAddressType + '''' END

	-- Line1
	IF (@UpdLine1 = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET Line1 = ' + CASE WHEN LEN(RTRIM(LTRIM(@Line1))) = 0 THEN 'null' ELSE '''' + @Line1 + '''' END
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',Line1 = ' + CASE WHEN LEN(RTRIM(LTRIM(@Line1))) = 0 THEN 'null' ELSE '''' + @Line1 + '''' END

	-- Line2
	IF (@UpdLine2 = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET Line2 = ' + CASE WHEN LEN(RTRIM(LTRIM(@Line2))) = 0 THEN 'null' ELSE '''' + @Line2 + '''' END
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',Line2 = ' + CASE WHEN LEN(RTRIM(LTRIM(@Line2))) = 0 THEN 'null' ELSE '''' + @Line2 + '''' END

	-- Line3
	IF (@UpdLine3 = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET Line3 = ' + CASE WHEN LEN(RTRIM(LTRIM(@Line3))) = 0 THEN 'null' ELSE '''' + @Line3 + '''' END
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',Line3 = ' + CASE WHEN LEN(RTRIM(LTRIM(@Line3))) = 0 THEN 'null' ELSE '''' + @Line3 + '''' END

	-- Line4
	IF (@UpdLine4 = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET Line4 = ' + CASE WHEN LEN(RTRIM(LTRIM(@Line4))) = 0 THEN 'null' ELSE '''' + @Line4 + '''' END
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',Line4 = ' + CASE WHEN LEN(RTRIM(LTRIM(@Line4))) = 0 THEN 'null' ELSE '''' + @Line4 + '''' END

	-- CityTown
	IF (@UpdCityTown = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET CityTown = ' + CASE WHEN LEN(RTRIM(LTRIM(@CityTown))) = 0 THEN 'null' ELSE '''' + @CityTown + '''' END
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',CityTown = ' + CASE WHEN LEN(RTRIM(LTRIM(@CityTown))) = 0 THEN 'null' ELSE '''' + @CityTown + '''' END

	-- StateProvince
	IF (@UpdStateProvince = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET StateProvince = ' + CASE WHEN LEN(RTRIM(LTRIM(@StateProvince))) = 0 THEN 'null' ELSE '''' + @StateProvince + '''' END
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',StateProvince = ' + CASE WHEN LEN(RTRIM(LTRIM(@StateProvince))) = 0 THEN 'null' ELSE '''' + @StateProvince + '''' END

	-- PostalCode
	IF (@UpdPostalCode = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET PostalCode = ' + CASE WHEN LEN(RTRIM(LTRIM(@PostalCode))) = 0 THEN 'null' ELSE '''' + @PostalCode + '''' END
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',PostalCode = ' + CASE WHEN LEN(RTRIM(LTRIM(@PostalCode))) = 0 THEN 'null' ELSE '''' + @PostalCode + '''' END

	-- Country
	IF (@UpdCountry = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET Country = ' + CASE WHEN LEN(RTRIM(LTRIM(@Country))) = 0 THEN 'null' ELSE '''' + @Country + '''' END
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',Country = ' + CASE WHEN LEN(RTRIM(LTRIM(@Country))) = 0 THEN 'null' ELSE '''' + @Country + '''' END

	-- CityLine
	IF (@UpdCityLine = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET CityLine = ' + CASE WHEN LEN(RTRIM(LTRIM(@CityLine))) = 0 THEN 'null' ELSE '''' + @CityLine + '''' END
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',CityLine = ' + CASE WHEN LEN(RTRIM(LTRIM(@CityLine))) = 0 THEN 'null' ELSE '''' + @CityLine + '''' END

	-- UseCityLine
	IF (@UpdUseCityLine = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET UseCityLine = ' + CONVERT(VARCHAR, @UseCityLine)
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',UseCityLine = ' + CONVERT(VARCHAR, @UseCityLine)

	-- IsPreferred
	IF (@UpdIsPreferred = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET IsPreferred = ' + CONVERT(VARCHAR, @IsPreferred)
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',IsPreferred = ' + CONVERT(VARCHAR, @IsPreferred)

	-- Update record in EmergencyContact if necessary
	IF (LEN(@FieldsToUpdate) > 0)
		BEGIN
			-- Add update timestamp
			SET @FieldsToUpdate = @FieldsToUpdate + ',UpdateDateTime = ''' + @DateTimeStamp + ''''

			-- If IsPreferred, then reset all other phone numbers to not preferred
			IF (@UpdIsPreferred = 1 AND @IsPreferred = 1)
				UPDATE dbo.tbMailAddress SET IsPreferred = 0 WHERE UserAccountID = @UserAccountID

			EXECUTE ('UPDATE dbo.tbMailAddress' + @FieldsToUpdate + ' WHERE MailAddressID = ' + @MailAddressID + ' AND UserAccountID = ' + @UserAccountID)
		END
	ELSE
		BEGIN
				RAISERROR('upUpdateMailAddress - no fields were specified for update', 16, 1)
				RETURN
		END

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upUpdateMailAddress - mail address was NOT successfully updated', 16, 1)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF