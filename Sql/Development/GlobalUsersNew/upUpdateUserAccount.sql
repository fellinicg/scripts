USE [GlobalUsersNew]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 7, 2006
-- Description:	
-- =============================================
ALTER PROCEDURE dbo.upUpdateUserAccount 
	@UpdResult bit = 0 OUTPUT,  -- (1) Success or (0) Failure
	@UserAccountID varchar(40),
	@UpdAccountType bit = 0,  -- (1) Update or (0) Ignore
	@AccountType char(1) = '',  -- (C)lient, (M)adison Employee, (O)ther or (V)endor
	@UpdIsPerson bit = 0,  -- (1) Update or (0) Ignore
	@IsPerson bit = 1,  -- (1) True or (0) False
	@UpdClientID bit = 0,  -- (1) Update or (0) Ignore
	@ClientID int = 0,  -- (0) for null
	@UpdCustAccountCode bit = 0,  -- (1) Update or (0) Ignore
	@CustAccountCode varchar(40) = '',
	@UpdAltAccountCode bit = 0,  -- (1) Update or (0) Ignore
	@AltAccountCode varchar(40) = '',
	@UpdAccountName bit = 0,  -- (1) Update or (0) Ignore
	@AccountName varchar(80) = '',
	@UpdPersonNamePrefix bit = 0,  -- (1) Update or (0) Ignore
	@PersonNamePrefix varchar(20) = '',
	@UpdPersonFirstGivenName bit = 0,  -- (1) Update or (0) Ignore
	@PersonFirstGivenName varchar(40) = '',
	@UpdPersonSecondGivenName bit = 0,  -- (1) Update or (0) Ignore
	@PersonSecondGivenName varchar(40) = '',
	@UpdPersonFamilyName bit = 0,  -- (1) Update or (0) Ignore
	@PersonFamilyName varchar(60) = '',
	@UpdPersonNameSuffix bit = 0,  -- (1) Update or (0) Ignore
	@PersonNameSuffix varchar(20) = '',
	@UpdPersonNaturalFullName bit = 0,  -- (1) Update or (0) Ignore
	@PersonNaturalFullName varchar(200) = '',
	@UpdPersonBuiltFullName bit = 0,  -- (1) Update or (0) Ignore
	@PersonBuiltFullName varchar(200) = '',
	@UpdPersonGovtIDType bit = 0,  -- (1) Update or (0) Ignore
	@PersonGovtIDType char(1) = '',  -- (S)SN
	@UpdPersonGovtIDCodeOneWayCrypted bit = 0,  -- (1) Update or (0) Ignore
	@PersonGovtIDCodeOneWayCrypted varchar(200) = '',
	@UpdPersonGovtIDCodeTwoWayCrypted bit = 0,  -- (1) Update or (0) Ignore
	@PersonGovtIDCodeTwoWayCrypted varchar(200) = '',
	@UpdAccountStatus bit = 0,  -- (1) Update or (0) Ignore
	@AccountStatus char(1) = ''  -- (D)isabled, (E)nabled, or (X)Deleted
AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp varchar(40)
	DECLARE @FieldsToUpdate varchar(8000)

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CONVERT(VARCHAR, CURRENT_TIMESTAMP, 121)
	SET @FieldsToUpdate = ''

	-- ***** Check Inputs *****
	-- 
	-- AccountType
	IF (@UpdAccountType = 1)
		BEGIN
			-- Validate AccountType
			SET @AccountType = UPPER(@AccountType)
			IF (NOT EXISTS(SELECT 1 FROM dbo.tbAccountType WHERE AccountType = @AccountType))
			BEGIN
				RAISERROR('upUpdateUserAccount - @AccountType supplied is not a valid account type on the system', 16, 1)
				RETURN
			END
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET AccountType = ''' + @AccountType + ''''
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',AccountType = ''' + @AccountType + ''''
		END

	-- IsPerson
	IF (@UpdIsPerson = 1)
		BEGIN
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET IsPerson = ' + CONVERT(VARCHAR, @IsPerson)
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',IsPerson = ' + CONVERT(VARCHAR, @IsPerson)
		END

	-- ClientID
	IF (@UpdClientID = 1)
		BEGIN
			-- Validate ClientID
			IF (@ClientID = 0) SET @ClientID = null
			ELSE
				IF (NOT EXISTS(SELECT 1 FROM SQL2KPROD.Catalog.dbo.Clients WHERE cl_ID = @ClientID))
				BEGIN
					RAISERROR('upUpdateUserAccount - @ClientID supplied is not a valid client id on the system', 16, 1)
					RETURN
				END
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET ClientID = ' + CASE WHEN @ClientID IS NULL THEN 'null' ELSE CONVERT(VARCHAR, @ClientID) END
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',ClientID = ' + CASE WHEN @ClientID IS NULL THEN 'null' ELSE CONVERT(VARCHAR, @ClientID) END
		END

	-- CustAccountCode
	IF (@UpdCustAccountCode = 1)
		BEGIN
			-- Nullify CustAccountCode when blank
			IF (LEN(RTRIM(LTRIM(@CustAccountCode))) = 0) SET @CustAccountCode = null
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET CustAccountCode = ' + CASE WHEN @CustAccountCode IS NULL THEN 'null' ELSE '''' + @CustAccountCode + '''' END
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',CustAccountCode = ' + CASE WHEN @CustAccountCode IS NULL THEN 'null' ELSE '''' + @CustAccountCode + '''' END
		END

	-- AltAccountCode
	IF (@UpdAltAccountCode = 1)
		BEGIN
			-- Nullify AltAccountCode when blank
			IF (LEN(RTRIM(LTRIM(@AltAccountCode))) = 0) SET @AltAccountCode = null
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET AltAccountCode = ' + CASE WHEN @AltAccountCode IS NULL THEN 'null' ELSE '''' + @AltAccountCode + '''' END
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',AltAccountCode = ' + CASE WHEN @AltAccountCode IS NULL THEN 'null' ELSE '''' + @AltAccountCode + '''' END
		END

	-- AccountName
	IF (@UpdAccountName = 1)
		BEGIN
			-- Nullify AccountName when blank
			IF (LEN(RTRIM(LTRIM(@AccountName))) = 0) SET @AccountName = null
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET AccountName = ' + CASE WHEN @AccountName IS NULL THEN 'null' ELSE '''' + @AccountName + '''' END
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',AccountName = ' + CASE WHEN @AccountName IS NULL THEN 'null' ELSE '''' + @AccountName + '''' END
		END

	-- PersonNamePrefix
	IF (@UpdPersonNamePrefix = 1)
		BEGIN
			-- Nullify PersonNamePrefix when blank
			IF (LEN(RTRIM(LTRIM(@PersonNamePrefix))) = 0) SET @PersonNamePrefix = null
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET PersonNamePrefix = ' + CASE WHEN @PersonNamePrefix IS NULL THEN 'null' ELSE '''' + @PersonNamePrefix + '''' END
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',PersonNamePrefix = ' + CASE WHEN @PersonNamePrefix IS NULL THEN 'null' ELSE '''' + @PersonNamePrefix + '''' END
		END

	-- PersonFirstGivenName
	IF (@UpdPersonFirstGivenName = 1)
		BEGIN
			-- Nullify PersonFirstGivenName when blank
			IF (LEN(RTRIM(LTRIM(@PersonFirstGivenName))) = 0) SET @PersonFirstGivenName = null
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET PersonFirstGivenName = ' + CASE WHEN @PersonFirstGivenName IS NULL THEN 'null' ELSE '''' + @PersonFirstGivenName + '''' END
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',PersonFirstGivenName = ' + CASE WHEN @PersonFirstGivenName IS NULL THEN 'null' ELSE '''' + @PersonFirstGivenName + '''' END
		END

	-- PersonSecondGivenName
	IF (@UpdPersonSecondGivenName = 1)
		BEGIN
			-- Nullify PersonSecondGivenName when blank
			IF (LEN(RTRIM(LTRIM(@PersonSecondGivenName))) = 0) SET @PersonSecondGivenName = null
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET PersonSecondGivenName = ' + CASE WHEN @PersonSecondGivenName IS NULL THEN 'null' ELSE '''' + @PersonSecondGivenName + '''' END
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',PersonSecondGivenName = ' + CASE WHEN @PersonSecondGivenName IS NULL THEN 'null' ELSE '''' + @PersonSecondGivenName + '''' END
		END

	-- PersonFamilyName
	IF (@UpdPersonFamilyName = 1)
		BEGIN
			-- Nullify PersonFamilyName when blank
			IF (LEN(RTRIM(LTRIM(@PersonFamilyName))) = 0) SET @PersonFamilyName = null
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET PersonFamilyName = ' + CASE WHEN @PersonFamilyName IS NULL THEN 'null' ELSE '''' + @PersonFamilyName + '''' END
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',PersonFamilyName = ' + CASE WHEN @PersonFamilyName IS NULL THEN 'null' ELSE '''' + @PersonFamilyName + '''' END
		END

	-- PersonNameSuffix
	IF (@UpdPersonNameSuffix = 1)
		BEGIN
			-- Nullify PersonNameSuffix when blank
			IF (LEN(RTRIM(LTRIM(@PersonNameSuffix))) = 0) SET @PersonNameSuffix = null
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET PersonNameSuffix = ' + CASE WHEN @PersonNameSuffix IS NULL THEN 'null' ELSE '''' + @PersonNameSuffix + '''' END
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',PersonNameSuffix = ' + CASE WHEN @PersonNameSuffix IS NULL THEN 'null' ELSE '''' + @PersonNameSuffix + '''' END
		END

	-- PersonNaturalFullName
	IF (@UpdPersonNaturalFullName = 1)
		BEGIN
			-- Nullify PersonNaturalFullName when blank
			IF (LEN(RTRIM(LTRIM(@PersonNaturalFullName))) = 0) SET @PersonNaturalFullName = null
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET PersonNaturalFullName = ' + CASE WHEN @PersonNaturalFullName IS NULL THEN 'null' ELSE '''' + @PersonNaturalFullName + '''' END
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',PersonNaturalFullName = ' + CASE WHEN @PersonNaturalFullName IS NULL THEN 'null' ELSE '''' + @PersonNaturalFullName + '''' END
		END

	-- PersonBuiltFullName
	IF (@UpdPersonBuiltFullName = 1)
		BEGIN
			-- Nullify PersonBuiltFullName when blank
			IF (LEN(RTRIM(LTRIM(@PersonBuiltFullName))) = 0) SET @PersonBuiltFullName = null
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET PersonBuiltFullName = ' + CASE WHEN @PersonBuiltFullName IS NULL THEN 'null' ELSE '''' + @PersonBuiltFullName + '''' END
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',PersonBuiltFullName = ' + CASE WHEN @PersonBuiltFullName IS NULL THEN 'null' ELSE '''' + @PersonBuiltFullName + '''' END
		END

	-- PersonGovtIDType
	IF (@UpdPersonGovtIDType = 1)
		BEGIN
			-- Nullify PersonGovtIDType when blank, else verify
			IF (LEN(RTRIM(LTRIM(@PersonGovtIDType))) = 0) SET @PersonGovtIDType = null
			ELSE
				SET @PersonGovtIDType = UPPER(@PersonGovtIDType)
				IF (NOT EXISTS(SELECT 1 FROM dbo.tbGovtIDType WHERE GovtIDType = @PersonGovtIDType))
				BEGIN
					RAISERROR('upUpdateUserAccount - @PersonGovtIDType supplied is not a valid government id on the system', 16, 1)
					RETURN
				END
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET PersonGovtIDType = ' + CASE WHEN @PersonGovtIDType IS NULL THEN 'null' ELSE '''' + @PersonGovtIDType + '''' END
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',PersonGovtIDType = ' + CASE WHEN @PersonGovtIDType IS NULL THEN 'null' ELSE '''' + @PersonGovtIDType + '''' END
		END

	-- PersonGovtIDCodeOneWayCrypted
	IF (@UpdPersonGovtIDCodeOneWayCrypted = 1)
		BEGIN
			-- Nullify PersonGovtIDCodeOneWayCrypted when blank
			IF (LEN(RTRIM(LTRIM(@PersonGovtIDCodeOneWayCrypted))) = 0) SET @PersonGovtIDCodeOneWayCrypted = null
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET PersonGovtIDCodeOneWayCrypted = ' + CASE WHEN @PersonGovtIDCodeOneWayCrypted IS NULL THEN 'null' ELSE '''' + @PersonGovtIDCodeOneWayCrypted + '''' END
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',PersonGovtIDCodeOneWayCrypted = ' + CASE WHEN @PersonGovtIDCodeOneWayCrypted IS NULL THEN 'null' ELSE '''' + @PersonGovtIDCodeOneWayCrypted + '''' END
		END

	-- PersonGovtIDCodeTwoWayCrypted
	IF (@UpdPersonGovtIDCodeTwoWayCrypted = 1)
		BEGIN
			-- Nullify PersonGovtIDCodeTwoWayCrypted when blank
			IF (LEN(RTRIM(LTRIM(@PersonGovtIDCodeTwoWayCrypted))) = 0) SET @PersonGovtIDCodeTwoWayCrypted = null
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET PersonGovtIDCodeTwoWayCrypted = ' + CASE WHEN @PersonGovtIDCodeTwoWayCrypted IS NULL THEN 'null' ELSE '''' + @PersonGovtIDCodeTwoWayCrypted + '''' END
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',PersonGovtIDCodeTwoWayCrypted = ' + CASE WHEN @PersonGovtIDCodeTwoWayCrypted IS NULL THEN 'null' ELSE '''' + @PersonGovtIDCodeTwoWayCrypted + '''' END
		END

	-- AccountStatus
	IF (@UpdAccountStatus = 1)
		BEGIN
			-- Validate @AccountStatus
			SET @AccountStatus = UPPER(@AccountStatus)
			IF (NOT EXISTS(SELECT 1 FROM dbo.tbAccountStatus WHERE AccountStatus = @AccountStatus))
			BEGIN
				RAISERROR('upUpdateUserAccount - @AccountStatus supplied is not a valid account status on the system', 16, 1)
				RETURN
			END
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET AccountStatus = ''' + @AccountStatus + ''',StatusDateTime = ''' + @DateTimeStamp + ''''
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',AccountStatus = ''' + @AccountStatus + ''',StatusDateTime = ''' + @DateTimeStamp + ''''
		END

	-- Update record in tbUserAccount if necessary
	IF (LEN(@FieldsToUpdate) > 0)
		BEGIN
			-- Added update timestamp
			SET @FieldsToUpdate = @FieldsToUpdate + ',UpdateDateTime = ''' + @DateTimeStamp + ''''
--			PRINT 'UPDATE dbo.tbUserAccount' + @FieldsToUpdate + ' WHERE UserAccountID = ' + @UserAccountID
			EXECUTE ('UPDATE dbo.tbUserAccount' + @FieldsToUpdate + ' WHERE UserAccountID = ' + @UserAccountID)
		END
	ELSE
		BEGIN
				RAISERROR('upUpdateUserAccount - no fields were specified for update', 16, 1)
				RETURN
		END


	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @UpdResult = 1
	ELSE
		RAISERROR('upUpdateUserAccount - user account was NOT successfully updated', 16, 1)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF