USE [GlobalUsersNew]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 8, 2006
-- Description:	Delete an emergency contact for a user account
-- =============================================
ALTER PROCEDURE dbo.upDeleteEmergencyContact 
	@Result char(1) OUTPUT, -- (S)uccess or (F)ailure
	@EmergencyContactID int,
	@UserAccountID int
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'

	-- Delete record in tbEmergencyContact
	DELETE FROM dbo.tbEmergencyContact
	WHERE EmergencyContactID = @EmergencyContactID
	AND UserAccountID = @UserAccountID

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upDeleteEmergencyContact - emergency contact was NOT successfully deleted', 16, 1)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF