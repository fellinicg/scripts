USE [GlobalUsersNew]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 15, 2006
-- Description:	Retrieve all contact types for a specific contact method
-- =============================================
ALTER PROCEDURE dbo.upGetContactTypes
	@Method varchar(20)
AS
BEGIN

	-- Local declarations
	DECLARE @uf varchar(40)

	-- Initialize settings
	SET NOCOUNT ON

	-- Return data
	SET @uf = CASE UPPER(@Method) 
							WHEN 'EMAIL' THEN 'dbo.ufEMailAddressTypes()' 
							WHEN 'MAIL' THEN 'dbo.ufMailAddressTypes()' 
							WHEN 'PHONE' THEN 'dbo.ufPhoneNumTypes()' 
							ELSE ''
						END

	IF LEN(@uf) > 1 EXEC ('SELECT * FROM ' + @uf)

END


GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF