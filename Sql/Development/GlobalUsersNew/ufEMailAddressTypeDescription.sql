USE [GlobalUsersNew]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 14, 2006
-- Description:	Return email address type description
-- =============================================
ALTER FUNCTION dbo.ufEMailAddressTypeDescription
(
	@EMailAddressType char(1)
)
RETURNS varchar(40)
AS
BEGIN
	DECLARE @Result varchar(40)

	SELECT @Result = CASE UPPER(@EMailAddressType)
										WHEN 'H' THEN 'Home'
										WHEN 'W' THEN 'Work'
										WHEN 'M' THEN 'Mobile'
										WHEN 'O' THEN 'Other'
										WHEN 'U' THEN 'Unknown'
										ELSE '' END

	RETURN @Result

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF