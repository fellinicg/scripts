USE GlobalUsersNew

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 7, 2006
-- Description:	
-- =============================================
ALTER PROCEDURE dbo.upAddUserAccount 
	@AccountType char(1),  -- (C)lient, (M)adison Employee, (O)ther or (V)endor
	@IsPerson bit,  -- (1) True or (0) False
	@ClientID int = 0,  -- (0) for null
	@CustAccountCode varchar(40) = '',
	@AltAccountCode varchar(40) = '',
	@AccountName varchar(80) = '',
	@PersonNamePrefix varchar(20) = '',
	@PersonFirstGivenName varchar(40) = '',
	@PersonSecondGivenName varchar(40) = '',
	@PersonFamilyName varchar(60) = '',
	@PersonNameSuffix varchar(20) = '',
	@PersonNaturalFullName varchar(200) = '',
	@PersonBuiltFullName varchar(200) = '',
	@PersonGovtIDType char(1) = '',  -- (S)SN
	@PersonGovtIDCodeOneWayCrypted varchar(200) = '',
	@PersonGovtIDCodeTwoWayCrypted varchar(200) = '',
	@AccountStatus  char(1),  -- (D)isabled, (E)nabled, or (X)Deleted
	@UserAccountID int = -1 OUTPUT  -- (-1)
AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp datetime

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CURRENT_TIMESTAMP

	-- ***** Check Inputs *****
	-- 
	-- Validate AccountType
	IF (NOT EXISTS(SELECT 1 FROM dbo.tbAccountType WHERE AccountType = @AccountType))
	BEGIN
		RAISERROR('upAddUserAccount - @AccountType supplied is not a valid account type on the system', 16, 1)
		RETURN
	END

	-- Validate ClientID
	IF (@ClientID = 0) SET @ClientID = null
	ELSE
		IF (NOT EXISTS(SELECT 1 FROM SQL2KPROD.Catalog.dbo.Clients WHERE cl_ID = @ClientID))
		BEGIN
			RAISERROR('upAddUserAccount - @ClientID supplied is not a valid client id on the system', 16, 1)
			RETURN
		END

	-- Nullify CustAccountCode when blank
	IF (LEN(RTRIM(LTRIM(@CustAccountCode))) = 0) SET @CustAccountCode = null

	-- Nullify AltAccountCode when blank
	IF (LEN(RTRIM(LTRIM(@AltAccountCode))) = 0) SET @AltAccountCode = null

	-- Nullify AccountName when blank
	IF (LEN(RTRIM(LTRIM(@AccountName))) = 0) SET @AccountName = null

	-- Nullify PersonNamePrefix when blank
	IF (LEN(RTRIM(LTRIM(@PersonNamePrefix))) = 0) SET @PersonNamePrefix = null

	-- Nullify PersonFirstGivenName when blank
	IF (LEN(RTRIM(LTRIM(@PersonFirstGivenName))) = 0) SET @PersonFirstGivenName = null

	-- Nullify PersonSecondGivenName when blank
	IF (LEN(RTRIM(LTRIM(@PersonSecondGivenName))) = 0) SET @PersonSecondGivenName = null

	-- Nullify PersonFamilyName when blank
	IF (LEN(RTRIM(LTRIM(@PersonFamilyName))) = 0) SET @PersonFamilyName = null

	-- Nullify PersonNameSuffix when blank
	IF (LEN(RTRIM(LTRIM(@PersonNameSuffix))) = 0) SET @PersonNameSuffix = null

	-- Nullify PersonNaturalFullName when blank
	IF (LEN(RTRIM(LTRIM(@PersonNaturalFullName))) = 0) SET @PersonNaturalFullName = null

	-- Nullify PersonBuiltFullName when blank
	IF (LEN(RTRIM(LTRIM(@PersonBuiltFullName))) = 0) SET @PersonBuiltFullName = null

	-- Nullify PersonGovtIDType when blank, else verify
	IF (LEN(RTRIM(LTRIM(@PersonGovtIDType))) = 0) SET @PersonGovtIDType = null
	ELSE
		IF (NOT EXISTS(SELECT 1 FROM dbo.tbGovtIDType WHERE GovtIDType = @PersonGovtIDType))
		BEGIN
			RAISERROR('upAddUserAccount - @PersonGovtIDType supplied is not a valid government id on the system', 16, 1)
			RETURN
		END

	-- Nullify PersonGovtIDCodeOneWayCrypted when blank
	IF (LEN(RTRIM(LTRIM(@PersonGovtIDCodeOneWayCrypted))) = 0) SET @PersonGovtIDCodeOneWayCrypted = null

	-- Nullify PersonGovtIDCodeTwoWayCrypted when blank
	IF (LEN(RTRIM(LTRIM(@PersonGovtIDCodeTwoWayCrypted))) = 0) SET @PersonGovtIDCodeTwoWayCrypted = null

	-- Verify that either @PersonGovtIDCodeOneWayCrypted or @PersonGovtIDCodeTwoWayCrypted is populated
	-- when @PersonGovtIDType is supplied (not null)
	IF (NOT @PersonGovtIDType IS NULL)
		IF (@PersonGovtIDCodeOneWayCrypted IS NULL AND @PersonGovtIDCodeTwoWayCrypted IS NULL)
		BEGIN
			RAISERROR('upAddUserAccount - @PersonGovtIDCodeOneWayCrypted or @PersonGovtIDCodeTwoWayCrypted must be supplied', 16, 1)
			RETURN
		END

	-- Validate @AccountStatus
	IF (NOT EXISTS(SELECT 1 FROM dbo.tbAccountStatus WHERE AccountStatus = @AccountStatus))
	BEGIN
		RAISERROR('upAddUserAccount - @AccountStatus supplied is not a valid account status on the system', 16, 1)
		RETURN
	END

	-- Insert new record into tbUserAccount
	INSERT INTO dbo.tbUserAccount (
		AccountType, 
		IsPerson, 
		ClientID, 
		CustAccountCode, 
		AltAccountCode, 
		AccountName, 
		PersonNamePrefix, 
		PersonFirstGivenName, 
		PersonSecondGivenName, 
		PersonFamilyName, 
		PersonNameSuffix, 
		PersonNaturalFullName, 
		PersonBuiltFullName, 
		PersonGovtIDType, 
		PersonGovtIDCodeOneWayCrypted, 
		PersonGovtIDCodeTwoWayCrypted, 
		AccountStatus,
		StatusDateTime,
		CreateDateTime,
		UpdateDateTime)
	VALUES (
		UPPER(@AccountType),
		@IsPerson,
		@ClientID,
		@CustAccountCode,
		@AltAccountCode,
		@AccountName,
		@PersonNamePrefix,
		@PersonFirstGivenName,
		@PersonSecondGivenName,
		@PersonFamilyName,
		@PersonNameSuffix,
		@PersonNaturalFullName,
		@PersonBuiltFullName,
		@PersonGovtIDType,
		@PersonGovtIDCodeOneWayCrypted,
		@PersonGovtIDCodeTwoWayCrypted,
		@AccountStatus,
		@DateTimeStamp,
		@DateTimeStamp,
		@DateTimeStamp)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@IDENTITY > 0)
		SET @UserAccountID = @@IDENTITY
	ELSE
		RAISERROR('upAddUserAccount - user account was NOT successfully added', 16, 1)

END
GO
