USE [GlobalUsersNew]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 8, 2006
-- Description:	Delete an email address for a user account
-- =============================================
ALTER PROCEDURE dbo.upDeleteEmailAddress 
	@Result char(1) OUTPUT, -- (S)uccess or (F)ailure
	@EmailAddressID int,
	@UserAccountID int
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'

	-- Delete record in tbEmailAddress
	DELETE FROM dbo.tbEmailAddress
	WHERE EmailAddressID = @EmailAddressID
	AND UserAccountID = @UserAccountID

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upDeleteEmailAddress - email address was NOT successfully deleted', 16, 1)

END


GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF