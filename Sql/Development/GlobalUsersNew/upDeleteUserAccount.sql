USE [GlobalUsersNew]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 16, 2006
-- Description:	Delete a user account
-- =============================================
ALTER PROCEDURE dbo.upDeleteUserAccount
	@Result char(1) OUTPUT, -- (S)uccess or (F)ailure
	@UserAccountID int
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Begin transaction
	BEGIN TRAN

	-- Delete records in tbEmailAddress
	DELETE FROM dbo.tbEmailAddress
	WHERE UserAccountID = @UserAccountID

	-- Delete records in tbEmergencyContact
	DELETE FROM dbo.tbEmergencyContact
	WHERE UserAccountID = @UserAccountID

	-- Delete records in tbMailAddress
	DELETE FROM dbo.tbMailAddress
	WHERE UserAccountID = @UserAccountID

	-- Delete records in tbPhoneNum
	DELETE FROM dbo.tbPhoneNum
	WHERE UserAccountID = @UserAccountID

	-- Delete record in tbUserAccount
	DELETE FROM dbo.tbUserAccount
	WHERE UserAccountID = @UserAccountID

	-- Verify success
	IF (@@ROWCOUNT = 1)
		BEGIN
			SET @Result = 'S'
			COMMIT TRAN
		END
	ELSE
		BEGIN
			ROLLBACK TRAN
			SET @Result = 'F'
			RAISERROR('upDeleteUserAccount - user account was NOT successfully deleted', 16, 1)
		END

END



GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF