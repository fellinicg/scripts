USE [CitigroupVA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.rave_CreateToken
	@SessionID uniqueidentifier,
	@Token uniqueidentifier OUTPUT,
	@SessionIsInvalid bit OUTPUT
AS
DECLARE
	@UserID int,
	@UserGEID varchar(50),
	@RegionProgramID int

	SET NOCOUNT ON
	
	SET @UserID = dbo.ufSessionUserID(@SessionID)
	IF (@UserID = 0)
	BEGIN
		SET @SessionIsInvalid = 1
		RETURN
	END
	SET @SessionIsInvalid = 0
	SET @UserGEID = dbo.ufGetUserGEID(@UserID)	

	SELECT @RegionProgramID = r.RegionProgramID
	FROM dbo.citiUser u
	INNER JOIN dbo.citiCountry c ON u.u_CountryID = c.CountryID
	INNER JOIN dbo.citiRegion r ON c.RegionID = r.RegionID
	WHERE u.u_ID = 1

	EXECUTE CURLY.FulfilmentVA.dbo.sf2_CreateToken 
		 @ProgramID = @RegionProgramID
		,@UserId = @UserID
		,@Token = @Token OUTPUT

	-- NEW TOKEN REPLACES OLD
--	DELETE citiShopToken WHERE u_GEID = @UserGEID

--	SET @Token = @SessionID

	-- Create entry
--	INSERT citiShopToken(u_GEID, u_Token) VALUES(@UserGEID, @Token)
	
	SET NOCOUNT OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
