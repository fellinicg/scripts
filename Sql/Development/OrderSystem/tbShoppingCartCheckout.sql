USE [OrderSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbShoppingCartCheckout](
	[CartID] [int] NOT NULL,
	[ProgramItemID] [int] NOT NULL,
	[MasterCatalogID] [int] NOT NULL,
	[ItemDescription] [varchar](100) NOT NULL,
	[ItemType] [char](1) NOT NULL,
	[VendorID] [int] NOT NULL,
	[ItemModel] [varchar](100) NOT NULL,
	[ProgramItemValue] [money] NOT NULL,
	[CostPerUnit] [money] NOT NULL,
	[ShippingPerUnit] [money] NOT NULL,
	[TaxPerUnit] [money] NOT NULL,
	[ShippingFlag] [char](1) NOT NULL,
	[ItemsInPkg] [tinyint] NOT NULL,
	[Token] [varchar](50) NOT NULL,
	[RecordDate] [datetime] NOT NULL CONSTRAINT [DF_tbShoppingCartCheckout_RecordDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbShoppingCartCheckout] PRIMARY KEY CLUSTERED 
(
	[CartID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF