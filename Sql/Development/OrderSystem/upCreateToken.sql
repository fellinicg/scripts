USE [OrderSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061113
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upCreateToken 
	@ProgramID int, 
	@UserId int,
	@Token varchar(36) OUTPUT
AS
BEGIN
	SET NOCOUNT ON

	SET @Token = NEWID()

	INSERT INTO dbo.tbStoreFrontToken (TokenID, UserID, ProgramID)
	VALUES (@Token, @UserId, @ProgramID)

END
GO
