USE [OrderSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_tbOrderLine_tbOrder]') AND type = 'F')
ALTER TABLE [dbo].[tbOrderLine] DROP CONSTRAINT [FK_tbOrderLine_tbOrder]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbOrderLine]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbOrderLine]
GO
CREATE TABLE [dbo].[tbOrderLine](
	[OrderLineID] [bigint] IDENTITY(500000,1) NOT NULL,
	[OrderID] [bigint] NOT NULL,
	[ProgramItemID] [int] NOT NULL,
	[ItemDescription] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ProgramItemValue] [money] NOT NULL,
	[Quantity] [smallint] NOT NULL,
	[AddlShippingCharge] [money] NOT NULL CONSTRAINT [DF_tbOrderLine_AddlShippingCharge]  DEFAULT (0),
	[ItemDescriptionOverride] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[QuantityOverride] [smallint] NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbOrderLine_IsCancelled]  DEFAULT (0),
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[DeletedComments] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_tbOrderLine] PRIMARY KEY CLUSTERED 
(
	[OrderLineID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [OrderSystem]
GO
ALTER TABLE [dbo].[tbOrderLine]  WITH CHECK ADD  CONSTRAINT [FK_tbOrderLine_tbOrder] FOREIGN KEY([OrderID])
REFERENCES [dbo].[tbOrder] ([OrderID])
GO
ALTER TABLE [dbo].[tbOrderLine] CHECK CONSTRAINT [FK_tbOrderLine_tbOrder]