USE [OrderSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061113
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upGetCreditCards
AS
BEGIN

	SET NOCOUNT ON

	SELECT CreditCardAbbrev, CreditCard
	FROM dbo.tbCreditCard
	WHERE Accepted = 1
	ORDER BY CreditCard

	SET NOCOUNT OFF

END
GO
