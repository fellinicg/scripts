USE [OrderSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbCreditCard]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbCreditCard]
GO
CREATE TABLE [dbo].[tbCreditCard](
	[CreditCardAbbrev] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CreditCard] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Accepted] [bit] NOT NULL DEFAULT (1),
 CONSTRAINT [PK_CreditCards] PRIMARY KEY CLUSTERED 
(
	[CreditCardAbbrev]ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF

GO

INSERT INTO [OrderSystem].[dbo].[tbCreditCard]
           ([CreditCardAbbrev]
           ,[CreditCard])
     VALUES
           ('amex'
           ,'American Express')
GO
INSERT INTO [OrderSystem].[dbo].[tbCreditCard]
           ([CreditCardAbbrev]
           ,[CreditCard])
     VALUES
           ('visa'
           ,'Visa')
GO
INSERT INTO [OrderSystem].[dbo].[tbCreditCard]
           ([CreditCardAbbrev]
           ,[CreditCard])
     VALUES
           ('mc'
           ,'MasterCard')
GO
INSERT INTO [OrderSystem].[dbo].[tbCreditCard]
           ([CreditCardAbbrev]
           ,[CreditCard])
     VALUES
           ('discover'
           ,'Discover')