USE [OrderSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbOrderLineItem](
	[OrderLineItemID] [bigint] IDENTITY(1,500000) NOT NULL,
	[OrderLineID] [bigint] NOT NULL,
	[ItemType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MasterCatalogID] [int] NOT NULL,
	[VendorID] [int] NOT NULL,
	[ItemDescription] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ItemModel] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShippingFlag] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CostPerUnit] [money] NOT NULL,
	[ShippingPerUnit] [money] NOT NULL,
	[TaxPerUnit] [money] NOT NULL,
	[IsBackOrdered] [bit] NOT NULL CONSTRAINT [DF_tbOrderLineItem_IsBackOrdered]  DEFAULT (0),
	[BackOrderedNotes] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DiscountApplied] [bit] NOT NULL CONSTRAINT [DF_tbOrderLineItem_DiscountApplied]  DEFAULT (0),
	[CertNumbers] [varchar](3000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShippedDate] [datetime] NULL,
	[ShippedBy] [int] NULL,
	[ShippedVia] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TrackingNumber] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PONumber] [int] NULL,
	[POLine] [int] NULL,
	[SBTStatus] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SBTStatusDate] [datetime] NULL,
	[ItemDescriptionOverride] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ItemModelOverride] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CostPerUnitOverride] [money] NULL,
	[OverrideByID] [int] NULL,
	[PONotes] [varchar](5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_tbOrderLineItem] PRIMARY KEY CLUSTERED 
(
	[OrderLineItemID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [OrderSystem]
GO
ALTER TABLE [dbo].[tbOrderLineItem]  WITH NOCHECK ADD  CONSTRAINT [FK_tbOrderLineItem_tbOrderLine] FOREIGN KEY([OrderLineID])
REFERENCES [dbo].[tbOrderLine] ([OrderLineID])
GO
ALTER TABLE [dbo].[tbOrderLineItem] CHECK CONSTRAINT [FK_tbOrderLineItem_tbOrderLine]