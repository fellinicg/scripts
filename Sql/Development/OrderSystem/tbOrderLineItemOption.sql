USE [OrderSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbOrderLineItemOption](
	[OrderLineItemID] [bigint] NOT NULL,
	[OptionID] [int] NOT NULL,
	[OptionDescription] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OptionDescriptionOverride] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OverrideByID] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [OrderSystem]
GO
ALTER TABLE [dbo].[tbOrderLineItemOption]  WITH CHECK ADD  CONSTRAINT [FK_tbOrderLineItemOption_ttbOrderLineItemOption] FOREIGN KEY([OrderLineItemID])
REFERENCES [dbo].[tbOrderLineItem] ([OrderLineItemID])