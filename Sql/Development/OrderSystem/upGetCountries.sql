USE [OrderSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061113
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upGetCountries
AS
BEGIN

	SET NOCOUNT ON

	SELECT Country, ISOAlpha2, ISOAlpha3
	FROM dbo.tbCountry
	WHERE IsDeleted = 0
	ORDER BY Country

	SET NOCOUNT OFF
END
GO
