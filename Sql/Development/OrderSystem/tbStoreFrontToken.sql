USE [OrderSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbStoreFrontToken](
	[TokenID] [uniqueidentifier] NOT NULL,
	[UserID] [int] NOT NULL,
	[ProgramID] [int] NOT NULL,
	[CreatedOn] [smalldatetime] NOT NULL CONSTRAINT [DF_StoreFrontToken_CreatedOn]  DEFAULT (getdate())
) ON [PRIMARY]

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF