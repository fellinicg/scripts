DECLARE	@InputFile VARCHAR(50)
DECLARE @TabName VARCHAR(50)
DECLARE @bcpcmd VARCHAR(5000)

-- Initialize variables
SET @InputFile = 'iso3166-countrycodes.xls'
SET @TabName = RTRIM(LEFT(@InputFile, CHARINDEX('.', @InputFile) - 1))

-- Map drive
SET @bcpcmd = 'net use z: /delete&net use z: \\TODD\OnvantageReports$\SourceFiles'
EXECUTE master.dbo.xp_cmdshell @bcpcmd

-- Copy file to SQL server
SET @bcpcmd = 'copy z:\' + @InputFile + ' d:\' + @InputFile
EXECUTE master.dbo.xp_cmdshell @bcpcmd

-- Clear Table
TRUNCATE TABLE OrderSystem.dbo.tbCountry

-- Import file
EXEC ('INSERT INTO OrderSystem.dbo.tbCountry
SELECT * FROM OPENROWSET(''Microsoft.Jet.OLEDB.4.0'', 
''Excel 8.0;DATABASE=d:\' + @InputFile + ''', ''select * from [' + @TabName + '$]'')')
