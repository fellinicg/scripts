USE [OrderSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbCountry]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbCountry]
GO
CREATE TABLE [dbo].[tbCountry](
	[Country] [varchar](50) NOT NULL,
	[ISOAlpha2] [char](2) NOT NULL,
	[ISOAlpha3] [char](3) NOT NULL,
	[IsDeleted] [bit] NOT NULL DEFAULT 0
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF