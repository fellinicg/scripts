USE [OrderSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbOrder](
	[OrderID] [bigint] IDENTITY(50000,1) NOT NULL,
	[ProgramID] [int] NOT NULL,
	[DKNumber] [int] NOT NULL,
	[UserAccountID] [int] NOT NULL,
	[UserAccountEmail] [varchar](120) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[UserAccountClientID] [int] NOT NULL,
	[ShipName] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ShipCompany] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShipAddress1] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ShipAddress2] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShipAddress3] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShipAddress4] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShipCityTown] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ShipStateProvince] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ShipPostalCode] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ShipCountry] [char](2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ShipDayPhone] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ShipEveningPhone] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShipEmail] [varchar](120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreateDateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_tbOrder] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF