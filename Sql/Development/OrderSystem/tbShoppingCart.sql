USE [OrderSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbShoppingCart]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbShoppingCart]
GO
CREATE TABLE [dbo].[tbShoppingCart](
	[CartID] [int] IDENTITY(1,1) NOT NULL,
	[ProgramID] [int] NULL,
	[UserID] [int] NULL,
	[AwardID] [int] NULL,
	[Quantity] [int] NULL,
	[IsWishList] [bit] NOT NULL CONSTRAINT [DF_ShoppingCart_IsWishList]  DEFAULT (0),
	[RecordDateTime] [datetime] NULL CONSTRAINT [DF_ShoppingCart_RecordDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbShoppingCart] PRIMARY KEY CLUSTERED 
(
	[CartID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF