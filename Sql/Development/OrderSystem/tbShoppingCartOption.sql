USE [OrderSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_tbShoppingCartOption_tbShoppingCart]') AND type = 'F')
ALTER TABLE [dbo].[tbShoppingCartOption] DROP CONSTRAINT [FK_tbShoppingCartOption_tbShoppingCart]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbShoppingCartOption]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbShoppingCartOption]
GO
CREATE TABLE [dbo].[tbShoppingCartOption](
	[CartOptionID] [int] IDENTITY(1,1) NOT NULL,
	[CartID] [int] NULL,
	[OptionID] [int] NULL,
	[OptionText] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbShoppingCartOption]  WITH CHECK ADD  CONSTRAINT [FK_tbShoppingCartOption_tbShoppingCart] FOREIGN KEY([CartID])
REFERENCES [dbo].[tbShoppingCart] ([CartID])