USE [OrderSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061113
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upGetTokenInfo
	@Token varchar(36),
	@ProgramID int OUTPUT, 
	@UserId int OUTPUT
AS
BEGIN

	SET NOCOUNT ON

	-- Retrieve information associated with token.
	--
	SELECT @UserId = UserID, @ProgramID = ProgramID
	FROM dbo.tbStoreFrontToken
  WHERE TokenID = @Token

	-- Delete token.
	--
	DELETE FROM dbo.tbStoreFrontToken
  WHERE TokenID = @Token

END
GO
