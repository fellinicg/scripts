USE [OrderSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061113
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upGetStates
AS
BEGIN

	SET NOCOUNT ON

	SELECT State, StateCode
	FROM dbo.tbState
	WHERE IsDeleted = 0
	ORDER BY State

	SET NOCOUNT OFF
END
GO
