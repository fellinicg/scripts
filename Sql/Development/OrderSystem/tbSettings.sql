USE [OrderSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbSettings](
	[Label] [varchar](80) NOT NULL,
	[Value] [varchar](1024) NOT NULL,
 CONSTRAINT [pkSettings] PRIMARY KEY NONCLUSTERED 
(
	[Label] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
