USE [GlobalUsers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 8, 2006
-- Description:	Add an emergency contact for a user account
-- =============================================
ALTER PROCEDURE dbo.upAddEmergencyContact 
	@EmergencyContactID int OUTPUT,
	@UserAccountID int,
	@FullName varchar(200),
	@RelationshipDesc varchar(20) = '',
	@IDDCode varchar(10) = '',
	@CityAreaCode varchar(10) = '',
	@PhoneNum varchar(20),
	@Extension varchar(10) = ''
AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp datetime

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CURRENT_TIMESTAMP

	-- ***** Check Inputs *****
	-- 
	-- Validate FullName
	IF (LEN(LTRIM(RTRIM(@FullName))) = 0)
		BEGIN
			RAISERROR('upAddEmergencyContact - @FullName must not be blank', 16, 1)
			RETURN
		END

	-- Validate PhoneNum
	IF (LEN(LTRIM(RTRIM(@PhoneNum))) = 0)
		BEGIN
			RAISERROR('upAddEmergencyContact - @PhoneNum must not be blank', 16, 1)
			RETURN
		END

	-- Nullify RelationshipDesc when blank
	IF (LEN(RTRIM(LTRIM(@RelationshipDesc))) = 0) SET @RelationshipDesc = null

	-- Nullify IDDCode when blank
	IF (LEN(RTRIM(LTRIM(@IDDCode))) = 0) SET @IDDCode = null

	-- Nullify CityAreaCode when blank
	IF (LEN(RTRIM(LTRIM(@CityAreaCode))) = 0) SET @CityAreaCode = null

	-- Nullify Extension when blank
	IF (LEN(RTRIM(LTRIM(@Extension))) = 0) SET @Extension = null

	-- Insert new record into tbEmergencyContact
	INSERT INTO dbo.tbEmergencyContact (
		UserAccountID,
		FullName,
		RelationshipDesc,
		IDDCode,
		CityAreaCode,
		PhoneNum,
		Extension,
		CreateDateTime, 
		UpdateDateTime)
	VALUES (
		@UserAccountID,
		@FullName,
		@RelationshipDesc,
		@IDDCode,
		@CityAreaCode,
		@PhoneNum,
		@Extension,
		@DateTimeStamp,
		@DateTimeStamp)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@IDENTITY > 0)
		SET @EmergencyContactID = @@IDENTITY
	ELSE
		RAISERROR('upAddEmergencyContact - emargency contact was NOT successfully added', 16, 1)

END


GO
SET ANSI_NULLS OFF
GO 
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upAddEmergencyContact
TO system