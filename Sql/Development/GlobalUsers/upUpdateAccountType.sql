USE [GlobalUsers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 9, 2006
-- Description:	Update an account type
-- =============================================
ALTER PROCEDURE dbo.upUpdateAccountType 
	@Result char(1) OUTPUT, -- (S)uccess or (F)ailure
	@AccountType int,
	@Description	varchar(40)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'

	-- Update record in tbAccountType
	UPDATE dbo.tbAccountType SET Description = @Description
	WHERE AccountType = @AccountType

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upUpdateAccountType - account type was NOT successfully updated', 16, 1)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upUpdateAccountType
TO system