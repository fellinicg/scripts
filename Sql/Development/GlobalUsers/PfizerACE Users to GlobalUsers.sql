ALTER TABLE tbUserAccount DISABLE TRIGGER tiUserAccount
GO
SET IDENTITY_INSERT tbUserAccount ON
GO
	-- NON SYSTEM ACCOUNTS
INSERT tbUserAccount
(
	UserAccountID,
	AccountType,
	IsPerson,
	ClientID,
	CustAccountCode,
	AltAccountCode,
	AccountName,
	PersonNamePrefix,
	PersonFirstGivenName,
	PersonSecondGivenName,
	PersonFamilyName,
	PersonNameSuffix,
	PersonNaturalFullName,
	PersonBuiltFullName,
	PersonGovtIDType,
	PersonGovtIDCodeOneWayCrypted,
	PersonGovtIDCodeTwoWayCrypted,
	AccountStatus,
	StatusDateTime,
	CreateDateTime,
	UpdateDateTime
)
SELECT
	gu_ID AS UserAccountID,
	CASE WHEN gu_cl_ID = 5 THEN 'M' WHEN gu_FirstName = 'Vendor' THEN 'V' ELSE 'C' END AS AccountType,
	CASE WHEN gu_FirstName = 'Vendor' THEN 0 ELSE 1 END AS IsPerson,
	NULLIF(gu_cl_ID, 0) AS ClientID,
	CASE WHEN gu_cl_ID = 5 THEN NULL WHEN gu_FirstName = 'Vendor' THEN NULL ELSE NULLIF(gu_CustUserCode, 0) END AS CustAccountCode,
	CASE WHEN gu_cl_ID = 5 THEN NULL WHEN gu_FirstName = 'Vendor' THEN NULLIF(gu_OldID, 0) ELSE NULL END AS AltAccountCode,
	CASE WHEN gu_FirstName = 'Vendor' THEN NULLIF(gu_LastName, '') ELSE NULL END AS AccountName,
	NULL AS NamePrefix,
	CASE WHEN gu_FirstName <> 'Vendor' THEN NULLIF(gu_FirstName, '') ELSE NULL END AS FirstGivenName,
	CASE WHEN gu_FirstName <> 'Vendor' THEN NULLIF(gu_MiddleName, '') ELSE NULL END AS SecondGivenName,
	CASE WHEN gu_FirstName <> 'Vendor' THEN NULLIF(gu_LastName, '') ELSE NULL END AS FamilyName,
	NULL AS NameSuffix,
	NULL AS PersonNaturalFullName,
	CASE WHEN gu_FirstName <> 'Vendor' THEN NULLIF(gu_FirstName, '') + ' ' + ISNULL(gu_MiddleName + ' ', '') + NULLIF(gu_LastName, '') ELSE NULL END AS PersonBuiltFullName,
	CASE WHEN ISNULL(gu_SSN, '') <> '' THEN 'S' ELSE NULL END AS PersonGovtIDType,
	NULL AS PersonGovtIDCodeOneWayCrypted,
	CASE WHEN ISNULL(gu_SSN, '') <> '' THEN master.dbo.ud_CCEncrypt(gu_SSN) ELSE NULL END AS PersonGovtIDCodeTwoWayCrypted,
	CASE gu_rs_ID WHEN 0 THEN 'E' WHEN 1 THEN 'D' ELSE 'X' END AS AccountStatus,
	CURRENT_TIMESTAMP AS StatusDateTime,
	gu_PostDate AS CreateDateTime,
	CURRENT_TIMESTAMP AS UpdateDateTime
FROM FastRewards.dbo.Users
LEFT JOIN GlobalUser.dbo.GlobalUsers on u_id = gu_id
WHERE gu_cl_ID = 3
GO

SET IDENTITY_INSERT tbUserAccount OFF
GO
ALTER TABLE tbUserAccount ENABLE TRIGGER tiUserAccount
GO