-- EMAIL ADDRESSES
ALTER TABLE tbEmailAddress DISABLE TRIGGER tiEmailAddress
GO
SET IDENTITY_INSERT tbEmailAddress ON
GO

INSERT tbEmailAddress
(
	EmailAddressID,
	UserAccountID,
	EmailAddressType,
	EmailAddress,
	IsPreferred,
	CreateDateTime,
	UpdateDateTime
)
SELECT
	em_ID AS EmailAddressID,
	em_gu_ID AS UserAccountID,
	CASE em_ct_ID WHEN 1 THEN 'W' WHEN 2 THEN 'H' ELSE 'U' END AS EmailAddressType,
	NULLIF(em_Address, '') AS EmailAddress,
	CASE em_Default WHEN 1 THEN 1 ELSE 0 END AS IsPreferred,
	em_PostDate AS CreateDateTime,
	CURRENT_TIMESTAMP AS UpdateDateTime
FROM FastRewards.dbo.Users
LEFT JOIN GlobalUser.dbo.GlobalUsers on u_id = gu_id
INNER JOIN GlobalUser.dbo.Email ON gu_ID = em_gu_ID
WHERE gu_cl_ID = 3
GO

SET IDENTITY_INSERT tbEmailAddress OFF
GO
ALTER TABLE tbEmailAddress ENABLE TRIGGER tiEmailAddress
GO

	-- CHECK RESULTS (ONLY ONE PREFERRED EMAIL ADDRESS)
IF (EXISTS (SELECT UserAccountID FROM tbEmailAddress WHERE IsPreferred = 1 GROUP BY UserAccountID HAVING COUNT(*) > 1))
BEGIN
	PRINT 'DUPLICATE PREFERRED EMAIL ADDRESSES'
	RETURN
END
GO
