USE [GlobalUsers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: September 5, 2006
-- Description:	Disable/Enable delete trigger on tbUserAccount
-- =============================================
CREATE PROCEDURE dbo.upSettdUserAccount
	@Action varchar(10)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Take appropriate action
	IF (UPPER(@Action) = 'ENABLE')
		ALTER TABLE tbUserAccount ENABLE TRIGGER tdUserAccount
	IF (UPPER(@Action) = 'DISABLE')
		ALTER TABLE tbUserAccount DISABLE TRIGGER tdUserAccount

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upSettdUserAccount
TO system