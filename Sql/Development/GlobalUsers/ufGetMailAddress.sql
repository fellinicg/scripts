USE [GlobalUsers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 15, 2006
-- Description:	Retrieve a mail address
-- =============================================
ALTER FUNCTION dbo.ufGetMailAddress 
(	
	@MailAddressID int,
	@UserAccountID int
)
RETURNS TABLE 
AS
RETURN 
(
	-- Retrieve record
	SELECT 
		MailAddressType,
		Line1,
		Line2,
		Line3,
		Line4,
		CityTown,
		StateProvince,
		PostalCode,
		Country,
		CityLine,
		UseCityLine,
		IsPreferred,
		CreateDateTime,
		UpdateDateTime
	FROM dbo.tbMailAddress
	WHERE MailAddressID = @MailAddressID
	AND UserAccountID = @UserAccountID
)
GO
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT SELECT
ON dbo.ufGetMailAddress
TO system