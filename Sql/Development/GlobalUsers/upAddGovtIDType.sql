USE [GlobalUsers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 9, 2006
-- Description:	Add a government id type
-- =============================================
ALTER PROCEDURE dbo.upAddGovtIDType 
	@Result char(1) OUTPUT, -- (S)uccess or (F)ailure
	@GovtIDType char(1),
	@Description varchar(40)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'

	-- Insert new record into tbGovtIDType
	INSERT INTO dbo.tbGovtIDType (GovtIDType, Description)
	VALUES (@GovtIDType, @Description)

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upAddGovtIDType - government id type was NOT successfully added', 16, 1)

END



GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upAddGovtIDType
TO system