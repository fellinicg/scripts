USE [GlobalUsers]
GO
/****** Object:  StoredProcedure [dbo].[upAddMailAddress]    Script Date: 06/08/2006 15:01:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 8, 2006
-- Description:	Add a mail address for a user account
-- =============================================
ALTER PROCEDURE dbo.upAddMailAddress 
	@MailAddressID int OUTPUT,
	@UserAccountID int,
	@MailAddressType char(1),  -- U)nknown, (O)ther, (W)ork or (H)ome
	@Line1 varchar(80) = '',
	@Line2 varchar(80) = '',
	@Line3 varchar(80) = '',
	@Line4 varchar(80) = '',
	@CityTown varchar(40) = '',
	@StateProvince varchar(40) = '',
	@PostalCode varchar(20) = '',
	@Country varchar(80) = '',
	@CityLine varchar(120) = '',
	@UseCityLine bit = 0,
	@IsPreferred bit
AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp datetime

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CURRENT_TIMESTAMP

	-- ***** Check Inputs *****
	-- 
	-- Nullify Line1 when blank
	IF (LEN(RTRIM(LTRIM(@Line1))) = 0) SET @Line1 = null

	-- Nullify Line2 when blank
	IF (LEN(RTRIM(LTRIM(@Line2))) = 0) SET @Line2 = null

	-- Nullify Line3 when blank
	IF (LEN(RTRIM(LTRIM(@Line3))) = 0) SET @Line3 = null

	-- Nullify Line4 when blank
	IF (LEN(RTRIM(LTRIM(@Line4))) = 0) SET @Line4 = null

	-- Nullify CityTown when blank
	IF (LEN(RTRIM(LTRIM(@CityTown))) = 0) SET @CityTown = null

	-- Nullify StateProvince when blank
	IF (LEN(RTRIM(LTRIM(@StateProvince))) = 0) SET @StateProvince = null

	-- Nullify PostalCode when blank
	IF (LEN(RTRIM(LTRIM(@PostalCode))) = 0) SET @PostalCode = null

	-- Nullify Country when blank
	IF (LEN(RTRIM(LTRIM(@Country))) = 0) SET @Country = null

	-- Nullify CityLine when blank
	IF (LEN(RTRIM(LTRIM(@CityLine))) = 0) SET @CityLine = null

	-- If IsPreferred, then reset all other phone numbers to not preferred
	IF (@IsPreferred = 1)
		UPDATE dbo.tbMailAddress SET IsPreferred = 0 WHERE UserAccountID = @UserAccountID

	-- Insert new record into tbMailAddress
	INSERT INTO dbo.tbMailAddress (
		UserAccountID,
		MailAddressType,
		Line1,
		Line2,
		Line3,
		Line4,
		CityTown,
		StateProvince,
		PostalCode,
		Country,
		CityLine,
		UseCityLine,
		IsPreferred,
		CreateDateTime, 
		UpdateDateTime)
	VALUES (
		@UserAccountID,
		UPPER(@MailAddressType),
		@Line1,
		@Line2,
		@Line3,
		@Line4,
		@CityTown,
		@StateProvince,
		@PostalCode,
		@Country,
		@CityLine,
		@UseCityLine,
		@IsPreferred,
		@DateTimeStamp,
		@DateTimeStamp)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@IDENTITY > 0)
		SET @MailAddressID = @@IDENTITY
	ELSE
		RAISERROR('upAddMailAddress - mail address was NOT successfully added', 16, 1)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upAddMailAddress
TO system