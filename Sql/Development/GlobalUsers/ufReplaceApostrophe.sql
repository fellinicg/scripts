USE [GlobalUsers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061122
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE FUNCTION dbo.ufReplaceApostrophe 
(
	@InputString varchar(4000)
)
RETURNS varchar(8000)
AS
BEGIN
	DECLARE @Result varchar(8000)
 
	SET @Result = REPLACE(@InputString, char(39), char(39) + char(39))

	RETURN @Result

END

