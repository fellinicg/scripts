USE [GlobalUsers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 9, 2006
-- Description:	Delete a mail address for a user account
-- =============================================
ALTER PROCEDURE dbo.upDeleteMailAddress 
	@Result char(1) OUTPUT, -- (S)uccess or (F)ailure
	@MailAddressID int,
	@UserAccountID int
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'

	-- Delete record in tbMailAddress
	DELETE FROM dbo.tbMailAddress
	WHERE MailAddressID = @MailAddressID
	AND UserAccountID = @UserAccountID

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upDeleteMailAddress - mail address was NOT successfully deleted', 16, 1)

END


GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upDeleteMailAddress
TO system