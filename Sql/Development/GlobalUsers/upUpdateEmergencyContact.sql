USE [GlobalUsers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 8, 2006
-- Description:	Update an emergency contact for a user account
-- =============================================
ALTER PROCEDURE dbo.upUpdateEmergencyContact 
	@Result char(1) OUTPUT, -- (S)uccess or (F)ailure
	@EmergencyContactID varchar(40),
	@UserAccountID varchar(40),
	@UpdFullName bit = 0,
	@FullName varchar(200) = '',
	@UpdRelationshipDesc bit = 0,
	@RelationshipDesc varchar(20) = '',
	@UpdIDDCode bit = 0,
	@IDDCode varchar(10) = '',
	@UpdCityAreaCode bit = 0,
	@CityAreaCode varchar(10) = '',
	@UpdPhoneNum bit = 0,
	@PhoneNum varchar(20) = '',
	@UpdExtension bit = 0,
	@Extension varchar(10) = ''
AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp varchar(40)
	DECLARE @FieldsToUpdate varchar(8000)

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CONVERT(VARCHAR, CURRENT_TIMESTAMP, 121)
	SET @FieldsToUpdate = ''
	SET @Result = 'F'

	-- ***** Check Inputs *****
	-- 
	-- FullName
	IF (@UpdFullName = 1)
		BEGIN
			IF (LEN(LTRIM(RTRIM(@FullName))) = 0)
				BEGIN
					RAISERROR('upUpdateEmergencyContact - @FullName must not be blank', 16, 1)
					RETURN
				END
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET FullName = ''' + @FullName + ''''
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',FullName = ''' + @FullName + ''''
		END

	-- Validate PhoneNum
	IF (@UpdPhoneNum = 1)
		BEGIN
			IF (LEN(LTRIM(RTRIM(@PhoneNum))) = 0)
				BEGIN
					RAISERROR('upUpdateEmergencyContact - @PhoneNum must not be blank', 16, 1)
					RETURN
				END
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET PhoneNum = ''' + @PhoneNum + ''''
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',PhoneNum = ''' + @PhoneNum + ''''
		END

	-- RelationshipDesc
	IF (@UpdRelationshipDesc = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET RelationshipDesc = ' + CASE WHEN LEN(RTRIM(LTRIM(@RelationshipDesc))) = 0 THEN 'null' ELSE '''' + @RelationshipDesc + '''' END
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',RelationshipDesc = ' + CASE WHEN LEN(RTRIM(LTRIM(@RelationshipDesc))) = 0 THEN 'null' ELSE '''' + @RelationshipDesc + '''' END

	-- IDDCode
	IF (@UpdIDDCode = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET IDDCode = ' + CASE WHEN LEN(RTRIM(LTRIM(@IDDCode))) = 0 THEN 'null' ELSE '''' + @IDDCode + '''' END
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',IDDCode = ' + CASE WHEN LEN(RTRIM(LTRIM(@IDDCode))) = 0 THEN 'null' ELSE '''' + @IDDCode + '''' END

	-- CityAreaCode
	IF (@UpdCityAreaCode = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET CityAreaCode = ' + CASE WHEN LEN(RTRIM(LTRIM(@CityAreaCode))) = 0 THEN 'null' ELSE '''' + @CityAreaCode + '''' END
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',CityAreaCode = ' + CASE WHEN LEN(RTRIM(LTRIM(@CityAreaCode))) = 0 THEN 'null' ELSE '''' + @CityAreaCode + '''' END

	-- Extension
	IF (@UpdExtension = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET Extension = ' + CASE WHEN LEN(RTRIM(LTRIM(@Extension))) = 0 THEN 'null' ELSE '''' + @Extension + '''' END
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',Extension = ' + CASE WHEN LEN(RTRIM(LTRIM(@Extension))) = 0 THEN 'null' ELSE '''' + @Extension + '''' END

	-- Update record in EmergencyContact if necessary
	IF (LEN(@FieldsToUpdate) > 0)
		BEGIN
			-- Added update timestamp
			SET @FieldsToUpdate = @FieldsToUpdate + ',UpdateDateTime = ''' + @DateTimeStamp + ''''
			EXECUTE ('UPDATE dbo.tbEmergencyContact' + @FieldsToUpdate + ' WHERE EmergencyContactID = ' + @EmergencyContactID + ' AND UserAccountID = ' + @UserAccountID)
		END
	ELSE
		BEGIN
				RAISERROR('upUpdateEmergencyContact - no fields were specified for update', 16, 1)
				RETURN
		END

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upUpdateEmergencyContact - emergency contact was NOT successfully updated', 16, 1)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upUpdateEmergencyContact
TO system