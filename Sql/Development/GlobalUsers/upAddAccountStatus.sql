USE [GlobalUsers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June , 2006
-- Description:	Add an account status
-- =============================================
ALTER PROCEDURE dbo.upAddAccountStatus 
	@Result char(1) OUTPUT, -- (S)uccess or (F)ailure
	@AccountStatus char(1),
	@Description	varchar(40)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'

	-- Insert new record into tbAccountStatus
	INSERT INTO dbo.tbAccountStatus (AccountStatus, Description)
	VALUES (@AccountStatus, @Description)

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upAddAccountStatus - account status was NOT successfully added', 16, 1)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upAddAccountStatus
TO system