USE [GlobalUsersNew]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 8, 2006
-- Description:	Update an email address for a user account
-- =============================================
ALTER PROCEDURE dbo.upUpdateEmailAddress 
	@Result char(1) OUTPUT, -- (S)uccess or (F)ailure
	@EmailAddressID varchar(40),
	@UserAccountID varchar(40),
	@UpdEMailAddressType bit = 0,
	@EMailAddressType char(1) = '',  -- (U)nknown, (O)ther, (M)obile, (W)ork or (H)ome
	@UpdEmailAddress bit = 0,
	@EmailAddress	varchar(120) = '',
	@UpdIsPreferred bit = 0,
	@IsPreferred bit = 0
AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp varchar(40)
	DECLARE @FieldsToUpdate varchar(8000)

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CONVERT(VARCHAR, CURRENT_TIMESTAMP, 121)
	SET @FieldsToUpdate = ''
	SET @Result = 'F'

	-- ***** Check Inputs *****
	-- 
	-- EMailAddressType
	IF (@UpdEMailAddressType = 1)
		BEGIN
			SET @EMailAddressType = UPPER(@EMailAddressType)
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET EMailAddressType = ''' + @EMailAddressType + ''''
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',EMailAddressType = ''' + @EMailAddressType + ''''
		END

	-- EmailAddress
	IF (@UpdEmailAddress = 1)
		BEGIN
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET EmailAddress = ''' + @EmailAddress + ''''
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',EmailAddress = ''' + @EmailAddress + ''''
		END

	-- IsPreferred
	IF (@UpdIsPreferred = 1)
		BEGIN
			SET @EMailAddressType = UPPER(@EMailAddressType)
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET IsPreferred = ' + CONVERT(VARCHAR, @IsPreferred)
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',IsPreferred = ' + CONVERT(VARCHAR, @IsPreferred)
		END

	-- Update record in tbEmailAddress if necessary
	IF (LEN(@FieldsToUpdate) > 0)
		BEGIN
			-- Added update timestamp
			SET @FieldsToUpdate = @FieldsToUpdate + ',UpdateDateTime = ''' + @DateTimeStamp + ''''

			-- If IsPreferred, then reset all other phone numbers to not preferred
			IF (@UpdIsPreferred = 1 AND @IsPreferred = 1)
				UPDATE dbo.tbEmailAddress SET IsPreferred = 0 WHERE UserAccountID = @UserAccountID

			EXECUTE ('UPDATE dbo.tbEmailAddress' + @FieldsToUpdate + ' WHERE EmailAddressID = ' + @EmailAddressID + ' AND UserAccountID = ' + @UserAccountID)
		END
	ELSE
		BEGIN
				RAISERROR('upUpdateEmailAddress - no fields were specified for update', 16, 1)
				RETURN
		END

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upUpdateEmailAddress - email address was NOT successfully updated', 16, 1)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF