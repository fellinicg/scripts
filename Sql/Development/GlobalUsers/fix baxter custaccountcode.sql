ALTER TABLE tbUserAccount DISABLE TRIGGER tuUserAccount
GO
UPDATE dbo.tbUserAccount SET CustAccountCode = 
CASE len(CustAccountCode)
	WHEN 1 THEN '00000' + CONVERT(varchar, CustAccountCode)
	WHEN 2 THEN '0000' + CONVERT(varchar, CustAccountCode)
	WHEN 3 THEN '000' + CONVERT(varchar, CustAccountCode)
	WHEN 4 THEN '00' + CONVERT(varchar, CustAccountCode)
	WHEN 5 THEN '0' + CONVERT(varchar, CustAccountCode)
	ELSE CustAccountCode
END
WHERE ClientID = 22
GO
ALTER TABLE tbUserAccount ENABLE TRIGGER tuUserAccount
