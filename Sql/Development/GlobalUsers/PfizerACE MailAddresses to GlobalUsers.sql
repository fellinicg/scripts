-- MAIL ADDRESSES
ALTER TABLE tbMailAddress DISABLE TRIGGER tiMailAddress
GO
SET IDENTITY_INSERT tbMailAddress ON
GO

INSERT tbMailAddress
(
	MailAddressID,
	UserAccountID,
	MailAddressType,
	Line1,
	Line2,
	Line3,
	Line4,
	CityTown,
	StateProvince,
	PostalCode,
	Country,
	UseCityLine,
	IsPreferred,
	CreateDateTime,
	UpdateDateTime
)
SELECT 
	ad_ID AS MailAddressID,
	ad_gu_ID AS UserAccountID,
	CASE ad_ct_ID
		WHEN 5 THEN 'H'
		WHEN 7 THEN 'W'
		ELSE 'O'
	END AS MailAddressType,
	ad_StreetAddr1 AS Line1,
	ad_StreetAddr2 AS Line2,
	ad_StreetAddr3 AS Line3,
	ad_StreetAddr4 AS Line4,
	ad_City AS CityTown,
	ad_State AS StateProvince,
	ad_PostalCode AS PostalCode,
	CASE ad_Country
		WHEN 'US' THEN 'USA'
		WHEN '' THEN NULL
	END AS Country,
	0 AS UseCityLine,
	ad_Default AS IsPreferred,
	ad_PostDate AS CreateDateTime,
	CURRENT_TIMESTAMP AS UpdateDateTime
FROM FastRewards.dbo.Users
LEFT JOIN GlobalUser.dbo.GlobalUsers on u_id = gu_id
INNER JOIN GlobalUser.dbo.Addresses ON gu_ID = ad_gu_ID
WHERE gu_cl_ID = 3
GO
SET IDENTITY_INSERT tbMailAddress OFF
GO
ALTER TABLE tbMailAddress ENABLE TRIGGER tiMailAddress
GO

	-- CHECK RESULTS (ONLY ONE PREFERRED MAIL ADDRESS)
IF (EXISTS (SELECT UserAccountID FROM tbMailAddress WHERE IsPreferred = 1 GROUP BY UserAccountID HAVING COUNT(*) > 1))
BEGIN
	PRINT 'DUPLICATE PREFERRED MAIL ADDRESSES'
	RETURN
END
