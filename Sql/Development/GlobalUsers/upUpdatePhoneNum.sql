USE [GlobalUsers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 9, 2006
-- Description:	Update a phone number for a user account
-- =============================================
ALTER PROCEDURE dbo.upUpdatePhoneNum 
	@Result char(1) OUTPUT, -- (S)uccess or (F)ailure
	@PhoneNumID varchar(40),
	@UserAccountID varchar(40),
	@UpdPhoneNumType  bit = 0,
	@PhoneNumType char(1) = '',  -- (U)nknown, (O)ther, (M)obile, (W)ork or (H)ome
	@UpdIDDCode   bit = 0,
	@IDDCode varchar(10) = '',
	@UpdCityAreaCode bit = 0,
	@CityAreaCode varchar(10) = '',
	@UpdPhoneNum  bit = 0,
	@PhoneNum varchar(20) = '',
	@UpdExtension  bit = 0,
	@Extension varchar(10) = '',
	@UpdOrigPhoneNum  bit = 0,
	@OrigPhoneNum varchar(20) = '',
	@UpdTxtMsgCapable  bit = 0,
	@TxtMsgCapable bit = 0,
	@UpdIsPreferred bit = 0,
	@IsPreferred bit = 0
AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp varchar(40)
	DECLARE @FieldsToUpdate varchar(8000)

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CONVERT(VARCHAR, CURRENT_TIMESTAMP, 121)
	SET @FieldsToUpdate = ''
	SET @Result = 'F'

	-- ***** Check Inputs *****
	-- 
	-- PhoneNumType
	IF (@UpdPhoneNumType = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET PhoneNumType = ' + CASE WHEN LEN(RTRIM(LTRIM(@PhoneNumType))) = 0 THEN 'null' ELSE '''' + UPPER(@PhoneNumType) + '''' END
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',PhoneNumType = ' + CASE WHEN LEN(RTRIM(LTRIM(@PhoneNumType))) = 0 THEN 'null' ELSE '''' + UPPER(@PhoneNumType) + '''' END

	-- IDDCode
	IF (@UpdIDDCode = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET IDDCode = ' + CASE WHEN LEN(RTRIM(LTRIM(@IDDCode))) = 0 THEN 'null' ELSE '''' + @IDDCode + '''' END
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',IDDCode = ' + CASE WHEN LEN(RTRIM(LTRIM(@IDDCode))) = 0 THEN 'null' ELSE '''' + @IDDCode + '''' END

	-- CityAreaCode
	IF (@UpdCityAreaCode = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET CityAreaCode = ' + CASE WHEN LEN(RTRIM(LTRIM(@CityAreaCode))) = 0 THEN 'null' ELSE '''' + @CityAreaCode + '''' END
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',CityAreaCode = ' + CASE WHEN LEN(RTRIM(LTRIM(@CityAreaCode))) = 0 THEN 'null' ELSE '''' + @CityAreaCode + '''' END

	-- PhoneNum
	IF (@UpdPhoneNum = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET PhoneNum = ' + CASE WHEN LEN(RTRIM(LTRIM(@PhoneNum))) = 0 THEN 'null' ELSE '''' + @PhoneNum + '''' END
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',PhoneNum = ' + CASE WHEN LEN(RTRIM(LTRIM(@PhoneNum))) = 0 THEN 'null' ELSE '''' + @PhoneNum + '''' END

	-- Extension
	IF (@UpdExtension = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET Extension = ' + CASE WHEN LEN(RTRIM(LTRIM(@Extension))) = 0 THEN 'null' ELSE '''' + @Extension + '''' END
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',Extension = ' + CASE WHEN LEN(RTRIM(LTRIM(@Extension))) = 0 THEN 'null' ELSE '''' + @Extension + '''' END

	-- OrigPhoneNum
	IF (@UpdOrigPhoneNum = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET OrigPhoneNum = ' + CASE WHEN LEN(RTRIM(LTRIM(@OrigPhoneNum))) = 0 THEN 'null' ELSE '''' + @OrigPhoneNum + '''' END
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',OrigPhoneNum = ' + CASE WHEN LEN(RTRIM(LTRIM(@OrigPhoneNum))) = 0 THEN 'null' ELSE '''' + @OrigPhoneNum + '''' END

	-- TxtMsgCapable
	IF (@UpdTxtMsgCapable = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET TxtMsgCapable = ' + CONVERT(VARCHAR, @TxtMsgCapable)
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',TxtMsgCapable = ' + CONVERT(VARCHAR, @TxtMsgCapable)

	-- IsPreferred
	IF (@UpdIsPreferred = 1)
		IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET IsPreferred = ' + CONVERT(VARCHAR, @IsPreferred)
		ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',IsPreferred = ' + CONVERT(VARCHAR, @IsPreferred)

	-- Update record in tbPhoneNum if necessary
	IF (LEN(@FieldsToUpdate) > 0)
		BEGIN
			-- If IsPreferred, then reset all other phone numbers to not preferred
			IF (@UpdIsPreferred = 1 AND @IsPreferred = 1)
				UPDATE dbo.tbPhoneNum SET IsPreferred = 0 WHERE UserAccountID = @UserAccountID

			-- Add update timestamp
			SET @FieldsToUpdate = @FieldsToUpdate + ',UpdateDateTime = ''' + @DateTimeStamp + ''''
			EXECUTE ('UPDATE dbo.tbPhoneNum' + @FieldsToUpdate + ' WHERE PhoneNumID = ' + @PhoneNumID + ' AND UserAccountID = ' + @UserAccountID)
		END
	ELSE
		BEGIN
				RAISERROR('upAddPhoneNum - no fields were specified for update', 16, 1)
				RETURN
		END

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upAddPhoneNum - phone number was NOT successfully updated', 16, 1)

END


GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upAddPhoneNum
TO system