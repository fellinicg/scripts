USE [GlobalUsers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 16, 2005
-- Description:	Return full name
-- =============================================
CREATE FUNCTION dbo.ufGetPersonsFullName 
(
	@UserAccountID int
)
RETURNS varchar(200)
AS
BEGIN
	DECLARE @Result varchar(200)
 
	SET @Result = 'Unknown'

	SELECT @Result = ISNULL(PersonBuiltFullName, ISNULL(PersonFirstGivenName + ' ' + PersonFamilyName, ISNULL(AccountName, '')))
	FROM dbo.tbUserAccount
	WHERE UserAccountID = @UserAccountID

	RETURN @Result

END

