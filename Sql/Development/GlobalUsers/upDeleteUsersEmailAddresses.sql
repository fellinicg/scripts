USE [GlobalUsers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

-- =============================================
-- Author:			Todd Fellini
-- Create date: June 23, 2006
-- Description:	Delete all email addresses for a user account
-- =============================================
ALTER PROCEDURE [dbo].[upDeleteUsersEmailAddresses] 
	@Result char(1) OUTPUT, -- (S)uccess or (F)ailure
	@UserAccountID int,
	@EMailAddressType char(1) = '%'
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'

	-- Delete records in tbEmailAddress, if there are any
	IF EXISTS (SELECT 1	FROM dbo.tbEmailAddress WHERE UserAccountID = @UserAccountID 
						AND EMailAddressType LIKE @EMailAddressType)
		BEGIN
			DELETE FROM dbo.tbEmailAddress
			WHERE UserAccountID = @UserAccountID
			AND EMailAddressType LIKE @EMailAddressType

			-- Verify success
			IF (@@ROWCOUNT <> 0)
				SET @Result = 'S'
			ELSE
				RAISERROR('upDeleteUsersEmailAddresses - email addresses were NOT successfully deleted', 16, 1)
		END
	ELSE SET @Result = 'S'

END
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
GRANT EXECUTE
ON dbo.upDeleteEmailAddress
TO system