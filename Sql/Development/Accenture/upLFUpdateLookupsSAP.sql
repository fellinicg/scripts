-- ============================================================
-- OrigAuthorFullName: Jason McElroy
-- OrigCreateDate: 20061023
-- LastEditedByFullName: Jason McElroy
-- LastEditDate: 20061024
-- ============================================================
CREATE PROCEDURE dbo.upLFUpdateLookupsSAP
	@LoadFileID int,
	@LoadFileStatus char(1) OUTPUT
AS

	SET NOCOUNT ON


	-- CONSTANTS
	DECLARE @LFStatError char(1)
	DECLARE @LFStatValidated char(1)
	DECLARE @LFStatUpdated char(1)
	DECLARE @LFTypeSAP char(1)

	SET @LFStatError = 'E'
	SET @LFStatValidated = 'V'
	SET @LFStatUpdated = 'U'
	SET @LFTypeSAP = 'S'


	SET @LoadFileStatus = @LFStatError


	-- VALID CONDITIONS
	IF (NOT EXISTS (SELECT 1 FROM tbLFLoadFile WHERE LoadFileID = @LoadFileID AND LoadFileType = @LFTypeSAP AND Status = @LFStatValidated))
	BEGIN
		RAISERROR('upLFUpdateLookupsSAP - @LoadFileID supplied does not exist, is not an SAP file, or has status other than (V)alidated', 16, 1)
		RETURN
	END

	IF (NOT EXISTS (SELECT 1 FROM tbLFStagingSAP) OR EXISTS (SELECT 1 FROM tbLFStagingSAP WHERE LoadFileID <> @LoadFileID))
	BEGIN
		RAISERROR('upLFUpdateLookupsSAP - tbLFStagingSAP is empty or contains records for another LoadFileID', 16, 1)
		RETURN
	END


	-- ================================================================================================================================
	-- 20061024 JMC
	--		Business rule is for a description value to be chosen at random in cases where more than one description occurs for the
	--		same code. This is why the MIN() aggregate function is used when updating and creating lookup table description values.
	-- ================================================================================================================================


	-- COUNTRY: UPDATES
	DECLARE @UpdlookUps TABLE (Code varchar(20), Info varchar(40))

	INSERT @UpdlookUps(Code, Info)
	SELECT l.CountryKey, MIN(l.CountryNm)
	FROM tbCountry c
		INNER JOIN tbLFStagingSAP l ON (c.CountryCode = l.CountryKey)
	WHERE c.CountryName <> l.CountryNm
		AND l.RejectReason IS NULL
	GROUP BY l.CountryKey

	IF (@@ROWCOUNT > 0)
		UPDATE tbCountry SET CountryName = u.Info, UpdateDate = DEFAULT
		FROM tbCountry c
			INNER JOIN @UpdlookUps u ON (c.CountryCode = u.Code)


	-- COUNTRY: INSERTS
	INSERT tbCountry(CountryCode, CountryName)
	SELECT l.CountryKey, MIN(l.CountryNm)
	FROM tbLFStagingSAP l
		LEFT OUTER JOIN tbCountry c ON (l.CountryKey = c.CountryCode)
	WHERE c.CountryCode IS NULL
		AND l.RejectReason IS NULL
	GROUP BY l.CountryKey


	-- COST CENTER: UPDATES
	DELETE FROM @UpdlookUps

	INSERT @UpdlookUps(Code, Info)
	SELECT l.CostCenterNbr, MIN(l.CostCenterDescr)
	FROM dbo.tbCostCenter c
		INNER JOIN dbo.tbLFStagingSAP l ON (c.CostCtrCode = l.CostCenterNbr)
	WHERE c.CostCtrDesc <> l.CostCenterDescr
		AND l.RejectReason IS NULL
	GROUP BY l.CostCenterNbr

	IF (@@ROWCOUNT > 0)
		UPDATE dbo.tbCostCenter SET CostCtrDesc = u.Info, UpdateDate = DEFAULT
		FROM dbo.tbCostCenter c
			INNER JOIN @UpdlookUps u ON (c.CostCenterNbr = u.Code)


	-- COST CENTER: INSERTS
	INSERT dbo.tbCostCenter(CostCtrCode, CostCtrDesc)
	SELECT l.CostCenterNbr, MIN(l.CostCenterDescr)
	FROM dbo.tbLFStagingSAP l
		LEFT OUTER JOIN dbo.tbCostCenter c ON (l.CostCenterNbr = c.CostCtrCode)
	WHERE c.CostCtrCode IS NULL
		AND l.RejectReason IS NULL
	GROUP BY l.CostCenterNbr

	-- COMPANY: UPDATES
	DELETE FROM @UpdlookUps

	INSERT @UpdlookUps(Code, Info)
	SELECT l.CompanyCd, MIN(l.CompanyDescr)
	FROM dbo.tbCompany c
		INNER JOIN dbo.tbLFStagingSAP l ON (c.CompanyCode = l.CompanyCd)
	WHERE c.CompanyDesc <> l.CompanyDescr
		AND l.RejectReason IS NULL
	GROUP BY l.CompanyCd

	IF (@@ROWCOUNT > 0)
		UPDATE dbo.tbCompany SET CompanyDesc = u.Info, UpdateDate = DEFAULT
		FROM dbo.tbCompany c
			INNER JOIN @UpdlookUps u ON (c.CompanyCode = u.Code)


	-- COMPANY: INSERTS
	INSERT dbo.tbCompany(CompanyCode, CompanyDesc)
	SELECT l.CompanyCd, MIN(l.CompanyDescr)
	FROM dbo.tbLFStagingSAP l
		LEFT OUTER JOIN dbo.tbCompany c ON (l.CompanyCd = c.CompanyCode)
	WHERE c.CompanyCode IS NULL
		AND l.RejectReason IS NULL
	GROUP BY l.CompanyCd


	-- FACILITY: UPDATES
	DELETE FROM @UpdlookUps

	INSERT @UpdlookUps(Code, Info)
	SELECT l.FacilityCd, MIN(l.FacilityDescr)
	FROM dbo.tbFacility f
		INNER JOIN dbo.tbLFStagingSAP l ON (f.FacilityCode = l.FacilityCd)
	WHERE f.FacilityDesc <> l.FacilityDescr
		AND l.RejectReason IS NULL
	GROUP BY l.FacilityCd

	IF (@@ROWCOUNT > 0)
		UPDATE dbo.tbFacility SET FacilityDesc = u.Info, UpdateDate = DEFAULT
		FROM dbo.tbFacility f
			INNER JOIN @UpdlookUps u ON (f.FacilityCode = u.Code)


	-- FACILITY: INSERTS
	INSERT dbo.tbFacility(FacilityCode, FacilityDesc)
	SELECT l.FacilityCd, MIN(l.FacilityDescr)
	FROM dbo.tbLFStagingSAP l
		LEFT OUTER JOIN dbo.tbFacility f ON (l.FacilityCd = f.FacilityCode)
	WHERE f.FacilityCode IS NULL
		AND l.RejectReason IS NULL
	GROUP BY l.FacilityCd


	-- GEO UNIT: UPDATES
	DELETE FROM @UpdlookUps

	INSERT @UpdlookUps(Code, Info)
	SELECT l.GeographicUnitCd, MIN(l.GeographicUnitDescr)
	FROM dbo.tbGeoUnit g
		INNER JOIN dbo.tbLFStagingSAP l ON (g.GeoUnitCode = l.GeographicUnitCd)
	WHERE g.GeoUnitDesc <> l.GeographicUnitDescr
		AND l.RejectReason IS NULL
	GROUP BY l.GeographicUnitCd

	IF (@@ROWCOUNT > 0)
		UPDATE dbo.tbGeoUnit SET GeographicUnitDescr = u.Info, UpdateDate = DEFAULT
		FROM dbo.tbGeoUnit g
			INNER JOIN @UpdlookUps u ON (g.GeoUnitCode = u.Code)


	-- GEO UNIT: INSERTS
	INSERT dbo.tbGeoUnit(GeoUnitCode, GeoUnitDesc)
	SELECT l.GeographicUnitCd, MIN(l.GeographicUnitDescr)
	FROM dbo.tbLFStagingSAP l
		LEFT OUTER JOIN dbo.tbGeoUnit g ON (l.GeographicUnitCd = g.GeoUnitCode)
	WHERE g.GeoUnitCode IS NULL
		AND l.RejectReason IS NULL
	GROUP BY l.GeographicUnitCd


	-- CAPABILITY: UPDATES
	DELETE FROM @UpdlookUps

	INSERT @UpdlookUps(Code, Info)
	SELECT l.CapabilityCd, MIN(l.CapabilityDescr)
	FROM dbo.tbCapability c
		INNER JOIN dbo.tbLFStagingSAP l ON (c.CapabilityCode = l.CapabilityCd)
	WHERE c.CapabilityDesc <> l.CapabilityDescr
		AND l.RejectReason IS NULL
	GROUP BY l.CapabilityCd

	IF (@@ROWCOUNT > 0)
		UPDATE dbo.tbCapability SET CapabilityDesc = u.Info, UpdateDate = DEFAULT
		FROM dbo.tbCapability c
			INNER JOIN @UpdlookUps u ON (c.CapabilityCode = u.Code)


	-- CAPABILITY: INSERTS
	INSERT dbo.tbCapability(CapabilityCode, CapabilityDesc)
	SELECT l.CapabilityCd, MIN(l.CapabilityDescr)
	FROM dbo.tbLFStagingSAP l
		LEFT OUTER JOIN dbo.tbCapability c ON (l.CapabilityCd = c.CapabilityCode)
	WHERE c.CapabilityCode IS NULL
		AND l.RejectReason IS NULL
	GROUP BY l.CapabilityCd


	-- CUST EMP STAT: UPDATES
	DELETE FROM @UpdlookUps

	INSERT @UpdlookUps(Code, Info)
	SELECT l.EmploymentStatusCd, MIN(l.EmploymentStatusDescr)
	FROM dbo.tbCustEmpStat c
		INNER JOIN dbo.tbLFStagingSAP l ON (c.CustEmpStatCode = l.EmploymentStatusCd)
	WHERE c.CustEmpStatDesc <> l.EmploymentStatusDescr
		AND l.RejectReason IS NULL
	GROUP BY l.EmploymentStatusCd

	IF (@@ROWCOUNT > 0)
		UPDATE dbo.tbCustEmpStat SET CustEmpStatDesc = u.Info, UpdateDate = DEFAULT
		FROM dbo.tbCustEmpStat c
			INNER JOIN @UpdlookUps u ON (c.CustEmpStatCode = u.Code)


	-- CUST EMP STAT: INSERTS
	INSERT dbo.tbCustEmpStat(CustEmpStatCode, CustEmpStatDesc)
	SELECT l.EmploymentStatusCd, MIN(l.EmploymentStatusDescr)
	FROM dbo.tbLFStagingSAP l
		LEFT OUTER JOIN dbo.tbCustEmpStat c ON (l.EmploymentStatusCd = c.CustEmpStatCode)
	WHERE c.CustEmpStatCode IS NULL
		AND l.RejectReason IS NULL
	GROUP BY l.EmploymentStatusCd


	-- WORK FORCE GROUP: UPDATES
	DELETE FROM @UpdlookUps



	-- WORK FORCE GROUP: INSERTS













	-- EVALUATE RESULTS
		SET @LoadFileStatus = @LFStatUpdated


	-- UPDATE RECORD
	UPDATE tbLFLoadFile SET Status = @LoadFileStatus, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID


	SET NOCOUNT OFF
