USE Accenture
--
--TRUNCATE TABLE dbo.tbStagingPerfFactor
--
--EXEC ('INSERT INTO dbo.tbStagingPerfFactor
--SELECT * FROM OPENROWSET(''Microsoft.Jet.OLEDB.4.0'', 
--''Excel 8.0;DATABASE=d:\Accenture_PerfFactors.xls'', ''select * from [ENTERPRISE$]'')' )

INSERT INTO dbo.tbPerfFactor (StdWFGroupID, JobCode, PerfFactorTitle, PerfFactorDesc, DisplayOrder)
SELECT 
	StdWFGroupID,
	JobCode,
	FactorTitle,
	ISNULL(FactorDesc, ''),
	1
FROM dbo.tbStagingPerfFactor 
INNER JOIN dbo.tbJob ON JobCodeDesc = JobDesc
INNER JOIN dbo.tbSTDWFGroup ON GroupDesc = StdWFGroupDesc

SELECT * FROM dbo.tbPerfFactor