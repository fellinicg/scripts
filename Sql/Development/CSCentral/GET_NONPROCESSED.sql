USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE GET_NONPROCESSED
AS

SELECT 
F.ID,
F.PONUM,
RTrim(LTrim(dbo.FN_FIXFIELD(F.FULLNAME))) as FULLNAME,
RTrim(LTrim(dbo.FN_FIXFIELD(F.COMPANY))) as COMPANY,
RTrim(LTrim(dbo.FN_FIXFIELD(F.ADDRESS1))) as ADDRESS1,
RTrim(LTrim(dbo.FN_FIXFIELD(F.ADDRESS2))) as ADDRESS2,
RTrim(LTrim(dbo.FN_FIXFIELD(F.ADDRESS3))) as ADDRESS3,
RTrim(LTrim(dbo.FN_FIXFIELD(F.ADDRESS4))) as ADDRESS4,
RTrim(LTrim(dbo.FN_FIXFIELD(F.CITY))) as CITY,
RTrim(LTrim(dbo.FN_FIXFIELD(F.STATE))) as STATE,
RTrim(LTrim('"'+ F.ZIP + '"')) as ZIP,
RTrim(LTrim(DBO.FN_FIXPHONE(F.PHONE))) as PHONE,
RTrim(LTrim(dbo.FN_FIXFIELD(F.COUNTRY))) as COUNTRY,
RTrim(LTrim(F.SVCCODE)) as SVCCODE,
RTrim(LTrim(F.WEIGHT)) as WEIGHT,
RTrim(LTrim(dbo.FN_FIXFIELD(F.ITEMLINEID))) as ITEMLINEID,
RTrim(LTrim(dbo.FN_FIXFIELD(F.DKNUM))) as DKNUM,
RTrim(LTrim(DBO.FN_FIXEMAIL(F.EMAIL))) AS EMAIL,
'' as BLANK,
F.BILLCODE,
F.BILLACOUNTNUM,
F.SAT_DELIVERY,
F.DECLAREDVALUE,
'' as BLANK2,
'Y' as SIGNATURE,
dbo.ISRESIDENTIAL(F.COMPANY) AS RESIDENTIAL,
RTrim(LTrim(F.SENDERNAME)) as SENDERNAME
FROM FEDEX F 
LEFT JOIN grp_ShipUSPS u ON F.Programid = u.Programid
WHERE F.PROCESSED='0' 
AND u.PROGRAMID IS NULL
AND ISNULL(ProgramItemType, 'G') = 'G'
ORDER BY F.PONUM

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF