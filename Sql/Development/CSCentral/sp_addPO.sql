USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE sp_addPO @PONum int, @sendername varchar(50)=''
as

set @sendername='Marie Varacchi'

if Exists(Select * from ItemLine where PONumber = @PONum)
	Begin

	INSERT INTO Fedex
		(PONum, DKNum, FullName, Address1, Address2, City, State, Zip, Phone, ItemLineID, SenderName, DeclaredValue, DeclaredValueOrig, Email, ProgramID, ProgramItemType )
	
	SELECT IL.PONumber, 
		OH.ProjectNumber, 
		LEFT(Replace(OH.ShipName,',',''),50) Shipname, 
		LEFT(Replace(OH.ShipAddress1,',',''),50) Addr1, 
		LEFT(Replace(OH.ShipAddress2,',',''),50) Addr2, 
		LEFT(Replace(OH.ShipCity,',',''),50) City, 
		OH.ShipState State, 
		LEFT(OH.ShipPostal,5) Zip, 
		LEFT(Replace(Left(OH.ShipPhone,14),',',''),14) Phone, 
		IL.poline,
		@sendername SenderName,
		CASE WHEN AL.Quantity*IL.CostPerUnit>15000 then 15000 else AL.Quantity*IL.CostPerUnit end 'DeclValue',
		CASE WHEN AL.Quantity*IL.CostPerUnit>15000 then 15000 else AL.Quantity*IL.CostPerUnit end 'DeclValue', 
		LEFT (OH.EMAIL,50),
		PIT.pit_programID,
		PIT.pit_ItemType
	FROM OrderHeader OH 
	JOIN AwardLine AL ON OH.OrderID = AL.OrderID 
	JOIN ItemLine IL ON AL.AwardLineID = IL.AwardLineID
	LEFT JOIN SQL2KPROD.Catalog.dbo.Programs P ON OH.ProgramCode = P.p_Code 
	LEFT JOIN SQL2KPROD.Catalog.dbo.ProgramItemTypes PIT ON P.p_ID = PIT.pit_programID AND IL.ItemType = PIT.pit_ItemType 
	WHERE IL.PONumber = @PONum 
	AND AL.Void <> 1 
	ORDER BY il.poline

	select 'OK'
	End
else	
	Begin
		Select 'PO# does not exist. Try again!'
	End
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
