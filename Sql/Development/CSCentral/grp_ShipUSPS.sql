USE [Fulfilment]

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[grp_ShipUSPS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[grp_ShipUSPS]
GO

CREATE TABLE [dbo].[grp_ShipUSPS] (
	[ProgramID] [int] NOT NULL 
) ON [PRIMARY]
GO

INSERT INTO grp_ShipUSPS (ProgramID) Values (182)
INSERT INTO grp_ShipUSPS (ProgramID) Values (202)

