USE [AbbottReports]
GO
/****** Object:  StoredProcedure [dbo].[reports_Award]    Script Date: 11/17/2005 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE reports_Award AS

delete from awardreport
delete from report_nextApprover


insert into awardreport
(
	awardee_u_ID, awardee_LASTNAME, awardee_FIRSTNAME, 
	awardee_RptsTo ,awardee_MI ,awardee_u_SSN, 
	awardee_TER_TYPE_CAT ,awardee_TERRITORY_NO,awardee_REGION ,awardee_DISTRICT, 
	awarder_u_ID ,awarder_LASTNAME ,awarder_FIRSTNAME ,awarder_MI ,awarder_u_SSN, 
	awarder_TER_TYPE_CAT ,awarder_TERRITORY_NO ,awarder_REGION,
	awarder_DISTRICT , awarder_RptsTo , budgetholder_u_ID,
	budgetholder_LASTNAME ,budgetholder_FIRSTNAME,
	budgetholder_MI ,budgetholder_u_SSN ,budgetholder_TER_TYPE_CAT,budgetholder_TERRITORY_NO ,
	budgetholder_REGION ,budgetholder_DISTRICT ,approver_u_ID ,approver_LASTNAME ,
	approver_FIRSTNAME ,approver_MI ,approver_u_SSN,approver_TER_TYPE_CAT ,approver_TERRITORY_NO ,
	approver_REGION ,approver_DISTRICT ,approver_EMAILID ,ph_ProgramID ,ph_id ,ph_Points,
	ph_Status, ph_pointsDate , ph_DateAdded ,ph_Category, ph_AwardCriteria , Cash,ph_PointsPeriod,pe_emailbody,
	pas_Description , DaysSinceLastStatus ,LastChangeDate , ApproverLastLogin,Pending , ClarityTransactionID , Spiff
)
select awardee.u_ID 'awardee u_ID', awardee.LASTNAME 'awardee LASTNAME', awardee.FIRSTNAME 'awardee FIRSTNAME', awardee.u_rptsto , awardee.MI 'awardee MI', awardee.u_SSN 'awardee u_SSN', awardee.TER_TYPE_CAT 'awardee TER_TYPE_CAT', awardee.TERRITORY_NO 'awardee TERRITORY_NO', awardee.REGION 'awardee REGION', awardee.DISTRICT 'awardee DISTRICT',
awarder.u_ID 'awarder u_ID', awarder.LASTNAME 'awarder LASTNAME', awarder.FIRSTNAME 'awarder FIRSTNAME', awarder.MI 'awarder MI', awarder.u_SSN 'awarder u_SSN', awarder.TER_TYPE_CAT 'awarder TER_TYPE_CAT', awarder.TERRITORY_NO 'awarder TERRITORY_NO', awarder.REGION 'awarder REGION', awarder.DISTRICT 'awarder DISTRICT', awarder.u_rptsTo 'awarder_RptsTo' , 
budgetholder.u_ID 'budgetholder.u_ID', budgetholder.LASTNAME 'budgetholder LASTNAME', budgetholder.FIRSTNAME 'budgetholder FIRSTNAME', budgetholder.MI 'budgetholder MI', budgetholder.u_SSN 'budgetholder u_SSN', budgetholder.TER_TYPE_CAT 'budgetholder TER_TYPE_CAT', budgetholder.TERRITORY_NO 'budgetholder TERRITORY_NO', budgetholder.REGION 'budgetholder REGION', budgetholder.DISTRICT 'budgetholder DISTRICT',
approver.u_ID 'approver u_ID', approver.LASTNAME 'approver LASTNAME', approver.FIRSTNAME 'approver FIRSTNAME', approver.MI 'approver MI', approver.u_SSN 'approver u_SSN', approver.TER_TYPE_CAT 'approver TER_TYPE_CAT', approver.TERRITORY_NO 'approver TERRITORY_NO', approver.REGION 'approver REGION', approver.DISTRICT 'approver DISTRICT', approver.Emailid 'approver EMAILID',
ph_ProgramID, ph_id, ph_Points, ph_Status , ph_pointsDate , ph_DateAdded,ph_Category, ph_Commentary, Case ph_Description When 'Cash' Then 1 Else 0 End Cash, ph_PointsPeriod, pe_emailbody,pas_Description, Null ,null,l_LastLogin,1 , ph_TranID , isnull(ProgType,1)
from Moonraker.points.dbo.vw_PointsUnionHistory ph 
left join Moonraker.abbott.dbo.vw_usersall awardee on ph_userid = awardee.u_id and ph_dateadded between isnull(awardee.territory_strt_dt,'1/1/2000') and IsNull(awardee.territory_end_dt,'1/1/2010') 
left join Moonraker.abbott.dbo.vw_usersall  awarder on ph.ph_origin = awarder.u_id and ph_dateadded between isnull(awarder.territory_strt_dt,'1/1/2000') and IsNull(awarder.territory_end_dt,'1/1/2010') 
left join Moonraker.abbott.dbo.vw_usersall budgetholder on ph.ph_origincode = budgetholder.u_id and ph_dateadded between isnull(budgetholder.territory_strt_dt,'1/1/2000') and IsNull(budgetholder.territory_end_dt,'1/1/2010') 
left join Moonraker.abbott.dbo.vw_usersall approver on ph.ph_ModifiedBy = approver.u_id and ph_dateadded between isnull(approver.territory_strt_dt,'1/1/2000') and IsNull(approver.territory_end_dt,'1/1/2010')
left join Moonraker.points.dbo.pointsemail on ph_id = pe_p_id left join (select distinct pas_bdr_id,pas_statusid,pas_Description from Moonraker.points.dbo.pointapprovalscheme) pas on ph_status = pas_statusid and ph_pointsperiod = pas_bdr_id
left join Moonraker.abbott.dbo.Login on approver.u_id = l_u_id 
left join (select o_programId , 'ProgType' = 2 from Moonraker.abbott.dbo.Objects join Moonraker.abbott.dbo.objecttypeprogram on otp_o_id = o_id where o_type='P') PT on ph_ProgramID = o_programid

order by ph_id , ph_Dateadded 
--where ph_modifiedby <> ''



update awardreport set pending = 0 where pas_Description = 'Approved'

update awardreport set ProgramName = o_title from awardreport join (select o_title , o_programid from Moonraker.abbott.dbo.objects where o_type = 'P') O on ph_programid = o_programid




update awardreport set lastChangedate = a3.ph_dateAdded
from awardreport awardreport join
(
select a1.awardid ,a1.ph_id, a2.ph_dateAdded from
(select ph_id , ph_Dateadded ,awardid from awardreport) a1 
join
(select ph_id , ph_Dateadded ,awardid from awardreport) a2
on a1.ph_id = a2.ph_id and a1.awardid = a2.awardid + 1
) a3 on awardreport.awardid = a3.awardid and awardreport.ph_id = a3.ph_id


Update awardreport set DaysSinceLastStatus = datediff(d,LastChangeDate,ph_DateAdded)

update awardreport set CurrentRecord=1
from awardreport
join
(select max(awardid) as maxAwardId from awardreport
 group by ph_id
) a2 on awardid = maxAwardId

insert into Report_NextApprover
select distinct awardID, null , approver.u_id,approver.FirstName , approver.LastName , approver.ter_type_cat,
l_lastLogin , approver.emailid , approver.u_ssn  
from awardreport
join Moonraker.points.dbo.PointStatusPeriod on psp_bdr_id = ph_pointsPeriod and ph_status = psp_pas_statusid
join Moonraker.points.dbo.PonitStatusAccess on psp_id = psa_psp_id
join Moonraker.abbott.dbo.vw_UserObjects approver on oa_id = psa_approver_oa_id
left join Moonraker.abbott.dbo.login on l_u_id = approver.u_id
where ph_programid <> 210 and ph_status not in (10,11,18)

insert into Report_NextApprover
select distinct awardID , null ,approver.u_id,approver.FirstName , approver.LastName , approver.ter_type_cat,
l_lastLogin ,approver.emailid , approver.u_ssn 
from awardreport
join Moonraker.points.dbo.PointStatusPeriod on psp_bdr_id = ph_pointsPeriod and ph_status = psp_pas_statusid
join Moonraker.points.dbo.PonitStatusAccess on psp_id = psa_psp_id
join Moonraker.abbott.dbo.vw_UserObjects approver on oa_id = psa_approver_oa_id and budgetholder_u_ID = approver.u_ID
left join Moonraker.abbott.dbo.login on l_u_id = approver.u_id
where ph_programid <> 210 and ph_status in (18)

insert into Report_NextApprover
select distinct awardID , null ,approver.u_id,approver.FirstName , approver.LastName , approver.ter_type_cat,
l_lastLogin ,approver.emailid , approver.u_ssn from awardreport
join Moonraker.points.dbo.PointStatusPeriod on psp_bdr_id = ph_pointsPeriod
and ph_status = psp_pas_statusid
join Moonraker.points.dbo.PonitStatusAccess on psp_id = psa_psp_id
join Moonraker.abbott.dbo.vw_UserObjects approver on oa_id = psa_approver_oa_id and awarder_rptsto = approver.PCN
left join Moonraker.abbott.dbo.login on l_u_id = approver.u_id
where ph_programid <> 210 and ph_status in (10,11)

insert into Report_NextApprover
select distinct awardID, null , approver.u_id,approver.FirstName , approver.LastName , approver.ter_type_cat,l_lastLogin , approver.emailid ,  approver.u_ssn
from awardreport join Moonraker.Points.dbo.PointStatusPeriod on psp_bdr_id = ph_pointsPeriod and ph_status = psp_pas_statusid
join Moonraker.Points.dbo.PonitStatusAccess on psp_id=psa_psp_id 
join Moonraker.abbott.dbo.vw_userobjects approver on psa_approver_oa_id = oa_id
join Moonraker.points.dbo.points on psp_pas_StatusId = p_status and psp_bdr_id=p_PointsPeriod
left join Moonraker.abbott.dbo.login on l_u_id = approver.u_id
where o_id=266 and ph_programid=210 and approver.ter_type_cat not in ('RM','SD','NSD') and approver.u_id not in(3380,3489)

insert into Report_NextApprover
select distinct awardID, null , approver.u_id,approver.FirstName , approver.LastName , approver.ter_type_cat,l_lastLogin ,approver.emailid ,  approver.u_ssn
from awardreport join Moonraker.Points.dbo.PointStatusPeriod on psp_bdr_id = ph_pointsPeriod and ph_status = psp_pas_statusid
join Moonraker.Points.dbo.PonitStatusAccess on psp_id=psa_psp_id 
join Moonraker.abbott.dbo.vw_userobjects approver on psa_approver_oa_id = oa_id and awarder_rptsto = approver.PCN
left join Moonraker.abbott.dbo.login on l_u_id = approver.u_id
where o_id=266 and  ph_programid = 210 and approver.ter_type_cat in ('RM','SD','NSD')


update Report_NextApprover set NextAppr_awardInitDate = ph_PointsDate from Report_NextApprover join awardreport on awardid =  NextAppr_awardId
--select * from awardreport
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF