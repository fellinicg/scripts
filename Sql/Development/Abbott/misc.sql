--		SELECT L01MGR FROM dbo.aims WHERE u_ID = 1

SELECT u_ID, 
CASE 0 
	WHEN LEN(L01MGR) then '1' 
	WHEN LEN(L02MGR) then '2' 
	WHEN LEN(L03MGR) then '3' 
	WHEN LEN(L04MGR) then '4' 
	WHEN LEN(L05MGR) then '5' 
	WHEN LEN(L06MGR) then '6' 
	WHEN LEN(L07MGR) then '7' 
	WHEN LEN(L08MGR) then '8' 
	WHEN LEN(L09MGR) then '9' 
	WHEN LEN(L10MGR) then '10' 
	WHEN LEN(L11MGR) then '11' 
	WHEN LEN(L12MGR) then '12' 
	WHEN LEN(L13MGR) then '13' 
	WHEN LEN(L14MGR) then '14' 
	WHEN LEN(L15MGR) then '15'
	ELSE -99
END
FROM dbo.aims
WHERE LEN(PCN) > 0
--select u_ID, dbo.ufGetEmployeeAIMSLevel(u_ID)
--from aims

=====================================================

SELECT PCN, Parent, Level, diff
FROM (
	SELECT src.PCN, src.L01MGR Parent, levels.Level, levels.Level diff FROM dbo.aims src INNER JOIN dbo.ufGetEmployeesAIMSLevel() levels ON src.u_ID = levels.u_ID
	UNION
	SELECT src.PCN, src.L02MGR Parent, levels.Level, levels.Level-1 diff FROM dbo.aims src INNER JOIN dbo.ufGetEmployeesAIMSLevel() levels ON src.u_ID = levels.u_ID
	UNION
	SELECT src.PCN, src.L03MGR Parent, levels.Level, levels.Level-2 diff FROM dbo.aims src INNER JOIN dbo.ufGetEmployeesAIMSLevel() levels ON src.u_ID = levels.u_ID
	UNION
	SELECT src.PCN, src.L04MGR Parent, levels.Level, levels.Level-3 diff FROM dbo.aims src INNER JOIN dbo.ufGetEmployeesAIMSLevel() levels ON src.u_ID = levels.u_ID
	UNION
	SELECT src.PCN, src.L05MGR Parent, levels.Level, levels.Level-4 diff FROM dbo.aims src INNER JOIN dbo.ufGetEmployeesAIMSLevel() levels ON src.u_ID = levels.u_ID
	UNION
	SELECT src.PCN, src.L06MGR Parent, levels.Level, levels.Level-5 diff FROM dbo.aims src INNER JOIN dbo.ufGetEmployeesAIMSLevel() levels ON src.u_ID = levels.u_ID
	UNION
	SELECT src.PCN, src.L07MGR Parent, levels.Level, levels.Level-6 diff FROM dbo.aims src INNER JOIN dbo.ufGetEmployeesAIMSLevel() levels ON src.u_ID = levels.u_ID
	UNION
	SELECT src.PCN, src.L08MGR Parent, levels.Level, levels.Level-7 diff FROM dbo.aims src INNER JOIN dbo.ufGetEmployeesAIMSLevel() levels ON src.u_ID = levels.u_ID
	UNION
	SELECT src.PCN, src.L09MGR Parent, levels.Level, levels.Level-8 diff FROM dbo.aims src INNER JOIN dbo.ufGetEmployeesAIMSLevel() levels ON src.u_ID = levels.u_ID
	UNION
	SELECT src.PCN, src.L10MGR Parent, levels.Level, levels.Level-9 diff FROM dbo.aims src INNER JOIN dbo.ufGetEmployeesAIMSLevel() levels ON src.u_ID = levels.u_ID
	UNION
	SELECT src.PCN, src.L11MGR Parent, levels.Level, levels.Level-10 diff FROM dbo.aims src INNER JOIN dbo.ufGetEmployeesAIMSLevel() levels ON src.u_ID = levels.u_ID
	UNION
	SELECT src.PCN, src.L12MGR Parent, levels.Level, levels.Level-11 diff FROM dbo.aims src INNER JOIN dbo.ufGetEmployeesAIMSLevel() levels ON src.u_ID = levels.u_ID
	UNION
	SELECT src.PCN, src.L13MGR Parent, levels.Level, levels.Level-12 diff FROM dbo.aims src INNER JOIN dbo.ufGetEmployeesAIMSLevel() levels ON src.u_ID = levels.u_ID
	UNION
	SELECT src.PCN, src.L14MGR Parent, levels.Level, levels.Level-13 diff FROM dbo.aims src INNER JOIN dbo.ufGetEmployeesAIMSLevel() levels ON src.u_ID = levels.u_ID
	UNION
	SELECT src.PCN, src.L15MGR Parent, levels.Level, levels.Level-14 diff FROM dbo.aims src INNER JOIN dbo.ufGetEmployeesAIMSLevel() levels ON src.u_ID = levels.u_ID
	UNION
	SELECT src.PCN, src.u_RPTSTO, levels.Level, 1 diff FROM dbo.aims src INNER JOIN dbo.ufGetEmployeesAIMSLevel() levels ON src.u_ID = levels.u_ID
) a
WHERE LEN(Parent) > 0
AND LEN(PCN) > 0
ORDER BY PCN, diff

===========================================================

select *
from aims
where pcn = 'ADLCAB033007'

select *
from aims
where pcn = 'PDLCAC032002'