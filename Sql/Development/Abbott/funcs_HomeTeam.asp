<!--#INCLUDE FILE="../connect.asp"-->
<!--#INCLUDE FILE="../adovbs.inc"-->
<%
'strDBServer="SQL2KDEV"
'strCatalog = "Abbott"
'strPointsDBServer = "SQL2KDEV"

strDBServer="Moonraker"
strCatalog = "Abbott"
strPointsDBServer = "Moonraker"

strConnstring =  "Driver={SQL Server}; Server=" & strDBServer & ";Database=" & strCatalog & "; uid=sa; pwd=;"

Sub CloseConnection
oConn.Close()
Set oConn = Nothing
End Sub

Function RegisterPoints ( nUserId, nPoints, nTeamId, nAddedByUserId)
'---------------------------------------------------------------------
'Register points in the abbott points database.
' -nUserId: The user you are assigning the points to.
' -nPoints: The amount of cash/points
' -nTeamId: The team id (primary key in tbHomeTeam)
' -nAddedByUserId: The user logged into the system ( session("UID") )
'---------------------------------------------------------------------
Set oConn = Server.CreateObject("Adodb.Connection")
oConn.Open ( strConnstring )
'Make sure the connection is open
If oConn.State = 1 Then
  Set oCommand = Server.CreateObject("Adodb.Command")
  oCommand.CommandText = "abbott_HomeTeam_RegisterPoints"
  oCommand.CommandType = 4
  oCommand.ActiveConnection = oConn
  oCommand.Parameters.Append oCommand.CreateParameter("@UserId", adInteger, adParamInput,,nUserId )
  oCommand.Parameters.Append oCommand.CreateParameter("@Points", adInteger, adParamInput,,nPoints )
  oCommand.Parameters.Append oCommand.CreateParameter("@AddedByUserId", adInteger, adParamInput,,nAddedByUserId )
  oCommand.Parameters.Append oCommand.CreateParameter("@TeamId", adInteger, adParamInput,,nTeamId )
  oCommand.Parameters.Append oCommand.CreateParameter("@RetVal", adInteger, adParamOutput,,0 )
  oCommand.Execute()
  PointId = oCommand.Parameters("@RetVal")
  RegisterPoints = PointId
End If
End Function

Function QueuePoints ( nUserId, nPoints, nTeamId)
'---------------------------------------------------------------------
'Queue Points in the HomeTeam holding tables.
' -nUserId: The user you are assigning the points to.
' -nPoints: The amount of cash/points
' -nTeamId: The team id (primary key in tbHomeTeam)
' -nAddedByUserId: The user logged into the system ( session("UID") )
'---------------------------------------------------------------------
Set oConn = Server.CreateObject("Adodb.Connection")
oConn.Open ( strConnstring )
'Make sure the connection is open
If oConn.State = 1 Then
  Set oCommand = Server.CreateObject("Adodb.Command")
  oCommand.CommandText = "abbott_HomeTeam_QueuePoints"
  oCommand.CommandType = 4
  oCommand.ActiveConnection = oConn
  oCommand.Parameters.Append oCommand.CreateParameter("@UserId", adInteger, adParamInput,,nUserId )
  oCommand.Parameters.Append oCommand.CreateParameter("@Points", adInteger, adParamInput,,nPoints )  
  oCommand.Parameters.Append oCommand.CreateParameter("@TeamId", adInteger, adParamInput,,nTeamId )
  oCommand.Parameters.Append oCommand.CreateParameter("@RetVal", adInteger, adParamOutput,,0 )
  oCommand.Execute()
  HoldingId = oCommand.Parameters("@RetVal")
  QueuePoints = HoldingId
End If
End Function

Sub RetrieveTransactionsByTeam(nTeamId, rsTemp) 
'---------------------------------------------------------------------
'Creates a recordset of all transactions by team
' -nTeamId: The team id (primary key in tbHomeTeam)
' -rsTemp: Unique name for the recordset
'---------------------------------------------------------------------
Set rsTemp = Server.CreateObject("ADODB.Recordset")
Set oConn = Server.CreateObject("Adodb.Connection")
oConn.Open ( strConnstring )
'Make sure the connection is open
If oConn.State = 1 Then
  Set oCommand = Server.CreateObject("Adodb.Command")
  oCommand.CommandText = "abbott_HomeTeam_RetrieveTransByTeam"
  oCommand.CommandType = 4
  oCommand.ActiveConnection = oConn
  oCommand.Parameters.Append oCommand.CreateParameter("@TeamId", adInteger, adParamInput,,nTeamId )
  Set rsTemp = oCommand.Execute()
End If
End Sub


Sub RetrieveSummaryTransactionsByTeam(nTeamId, rsTemp) 
'---------------------------------------------------------------------
'Creates a summary rescordset of all transactions by user filtered by teams
' -nTeamId: The team id (primary key in tbHomeTeam)
' -rsTemp: Unique name for the recordset
'---------------------------------------------------------------------
Set rsTemp = Server.CreateObject("ADODB.Recordset")
Set oConn = Server.CreateObject("Adodb.Connection")
oConn.Open ( strConnstring )
'Make sure the connection is open
If oConn.State = 1 Then
  Set oCommand = Server.CreateObject("Adodb.Command")
  oCommand.CommandText = "abbott_HomeTeam_RetrieveSummaryTransByTeam"
  oCommand.CommandType = 4
  oCommand.ActiveConnection = oConn
  oCommand.Parameters.Append oCommand.CreateParameter("@TeamId", adInteger, adParamInput,,nTeamId )
  Set rsTemp = oCommand.Execute()
End If
End Sub

Sub RetrieveActivePrograms(rsTemp) 
'---------------------------------------------------------------------
'Retrieves a list of active programs in the system.
'---------------------------------------------------------------------
Set rsTemp = Server.CreateObject("ADODB.Recordset")
Set oConn = Server.CreateObject("Adodb.Connection")
oConn.Open ( strConnstring )
'Make sure the connection is open
If oConn.State = 1 Then
  Set oCommand = Server.CreateObject("Adodb.Command")
  oCommand.CommandText = "abbott_Hometeam_RetrieveActivePrograms"
  oCommand.CommandType = 4
  oCommand.ActiveConnection = oConn
  Set rsTemp = oCommand.Execute()
End If
End Sub

Function RegisterFastRewardsPoints ( nUserId, nPoints, nTeamId, nTeamName)
'---------------------------------------------------------------------
'Register points in the abbott points database.
' -nUserId: The user you are assigning the points to.
' -nPoints: The amount of cash/points
' -nTeamId: The team id (primary key in tbHomeTeam)
' -nAddedByUserId: The user logged into the system ( session("UID") )
'---------------------------------------------------------------------
Set oConn = Server.CreateObject("Adodb.Connection")
oConn.Open ( strConnstring )
'Make sure the connection is open
If oConn.State = 1 Then
  Set oCommand = Server.CreateObject("Adodb.Command")
  oCommand.CommandText = "abbott_HomeTeam_RegisterFastRewardPoints"
  oCommand.CommandType = 4
  oCommand.ActiveConnection = oConn
  oCommand.Parameters.Append oCommand.CreateParameter("@UserId", adInteger, adParamInput,,nUserId )
  oCommand.Parameters.Append oCommand.CreateParameter("@Points", adInteger, adParamInput,,nPoints )
  oCommand.Parameters.Append oCommand.CreateParameter("@TeamName", adVarChar, adParamInput,19,nTeamName )
  oCommand.Parameters.Append oCommand.CreateParameter("@TeamId", adInteger, adParamInput,,nTeamId )
  oCommand.Parameters.Append oCommand.CreateParameter("@RetVal", adInteger, adParamOutput,,0 )
  oCommand.Execute()
  PointId = oCommand.Parameters("@RetVal")
  RegisterFastRewardsPoints = PointId
End If
End Function

'-------------------------------------------------------------------------------------------
function InitDatabases
	'cnGU.ConnectionString = "Driver={SQL Server}; Server=SQL2KDEV;Database=GlobalUser; uid=sa; pwd=;"
	'cnGU.ConnectionString = "Driver={SQL Server}; Server=SQL2KPROD;Database=GlobalUsers; uid=sa; pwd=Beerisgood;"
	cnGU.ConnectionString = "Driver={SQL Server}; Server=SQL2KPROD;Database=GlobalUser; uid=sa; pwd=Beerisgood;"
	cnGU.CursorLocation = 3 'use client cursor
	cnGU.Open
	cnGU.CommandTimeout = 200
		
	'cnFR.ConnectionString = "Driver={SQL Server}; Server=SQL2KDEV;Database=FastRewards; uid=sa; pwd=;"		
	cnFR.ConnectionString = "Driver={SQL Server}; Server=SQL2KPROD;Database=FastRewards; uid=sa; pwd=Beerisgood;"
	cnFR.CursorLocation = 3 'use client cursor
	cnFR.Open
	cnFR.CommandTimeout = 200
	
	'cnCC.ConnectionString = "Driver={SQL Server}; Server=SQL2KDEV;Database=ClarityCards; uid=sa; pwd=;"
	cnCC.ConnectionString = "Driver={SQL Server}; Server=GOLDFINGER;Database=ClarityCards; uid=sa; pwd=;"
	cnCC.CursorLocation = 3 'use client cursor
	cnCC.Open
	cnCC.CommandTimeout = 200
end function

'-------------------------------------------------------------------------------------------
function CloseDatabases
	cnGU.close
	cnFR.close
	cnCC.close
	
	set cnGU = nothing
	set cnFR = nothing
	set cnCC = nothing
	
end function
'-------------------------------------------------------------------------------------------

Sub SendEmail (TeamName,TeamID,UserID, LastName, FirstName) 

	Rcpt1 = "Andrew Newman"
	Rcpt2 = "anewman@madisonpg.com"	
	
	Subject = "Attn: HomeTeam Manager w/o valid address"

	Body = "A HomeTeam was created with a manager with an invalid address.  This FastRewards card will be sent to Madison." & vbcr
	Body = Body & "HomeTeam Name:" & TeamName & vbcr
	Body = Body & "HomeTeam ID:" & TeamID & vbcr
	Body = Body & "HomeTeam Manager's UserID:" & UserID & vbcr
	Body = Body & "HomeTeam Manager's Name:" & FirstName & " " & LastName & vbcr


	set oMailer  = Server.CreateObject("SMTPsvg.Mailer")
	oMailer.FromAddress = "help@madisonpg.com"
	oMailer.RemoteHost = "mail.madisonpg.com"
	oMailer.Subject = Subject
	oMailer.AddRecipient Rcpt1,Rcpt2
	oMailer.BodyText = Body
	oMailer.SendMail
 End Sub

'-------------------------------------------------------------------------------------------
 Sub SendHomeTeamClosedEmail (TeamName,TeamID,UserID, ManagerName) 
		
	Subject = "Attn: HomeTeam Closed"

	Body = "A HomeTeam was closed and the FastRewards Card has been blocked." & vbcr
	Body = Body & "HomeTeam Name:" & TeamName & vbcr
	Body = Body & "HomeTeam ID:" & TeamID & vbcr
	Body = Body & "HomeTeam Manager's UserID:" & UserID & vbcr
	Body = Body & "HomeTeam Manager's Name:" & trim(ManagerName) & vbcr

	set oMailer  = Server.CreateObject("SMTPsvg.Mailer")
	oMailer.FromAddress = "help@madisonpg.com"
	oMailer.RemoteHost = "mail.madisonpg.com"
	oMailer.Subject = Subject
	'oMailer.AddRecipient Rcpt1,Rcpt2
	oMailer.AddRecipient "Andrew Newman","ANewman@madisonpg.com"
	oMailer.AddRecipient "Christian Dacayanan","cdacayanan@madisonpg.com"
	oMailer.AddRecipient "John Catuira","jcatuira@madisonpg.com"	
	oMailer.BodyText = Body
	oMailer.SendMail
				
 End Sub
  
'-------------------------------------------------------------------------------------------

Sub RetrieveEndPrograms (nSessionId) 
'---------------------------------------------------------------------
'Alert admin user when a program has been closed.
'---------------------------------------------------------------------
Set rsTemp = Server.CreateObject("ADODB.Recordset")
Set oConn = Server.CreateObject("Adodb.Connection")
oConn.Open ( strConnstring )
'Make sure the connection is open
If oConn.State = 1 Then
  Set oCommand = Server.CreateObject("Adodb.Command")
  oCommand.CommandText = "abbott_Hometeam_RetrieveEndPrograms"
  oCommand.CommandType = 4
  oCommand.Parameters.Append oCommand.CreateParameter("@sessionuid", adInteger, adParamInput,,nSessionId )
  oCommand.ActiveConnection = oConn
  Set rsTemp = oCommand.Execute()
  If rsTemp.EOF = False Then
    Do While Not rsTemp.EOF
      strPrograms = strPrograms & rsTemp("teamname") & "\n"
    rsTemp.Movenext
    Loop
    strMsg = "The following hometeam programs were closed: " & "\n\n" & strPrograms
    If Len(strMsg) <> 0 Then
      %>
        <script language="javascript">
          alert('<%=Replace(strMsg,"'","''")%>');
        </script>
      <%
    End If
  End If
End If
End Sub

Sub RetrieveNewUsers (nSessionId) 
'---------------------------------------------------------------------
'Retrieve New Users
'---------------------------------------------------------------------
Set rsTemp = Server.CreateObject("ADODB.Recordset")
Set oConn = Server.CreateObject("Adodb.Connection")
oConn.Open ( strConnstring )
'Make sure the connection is open
If oConn.State = 1 Then
  Set oCommand = Server.CreateObject("Adodb.Command")
  oCommand.CommandText = "abbott_hometeam_detectnewusers"
  oCommand.CommandType = 4
  oCommand.Parameters.Append oCommand.CreateParameter("@sessionuid", adInteger, adParamInput,,nSessionId )
  oCommand.ActiveConnection = oConn
  Set rsTemp = oCommand.Execute()
  If rsTemp.EOF = False Then
    Do While Not rsTemp.EOF
      strPrograms = strPrograms & rsTemp("teamname") & " - " & rsTemp("NewUserName") &  "\n"
    rsTemp.Movenext
    Loop
    strMsg = "The following direct reports were placed into the system: " & "\n\n" & strPrograms
    If Len(strMsg) <> 0 Then
      %>
        <script language="javascript">
          alert('<%=Replace(strMsg,"'","''")%>');
        </script>
      <%
    End If
  End If
End If
End Sub

%>
