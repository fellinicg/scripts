SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: October 27, 2005
-- Description:	Return level for employee in aims
-- =============================================
CREATE FUNCTION ufGetEmployeeAIMSLevel (@ID INT)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result int

	-- Get the level
	SELECT @Result =
	CASE 0 
		WHEN LEN(L01MGR) then '1' 
		WHEN LEN(L02MGR) then '2' 
		WHEN LEN(L03MGR) then '3' 
		WHEN LEN(L04MGR) then '4' 
		WHEN LEN(L05MGR) then '5' 
		WHEN LEN(L06MGR) then '6' 
		WHEN LEN(L07MGR) then '7' 
		WHEN LEN(L08MGR) then '8' 
		WHEN LEN(L09MGR) then '9' 
		WHEN LEN(L10MGR) then '10' 
		WHEN LEN(L11MGR) then '11' 
		WHEN LEN(L12MGR) then '12' 
		WHEN LEN(L13MGR) then '13' 
		WHEN LEN(L14MGR) then '14' 
		WHEN LEN(L15MGR) then '15'
		ELSE -99
	END
	FROM dbo.aims
	WHERE u_ID = @ID

	-- Return the result of the function
	RETURN @Result

END
GO

