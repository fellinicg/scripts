<!--#include file="../connect.asp"-->
<!--#INCLUDE FILE="../includes/funcs_HomeTeam.asp"-->
<%

set objClarity = Server.CreateObject("Clarity_v1.clsTransactions_v1")
set oCards = Server.CreateObject("Clarity_v1.clsAPI_v1")

dim UserID
dim msg
dim error
dim strWhere
dim strAddedUsers
dim rsSearch
set cnGU = server.CreateObject("ADODB.Connection")
set cnFR = server.CreateObject("ADODB.Connection")
set cnCC = server.CreateObject("ADODB.Connection")
error=false

InitDatabases()

'------------------------------------------Search-------------------------------------------
if request("state") = "Search" then		
	SearchUsers()
'-----------------------------------------------Setup---------------------------------------
elseif request("state") = "Setup" then
	UserID = request("uid")
	
	if clng(UserID) <> -1 then	
		sql="abbott_HomeTeamManagerCheck @UserID=" & UserID
	
		set rsManagerCheck = server.CreateObject("ADODB.Recordset")	
		set rsManagerCheck = cn.Execute(sql)
		
		if not rsManagerCheck.EOF then
			msg = "<font color=red>User is already a manager of an active team: " & rsManagerCheck("teamname") & "</font><br>"
			error =true
		end if
	end if
	
	set rsManagerCheck = nothing			
	
'----------------------------------------------Create---------------------------------------
elseif request("state") = "Create" then

	dim FirstName
	dim MI
	dim LastName
	dim u_SSN
	dim Street1
	dim Street2				
	dim City
	dim State
	dim Zip5						
	dim	emailID
	dim globalID
	dim ad_ID
	dim em_ID
	dim TeamName	
	dim TotalBudget
	dim FastRewardBudget
	dim MemberBudget
	dim StartDate
	dim EndDate
	dim UserList
	dim ExtKey
	dim BadAddress
	dim vReturn
	
	UserID = request("uid")
	TeamName = replace(request("TeamName"),"'","''")
	TotalBudget = request("TotalBudget")
	FastRewardBudget = request("FastRewardBudget")
	MemberBudget = request("MemberBudget")
	StartDate = request("StartDate")
	EndDate = request("EndDate")
	UserList = request("UserCheck")
	ExtKey = 0
	globalID = 0
	BadAddress = false	
	
	if CheckDistribution(MemberBudget,UserList,UserID) then
	
		if clng(UserID) <> -1 then 
			AddGlobalUser FirstName,MI,LastName,u_SSN,Street1,Street2,City,State,Zip5,emailID,globalID,ad_ID,em_ID
			' if globalID does not exist by this point, an error has occured
			if globalID <> 0 then
			
				vReturn = CreateFastRewardsCard(globalID,LastName,FirstName,STREET1,STREET2,City,State,Zip5,EmailID)			
				' check if vReturn <> a number then rollback
				if isnumeric(vReturn) then

					ExtKey = GetExternalKey(vreturn)
					InsertFastRewards(globalID)
					TeamID = CreateNewHomeTeam(TeamName,TotalBudget,FastRewardBudget,MemberBudget,StartDate,EndDate,UserID,UserList,ExtKey,globalID)
					DistributeFunds UserList, UserID, TeamID
																							
					' add point to FR card
					if FastRewardBudget <> 0 then
						RegisterFastRewardsPoints globalID, FastRewardBudget, TeamId, TeamName
					end if

					if BadAddress = true then
						SendEmail TeamName,TeamID,UserID, LastName, FirstName
					end if

					msg = "<br><br><font color=red>Fast Rewards card generated and " & TeamName & " was created</font>"
				else			
					DeleteGlobalUser globalID,ad_ID,em_ID
					msg = "<br><br><font color=red>Error: " & vReturn & "<br>Fast Rewards Card creation failed</font><br>"
				end if					
			else
				msg = "<br><br><font color=red>Global user creation failed</font><br>"
			end if	
			'cleanup
			set cmdCreateGlobalUser = nothing
		else
			'If a blank mananger
			TeamID = CreateNewHomeTeam(TeamName,TotalBudget,FastRewardBudget,MemberBudget,StartDate,EndDate,UserID,UserList,ExtKey,globalID)
			DistributeFunds UserList, UserID, TeamID
			msg = "<br><br><font color=red>" & TeamName & " was created</font>"
		end if
	else
		msg = "<br><br><font color=red>Member Budget has been exceeded</font>"
	end if

'-------------------------------------------------Edit----------------------------------------
elseif request("state") = "Edit" then

	if len(request("selTeam")) > 0 then
		TeamID = request("selTeam")		
	else
		TeamID = 0
	end if
	
	func = request("func")
	EmpID  = request("eid")
	Budget = request("Budget")
	FRBudget = request("FRBudget")
	StartDate = request("StartDate")
	EndDate = request("EndDate")
	ManagerID = request("uid")
	TeamName = request("TeamName")
	GlobalUserID = request("GlobalUserID")
	ProgramOver = request("ProgramOver")
	intKey = 0
	if len(request("CardBlocked")) > 0 then
		CardBlocked=request("CardBlocked")
	else
		CardBlocked=0
	end if
			
	if len(func) > 0 then
	
		if CardBlocked = 0 then						
			select case func
				case "Edit"
					if ProgramOver <> 1 then
						set rsUpdate = server.CreateObject("ADODB.Recordset")
						sql = "abbott_HomeTeamUpdate @TeamID=" & TeamID & ",@NewBudget=" & Budget & ",@FRBudget=" & FRBudget & ",@StartDate='" & StartDate & "',@EndDate='" & EndDate & "', @Addedby=" & session("uid")
						set rsUpdate = cn.Execute(sql)
						if not rsUpdate.EOF then
							if rsUpdate("FRChange") <> 0 then
								RegisterFastRewardsPoints GlobalUserID, rsUpdate("FRChange"), TeamID, TeamName
							end if
						end if				
					end if
				case "Remove"
					if ProgramOver <> 1 then				
						'sql = "abbott_HomeTeamRemoveMembers @teamid=" & TeamID & ", @EmpID=" & EmpID
						'cn.Execute(sql)
						
						'RetrieveSummaryTransactionsByTeam TeamId, rsPoints
						'rsPoints.filter = "UserId=" & EmpID
						'sql = "Select points from tbHometeamPointsHoldingTable where teamid=" & TeamID & " and UserID=" & EmpID & " and processed=0"
						'set rsPoints = cn.Execute(sql)
						'points = rsPoints("points")
						
						Set cmdRemoveMember = server.CreateObject("ADODB.Command")	
						With cmdRemoveMember
							set .ActiveConnection = cn
							.CommandText="abbott_HomeTeamRemoveMembers"
							.CommandType= adCmdStoredProc
							.Parameters.Append  .CreateParameter("@TeamID", adInteger, adParamInput,, TeamID)
							.Parameters.Append  .CreateParameter("@EmpID", adInteger, adParamInput,, EmpID)
							.Parameters.Append  .CreateParameter("@Sessionuid", adSingle, adParamInput,, NULL)
							.Parameters.Append  .CreateParameter("@Points", adInteger, adParamOutput)
							.Execute
							points=.Parameters("@Points").value		
						end with
						set cmdRemoveMember = nothing
										
						if len(points) > 0 then					
							points = cint(points)
						else					
							points = 0
						end if				
						
						if points <> 0 then
							points=points * -1
							'PID = RegisterPoints(EmpID, points, TeamID, ManagerID)
							'UpdateHomeTeamBudget TeamID, points, EmpID,PID
							HID = QueuePoints(EmpID, points, Teamid)
							UpdateHomeTeamBudget TeamID, points, EmpID, HID
						end if				
					end if
				case "Add"
					if ProgramOver <> 1 then
						sql = "abbott_HomeTeamAddMembers @teamid=" & TeamID & ", @EmpID=" & EmpID
						cn.Execute(sql)
					end if
				case "Adjust"
					'if ProgramOver <> 1 then
						CheckClarityCard EmpID
						AddPoints EmpID, Teamid, ManagerID
					'end if
				case "MoveQueued"
					sql = "abbott_HomeTeamMoveQueuedFunds @teamid=" & TeamID & ",@AddedBy=" & Session("uid")
					cn.Execute(sql)		
				case "End"	
					'get amount of points from card
					'points = cint(request("FRBudgetHidden")) 'value for now				
					if ProgramOver <> 1 then
						sql = "fr_GetProgAndKey @gu_ID=" & GlobalUserID

						set rsFR = cnFR.Execute(sql)
			
						if not rsFR.eof then
							intKey = rsFR("IK")
							rsFR.close
						end if	
						
						'this needs to be rewritten
						if intKey <> 0 then
							points = int(oCards.GetUserAvailBalance(intKey))
						end if
										
						'take all points from FR card
						if points <> 0 then
							RegisterFastRewardsPoints GlobalUserID, points*-1, TeamID, TeamName 
						end if
						
						'block card  - This will not work ATM - if card is blocked, no funds can be removed
						'BlockCard(GlobalUserID)
						
						sql = "abbott_HomeTeamEndProgram @teamid=" & TeamID & ", @Points=" & points*-1 & ",@AddedBy=" & session("uid")
						cn.Execute(sql)				
						
					'taken out because they no longer need to distribute funds automatically
						'points = points +  cint(request("Remaining"))'value for now
						
						'dim UserArray()
						
						'redim UserArray(0)
						
						'UserArray(0) = ManagerID
						
						'sql = "Select UserID from tbHometeamUsers where teamid=" & teamid
						'set rsUsers = server.CreateObject("ADODB.Recordset")
						'set rsUsers = cn.Execute(sql)
						
						''split up rewards
						'i=1
						'if not rsUsers.EOF then					
						'	do while not rsUsers.EOF
						'		redim preserve UserArray(i)
						'		UserArray(i) = rsUsers("UserID")
						'		i=i+1	
						'		rsUsers.MoveNext
						'	loop
						'	rsUsers.Close
						'end if
						
						'set rsUsers = nothing
						
						'PointShare = points\i
						
						'if pointshare <> 0 then
						'	for j=0 to ubound(UserArray)
						'		CheckClarityCard(UserArray(j))						
						'		'PID = RegisterPoints(UserArray(j), PointShare, TeamID, ManagerID)
						'		'UpdateHomeTeamBudget TeamID, PointShare, UserArray(j),PID
						'		HID = QueuePoints(UserArray(j), PointShare, TeamID)
						'		UpdateHomeTeamBudget TeamID, PointShare, UserArray(j), HID
						'	next
						'end if
						
						'msg = "<br><font color=red>" & PointShare & " added to each member's card</font><br>"
					end if
					
				case "Block"
					'block card  - This will not work ATM - if card is blocked, no funds can be removed					
					retval = BlockCard(GlobalUserID)					
					if retval = true then
						sql = "abbott_HomeTeamBlockCard @TeamID=" & teamID
						cn.execute(sql)
						SendHomeTeamClosedEmail TeamName,TeamID,ManagerID, GetManagerName(ManagerID)
						msg = "<font color=red>Card has been blocked</font><br>"
					else
						msg = "<font color=red>Error: card has not been blocked.  Please try again later.</font><br>"
					end if
					
			end select
		else
			msg = "<font color=red>This Program is over and the Fast Rewards card has been blocked.  No further changes are possible."
			error = true
		end if
		
		if ProgramOver = 1 and func <> "Block" and func <> "MoveQueued" and func <> "Adjust" then
			msg = "<font color=red>This program is closed</font>"
			error = true
		end if

	end if


'----------------------------------------------Reports------------------------------------
elseif request("state") = "HTReport" then
	teamid = request("selTeam")	
elseif request("state") = "GUReport" then
	teamid = request("selTeam")		
elseif request("state") = "BudgetReport" then
	teamid = request("selTeam")		
elseif request("state") = "FastRewardsReport" then
	teamid = request("selTeam")				

'---------------------------------------------Search for Users-----------------------------
elseif request("state") = "SearchNHUsers" then
	TeamID = request("selTeam")
	UserID = request("uid")
	NHUsersList = trim(request("NHUsersList"))
	PriorState = request("PriorState")
	TeamName = request("TeamName")
	TotalBudget= request("TotalBudget")
	FastRewardBudget= request("FastRewardBudget")
	MemberBudget= request("MemberBudget")
	StartDate= request("StartDate")
	EndDate= request("EndDate")
	GetCurrentUsers()
elseif request("state") = "SearchNHUsersResults" then
	TeamID = request("selTeam")
	UserID = request("uid")
	NHUsersList = trim(request("NHUsersList"))
	PriorState = request("PriorState")
	TeamName = request("TeamName")
	TotalBudget= request("TotalBudget")
	FastRewardBudget= request("FastRewardBudget")
	MemberBudget= request("MemberBudget")
	StartDate= request("StartDate")
	EndDate= request("EndDate")
	GetCurrentUsers()
	SearchUsers()
elseif request("state") = "AddNHUsers" then	
	TeamID = request("selTeam")	
	PriorState = request("PriorState")
	UserID = request("uid")
	TeamName = request("TeamName")
	TotalBudget= request("TotalBudget")
	FastRewardBudget= request("FastRewardBudget")
	MemberBudget= request("MemberBudget")
	StartDate= request("StartDate")
	EndDate= request("EndDate")
	NHUsers = request("NHUsers")
	NHUsersList = trim(request("NHUsersList"))
	if len(NHUsersList) > 0 then
		NHUsersList = NHUsersList & "," & NHUsers
	else
		NHUsersList = NHUsers
	end if
	AddNonHierarchalUsers()
elseif request("state") = "SearchManager" then
	TeamID = request("selTeam")
elseif request("state") = "SearchManagerResults" then
	TeamID = request("selTeam")
	SearchUsers()
elseif request("state") = "AssignManager" then
	TeamID = request("selTeam")
	UserID = request("uid")
	
	sql="abbott_HomeTeamManagerCheck @UserID=" & UserID	
	set rsManagerCheck = server.CreateObject("ADODB.Recordset")	
	set rsManagerCheck = cn.Execute(sql)
	
	ManagerActive = false
	ManagerOnTeamActive = false
	
	if not rsManagerCheck.EOF then
		ManagerActive = true
	end if
	
	'check here if manager is on the team already	
	sql="abbott_HomeTeamManagerOnTeam @UserID=" & UserID & ",@TeamID=" & TeamID	
	set rsManagerOnTeam = server.CreateObject("ADODB.Recordset")	
	set rsManagerOnTeam = cn.Execute(sql)
	
	if not rsManagerOnTeam.EOF then
		ManagerOnTeamActive = true
	end if
		
	if ManagerActive then
		msg = "<font color=red>User is already a manager of an active team: " & rsManagerCheck("teamname") & "</font><br>"
		error =true
	elseif ManagerOnTeamActive then
		msg = "<font color=red>The user you selected as manager is already a member of the team.</font><br>"
		error =true
	else
		AssignManagerToHomeTeam(TeamID)
	end if
end if

'--------------------------------------------Functions------------------------------
function GetManagerName(UID)	
	if clng(UID) <> -1 then
		sql = "select lastname, firstname from vw_Users vw where u_id = " & UID
		set rsMananger = cn.execute (sql)	
		ManagerName = trim(rsMananger("firstname")) & " " & trim(rsMananger("lastname"))	
	else
		ManagerName = "Blank Manager"
	end if
	GetManagerName = ManagerName	
end function


'---------------------------------------------------------------------------------------
sub CheckClarityCard(UserID)
	set rsAccnt = Server.CreateObject("ADODB.Recordset")	
	set rsAccnt = oCards.GetAccount(1, UserID,false)
	
	if not rsAccnt.EOF then
		rsAccnt.Filter="ac_Status='ACCOUNT_OPEN'"
	end if
	
	acStatus = "" 'First Accnt is nothing
	if not rsAccnt.EOF then
		rsAccnt.Sort="ac_Stamp DESC"
		acStatus = rsAccnt("ac_Status")
	end if
	
	if acStatus <> "ACCOUNT_OPEN" then
		CreateCard(UserID) 'x=Abbott Card number
	end if
			
end sub

'---------------------------------------------------------------------------------------
sub CreateCard(UserID)

	set rsAbbottUser = server.CreateObject("ADODB.Recordset")	
	sql = "Select FirstName,MI,LastName,u_SSN,Street1,Street2,City,State,Zip5,emailID from users where u_id=" & UserID
	set rsAbbottUser = cn.execute(sql)
		
	if not rsAbbottUser.EOF then
		CC_FirstName = rsAbbottUser("FirstName")
		CC_MI = rsAbbottUser("MI")
		CC_LastName = rsAbbottUser("LastName")
		CC_u_SSN = rsAbbottUser("u_SSN")
		CC_Street1 = rsAbbottUser("Street1")
		CC_Street2 = rsAbbottUser("Street2")
		CC_City = rsAbbottUser("City")
		CC_State = rsAbbottUser("State")
		CC_Zip5 = rsAbbottUser("Zip5")
		
		if isnull(rsAbbottUser("emailID")) then
			CC_emailID	= CC_FirstName + "." + CC_LastName + "@fakeabbottfr.com"
		else
			CC_emailID	 = rsAbbottUser("emailID")
		end if 
	end if

	dim vArr(11)
	
	vArr(0) = globalID
	vArr(1) = 1
	vArr(2) = CC_LastName
	vArr(3) = CC_FirstName
	vArr(4) = CC_STREET1
	vArr(5) = CC_STREET2
	vArr(6) = CC_City
	vArr(7) = CC_State
	vArr(8) = CC_Zip5
	vArr(9) = "USA"
	vArr(10) = CC_EmailID
	vArr(11) = "physical"
	
	vReturn = objClarity.CreateAccount(vArr)

end sub
'-------------------------------------------------------------------------------------------
function CheckDistribution(Budget,List,ManagerID)

	dim listTotal

	if len(request("txt" & ManagerID)) > 0 and isnumeric(request("txt" & ManagerID)) then
		listTotal = cint(request("txt" & ManagerID))
	else
		listTotal = 0
	end if

	UserArray = split(List,",")

	for i = 0 to ubound(UserArray)						
		if len(request("txt" & trim(UserArray(i)))) > 0 and isnumeric(request("txt" & trim(UserArray(i)))) then			
			listTotal = listTotal + cint(request("txt" & trim(UserArray(1))))
		end if
	next
		
	if listTotal > Budget then
		CheckDistribution = false
	else
		CheckDistribution = true
	end if

end function

'-------------------------------------------------------------------------------------------
sub AddPoints(UserID, Teamid, ManagerID)
	
	points = request("txt" & trim(UserID))
		
	if len(points) > 0 and isnumeric(points) then
		if points <> 0 then
			'PID = RegisterPoints(UserId, points, Teamid, ManagerID)
			'UpdateHomeTeamBudget TeamID, points, UserID, PID
			HID = QueuePoints(UserId, points, Teamid)
			UpdateHomeTeamBudget TeamID, points, UserID, HID
		end if
	end if
end sub

'-------------------------------------------------------------------------------------------
sub UpdateHomeTeamBudget(TeamID, points, MemberID, HID)
	'call to update team budget with points
	
	sql = "abbott_HomeTeamUserBudgetUpdate @TeamID=" & TeamID & ",@Change=" & points & ", @Addedby=" & session("uid") & ",@MemberID=" & MemberID & ",@HID=" & HID
	cn.execute(sql)	
end sub

'-------------------------------------------------------------------------------------------
sub DeleteGlobalUser(gu_id,ad_ID,em_ID)
	if ad_ID <> 0 and em_ID <> 0 then
	
		Set cmdNewUser = server.CreateObject("ADODB.Command")	
			With cmdNewUser
				set .ActiveConnection = cnGU
				.CommandText="upDeleteUserAccount"
				.CommandType= adCmdStoredProc
				.Parameters.Append  .CreateParameter("@UserAccountID", adInteger, adParamInput,, gu_id)	
				.Parameters.Append  .CreateParameter("@Result", adChar, adParamOutput)
				.Execute
			end with
		set cmdNewUser = nothing
		
'***		Set cmdNewUser = server.CreateObject("ADODB.Command")	
'			With cmdNewUser
'				set .ActiveConnection = cnGU
'				.CommandText="DeleteGlobalUser"
'				.CommandType= adCmdStoredProc
'				.Parameters.Append  .CreateParameter("@gu_ID", adInteger, adParamInput,, gu_id)	
'				.Execute
'			end with
'		set cmdNewUser = nothing
'
'***		Set cmdNewUser = server.CreateObject("ADODB.Command")	
'			With cmdNewUser
'				set .ActiveConnection = cnGU
'				.CommandText="DeleteGlobalUserAddress"
'				.CommandType= adCmdStoredProc
'				.Parameters.Append  .CreateParameter("@ad_ID", adInteger, adParamInput,, ad_ID)	
'				.Execute		
'			end with
'		set cmdNewUser = nothing
'		
'***		Set cmdNewUser = server.CreateObject("ADODB.Command")	
'			With cmdNewUser
'				set .ActiveConnection = cnGU
'				.CommandText="DeleteGlobalUserEMail"
'				.CommandType= adCmdStoredProc
'				.Parameters.Append  .CreateParameter("@em_ID", adInteger, adParamInput,, em_ID)	
'				.Execute		
'			end with
'		set cmdNewUser = nothing
		
	end if	
end sub
'-------------------------------------------------------------------------------------------
function BlockCard(gu_id)

	sql = "fr_GetProgAndKey @gu_ID=" & gu_id
	
	set rsFR = cnFR.Execute(sql)
	
	if not rsFR.eof then
		intKey = rsFR("IK")
		progID = rsFR("PROG")
		retVal = oCards.BlockCard(progID,intKey,"UNKNOWN")
		
		retVal = replace(retval,Chr(13),"")
		retVal = replace(retval,Chr(10),"")
		response.Write("<!--retval:" & retval & "-->")		
	
		if ucase(trim(retVal)) = "SUCCESS" or ucase(trim(retVal)) = "FAILURE_BLOCKED" then
			BlockCard = true
		else
			BlockCard = false
		end if
	else
		BlockCard = false
	end if
	
end function
'-------------------------------------------------------------------------------------------
sub WriteHTReport(TeamID)

	response.Write("<table cellpadding=2 cellspacing=2 width=75% border=1 class=smalltextblack>" & vbcr)
	
	if TeamID = "ALL" then
		sql = "abbott_HomeTeamReport"
	else
		sql = "abbott_HomeTeamReportByTeam @TeamID=" & TeamID 
	end if
	
	set rsHTRpt = cn.Execute(sql)
	if not rsHTRpt.EOF then	
		response.Write ("<tr>" & vbcr)
		response.Write ("<th bgcolor=#CCCCCC>TeamName</th>" & vbcr)
		response.Write ("<th bgcolor=#CCCCCC>Open/Closed</th>" & vbcr)			
		response.Write ("<th bgcolor=#CCCCCC>Start Date</th>" & vbcr)
		response.Write ("<th bgcolor=#CCCCCC>End Date</th>" & vbcr)
		response.Write ("<th bgcolor=#CCCCCC>Team Member</th>" & vbcr)
		response.Write ("<th bgcolor=#CCCCCC>Budget</th>" & vbcr)
		response.Write ("<th bgcolor=#CCCCCC>FastReward Dollars</th>" & vbcr)
		response.Write ("<th bgcolor=#CCCCCC>AWOO Card Dollars Given</th>" & vbcr)			
		response.Write ("</tr>" & vbcr)
		do while not rsHTRpt.EOF
			if cint(rsHTRpt("Deleted")) = 0 then							
				response.Write ("<tr>" & vbcr)
			else
				response.Write ("<tr bgcolor=yellow>" & vbcr)
			end if
			
			response.Write ("<td>" & rsHTRpt("TeamName") & "</td>" & vbcr)
			if cint(rsHTRpt("Deleted")) = 0 then
				response.Write ("<td>Open</td>" & vbcr)
			else
				response.Write ("<td>Closed</td>" & vbcr)
			end if
			response.Write ("<td>" & rsHTRpt("startdt") & "</td>" & vbcr)
			response.Write ("<td>" & rsHTRpt("enddt") & "</td>" & vbcr)
			response.Write ("<td>" & rsHTRpt("UserName") & "</td>" & vbcr)
			if cint(rsHTRpt("SortOrder")) = 1 then
				response.Write ("<td>" & rsHTRpt("Budget") & "</td>" & vbcr)
				response.Write ("<td>" & rsHTRpt("TotalFRPoints") & "</td>" & vbcr)
			else
				response.Write ("<td>&nbsp;</td>" & vbcr)
				response.Write ("<td>&nbsp;</td>" & vbcr)
			end if
			response.Write ("<td>" & rsHTRpt("TotalPoints") & "</td>" & vbcr)
			response.Write ("</tr>" & vbcr)
			
		  	rsHTRpt.MoveNext
		loop		
	end if
	response.Write("</table>" & vbcr)		
	
end sub
'-------------------------------------------------------------------------------------------
sub WriteHTReportOld(TeamID)
		
	response.Write("<table cellpadding=2 cellspacing=2 width=75% border=1>" & vbcr)
	
	if TeamID = "ALL" then
		sql = "abbott_HomeTeamReportTop"
	else
		sql = "abbott_HomeTeamReportTopByTeam @TeamID=" & TeamID 
	end if
	
	set rsHTRptTop = cn.Execute(sql)
	if not rsHTRptTop.EOF then
		do while not rsHTRptTop.EOF			
		
			response.Write ("<tr>" & vbcr)
			response.Write ("<th bgcolor=#CCCCCC>TeamName</th>" & vbcr)
			response.Write ("<th bgcolor=#CCCCCC>Budget</th>" & vbcr)
			response.Write ("<th bgcolor=#CCCCCC>Start Date</th>" & vbcr)
			response.Write ("<th bgcolor=#CCCCCC>End Date</th>" & vbcr)
			response.Write ("</tr>" & vbcr)

			response.Write ("<tr>" & vbcr)
			response.Write ("<td>" & rsHTRptTop("TeamName") & "</td>" & vbcr)
			response.Write ("<td>" & rsHTRptTop("Budget") & "</td>" & vbcr)
			response.Write ("<td>" & rsHTRptTop("startdt") & "</td>" & vbcr)
			response.Write ("<td>" & rsHTRptTop("enddt") & "</td>" & vbcr)
			response.Write ("</tr>" & vbcr)
			
			
			sql = "abbott_HomeTeamReportMemberInfo @TeamID=" & rsHTRptTop("TeamID") & ",@Type=2" 
			set rsHTRptManager = cn.Execute(sql)
			if not rsHTRptManager.EOF then							
				response.Write ("<tr>" & vbcr)
				response.Write ("	<td>&nbsp;</td>" & vbcr)	
				response.Write ("	<td colspan=3>" & vbcr)	
				response.Write ("		<table border=1 cellspacing=2 width=100% cellpadding=2 >" & vbcr)
				response.Write ("			<tr>" & vbcr)
				response.Write ("				<th bgcolor=#CCCCCC>Manager</th>" & vbcr)
				response.Write ("				<th bgcolor=#CCCCCC>FastReward Dollars</th>" & vbcr)						
				response.Write ("			</tr>" & vbcr)
				do while not rsHTRptManager.EOF		
					response.Write ("<tr>" & vbcr)
					response.Write ("<td>" & rsHTRptManager("TeamMemberName") & "</td>" & vbcr)
					response.Write ("<td>" & rsHTRptManager("TotalPoints") & "</td>" & vbcr)
					response.Write ("</tr>" & vbcr)
					rsHTRptManager.movenext
				loop
				
				response.Write ("		</table>" & vbcr)
				response.Write ("	</td>" & vbcr)	
				response.Write ("</tr>" & vbcr)

			end if	
				
			sql = "abbott_HomeTeamReportMemberInfo @TeamID=" & rsHTRptTop("TeamID") & ",@Type=1" 
			set rsHTRptManager = cn.Execute(sql)
			if not rsHTRptManager.EOF then							
				response.Write ("<tr>" & vbcr)
				response.Write ("	<td>&nbsp;</td>" & vbcr)	
				response.Write ("	<td colspan=3>" & vbcr)	
				response.Write ("		<table border=1 cellspacing=2 width=100% cellpadding=2 >" & vbcr)
				response.Write ("			<tr>" & vbcr)
				response.Write ("				<th bgcolor=#CCCCCC>Manager</th>" & vbcr)
				response.Write ("				<th bgcolor=#CCCCCC>Dollars Given</th>" & vbcr)						
				response.Write ("			</tr>" & vbcr)
				do while not rsHTRptManager.EOF		
					response.Write ("<tr>" & vbcr)
					response.Write ("<td>" & rsHTRptManager("TeamMemberName") & "</td>" & vbcr)
					response.Write ("<td>" & rsHTRptManager("TotalPoints") & "</td>" & vbcr)
					response.Write ("</tr>" & vbcr)
					rsHTRptManager.movenext
				loop
				
				response.Write ("		</table>" & vbcr)
				response.Write ("	</td>" & vbcr)	
				response.Write ("</tr>" & vbcr)

			end if							
				
			sql = "abbott_HomeTeamReportMemberInfo @TeamID=" & rsHTRptTop("TeamID") & ",@Type=0" 
			set rsHTRptManager = cn.Execute(sql)
			if not rsHTRptManager.EOF then							
				response.Write ("<tr>" & vbcr)
				response.Write ("	<td>&nbsp;</td>" & vbcr)	
				response.Write ("	<td colspan=3>" & vbcr)	
				response.Write ("		<table border=1 cellspacing=2 width=100% cellpadding=2 >" & vbcr)
				response.Write ("			<tr>" & vbcr)
				response.Write ("				<th bgcolor=#CCCCCC>Team Member</th>" & vbcr)
				response.Write ("				<th bgcolor=#CCCCCC>Dollars Given</th>" & vbcr)						
				response.Write ("			</tr>" & vbcr)
				do while not rsHTRptManager.EOF		
					response.Write ("<tr>" & vbcr)
					response.Write ("<td>" & rsHTRptManager("TeamMemberName") & "</td>" & vbcr)
					response.Write ("<td>" & rsHTRptManager("TotalPoints") & "</td>" & vbcr)
					response.Write ("</tr>" & vbcr)
					rsHTRptManager.movenext
				loop
				
				response.Write ("		</table>" & vbcr)
				response.Write ("	</td>" & vbcr)	
				response.Write ("</tr>" & vbcr)

			end if							
			
'			set rsAccnt = Server.CreateObject("ADODB.Recordset")			
'			set rsAccnt = oCards.GetAccount(14, cstr(SESSION("UID")),true)
'			set rsSTMTs = Server.CreateObject("ADODB.Recordset")
			
'			acStatus = ""
'			IF NOT rsAccnt.EOF THEN
'				rsAccnt.Sort="ac_Stamp DESC"
'				acStatus = rsAccnt("ac_Status")
'			END IF
			
'			if acStatus = "ACCOUNT_OPEN" then 
'				set rsSTMTs = oCards.GetAccountTransactions(InternalKey)
'				
'				response.Write ("<tr>" & vbcr)
'				response.Write ("	<td>&nbsp;</td>" & vbcr)	
'				response.Write ("	<td colspan=3>" & vbcr)	
'				response.Write ("		<table border=1 cellspacing=2 width=100% cellpadding=2 >" & vbcr)
'				response.Write ("			<tr>" & vbcr)
'				response.Write ("				<th bgcolor=#CCCCCC>Date</th>" & vbcr)
'				response.Write ("				<th bgcolor=#CCCCCC>Amount</th>" & vbcr)						
'				response.Write ("				<th bgcolor=#CCCCCC>Description</th>" & vbcr)
'				response.Write ("			</tr>" & vbcr)
'				
'				do while not rsSTMTs.EOF				
'					response.Write ("<tr>" & vbcr)
'					response.Write ("<td>" & rsSTMTs("s_TranDate") & "</td>" & vbcr)
'					response.Write ("<td>" & FormatCurrency( rsSTMTs("s_TranAmount"),2,true,false) & "</td>" & vbcr)
'					response.Write ("<td>" & rsSTMTs("s_TranDescr") & "</td>" & vbcr)
'					response.Write ("</tr>" & vbcr)
'				loop
				
'				response.Write ("		</table>" & vbcr)
'				response.Write ("	</td>" & vbcr)	
'				response.Write ("</tr>" & vbcr)
				
'			end if
				
		  	rsHTRptTop.MoveNext
		loop		
	end if
	response.Write("</table>" & vbcr)		
end sub

'-------------------------------------------------------------------------------------------
sub WriteGUReport(TeamID,SDate,EDate)

	response.Write("<table cellpadding=2 cellspacing=2 width=75% border=1 class=smalltextblack>" & vbcr)
	
	response.Write ("<tr>" & vbcr)
	response.Write ("<th bgcolor=#778899>Team</th>" & vbcr)
	response.Write ("<th bgcolor=#778899>Team Members</th>" & vbcr)
	response.Write ("<th bgcolor=#778899>SSN</th>" & vbcr)
	response.Write ("<th bgcolor=#778899>Total Dollars</th>" & vbcr)	
	response.Write ("<th bgcolor=#778899>Type of Transaction</th>" & vbcr)
	response.Write ("</tr>" & vbcr)
	
	if len(SDate) = 0 then
		SDate = "1/1/2000"
	end if
	
	if len(EDate) = 0 then
		EDate = "12/31/2020"
	end if
	
	'tid = 0
	if TeamID = "ALL" then
		sql = "abbott_HomeTeamGrossUp @StartDate='" & SDate & "', @EndDate='" & EDate & "'"
	else
		sql = "abbott_HomeTeamGrossUpByTeam @TeamID=" & TeamID & ", @StartDate='" & SDate & "', @EndDate='" & EDate & "'"
	end if
	
	set rsHTGrossUpRpt = cn.Execute(sql)
	if not rsHTGrossUpRpt.EOF then
		do while not rsHTGrossUpRpt.EOF								
			'if tid <> rsHTGrossUpRpt("TeamID") then
			'	response.Write ("<tr>" & vbcr)
			'	response.Write ("<td colspan=4 bgcolor=#C0C0C0 align=center><b>Team: " & rsHTGrossUpRpt("TeamName") & "</b></td>" & vbcr)
			'	response.Write ("</tr>" & vbcr)				
			'	tid=rsHTGrossUpRpt("TeamID")
			'end if
			if rsHTGrossUpRpt("Type") <> "AWOO" then
				response.Write ("<tr bgcolor=#B0C4DE>" & vbcr)
			else
				response.Write ("<tr>" & vbcr)
			end if
			response.Write ("<td>" & rsHTGrossUpRpt("TeamName") & "&nbsp;</td>" & vbcr)
			response.Write ("<td>" & rsHTGrossUpRpt("FirstName") & " " & rsHTGrossUpRpt("LastName") & "</td>" & vbcr)
			response.Write ("<td>" & rsHTGrossUpRpt("u_SSN") & "</td>" & vbcr)
			response.Write ("<td>" & rsHTGrossUpRpt("TotalPoints") & "</td>" & vbcr)
			response.Write ("<td>" & rsHTGrossUpRpt("Type") & "</td>" & vbcr)
			response.Write ("</tr>" & vbcr)
			rsHTGrossUpRpt.movenext			
		loop
	end if	
	
	response.Write("</table>")
end sub

'-------------------------------------------------------------------------------------------
sub WriteBudgetReport(TeamID)
	response.Write("<table cellpadding=2 cellspacing=2 width=90% border=1 class=smalltextblack>" & vbcr)
	
	response.Write ("<tr>" & vbcr)
	response.Write ("<th bgcolor=#778899>Team Name</th>" & vbcr)	
	response.Write ("<th bgcolor=#778899>Transaction Type</th>" & vbcr)	
	response.Write ("<th bgcolor=#778899>Budget Change</th>" & vbcr)
	response.Write ("<th bgcolor=#778899>Transaction Status</th>" & vbcr)
	response.Write ("<th bgcolor=#778899>Timestamp</th>" & vbcr)
	response.Write ("<th bgcolor=#778899>AWOO User Name</th>" & vbcr)	
	response.Write ("<th bgcolor=#778899>Changed By</th>" & vbcr)
	response.Write ("</tr>" & vbcr)
		
	if TeamID = "ALL" then
		sql = "abbott_HomeTeamBudgetReportAll"
	else
		sql = "abbott_HomeTeamBudgetReport @TeamID=" & TeamID	
	end if

	set rsBudgetRpt = cn.Execute(sql)
	
	if not rsBudgetRpt.eof then
		do while not rsBudgetRpt.eof
			response.Write ("<tr>" & vbcr)
			response.Write ("<td>" & rsBudgetRpt("TeamName") & "</td>" & vbcr)
			response.Write ("<td>" & rsBudgetRpt("TransactionType") & "</td>" & vbcr)
			response.Write ("<td>" & rsBudgetRpt("BudgetChange") & "</td>" & vbcr)
			response.Write ("<td>" & rsBudgetRpt("TransactionStatus") & "</td>" & vbcr)
			response.Write ("<td>" & rsBudgetRpt("DateAdded") & "</td>" & vbcr)
			response.Write ("<td>" & rsBudgetRpt("UserName") & "</td>" & vbcr)
			response.Write ("<td>" & rsBudgetRpt("ChangedBy") & "</td>" & vbcr)
			response.Write ("</tr>" & vbcr)
			rsBudgetRpt.movenext
		loop
	end if

end sub
'-------------------------------------------------------------------------------------------
sub WriteFastRewardsReport(TeamID)


	sql = "abbott_HomeTeamGetInfo @TeamID=" & TeamID 
	Set rs = cn.Execute(sql)

	if not rs.eof then
		set rsAccnt = Server.CreateObject("ADODB.Recordset")			
		set rsAccnt = oCards.GetAccount(14, rs("GlobalUserID"),true)
		set rsSTMTs = Server.CreateObject("ADODB.Recordset")
						
		acStatus = ""
		IF NOT rsAccnt.EOF THEN
			rsAccnt.Sort="ac_Stamp DESC"
			acStatus = rsAccnt("ac_Status")
		END IF
					
		if acStatus = "ACCOUNT_OPEN" then 				
			IK=rsAccnt("ac_InternalKey")
			curAvailBal = oCards.GetUserAvailBalance(IK)
		else
			curAvailBal = "N/A"
		end if
						
		if acStatus = "ACCOUNT_OPEN" then 
			set rsSTMTs = oCards.GetAccountTransactions(IK)
			rsSTMTs.FILTER = "s_tranType = 4012 or s_tranType = 4040" 'statements				
			response.Write ("<table border=1 cellspacing=2 width=100% cellpadding=2 class=smalltextblack>" & vbcr)
			response.Write ("	<tr>" & vbcr)
			response.Write ("		<th bgcolor=#CCCCCC>HomeTeam</th>" & vbcr)
			response.Write ("		<th bgcolor=#CCCCCC>Current Balance</th>" & vbcr)
			response.Write ("		<th bgcolor=#CCCCCC>Date</th>" & vbcr)
			response.Write ("		<th bgcolor=#CCCCCC>Amount</th>" & vbcr)						
			response.Write ("		<th bgcolor=#CCCCCC>Description</th>" & vbcr)
			response.Write ("	</tr>" & vbcr)
			i=0	
			do while not rsSTMTs.EOF				
				response.Write ("<tr>" & vbcr)
				response.Write ("<td>" & rs("TeamName") & "</td>" & vbcr)
				if i=0 then
					response.Write ("<td>" & curAvailBal & "</td>" & vbcr)
				else
					response.Write ("<td>&nbsp;</td>" & vbcr)
				end if				
				response.Write ("<td>" & rsSTMTs("s_TranDate") & "</td>" & vbcr)
				response.Write ("<td>" & FormatCurrency( rsSTMTs("s_TranAmount"),2,true,false) & "</td>" & vbcr)
				response.Write ("<td>" & rsSTMTs("s_TranDescr") & "</td>" & vbcr)
				response.Write ("</tr>" & vbcr)
				i=1
				rsSTMTs.MoveNext
			loop
					
			response.Write ("		</table>" & vbcr)				
		end if
	else
		msg = "No information available for this program"
	end if

end sub
'-------------------------------------------------------------------------------------------
sub SearchUsers()
	sql="select * from vw_UsersAll vw "

	if request("lastname") <> "" then
		strWhere= " where vw.lastname like '%" & replace(request("lastname"),"'","''") & "%'"
	end if
	
	if request("region") <> "" then
		if strWhere="" then 
			strWhere = " WHERE vw.REGION = '" & request("region") & "'"
		else
			strWhere= strWhere & " AND vw.REGION = '" & request("region") & "'"
		end if
	end if
	
	if request("district") <> "" then
		if strWhere="" then 
			strWhere= " where vw.DISTRICT = '" & request("district") & "'"
		else
	 		strWhere= strWhere & " and vw.DISTRICT = '" & request("district") & "'"
		end if
	end if
	
	if request("reptype") <> "" then
		if strWhere="" then 
			strWhere= " where vw.TER_TYPE_CAT = '" & request("reptype") & "'"
		else
	 		strWhere= strWhere & " and vw.TER_TYPE_CAT = '" & request("reptype") & "'"
		end if
	end if
	
	if len(strAddedUsers) > 0 then
		if strWhere="" then 
			strWhere= " where vw.U_ID not in (" & strAddedUsers & ")"
		else
			strWhere= strWhere & " and vw.U_ID not in (" & strAddedUsers & ")"
		end if
	end if
		
	if strWhere <> "" then 
		sql = sql & strWhere & " and getDate() between IsNULL(vw.TERRITORY_STRT_DT,'01/01/2001') and isNULL(vw.TERRITORY_END_DT,'01/01/2010')  order by vw.Lastname,vw.Firstname"	
	end if

	'response.Write(sql)
	'response.end
	set rsSearch = cn.execute (sql)
	
end sub
'-------------------------------------------------------------------------------------------	
sub GetCurrentUsers()

	if PriorState = "Create" then
		sql = "Select distinct u_ID, LastName, FirstName, JobTitle, TERRITORY_NO, Status from vw_Users vw where (rpttossn in (Select u_ssn from vw_Users vw where u_id=" & userID & ") or u_RPTSTO in (Select PCN from vw_Users vw where u_id=" & userID & ")) and Status in ('A','L')"		
	elseif PriorState = "Edit" then
		sql = "abbott_HomeTeamGetMembers @teamid=" & teamID
	end if
	
	set rs = cn.execute(sql)
	
	if not rs.eof then
		do while not rs.eof
			strAddedUsers = strAddedUsers & rs("U_ID") & ","
			rs.movenext
		loop
			
		'strAddedUsers = left(strAddedUsers,len(strAddedUsers)-1)
		strAddedUsers = strAddedUsers & UserID
	end if

end sub

'-------------------------------------------------------------------------------------------	
sub AddNonHierarchalUsers()
	if len(trim(NHUsers)) > 0 then
		if PriorState = "Create" then
			sql = ""
			' nothing happens here for Create
		elseif PriorState = "Edit" then
			sql = "abbott_HomeTeamAddNonHierarchalUsers @teamid=" & teamID & ",@UserList='" & NHUsers & "'"
			cn.execute(sql)
		end if
	end if
	
end sub
'-------------------------------------------------------------------------------------------	
sub WriteHiddenVariables()
	
	dim arr1, arr2
	
	i=0	
	if len(strAddedUsers) > 0 then
		arr1 = split(strAddedUsers,",")
		
		for i = 0 to ubound(arr1)
			response.write("<input type=hidden name=txt" & trim(arr1(i)) & " value=" & request("txt" & trim(arr1(i))) & ">" & vbcr)
		next 
	end if
	
	if len(NHUsersList) > 0 then
		arr2 = split(NHUsersList,",")
		
		for j = 0 to ubound(arr2)
			response.write("<input type=hidden name=txt" & trim(arr2(j)) & " value=" & request("txt" & trim(arr2(j))) & ">" & vbcr)
		next
	end if
		 
end sub

'-------------------------------------------------------------------------------------------	
sub AddGlobalUser(FirstName,MI,LastName,u_SSN,Street1,Street2,City,State,Zip5,emailID,globalID,ad_ID,em_ID)

	'Check to see if global user already exists for this manager
	set rsGblUser = server.CreateObject("ADODB.Recordset")	
	sql = "abbott_HomeTeamGlobalUserCheck @ManagerID=" & UserID	
	set rsGblUser = cn.execute(sql)
	if not rsGblUser.eof then
		globalID = rsGblUser("GlobalUserID")
		rsGblUser.Close
	else
		globalID = 0
	end if
	set rsGblUser = nothing
		
	'If global user does not exist, create
	if globalID = 0 then
		set rsAbbottUser = server.CreateObject("ADODB.Recordset")	
'***		sql = "SELECT FirstName,MI,LastName,u_SSN,Street1,Street2,City,State,Zip5,emailID,master.dbo.ud_CCEncrypt(u_SSN) enc_SSN from users where u_id=" & UserID
		sql = "SELECT FirstName,MI,LastName,u_SSN,Street1,Street2,City,State,Zip5,emailID from users where u_id=" & UserID
		set rsAbbottUser = cn.execute(sql)
			
		if not rsAbbottUser.EOF then
'***			PersonGovtIDType = "S"
			FirstName = rsAbbottUser("FirstName")
			MI = rsAbbottUser("MI")
			LastName = rsAbbottUser("LastName")
			u_SSN = rsAbbottUser("u_SSN")
'***			enc_SSN = rsAbbottUser("enc_SSN")
			Street1 = rsAbbottUser("Street1")
			if len(rsAbbottUser("Street2")) > 0 then
				Street2 = rsAbbottUser("Street2")
			else
				Street2 = ""
			end if
			City = rsAbbottUser("City")
			State = rsAbbottUser("State")
			Zip5 = rsAbbottUser("Zip5")
			
			if isnull(Street1) or isnull(City) or isnull(State) then
				Street1 = "488 Madison Ave"
				Street2 = "3rd Floor"
				City = "New York"
				State = "NY"
				Zip5 = "10022"
				BadAddress = true					
			end if
				
			if u_SSN = "PCN116" then
				u_SSN = null
'***				enc_SSN = ""
'***				PersonGovtIDType = ""
			end if
				
			if isnull(rsAbbottUser("emailID")) then
				emailID	= FirstName + "." + LastName + "@fakeabbottfr.com"
			else
				emailID	 = rsAbbottUser("emailID")
			end if 
		end if

		Set cmdNewUser = server.CreateObject("ADODB.Command")	
		With cmdNewUser
			set .ActiveConnection = cnGU
			.CommandText="upAddUserAccount"
			.CommandType= adCmdStoredProc
			.Parameters.Append  .CreateParameter("@AccountType", adChar, adParamInput, 1, "C")	
			.Parameters.Append  .CreateParameter("@IsPerson", adInteger, adParamInput, 0, 1)	
			.Parameters.Append  .CreateParameter("@ClientID", adInteger, adParamInput,0, 6)  ' client ID (6) or programid (14)
			.Parameters.Append  .CreateParameter("@CustAccountCode", adVarChar, adParamInput, 40, "") 
			.Parameters.Append  .CreateParameter("@AltAccountCode", adVarChar, adParamInput, 40, UserID) 
			.Parameters.Append  .CreateParameter("@AccountName", adVarChar, adParamInput, 80, "") 
			.Parameters.Append  .CreateParameter("@PersonNamePrefix", adVarChar, adParamInput, 20, "") 
			.Parameters.Append  .CreateParameter("@PersonFirstGivenName", adVarChar, adParamInput, 40, FirstName)	
			.Parameters.Append  .CreateParameter("@PersonSecondGivenName", adVarChar, adParamInput, 40, MI)	
			.Parameters.Append  .CreateParameter("@PersonFamilyName", adVarChar, adParamInput, 60, LastName)					
			.Parameters.Append  .CreateParameter("@PersonNameSuffix", adVarChar, adParamInput, 20, "")	
			.Parameters.Append  .CreateParameter("@PersonNaturalFullName", adVarChar, adParamInput, 200, "")	
			.Parameters.Append  .CreateParameter("@PersonBuiltFullName", adVarChar, adParamInput, 200, "")	
			.Parameters.Append  .CreateParameter("@PersonGovtIDType", adChar, adParamInput, 1, PersonGovtIDType)	
			.Parameters.Append  .CreateParameter("@PersonGovtIDCodeOneWayCrypted", adVarChar, adParamInput, 200, "")	
			.Parameters.Append  .CreateParameter("@PersonGovtIDCodeTwoWayCrypted", adVarChar, adParamInput, 200, enc_SSN)	
			.Parameters.Append  .CreateParameter("@AccountStatus", adChar, adParamInput,1, "E") 				
			.Parameters.Append  .CreateParameter("@UserAccountID", adInteger, adParamOutput)
			.Execute
			globalID=.Parameters("@UserAccountID").value		
		end with
		set cmdNewUser = nothing
		
'***		Set cmdNewUser = server.CreateObject("ADODB.Command")	
'		With cmdNewUser
'			set .ActiveConnection = cnGU
'			.CommandText="AddNewGlobalUser"
'			.CommandType= adCmdStoredProc
'			.Parameters.Append  .CreateParameter("@gu_FirstName", adVarChar, adParamInput, 50, FirstName)	
'			.Parameters.Append  .CreateParameter("@gu_MiddleName", adVarChar, adParamInput, 50, MI)	
'			.Parameters.Append  .CreateParameter("@gu_LastName", adVarChar, adParamInput, 100, LastName)					
'			.Parameters.Append  .CreateParameter("@gu_SSN", adVarChar, adParamInput, 9, u_SSN)	
'			.Parameters.Append  .CreateParameter("@gu_OldId", adInteger, adParamInput,0, 0) 
'			.Parameters.Append  .CreateParameter("@gu_cl_ID", adInteger, adParamInput,0, 6)  ' client ID (6) or programid (14)
'			.Parameters.Append  .CreateParameter("@gu_rs_ID", adInteger, adParamInput,0, 0) 				
'			.Parameters.Append  .CreateParameter("@UserReturn", adInteger, adParamOutput)
'			.Execute
'			globalID=.Parameters("@UserReturn").value		
'		end with
'		set cmdNewUser = nothing
		
		Set cmdNewAddress = server.CreateObject("ADODB.Command")	
		With cmdNewAddress
			set .ActiveConnection = cnGU
			.CommandText="upAddMailAddress"
			.CommandType= adCmdStoredProc
			.Parameters.Append  .CreateParameter("@MailAddressID", adInteger, adParamOutput)
			.Parameters.Append  .CreateParameter("@UserAccountID", adInteger, adParamInput,, globalID)	
			.Parameters.Append  .CreateParameter("@MailAddressType", adChar, adParamInput, 1, "H")	
			.Parameters.Append  .CreateParameter("@Line1", adVarChar, adParamInput, 80, Street1)	
			.Parameters.Append  .CreateParameter("@Line2", adVarChar, adParamInput, 80, Street2)	
			.Parameters.Append  .CreateParameter("@Line3", adVarChar, adParamInput, 80, "")	
			.Parameters.Append  .CreateParameter("@Line4", adVarChar, adParamInput, 80, "")
			.Parameters.Append  .CreateParameter("@CityTown", adVarChar, adParamInput, 40, City)
			.Parameters.Append  .CreateParameter("@StateProvince", adVarChar, adParamInput, 40, State)
			.Parameters.Append  .CreateParameter("@PostalCode", adVarChar, adParamInput, 20, Zip5)
			.Parameters.Append  .CreateParameter("@Country", adVarChar, adParamInput, 80, "")
			.Parameters.Append  .CreateParameter("@CityLine", adVarChar, adParamInput, 120, "")
			.Parameters.Append  .CreateParameter("@UseCityLine", adSmallInt, adParamInput,, 0)
			.Parameters.Append  .CreateParameter("@IsPreferred", adSmallInt, adParamInput,, 1)			
			.Execute		
			ad_ID=.Parameters("@MailAddressID").value		
		end with
		set cmdNewAddress = nothing

'***		Set cmdNewAddress = server.CreateObject("ADODB.Command")	
'		With cmdNewAddress
'			set .ActiveConnection = cnGU
'			.CommandText="AddGlobalUserAddress"
'			.CommandType= adCmdStoredProc
'			.Parameters.Append  .CreateParameter("@gu_ID", adInteger, adParamInput,, globalID)	
'			.Parameters.Append  .CreateParameter("@ad_ct_ID", adInteger, adParamInput,, 5)	
'			.Parameters.Append  .CreateParameter("@ad_StreetAddr1", adVarChar, adParamInput, 50, Street1)	
'			.Parameters.Append  .CreateParameter("@ad_StreetAddr2", adVarChar, adParamInput, 50, Street2)	
'			.Parameters.Append  .CreateParameter("@ad_StreetAddr3", adVarChar, adParamInput, 50, "")	
'			.Parameters.Append  .CreateParameter("@ad_StreetAddr4", adVarChar, adParamInput, 50, "")
'			.Parameters.Append  .CreateParameter("@ad_City", adVarChar, adParamInput, 50, City)
'			.Parameters.Append  .CreateParameter("@ad_State", adVarChar, adParamInput, 50, State)
'			.Parameters.Append  .CreateParameter("@ad_PostalCode", adVarChar, adParamInput, 50, Zip5)
'			.Parameters.Append  .CreateParameter("@ad_Country", adVarChar, adParamInput, 50, "")
'			.Parameters.Append  .CreateParameter("@ad_Default", adSmallInt, adParamInput,, 1)			
'			.Parameters.Append  .CreateParameter("@AddReturn", adSmallInt, adParamOutput)
'			.Execute		
'			ad_ID=.Parameters("@AddReturn").value		
'		end with
'		set cmdNewAddress = nothing
		
'***		Set cmdNewEmail = server.CreateObject("ADODB.Command")	
'		With cmdNewEmail
'			set .ActiveConnection = cnGU
'			.CommandText="AddNewEMailAddress"
'			.CommandType= adCmdStoredProc
'			.Parameters.Append  .CreateParameter("@em_gu_ID", adInteger, adParamInput,, globalID)	
'			.Parameters.Append  .CreateParameter("@em_ct_ID", adInteger, adParamInput,, 1)	
'			.Parameters.Append  .CreateParameter("@em_Address", adVarChar, adParamInput, 50, emailID)	
'			.Parameters.Append  .CreateParameter("@em_Default", adInteger, adParamInput,, 1)	
'			.Parameters.Append  .CreateParameter("@EmailReturn", adInteger, adParamOutput)
'			.Execute		
'			em_ID=.Parameters("@EmailReturn").value		
'		end with
'		set cmdNewEmail = nothing					

		Set cmdNewEmail = server.CreateObject("ADODB.Command")	
		With cmdNewEmail
			set .ActiveConnection = cnGU
			.CommandText="upAddEmailAddress"
			.CommandType= adCmdStoredProc
			.Parameters.Append  .CreateParameter("@UserAccountID", adInteger, adParamInput, , globalID)	
			.Parameters.Append  .CreateParameter("@EMailAddressType", adChar, adParamInput, 1, "W")	
			.Parameters.Append  .CreateParameter("@EmailAddress", adVarChar, adParamInput, 120, emailID)	
			.Parameters.Append  .CreateParameter("@IsPreferred", adInteger, adParamInput, , 1)	
			.Parameters.Append  .CreateParameter("@EmailAddressID", adInteger, adParamOutput)
			.Execute		
			em_ID=.Parameters("@EmailAddressID").value		
		end with
		set cmdNewEmail = nothing					

	end if
end sub

'-------------------------------------------------------------------------------------------	
function CreateFastRewardsCard(globalID,LastName,FirstName,STREET1,STREET2,City,State,Zip5,EmailID)

	dim vArr(11)		
	' call to clarity component to create a Fast Rewards card		
	vArr(0) = globalID
	vArr(1) = 14
	vArr(2) = LastName
	vArr(3) = FirstName
	vArr(4) = STREET1
	vArr(5) = STREET2
	vArr(6) = City
	vArr(7) = State
	vArr(8) = Zip5
	vArr(9) = "USA"
	vArr(10) = EmailID
	vArr(11) = "physical"
	
	CreateFastRewardsCard = objClarity.CreateAccount(vArr)

end function

'-------------------------------------------------------------------------------------------	
function GetExternalKey(IK)

	dim ExtKey
	ExtKey = 0
		
	sql = "Select ac_externalKey From Accounts where ac_InternalKey=" & IK
	set rsCC = cnCC.Execute(sql)
			
	if not rsCC.EOF then
		ExtKey = rsCC("ac_externalKey")
	end if
	
	GetExternalKey = ExtKey
	
end function

'-------------------------------------------------------------------------------------------	
sub InsertFastRewards(globalID)
	' insert into fast rewards db							
	sql = "INSERT INTO dbo.Users (u_ID, u_Card_No, u_Password, u_DK, u_Prog_Desc, u_Program) "
	sql = sql & " SELECT " & globalID & ", ac_CNum, master.dbo.ud_CCEncrypt(ac_CVV2), 5880, 'Abbott Home Team', 14"
	sql = sql & " FROM GOLDFINGER.CLARITYCARDS.dbo.vw_AccountsInfo" 
	sql = sql & " WHERE p_ID=14 AND u_mpgProgUserid =" & globalID
			
	'@u_DK is the programís DK number				
	cnFR.Execute(sql)
end sub

'-------------------------------------------------------------------------------------------	
function CreateNewHomeTeam(TeamName,TotalBudget,FastRewardBudget,MemberBudget,StartDate,EndDate,UserID,UserList,ExtKey,globalID)

	dim TeamID

	' create team									
	Set cmdNewTeam = server.CreateObject("ADODB.Command")	
	With cmdNewTeam
		set .ActiveConnection = cn
		.CommandText="abbott_HomeTeamCreate"
		.CommandType= adCmdStoredProc
		.Parameters.Append  .CreateParameter("@TeamName", adVarChar, adParamInput, 19, TeamName)
		.Parameters.Append  .CreateParameter("@TotalBudget", adSingle, adParamInput,, TotalBudget)	
		.Parameters.Append  .CreateParameter("@FastRewardBudget", adSingle, adParamInput,, FastRewardBudget)	
		.Parameters.Append  .CreateParameter("@MemberBudget", adSingle, adParamInput,, MemberBudget)					
		.Parameters.Append  .CreateParameter("@StartDate", adDBTimeStamp, adParamInput,, StartDate)
		.Parameters.Append  .CreateParameter("@EndDate", adDBTimeStamp, adParamInput,, EndDate)							
		.Parameters.Append  .CreateParameter("@ManagerID", adInteger, adParamInput,, UserID)						
		.Parameters.Append  .CreateParameter("@UserList", adVarChar, adParamInput,1000, UserList)						
		.Parameters.Append  .CreateParameter("@ExtKey", adInteger, adParamInput,,ExtKey)
		.Parameters.Append  .CreateParameter("@gu_ID", adInteger, adParamInput,,globalID)
		.Parameters.Append  .CreateParameter("@AddedBy", adInteger, adParamInput,,session("uid"))					
		.Parameters.Append  .CreateParameter("@TeamID", adInteger, adParamOutput)
		.Execute		
		TeamID=.Parameters("@TeamID").value		
	end with
	set cmdNewTeam = nothing
	
	CreateNewHomeTeam = TeamID	
end function

'-------------------------------------------------------------------------------------------	
sub DistributeFunds(UserList, UserID, TeamID)

	dim UserArray
	
	' check if team users have a card, if not, create one
	UserArray = split(UserList,",")
				
	CheckClarityCard(UserID)
	AddPoints UserID,TeamID,UserID
	for i= 0 to ubound(UserArray)
		CheckClarityCard(UserArray(i)) ' will need to check for card
		AddPoints UserArray(i),TeamID,UserID ' add points to the card and tracking table
	next
	
end sub

'-------------------------------------------------------------------------------------------	
sub AssignManagerToHomeTeam(tID)

	dim FirstName
	dim MI
	dim LastName
	dim u_SSN
	dim Street1
	dim Street2				
	dim City
	dim State
	dim Zip5						
	dim	emailID
	dim globalID
	dim ad_ID
	dim em_ID
	dim vReturn	
	
	AddGlobalUser FirstName,MI,LastName,u_SSN,Street1,Street2,City,State,Zip5,emailID,globalID,ad_ID,em_ID
	' if globalID does not exist by this point, an error has occured
	if globalID <> 0 then
		
		vReturn = CreateFastRewardsCard(globalID,LastName,FirstName,STREET1,STREET2,City,State,Zip5,EmailID)			
		' check if vReturn <> a number then rollback
		if isnumeric(vReturn) then

			ExtKey = GetExternalKey(vreturn)
			InsertFastRewards(globalID)			

			sql = "abbott_HomeTeamAddManager @TeamID=" & tID & ",@ManagerID=" & UserID & ",@gu_ID=" & globalID & ",@ExtKey=" & ExtKey & ",@AddedBy=" & session("uid")
			cn.execute(sql)			
																							
			if BadAddress = true then
				SendEmail TeamName,TeamID,UserID, LastName, FirstName
			end if

			msg = "<br><br><font color=red>Manager Assigned</font><br>"
		else			
			DeleteGlobalUser globalID,ad_ID,em_ID
			msg = "<br><br><font color=red>Error: " & vReturn & "<br>Fast Rewards Card creation failed</font><br>"
		end if					
	else
		msg = "<br><br><font color=red>Global user creation failed</font><br>"
	end if	

end sub

'-------------------------------------------------------------------------------------------	
sub WriteHomeTeamArray()
	response.write("<script language=""javascript"">" & vbcr)
	sql="Select teamid, teamname from tbHomeTeam"
	set rsHomeTeamName = cn.execute(sql)

	if not rsHomeTeamName.eof then
		i=0
		response.write("var HomeTeamArray=new Array();" & vbcr)
		do while not rsHomeTeamName.eof			
			response.write("HomeTeamArray[" & i & "]='" & rsHomeTeamName("TeamName") & "';" & vbcr)
			i=i+1
			rsHomeTeamName.movenext
		loop
	end if
	response.write("</script>")
end sub

'-------------------------------------------------------------------------------------------	
%>

<script language="javascript" src="calendar/codethatcalendarstd.js"></script>	
<script language="javascript">
	var c1 = new CodeThatCalendar(caldef1);
</script>

<SCRIPT Language="javascript" src="../includes/formcheck.js"></SCRIPT>

<input type="hidden" name="state" id="state">
<p class="titlesmallblue"><em>
<%

if request("state") = "Init" or request("state") = "Search" or request("state") = "Setup" then
	response.Write("Create Home Team")
elseif request("state") = "Edit" or request("state") = "AssignManager" then
	response.Write("Edit Home Team")
elseif request("state") = "HTReport" then
	response.Write("Home Team Report")
elseif request("state") = "GUReport" then
	response.Write("Gross Up Report")
elseif request("state") = "BudgetReport" then
	response.Write("Budget Report")
end if
%>
</em></p>

<% =msg %>

<% if error = false then %>

<!-----------------------------------------Menu------------------------------------------->
<% if len(request("state")) = 0 then%>

	<table width=90% border=0 cellpadding=1 cellspacing=1 ID="Table1">
		<tr>
			<td><a href="#" onclick="Javascript:SubmitForm(0)">Create Home Team</a></td>
			<td><a href="#" onclick="Javascript:SubmitForm(4)">Edit Existing Home Team</a></td>
			<td><a href="#" onclick="Javascript:SubmitForm(5)">Home Team Report</a></td>			
			<td><a href="#" onclick="Javascript:SubmitForm(6)">Gross Up Report</a></td>			
			<td><a href="#" onclick="Javascript:SubmitForm(7)">Budget Report</a></td>
			<td><a href="#" onclick="Javascript:SubmitForm(8)">FastRewards Report</a></td>
		</tr>
	</table>	 	 

<!----------------------------------Search or Init------------------------------------------->
<% elseif request("state") = "Init" or request("state") = "Search" or request("state") = "SearchManager" or request("state") = "SearchManagerResults" then%>

	<%	if request("state") = "Init" or request("state") = "Search" then
			stateValue = "Search"
		else
			stateValue = "SearchManagerResults"
			response.Write("<input type=hidden name=selTeam value=" & teamid & ">")
		end if
	%>
	<script language=javascript>
		function CheckKey() {
			if ((window.event) && (window.event.keyCode == 13))
			{				
				form1.state.value="<%=stateValue%>";
				form1.submit();
			}
		}
	</script>

	<i>Search for users below:</i>
	<p></p>

	<% if strWhere="" and (request("state") = "Search" or request("state") = "SearchManagerResults") then %>
		<div align="left"><font color="#FF0000" size="4"><strong>At least one criteria 
		must be selected</strong></font> </div>
	<%end if%>
	  
	<table width="100%" border="0" cellspacing="0" cellpadding="2" ID="Table2" class=smalltextblack>
	<tr>
		<td>	
			<table width="60%" border="1" cellspacing="0" cellpadding="2" ID="Table3" class=smalltextblack>
			<tr> 
				<td>Last Name </td>
				<td><input name="lastname" type="text" value="<%=request("lastname")%>" size="30" ID="Text1" onkeypress="CheckKey()"></td>
			</tr>
	          
			<tr> 
				<td>Region</td>
				<td> 
					<%
						sql="select distinct REGION from TerrCode where REGION <> '' order by Region"
						set rs = cn.execute(sql)
					%>
					
					<select name="region" ID="Select3" onkeypress="CheckKey()">
					<option value="" selected>Select Region</option>
					<%
						do while not rs.eof
							sSelect=""
							if request("region") = trim(rs(0)) then sSelect = "selected"
					%>
							<option value="<%=trim(rs(0))%>" <%=sSelect%>><%=trim(rs(0))%></option>
					<%
							rs.MoveNext
						Loop
					%>
				</select>
			</td>
			</tr>
			<tr> 
				<td>District</td>
				<td> 
				<%
					sql="select distinct District from TerrCode order by District"
					set rs = cn.execute(sql)
				%>
				<select name="district" ID="Select4" onkeypress="CheckKey()">
					<option value="" selected>Select District</option>
					<%
						do while not rs.eof
							sSelect=""
							if request("district") = trim(rs(0)) then sSelect = "selected"
					%>
					<option value="<%=trim(trim(rs(0)))%>" <%=sSelect%>><%=trim(trim(rs(0)))%></option>
					<%
							rs.MoveNext
						Loop
					%>
				</select> </td>
			</tr>
			<tr> 
				<td>User Type</td>
				<td> 
					<%'B.B. 02/19/04
						sql = "select distinct TER_TYPE_CAT from vw_terrCode where not TER_TYPE_CAT is NULL and TER_TYPE_CAT <> '' order by TER_TYPE_CAT"
						set rs = cn.execute(sql)
					%>
				<select name="reptype" ID="Select5" onkeypress="CheckKey()">
					<option value="" selected>Select User Type</option>
					<%
						do until rs.eof
							sSelect=""
							if request("reptype") = trim(rs(0)) then sSelect = "selected"
					%>
						<option value="<%=trim(rs(0))%>" <%=sSelect%>><%=trim(rs(0))%></option>
					<%
						rs.MoveNext
						Loop
					%>
				</select> </td>
			</tr>
			
			<% if request("state") = "Init" or request("state") = "Search" then %>
			<tr>
				<td colspan=2 align=center>OR</td>
			</tr>			
			
			<tr>
				<td>Blank Manager</td>
				<td><input type=button value="Select" onclick="Javascript:SelectManager(-1)" ID="Button1" NAME="Button1"></td>
			</tr>
			<% end if %>
			
			</table>

			
			<p>
			<% if request("state") = "Init" or request("state") = "Search" then %>
				<input type="submit" value="SEARCH" onclick="javascript:SubmitForm(1)" ID="Submit1" NAME="Submit1">
			<% else %>
				<input type="submit" value="SEARCH" onclick="javascript:SubmitForm(13)" ID="Submit2" NAME="Submit2">
			<%end if %>
			</p>	
			
		<% if strWhere <> "" and (request("state") = "Search" or request("state") = "SearchManagerResults") THEN %>	
			<% if not rsSearch.eof then	%>
			<table width="60%" border="1" cellspacing="1" cellpadding="1" class=smalltextblack ID="Table4">						
				<tr bgcolor="#CCCCCC"> 
					<td>Name</td>
					<td>Job Title</td>
					<td>Region</td>
					<td>District</td>
					<td>Territory #</td>
					<td>Select</td>
				</tr>
				<% do while not rsSearch.eof %>								  	
						<tr> 
							<td><%=rsSearch("LASTNAME") & " " & rsSearch("FIRSTNAME")%></td>						
							<td><%=trim(rsSearch("JOBTITLE"))%></td>
							<td><%=trim(rsSearch("REGION"))%></td>
							<td><%=trim(rsSearch("DISTRICT"))%></td>
							<td><%if len(trim(rsSearch("TERRITORY_NO")))>0 then response.Write(trim(rsSearch("TERRITORY_NO"))) else response.Write("&nbsp;") end if%></td>
							
							<% if request("state") = "Search" then %>
							<td><input type=button value="Select" onclick="Javascript:SelectManager(<%=trim(rsSearch("u_ID"))%>)" ID="Button2" NAME="Button2"></td>
							<% else %>
							<td><input type=button value="Select" onclick="Javascript:SelectManagerFromBlank(<%=trim(rsSearch("u_ID"))%>)" ID="Button3" NAME="Button3"></td>
							<% end if %>
          				</tr>
		  		<%
			 			rsSearch.MoveNext
			  		Loop
				end if
				%>
				
			</table>		
	      
		<% end if %>	
		</td>
	</tr>
	</table>
	
<!--------------------------------------Setup/Creation--------------------------------------->	

<% elseif request("state") = "Setup" or (request("state") = "AddNHUsers" and PriorState = "Create") then%>

	<%	
		if len(trim(FastRewardBudget)) = 0 then
			FastRewardBudget=0
		end if
		
		if len(trim(MemberBudget)) = 0 then
			MemberBudget=0
		end if
	
	%>
	
	<% WriteHomeTeamArray() %>
	<table cellpadding=3 cellspacing=2 class=smalltextblack ID="Table5">
		<tr>
			<td>Team Name:</td>
			<td><input name="TeamName" id="TeamName" type="text" maxlength=19 value="<%=TeamName%>"></td>
		</tr>
		<tr>
			<td>Team Total Budget:</td>
			<td><input name="TotalBudget" id="TotalBudget" type="text" value="<%=TotalBudget%>"></td>
		</tr>		
		<tr>
			<td>Fast Reward Card Budget:</td>
			<td><input name="FastRewardBudget" id="FastRewardBudget" type="text" value="<%=FastRewardBudget%>" <%if clng(UserID)=-1 then%> readonly<%end if%>></td>
		</tr>		
		<tr>
			<td>AWOO Card Budget:</td>
			<td><input name="MemberBudget" id="MemberBudget" type="text" value=<%=MemberBudget%>></td>
		</tr>		
		<tr>
			<td>Start Date:</td>
			<td><input name="StartDate" id="StartDate" type="text" readonly value="<%=StartDate%>"><a href="javascript:c1.popup('StartDate');"><IMG SRC="images/calendar.gif" BORDER=0></a></td>
		</tr>
		<tr>
			<td>End Date:</td>
			<td><input name="EndDate" id="EndDate" type="text" readonly value="<%=EndDate%>"><a href="javascript:c1.popup('EndDate');"><IMG SRC="images/calendar.gif" BORDER=0></a></td>
		</tr>
	</table>
	
	<br>
	
	<table cellpadding=0 border="0" width=60% class=smalltextblack ID="Table6">						
		<tr>
			<td align=center><input type=button value="Split Budget" onclick="javascript:SplitBudget()" ID="Button4" NAME="Button4"></td>
		</tr>
	</table>
		
	<br>
	
	<%
		sql = "Select distinct u_ID, LastName, FirstName, JobTitle, TERRITORY_NO, Status from vw_Users vw where (rpttossn in (Select u_ssn from vw_Users vw where u_id=" & userID & ") or u_RPTSTO in (Select PCN from vw_Users vw where u_id=" & userID & ")) and Status in ('A','L')"
		'sql = "Select distinct u_ID, LastName, FirstName, JobTitle, TERRITORY_NO, Status from vw_Users vw where rpttossn in (Select u_ssn from vw_Users vw where u_id=" & userID & ") and Status in ('A','L')"
		Set rsUsers = cn.execute (sql)
		i=1
		if not rsUsers.EOF or len(NHUsersList) > 0 then  
	
	%>
	
	<table border="1" cellspacing="1" cellpadding="1" width=75% class=smalltextblack ID="Table7">
		<tr>
			<td colspan=5>Users that report to <% =GetManagerName(UserID)%></td>
			<td><input type=text name="txt<% =UserID %>" id=BudgetTxt0 value="<%=request("txt" & UserID) %>" size=5 <%if clng(UserID)=-1 then%> readonly<%end if%>></td>
		</tr>
		
		<tr bgcolor="#CCCCCC">
			<td>Select</td>			
			<td>User Name</td>
			<td>Territory #</td>
			<td>Job Title</td>
			<td>Status</td>
			<td>Distribution</td>
		</tr>						
		
		<%		
				
				do while not rsUsers.EOF
		%>
				<tr>			
					<td><input type="checkbox" name="UserCheck" id="UserCheck<%=i%>" value="<% =rsUsers("u_ID") %>" checked></td>
					<td><%=rsUsers("FirstName") & " " & rsUsers("LastName")%></td>
					<td><% Response.write(rsUsers("TERRITORY_NO"))%></td>
					<td><% Response.write(rsUsers("JobTitle"))%></td>
					<td><%=rsUsers("Status")%></td>
					<td><input type=text name="txt<% =rsUsers("u_ID")%>" id=BudgetTxt<%=i%> value="<%=request("txt" & rsUsers("u_ID")) %>" size=5></td>
					
				</tr>					
		<%			i=i+1	
					rsUsers.movenext
				loop					
		%>		
		</table>
		
		<%
			if len(NHUsersList) > 0 then
				sql = "Select distinct u_ID, LastName, FirstName, JobTitle, TERRITORY_NO, Status from vw_Users vw where u_id in(" & NHUsersList & ") and Status in ('A','L')"		
				Set rsNHUsers = cn.execute (sql)
			
				if not rsNHUsers.EOF then  	
		%>

		<br>		
		<table border="1" cellspacing="1" cellpadding="1" width=75% class=smalltextblack ID="Table8">
		<tr><td colspan=6>Non-Hierarchal Users</td></tr>
		<tr bgcolor="#CCCCCC">
			<td>Select</td>			
			<td>User Name</td>
			<td>Territory #</td>
			<td>Job Title</td>
			<td>Status</td>
			<td>Distribution</td>
		</tr>						
		
		<%						
					do while not rsNHUsers.EOF
		%>
				<tr>			
					<td><input type="checkbox" name="UserCheck" id="UserCheck<%=i%>" value="<% =rsNHUsers("u_ID") %>" checked></td>
					<td><%=rsNHUsers("FirstName") & " " & rsNHUsers("LastName")%></td>
					<td><% Response.write(rsNHUsers("TERRITORY_NO"))%></td>
					<td><% Response.write(rsNHUsers("JobTitle"))%></td>
					<td><%=rsNHUsers("Status")%></td>
					<td><input type=text name="txt<% =rsNHUsers("u_ID")%>" id=BudgetTxt<%=i%> value="<%=request("txt" & rsNHUsers("u_ID")) %>" size=5></td>
					
				</tr>					
		<%				i=i+1	
						rsNHUsers.movenext
					loop		
				end if
			end if
		%>		
		</table>
		
		
		<br>
		<table cellpadding=0 border="0" width=60% class=smalltextblack ID="Table9">						
			<tr>
				<td align=center><input type="button" name="SearchNonHUsers" value="Add Non-Hierarchal Users" onclick=javascript:SearchNHUsers(0) ID="Button5"></td>
				<td align=center><input type="button" value="Create Team" onclick="javascript:CreateTeam()" ID="Button6" NAME="Button6"></td>
			</tr>
		</table>		
		
		<% else %>
			No users report to <% =GetManagerName(UserID) %>
			
			<table cellpadding=0 border="0" width=60% class=smalltextblack ID="Table10">						
			<tr>
				<td><input type="button" name="SearchNonHUsers" value="Add Non-Hierarchal Users" onclick=javascript:SearchNHUsers(0) ID="Button7"></td>	
			</tr>
			</table>		
		<% end if %>


<!--------------------------------------Edit----------------------------------------------->		
<% elseif request("state") = "Edit" or (request("state") = "AddNHUsers" and PriorState = "Edit") or request("state") = "AssignManager" then%>
	
	<%	sql="Select teamid, teamname, CardBlocked from tbHomeTeam order by TeamName"
		set rsSelect = cn.execute(sql)		
	%>
	
	<% if not rsSelect.eof then%>
		<select name="selTeam" id="selTeam" onchange="javascript:SelectTeam()">
			<option value=0>Please select a Program</option>
		
			<%	do while not rsSelect.eof%>
				<option value=<%=rsSelect("teamid")%> <% if cint(teamid)=cint(rsSelect("teamid")) then%> selected<%end if%><%if rsSelect("CardBlocked")=1 then%> style="color:red"<%end if%>><%if rsSelect("CardBlocked")=1 then%><font color=red><%end if%><%=rsSelect("teamname")%></option>
			<%
					rsSelect.movenext
				loop
			 %>
		</select>
	<% else %>
		No teams to be edited
	<% end if %>
	
	
	<%	if teamid <> 0 then
			'sql = "Select TeamName, TeamCurrentBudget, FastCardCurrentBudget, StartDt, EndDt, ManagerID from tbHomeTeam where TeamID = " & TeamID 
			sql = "abbott_HomeTeamGetInfo @TeamID=" & TeamID 
			Set rsEdit = cn.Execute(sql)
			if not rsEdit.EOF then
				UserID = rsEdit("ManagerID")
				
				if len(rsEdit("TotalPoints")) > 0 then
					ManagerPoints = rsEdit("TotalPoints")
				else
					ManagerPoints = 0
				end if
				QueuedPoints = rsEdit("QueuedPoints")
				
				ProgramOver = rsEdit("Deleted")
				CardBlocked = rsEdit("CardBlocked")
				StartDate = rsEdit("StartDt")
				
				set rsAccnt = Server.CreateObject("ADODB.Recordset")		
				set rsAccnt = oCards.GetAccount(14, rsEdit("GlobalUserID"),true)
				
				acStatus = ""
				IF NOT rsAccnt.EOF THEN
					rsAccnt.Sort="ac_Stamp DESC"
					acStatus = rsAccnt("ac_Status")
				END IF
				
				if acStatus = "ACCOUNT_OPEN" then 				
					IK=rsAccnt("ac_InternalKey")
					curAvailBal = oCards.GetUserAvailBalance(IK)
				else
					curAvailBal = "N/A"
				end if
	%>
		<br><br>
		
		<input type=hidden name="ProgramOver" id="ProgramOver" value=<%=ProgramOver%>>
		<input type=hidden name="CardBlocked" id="CardBlocked" value=<%=CardBlocked%>>
		
		<% if ProgramOver and CardBlocked <> 1 then %>
			<font color=red>This Program is over.  Funds may still be queued and moved live.</font><br>
		<% elseif CardBlocked then %>
			<font color=red>This Program is over and the Fast Rewards card has been blocked.  No further changes are possible.</font><br>
		<% end if %>
		
		<table width=60% border=0 cellspacing=1 cellpadding=1 class=smalltextblack ID="Table11">
			<tr>
				<td>Team Name:</td>
				<td><input type="text" name="TeamName" id="TeamName" readOnly value="<%=rsEdit("TeamName")%>">
					<input type="hidden" name="GlobalUserID" id="GlobalUserID" value="<%=rsEdit("GlobalUserID")%>"></td>
			</tr>
			
			<tr>
				<td>Total Budget:</td>
				<td><input type="text" name="Budget" id="Budget" value="<%=rsEdit("TeamCurrentBudget")%>">
					<input type="hidden" name="BudgetHidden" id="BudgetHidden" value="<%=rsEdit("TeamCurrentBudget")%>"></td>
			</tr>

			<tr>
				<td>Remaining Budget:</td>  
				<td><input type="text" name="Remaining" id="Remaining" readonly value="<%=rsEdit("TeamCurrentBudget")-rsEdit("SpentBudget")%>">&nbsp;(Does not reflect queued dollars)</td>
			</tr>

			<tr>
				<td>Fast Rewards Card:</td>
				<td><input type="text" name="FRBudget" id="FRBudget" value="<%=rsEdit("FastCardCurrentBudget")%>" <% if clng(UserID)=-1 then %> readonly<%end if%>>
				    <input type="hidden" name="FRBudgetHidden" id="FRBudgetHidden" value="<%=rsEdit("FastCardCurrentBudget")%>"></td>
			</tr>

			<tr>
				<td>Fast Rewards Balance:</td>
				<td><input type="text" name="FRBalance" id="FRBalance" readOnly value="<%=curAvailBal%>">&nbsp;(Takes up to 15 mins to update)</td>
			</tr>

			<tr>
				<td>Start Date:</td>
				<td><input type="text" name="StartDate" id="StartDate" value="<%=rsEdit("StartDt")%>" readonly><a href="javascript:c1.popup('StartDate');"><IMG SRC="images/calendar.gif" BORDER=0></a></td>
			</tr>

			<tr>
				<td>End Date:</td>
				<td><input type="text" name="EndDate" id="EndDate" value="<%=rsEdit("EndDt")%>" readonly><a href="javascript:c1.popup('EndDate');"><IMG SRC="images/calendar.gif" BORDER=0></a></td>
			</tr>

			<tr>
				<td colspan=2>&nbsp;</td>				
			</tr>
			
			<tr>
				<td colspan=2 align=center><input type=button value="Save Changes" onclick=javascript:EditTeam('Edit',0) ID="Button8" NAME="Button8"></td>				
			</tr>

		</table>
		
		<br><br>
		<% rsEdit.close %>
		
		<%	TotalQueuedPoints = cint(QueuedPoints)
			TotalLivePoints = cint(ManagerPoints)
			if cint(QueuedPoints) + cint(ManagerPoints) < 0 then
				QueueLarger = 1
			end if
		
			if cint(QueuedPoints) <> 0 then
				HasQueued = 1
			end if
		%>
		<table width=90% border=1 cellspacing=1 cellpadding=1 class="smalltextblack" ID="Table12">
			<% if clng(UserID) <> -1 then %>
				<td colspan=4><b>Manager:</b> <%=GetManagerName(UserID)%></td>	
				<td><%=ManagerPoints%><input type=hidden id="hidden<%=UserID%>" value="<%=ManagerPoints%>" NAME="hidden<%=UserID%>"></td>
				<td><%=QueuedPoints%><input type=hidden id="queued<%=UserID%>" value="<%=QueuedPoints%>" NAME="queued<%=UserID%>"></td>
				<td><input type=text name="txt<%=UserID%>" size=5 ID="Text6"><input type=button name="Adjust" value="Adjust" onclick=javascript:EditTeam('Adjust',<%=UserID%>) ID="Button9"></td>
				<td>&nbsp;</td>
			<% else %>
				<td colspan=8><b>Manager:</b> <%=GetManagerName(UserID)%></td>	
			<% end if %>				
		
			<tr bgcolor="#CCCCCC">
				<td>Name</td>
				<td>Territory #</td>
				<td>Job Title</td>
				<td>Status</td>
				<td>Live Dollars</td>
				<td>Queued Dollars</td>
				<td>Adjustments</td>
				<td>Remove</td>				
			</tr>
			
			<%	'sql = "Select u_id, FirstName, LastName, JobTitle,Status from Users Where u_id in (Select userid from tbHometeamUsers where teamid=" &  TeamID & ") order by LastName"
				sql = "abbott_HomeTeamGetMembers @teamID=" & teamID			
				set rsAssigned = cn.execute(sql)
				if not rsAssigned.EOF then
					do while not rsAssigned.EOF
			%>
					<tr>
						<td><% response.Write(rsAssigned("LastName") & "," & rsAssigned("FirstName")) %></td>
						<td><% =rsAssigned("Territory_No") %></td>
						<td><% =rsAssigned("Jobtitle") %></td>
						
						<% if trim(rsAssigned("Status")) <> "A" and trim(rsAssigned("Status")) <> "L" then
								InvalidStatus=true
						%>						
								<td><font color=red><% =rsAssigned("Status") %></font></td>
						<% else
								InvalidStatus=false
						%>
								<td><% =rsAssigned("Status") %></td>
						<% end if %>
						
						<%
							if len(rsAssigned("TotalPoints")) > 0 then
								UserPoints = cint(rsAssigned("TotalPoints"))
							else
								UserPoints = 0
							end if
							
							TotalQueuedPoints = TotalQueuedPoints + cint(rsAssigned("QueuedPoints"))
							TotalLivePoints = TotalLivePoints + UserPoints

							if cint(rsAssigned("QueuedPoints")) + cint(UserPoints) < 0 then
								QueueLarger = 1
							end if
							
							if cint(rsAssigned("QueuedPoints")) <> 0 then
								HasQueued = 1
							end if
						%>
						<td><%=UserPoints%><input type=hidden id="hidden<%=rsAssigned("u_id")%>" value="<%=UserPoints%>" NAME=hidden<%=rsAssigned("u_id")%>></td>
						<td><%=rsAssigned("QueuedPoints")%><input type=hidden id="queued<%=rsAssigned("u_id")%>" value="<%=rsAssigned("QueuedPoints")%>" NAME=queued<%=rsAssigned("u_id")%>></td>
						<td><input type=text name="txt<%=rsAssigned("u_id")%>" size=5 ID="Text7"><input type=button name="Adjust" value="Adjust" onclick=javascript:EditTeam('Adjust',<%=rsAssigned("u_id")%>) ID="Button10"></td>
						<td><input type=button name="RemoveUser" value="Remove" onclick=javascript:EditTeam('Remove',<%=rsAssigned("u_id")%>) ID="Button11"></td>
					</tr>
			
			<%
						if InvalidStatus then
							warning = "<br><br><font color=red>There are one or more users with a invalid status.  Please remove these users from the program</font>"
						end if
						rsAssigned.movenext						
					loop
					rsAssigned.close
				end if
			%>
			<tr>
				<td colspan=4 align=right>Totals: &nbsp;&nbsp;&nbsp;</td>
				<td><%=TotalLivePoints%></td>
				<td><%=TotalQueuedPoints%></td>
				<td colspan=2>&nbsp;</td>
			</tr>
			</table>
			<input type=hidden name="TotalQueuedPoints" id="TotalQueuedPoints" value=<%=TotalQueuedPoints%>>
			<input type=hidden name="QueueLarger" id="QueueLarger" value="<%=QueueLarger%>">
			<input type=hidden name="HasQueued" id="HasQueued" value="<%=HasQueued%>">
			
			<%=warning%>
			<br><br>
			Other Users That Report To <%=GetManagerName(UserID) %>
			<br>
			
			<table width=80% border=1 cellspacing=1 cellpadding=1 class=smalltextblack ID="Table13">
			<tr bgcolor="#CCCCCC">
				<td>Name</td>
				<td>Job Title</td>
				<td>Status</td>
				<td>Add</td>
			</tr>
			<%
				sql = "Select distinct u_ID, LastName, FirstName, JobTitle, Status,territory_strt_dt from vw_Users vw where (rpttossn in (Select u_ssn from vw_Users vw where u_id=" & userID & ") or u_RPTSTO in (Select PCN from vw_Users vw where u_id=" & userID & ")) and u_ID not in (select userid from tbHometeamUsers where teamid=" & TeamID & ") and Status in ('A','L') order by LastName"
				'sql = "Select distinct u_ID, LastName, FirstName, JobTitle, Status,territory_strt_dt from vw_Users vw where rpttossn in (Select u_ssn from vw_Users vw where u_id=" & userID & ") and u_ID not in (select userid from tbHometeamUsers where teamid=" & TeamID & ") and Status in ('A','L') order by LastName"
				' GDurgha - To test users not added to the program enable the following:
        ' sql = "Select distinct u_ID, LastName, FirstName, JobTitle, Status,territory_strt_dt from vw_UsersAll vw where rpttossn in (Select u_ssn from vw_UsersAll vw where u_id=" & userID & ") and Status in ('A','L') order by LastName"
        set rsUnassigned = cn.execute(sql)
				if not rsUnassigned.EOF then
					do while not rsUnassigned.EOF
					' Added by GDurgha - 04.12.06
					' Checks to see if the user came into the system after the program was started.
					If isDate(StartDate) Then
					 If rsUnassigned("territory_strt_dt") > StartDate Then
					   bgColor = "red"
             fontColor = "white"
             cntNewUsers = cntNewUsers + 1
           Else
             bgColor = "white"		
             fontColor = "black"		
					 End If
					End If
			%>
					<tr bgColor="<%=bgColor%>">
						<td><font color="<%=fontColor%>"><% response.Write(rsUnassigned("LastName") & "," & rsUnassigned("FirstName")) %></font></td>
						<td><font color="<%=fontColor%>"><% =rsUnassigned("Jobtitle") %></font></td>
						<td><font color="<%=fontColor%>"><% =rsUnassigned("Status") %></font></td>
						<td><font color="<%=fontColor%>"><input type=button name="AddUser" value="Add To Program" onclick=javascript:EditTeam('Add',<%=rsUnassigned("u_ID")%>) ID="Button12"></font></td>
					</tr>
			
			<%
						rsUnassigned.movenext
					loop
					rsUnassigned.close
					If cntNewUsers > 0 Then
			%>
			    <tr>
			         <td colspan="4"><b>Note:</b> New users are depicted in red.</td>
			    </tr>
			<%
			    End If
				End if
			%>
			</table>
		
			<br><br>
			
			<center>
			<table width=70% border=0 class=smalltextblack ID="Table14">
				<tr>
					<% if clng(UserID) = -1 then %>
					<td><input type="button" value="Assign Manager" onclick="javascript:AssignManager()" ID="Button13" NAME="Button13"></td>
					<% end if %>
					<td><input type="button" name="SearchNonHUsers" value="Add Non-Hierarchal Users" onclick=javascript:SearchNHUsers(1) ID="Button14"></td>
					<td><input type="button" name="MoveQueued" value="Move Queued Funds Live" onclick=javascript:EditTeam('MoveQueued',0) ID="Button15"></td>
					<% if ProgramOver then %>
						<td><input type="button" name="BlockCard" value="Block Fast Rewards Card" onclick=javascript:EditTeam('Block',0) ID="Button16"></td>
					<% else %>
						<td><input type="button" name="EndProgram" value="End Program" onclick=javascript:EditTeam('End',0) ID="Button17"></td>
					<% end if %>
				</tr>
			</table>
			</center>			

		<% else %>
			Error retrieving Team Information
		<% end if %>

	<% end if %>

<!--------------------------------------HTReport--------------------------------------------->		
<% elseif request("state") = "HTReport" then%>
	
	<%	sql="Select teamid, teamname, CardBlocked from tbHomeTeam order by TeamName"
		set rsSelect = cn.execute(sql)		
	%>
	
	<% if not rsSelect.eof then%>
	
		<select name="selTeam" id="selTeam" onchange="javascript:SelectRptTeam(5)">
			<option value=0>Please select a Program</option>
			<option value="ALL" <% if trim(teamid)="ALL" then%> selected <%end if%>>All Teams</option>
				<%	do while not rsSelect.eof%>
					<option value=<%=rsSelect("teamid")%> <% if trim(teamid)=trim(rsSelect("teamid")) then%> selected <%end if%> <%if rsSelect("CardBlocked")=1 then%> style="color:red"<%end if%>><%=rsSelect("teamname")%></option>
				<%
						rsSelect.movenext
					loop
				%>
		</select>
				
	<% else %>
		No teams created
	<% end if %>
	
	<br>
	
	
	<%
		if len(request("selTeam")) > 0 then
	%>
	
	<table width=75% ID="Table15" class=smalltextblack>
		<tr>		
			<td>
				<input type=button name="SaveXL" id="SaveXL" value="Save to XL" onClick="javascript: window.open ( 'a_HomeTeamReportXL.asp?RptType=HT&TeamID=<%=request("selTeam")%>','HomeTeam','width=600,height=300,scrollbars=yes');"> 
			</td>
		</tr>
	</table>		

	<%
			WriteHTReport(request("selTeam"))
		end if
	%>
		
	
	<br>
	
<!--------------------------------------GUReport--------------------------------------------->					
<% elseif request("state") = "GUReport" then%>
	
	<%	sql="Select teamid, teamname, CardBlocked from tbHomeTeam order by TeamName"
		set rsSelect = cn.execute(sql)		
	%>
	
	<% if not rsSelect.eof then%>		
		<table class=smalltextblack ID="Table16">
			<tr>
				<td>Team Name</td>
				<td>
					<select name="selTeam" id="selTeam">
						<!--option value=0>Please select a Program</option-->
						<option value="ALL" <% if trim(teamid)="ALL" then%> selected <%end if%>>All Teams</option>
						
							<%	do while not rsSelect.eof%>
								<option value=<%=rsSelect("teamid")%> <% if trim(teamid)=trim(rsSelect("teamid")) then%> selected <%end if%> <%if rsSelect("CardBlocked")=1 then%> style="color:red"<%end if%>><%=rsSelect("teamname")%></option>
							<%
									rsSelect.movenext
								loop
							%>
					</select>
				</td>
			</tr>
			
			<tr>
				<td>Start Date</td>
				<td><input type="text" name="SDate" id="SDate" value="<%=request("SDate")%>" readonly><a href="javascript:c1.popup('SDate');"><IMG SRC="images/calendar.gif" BORDER=0></a></td>
			</tr>
			
			<tr>
				<td>End Date</td>
				<td><input type="text" name="EDate" id="EDate" value="<%=request("EDate")%>" readonly><a href="javascript:c1.popup('EDate');"><IMG SRC="images/calendar.gif" BORDER=0></a></td>
			</tr>
		
			<tr>
				<td>
					<br>
					<input type=button value="View Report" onclick="javascript:SelectRptTeam(6)" ID="Button18" NAME="Button18">
				</td>
				
				<td>
					<br>
					<input type=button value="Clear Dates" onclick="javascript:ClearDates()" ID="Button19" NAME="Button19">
				</td>
			</tr>
					
		</table>
				
	<% else %>
		No teams created
	<% end if %>
	
	<br>
		
	<%
		if len(request("selTeam")) > 0 then
	%>
	
	<table width=75% ID="Table17" class=smalltextblack>
		<tr>		
			<td>
				<input type=button name="SaveXL" value="Save to XL" onClick="javascript: window.open ( 'a_HomeTeamReportXL.asp?RptType=GU&TeamID=<%=request("selTeam")%>&SDate=<%=request("SDate")%>&EDate=<%=request("EDate")%>','HomeTeam','width=600,height=300,scrollbars=yes');" ID="Button20"> 
			</td>
		</tr>
	</table>		

	<%
			WriteGUReport request("selTeam"),request("SDate"),request("EDate")
		end if
	%>	

<!--------------------------------------BudgetReport--------------------------------------------->				
<% elseif request("state") = "BudgetReport" then%>	

	<%	sql="Select teamid, teamname, CardBlocked from tbHomeTeam order by TeamName"
		set rsSelect = cn.execute(sql)		
	%>
	
	<% if not rsSelect.eof then%>
	
		<select name="selTeam" id="selTeam" onchange="javascript:SelectRptTeam(7)">
			<option value=0>Please select a Program</option>		
			<option value="ALL" <% if trim(teamid)="ALL" then%> selected <%end if%>>All Teams</option>
			
				<%	do while not rsSelect.eof%>
					<option value=<%=rsSelect("teamid")%> <% if trim(teamid)=trim(rsSelect("teamid")) then%> selected <%end if%> <%if rsSelect("CardBlocked")=1 then%> style="color:red"<%end if%>><%=rsSelect("teamname")%></option>
				<%
						rsSelect.movenext
					loop
				%>
		</select>
				
	<% else %>
		No teams created
	<% end if %>
	
	<br>
	
	
	<%
		if len(request("selTeam")) > 0 then
	%>
	
	<table width=75% ID="Table18" class=smalltextblack>
		<tr>		
			<td>
				<input type=button name="SaveXL" value="Save to XL" onClick="javascript: window.open ( 'a_HomeTeamReportXL.asp?RptType=BR&TeamID=<%=request("selTeam")%>','HomeTeam','width=600,height=300,scrollbars=yes');" ID="Button21"> 
			</td>
		</tr>
	</table>		

	<%
			WriteBudgetReport(request("selTeam"))
		end if
	%>	

<!--------------------------------------FastRewardsReport--------------------------------------------->				
<% elseif request("state") = "FastRewardsReport" then%>	

	<%	sql="Select teamid, teamname, CardBlocked from tbHomeTeam order by TeamName"
		set rsSelect = cn.execute(sql)		
	%>
	
	<% if not rsSelect.eof then%>
	
		<select name="selTeam" id="selTeam" onchange="javascript:SelectRptTeam(8)">
			<option value=0>Please select a Program</option>					
				<%	do while not rsSelect.eof%>
					<option value=<%=rsSelect("teamid")%> <% if trim(teamid)=trim(rsSelect("teamid")) then%> selected <%end if%> <%if rsSelect("CardBlocked")=1 then%> style="color:red"<%end if%>><%=rsSelect("teamname")%></option>
				<%
						rsSelect.movenext
					loop
				%>
		</select>
				
	<% else %>
		No teams created
	<% end if %>
	
	<br>
	
	
	<%
		if len(request("selTeam")) > 0 then
	%>
	
	<!--table width=75% ID="Table6" class=smalltextblack>
		<tr>		
			<td>
				<input type=button name="SaveXL" value="Save to XL" onClick="javascript: window.open ( 'a_HomeTeamReportXL.asp?RptType=FR&TeamID=<%'=request("selTeam")%>','HomeTeam','width=600,height=300,scrollbars=yes');" ID="Button3"> 
			</td>
		</tr>
	</table-->		

	<%
			WriteFastRewardsReport(request("selTeam"))
		end if
	%>	


<!----------------------------------------NHUserSearch--------------------------------------------->
<% elseif request("state") = "SearchNHUsers" or request("state") = "SearchNHUsersResults" then %>

<input type=hidden name=selTeam id="Hidden7" value="<%=teamID%>">
<input type=hidden name="TeamName" id="Hidden8" value="<%=TeamName%>">
<input type=hidden name="TotalBudget" id="Hidden9" value="<%=TotalBudget%>">
<input type=hidden name="FastRewardBudget" id="Hidden10" value="<%=FastRewardBudget%>">
<input type=hidden name="MemberBudget" id="Hidden11" value="<%=MemberBudget%>">
<input type=hidden name="StartDate" id="Hidden12" value="<%=StartDate%>">
<input type=hidden name="EndDate" id="Hidden13" value="<%=EndDate%>">

<% WriteHiddenVariables() %>

	<script language=javascript>
		function CheckKey() {
			if ((window.event) && (window.event.keyCode == 13))
			{
				
				form1.state.value="SearchNHUsersResults";
				form1.submit();
			}
		}
	</script>
	
<i>Search for users below:</i>
	<p></p>

	<% if strWhere="" and request("state") = "SearchNHUsersResults" then%>
		<div align="left"><font color="#FF0000" size="4"><strong>At least one criteria 
		must be selected</strong></font> </div>
	<%end if%>
	  
	<table width="100%" border="0" cellspacing="0" cellpadding="2" class=smalltextblack ID="Table19">
	<tr>
		<td>	
			<table width="60%" border="1" cellspacing="0" cellpadding="2" class=smalltextblack ID="Table20">
			<tr> 
				<td>Last Name </td>
				<td><input name="lastname" type="text" onkeypress="CheckKey()" value="<%=request("lastname")%>" size="30" ID="Text8"></td>
			</tr>
	          
			<tr> 
				<td>Region</td>
				<td> 
					<%
						sql="select distinct REGION from TerrCode where REGION <> '' order by Region"
						set rs = cn.execute(sql)
					%>
					
					<select name="region" ID="Select8" onkeypress="CheckKey()">
					<option value="" selected>Select Region</option>
					<%
						do while not rs.eof
							sSelect=""
							if request("region") = trim(rs(0)) then sSelect = "selected"
					%>
							<option value="<%=trim(rs(0))%>" <%=sSelect%>><%=trim(rs(0))%></option>
					<%
							rs.MoveNext
						Loop
					%>
				</select>
			</td>
			</tr>
			<tr> 
				<td>District</td>
				<td> 
				<%
					sql="select distinct District from TerrCode order by District"
					set rs = cn.execute(sql)
				%>
				<select name="district" ID="Select9" onkeypress="CheckKey()">
					<option value="" selected>Select District</option>
					<%
						do while not rs.eof
							sSelect=""
							if request("district") = trim(rs(0)) then sSelect = "selected"
					%>
					<option value="<%=trim(trim(rs(0)))%>" <%=sSelect%>><%=trim(trim(rs(0)))%></option>
					<%
							rs.MoveNext
						Loop
					%>
				</select> </td>
			</tr>
			<tr> 
				<td>User Type</td>
				<td> 
					<%'B.B. 02/19/04
						sql = "select distinct TER_TYPE_CAT from vw_terrCode where not TER_TYPE_CAT is NULL and TER_TYPE_CAT <> '' order by TER_TYPE_CAT"
						set rs = cn.execute(sql)
					%>
				<select name="reptype" ID="Select10" onkeypress="CheckKey()">
					<option value="" selected>Select User Type</option>
					<%
						do until rs.eof
							sSelect=""
							if request("reptype") = trim(rs(0)) then sSelect = "selected"
					%>
						<option value="<%=trim(rs(0))%>" <%=sSelect%>><%=trim(rs(0))%></option>
					<%
						rs.MoveNext
						Loop
					%>
				</select> </td>
			</tr>
			</table>

			<p> 
			<input type="submit" value="SEARCH" onclick="javascript:FindNHUsers()" ID="Submit3" NAME="Submit3">
			</p>	
			
		<% if strWhere <> "" and request("state") = "SearchNHUsersResults" THEN %>	
			<% if not rsSearch.eof then	%>
			<table width="60%" border="1" cellspacing="1" cellpadding="1" class=smalltextblack ID="Table21">
				<tr bgcolor="#CCCCCC"> 
					<td>Name</td>
					<td>Job Title</td>
					<td>Region</td>
					<td>District</td>
					<td>Territory #</td>
					<td>Select</td>
				</tr>
				<% do while not rsSearch.eof %>								  	
						<tr>
							<td><input type=checkbox name="NHUsers" id="NHUsers" value="<%=trim(rsSearch("u_ID"))%>"></td> 
							<td><%=rsSearch("LASTNAME") & " " & rsSearch("FIRSTNAME")%></td>						
							<td><%=trim(rsSearch("JOBTITLE"))%></td>
							<td><%=trim(rsSearch("REGION"))%></td>
							<td><%=trim(rsSearch("DISTRICT"))%></td>
							<td><%if len(trim(rsSearch("TERRITORY_NO")))>0 then response.Write(trim(rsSearch("TERRITORY_NO"))) else response.Write("&nbsp;") end if%></td>							
          				</tr>
		  		<%
			 			rsSearch.MoveNext
			  		Loop
				end if
				%>				
			</table>		
			
			<table width="60%" border="1" cellspacing="1" cellpadding="1" class=smalltextblack ID="Table22">
				<tr>
					<td align=center><input type=button value="Add Users" onclick="javascript:AddNHUser();" ID="Button22" NAME="Button22"></td>
				</tr>
			</table>
	      
				
		<% end if %>	
		</td>
	</tr>
	</table>

			
<% end if ' end if for the states%>

<% end if 'end if for the error statement%>

<input type="hidden" name="uid" id="uid" value="<%=UserID%>">
<input type="hidden" name="func" id="func">
<input type="hidden" name="eid" id="eid">
<input type="hidden" name="numMembers" id="numMembers" value=<%=i%>>
<input type="hidden" name="SaveToXL" id="SaveToXL">
<input type="hidden" name="PriorState" id="PriorState" value="<%=PriorState%>">
<input type="hidden" name="NHUsersList" id="NHUsersList" value="<%=NHUsersList%>">

<script language=javascript>
function SelectManager (UID) {
	form1.uid.value=UID;	
	SubmitForm(2);
}

function SelectManagerFromBlank (UID) {
	form1.uid.value=UID;	
	SubmitForm(14);
}

function AssignManager() {	
	SubmitForm(12);
}

function SelectTeam() {
	if (form1.selTeam.value == 0)
	{
		return;
	}
	else
	{
		SubmitForm(4);
	}
}

function SelectRptTeam(i) {
	if (form1.selTeam.value == 0)
	{
		return;
	}
	else
	{
		SubmitForm(i);
	}
}

function ClearDates() {
	form1.SDate.value = '';
	form1.EDate.value = '';
}

function CreateTeam() {
	if (CheckForm(1)) {
		SubmitForm(3);
	}
}

function AddNHUser() {
	SubmitForm(11);
}

function FindNHUsers() {
	SubmitForm(10);
}

function SearchNHUsers(i) {
	if (i==0) {
		form1.PriorState.value = 'Create';
	}
	
	else if (i==1) {
		form1.PriorState.value = 'Edit';
	}
	
	SubmitForm(9);
}

function EditTeam(func,eid) {
	
	if (form1.CardBlocked.value ==1)
	{
		alert("This program is over and the Fast Rewards card has been blocked.  No further changes can be made.");
		return;
	}
	
		
	if ((form1.ProgramOver.value == 1) && (func != 'Block') && (func != 'Adjust') && (func != 'MoveQueued'))
	{
		alert("This program is over.  That function cannot be done.");
		return;	
	}
	
	if (func == 'End') {
		if (form1.HasQueued.value == 1) {
			alert("Program cannot be ended with dollars still queued.");
			return;
		}
				
		var ans = confirm("Are you sure you want to end this program?");
		if (!ans)
		{
			return;
		}
	}	
	
	if (func == 'MoveQueued') {				
		var ans1 = confirm("Clicking this button will move the queued funds to the members AWOO cards.  Are you sure you want to do this?");
		if (!ans1)
		{
			return;
		}
	}
	
	if (func=='Block') {
		
		var ans2 = confirm("Clicking this button will block the card and allow no other changes to be made. Are you sure you want to do this?");
		if (!ans2)
		{
			return;
		}
	
		if (form1.HasQueued.value == 1)
		{
			alert("Program cannot be ended with dollars still queued.");
			return;			
		}
		
		if (parseInt(form1.FRBalance.value) > 0) {
			alert("The Fast Reward card balance is not 0. It may take up to 15 minutes to withdraw funds from the Fast Rewards card.  Please try again later.");
			return;
		}
	}
		
	form1.func.value = func;   // possible values -- Edit,Adjust,Remove,Add,End
	form1.eid.value = eid; 
	
	if ((func == 'End') || (func == 'Block')) {
		SubmitForm(4);
	}
	else if (CheckForm(2)) {
		SubmitForm(4);
	}
}

function HomeTeamReport(i) {
	if (i==0) {
		form1.SaveToXL.value="True";
	}
	
	SubmitForm(5);
}

function SubmitForm(type) {

	if (type==0)
	{
		form1.state.value="Init";
	}

	else if (type==1)
	{
		form1.state.value="Search";
	}
	
	else if (type==2)
	{
		form1.state.value="Setup";
	}
	
	else if (type==3)
	{
		form1.state.value="Create";		
	}
	
	else if (type==4)
	{	
		form1.state.value="Edit";		
	}
	
	else if (type==5)
	{	
		form1.state.value="HTReport";		
	}

	else if (type==6)
	{	
		form1.state.value="GUReport";		
	}

	else if (type==7)
	{
		form1.state.value="BudgetReport";
	}
	
	else if (type==8)
	{
		form1.state.value="FastRewardsReport";
	}
	
	else if (type==9)
	{
		form1.state.value="SearchNHUsers";
	}
	
	else if (type==10)
	{
		form1.state.value="SearchNHUsersResults";
	}
	
	else if (type==11)
	{
		form1.state.value="AddNHUsers";
	}	
	
	else if (type==12)
	{
		form1.state.value="SearchManager";
	}
	
	else if (type==13)
	{
		form1.state.value="SearchManagerResults";
	}
	
	else if (type==14)
	{
		form1.state.value="AssignManager";
	}


	form1.submit();
}

function SplitBudget() {	

	var budget = form1.MemberBudget.value;	
	var numofMembers = form1.numMembers.value;
	var numChecked = 1;
	var CurrentCheck;
		
	if (form1.uid.value == "-1") {
		numChecked = 0;		
	}
	
	if (isPositiveInteger(budget)) {
		
		for (i=1; i < numofMembers; i++) {
			CurrentCheck = eval('form1.UserCheck' + i + '.checked');
			if (CurrentCheck) {
				numChecked = numChecked + 1;				
			}			
		}
	
		if (numofMembers > 0)
		{
			//var dividend = Math.floor(budget/numofMembers)				
			var dividend = Math.floor(budget/numChecked)
	
			if (form1.uid.value != "-1") {
				form1.BudgetTxt0.value=dividend;
			}
			
			for (i=1; i<numofMembers; i++)
			{				
				CurrentCheck = eval('form1.UserCheck' + i + '.checked');
				if (CurrentCheck) {
					eval('form1.BudgetTxt'+i+'.value=dividend');
				}
				else
					eval('form1.BudgetTxt'+i+'.value=0');
			}
		}		
	}
	else
		alert("Please enter a positive integer value for AWOO Card budget");
}

function CheckDistributions() {

	var numofMembers = form1.numMembers.value;	
	
	if (form1.uid.value == "-1") {
		i=1;
	}
	else {
		i=0;
	}
	
	for (; i<numofMembers; i++)
		{
			var tempTxt	
			tempTxt= eval("form1.BudgetTxt"+i);			
			if ((!isPositiveInteger(tempTxt.value)) && (tempTxt.value.length!=0)) {
				return false;				
			}
		}
	return true;
	
}


function CalcDistributions() {

	var budget = form1.MemberBudget.value;
	var numofMembers = form1.numMembers.value;	
	var sum = 0
	
	if (isPositiveInteger(form1.BudgetTxt0.value))
		sum = parseInt(form1.BudgetTxt0.value);
	
	for (i=1; i<numofMembers; i++)
		{
			var tempObj
			tempObj=eval("form1.UserCheck" + i)
			if (tempObj.checked)
			{
				var tempTxt	
				tempTxt= eval("form1.BudgetTxt"+i);
				if (isPositiveInteger(tempTxt.value)) {
					sum = sum + parseInt(tempTxt.value);
				}
			}
		}	
	if (sum > budget)
	{
		return false;		
	}
	else
		return true;
}

function CheckTeamName() {
	for (i=0;i<HomeTeamArray.length; i++)
	{
		if (trim(form1.TeamName.value) == trim(HomeTeamArray[i]))
		{
			return false;
		}
	}
	return true;
}

function trim(str) {
	return	str.replace(/^\s*|\s*$/g,"");
}

function CheckForm(i) {

	if (i == 1)
	{
		if (isWhitespace(form1.TeamName.value)) {
			alert("Please enter a value for Team Name");
			return false;
		}
		
		if (!CheckTeamName()) {
			alert("That Team Name already exists.  Please enter another one");
			return false;
		}
		
		if (!isNonnegativeInteger(form1.TotalBudget.value)) {
			alert("Please enter a non-negative integer value for Total budget.");
			return false;
		}
		if (!isNonnegativeInteger(form1.FastRewardBudget.value)) {
			alert("Please enter a non-negative integer value for Fast Rewards budget.");
			return false;
		}
		if (!isNonnegativeInteger(form1.MemberBudget.value)) {
			alert("Please enter a non-negative integer value for AWOO Card budget.");
			return false;
		}

		if (parseInt(form1.FastRewardBudget.value) + parseInt(form1.MemberBudget.value) > parseInt(form1.TotalBudget.value)) {
			alert("The Fast Rewards budget and Initial Members budget cannot be more than the total budget.");
			return false;
		}
		
		if (!CheckDistributions()) {
			alert("The AWOO distributions contain one or more non-integers.");
			return false;
		}	
		
		if (!CalcDistributions()) {
			alert("The AWOO distributions are more than the AWOO Card budget");
			return false;
		}	
		
		if (isEmpty(form1.StartDate.value)) {
			alert("Please enter a Start Date");
			return false;
		}
		
		if (isEmpty(form1.EndDate.value)) {
			alert("Please enter a End Date");
			return false;
		}
		
		if (!isDateCompare(form1.StartDate.value,form1.EndDate.value)) {
			alert("The end date is before the start date");
			return false
		}
		
	}
	else if (i == 2) 
	{
		var remaining = parseInt(form1.Remaining.value)
		
		if (form1.func.value == 'Edit') {
		
			var totalCurr = form1.Budget.value;
			var totalOrig = parseInt(form1.BudgetHidden.value);
			var totalChange
			var frCurr = form1.FRBudget.value;
			var frOrig = parseInt(form1.FRBudgetHidden.value);
			var frChange
			
			if (!isNonnegativeInteger(totalCurr))
			{
				alert("Total budget must be a non-negative integer.")
				return false;
			}
			
			totalChange = parseInt(totalCurr) - totalOrig			
			
			if (parseInt(totalChange) + parseInt(remaining) < 0)
			{
				alert("Total budget cannot be changed. Remaining funds too low.")
				return false;
			}
			
			if ((!isPositiveInteger(frCurr)) && (frCurr != 0)) 
			{
				alert("Fast Rewards budget must be a non-negative integer.")
				return false;
			}
						
			frChange = frCurr - frOrig;
						
			if (frChange > remaining + totalChange)
			{
				alert("Fast Rewards budget is larger then remaining budget");
				return false;
			}
			
			if (!isDateCompare(form1.StartDate.value,form1.EndDate.value))
			{
				alert("The end date is before the start date");
				return false
			}						
		}
		
		if (form1.func.value == 'Adjust') {
			var tempObj
			var tempID
			
			tempID = form1.eid.value 
			tempObj = eval("form1.txt" + tempID + ".value");
			tempHid = eval("form1.hidden" + tempID + ".value");			
			//tempQ = eval("form1.queued" + tempID + ".value");			

			if (!isSignedInteger(tempObj))
			{
				alert("Adjustment must be an integer");	
				return false;							
			}
			
//			if (parseInt(tempObj) > parseInt(remaining))
//			{
//				alert("Adjustment is larger then remaining budget");
//				return false;
//			}
			
//			if (parseInt(tempObj) + parseInt(tempHid) < 0)
//			{
//				alert("Adjustment will take away more than was given by program");
//				return false;
//			}
			
		}
		
		if (form1.func.value == 'MoveQueued') {
			var tempObj
			var tempID
			
			tempTotalQ = form1.TotalQueuedPoints.value
			QueueLarger = form1.QueueLarger.value
			
			if (QueueLarger == 1) {
				alert("One or more entries in Queued Dollars will take away more than was given by program");
				return false;
			}
			
			if (parseInt(tempTotalQ) > parseInt(remaining))
			{
				alert("Total Queued points are more than the remaining budget")
				return false;
			}					
		}
	}
	
	return true;
}
</script>

<% CloseDatabases() %>