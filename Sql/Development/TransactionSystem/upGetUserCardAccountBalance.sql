USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060804
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20061204
-- ============================================================
ALTER PROCEDURE dbo.upGetUserCardAccountBalance
	@AccountID int,
	@OwnerGUID int,
	@BalCardAmt money OUTPUT
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	
	-- VALIDATE
	IF (dbo.ufValidateAccountOwner(@AccountID, @OwnerGUID) <> 1)
	BEGIN
		RAISERROR('upGetUserCardAccountBalance - @AccountID, @OwnerGUID values supplied are invalid', 16, 1)
		RETURN
	END

	IF (dbo.ufAccountType(@AccountID) <> 'D')  -- Card
	BEGIN
		RAISERROR('upGetUserCardAccountBalance - @AccountID supplied is not a card account', 16, 1)
		RETURN
	END

	-- RESULT
	SELECT @BalCardAmt = dbo.ufCardAcctBalance(@AccountID)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upGetUserCardAccountBalance
TO system