USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060808
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upGetUserPointAccountBalances
	@OwnerGUID int,
	@TotPointBal int OUTPUT

AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	
	-- Put accounts and balances into temporary table
	SELECT AccountID, AccountPointBalance
	FROM dbo.tfAccountPointBalances(@OwnerGUID)

	-- Set output parameter
	SELECT @TotPointBal = dbo.ufPointsBalance(@OwnerGUID)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upGetUserPointAccountBalances
TO system