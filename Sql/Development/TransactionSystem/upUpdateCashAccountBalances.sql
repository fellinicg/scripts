USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060804
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upUpdateCashAccountBalances
	@Result char(1) OUTPUT -- (S)uccess or (F)ailure

AS
BEGIN

	-- Local variable declarations
	DECLARE @AccountType char(1)

	-- Initialize settings
	SET NOCOUNT ON
	
	-- Initialize variables
	SET @Result = 'F'
	SET @AccountType = 'C'

	-- Store data in temporary table
	SELECT a.AccountID 'AcctID', a.BalanceAmt + SUM(c.CashAmtUSD) 'Balance', MAX(c.CashTranID) 'TranID'
	INTO #Temp
	FROM dbo.tbAccount a
	INNER JOIN dbo.tbCashTran c ON a.AccountID = c.AccountID 
	WHERE a.AccountType = @AccountType
	AND c.CashTranID > a.BalanceTranID
	GROUP BY a.AccountID, a.BalanceAmt

	-- Update records in tbAccount 
	UPDATE dbo.tbAccount SET BalanceAmt = Balance, BalanceTranID = TranID
	FROM dbo.tbAccount
	INNER JOIN #Temp ON AccountID = AcctID

	-- Verify success
	IF (@@ERROR = 0)
		SET @Result = 'S'
	ELSE
		RAISERROR('upUpdateCashAccountBalances - cash account balances were NOT successfully updated', 16, 1)

	-- Drop temporary table
	DROP TABLE #Temp

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upUpdateCashAccountBalances
TO system