USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060808
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upGetUserCashAccountBalances
	@OwnerGUID int,
	@TotCashBal money OUTPUT

AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	
	-- Put accounts and balances into temporary table
	SELECT AccountID, AccountCashBalance
	FROM dbo.tfAccountCashBalances(@OwnerGUID)

	-- Set output parameter
	SELECT @TotCashBal = dbo.ufCashBalance(@OwnerGUID)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upGetUserCashAccountBalances
TO system