USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060719
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upAddTranType 
	@TranType char(1), 
	@Description varchar(40), 
	@Result char(1) OUTPUT
AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp datetime

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CURRENT_TIMESTAMP
	SET @Result = 'F'

	-- Ensure @TranType doesn't exist
	IF EXISTS (SELECT 1 FROM dbo.tbTranType WHERE TranType = @TranType)
		BEGIN
			RAISERROR('upAddTranType - transaction type already exists', 16, 1)
			RETURN
		END

	-- Insert new record into tbTranType
	INSERT INTO dbo.tbTranType (
		TranType, 
		Description, 
		RecordDateTime)
	VALUES (
		@TranType, 
		@Description, 
		@DateTimeStamp)

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upAddTranType - transaction type was NOT successfully added', 16, 1)

END
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upAddTranType
TO system