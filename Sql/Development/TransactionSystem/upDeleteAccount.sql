USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060718
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upDeleteAccount
	@AccountID int,
	@OwnerGUID int,
	@Result char(1) OUTPUT -- (S)uccess or (F)ailure
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Makes sure the Account is owned by the OwnerGUID supplied
	IF EXISTS (SELECT 1 FROM dbo.tbAccount WHERE AccountID = @AccountID AND OwnerGUID = @OwnerGUID)
		BEGIN

			-- Begin transaction
			BEGIN TRAN

			-- Delete records in tbCardTran
			DELETE FROM dbo.tbCardTran
			WHERE AccountID = @AccountID

			-- Delete records in tbCashTran
			DELETE FROM dbo.tbCashTran
			WHERE AccountID = @AccountID

			-- Delete records in tbIssuedCheck
			DELETE FROM dbo.tbIssuedCheck
			WHERE AccountID = @AccountID

			-- Delete records in tbPointTran
			DELETE FROM dbo.tbPointTran
			WHERE AccountID = @AccountID

			-- Delete records in tbAccountStatusHistory
			DELETE FROM dbo.tbAccountStatusHistory
			WHERE AccountID = @AccountID

			-- Delete records in tbAccountComment
			DELETE FROM dbo.tbAccountComment
			WHERE AccountID = @AccountID

			-- Delete record in tbAccount
			DELETE FROM dbo.tbAccount
			WHERE AccountID = @AccountID

			-- Verify success
			IF (@@ROWCOUNT = 1)
				BEGIN
					SET @Result = 'S'
					COMMIT TRAN
				END
			ELSE
				BEGIN
					ROLLBACK TRAN
					SET @Result = 'F'
					RAISERROR('upDeleteAccount - account was NOT successfully deleted', 16, 1)
				END

		END
END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upDeleteAccount
TO system