USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060731
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upAddCardTransaction 
	@AccountID int,
	@OwnerGUID int,
	@TranType char(1),  -- (A)ward, (R)edemption, (F)Refund, (J)Adjustment, (M)aintenance, or (T)ransfer
	@CardAmtUSD money,
	@TranDesc varchar(120) = '',
	@OriginSystemID int,
	@InitiatingUserGUID int = null,
	@ProgramID int = null,
	@DKNumber int = null,
	@SuppressDisplay bit,
	@MPGTranPeriodID int,
	@ClientTranPeriodID int,
	@CardProviderTranCode varchar(120) = '',
	@TranDateTime datetime,
	@PointTranID int OUTPUT
AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp datetime

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CURRENT_TIMESTAMP

	-- ***** Check Inputs *****
	-- 
	-- Validate @AccountID
	IF (dbo.ufValidateAccountOwner(@AccountID, @OwnerGUID) = 0)
		BEGIN
			RAISERROR('upAddCardTransaction - @AccountID supplied is not a valid account on the system', 16, 1)
			RETURN
		END

	-- Validate @OriginSystemID
	IF (dbo.ufValidateOriginSystem(@OriginSystemID) = 0)
		BEGIN
			RAISERROR('upAddCardTransaction - @OriginSystemID supplied is not a valid origin system on the system', 16, 1)
			RETURN
		END

	-- Validate @InitiatingUserGUID
	IF (@InitiatingUserGUID IS NOT NULL AND @InitiatingUserGUID > 0)
		BEGIN
			IF (dbo.ufValidateGUID(@InitiatingUserGUID) = 0)
				BEGIN
					RAISERROR('upAddCardTransaction - @InitiatingUserGUID supplied is not a valid user account id on the system', 16, 1)
					RETURN
				END
		END
	ELSE 
		SET @InitiatingUserGUID = null

	-- Validate @ProgramID
	IF (@ProgramID IS NOT NULL AND @ProgramID > 0)
		BEGIN
			IF (dbo.ufValidateProgram(@ProgramID) = 0)
				BEGIN
					RAISERROR('upAddCardTransaction - @ProgramID supplied is not a valid program on the system', 16, 1)
					RETURN
				END
		END
	ELSE 
		SET @ProgramID = null

	-- Validate @DKNumber
	IF (@DKNumber < 1)
		SET @DKNumber = null

	-- Validate @ClientTranPeriodID
	IF (dbo.ufValidateAccountClientTranPeriod(@AccountID, @ClientTranPeriodID) = 0)
		BEGIN
			RAISERROR('upAddCardTransaction - @ClientTranPeriodID supplied is not a valid client transaction period', 16, 1)
			RETURN
		END

	-- Validate @MPGTranPeriodID
	IF (dbo.ufValidateMPGTranPeriod(@MPGTranPeriodID) = 0)
		BEGIN
			RAISERROR('upAddCardTransaction - @MPGTranPeriodID supplied is not a valid Madison transaction period', 16, 1)
			RETURN
		END

	-- Nullify @TranDesc if empty
	IF (dbo.ufStringLen(@TranDesc) = 0) SET @TranDesc = null

	-- Nullify @CardProviderTranCode if empty
	IF (dbo.ufStringLen(@CardProviderTranCode) = 0) SET @CardProviderTranCode = null

	-- Insert new record into tbPointTran
	INSERT INTO dbo.tbCardTran (
		AccountID,
		TranType,
		CardAmtUSD,
		TranDesc,
		OriginSystemID,
		InitiatingUserGUID,
		ProgramID,
		DKNumber,
		SuppressDisplay,
		MPGTranPeriodID,
		ClientTranPeriodID,
		CardProviderTranCode,
		TranDateTime,
		RecordDateTime)
	VALUES (
		@AccountID,
		UPPER(@TranType),
		@CardAmtUSD,
		@TranDesc,
		@OriginSystemID,
		@InitiatingUserGUID,
		@ProgramID,
		@DKNumber,
		@SuppressDisplay,
		@MPGTranPeriodID,
		@ClientTranPeriodID,
		@CardProviderTranCode,
		@TranDateTime,
		@DateTimeStamp)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@IDENTITY > 0)
		SET @PointTranID = @@IDENTITY
	ELSE
		RAISERROR('upAddCardTransaction - cash transaction was NOT successfully added', 16, 1)

END
GO
GRANT EXECUTE
ON dbo.upAddCardTransaction
TO system