USE [TranSys]
GO
/****** Object:  StoredProcedure [dbo].[upAddPointTransaction]    Script Date: 12/04/2006 12:02:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060731
-- LastEditedByFullName: Jason McElroy
-- LastEditDate: 20061127
-- ============================================================
ALTER PROCEDURE [dbo].[upAddPointTransaction]
	@AccountID int,
	@OwnerGUID int,
	@TranType char(1),  -- (A)ward, (R)edemption, (F)Refund, (J)Adjustment, (P)urchase, (E)xpiration, (M)aintenance, or (T)ransfer
	@PointsAmt int,
	@TranDesc varchar(120) = NULL,
	@OriginSystemID int,
	@InitiatingUserGUID int = NULL,
	@ProgramID int = NULL,
	@DKNumber int = NULL,
	@ShoppingOrderID int = NULL,
	@SuppressDisplay bit = 0,
	@AltTranCode varchar(120) = NULL,
	@TranDateTime datetime = NULL,
	@PointTranID int OUTPUT,
	@IsSuccess bit OUTPUT
AS

	SET NOCOUNT ON

	SET @IsSuccess = 0


	-- ***** Check Inputs *****
	--
	-- Validate @AccountID
	IF (dbo.ufValidateAccountOwner(@AccountID, @OwnerGUID) = 0)
	BEGIN
		RAISERROR('upAddPointTransaction - @AccountID supplied is not a valid account on the system', 16, 1)
		RETURN
	END

	-- Validate Account is a Point(P) account
	IF (dbo.ufAccountType(@AccountID) <> 'P')
	BEGIN
		RAISERROR('upAddPointTransaction - @AccountID supplied is not a Point account on the system', 16, 1)
		RETURN
	END

	-- Validate @InitiatingUserGUID
	IF (@InitiatingUserGUID IS NOT NULL)
		IF (dbo.ufValidateGUID(@InitiatingUserGUID) = 0)
		BEGIN
			RAISERROR('upAddPointTransaction - @InitiatingUserGUID supplied is not a valid user account id on the system', 16, 1)
			RETURN
		END

	-- Validate @ProgramID
	IF (@ProgramID IS NOT NULL)
		IF (dbo.ufValidateProgram(@ProgramID) = 0)
		BEGIN
			RAISERROR('upAddPointTransaction - @ProgramID supplied is not a valid program on the system', 16, 1)
			RETURN
		END


	-- DEFAULT TRAN DATE IF NOT SUPPLIED
	SET @TranDateTime = ISNULL(@TranDateTime, CURRENT_TIMESTAMP)


	-- GET TRAN PERIODS
	DECLARE @MPGTranPeriodID int
	DECLARE @ClientTranPeriodID int

	SET @MPGTranPeriodID = dbo.ufMPGTranPeriod(@TranDateTime)

	IF (ISNULL(@MPGTranPeriodID, 0) = 0)
	BEGIN
		RAISERROR('upAddPointTransaction - unable to obtain MPGTranPeriodID for @TranDateTime supplied', 16, 1)
		RETURN
	END

	SET @ClientTranPeriodID = dbo.ufClientTranPeriod(@TranDateTime)

	IF (ISNULL(@ClientTranPeriodID, 0) = 0)
	BEGIN
		RAISERROR('upAddPointTransaction - unable to obtain ClientTranPeriodID for @TranDateTime supplied', 16, 1)
		RETURN
	END


	-- Insert new record into tbPointTran
	INSERT dbo.tbPointTran (
		AltTranCode,
		AccountID,
		TranType,
		PointsAmt,
		TranDesc,
		OriginSystemID,
		InitiatingUserGUID,
		ProgramID,
		DKNumber,
		ShoppingOrderID,
		SuppressDisplay,
		MPGTranPeriodID,
		ClientTranPeriodID,
		TranDateTime,
		RecordDateTime)
	VALUES (
		@AltTranCode,
		@AccountID,
		UPPER(@TranType),
		@PointsAmt,
		@TranDesc,
		@OriginSystemID,
		@InitiatingUserGUID,
		@ProgramID,
		@DKNumber,
		@ShoppingOrderID,
		@SuppressDisplay,
		@MPGTranPeriodID,
		@ClientTranPeriodID,
		@TranDateTime,
		DEFAULT)

	SET @PointTranID = @@IDENTITY

	IF (ISNULL(@PointTranID, 0) = 0)
	BEGIN
		RAISERROR('upAddPointTransaction - point transaction record was NOT created', 16, 1)
		RETURN
	END

	SET @IsSuccess = 1


	SET NOCOUNT OFF
GO
GRANT EXECUTE
ON dbo.upAddPointTransaction
TO system