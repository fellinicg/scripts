USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061204
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER FUNCTION dbo.ufCardBalance
(
	@OwnerGUID int
)
RETURNS money
AS
BEGIN

	DECLARE @BalanceCard money

	-- STORED PORTION
	SELECT @BalanceCard = ISNULL(SUM(BalanceAmt), 0) 
	FROM dbo.tbAccount 
	WHERE OwnerGUID = @OwnerGUID
	AND AccountType = 'D'

	-- CALCULATED PORTION
	SELECT @BalanceCard = @BalanceCard + ISNULL(SUM(c.CardAmtUSD), 0)
	FROM dbo.tbAccount a
	INNER JOIN dbo.tbAccount a2 ON a.AccountID = a2.AccountID
	LEFT JOIN dbo.tbCardTran c ON a.AccountID = c.AccountID AND c.CardTranID > a2.BalanceTranID
	WHERE a.AccountType = 'D'
	AND a.OwnerGUID = @OwnerGUID

	RETURN @BalanceCard

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.ufCardBalance
TO system