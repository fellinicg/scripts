USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060728
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER FUNCTION dbo.ufStringLen 
(
	@InputStr varchar(4000)
)
RETURNS bit
AS
BEGIN
	DECLARE @Result int
 
	SET @Result = 0

	SELECT @Result = LEN(LTRIM(RTRIM(ISNULL(@InputStr, ''))))

	RETURN @Result

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.ufStringLen
TO system