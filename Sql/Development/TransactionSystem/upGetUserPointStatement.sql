USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060810
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upGetUserPointStatement
	@OwnerGUID int,
	@ClientID int,
	@TranTypeList varchar(10), -- Comma list (A)ward, (R)edemption, (F)Refund, (J)Adjustment, (P)urchase, (E)xpiration, (M)aintenance, or (T)ransfer 
	@SuppressDisplay bit,
	@BeginTranDate datetime = null,
	@EndTranDate	datetime = null

AS
BEGIN

	-- Local variable declarations
	DECLARE @AccountType char(1)

	-- Initialize settings
	SET NOCOUNT ON
	
	-- Initialize variables
	SET @TranTypeList = UPPER(@TranTypeList)
	SET @AccountType = 'P'
	IF (@BeginTranDate IS NULL)
		SET @BeginTranDate = '1/1/1900'
	IF (@EndTranDate IS NULL)
		SET @EndTranDate = '12/31/2050'

	-- Return Data
	SELECT 
		a.AccountID
		,p.PointTranID
		,p.AltTranCode
		,p.TranType
		,t.Description
		,p.PointsAmt
		,p.TranDesc
		,p.TranDateTime
		,p.RecordDateTime
	FROM dbo.tbAccount a
	INNER JOIN dbo.tbPointTran p ON a.AccountID = p.AccountID
	INNER JOIN dbo.tfCommaListToTable (@TranTypeList) tl ON p.TranType = tl.ListItem
	INNER JOIN dbo.tbPointTranType t ON p.TranType = t.TranType
	WHERE a.AccountType = @AccountType
	AND a.OwnerGUID = @OwnerGUID
	AND a.ClientID = @ClientID
	AND p.SuppressDisplay = @SuppressDisplay
	AND p.TranDateTime BETWEEN @BeginTranDate AND @EndTranDate
	ORDER BY a.AccountID, p.TranDateTime

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upGetUserPointStatement
TO system