USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061121
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upGetTranPeriods
	@ClientID int,
	@CurrentDateTime datetime = null

AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	
	-- Initialize variables
	IF (@CurrentDateTime IS NULL)
		SET @CurrentDateTime = CURRENT_TIMESTAMP+100

	-- Return Data
	SELECT m.MPGTranPeriodID, c.ClientTranPeriodID
	FROM dbo.tbMPGTranPeriod m
	LEFT JOIN dbo.tbClientTranPeriod c ON @CurrentDateTime BETWEEN c.StartDateTime AND c.EndDateTime 
																				AND ClientID = @ClientID
	WHERE @CurrentDateTime BETWEEN m.StartDateTime AND m.EndDateTime

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upGetTranPeriods
TO system