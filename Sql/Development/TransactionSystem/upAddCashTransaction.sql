USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060727
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20061204
-- ============================================================
ALTER PROCEDURE [dbo].[upAddCashTransaction] 
	@AccountID int,
	@OwnerGUID int,
	@TranType char(1),  -- (A)ward, (J)Adjustment, (M)aintenance, (T)ransfer, or (C)heck Issue
	@CashAmtUSD money,
	@TranDesc varchar(120) = '',
	@OriginSystemID int,
	@InitiatingUserGUID int = null,
	@ProgramID int = null,
	@DKNumber int = null,
	@SuppressDisplay bit,
	@TranDateTime datetime,
	@CashTranID int OUTPUT,
	@IsSuccess bit OUTPUT
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	SET @IsSuccess = 0

	-- ***** Check Inputs *****
	-- 
	-- Validate @AccountID
	IF (dbo.ufValidateAccountOwner(@AccountID, @OwnerGUID) = 0)
		BEGIN
			RAISERROR('upAddCashTransaction - @AccountID supplied is not a valid account on the system', 16, 1)
			RETURN
		END

	-- Validate Account is a Cash (C) account
	IF (dbo.ufAccountType(@AccountID) <> 'C')
	BEGIN
		RAISERROR('upAddPointTransaction - @AccountID supplied is not a Cash account on the system', 16, 1)
		RETURN
	END

	-- Validate @InitiatingUserGUID
	IF (@InitiatingUserGUID IS NOT NULL AND @InitiatingUserGUID > 0)
		BEGIN
			IF (dbo.ufValidateGUID(@InitiatingUserGUID) = 0)
				BEGIN
					RAISERROR('upAddCashTransaction - @InitiatingUserGUID supplied is not a valid user account id on the system', 16, 1)
					RETURN
				END
		END
	ELSE 
		SET @InitiatingUserGUID = null

	-- Validate @ProgramID
	IF (@ProgramID IS NOT NULL AND @ProgramID > 0)
		BEGIN
			IF (dbo.ufValidateProgram(@ProgramID) = 0)
				BEGIN
					RAISERROR('upAddCashTransaction - @ProgramID supplied is not a valid program on the system', 16, 1)
					RETURN
				END
		END
	ELSE 
		SET @ProgramID = null

	-- DEFAULT TRAN DATE IF NOT SUPPLIED
	SET @TranDateTime = ISNULL(@TranDateTime, CURRENT_TIMESTAMP)

	-- GET TRAN PERIODS
	DECLARE @MPGTranPeriodID int
	DECLARE @ClientTranPeriodID int

	SET @MPGTranPeriodID = dbo.ufMPGTranPeriod(@TranDateTime)

	IF (ISNULL(@MPGTranPeriodID, 0) = 0)
	BEGIN
		RAISERROR('upAddCashTransaction - unable to obtain MPGTranPeriodID for @TranDateTime supplied', 16, 1)
		RETURN
	END

	SET @ClientTranPeriodID = dbo.ufClientTranPeriod(@TranDateTime)

	IF (ISNULL(@ClientTranPeriodID, 0) = 0)
	BEGIN
		RAISERROR('upAddCashTransaction - unable to obtain ClientTranPeriodID for @TranDateTime supplied', 16, 1)
		RETURN
	END


	-- Insert new record into tbUserAccount
	INSERT INTO dbo.tbCashTran (
		AccountID,
		TranType,
		CashAmtUSD,
		TranDesc,
		OriginSystemID,
		InitiatingUserGUID,
		ProgramID,
		DKNumber,
		SuppressDisplay,
		MPGTranPeriodID,
		ClientTranPeriodID,
		TranDateTime,
		RecordDateTime)
	VALUES (
		@AccountID,
		UPPER(@TranType),
		@CashAmtUSD,
		@TranDesc,
		@OriginSystemID,
		@InitiatingUserGUID,
		@ProgramID,
		@DKNumber,
		@SuppressDisplay,
		@MPGTranPeriodID,
		@ClientTranPeriodID,
		@TranDateTime,
		DEFAULT)

	SET @CashTranID = @@IDENTITY

	IF (ISNULL(@CashTranID, 0) = 0)
	BEGIN
		RAISERROR('upAddCashTransaction - cash transaction record was NOT created', 16, 1)
		RETURN
	END

	SET @IsSuccess = 1

END
GO
GRANT EXECUTE
ON dbo.upAddCashTransaction
TO system