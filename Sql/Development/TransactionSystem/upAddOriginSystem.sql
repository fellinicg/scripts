USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060726
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upAddOriginSystem 
	@Name varchar(40), 
	@Description varchar(400) = '', 
	@OriginSystemID int OUTPUT
AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp datetime

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CURRENT_TIMESTAMP

	-- Nullify Description if it is blank
	IF(LEN(LTRIM(RTRIM(@Description))) = 0)
		SET @Description = null

	-- Insert new record into tbOriginSystem
	INSERT INTO dbo.tbOriginSystem (
		Name, 
		Description, 
		RecordDateTime)
	VALUES (
		@Name, 
		@Description, 
		@DateTimeStamp)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@IDENTITY > 0)
		SET @OriginSystemID = @@IDENTITY
	ELSE
		RAISERROR('upAddOriginSystem - origin system was NOT successfully added', 16, 1)

	SET NOCOUNT OFF

END
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upAddOriginSystem
TO system