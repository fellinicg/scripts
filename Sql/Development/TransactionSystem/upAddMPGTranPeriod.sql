USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060726
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upAddMPGTranPeriod 
	@StartDateTime datetime, 
	@EndDateTime datetime, 
	@MPGTranPeriodID int OUTPUT
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Set start and end date correctly
	SET @StartDateTime = CONVERT(DATETIME, CONVERT(VARCHAR, @StartDateTime, 101))
	SET @EndDateTime = CONVERT(VARCHAR, @EndDateTime, 101) + ' 23:59:59.998'

	-- Insert new record into tbMPGTranPeriod
	INSERT INTO dbo.tbMPGTranPeriod (
		StartDateTime, 
		EndDateTime)
	VALUES (
		@StartDateTime, 
		@EndDateTime)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@IDENTITY > 0)
		SET @MPGTranPeriodID = @@IDENTITY
	ELSE
		RAISERROR('upAddMPGTranPeriod - Madison transaction period was NOT successfully added', 16, 1)

	SET NOCOUNT OFF

END
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upAddMPGTranPeriod
TO system