USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060712
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE FUNCTION dbo.tfStatus ()
RETURNS TABLE 
AS
RETURN 
(
	SELECT 'O' AS Type, 'Open' AS Description
	UNION
	SELECT 'C', 'Closed'
	UNION
	SELECT 'H', 'Hold'
)

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT SELECT
ON dbo.tfStatus
TO system