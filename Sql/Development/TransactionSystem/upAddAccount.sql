USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060707
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upAddAccount 
	@AccountType char(1),  -- (P)oints, (C)ash, or (D)ebit Card
	@OwnerGUID int,
	@AccountDesc varchar(40),
	@ClientID int,
	@ClientTranPeriodID int,
	@Status char(1),  -- (O)pen, (C)losed, or (H)old,
	@InitiatorGUID int,
	@Comment varchar(4000) = '',
	@AccountID int OUTPUT
AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp datetime
	DECLARE @AccountCommentID int
	DECLARE @Result char(1)

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CURRENT_TIMESTAMP

	-- ***** Check Inputs *****
	-- 
	-- Validate @AccountType
	IF (NOT EXISTS(SELECT 1 FROM dbo.tbAccountType WHERE AccountType = @AccountType))
		BEGIN
			RAISERROR('upAddAccount - @AccountType supplied is not a valid account type on the system', 16, 1)
			RETURN
		END

	-- Validate @ClientID
	IF (NOT EXISTS(SELECT 1 FROM SQL2KPROD.Catalog.dbo.Clients WHERE cl_ID = @ClientID))
		BEGIN
			RAISERROR('upAddAccount - @ClientID supplied is not a valid client id on the system', 16, 1)
			RETURN
		END

	-- Validate @OwnerGUID. Make sure the GlobalUser is a valid user for the supplied client
	IF (NOT EXISTS(SELECT 1 FROM SQL2KPROD.GlobalUsers.dbo.tbUserAccount WHERE UserAccountID = @OwnerGUID AND ClientID = @ClientID))
		BEGIN
			RAISERROR('upAddAccount - @OwnerGUID supplied is not a valid user account id on the system', 16, 1)
			RETURN
		END

	-- Validate @ClientTranPeriodID. Make sure the ClientTranPeriodID is a valid ID for the supplied client
	IF (NOT EXISTS(SELECT 1 FROM dbo.tbClientTranPeriod WHERE ClientTranPeriodID = @ClientTranPeriodID AND ClientID = @ClientID))
		BEGIN
			RAISERROR('upAddAccount - @ClientTranPeriodID supplied is not a valid client transaction period id on the system', 16, 1)
			RETURN
		END

	-- Validate @Status
	IF (dbo.ufValidateStatus(@Status) = 0)
		BEGIN
			RAISERROR('upAddAccount - @Status supplied is not a valid status on the system', 16, 1)
			RETURN
		END

	-- Insert new record into tbUserAccount
	INSERT INTO dbo.tbAccount (
		AccountType, 
		OwnerGUID, 
		AccountDesc, 
		ClientID, 
		ClientTranPeriodID, 
		BalanceTranID,
		BalanceAmt, 
		Status, 
		StatusDateTime,
		RecordDateTime)
	VALUES (
		UPPER(@AccountType),
		@OwnerGUID, 
		@AccountDesc, 
		@ClientID, 
		@ClientTranPeriodID, 
		0, 
		0, 
		UPPER(@Status), 
		@DateTimeStamp,
		@DateTimeStamp)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@IDENTITY > 0)
		SET @AccountID = @@IDENTITY
	ELSE
		RAISERROR('upAddAccount - user account was NOT successfully added', 16, 1)

	-- Add account comment record, if necessary
	IF (dbo.ufStringLen(@Comment) > 0)
		BEGIN
			EXECUTE dbo.upAddAccountComment 
				@AccountID = @AccountID
				,@CommenterGUID = @InitiatorGUID
				,@Comment = @Comment
				,@AccountCommentID = @AccountCommentID OUTPUT

			-- Verify success
			IF (ISNULL(@AccountCommentID, 0) < 1)
				BEGIN
					SET @AccountID = null
					EXECUTE dbo.upDeleteAccount 
						@AccountID = @AccountID
						,@OwnerGUID = @OwnerGUID
						,@Result = @Result OUTPUT
					RAISERROR('upAddAccount - user account was NOT successfully added, account comment failed', 16, 1)
				END
		END
END
GO
GRANT EXECUTE
ON dbo.upAddAccount
TO system