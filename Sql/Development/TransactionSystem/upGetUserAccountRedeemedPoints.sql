USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061129
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upGetUserAccountRedeemedPoints
	@AccountID int,
	@OwnerGUID int,
	@RedeemedPoints int OUTPUT
AS
BEGIN

	-- Local variable declarations

	-- Initialize settings
	SET NOCOUNT ON
	
	-- Initialize variables

	-- Return Data
	SELECT @RedeemedPoints = SUM(ISNULL(p.PointsAmt, 0)) * -1
	FROM dbo.tbAccount a
	INNER JOIN dbo.tbAccount a2 ON a.AccountID = a2.AccountID
	LEFT JOIN dbo.tbPointTran p ON a2.AccountID = p.AccountID
	WHERE a.AccountID = @AccountID
	AND a.OwnerGUID = @OwnerGUID
	AND p.TranType IN ('R', 'F')
	GROUP BY a.AccountID

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upGetUserAccountRedeemedPoints
TO system