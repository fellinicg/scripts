USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061204
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER FUNCTION dbo.ufPointsBalance
(
	@OwnerGUID int
)
RETURNS int
AS
BEGIN

	DECLARE @BalancePoints money

	-- STORED PORTION
	SELECT @BalancePoints = ISNULL(SUM(BalanceAmt), 0) 
	FROM dbo.tbAccount 
	WHERE OwnerGUID = @OwnerGUID
	AND AccountType = 'P'

	-- CALCULATED PORTION
	SELECT @BalancePoints = @BalancePoints + ISNULL(SUM(p.PointsAmt), 0)
	FROM dbo.tbAccount a
	INNER JOIN dbo.tbAccount a2 ON a.AccountID = a2.AccountID
	LEFT JOIN dbo.tbPointTran p ON a.AccountID = p.AccountID AND p.PointTranID > a2.BalanceTranID
	WHERE a.AccountType = 'P'
	AND a.OwnerGUID = @OwnerGUID

	RETURN @BalancePoints

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.ufPointsBalance
TO system