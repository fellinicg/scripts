USE [TranSys]
GO
/****** Object:  StoredProcedure [dbo].[upGetUserPointAccountBalance]    Script Date: 12/04/2006 10:44:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060804
-- LastEditedByFullName: Jason McElroy
-- LastEditDate: 20061130
-- ============================================================
ALTER PROCEDURE [dbo].[upGetUserPointAccountBalance]
	@AccountID int,
	@OwnerGUID int,
	@BalPointAmt int OUTPUT
AS
BEGIN

	SET NOCOUNT ON

	-- VALIDATE
	IF (dbo.ufValidateAccountOwner(@AccountID, @OwnerGUID) <> 1)
	BEGIN
		RAISERROR('upGetUserPointAccountBalance - @AccountID, @OwnerGUID values supplied are invalid', 16, 1)
		RETURN
	END

	IF (dbo.ufAccountType(@AccountID) <> 'P')  -- POINTS
	BEGIN
		RAISERROR('upGetUserPointAccountBalance - @AccountID supplied is not a points account', 16, 1)
		RETURN
	END

	-- RESULT
	SELECT @BalPointAmt = dbo.ufPointsAcctBalance(@AccountID)

END
