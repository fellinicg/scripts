USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060731
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upAddIssuedCheck 
	@AccountID int,
	@DRCashTranID int,
	@CheckNum varchar(20),
	@Status char(1),
	@IssuedCheckID int OUTPUT
AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp datetime

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CURRENT_TIMESTAMP

	-- Open Transaction
	BEGIN TRAN

	-- Insert new record into tbIssuedCheck
	INSERT INTO dbo.tbIssuedCheck (
		AccountID, 
		DRCashTranID, 
		CheckNum, 
		Status, 
		StatusDateTime,
		RecordDateTime)
	VALUES (
		@AccountID,
		@DRCashTranID, 
		@CheckNum, 
		@Status, 
		@DateTimeStamp,
		@DateTimeStamp)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@IDENTITY > 0)
		SET @IssuedCheckID = @@IDENTITY
	ELSE
		RAISERROR('upAddIssuedCheck - issued check was NOT successfully added', 16, 1)

	-- Update tbCashTran
	UPDATE tbCashTran SET IssuedCheckID = @IssuedCheckID
	WHERE CashTranID = @DRCashTranID
	AND AccountID = @AccountID

	-- Verify success
	IF (@@ROWCOUNT = 1)
		COMMIT TRAN
	ELSE
		BEGIN
			ROLLBACK TRAN
			RAISERROR('upAddIssuedCheck - issued check was NOT successfully added', 16, 1)
		END

END
GO
GRANT EXECUTE
ON dbo.upAddIssuedCheck
TO system