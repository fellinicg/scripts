USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060713
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upAddAccountStatusHistory
	@AccountID int,
	@Status char(1),  -- (O)pen, (C)losed, or (H)old,
	@InitiatorGUID int,
	@Comment varchar(4000),
	@Result char(1) OUTPUT -- (S)uccess or (F)ailure
AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp datetime
	DECLARE @AccountCommentID int
	DECLARE @rc char(1)

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CURRENT_TIMESTAMP
	SET @AccountCommentID = null
	SET @Result = 'F'

	-- ***** Check Inputs *****
	-- 
	-- Validate @AccountID
	IF (dbo.ufValidateAccount(@AccountID) = 0)
		BEGIN
			RAISERROR('upAddAccountStatusHistory - @AccountID supplied is not a valid account on the system', 16, 1)
			RETURN
		END

	-- Validate @InitiatorGUID
	IF (dbo.ufValidateGUID(@InitiatorGUID) = 0)
		BEGIN
			RAISERROR('upAddAccountStatusHistory - @InitiatorGUID supplied is not a valid user account id on the system', 16, 1)
			RETURN
		END

	-- Validate @Status
	IF (dbo.ufValidateStatus(@Status) = 0)
		BEGIN
			RAISERROR('upAddAccountStatusHistory - @Status supplied is not a valid status on the system', 16, 1)
			RETURN
		END

	-- Add comment
	EXECUTE dbo.upAddAccountComment 
			@AccountID = @AccountID
		,@CommenterGUID = @InitiatorGUID
		,@Comment = @Comment
		,@AccountCommentID = @AccountCommentID OUTPUT

	-- Check for errors
	IF (@@ROWCOUNT = 0 OR @AccountCommentID IS NULL)
		BEGIN
			RAISERROR('upAddAccountStatusHistory - unable to add comment', 16, 1)
			RETURN
		END

	-- Insert new record into tbAccountComment
	INSERT INTO dbo.tbAccountStatusHistory (
		AccountID, 
		Status,
		StartDateTime,
		InitiatorGUID,
		InitiatorName,
		AccountCommentID)
	VALUES (
		@AccountID, 
		UPPER(@Status), 
		@DateTimeStamp,
		@InitiatorGUID, 
		dbo.ufGetPersonsFullName(@InitiatorGUID), 
		@AccountCommentID)

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		BEGIN
			IF (@AccountCommentID IS NOT NULL)
				EXECUTE dbo.upDeleteAccountComment
					@AccountCommentID = @AccountCommentID
					,@AccountID = @AccountID
					,@Result = @rc OUTPUT
			
			RAISERROR('upAddAccountStatusHistory - account status history was NOT successfully added', 16, 1)
		END

END
GO
GRANT EXECUTE
ON dbo.upAddAccountStatusHistory
TO system