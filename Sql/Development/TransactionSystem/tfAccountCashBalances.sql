USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061204
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE FUNCTION dbo.tfAccountCashBalances (@OwnerGUID int)
RETURNS table
AS

RETURN
(
	SELECT a.AccountID, dbo.ufStoredAccountBalance(a.AccountID) + SUM(ISNULL(c.CashAmtUSD, 0)) 'AccountCashBalance'
	FROM dbo.tbAccount a
	INNER JOIN dbo.tbAccount a2 ON a.AccountID = a2.AccountID
	LEFT JOIN dbo.tbCashTran c ON a2.AccountID = c.AccountID AND c.CashTranID > a2.BalanceTranID
	WHERE a.AccountType = 'C'
	AND a.OwnerGUID = @OwnerGUID
	GROUP BY a.AccountID
)

GO
GRANT SELECT
ON dbo.tfAccountCashBalances
TO system