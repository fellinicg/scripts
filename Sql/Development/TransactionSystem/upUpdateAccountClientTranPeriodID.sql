USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060726
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upUpdateAccountClientTranPeriodID
	@AccountID int,
	@OwnerGUID int,
	@ClientTranPeriodID int,
	@Result char(1) OUTPUT -- (S)uccess or (F)ailure

AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp datetime

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CONVERT(VARCHAR, CURRENT_TIMESTAMP, 121)
	SET @Result = 'F'

	-- ***** Check Inputs *****
	-- 
	-- Validate @ClientTranPeriodID. Make sure the ClientTranPeriodID is a valid ID for the supplied client
	IF (NOT EXISTS(SELECT 1 FROM dbo.tbClientTranPeriod c 
								INNER JOIN dbo.tbAccount a ON c.ClientTranPeriodID = a.ClientTranPeriodID 
																								AND c.ClientID = a.ClientID 
								WHERE c.ClientTranPeriodID = @ClientTranPeriodID AND a.AccountID = @AccountID))
		BEGIN
			RAISERROR('upUpdateAccountClientTranPeriodID - @ClientTranPeriodID supplied is not valid for this account', 16, 1)
			RETURN
		END

	-- Update record in tbAccount 
	UPDATE dbo.tbAccount 
	SET ClientTranPeriodID = @ClientTranPeriodID
	WHERE AccountID = @AccountID 
	AND OwnerGUID = @OwnerGUID 

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upUpdateAccountClientTranPeriodID - account was NOT successfully updated', 16, 1)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upUpdateAccountClientTranPeriodID
TO system