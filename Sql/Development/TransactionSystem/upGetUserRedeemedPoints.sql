USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061129
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upGetUserRedeemedPoints
	@OwnerGUID int,
	@TotRedeemedPoints int OUTPUT
AS
BEGIN

	-- Local variable declarations

	-- Initialize settings
	SET NOCOUNT ON
	
	-- Initialize variables

	-- Put accounts and balances into temporary table
	SELECT a.AccountID, SUM(ISNULL(p.PointsAmt, 0)) * -1 'RedeemedPoints'
	INTO #Temp
	FROM dbo.tbAccount a
	INNER JOIN dbo.tbAccount a2 ON a.AccountID = a2.AccountID
	LEFT JOIN dbo.tbPointTran p ON a2.AccountID = p.AccountID AND p.PointTranID BETWEEN a2.BalanceTranID+1 AND 2147483647
	WHERE a.AccountType = 'P'
	AND a.OwnerGUID = @OwnerGUID
	AND p.TranType IN ('R', 'F')
	GROUP BY a.AccountID

	-- Return Data
	SELECT AccountID, RedeemedPoints
	FROM #Temp

	-- Set output parameter
	SELECT @TotRedeemedPoints = ISNULL(SUM(RedeemedPoints), 0)
	FROM #Temp

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upGetUserRedeemedPoints
TO system