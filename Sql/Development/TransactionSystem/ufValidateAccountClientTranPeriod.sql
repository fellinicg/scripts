USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060728
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE FUNCTION dbo.ufValidateAccountClientTranPeriod 
(
	@AccountID int,
	@ClientTranPeriodID int
)
RETURNS bit
AS
BEGIN
	DECLARE @Result bit
 
	SET @Result = 0

	SELECT @Result = 1 
	FROM dbo.tbAccount a 
	INNER JOIN dbo.tbClientTranPeriod c ON a.AccountID = @AccountID AND a.ClientID = c.ClientID
	WHERE c.ClientTranPeriodID = @ClientTranPeriodID

	RETURN @Result

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.ufValidateAccountClientTranPeriod
TO system