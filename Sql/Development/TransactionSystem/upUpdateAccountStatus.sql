USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060726
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upUpdateAccountStatus
	@AccountID int,
	@OwnerGUID int,
	@Status char(1),
	@InitiatorGUID int,
	@Comment varchar(4000),
	@Result char(1) OUTPUT -- (S)uccess or (F)ailure

AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp datetime
	DECLARE @CurrentStatus char(1)
	DECLARE @CurrentStatusDateTime datetime

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CONVERT(VARCHAR, CURRENT_TIMESTAMP, 121)
	SET @Result = 'F'

	-- ***** Check Inputs *****
	-- 
	-- Validate @Status
	IF (dbo.ufValidateStatus(@Status) = 0)
		BEGIN
			RAISERROR('upUpdateAccountStatus - @Status supplied is not a valid status on the system', 16, 1)
			RETURN
		END

	-- Store current values in local variables in case StatusHistory insert fails
	SELECT @CurrentStatus = Status, @CurrentStatusDateTime = StatusDateTime
	FROM dbo.tbAccount
	WHERE AccountID = @AccountID 
	AND OwnerGUID = @OwnerGUID

	-- Verify success
	IF (@@ROWCOUNT = 0)
		BEGIN
			RAISERROR('upUpdateAccountStatus - user account was NOT successfully updated', 16, 1)
			RETURN
		END

	-- Update record in tbAccount
	UPDATE dbo.tbAccount SET
	Status = @Status,
	StatusDateTime = @DateTimeStamp
	WHERE AccountID = @AccountID 
	AND OwnerGUID = @OwnerGUID

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		BEGIN
			RAISERROR('upUpdateAccountStatus - user account was NOT successfully updated', 16, 1)
			RETURN
		END

	-- Add account status history record
	EXECUTE dbo.upAddAccountStatusHistory 
	   @AccountID = @AccountID
	  ,@Status = @Status
	  ,@InitiatorGUID = @InitiatorGUID
	  ,@Comment = @Comment
	  ,@Result = @Result OUTPUT

	-- Verify success
	IF (@Result <> 'S')
		BEGIN
			-- Revert record in tbAccount
			UPDATE dbo.tbAccount SET
			Status = @CurrentStatus,
			StatusDateTime = @CurrentStatusDateTime
			WHERE AccountID = @AccountID 
			AND OwnerGUID = @OwnerGUID

			RAISERROR('upUpdateAccountStatus - user account was NOT successfully added, status history failed', 16, 1)
		END

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upUpdateAccountStatus
TO system