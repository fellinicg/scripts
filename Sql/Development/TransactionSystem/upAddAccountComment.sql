USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060707
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upAddAccountComment
	@AccountID int,
	@CommenterGUID int,
	@Comment varchar(4000),
	@AccountCommentID int OUTPUT
AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp datetime

	-- Initialize settings
	SET NOCOUNT ON
	SET @AccountCommentID = null

	-- Initialize variables
	SET @DateTimeStamp = CURRENT_TIMESTAMP

	-- ***** Check Inputs *****
	-- 
	-- Validate @AccountID
	IF (dbo.ufValidateAccount(@AccountID) = 0)
		BEGIN
			RAISERROR('upAddAccountComment - @AccountID supplied is not a valid account on the system', 16, 1)
			RETURN
		END

	-- Validate @CommenterGUID
	IF (dbo.ufValidateGUID(@CommenterGUID) = 0)
		BEGIN
			RAISERROR('upAddAccountComment - @CommenterGUID supplied is not a valid user account id on the system', 16, 1)
			RETURN
		END

	-- Insert new record into tbAccountComment
	INSERT INTO dbo.tbAccountComment (
		AccountID, 
		CommentDateTime, 
		CommenterGUID, 
		CommenterName, 
		Comment)
	VALUES (
		@AccountID, 
		@DateTimeStamp, 
		@CommenterGUID, 
		dbo.ufGetPersonsFullName(@CommenterGUID), 
		@Comment)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@IDENTITY > 0)
		SET @AccountCommentID = @@IDENTITY
	ELSE
		RAISERROR('upAddAccountComment - comment was NOT successfully added', 16, 1)

END
GO
GRANT EXECUTE
ON dbo.upAddAccountComment
TO system