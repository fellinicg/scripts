USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061204
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER FUNCTION dbo.ufCardAcctBalance (@AccountID int)
RETURNS money
AS
BEGIN

	DECLARE @BalanceCard money
	DECLARE @BalanceTranID int

	-- STORED PORTION
	SELECT @BalanceCard = BalanceAmt, @BalanceTranID = BalanceTranID FROM dbo.tbAccount WHERE AccountID = @AccountID

	IF (@@ROWCOUNT = 0)
		RETURN (0)

	-- CALCULATED PORTION
	SELECT @BalanceCard = ISNULL(@BalanceCard, 0) + ISNULL(SUM(CardAmtUSD), 0) FROM dbo.tbCardTran WHERE AccountID = @AccountID AND CardTranID > @BalanceTranID

	RETURN (@BalanceCard)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.ufCardAcctBalance
TO system