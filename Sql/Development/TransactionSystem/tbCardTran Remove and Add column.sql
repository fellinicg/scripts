USE [TranSys]
GO
ALTER TABLE dbo.tbCardTran
	DROP COLUMN CardProviderTranID 
GO
ALTER TABLE dbo.tbCardTran
	ADD CardProviderTranCode varchar(120) NULL
GO
