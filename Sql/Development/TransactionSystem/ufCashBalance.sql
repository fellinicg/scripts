USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061204
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER FUNCTION dbo.ufCashBalance
(
	@OwnerGUID int
)
RETURNS money
AS
BEGIN

	DECLARE @BalanceCash money

	-- STORED PORTION
	SELECT @BalanceCash = ISNULL(SUM(BalanceAmt), 0) 
	FROM dbo.tbAccount 
	WHERE OwnerGUID = @OwnerGUID
	AND AccountType = 'C'

	-- CALCULATED PORTION
	SELECT @BalanceCash = @BalanceCash + ISNULL(SUM(c.CashAmtUSD), 0)
	FROM dbo.tbAccount a
	INNER JOIN dbo.tbAccount a2 ON a.AccountID = a2.AccountID
	LEFT JOIN dbo.tbCashTran c ON a.AccountID = c.AccountID AND c.CashTranID > a2.BalanceTranID
	WHERE a.AccountType = 'C'
	AND a.OwnerGUID = @OwnerGUID

	RETURN @BalanceCash

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.ufCashBalance
TO system