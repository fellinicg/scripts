USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060801
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upUpdateIssuedCheckStatus
	@IssuedCheckID int,
	@AccountID int,
	@Status char(1),
	@Result char(1)  -- (S)uccess or (F)ailure
AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp datetime

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'F'

	-- Initialize variables
	SET @DateTimeStamp = CURRENT_TIMESTAMP

	-- Insert new record into tbIssuedCheck
	UPDATE dbo.tbIssuedCheck 
	SET Status = @Status, StatusDateTime = @DateTimeStamp
	WHERE IssuedCheckID = @IssuedCheckID
	AND AccountID = @AccountID

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		BEGIN
			ROLLBACK TRAN
			RAISERROR('upUpdateIssuedCheckStatus - issued check status was NOT successfully updated', 16, 1)
		END

END
GO
GRANT EXECUTE
ON dbo.upUpdateIssuedCheckStatus
TO system