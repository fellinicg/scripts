USE [TranSys]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061203
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE FUNCTION dbo.tfAccountPointBalances (@OwnerGUID int)
RETURNS table
AS

RETURN
(
	SELECT a.AccountID, dbo.ufStoredAccountBalance(a.AccountID) + SUM(ISNULL(p.PointsAmt, 0)) 'AccountPointBalance'
	FROM dbo.tbAccount a
	INNER JOIN dbo.tbAccount a2 ON a.AccountID = a2.AccountID
	LEFT JOIN dbo.tbPointTran p ON a2.AccountID = p.AccountID AND p.PointTranID > a2.BalanceTranID
	WHERE a.AccountType = 'P'
	AND a.OwnerGUID = @OwnerGUID
	GROUP BY a.AccountID
)

GO
GRANT SELECT
ON dbo.tfAccountPointBalances
TO system