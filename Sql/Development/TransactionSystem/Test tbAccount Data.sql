--Move test data from Points to tbAccount
insert into TranSys.dbo.tbAccount (
AccountType
,OwnerGUID
,AccountDesc
,ClientID
,ClientTranPeriodID
,BalanceTranID
,BalanceAmt
,Status
,StatusDateTime)
select distinct 
'P'
,p_UserID
,'Primary Point Account'
,p_ClientID
,3
,0
,0
,'O'
,CURRENT_TIMESTAMP
from dbo.Points
where p_ProgramID = 182
and p_Status = 0