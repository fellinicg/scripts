USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060731
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upSuppressTransactionDisplay
	@TransactionType char(1),  -- (P)oints, (C)ash, or (D)ebit Card
	@TranID int,
	@AccountID int,
	@OwnerGUID int,
	@SuppressDisplay bit,
	@Result char(1) OUTPUT  -- (S)uccess or (F)ailure
AS
BEGIN

	-- Local variable declarations
	DECLARE @Table varchar(20)
	DECLARE @Column varchar(20)
	DECLARE @Sql varchar(1000)

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'
	SET @Table = CASE @TransactionType 
								WHEN 'P' THEN 'dbo.tbPointTran' 
								WHEN 'C' THEN 'dbo.tbCashTran'
								WHEN 'D' THEN 'dbo.tbCardTran' 
								ELSE NULL END
	SET @Column = CASE @TransactionType 
								WHEN 'P' THEN 'PointTranID' 
								WHEN 'C' THEN 'CashTranID'
								WHEN 'D' THEN 'CardTranID' 
								ELSE NULL END

	-- ***** Check Inputs *****
	-- 
	-- Validate @TransactionType
	IF (@Table IS NULL)
		BEGIN
			RAISERROR('upSuppressTransactionDisplay - @TransactionType supplied is not a valid transation type on the system', 16, 1)
			RETURN
		END

	-- Validate @AccountID
	IF (dbo.ufValidateAccountOwner(@AccountID, @OwnerGUID) = 0)
		BEGIN
			RAISERROR('upSuppressTransactionDisplay - @AccountID supplied is not a valid account on the system', 16, 1)
			RETURN
		END

	-- Double check that no local variables are null
	IF (@Table IS NOT NULL AND @Column IS NOT NULL)
		BEGIN
			-- Create update statement
			SET @Sql = 'UPDATE ' + @Table + 
				' SET SuppressDisplay = ' + CONVERT(varchar, @SuppressDisplay) + 
				' WHERE ' + @Column + ' = ' + CONVERT(varchar, @TranID) + 
				' AND AccountID = ' + CONVERT(varchar, @AccountID)

			-- Update record in appropriate transaction table
			EXECUTE (@Sql)

			-- Verify success
			IF (@@ROWCOUNT = 1)
				SET @Result = 'S'
			ELSE
				RAISERROR('upSuppressTransactionDisplay - display suppression flag was NOT successful set', 16, 1)
		END

END
GO
GRANT EXECUTE
ON dbo.upSuppressTransactionDisplay
TO system