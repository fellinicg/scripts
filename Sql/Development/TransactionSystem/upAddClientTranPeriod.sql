USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060718
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upAddClientTranPeriod 
	@ClientID int, 
	@StartDateTime datetime, 
	@EndDateTime datetime, 
	@ClientTranPeriodID int OUTPUT
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Set start and end date correctly
	SET @StartDateTime = CONVERT(DATETIME, CONVERT(VARCHAR, @StartDateTime, 101))
	SET @EndDateTime = CONVERT(VARCHAR, @EndDateTime, 101) + ' 23:59:59.998'

	-- Insert new record into tbClientTranPeriod
	INSERT INTO dbo.tbClientTranPeriod (
		ClientID, 
		StartDateTime, 
		EndDateTime)
	VALUES (
		@ClientID, 
		@StartDateTime, 
		@EndDateTime)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@IDENTITY > 0)
		SET @ClientTranPeriodID = @@IDENTITY
	ELSE
		RAISERROR('upAddClientTranPeriod - client transaction period was NOT successfully added', 16, 1)

	SET NOCOUNT OFF

END
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upAddClientTranPeriod
TO system