USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060808
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE FUNCTION dbo.ufStoredAccountBalance 
(
	@AccountID int
)
RETURNS int
AS
BEGIN
	DECLARE @Result int
 
	SET @Result = 0

	SELECT @Result = BalanceAmt 
	FROM dbo.tbAccount
	WHERE AccountID = @AccountID

	RETURN @Result

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.ufStoredAccountBalance
TO system