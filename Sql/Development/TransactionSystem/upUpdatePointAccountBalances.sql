USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060803
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upUpdatePointAccountBalances
	@Result char(1) OUTPUT -- (S)uccess or (F)ailure

AS
BEGIN

	-- Local variable declarations
	DECLARE @AccountType char(1)

	-- Initialize settings
	SET NOCOUNT ON
	
	-- Initialize variables
	SET @Result = 'F'
	SET @AccountType = 'P'

	-- Store data in temporary table
	SELECT a.AccountID 'AcctID', a.BalanceAmt + SUM(p.PointsAmt) 'Balance', MAX(p.PointTranID) 'TranID'
	INTO #Temp
	FROM dbo.tbAccount a
	INNER JOIN dbo.tbPointTran p ON a.AccountID = p.AccountID 
	WHERE a.AccountType = @AccountType
	AND p.PointTranID > a.BalanceTranID
	GROUP BY a.AccountID, a.BalanceAmt

	-- Update records in tbAccount 
	UPDATE dbo.tbAccount SET BalanceAmt = Balance, BalanceTranID = TranID
	FROM dbo.tbAccount
	INNER JOIN #Temp ON AccountID = AcctID

	-- Verify success
	IF (@@ERROR = 0)
		SET @Result = 'S'
	ELSE
		RAISERROR('upUpdatePointAccountBalances - point account balances were NOT successfully updated', 16, 1)

	-- Drop temporary table
	DROP TABLE #Temp

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upUpdatePointAccountBalances
TO system