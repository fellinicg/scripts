USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060808
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upGetUserCashBalance
	@OwnerGUID int
AS
BEGIN

	-- Local variable declarations

	-- Initialize settings
	SET NOCOUNT ON
	
	-- Initialize variables

	-- Put accounts and balances into temporary table
	SELECT a.AccountID, dbo.ufStoredAccountBalance(a.AccountID) + SUM(ISNULL(c.CashAmtUSD, 0)) 'AccountCashBalance'
	INTO #Temp
	FROM dbo.tbAccount a
	INNER JOIN dbo.tbAccount a2 ON a.AccountID = a2.AccountID
	LEFT JOIN dbo.tbCashTran c ON a2.AccountID = c.AccountID AND c.CashTranID BETWEEN a2.BalanceTranID+1 AND 2147483647
	WHERE a.AccountType = 'C'
	AND a.OwnerGUID = @OwnerGUID
	GROUP BY a.AccountID

	-- Return Data
	SELECT AccountID, AccountCashBalance
	FROM #Temp

	SELECT SUM(AccountCashBalance) 'TotalCashBalance'
	FROM #Temp

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upGetUserCashBalance
TO system