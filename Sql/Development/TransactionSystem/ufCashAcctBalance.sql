USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061204
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER FUNCTION dbo.ufCashAcctBalance (@AccountID int)
RETURNS money
AS
BEGIN

	DECLARE @BalanceCash money
	DECLARE @BalanceTranID int

	-- STORED PORTION
	SELECT @BalanceCash = BalanceAmt, @BalanceTranID = BalanceTranID FROM dbo.tbAccount WHERE AccountID = @AccountID

	IF (@@ROWCOUNT = 0)
		RETURN (0)

	-- CALCULATED PORTION
	SELECT @BalanceCash = ISNULL(@BalanceCash, 0) + ISNULL(SUM(CashAmtUSD), 0) FROM dbo.tbCashTran WHERE AccountID = @AccountID AND CashTranID > @BalanceTranID

	RETURN (@BalanceCash)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.ufCashAcctBalance
TO system