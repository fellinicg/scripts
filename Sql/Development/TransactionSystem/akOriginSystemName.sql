USE [TranSys]
GO
ALTER TABLE dbo.tbOriginSystem ADD CONSTRAINT [akOriginSystemName] UNIQUE NONCLUSTERED 
(
	[Name] ASC
) ON [PRIMARY]