USE [TransactionSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060707
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upUpdateAccount 
	@AccountID int,
	@OwnerGUID int,
	@ClientID int,
	@AccountDesc varchar(40) = '',
	@ClientTranPeriodID varchar(20) = '',
	@Status char(1) = '',
	@Result char(1) OUTPUT -- (S)uccess or (F)ailure

AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp datetime
	DECLARE @FieldsToUpdate varchar(8000)

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CONVERT(VARCHAR, CURRENT_TIMESTAMP, 121)
	SET @FieldsToUpdate = ''
	SET @Result = 'F'

	-- ***** Check Inputs *****
	-- 
	-- Validate @ClientID
	IF (NOT EXISTS(SELECT 1 FROM SQL2KPROD.Catalog.dbo.Clients WHERE cl_ID = @ClientID))
	BEGIN
		RAISERROR('upUpdateAccount - @ClientID supplied is not a valid client id on the system', 16, 1)
		RETURN
	END

	-- @AccountDesc
	IF (LEN(LTRIM(RTRIM(@AccountDesc))) > 0)
		BEGIN
			SET @AccountDesc = UPPER(@AccountDesc)
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET AccountDesc = ''' + @AccountDesc + ''''
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',AccountDesc = ''' + @AccountDesc + ''''
		END

	-- @ClientTranPeriodID
	IF (LEN(LTRIM(RTRIM(@ClientTranPeriodID))) > 0)
		BEGIN	
			-- Validate @ClientTranPeriodID. Make sure the ClientTranPeriodID is a valid ID for the supplied client
			IF (NOT EXISTS(SELECT 1 FROM dbo.tbClientTranPeriod WHERE ClientTranPeriodID = @ClientTranPeriodID AND ClientID = @ClientID))
			BEGIN
				RAISERROR('upUpdateAccount - @ClientTranPeriodID supplied is not a valid client transaction period id on the system', 16, 1)
				RETURN
			END
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET ClientTranPeriodID = ' + @ClientTranPeriodID
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',ClientTranPeriodID = ' + @ClientTranPeriodID
		END

	-- @Status
	IF (LEN(LTRIM(RTRIM(@Status))) > 0)
		BEGIN	
			-- Validate @Status
			IF (NOT EXISTS(SELECT 1 FROM dbo.ufStatusTypes() WHERE Type = @Status))
			BEGIN
				RAISERROR('upUpdateAccount - @Status supplied is not a valid status on the system', 16, 1)
				RETURN
			END
			-- Build SET statement
			IF (LEN(@FieldsToUpdate) = 0) SET @FieldsToUpdate = ' SET Status = ''' + @Status + ''',StatusDateTime = ''' + @DateTimeStamp + ''''
			ELSE SET @FieldsToUpdate = @FieldsToUpdate + ',Status = ''' + @Status + ''',StatusDateTime = ''' + @DateTimeStamp + ''''
		END

	-- Update record in tbAccount if necessary
	IF (LEN(@FieldsToUpdate) > 0)
		BEGIN
			-- Added update timestamp
			--SET @FieldsToUpdate = @FieldsToUpdate + ',UpdateDateTime = ''' + @DateTimeStamp + ''''

			-- Execute update
			EXECUTE ('UPDATE dbo.tbAccount' + @FieldsToUpdate + ' WHERE AccountID = ' + @AccountID + ' AND OwnerGUID = ' + @OwnerGUID + ' AND ClientID = ' + @ClientID)
		END
	ELSE
		BEGIN
				RAISERROR('upUpdateAccount - no fields were specified for update', 16, 1)
				RETURN
		END

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upUpdateAccount - account was NOT successfully updated', 16, 1)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upUpdateAccount
TO system