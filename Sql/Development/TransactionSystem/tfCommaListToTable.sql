USE [TranSys]
GO
-- ============================================================
-- OrigAuthorFullName: Jason McElroy
-- OrigCreateDate: 20050321
-- LastEditedByFullName: Jason McElroy
-- LastEditDate: 20060621
-- ============================================================
CREATE FUNCTION dbo.tfCommaListToTable (@CommaDelimitedList varchar(8000))
RETURNS @ResultTable table (ListItem varchar(120) NOT NULL)
AS
BEGIN
DECLARE
	@TokenLen int,
	@Token varchar(120),
	@Remainder varchar(8000)

	SET @Remainder = @CommaDelimitedList

	WHILE LEN(@Remainder) > 0
	BEGIN
		SET @TokenLen = CASE PATINDEX('%,%', @Remainder) WHEN 0 THEN LEN(@Remainder) ELSE PATINDEX('%,%', @Remainder) - 1 END
		SET @Token = RTRIM(LTRIM(LEFT(@Remainder, @TokenLen)))
		INSERT @ResultTable(ListItem) VALUES(@Token)
		SET @Remainder = RIGHT(@Remainder, LEN(@Remainder) - @TokenLen)
		IF (LEFT(@Remainder, 1) = ',')
			SET @Remainder = RIGHT(@Remainder, LEN(@Remainder) - 1)
	END

	RETURN

END
GO
GRANT SELECT
ON dbo.tfCommaListToTable
TO system