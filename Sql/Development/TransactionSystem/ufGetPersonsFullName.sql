USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060716
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE FUNCTION dbo.ufGetPersonsFullName 
(
	@UserAccountID int
)
RETURNS varchar(200)
AS
BEGIN
	DECLARE @Result varchar(200)
 
	SET @Result = 'Unknown'

	SELECT @Result = ISNULL(PersonBuiltFullName, ISNULL(PersonFirstGivenName + ' ' + PersonFamilyName, ISNULL(AccountName, '')))
	FROM SQL2KPROD.GlobalUsers.dbo.tbUserAccount
	WHERE UserAccountID = @UserAccountID

	RETURN @Result

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.ufGetPersonsFullName
TO system