USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060731
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upUpdatePointTranOrderID
	@PointTranID int,
	@AccountID int,
	@OwnerGUID int,
	@ShoppingOrderID int,
	@Result char(1) OUTPUT -- (S)uccess or (F)ailure

AS
BEGIN

	-- Local variable declarations

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'

	-- ***** Check Inputs *****
	-- 
	-- Validate @AccountID
	IF (dbo.ufValidateAccountOwner(@AccountID, @OwnerGUID) = 0)
		BEGIN
			RAISERROR('upAddCardTransaction - @AccountID supplied is not a valid account on the system', 16, 1)
			RETURN
		END

	-- Update record in tbPointTran 
	UPDATE dbo.tbPointTran
	SET ShoppingOrderID = @ShoppingOrderID
	WHERE PointTranID = @PointTranID 
	AND AccountID = @AccountID 

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upUpdatePointTranOrderID - shopping order id was NOT successfully updated', 16, 1)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upUpdatePointTranOrderID
TO system