USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061204
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upGetUserCardAccountBalances
	@OwnerGUID int,
	@TotCardBal money OUTPUT

AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	
	-- Put accounts and balances into temporary table
	SELECT AccountID, AccountCardBalance
	FROM dbo.tfAccountCardBalances(@OwnerGUID)

	-- Set output parameter
	SELECT @TotCardBal = dbo.ufCardBalance(@OwnerGUID)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upGetUserCardAccountBalances
TO system