USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060727
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE FUNCTION dbo.ufValidateAccount 
(
	@AccountID int
)
RETURNS bit
AS
BEGIN
	DECLARE @Result bit
 
	SET @Result = 0

	SELECT @Result = 1 
	FROM dbo.tbAccount
	WHERE AccountID = @AccountID

	RETURN @Result

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.ufValidateAccount
TO system