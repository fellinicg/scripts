ALTER TABLE dbo.tbAccountType
	ADD IsDeleted bit NOT NULL DEFAULT 0

ALTER TABLE dbo.tbOriginSystem
	ADD IsDeleted bit NOT NULL DEFAULT 0

ALTER TABLE dbo.tbTranType
	ADD IsDeleted bit NOT NULL DEFAULT 0