USE [TranSys]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060804
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20061204
-- ============================================================
ALTER PROCEDURE dbo.upGetUserCashAccountBalance
	@AccountID int,
	@OwnerGUID int,
	@BalCashAmt money OUTPUT
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	
	-- VALIDATE
	IF (dbo.ufValidateAccountOwner(@AccountID, @OwnerGUID) <> 1)
	BEGIN
		RAISERROR('upGetUserCashAccountBalance - @AccountID, @OwnerGUID values supplied are invalid', 16, 1)
		RETURN
	END

	IF (dbo.ufAccountType(@AccountID) <> 'C')  -- CASH
	BEGIN
		RAISERROR('upGetUserCashAccountBalance - @AccountID supplied is not a points account', 16, 1)
		RETURN
	END

	-- RESULT
	SELECT @BalCashAmt = dbo.ufCashAcctBalance(@AccountID)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upGetUserCashAccountBalance
TO system