INSERT INTO TranSys.dbo.tbPointTran (
AccountID
,TranType
,PointsAmt
,TranDesc
,OriginSystemID
,InitiatingUserGUID
,ProgramID
,DKNumber
,SuppressDisplay
,MPGTranPeriodID
,ClientTranPeriodID
,TranDateTime
,RecordDateTime)
SELECT 
AccountID
,CASE ISNULL(p_Origin, '') WHEN '' THEN 'M' ELSE 'A' END
,p_Points
,CASE ISNULL(p_Origin, '') WHEN '' THEN 'Opening Balance' ELSE 'Points Awarded' END
,3
,p_Origin
,p_ProgramID
,p_DK
,0
,1
,3
,p_PointsDate
,p_DateAdded
FROM dbo.Points
INNER JOIN TranSys.dbo.tbAccount ON p_UserID = OwnerGUID AND p_ClientID = ClientID
WHERE p_ProgramID = 182
AND p_Status = 0
--AND p_Origin IS NULL
ORDER BY p_DateAdded

--SELECT 
--AccountID
--,CASE ISNULL(p_Origin, '') WHEN '' THEN 'M' ELSE 'A' END
--,p_Points
--,CASE ISNULL(p_Origin, '') WHEN '' THEN 'Opening Balance' ELSE 'Points Awarded' END
--,3
--,p_Origin
--,p_ProgramID
--,p_DK
--,0
--,1
--,3
--,p_PointsDate
--,p_DateAdded
--FROM OPENROWSET('SQLOLEDB','192.168.97.75';'sa';'',
--   'SELECT * FROM points.dbo.points where p_ProgramID = 182 and p_ID = 324095 order by p_id desc') AS a
--INNER JOIN TranSys.dbo.tbAccount ON a.p_UserID = OwnerGUID AND a.p_ClientID = ClientID
