USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060815
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upGetFileTypeP
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables

	-- Truncate working table
	TRUNCATE TABLE dbo.tbLG20_555

	-- Put data into working table.  Makes file creation much easier cause of widths.
	INSERT INTO dbo.tbLG20_555 (
		art_descr,
		art_gr_id,
		article,
		purch_amt,
		serial_flag,
		stock_flag,
		warehouse)
	SELECT
		REPLACE(mc_SDesc, CHAR(10), ' '),
		mc_it_ID,
		mc_ID,
		REPLACE(CONVERT(VARCHAR, ip_Cost), '.', ''),
		CASE mc_it_ID WHEN 2 THEN 1 ELSE 0 END,
		CASE mc_it_ID WHEN 2 THEN 1 ELSE 0 END,
		CASE mc_it_ID WHEN 2 THEN '488' ELSE '' END
	FROM SQL2KPROD.Catalog.dbo.MasterCatalog
	INNER JOIN SQL2KPROD.Catalog.dbo.ItemPricing ON mc_ip_ID = ip_ID
	WHERE mc_rs_RecStatus = 0

	-- Retrieve data from working table
	SELECT
		amount_set, 
		apar_gr_id, 
		apar_id, 
		art_descr, 
		art_gr_id, 
		article, 
		article_2, 
		article_3, 
		batch_id, 
		bflag, 
		bonus_type, 
		client, 
		contract_ctr, 
		contract_id, 
		currency, 
		date_from, 
		date_limitted, 
		date_to, 
		deliv_day, 
		disc_type, 
		ean, 
		fixed_contract, 
		guarantee, 
		kit_type, 
		language, 
		line_no, 
		location, 
		lot_flag, 
		max_level, 
		message_text, 
		multiplier, 
		order_level, 
		order_val, 
		overrun, 
		period_from, 
		period_to, 
		price_gr, 
		SPACE(20 - LEN(LTRIM(RTRIM(purch_amt)))) + LTRIM(RTRIM(purch_amt)), 
		purch_price, 
		purch_rule, 
		reference_1, 
		rel_attr_id1, 
		rel_attr_id2, 
		rel_attr_id3, 
		rel_attr_id4, 
		rel_attr_id5, 
		rel_value1, 
		rel_value2, 
		rel_value3, 
		rel_value4, 
		rel_value5, 
		rent_flag, 
		sales_price, 
		serial_cat, 
		serial_flag, 
		short_info, 
		status, 
		stock_flag, 
		tax_code_po, 
		text_type, 
		trans_type, 
		unit_code, 
		unit_descr, 
		unit_value, 
		update_flag, 
		value_1, 
		value_from, 
		value_to, 
		volume, 
		warehouse, 
		weight
	FROM dbo.tbLG20_555

END

GO