USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbPOHeader]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbPOHeader]
GO
CREATE TABLE dbo.tbPOHeader(
	[HeaderID] [int] IDENTITY(1,1) NOT NULL,
	[ItemLineID] [varchar](25) NULL,
	[VendorNo] [varchar](6) NULL,
	[PODate] [smalldatetime] NULL,
	[PONumber] [varchar](10) NULL,
	[FileProcessed] [bit] NOT NULL DEFAULT (0),
	[SessionID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_poheader] PRIMARY KEY NONCLUSTERED 
(
	[HeaderID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF