USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
USE [AgressoLink]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbReqDetail]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbReqDetail]
GO
CREATE TABLE dbo.tbReqDetail(
	[linetype] [char] (2) NOT NULL DEFAULT ('03'),
	[product] [char] (25) NOT NULL DEFAULT ('SPECIAL'),
	[description] [char] (255) NOT NULL,
	[unit] [char] (3) NOT NULL DEFAULT ('EA'),
	[supplierid] [char] (25) NOT NULL DEFAULT (''),
	[deliverydate] [char] (8) NOT NULL DEFAULT (''),
	[number] [char] (10) NOT NULL,
	[currency] [char] (3) NOT NULL DEFAULT ('USD'),
	[price] [char] (10) NOT NULL,
	[account] [char] (25) NOT NULL DEFAULT ('5000'),
	[dim1] [char] (25) NOT NULL,
	[dim2] [char] (25) NOT NULL,
	[dim3] [char] (25) NOT NULL DEFAULT (''),
	[dim4] [char] (25) NOT NULL DEFAULT (''),
	[dim5] [char] (25) NOT NULL DEFAULT ('700'),
	[dim6] [char] (25) NOT NULL DEFAULT ('NYC'),
	[dim7] [char] (25) NOT NULL DEFAULT ('N'),
	[taxcode] [char] (25) NOT NULL DEFAULT ('0'),
	[taxsystem] [char] (25) NOT NULL DEFAULT ('0'),
	[itemlineid] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF