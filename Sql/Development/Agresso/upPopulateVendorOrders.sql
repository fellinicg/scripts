USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061011
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upPopulateVendorOrders (
	@Result char(1) output
)
AS
BEGIN

	DECLARE @OrderID int
	DECLARE @TimeStamp datetime
	DECLARE @SessionID uniqueidentifier
	DECLARE @RowCT int

	-- Initialize variables
	SET @OrderID = dbo.ufSettingValue('LastOrderProcessed')
	SET @TimeStamp = CURRENT_TIMESTAMP
	SET @SessionID = dbo.ufSettingValue('POSessionID')
	SET @Result = 'E'

	-- Insert data
	INSERT INTO dbo.tbVendorOrders(VendorID, RecordDateTime, SessionID)
	SELECT DISTINCT VendorID, @TimeStamp, @SessionID
	FROM OPENQUERY (CURLY, 
		'SELECT il.VendorID, oh.OrderID
		FROM FulFilment.dbo.OrderHeader oh
		INNER JOIN FulFilment.dbo.AwardLine al ON oh.OrderID = al.OrderID
		INNER JOIN FulFilment.dbo.ItemLine il ON al.AwardLineID = il.AwardLineID
		WHERE al.Processed = 0
		AND al.Void = 0
		AND ISNULL(il.VendorID, 0) > 0
		AND oh.OrderDate > ''12/31/2006''')
	INNER JOIN Agresso.dbo.asuheader ON client = 'MP' AND apar_gr_id = 'S6' AND VendorID = apar_id
	WHERE OrderID <= @OrderID

	-- Store row count
	SET @RowCT = @@ROWCOUNT

	-- Check results
	IF (@@ERROR = 0)
		IF (@RowCT = 0)
			SET @Result = 'N'
		ELSE
			SET @Result = 'S'

END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

