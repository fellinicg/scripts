USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION dbo.ufCreateUserID (@Input varchar(10))
RETURNS varchar(20)
AS
BEGIN

	DECLARE @Output varchar(20)
  
	SET @Output = ''

	WHILE LEN(@Input) > 0
		BEGIN
			SET @Output = @Output + CONVERT(char(2), ASCII(LEFT(@Input, 1)))
			SET @Input = SUBSTRING(@Input, 2, LEN(@Input)-1)
		END

	SET @Output = REVERSE(@Output)

  RETURN(@Output)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF