USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061011
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upCreatePOFile
AS
BEGIN
	DECLARE @Result char(1)
	DECLARE @SessionID uniqueidentifier
	DECLARE @OrderNo int
	DECLARE @StartTime smalldatetime

	SET NOCOUNT ON

	-- Set session values
	EXECUTE dbo.upSetSessionValues 'P'
	SET @SessionID = dbo.ufSettingValue('POSessionID')

	-- Vendor Orders
	EXECUTE dbo.upPopulateVendorOrders @Result OUTPUT

	-- Check result
	IF (@Result = 'E')
		BEGIN
				INSERT INTO dbo.tbOrderProcessLog (LastOrderProcessed, ProcessDateTime, SessionID, Comment)
				VALUES (dbo.ufSettingValue('LastOrderProcessed'), CURRENT_TIMESTAMP, @SessionID, 'An error occured in upPopulateVendorOrders.  Process aborted.')
				RETURN 1
		END
	IF (@Result = 'N')
		BEGIN
				INSERT INTO dbo.tbOrderProcessLog (LastOrderProcessed, ProcessDateTime, SessionID, Comment)
				VALUES (dbo.ufSettingValue('LastOrderProcessed'), CURRENT_TIMESTAMP, @SessionID, 'No vendors returned.  Process aborted.')
				RETURN 1
		END

	-- Item Orders
	EXECUTE dbo.upPopulateItemOrder @Result OUTPUT

	-- Check result
	IF (@Result = 'E')
		BEGIN
				INSERT INTO dbo.tbOrderProcessLog (LastOrderProcessed, ProcessDateTime, SessionID, Comment)
				VALUES (dbo.ufSettingValue('LastOrderProcessed'), CURRENT_TIMESTAMP, @SessionID, 'An error occured in upPopulateItemOrder.  Process aborted.')
				RETURN 1
		END
	IF (@Result = 'N')
		BEGIN
				INSERT INTO dbo.tbOrderProcessLog (LastOrderProcessed, ProcessDateTime, SessionID, Comment)
				VALUES (dbo.ufSettingValue('LastOrderProcessed'), CURRENT_TIMESTAMP, @SessionID, 'No items returned.  Process aborted.')
				RETURN 1
		END

	-- Assign Line Numbers
	EXECUTE dbo.upAssignLineNumbers @Result OUTPUT

	-- Check result
	IF (@Result = 'E')
		BEGIN
				INSERT INTO dbo.tbOrderProcessLog (LastOrderProcessed, ProcessDateTime, SessionID, Comment)
				VALUES (dbo.ufSettingValue('LastOrderProcessed'), CURRENT_TIMESTAMP, @SessionID, 'An error occured in upAssignLineNumbers.  Process aborted.')
				RETURN 1
		END
	IF (@Result = 'N')
		BEGIN
				INSERT INTO dbo.tbOrderProcessLog (LastOrderProcessed, ProcessDateTime, SessionID, Comment)
				VALUES (dbo.ufSettingValue('LastOrderProcessed'), CURRENT_TIMESTAMP, @SessionID, 'PO line numbers could not be assigned.  Process aborted.')
				RETURN 1
		END

	-- Update Agresso database
	EXECUTE dbo.upGetFileTypeO @Result OUTPUT

	-- Check result
	IF (@Result = 'E')
		BEGIN
				INSERT INTO dbo.tbOrderProcessLog (LastOrderProcessed, ProcessDateTime, SessionID, Comment)
				VALUES (dbo.ufSettingValue('LastOrderProcessed'), CURRENT_TIMESTAMP, @SessionID, 'An error occured in upGetFileTypeO.  Process aborted.')
				RETURN 1
		END
	IF (@Result = 'N')
		BEGIN
				INSERT INTO dbo.tbOrderProcessLog (LastOrderProcessed, ProcessDateTime, SessionID, Comment)
				VALUES (dbo.ufSettingValue('LastOrderProcessed'), CURRENT_TIMESTAMP, @SessionID, 'Unable to populate Agresso database.  Process aborted.')
				RETURN 1
		END

	-- Submit Agresso job to process PO
	EXECUTE dbo.upSubmitJob 'LG04', 4, 'LI', 'MP', 'SYSTEM', 1, '4444', @OrderNo OUTPUT

	-- Monitor Agresso job.  Status gets set to 'T' when it is complete
	SET @StartTime = CURRENT_TIMESTAMP
	WHILE NOT EXISTS (SELECT 1 FROM Agresso.dbo.acrrepord WHERE report_name = 'LG04' 
										AND func_id = 4 AND orderno = @OrderNo AND Status = 'T')
		BEGIN
			-- Only poll for 10 minutes
			IF (DATEDIFF(mi, @StartTime, CURRENT_TIMESTAMP) = 10)
				BEGIN
					-- Log Process
					INSERT INTO dbo.tbOrderProcessLog (LastOrderProcessed, ProcessDateTime, SessionID, Comment)
					VALUES (dbo.ufSettingValue('LastOrderProcessed'), CURRENT_TIMESTAMP, @SessionID, 'Agresso job did not respond in alotted timeframe.')
					RETURN 1
				END
			-- Delay loop for 1 minute
			WAITFOR DELAY '0:01'
		END

	-- If there are	records left in algbatchinput in the Agresso DB then the job failed
	IF EXISTS (SELECT 1 FROM Agresso.dbo.algbatchinput)
		BEGIN
			-- Log Process
			INSERT INTO dbo.tbOrderProcessLog (LastOrderProcessed, ProcessDateTime, SessionID, Comment)
			VALUES (dbo.ufSettingValue('LastOrderProcessed'), CURRENT_TIMESTAMP, @SessionID, 'Agresso job failed. Check error logs.')
			RETURN 1
		END
	ELSE
		-- Log Process
		INSERT INTO dbo.tbOrderProcessLog (LastOrderProcessed, ProcessDateTime, SessionID, Comment)
		VALUES (dbo.ufSettingValue('LastOrderProcessed'), CURRENT_TIMESTAMP, @SessionID, 'PO process completed successfully.')

	-- Update PO numbers in tbItemOrder
	EXECUTE dbo.upUpdateItemOrder

	-- Update PO numbers in Fulfilment
--	EXECUTE dbo.upUpdateItemLine

END

GO