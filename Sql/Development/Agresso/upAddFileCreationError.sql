USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060815
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upAddFileCreationError 
	@Description varchar(5000),
	@SessionID uniqueidentifier
AS

	SET NOCOUNT ON

	INSERT INTO dbo.tbFileCreationError(Description, SessionID) VALUES(@Description, @SessionID)
	
	SET NOCOUNT OFF

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF