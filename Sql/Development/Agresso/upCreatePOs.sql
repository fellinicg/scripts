USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20070209
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20070302
-- Notes:	Status list for the AgressoStatus field in CURLY.Fulfilment.dbo.ItemLine
--					U = Unprocessed
--					P = Processing PO
--					V = Voided in AwardLine
--					R = Request Processed
--					M = Missing info (Vendor, DK, etc)
--					C = Creation of PO Completed (PONumber and POLine should be populated)
--					F = Failure
-- ============================================================
ALTER PROCEDURE dbo.upCreatePOs
AS
BEGIN
	DECLARE @Result char(1)
	DECLARE @OrderNo int
	DECLARE @StartTime smalldatetime
	DECLARE @RowCT int

	-- Initialize settings
	SET NOCOUNT ON

	-- Set all unprocessed itemlines to Processing PO status (P)
	UPDATE CURLY.Fulfilment.dbo.ItemLine SET AgressoStatus = 'P' WHERE AgressoStatus = 'U'

	-- Check result
	IF (@@ROWCOUNT = 0)
		BEGIN
				-- Alert user
				RAISERROR('No items to be processed.  Process aborted.', 16, 1)
				-- Exit Proc
				RETURN 1
		END

	IF (@@ERROR > 0)
		BEGIN
				-- Alert user
				RAISERROR('An error occured trying to update AgressoStatus to (P).  Process aborted.', 16, 1)
				-- Exit Proc
				RETURN 1
		END

	-- Set all Processing PO statuses that are set to Void in AwardLine to Voided in AwardLine status (V)
	EXEC CURLY.Fulfilment.dbo.upSetFlagsPOProcess

	-- Add Header Info to Agresso database
	INSERT INTO Agresso.dbo.algbatchinput(accountable, amount_set, apar_id, apar_id_ref, att_7_id, batch_id, 
		client, control, currency, deliv_method, deliv_terms, ext_ord_ref, intrule_id, line_no, order_id, 
		order_type, responsible, responsible2, status, tax_code, tax_system, trans_type, voucher_type)
	SELECT 'JOANL', 1, VendorID, 'AP', 'BA', '4444', 
		'MP', 'N', 'USD', 'DROPSHIP', 'VNOTICE', 'Award Redemption Request', '0', '0', VendorID, 
		'DS', 'LSILVERSTEIN', 'LSILVERSTEIN', 'N', '0', '0', '41', '13'
	FROM OPENQUERY(CURLY, 'SELECT DISTINCT il.VendorID FROM Fulfilment.dbo.ItemLine il
		INNER JOIN Fulfilment.dbo.AwardLine al ON il.AwardLineID = al.AwardLineID AND al.Void = 0
		WHERE il.AgressoStatus = ''P''')

	-- Store row count
	SET @RowCT = @@ROWCOUNT

	-- Check result
	IF (@@ERROR > 0)
		BEGIN
				-- Alert user
				RAISERROR('An error occured while populating header info in Agresso database.  Process aborted.', 16, 1)
				-- Exit Proc
				RETURN 1
		END
	IF (@RowCT = 0)
		BEGIN
				-- Alert user
				RAISERROR('Unable to populate header info in Agresso database.  Process aborted.', 16, 1)
				-- Exit Proc
				RETURN 1
		END

	-- Add Detail Info to Agresso database
	INSERT INTO Agresso.dbo.algbatchinput(accountable, amount, amount_set, apar_id_ref, art_descr, article,
		att_1_id, att_2_id, att_5_id, att_7_id, batch_id, client, control, currency, deliv_method, deliv_terms,
		dim_1, dim_2, dim_5, dim_value_1, dim_value_2, dim_value_5, ext_ord_ref, intrule_id, line_no, order_id,
		order_type, status, sup_article, tax_code,  tax_system, trans_type, unit_code, value_1, voucher_type)
	SELECT 'JOANL', f.CostPerUnit, 1, 'AP', f.Description, 'SPECIAL',
		'C1', 'B0', 'B1', 'BA', '4444', 'MP', 'N', 'USD', 'DROPSHIP', 'VNOTICE',
		ISNULL(pr.department, 0), f.ProjectNumber, '700', ISNULL(pr.department, 0), f.ProjectNumber, '700', 'Award Redemption Request', '0', 1,	f.VendorID,
		'DS',	'N', f.ItemLineID, '0', '0', '41', 'EA', f.Quantity, '13' 
	FROM OPENQUERY(CURLY, 'SELECT il.ItemLineID, il.CostPerUnit,
				CONVERT(varchar, oh.Projectnumber) + '' * '' + oh.ShipName + '' * '' +
				CASE CHARINDEX(''Full Amount'', IL.ItemOptions) WHEN 0 THEN IL.ItemTitle
					ELSE SUBSTRING(IL.ItemTitle, 1, CHARINDEX(''$'', IL.ItemTitle)) + CONVERT(VARCHAR, CONVERT(INT, IL.CostPerUnit * AL.Quantity)) +
					SUBSTRING(IL.ItemTitle, CHARINDEX('' '', IL.ItemTitle, CHARINDEX(''$'', IL.ItemTitle)), 1000) END AS Description,
				CASE CHARINDEX(''Full Amount'', IL.ItemOptions) WHEN 0 THEN AL.Quantity ELSE 1 END AS Quantity,
				oh.Projectnumber,
				il.VendorID
			FROM Fulfilment.dbo.ItemLine il
			INNER JOIN Fulfilment.dbo.AwardLine al ON il.AwardLineID = al.AwardLineID
			INNER JOIN Fulfilment.dbo.OrderHeader oh ON al.OrderID = oh.OrderID
			WHERE il.AgressoStatus = ''P''') f
	LEFT JOIN Agresso.dbo.atsproject pr ON pr.client = 'MP' AND pr.project = CONVERT(varchar, f.ProjectNumber)

	-- Store row count
	SET @RowCT = @@ROWCOUNT

	-- Check result
	IF (@@ERROR > 0)
		BEGIN
				-- Alert user
				RAISERROR('An error occured while populating detail info in Agresso database.  Process aborted.', 16, 1)
				-- Exit Proc
				RETURN 1
		END
	IF (@RowCT = 0)
		BEGIN
				-- Alert user
				RAISERROR('Unable to populate detail info in Agresso database.  Process aborted.', 16, 1)
				-- Exit Proc
				RETURN 1
		END

	-- Assign Line Numbers
	EXECUTE dbo.upAssignLineNumbers @Result OUTPUT

	-- Check result
	IF (@Result = 'E')
		BEGIN
				-- Alert user
				RAISERROR('An error occured in upAssignLineNumbers.  Process aborted.', 16, 1)
				-- Exit Proc
				RETURN 1
		END

	-- Submit Agresso job to process PO
	EXECUTE dbo.upSubmitJob 'LG04', 4, 'LI', 'MP', 'SYSTEM', 1, '4444', @OrderNo OUTPUT

	-- Monitor Agresso job.  Status gets set to 'T' when it is complete
	SET @StartTime = CURRENT_TIMESTAMP
	WHILE NOT EXISTS (SELECT 1 FROM Agresso.dbo.acrrepord WHERE report_name = 'LG04' 
										AND func_id = 4 AND orderno = @OrderNo AND Status = 'T')
		BEGIN
			-- Only poll for 10 minutes
			IF (DATEDIFF(mi, @StartTime, CURRENT_TIMESTAMP) = 10)
				BEGIN
					-- Alert user
					RAISERROR('Agresso job did not respond in alotted timeframe.', 16, 1)
					-- Exit Proc
					RETURN 1
				END
			-- Delay loop for 1 minute
			WAITFOR DELAY '0:01'
		END

	-- If there are	records left in algbatchinput in the Agresso DB then the job failed
	IF EXISTS (SELECT 1 FROM Agresso.dbo.algbatchinput)
		BEGIN
			-- Alert user
			RAISERROR('Agresso job failed. Check error logs.', 16, 1)
			-- Exit Proc
			RETURN 1
		END

	-- Alert user
	SELECT 'POs successfully created in Agresso.'

	-- Update ItemLine info in Fulfilment
	EXECUTE CURLY.Fulfilment.dbo.upUpdateItemLineFromAgresso

	-- Set all processed itemlines to Missing Info status (M)
	UPDATE CURLY.Fulfilment.dbo.ItemLine SET AgressoStatus = 'M' WHERE AgressoStatus = 'P'

	-- Post process for PO Platform on the Portal
	EXECUTE CURLY.Fulfilment.dbo.POP_ImpPONumbers

	-- Alert user
	SELECT 'PO process completed.'

END

