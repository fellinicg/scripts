USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbGL07_55]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbGL07_55]
GO
CREATE TABLE dbo.tbGL07_55(
	[batch_id] [char] (25) NOT NULL,
	[interface] [char] (25) NOT NULL DEFAULT ('BI'),
	[voucher_type] [char] (25) NOT NULL DEFAULT ('AP'),
	[trans_type] [char] (2) NOT NULL,
	[client] [char] (25) NOT NULL DEFAULT ('MP'),
	[account] [char] (25) NOT NULL,
	[dim_1] [char] (25) NOT NULL,
	[dim_2] [char] (25) NOT NULL,
	[dim_3] [char] (25) NOT NULL DEFAULT (''),
	[dim_4] [char] (25) NOT NULL DEFAULT (''),
	[dim_5] [char] (25) NOT NULL DEFAULT ('625'),
	[dim_6] [char] (25) NOT NULL DEFAULT (''),
	[dim_7] [char] (25) NOT NULL DEFAULT (''),
	[tax_code] [char] (25) NOT NULL DEFAULT ('0'),
	[tax_system] [char] (25) NOT NULL DEFAULT (''),
	[currency] [char] (25) NOT NULL DEFAULT ('USD'),
	[dc_flag] [char] (2) NOT NULL DEFAULT (''),
	[cur_amount] [char] (20) NOT NULL,
	[amount] [char] (20) NOT NULL,
	[number_1] [char] (11) NOT NULL DEFAULT (''),
	[value_1] [char] (20) NOT NULL,
	[value_2] [char] (20) NOT NULL DEFAULT (''),
	[value_3] [char] (20) NOT NULL DEFAULT (''),
	[description] [char] (255) NOT NULL,
	[trans_date] [char] (8) NOT NULL,
	[voucher_date] [char] (8) NOT NULL,
	[voucher_no] [char] (15) NOT NULL DEFAULT (''),
	[period] [char] (6) NOT NULL,
	[tax_id] [char] (1) NOT NULL DEFAULT (''),
	[ext_inv_ref] [char] (100) NOT NULL,
	[ext_ref] [char] (255) NOT NULL DEFAULT (''),
	[due_date] [char] (8) NOT NULL,
	[disc_date] [char] (8) NOT NULL DEFAULT (''),
	[discount] [char] (20) NOT NULL DEFAULT (''),
	[commitment] [char] (25) NOT NULL DEFAULT (''),
	[order_id] [char] (15) NOT NULL DEFAULT (''),
	[kid] [char] (27) NOT NULL DEFAULT (''),
	[pay_transfer] [char] (2) NOT NULL DEFAULT (''),
	[status] [char] (1) NOT NULL DEFAULT ('N'),
	[apar_type] [char] (1) NOT NULL DEFAULT (''),
	[apar_id] [char] (25) NOT NULL,
	[pay_flag] [char] (1) NOT NULL DEFAULT (''),
	[voucher_ref] [char] (15) NOT NULL DEFAULT (''),
	[sequence_ref] [char] (9) NOT NULL DEFAULT (''),
	[intrule_id] [char] (25) NOT NULL DEFAULT (''),
	[factor_short] [char] (25) NOT NULL DEFAULT (''),
	[responsible] [char] (25) NOT NULL DEFAULT (''),
	[apar_name] [char] (255) NOT NULL DEFAULT (''),
	[address] [char] (160) NOT NULL DEFAULT (''),
	[province] [char] (40) NOT NULL DEFAULT (''),
	[place] [char] (40) NOT NULL DEFAULT (''),
	[bank_account] [char] (35) NOT NULL DEFAULT (''),
	[pay_method] [char] (2) NOT NULL DEFAULT (''),
	[vat_reg_no] [char] (25) NOT NULL DEFAULT (''),
	[zip_code] [char] (15) NOT NULL DEFAULT (''),
	[curr_licence] [char] (3) NOT NULL DEFAULT (''),
	[account2] [char] (25) NOT NULL DEFAULT (''),
	[base_amount] [char] (20) NOT NULL DEFAULT (''),
	[base_curr] [char] (20) NOT NULL DEFAULT (''),
	[pay_temp_id] [char] (4) NOT NULL DEFAULT (''),
	[allocation_key] [char] (2) NOT NULL DEFAULT (''),
	[period_no] [char] (2) NOT NULL DEFAULT (''),
	[clearing_code] [char] (13) NOT NULL DEFAULT (''),
	[swift] [char] (11) NOT NULL DEFAULT (''),
	[arrive_id] [char] (15) NOT NULL DEFAULT (''),
	[bank_acc_type] [char] (2) NOT NULL DEFAULT ('')
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF