USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060815
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upGetMasterProductList
AS
BEGIN
	DECLARE @ct INT
	DECLARE @RC INT
	DECLARE @Result INT
	DECLARE @bcpcmd VARCHAR(5000)
	DECLARE @FileType CHAR(1)
	DECLARE @DBProcName VARCHAR(40)
	DECLARE @OutputFileName VARCHAR(40)
	DECLARE @ArchiveDate VARCHAR(8)
	DECLARE @ParamStr VARCHAR(500)

	SET NOCOUNT ON

	-- Initialize variables
	SET @ct = 1
	SET @Result = 1
	SET @ArchiveDate = (SELECT CONVERT(VARCHAR, GETDATE(), 112))

	-- Map Z: drive to share
	SET @bcpcmd = 'net use z: /DELETE&net use z: ' + dbo.ufSettingValue('FileCreationDir')
	EXECUTE @RC = master..xp_cmdshell @bcpcmd

	-- Error check
	IF @RC <> 0 
		BEGIN
			EXECUTE dbo.upAddFileCreationError 'Error occured while mapping share during upGetMasterProductList'
			RETURN
		END

	-- Assign necessary info to local variables
	SELECT 
		@FileType = FileType,
		@OutputFileName = OutputFileName,
		@DBProcName = DBProcName
	FROM dbo.tbFileCreation
	WHERE FileType IN ('P')

	-- Check for error
	IF @RC <> 0 
		BEGIN
			SET @ParamStr = 'Error occured while retrieving column headers for report type ' + @FileType
			EXECUTE dbo.upAddFileCreationError @ParamStr
			GOTO CLEANUP
		END

	-- Get report data
	SET @bcpcmd = 'bcp "EXEC ' + @DBProcName + '" queryout "z:\output\' + @OutputFileName + '" -S ' + @@SERVERNAME + ' -T -c -t '
	EXECUTE @RC = master..xp_cmdshell @bcpcmd

	-- Check for error
	IF @RC <> 0 
		BEGIN
			SET @ParamStr = 'Error occured while executing ' + @DBProcName
			EXECUTE dbo.upAddFileCreationError @ParamStr
			GOTO CLEANUP
		END

	-- Archive file
	SET @bcpcmd = 'copy z:\output\' + @OutputFileName + ' z:\archive\w' + @ArchiveDate + '-' + @OutputFileName
	EXECUTE @RC = master..xp_cmdshell @bcpcmd

	-- Check for error
	IF @RC <> 0 
		BEGIN
			SET @ParamStr = 'Error occured while archiving ' + @OutputFileName
			EXECUTE dbo.upAddFileCreationError @ParamStr
			SET @bcpcmd = 'del z:\archive\' + @ArchiveDate + '*.* /F /Q'
			EXECUTE master..xp_cmdshell @bcpcmd
			GOTO CLEANUP
		END

	-- If all went well, set result variable to 0
	SET @Result = 0

	-- Add log entries
	SET @ParamStr = @ArchiveDate + '-' + @OutputFileName
	EXECUTE dbo.upAddFileCreationLog @FileType, @ParamStr

CLEANUP:

	-- Delete all files in working directory
	EXECUTE master..xp_cmdshell 'del z:\working\*.* /F /Q'

--	-- Delete all files in output directory
--	EXECUTE master..xp_cmdshell 'del z:\output\weekly_reports\*.* /F /Q'

	-- Delete mapped drive
	EXECUTE @RC = master..xp_cmdshell 'net use z: /DELETE'

	-- Return result code
	RETURN @Result

END

GO