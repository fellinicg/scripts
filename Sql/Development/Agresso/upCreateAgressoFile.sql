USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061017
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upCreateAgressoFile
(
	@FileType char(1),
	@DBProcName varchar(40),
	@OutputFileName varchar(200),
	@SessionID uniqueidentifier
)
AS
BEGIN
	DECLARE @ct INT
	DECLARE @RC INT
	DECLARE @Result INT
	DECLARE @bcpcmd varchar(5000)
	DECLARE @ArchiveDate varchar(17)
	DECLARE @ParamStr varchar(500)
	DECLARE @WorkingDir varchar(400)
	DECLARE @ArchiveDir varchar(400)
	DECLARE @OutputDir varchar(400)

	SET NOCOUNT ON

	-- Initialize variables
	SET @ct = 1
	SET @Result = 1
	SET @ArchiveDate = CONVERT(VARCHAR, CURRENT_TIMESTAMP, 112) 
		+ 'h' + CONVERT(varchar, DATEPART(hh, CURRENT_TIMESTAMP)) 
		+ 'm' + CONVERT(varchar, DATEPART(mm, CURRENT_TIMESTAMP))
		+ 's' + CONVERT(varchar, DATEPART(ss, CURRENT_TIMESTAMP))
	SET @WorkingDir = dbo.ufSettingValue('FileCreationWorkingDir')
	SET @ArchiveDir = dbo.ufSettingValue('FileCreationArchiveDir')
	SET @OutputDir = dbo.ufSettingValue('FileCreationOutputDir')

	-- Map Z: drive to share
	SET @bcpcmd = 'net use z: /DELETE&net use z: ' + @WorkingDir
	EXECUTE @RC = master..xp_cmdshell @bcpcmd

	-- Error check
	IF @RC <> 0 
		BEGIN
			EXECUTE dbo.upAddFileCreationError 'Error occured while mapping share during upCreateAgressoFile', @SessionID
			RETURN
		END

	-- Get report data
	SET @bcpcmd = 'bcp "EXEC ' + @DBProcName + '" queryout "z:\' + @OutputFileName + '" -S ' + @@SERVERNAME + ' -T -c -t '
	EXECUTE @RC = master..xp_cmdshell @bcpcmd

	-- Check for error
	IF @RC <> 0 
		BEGIN
			SET @ParamStr = 'Error occured while executing ' + @DBProcName
			EXECUTE dbo.upAddFileCreationError @ParamStr, @SessionID
			GOTO CLEANUP
		END

	-- Archive file
	SET @bcpcmd = 'copy z:\' + @OutputFileName + ' ' + @ArchiveDir + '\' +  @ArchiveDate + @OutputFileName
	EXECUTE @RC = master..xp_cmdshell @bcpcmd

	-- Check for error
	IF @RC <> 0 
		BEGIN
			SET @ParamStr = 'Error occured while archiving ' + @OutputFileName + ' (' + @bcpcmd + ')'
			EXECUTE dbo.upAddFileCreationError @ParamStr, @SessionID
			GOTO CLEANUP
		END

	-- Copy file to output directory
	SET @bcpcmd = 'copy z:\' + @OutputFileName + ' ' + @OutputDir
	EXECUTE @RC = master..xp_cmdshell @bcpcmd

	-- Check for error
	IF @RC <> 0 
		BEGIN
			SET @ParamStr = 'Error occured while copying ' + @OutputFileName + ' to ' + @OutputDir
			EXECUTE dbo.upAddFileCreationError @ParamStr, @SessionID
			SET @bcpcmd = 'del ' + @ArchiveDir + '\' + @ArchiveDate + @OutputFileName + ' /F /Q'
			EXECUTE master..xp_cmdshell @bcpcmd
			GOTO CLEANUP
		END

	-- If all went well, set result variable to 0
	SET @Result = 0

	-- Add log entries
	SET @ParamStr = @ArchiveDate + @OutputFileName
	EXECUTE dbo.upAddFileCreationLog @FileType, @ParamStr, @SessionID

CLEANUP:

	-- Delete file in working directory
	SET @bcpcmd = 'del ' + @WorkingDir + '\' +  @OutputFileName + ' /F /Q'
	EXECUTE master..xp_cmdshell @bcpcmd 

	-- Delete mapped drive
	EXECUTE @RC = master..xp_cmdshell 'net use z: /DELETE'

	-- Return result code
	RETURN @Result

END
