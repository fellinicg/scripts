USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbOrderProcessLog]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbOrderProcessLog]
GO
CREATE TABLE [dbo].[tbOrderProcessLog](
	[OrderProcessLogID] [int] IDENTITY(1,1) NOT NULL,
	[LastOrderProcessed] [bigint] NOT NULL,
	[ProcessDateTime] [datetime] NOT NULL,
	[SessionID] [uniqueidentifier] NOT NULL,
	[Comment] [varchar](255) NULL,
 CONSTRAINT [PK_tbOrderProcessLog] PRIMARY KEY NONCLUSTERED 
(
	[OrderProcessLogID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF