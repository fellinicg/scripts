USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20070130
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upSubmitJob (
	@ReportName varchar(25),
	@FuncId int,
	@Module varchar(25),
	@Client varchar(25),
	@UserId varchar(25),
	@Variant int,
	@BatchID varchar(25),
	@OrderNo int output
)
AS
BEGIN

	-- Declarations
	DECLARE @StartDateTime datetime
	DECLARE @CurrPeriod char(6)

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @OrderNo = 0
	SET @StartDateTime = CURRENT_TIMESTAMP
	SELECT @CurrPeriod = curr_period FROM Agresso.dbo.acrclient WHERE client = @Client

	-- Retrieve orderno
	SELECT @OrderNo = orderno FROM Agresso.dbo.aagreporder WHERE report_name = @ReportName

	-- Check orderno
	IF (@OrderNo = 0) -- No entry, so add an entry
		BEGIN
			SET @OrderNo = 1
			INSERT INTO Agresso.dbo.aagreporder (orderno, report_name)
			VALUES (@OrderNo + 1, @ReportName)
		END
	ELSE -- Increment orderno
		UPDATE Agresso.dbo.aagreporder SET orderno = @OrderNo + 1
		WHERE report_name = @ReportName

	-- Insert report header	
	INSERT INTO Agresso.dbo.acrrepord (expire_days, poll_status, user_id, report_name, report_id, report_type,
		report_cols, client, copies,	order_date, server_queue, priority,	priority_no, printer, status,	last_update, 
		orderno, func_arg, description, module, func_id, mail_flag, output_id, variant, invoke_time)
	SELECT 0, 'N', @UserId, @ReportName, s.argument, 'B', r.report_cols, @Client, 1, @StartDateTime, r.server_queue, 
		r.priority, r.priority_no, r.printer, 'N', @StartDateTime, @OrderNo, s.assembly, r.description, @Module, 
		@FuncId, r.mail_flag, r.output_id, @Variant, @StartDateTime
	FROM Agresso.dbo.aagrepdef r
	INNER JOIN Agresso.dbo.asysmenu m ON r.module = m.module AND r.func_id = m.func_id
	INNER JOIN Agresso.dbo.asysfunction s ON m.func_name = s.func_name AND m.func_type = s.func_type
	WHERE r.report_name = @ReportName 
		AND r.func_id = @FuncId 
		AND r.module = @Module 
		AND r.variant = @Variant

	-- Insert report parameters
	INSERT INTO Agresso.dbo.acrparord (client, data_length, orderno, param_id, param_name, 
		param_val, report_name, sequence_no, text_type )
	SELECT @Client, p.data_length, @OrderNo, p.param_id, p.title, 
		ISNULL(dbo.ufGetParamValue(p.param_id, @CurrPeriod, @ReportName, @BatchID), p.param_def) param_def, 
		@ReportName, p.sequence_no, p.text_type
	FROM Agresso.dbo.aagreppardef p
	WHERE p.func_id = @FuncId 
		AND p.module = @Module 
		AND p.variant = @Variant
	UNION
	SELECT @Client, s.data_length, @OrderNo, s.param_id, t2.title,
		ISNULL(dbo.ufGetParamValue(s.param_id, @CurrPeriod, @ReportName, @BatchID), t1.title),
		@ReportName, s.sequence_no, dbo.ufGetTextType(s.param_id)
	FROM Agresso.dbo.asysreppardef s 
	INNER JOIN Agresso.dbo.asystitlesus t1 ON t1.title_no = s.text_no
	INNER JOIN Agresso.dbo.asystitlesus t2 ON t2.title_no = s.title_no
	LEFT JOIN Agresso.dbo.aagreppardef p ON p.param_id = s.param_id AND p.func_id = s.func_id AND p.module = s.Module AND p.variant = @Variant
	WHERE s.func_id = @FuncId 
		AND s.module = @Module
		AND p.param_id IS NULL

END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
