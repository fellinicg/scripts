USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbSettings](
	[Label] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Value] [varchar](1024) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [pkSettings] PRIMARY KEY NONCLUSTERED 
(
	[Label] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
INSERT INTO dbo.tbSettings (Label, Value)
VALUES ('FileCreationWorkingDir', '\\mpg-fs\dataoperations$\load_file_testing\Agresso\file_creation_system\working')
GO
INSERT INTO dbo.tbSettings (Label, Value)
VALUES ('FileCreationArchiveDir', '\\mpg-fs\dataoperations$\load_file_testing\Agresso\file_creation_system\archive')
GO
INSERT INTO dbo.tbSettings (Label, Value)
VALUES ('FileCreationOutputDir', '\\mpg-fs\dataoperations$\load_file_testing\Agresso\file_creation_system\output')
GO
INSERT INTO dbo.tbSettings (Label, Value)
VALUES ('LastOrderProcessed', '0')
GO
INSERT INTO dbo.tbSettings (Label, Value)
VALUES ('POSessionID', '0')
GO
INSERT INTO dbo.tbSettings (Label, Value)
VALUES ('ReqSessionID', '0')