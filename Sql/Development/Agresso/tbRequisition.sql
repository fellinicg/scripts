USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
USE [AgressoLink]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbRequisition]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbRequisition]
GO
CREATE TABLE dbo.tbRequisition(
	[linetype] [char] (2) NOT NULL DEFAULT ('01'),
	[client] [char] (25) NOT NULL DEFAULT ('MP'),
	[numbercycle] [char] (25) NOT NULL DEFAULT ('RQ'),
	[requisitionerid] [char] (15) NOT NULL DEFAULT ('99999'),
	[period] [char] (8) NOT NULL,
	[deliverydate] [char] (8) NOT NULL,
	[externalreference] [char] (100) NOT NULL DEFAULT ('Award Redemption request'),
	[supplierid] [char] (25) NOT NULL,
	[status] [char] (1) NOT NULL DEFAULT ('N'),
	[address1] [char] (40) NOT NULL DEFAULT (''),
	[address2] [char] (40) NOT NULL DEFAULT (''),
	[address3] [char] (40) NOT NULL DEFAULT (''),
	[address4] [char] (40) NOT NULL DEFAULT (''),
	[description] [char] (120) NOT NULL DEFAULT (''),
	[attention] [char] (120) NOT NULL DEFAULT (''),
	[linetype2] [char] (2) NOT NULL DEFAULT ('03'),
	[product] [char] (25) NOT NULL DEFAULT ('SPECIAL'),
	[description2] [char] (255) NOT NULL,
	[unit] [char] (3) NOT NULL DEFAULT ('EA'),
	[supplierid2] [char] (25) NOT NULL DEFAULT (''),
	[deliverydate2] [char] (8) NOT NULL DEFAULT (''),
	[number] [char] (10) NOT NULL,
	[currency] [char] (3) NOT NULL DEFAULT ('USD'),
	[price] [char] (10) NOT NULL,
	[account] [char] (25) NOT NULL DEFAULT ('5000'),
	[dim1] [char] (25) NOT NULL,
	[dim2] [char] (25) NOT NULL,
	[dim3] [char] (25) NOT NULL DEFAULT (''),
	[dim4] [char] (25) NOT NULL DEFAULT (''),
	[dim5] [char] (25) NOT NULL DEFAULT ('700'),
	[dim6] [char] (25) NOT NULL DEFAULT ('NYC'),
	[dim7] [char] (25) NOT NULL DEFAULT ('N'),
	[taxcode] [char] (25) NOT NULL DEFAULT ('0'),
	[taxsystem] [char] (25) NOT NULL DEFAULT ('0')
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF