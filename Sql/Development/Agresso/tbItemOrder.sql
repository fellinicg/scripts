USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbItemOrder]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbItemOrder]
GO
CREATE TABLE dbo.tbItemOrder(
	[ItemOrderID] [int] IDENTITY(1,1) NOT NULL,
	[ItemLineID] [varchar](25) NULL,
	[ItemType] [char](1) NULL,
	[VendorNo] [varchar](6) NULL,
	[DKNumber] [varchar](25) COLLATE Latin1_General_CI_AI NULL,
	[Description] [varchar](60) NULL,
	[Quantity] [int] NULL DEFAULT (0),
	[Cost] [money] NULL DEFAULT (0),
	[OrderDate] [varchar](10),
	[POLineMemo] [text] NULL,
	[PONumber] [varchar](10) NULL,
	[POLineNumber] [smallint] NULL DEFAULT (0),
	[PODate] [datetime] NULL,
	[SessionID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_tbItemOrder] PRIMARY KEY NONCLUSTERED 
(
	[ItemOrderID] ASC
) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF