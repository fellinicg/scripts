USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
USE [AgressoLink]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbReqHeader]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbReqHeader]
GO
CREATE TABLE dbo.tbReqHeader(
	[linetype] [char] (2) NOT NULL DEFAULT ('01'),
	[client] [char] (25) NOT NULL DEFAULT ('MP'),
	[numbercycle] [char] (25) NOT NULL DEFAULT ('Requisitions'),
	[requisitionerid] [char] (15) NOT NULL DEFAULT ('99999'),
	[period] [char] (8) NOT NULL,
	[deliverydate] [char] (8) NOT NULL,
	[externalreference] [char] (100) NOT NULL DEFAULT (''),
	[supplierid] [char] (25) NOT NULL,
	[status] [char] (1) NOT NULL DEFAULT ('N'),
	[address1] [char] (40) NOT NULL DEFAULT (''),
	[address2] [char] (40) NOT NULL DEFAULT (''),
	[address3] [char] (40) NOT NULL DEFAULT (''),
	[address4] [char] (40) NOT NULL DEFAULT (''),
	[description] [char] (120) NOT NULL DEFAULT (''),
	[attention] [char] (120) NOT NULL DEFAULT (''),
	[itemlineid] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF