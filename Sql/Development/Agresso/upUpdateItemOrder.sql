USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061207
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upUpdateItemOrder
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Update PONumber in tbItemOrder
	UPDATE dbo.tbItemOrder SET PONumber = order_id
	FROM dbo.tbItemOrder
	INNER JOIN SQL2KPROD2.Agresso.dbo.apodetail ON CONVERT(varchar, ItemOrderID) = sup_article AND POLineNumber = line_no
	WHERE PONumber IS NULL

END
GO