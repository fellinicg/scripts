USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbCS15_5]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbCS15_5]
GO
CREATE TABLE dbo.tbCS15_5(
	[full_record] [char] (1) NOT NULL DEFAULT ('1'),
	[change_status] [char] (1) NOT NULL DEFAULT ('I'),
	[apar_gr_id] [char] (25) NOT NULL DEFAULT ('S3'),
	[apar_id] [char] (25) NOT NULL,
	[apar_id_ref] [char] (25) NOT NULL DEFAULT (''),
	[apar_name] [char] (255) NOT NULL,
	[apar_type] [char] (1) NOT NULL DEFAULT ('P'),
	[bank_account] [char] (35) NOT NULL DEFAULT ('N/A'),
	[bonus_gr] [char] (4) NOT NULL DEFAULT (''),
	[cash_delay] [char] (3) NOT NULL DEFAULT (''),
	[clearing_code] [char] (13) NOT NULL DEFAULT (''),
	[client] [char] (25) NOT NULL DEFAULT ('MP'),
	[collect_flag] [char] (1) NOT NULL DEFAULT (''),
	[comp_reg_no] [char] (25) NOT NULL,
	[control] [char] (1) NOT NULL DEFAULT (''),
	[credit_limit] [char] (20) NOT NULL DEFAULT (''),
	[currency] [char] (25) NOT NULL DEFAULT ('USD'),
	[currency_set] [char] (1) NOT NULL DEFAULT (''),
	[disc_code] [char] (4) NOT NULL DEFAULT (''),
	[ext_apar_ref] [char] (100) NOT NULL,
	[factor_short] [char] (25) NOT NULL DEFAULT (''),
	[foreign_acc] [char] (35) NOT NULL DEFAULT (''),
	[intrule_id] [char] (25) NOT NULL DEFAULT ('0'),
	[invoice_code] [char] (25) NOT NULL DEFAULT (''),
	[language] [char] (2) NOT NULL DEFAULT ('US'),
	[main_apar_id] [char] (25) NOT NULL,
	[message_text] [char] (255) NOT NULL DEFAULT (''),
	[pay_delay] [char] (3) NOT NULL DEFAULT (''),
	[pay_method] [char] (2) NOT NULL DEFAULT ('CH'),
	[postal_acc] [char] (35) NOT NULL DEFAULT ('N/A'),
	[priority_no] [char] (1) NOT NULL DEFAULT (''),
	[short_name] [char] (10) NOT NULL,
	[status] [char] (1) NOT NULL DEFAULT ('N'),
	[swift] [char] (11) NOT NULL DEFAULT (''),
	[tax_set] [char] (1) NOT NULL DEFAULT ('0'),
	[tax_system] [char] (25) NOT NULL DEFAULT ('0'),
	[terms_id] [char] (25) NOT NULL DEFAULT ('0'),
	[terms_set] [char] (1) NOT NULL DEFAULT (''),
	[vat_reg_no] [char] (25) NOT NULL DEFAULT (''),
	[address] [char] (160) NOT NULL,
	[address_type] [char] (1) NOT NULL DEFAULT ('1'),
	[agr_user_id] [char] (25) NOT NULL DEFAULT ('CS15'),
	[cc_name] [char] (255) NOT NULL DEFAULT (''),
	[country_code] [char] (25) NOT NULL DEFAULT ('US'),
	[description] [char] (255) NOT NULL DEFAULT (''),
	[ean] [char] (50) NOT NULL DEFAULT (''),
	[place] [char] (40) NOT NULL,
	[province] [char] (40) NOT NULL,
	[telephone_1] [char] (35) NOT NULL DEFAULT (''),
	[telephone_2] [char] (35) NOT NULL DEFAULT (''),
	[telephone_3] [char] (35) NOT NULL DEFAULT (''),
	[telephone_4] [char] (35) NOT NULL DEFAULT (''),
	[telephone_5] [char] (35) NOT NULL DEFAULT (''),
	[telephone_6] [char] (35) NOT NULL DEFAULT (''),
	[telephone_7] [char] (35) NOT NULL DEFAULT (''),
	[to_name] [char] (255) NOT NULL DEFAULT (''),
	[zip_code] [char] (15) NOT NULL,
	[e_mail] [char] (255) NOT NULL DEFAULT (''),
	[pos_title] [char] (35) NOT NULL DEFAULT (''),
	[pay_temp_id] [char] (4) NOT NULL DEFAULT (''),
	[reference_1] [char] (255) NOT NULL DEFAULT (''),
	[old_rel_value] [char] (25) NOT NULL DEFAULT (''),
	[rel_attr_id] [char] (4) NOT NULL DEFAULT (''),
	[rel_value] [char] (25) NOT NULL DEFAULT (''),
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF