USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060815
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upAddFileCreationLog
	@FileType char(1),
	@ArchiveFileName varchar(80),
	@SessionID uniqueidentifier
AS

	SET NOCOUNT ON

	INSERT INTO dbo.tbFileCreationLog(FileType, ArchiveFileName, SessionID, CreateDateTime)
	VALUES (@FileType, @ArchiveFileName, @SessionID, CURRENT_TIMESTAMP)

	SET NOCOUNT OFF

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF