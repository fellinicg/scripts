UPDATE Agresso.dbo.agladdress SET telephone_3 = phone2, pos_title = ap.title, e_mail = ap.email
FROM Agresso.dbo.agladdress ag
INNER JOIN SQL2KPROD.MPGPRO02.dbo.apvend ap ON ag.dim_value = ap.vendno COLLATE Latin1_General_CI_AI
WHERE ag.client = 'MP'
AND ap.code = 'IP'