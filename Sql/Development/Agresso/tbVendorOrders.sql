USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbVendorOrders]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbVendorOrders]
GO
CREATE TABLE dbo.tbVendorOrders(
	[AgressoID] [int] IDENTITY(1,1) NOT NULL,
	[VendorID] [varchar](25) NOT NULL,
	[RecordDateTime] [datetime] NOT NULL DEFAULT (GETDATE()),
	[SessionID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_tbVendorOrders] PRIMARY KEY NONCLUSTERED 
(
	[AgressoID] ASC
) ON [PRIMARY]
)

GO
SET ANSI_PADDING OFF