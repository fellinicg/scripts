USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061017
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upPopulatetbGL07_55
AS
BEGIN

	DECLARE @Today char(8)
	DECLARE @BatchID char(25)

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Today = CONVERT(varchar, CURRENT_TIMESTAMP, 112)
	SET @BatchID = CONVERT(varchar, CURRENT_TIMESTAMP, 112) + REPLACE(CONVERT(varchar, CURRENT_TIMESTAMP, 114), ':', '')

	-- Truncate working table
	TRUNCATE TABLE dbo.tbGL07_55

	-- Insert data for GL line 
	INSERT INTO dbo.tbGL07_55 (batch_id, trans_type, account, dim_1, dim_2, cur_amount, amount, value_1, description, trans_date, voucher_date, period, ext_inv_ref, due_date, apar_id)
	SELECT
		@BatchID,
		'GL',
		'5000',
		pr.department,
		pg.p_DKno,
		REPLACE(CONVERT(varchar, i.Amount), '.', ''),
		REPLACE(CONVERT(varchar, i.Amount), '.', ''),
		REPLACE(CONVERT(varchar, i.Amount), '.', ''),
		i.Reference,
		@Today,
		@Today,
		LEFT(@Today, 6),
		i.ClaimNumber,
		@Today,
		a.apar_id
	FROM tbInvoices
	INNER JOIN OPENQUERY (MOONRAKER, 'SELECT ID, ClaimNumber, Amount, Reference, SSNumber FROM Ricoh.dbo.Invoices WHERE PostDt > ''11/1/2006''') i ON SessionID = dbo.ufSettingValue('ReqSessionID') AND InvoiceID = i.ID
	INNER JOIN SQL2KPROD.Catalog.dbo.Programs pg ON pg.p_ID = 13
	LEFT JOIN OPENQUERY (SQL2KPROD2, 'SELECT apar_id, comp_reg_no FROM Agresso.dbo.asuheader WHERE apar_gr_id = ''S3'' AND client = ''MP''') a ON a.comp_reg_no = dbo.ufCreateUserID(REPLACE(i.SSNumber, '-', ''))
	LEFT JOIN OPENQUERY (SQL2KPROD2, 'SELECT department, project FROM Agresso.dbo.atsproject WHERE client = ''MP''') pr ON pr.project = CONVERT(varchar, pg.p_DKNo)

	-- Insert data for AP line 
	INSERT INTO dbo.tbGL07_55 (batch_id, trans_type, account, dim_1, dim_2, cur_amount, amount, value_1, description, trans_date, voucher_date, period, ext_inv_ref, due_date, apar_type, apar_id, apar_name, address, province, place, zip_code)
	SELECT
		@BatchID,
		'AP',
		'2215',
		pr.department,
		pg.p_DKno,
		REPLACE(CONVERT(varchar, (i.Amount * -1)), '.', ''),
		REPLACE(CONVERT(varchar, (i.Amount * -1)), '.', ''),
		REPLACE(CONVERT(varchar, (i.Amount * -1)), '.', ''),
		i.Reference,
		@Today,
		@Today,
		LEFT(@Today, 6),
		i.ClaimNumber,
		@Today,
		'P',
		a.apar_id,
		i.Name,
		LEFT(RTRIM(i.Address1 + ' ' + ISNULL(i.Address2, '')), 40),
		i.State,
		i.City,
		i.Zip
	FROM tbInvoices
	INNER JOIN OPENQUERY (MOONRAKER, 'SELECT ID, ClaimNumber, Amount, Reference, Name, SSNumber, Address1, Address2, City, State, Phone, Zip FROM Ricoh.dbo.Invoices WHERE PostDt > ''11/1/2006''') i ON SessionID = dbo.ufSettingValue('ReqSessionID') AND InvoiceID = i.ID
	INNER JOIN SQL2KPROD.Catalog.dbo.Programs pg ON pg.p_ID = 13
	LEFT JOIN OPENQUERY (SQL2KPROD2, 'SELECT apar_id, comp_reg_no FROM Agresso.dbo.asuheader WHERE apar_gr_id = ''S3'' AND client = ''MP''') a ON a.comp_reg_no = dbo.ufCreateUserID(REPLACE(i.SSNumber, '-', ''))
	LEFT JOIN OPENQUERY (SQL2KPROD2, 'SELECT department, project FROM Agresso.dbo.atsproject WHERE client = ''MP''') pr ON pr.project = CONVERT(varchar, pg.p_DKNo)

END

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO