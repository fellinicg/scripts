USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION dbo.ufEncryption
 (@long_string     varchar(255))
RETURNS  varchar(255)
--with encryption
AS
BEGIN

	DECLARE @short_string varchar(36)
	DECLARE @out_string varchar(255)
	DECLARE
		@state smallint,
		@key char(1),
		@tempSwap binary(1),
		@a smallint,
		@b smallint,
		@N smallint,
 		@temp binary(1),
		@i smallint,
		@j smallint,
		@k smallint,
		@cipherby smallint,
		@cipher varchar(255),
		@code   varchar(64),

		@keys_key varchar(256),
		@keys_state varbinary(256)

		-- The RC4 algorithm works in two phases, key setup and ciphering.
		-- Key setup is the first and most difficult phase of this algorithm.
		-- During a N-bit key setup (N being your key length),
		--  the encryption key is used to generate an encrypting variable using two arrays,
		--  state and key, and N-number of mixing operations.
		--  These mixing operations consist of swapping bytes,
		--   modulo operations, and other formulas.
		--  A modulo operation is the process of yielding a remainder from division.
		-- This implementation of RC4 uses temporary tables in place of arrays.

		-- Encryption/Decryption Key
		SET @short_string = 'C869077F-9347-4C50-AC37-80D96DAA0AEE'
		-- Initialize variable values.
		SET @code = @short_string + ',./;''p[\`0-=Z<?L:"P{|~!@#%^*(_'

		-- Initialize temp table with key and state values.
		SET @keys_key = @code + @code + @code + @code + @code + @code + @code + @code + @code
		SET @keys_state = 0x000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F202122232425262728292A2B2C2D2E2F303132333435363738393A3B3C3D3E3F404142434445464748494A4B4C4D4E4F505152535455565758595A5B5C5D5E5F606162636465666768696A6B6C6D6E6F707172737475767778797A7B7C7D7E7F808182838485868788898A8B8C8D8E8F909192939495969798999A9B9C9D9E9FA0A1A2A3A4A5A6A7A8A9AAABACADAEAFB0B1B2B3B4B5B6B7B8B9BABBBCBDBEBFC0C1C2C3C4C5C6C7C8C9CACBCCCDCECFD0D1D2D3D4D5D6D7D8D9DADBDCDDDEDFE0E1E2E3E4E5E6E7E8E9EAEBECEDEEEFF0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF

		-- select @keys_state, @keys_key

		-- Initialize variable values.
		SELECT
			@b = 0,
			@a = 0
			-- The state array now undergoes 256 mixing operations.
		WHILE @a < 256
		BEGIN
			SELECT @b = (@b + convert(int, substring(@keys_state, @a + 1, 1)) +
						ascii(substring(@keys_key, @a + 1, 1))
		) % 256 
			SELECT @tempSwap = substring(@keys_state, @a + 1, 1)

			SET @keys_state = convert(varbinary(256), stuff(@keys_state, @a + 1, 1, substring(@keys_state, @b + 1, 1) )  )

			SET @keys_state = convert(varbinary(256), stuff(@keys_state, @b + 1, 1, @tempSwap) )

			SET @a = @a + 1
		END

	-- select @keys_state, @keys_key

		-- Initialize variable values.
		SELECT
		@i = 0,
		@j = 0,
		@a = 1,
		@cipher = '',
		@cipherby = 0

		-- Once the encrypting variable is produced from the key setup,
		--  it enters the ciphering phase,
		--  where it is XORed with the plain text message to create and encrypted message.
		--  XOR is the logical operation of comparing two binary bits.
		--  If the bits are different, the result is 1.
		--  If the bits are the same, the result is 0.
		--  The string is decrypted by XORing the encrypted message with the same encrypting key. 
		WHILE @a < datalength(@long_string) + 1
		BEGIN

			SET @j = (@j + convert(smallint, substring(@keys_state, @i + 1, 1))) % 256
			SET @temp = substring(@keys_state, @i + 1, 1)
			SET @i = (@i + 1) % 256

			SET @keys_state = convert(varbinary(256), stuff(@keys_state, @i + 1, 1, substring(@keys_state, @j + 1, 1)) ) 

			SET @keys_state = convert(varbinary(256), stuff(@keys_state, @i + 1, 1, @temp ) ) 

			SET @k = convert(smallint, 
						substring(@keys_state, 
							1 + ((convert(smallint,
	substring(@keys_state, @i + 1, 1)) +
										convert(smallint,
	substring(@keys_state, @j + 1, 1))  ) % 256),
							1)	)

			SET @cipherby = Ascii(Substring(@long_string, @a, 1)) ^ @k
			SET @cipher = @cipher + Char(@cipherby)
			SET @a = @a + 1
		END

		-- Set ouput variable.
		SET @out_string = @cipher

		RETURN @out_string
END
GO

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF