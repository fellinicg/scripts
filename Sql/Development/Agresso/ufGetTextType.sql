USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION dbo.ufGetTextType (@ParamID varchar(12))
RETURNS char(1)
AS
BEGIN

	DECLARE @TextType char(1)
  
	SET @TextType = CASE @ParamID
										WHEN 'agg_apar' THEN 'b'
										WHEN 'agg_trans' THEN 'b'
										WHEN 'asqlflag' THEN 'b'
										WHEN 'compute_tax' THEN 'b'
										WHEN 'err_acc' THEN 'A'
										WHEN 'file_type' THEN 'a'
										WHEN 'format' THEN 'A'
										WHEN 'init_ag16' THEN 'b'
										WHEN 'keep_orderid' THEN 'b'
										WHEN 'key_column  ' THEN 'a'
										WHEN 'max_errors' THEN 'n'
										WHEN 'max_inv_diff' THEN 'b'
										WHEN 'only_errors' THEN 'b'
										WHEN 'period' THEN 'n'
										WHEN 'query' THEN 'A'
										WHEN 'query_param1' THEN 'a'
										WHEN 'query_param2' THEN 'a'
										WHEN 'query_param3' THEN 'a'
										WHEN 'recalc_amt' THEN 'b'
										WHEN 'recalc_v2' THEN 'b'
										WHEN 'recalc_v3' THEN 'b'
										WHEN 'relation' THEN 'A'
										WHEN 'report_file' THEN 'A'
										WHEN 'report_file1' THEN 'A'
										WHEN 'report_file2' THEN 'A'
										WHEN 'start_tps' THEN 'b'
										WHEN 'trans_type' THEN 'a'
										WHEN 'trig_chk' THEN 'b'
										WHEN 'trig_stop' THEN 'b'
										WHEN 'variant' THEN 'a'
										ELSE null
									END

  RETURN(@TextType)

END
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

