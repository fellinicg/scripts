USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061011
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upPopulateItemOrder (
	@Result char(1) output
)
AS
BEGIN

	DECLARE @OrderID int
	DECLARE @SessionID uniqueidentifier
	DECLARE @RowCT int

	-- Initialize variables
	SET @Result = 'E'
	SET @OrderID = dbo.ufSettingValue('LastOrderProcessed')
	SET @SessionID = dbo.ufSettingValue('POSessionID')

	-- Insert data
	INSERT INTO dbo.tbItemOrder (ItemLineID, ItemType, DKNumber, VendorNo, Description, POLineMemo, Quantity, Cost, OrderDate, SessionID)
	SELECT 
		IL.ItemLineID,
		IL.ItemType,
		OH.ProjectNumber,
		il.VendorID,
		LEFT(CASE CHARINDEX('Full Amount', IL.ItemOptions)
					 WHEN 0 THEN IL.ItemTitle
					 ELSE SUBSTRING(IL.ItemTitle, 1, CHARINDEX('$', IL.ItemTitle)) + CONVERT(VARCHAR, CONVERT(INT, IL.CostPerUnit * AL.Quantity)) +
								SUBSTRING(IL.ItemTitle, CHARINDEX(' ', IL.ItemTitle, CHARINDEX('$', IL.ItemTitle)), 1000)
				 END, 60) AS 'Description',
		(CASE IL.ItemModel
		 WHEN '' THEN ''
		 ELSE CASE CHARINDEX('Full Amount', IL.ItemOptions)
					 WHEN 0 THEN 'MODEL: ' + IL.ItemModel
					 ELSE 'MODEL: $' + CONVERT(VARCHAR, CONVERT(INT, IL.CostPerUnit * AL.Quantity)) +
								SUBSTRING(IL.ItemModel, CHARINDEX(' ', IL.ItemModel, CHARINDEX('$', IL.ItemModel)), 1000)
					END  + CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10)
		END) + 
		(CASE IL.ItemOptions
		 WHEN '' THEN ''
		 ELSE IL.ItemOptions + CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10)
		END) +
		'SHIP TO:' + CHAR(13) + CHAR(10) + '   ' + OH.ShipName + CHAR(13) + CHAR(10) + '   ' + OH.ShipAddress1 + CHAR(13) + CHAR(10) +
		(CASE ISNULL(OH.ShipAddress2, '')
		 WHEN '' THEN ''
		 ELSE '   ' + OH.ShipAddress2 + CHAR(13) + CHAR(10)
		END) +
		'   ' + OH.ShipCity + ' ' + OH.ShipState + '  ' + OH.ShipPostal + CHAR(13) + CHAR(10) + ISNULL(OH.ShipCountry, '')  +  
		ISNULL(OH.InternationalAddress, '') +  '   PHONE: ' + ISNULL(OH.ShipPhone, '') + CASE ISNULL(PIT.pit_AddlPOInfo ,'') WHEN '' THEN '' ELSE CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10) + PIT.pit_AddlPOInfo END
		AS 'LineMemo',
		CASE CHARINDEX('Full Amount', IL.ItemOptions)
		 WHEN 0 THEN AL.Quantity
		 ELSE 1
		END AS 'Quantity',
		CONVERT(DECIMAL(20,2), 
		 CASE CHARINDEX('Full Amount', IL.ItemOptions)
			WHEN 0 THEN IL.CostPerUnit
			ELSE IL.CostPerUnit * AL.Quantity
		 END) AS 'Amount',
		CONVERT(varchar, oh.OrderDate, 1),
		@SessionID
	FROM Curly.FulFilment.dbo.AwardLine AL
	INNER JOIN Curly.FulFilment.dbo.ItemLine IL ON AL.AwardLineID = IL.AwardLineID
	INNER JOIN Curly.FulFilment.dbo.OrderHeader OH ON AL.OrderID = OH.OrderID
	INNER JOIN Agresso.dbo.asuheader ah ON ah.client = 'MP' AND ah.apar_gr_id = 'S6' AND il.VendorID = ah.apar_id
	LEFT JOIN OPENQUERY(SQL2KPROD, 'SELECT p_ID, p_Code FROM Catalog.dbo.Programs') P ON OH.ProgramCode = P.p_Code
	LEFT JOIN OPENQUERY(SQL2KPROD, 'SELECT pit_programID, pit_ItemType, pit_AddlPOInfo FROM Catalog.dbo.ProgramItemTypes') PIT ON P.p_ID = PIT.pit_programID AND IL.ItemType = PIT.pit_ItemType
	WHERE OH.OrderID <= @OrderID
	AND OH.Orderdate > '12/31/2006'
	AND AL.Void = 0
	AND AL.Processed = 0

	-- Store row count
	SET @RowCT = @@ROWCOUNT

	-- Check results
	IF (@@ERROR = 0)
		IF (@RowCT = 0)
				SET @Result = 'N'
		ELSE
			SET @Result = 'S'

END

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

