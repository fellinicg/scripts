USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061011
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upPopulatePOHeader
AS
BEGIN

	DECLARE @OrderID int
	DECLARE @SessionID uniqueidentifier

	-- Initialize variables
	SET @OrderID = dbo.ufSettingValue('LastOrderProcessed')
	SET @SessionID = dbo.ufSettingValue('SessionID')

	-- Insert data
	INSERT INTO dbo.tbPOHeader (ItemLineID, VendorNo, SessionID)
	SELECT il.ItemLineID, il.VendorID, @SessionID
	FROM Curly.FulFilment.dbo.OrderHeader oh
	INNER JOIN Curly.FulFilment.dbo.AwardLine al ON oh.OrderID = al.OrderID
	INNER JOIN Curly.FulFilment.dbo.ItemLine il ON al.AwardLineID = il.AwardLineID
	WHERE oh.OrderID <= @OrderID
	AND al.Processed = 0
	AND al.Void = 0

END

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

