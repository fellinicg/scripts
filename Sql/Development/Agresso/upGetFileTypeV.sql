USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061013
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upGetFileTypeV 
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Retrieve data from working table
	SELECT 
		full_record, 
		change_status, 
		apar_gr_id, 
		apar_id, 
		apar_id_ref, 
		apar_name, 
		apar_type, 
		bank_account, 
		bonus_gr, 
		cash_delay, 
		clearing_code, 
		client, 
		collect_flag, 
		comp_reg_no, 
		control, 
		credit_limit, 
		currency, 
		currency_set, 
		disc_code, 
		ext_apar_ref, 
		factor_short, 
		foreign_acc, 
		intrule_id, 
		invoice_code, 
		language, 
		main_apar_id, 
		message_text, 
		pay_delay, 
		pay_method, 
		postal_acc, 
		priority_no, 
		short_name, 
		status, 
		swift, 
		tax_set, 
		tax_system, 
		terms_id, 
		terms_set, 
		vat_reg_no, 
		address, 
		address_type, 
		agr_user_id, 
		cc_name, 
		country_code, 
		description, 
		ean, 
		place, 
		province, 
		telephone_1, 
		telephone_2, 
		telephone_3, 
		telephone_4, 
		telephone_5, 
		telephone_6, 
		telephone_7, 
		to_name, 
		zip_code, 
		e_mail, 
		pos_title, 
		pay_temp_id, 
		reference_1, 
		old_rel_value, 
		rel_attr_id, 
		rel_value
	FROM dbo.tbCS15_5

END

SET ANSI_NULLS OFF
GO
GO
SET QUOTED_IDENTIFIER OFF
GO