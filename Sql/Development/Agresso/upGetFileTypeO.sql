USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061005
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upGetFileTypeO (
	@Result char(1) output
)
AS
BEGIN

	DECLARE @RowCT int

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'E'

	-- Header Info
	INSERT INTO SQL2KPROD2.Agresso.dbo.algbatchinput(
		accountable,
		amount_set,
		apar_id,
		apar_id_ref,
		att_7_id,
		batch_id,
		client,
		control,
		currency,
		deliv_method,
		deliv_terms,
		ext_ord_ref,
		intrule_id,
		line_no,
		order_id,
		order_type,
		responsible,
		responsible2,
		status,
		tax_code,
		tax_system,
		trans_type,
		voucher_type)
	SELECT DISTINCT
		'JOANL',
		1,
		ISNULL(vo.VendorID, ''),
		'AP',
		'BA',
		'4444',
		'MP',
		'N',
		'USD',
		'DROPSHIP',
		'VNOTICE',
		'Award Redemption Request',
		'0',
		'0',
		vo.AgressoID, 
		'DS',
		'LSILVERSTEIN',
		'LSILVERSTEIN',
		'N',
		'0',
		'0',
		'41',
		'13'
	FROM dbo.tbItemOrder io
	INNER JOIN dbo.tbVendorOrders vo ON io.VendorNo = vo.VendorID AND io.SessionID = vo.SessionID
	WHERE io.SessionID = dbo.ufSettingValue('POSessionID')
--	AND io.ItemType = 'M'

	-- Store row count
	SET @RowCT = @@ROWCOUNT

	-- Check results
	IF (@@ERROR > 0)
		RETURN
	IF (@RowCT = 0)
		BEGIN
			SET @Result = 'N'
			RETURN
		END

	-- Detail Info
	INSERT INTO SQL2KPROD2.Agresso.dbo.algbatchinput(
		accountable,
		amount,
		amount_set,
		apar_id_ref,
		art_descr,
		article,
		att_1_id,
		att_2_id,
		att_5_id,
		att_7_id,
		batch_id,
		client,
		control,
		currency,
		deliv_method,
		deliv_terms,
		dim_1,
		dim_2,
		dim_5,
		dim_value_1,
		dim_value_2,
		dim_value_5,
		ext_ord_ref,
		intrule_id,
		line_no,
		order_id,
		order_type,
		status,
		sup_article,
		tax_code,
		tax_system,
		trans_type,
		unit_code,
		value_1,
		voucher_type
	)
	SELECT 
		'JOANL',
		io.Cost,
		1,
		'AP',
		io.Description,
		'SPECIAL',
		'C1', 
		'B0', 
		'B1', 
		'BA',
		'4444',
		'MP',
		'N',
		'USD',
		'DROPSHIP',
		'VNOTICE',
		ISNULL(pr.department, 0),
		io.DKNumber,
		'700',
		ISNULL(pr.department, 0),
		io.DKNumber,
		'700',
		'Award Redemption Request',
		'0',
		io.POLineNumber,
		vo.AgressoID,
		'DS',
		'N',
		io.ItemOrderID,
		'0',
		'0',
		'41',
		'EA', 
		io.Quantity,
		'13'
	FROM dbo.tbItemOrder io
	INNER JOIN dbo.tbVendorOrders vo ON io.VendorNo = vo.VendorID AND io.SessionID = vo.SessionID
	LEFT JOIN SQL2KPROD2.Agresso.dbo.atsproject pr ON pr.client = 'MP' AND pr.project = io.DKNumber
	WHERE io.SessionID = dbo.ufSettingValue('POSessionID')
--	AND io.ItemType = 'M'

	-- Store row count
	SET @RowCT = @@ROWCOUNT

	-- Check results
	IF (@@ERROR = 0)
		IF (@RowCT = 0)
			SET @Result = 'N'
		ELSE
			SET @Result = 'S'

END
GO