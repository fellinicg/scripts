USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION dbo.ufGetParamValue 
(
	@ParamID varchar(12), 
	@Period varchar(20), 
	@ReportName varchar(20),
	@BatchID varchar(25)
)
RETURNS varchar(255)
AS
BEGIN

	DECLARE @ParamVal varchar(255)
  
	SET @ParamVal = CASE @ParamID
										WHEN 'agg_apar' THEN '0'
										WHEN 'agg_trans' THEN '0'
										WHEN 'batch_id' THEN @BatchID
										WHEN 'compute_tax' THEN '0'
										WHEN 'init_ag16' THEN '0'
										WHEN 'max_inv_diff' THEN '0'
										WHEN 'only_errors' THEN '0'
										WHEN 'period' THEN @Period
										WHEN 'recalc_amt' THEN '0'
										WHEN 'recalc_v2' THEN '0'
										WHEN 'recalc_v3' THEN '0'
										WHEN 'report_file' THEN @ReportName
										WHEN 'report_file1' THEN @ReportName
										WHEN 'trig_chk' THEN '0'
										WHEN 'trig_stop' THEN '0'
										ELSE null
									END

  RETURN(@ParamVal)

END
