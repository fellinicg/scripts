USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbFileCreation](
	[FileType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OutputFileName] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DBProcName] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [pkFileCreation] PRIMARY KEY NONCLUSTERED 
(
	[FileType] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
INSERT INTO dbo.tbFileCreation (FileType, Description, OutputFileName, DBProcName)
VALUES ('P','Product Load','PRODUCTS.txt','AgressoLink.dbo.upGetFileTypeP')
GO
INSERT INTO dbo.tbFileCreation (FileType, Description, OutputFileName, DBProcName)
VALUES ('O','Order Load','specialPO.dat','AgressoLink.dbo.upGetFileTypeO')
GO
INSERT INTO dbo.tbFileCreation (FileType, Description, OutputFileName, DBProcName)
VALUES ('V','SPIF Vendor Load','NEWSPIFVENDORS.txt','AgressoLink.dbo.upGetFileTypeV')

