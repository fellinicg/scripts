USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061017
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upPopulatetbCS15_5
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Insert IDs of checks to process into local table
	INSERT INTO tbInvoices (InvoiceID, SessionID)
	SELECT ID, dbo.ufSettingValue('ReqSessionID')
	FROM  tbInvoices
	RIGHT JOIN MOONRAKER.Ricoh.dbo.Invoices i ON InvoiceID = i.ID AND i.PostDt > '11/1/2006'
	WHERE i.UpdatedSBT = 0
	AND i.CheckNumber IS NULL
	AND i.SSNumber IS NOT NULL
	AND i.SBTVendor IS NULL
	AND InvoiceID IS NULL

	-- Truncate working table
	TRUNCATE TABLE dbo.tbCS15_5

	-- Insert data
	INSERT INTO dbo.tbCS15_5 (apar_id, apar_name, comp_reg_no, ext_apar_ref, main_apar_id, short_name, address, place, province, telephone_1, zip_code)
	SELECT DISTINCT
		u.UserID,
		i.Name,
		dbo.ufCreateUserID(REPLACE(i.SSNumber, '-', '')),
		u.UserID,
		u.UserID,
		u.UserID,
		LEFT(RTRIM(i.Address1 + ' ' + ISNULL(i.Address2, '')), 40),
		i.City,
		i.State,
		ISNULL(i.Phone, ''),
		i.Zip
	FROM tbInvoices
	INNER JOIN OPENQUERY (MOONRAKER, 'SELECT ID, EncID, Name, SSNumber, Address1, Address2, City, State, Phone, Zip FROM Ricoh.dbo.Invoices WHERE PostDt > ''11/1/2006''') i ON SessionID = dbo.ufSettingValue('ReqSessionID') AND InvoiceID = i.ID
	INNER JOIN OPENQUERY (MOONRAKER, 'SELECT SSN, UserID FROM Ricoh.dbo.Users WHERE Terminated = 0 AND Disabled = 0') u ON i.EncID = u.SSN
	LEFT JOIN OPENQUERY (SQL2KPROD2, 'SELECT comp_reg_no FROM Agresso.dbo.asuheader WHERE apar_gr_id = ''S3'' AND client = ''MP''') a ON a.comp_reg_no = dbo.ufCreateUserID(REPLACE(i.SSNumber, '-', ''))
	WHERE a.comp_reg_no IS NULL

END

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO