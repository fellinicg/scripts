USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061019
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upCreateGCFile
AS
BEGIN
	DECLARE @Result int
	DECLARE @FileType char(1)
	DECLARE @DBProcName varchar(40)
	DECLARE @OutputFileName varchar(40)

	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 1

	-- Assign necessary info to local variables
	SELECT 
		@FileType = FileType,
		@OutputFileName = OutputFileName,
		@DBProcName = DBProcName
	FROM dbo.tbFileCreation
	WHERE FileType IN ('G')

	-- Vendor Orders
	EXECUTE dbo.upPopulateVendorOrders

	-- Item Orders
	EXECUTE dbo.upPopulateItemOrder

	-- Assign Line Numbers
	EXECUTE dbo.upAssignLineNumbers

	-- Create file
	EXECUTE @Result = dbo.upCreateAgressoFile
		@FileType = @FileType,
		@DBProcName = @DBProcName,
		@OutputFileName = @OutputFileName

	-- Log Process
	INSERT INTO dbo.tbOrderProcessLog (ProcessDateTime, FileType, SessionID)
	VALUES (CURRENT_TIMESTAMP, @FileType, dbo.ufSettingValue('SessionID'))

	-- Return result code
	RETURN @Result

END

GO