USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061011
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upSetSessionValues (@Type char(1))
AS
BEGIN

	-- Upcase var
	SET @Type = UPPER(@Type)

	-- POs
	IF (@Type = 'P')
		BEGIN
			DECLARE @MaxOrderID int

			-- Retrieve the last unprocessed orderid
			SELECT @MaxOrderID =	MAX(OrderID)
			FROM Curly.FulFilment.dbo.AwardLine
			WHERE Void = 0
			AND Processed = 0

			-- Update LastOrderProcessed setting
			UPDATE dbo.tbSettings SET Value = @MaxOrderID
			WHERE Label = 'LastOrderProcessed'

			-- Update POSessionID setting
			UPDATE dbo.tbSettings SET Value = NEWID()
			WHERE Label = 'POSessionID'

			-- Prevent further execution
			RETURN
		END

	-- Check Requests
	IF (@Type = 'R')
		BEGIN
			-- Update ReqSessionID setting
			UPDATE dbo.tbSettings SET Value = NEWID()
			WHERE Label = 'ReqSessionID'

			-- Prevent further execution
			RETURN
		END

END

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

