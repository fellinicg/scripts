USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061011
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20070315
-- Notes:	Status list for the AgressoStatus field in MOONRAKER.Ricoh.dbo.Invoices
--					U = Unprocessed
--					X = Invoice Voided
--					D = Do Not Process
--					V = Vendor Creation
--					P = Processing Check Request
--					R = Request Processed
--					C = Check Cut (Check Number Populated)
--					F = Failure
-- ============================================================
ALTER PROCEDURE dbo.upCreateSPIFFRequests 
AS
BEGIN


	DECLARE @StartTime smalldatetime
	DECLARE @RowCT int
	DECLARE @OrderNo int
	DECLARE @Today char(8)
	DECLARE @BatchID char(25)

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Today = CONVERT(varchar, CURRENT_TIMESTAMP, 112)
	SET @BatchID = @Today + REPLACE(CONVERT(varchar, CURRENT_TIMESTAMP, 108), ':', '')

	-- Set all unprocessed invoices that are voided to nvoice Voided status (X)
	UPDATE MOONRAKER.Ricoh.dbo.Invoices	SET AgressoStatus = 'X' WHERE AgressoStatus = 'U' AND Void = 1

	-- Set all unprocessed invoices for RBS Team to Do Not Process status (D)
	UPDATE MOONRAKER.Ricoh.dbo.Invoices	SET AgressoStatus = 'D' WHERE AgressoStatus = 'U' AND Name LIKE '%RBS TEAM%'

	-- Set all unprocessed invoices to Vendor Creation status (V)
	UPDATE MOONRAKER.Ricoh.dbo.Invoices	SET AgressoStatus = 'V' WHERE AgressoStatus = 'U'

	-- Insert vendors which need to be created into Agresso DB
	INSERT INTO Agresso.dbo.acsheaderinput (address, address_type, apar_gr_id, apar_id, 
		apar_name, apar_type, bank_account, change_status, client, country_code, comp_reg_no, 
		currency, ext_apar_ref, full_record, intrule_id, language, main_apar_id, pay_method, place, 
		postal_acc, province, short_name, status, tax_set, tax_system, telephone_1, terms_id,	user_id, zip_code)
	SELECT DISTINCT CONVERT(char(40), i.Address1) + ISNULL(i.address2, ''), '1', 'S3', u.UserID,
		i.Name, 'P', 'N/A', 'I', 'MP', 'US', dbo.ufCreateUserID(REPLACE(i.SSNumber, '-', '')),
		'USD', u.UserID, 1, '0', 'US', u.UserID, 'CH', i.City,
		'N/A', i.State, u.UserID, 'N', '0', '0', ISNULL(i.Phone, ''), '0', 'SYSTEM', i.Zip
	FROM OPENQUERY (MOONRAKER, 'SELECT ID, EncID, Name, SSNumber, Address1, Address2, City, State, Phone, Zip FROM Ricoh.dbo.Invoices WHERE AgressoStatus = ''V''') i
	INNER JOIN OPENQUERY (MOONRAKER, 'SELECT SSN, UserID FROM Ricoh.dbo.Users WHERE Terminated = 0 AND Disabled = 0') u ON i.EncID = u.SSN
	LEFT JOIN Agresso.dbo.asuheader a ON apar_gr_id = 'S3' AND client = 'MP' AND a.comp_reg_no = dbo.ufCreateUserID(REPLACE(i.SSNumber, '-', ''))
	WHERE a.comp_reg_no IS NULL

	-- Check results
	IF (@@ERROR > 0)
		BEGIN
			RAISERROR('Unable to populate Agresso.dbo.acsheaderinput with vendor information.', 16, 1)
			RETURN 1
		END

	-- Submit Agresso job to create vendors
	EXECUTE dbo.upSubmitJob 'CS15', 192, 'BI', 'MP', 'SYSTEM', 1, @BatchID, @OrderNo OUTPUT

	-- Monitor Agresso job.  Status gets set to 'T' when it is complete
	SET @StartTime = CURRENT_TIMESTAMP
	WHILE NOT EXISTS (SELECT 1 FROM Agresso.dbo.acrrepord WHERE report_name = 'CS15' 
										AND func_id = 192 AND orderno = @OrderNo AND Status = 'T')
		BEGIN
			-- Only poll for 10 minutes
			IF (DATEDIFF(mi, @StartTime, CURRENT_TIMESTAMP) = 10)
				BEGIN
					RAISERROR('Agresso vendor creation job has not completed in alotted time.', 16, 1)
					RETURN 1
				END
			-- Delay loop for 1 minute
			WAITFOR DELAY '0:01'
		END

	-- If there are	records left in acsheaderinput in the Agresso DB then the job failed
	IF EXISTS (SELECT 1 FROM Agresso.dbo.acsheaderinput)
		BEGIN
			RAISERROR('Agresso vendor creation job failed.', 16, 1)
			RETURN 1
		END

	-- Set all Vendor Creation invoices to Processing Check Request status (P)
	UPDATE MOONRAKER.Ricoh.dbo.Invoices	SET AgressoStatus = 'P' WHERE AgressoStatus = 'V'

	-- Insert data for GL line 
	INSERT INTO Agresso.dbo.acrbatchinput (account, amount, apar_id, batch_id, client, cur_amount, 
		currency, description, dim_1, dim_2, dim_4, dim_5, due_date, ext_inv_ref, interface, period, 
		status, tax_code, trans_date, trans_type, value_1, voucher_date, voucher_no, voucher_type)
	SELECT '5000', i.Amount, a.apar_id, @BatchID, 'MP', i.Amount,
		'USD', i.Reference, pr.department, pg.p_DKno, i.ID, '625', @Today, i.ClaimNumber, 'BI', LEFT(@Today, 6),
		'N', '0', @Today, 'GL', i.Amount, @Today, i.ID, 'AP'
	FROM OPENQUERY (SQL2KPROD, 'SELECT p_DKno FROM Catalog.dbo.Programs WHERE p_ID = 13') pg
	INNER JOIN OPENQUERY (MOONRAKER, 'SELECT ID, ClaimNumber, Amount, Reference, SSNumber FROM Ricoh.dbo.Invoices WHERE AgressoStatus = ''P''') i ON 1=1
	INNER JOIN Agresso.dbo.asuheader a ON a.apar_gr_id = 'S3' AND a.client = 'MP' AND a.comp_reg_no = dbo.ufCreateUserID(REPLACE(i.SSNumber, '-', ''))
	INNER JOIN Agresso.dbo.atsproject pr ON pr.client = 'MP' AND pr.project = CONVERT(varchar, pg.p_DKNo)

	-- Check results
	IF (@@ERROR > 0)
		BEGIN
			RAISERROR('Unable to populate Agresso.dbo.acrbatchinput with GL information.', 16, 1)
			RETURN 1
		END

	-- Insert data for AP line 
	INSERT INTO Agresso.dbo.acrbatchinput (account, address, amount, apar_id, apar_name, 
		apar_type, batch_id, client, cur_amount, currency, description, dim_1, dim_2, dim_4, dim_5, 
		due_date, ext_inv_ref, interface, period, place, province, sequence_no, status, tax_code, trans_date, trans_type, 
		value_1, voucher_date, voucher_no, voucher_type, zip_code)
	SELECT '2217', LEFT(RTRIM(i.Address1 + ' ' + ISNULL(i.Address2, '')), 40), i.Amount * -1, a.apar_id, i.Name,
		'P', @BatchID, 'MP', i.Amount * -1, 'USD', i.Reference, pr.department, pg.p_DKno, i.ID, '625',
		@Today, i.ClaimNumber, 'BI', LEFT(@Today, 6), i.City, i.State, 1, 'N', '0', @Today, 'AP',
		i.Amount * -1, @Today, i.ID, 'AP', i.Zip
	FROM OPENQUERY (SQL2KPROD, 'SELECT p_DKno FROM Catalog.dbo.Programs WHERE p_ID = 13') pg
	INNER JOIN OPENQUERY (MOONRAKER, 'SELECT ID, ClaimNumber, Amount, Reference, Name, SSNumber, Address1, Address2, City, State, Phone, Zip FROM Ricoh.dbo.Invoices WHERE AgressoStatus = ''P''') i ON 1=1
	INNER JOIN Agresso.dbo.asuheader a ON a.apar_gr_id = 'S3' AND a.client = 'MP' AND a.comp_reg_no = dbo.ufCreateUserID(REPLACE(i.SSNumber, '-', ''))
	INNER JOIN Agresso.dbo.atsproject pr ON pr.client = 'MP' AND pr.project = CONVERT(varchar, pg.p_DKNo)

	-- Check results
	IF (@@ERROR > 0)
		BEGIN
			RAISERROR('Unable to populate Agresso.dbo.acrbatchinput with AP information.', 16, 1)
			RETURN 1
		END

	-- Submit Agresso job to create check requests
	EXECUTE dbo.upSubmitJob 'GL07', 88, 'BI', 'MP', 'SYSTEM', 1, @BatchID, @OrderNo OUTPUT

	-- Monitor Agresso job.  Status gets set to 'T' when it is complete
	SET @StartTime = CURRENT_TIMESTAMP
	WHILE NOT EXISTS (SELECT 1 FROM Agresso.dbo.acrrepord WHERE report_name = 'GL07' 
										AND func_id = 88 AND orderno = @OrderNo AND Status = 'T')
		BEGIN
			-- Only poll for 10 minutes
			IF (DATEDIFF(mi, @StartTime, CURRENT_TIMESTAMP) = 10)
				BEGIN
					RAISERROR('Agresso check request job has not completed in alotted time.', 16, 1)
					RETURN 1
				END
			-- Delay loop for 1 minute
			WAITFOR DELAY '0:01'
		END

	-- If there are	records left in acrbatchinput in the Agresso DB then the job failed
	IF EXISTS (SELECT 1 FROM Agresso.dbo.acrbatchinput WHERE batch_id = @BatchID)
		BEGIN
			RAISERROR('Agresso check request job failed.', 16, 1)
			RETURN 1
		END

	-- Set all Processing Check Request invoices to Request Processed status (R)
	UPDATE MOONRAKER.Ricoh.dbo.Invoices SET AgressoStatus = 'R' WHERE AgressoStatus = 'P'

	-- Success
	PRINT('Check requests successfully entered into Agresso.')
	RETURN 0

END

