USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061019
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upGetFileTypeG
AS
BEGIN

	DECLARE @Today char(8)

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Today = CONVERT(varchar, CURRENT_TIMESTAMP, 112)

	-- Truncate working table
	TRUNCATE TABLE dbo.tbRequisition

	-- Put data into working table.  Makes file creation much easier cause of widths.
	INSERT INTO dbo.tbRequisition(
		period,
		deliverydate,
		supplierid,
		description2,
		number,
		price,
		dim1,
		dim2
		)
	SELECT 
		LEFT(@Today, 6),
		@Today,
		io.VendorNo,
		io.Description,
		CONVERT(varchar, io.Quantity * 100),
		REPLACE(CONVERT(varchar, io.Cost), '.', ''),
		ISNULL(pr.department, 0),
		io.DKNumber
	FROM dbo.tbItemOrder io
	LEFT JOIN SQL2KPROD2.Agresso.dbo.atsproject pr ON pr.client = 'MP' AND pr.project = CONVERT(varchar, io.DKNumber)
	WHERE io.SessionID = dbo.ufSettingValue('POSessionID')
	AND io.ItemType = 'G'

	-- Retrieve data from working tables
	SELECT 
		linetype, 
		client, 
		numbercycle, 
		requisitionerid, 
		period, 
		deliverydate, 
		externalreference, 
		supplierid, 
		status, 
		address1, 
		address2, 
		address3, 
		address4, 
		description, 
		attention, 
		CHAR(13),
		linetype2, 
		product, 
		description2, 
		unit, 
		supplierid2, 
		deliverydate2, 
		number, 
		currency, 
		price, 
		account, 
		dim1, 
		dim2, 
		dim3, 
		dim4, 
		dim5, 
		dim6, 
		dim7, 
		taxcode, 
		taxsystem 
	FROM tbRequisition

END
GO