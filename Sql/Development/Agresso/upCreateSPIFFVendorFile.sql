USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061011
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20070207
-- ============================================================
ALTER PROCEDURE dbo.upCreateSPIFFVendorFile 
(
	@Result char(1) OUTPUT
)
AS
BEGIN

	DECLARE @StartTime smalldatetime
	DECLARE @RowCT int
	DECLARE @OrderNo int

	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'E'

	-- Set all unprocessed invoices to vendor creation status (V)
	UPDATE MOONRAKER.Ricoh.dbo.Invoices	SET AgressoStatus = 'V' WHERE AgressoStatus = 'U'

	-- Insert vendors which need to be created into Agresso DB
	INSERT INTO Agresso.dbo.acsheaderinput (address, address_type, apar_gr_id, apar_id, apar_name, 
		apar_type, bank_account, change_status, client, country_code, comp_reg_no, currency, ext_apar_ref,
		full_record, intrule_id, language, main_apar_id, pay_method, place, postal_acc, province, short_name, 
		status, tax_set, tax_system, telephone_1, terms_id, user_id, zip_code)
	SELECT DISTINCT LEFT(RTRIM(i.Address1 + ' ' + ISNULL(i.Address2, '')), 40), '1', 'S3', u.UserID, i.Name,
		'P', 'N/A', 'I', 'MP', 'US', dbo.ufCreateUserID(REPLACE(i.SSNumber, '-', '')), 'USD', u.UserID,
		1, '0', 'US', u.UserID, 'CH', i.City, 'N/A', i.State, u.UserID,
		'N', '0', '0', ISNULL(i.Phone, ''), '0', 'SYSTEM', i.Zip
	FROM OPENQUERY (MOONRAKER, 'SELECT ID, EncID, Name, SSNumber, Address1, Address2, City, State, Phone, Zip FROM Ricoh.dbo.Invoices WHERE AgressoStatus = ''V''') i
	INNER JOIN OPENQUERY (MOONRAKER, 'SELECT SSN, UserID FROM Ricoh.dbo.Users WHERE Terminated = 0 AND Disabled = 0 AND FirstName <> ''RBS Team''') u ON i.EncID = u.SSN
	LEFT JOIN Agresso.dbo.asuheader a ON apar_gr_id = 'S3' AND client = 'MP' AND a.comp_reg_no = dbo.ufCreateUserID(REPLACE(i.SSNumber, '-', ''))
	WHERE a.comp_reg_no IS NULL

	-- Check results
	IF (@@ERROR > 0)
		RETURN 1

	-- Submit Agresso job to process PO
	EXECUTE dbo.upSubmitJob 'CS15', 192, 'BI', 'MP', 'SYSTEM', 1, @OrderNo OUTPUT

	-- Monitor Agresso job.  Status gets set to 'T' when it is complete
	SET @StartTime = CURRENT_TIMESTAMP
	WHILE NOT EXISTS (SELECT 1 FROM Agresso.dbo.acrrepord WHERE report_name = 'CS15' 
										AND func_id = 192 AND orderno = @OrderNo AND Status = 'T')
		BEGIN
			-- Only poll for 10 minutes
			IF (DATEDIFF(mi, @StartTime, CURRENT_TIMESTAMP) = 10)
				BEGIN
					RETURN 1
				END
			-- Delay loop for 1 minute
			WAITFOR DELAY '0:01'
		END

	-- If there are	records left in acsheaderinput in the Agresso DB then the job failed
	IF EXISTS (SELECT 1 FROM Agresso.dbo.acsheaderinput)
		BEGIN
			RETURN 1
		END

	-- Set agresso status to processing (P)
	UPDATE MOONRAKER.Ricoh.dbo.Invoices SET AgressoStatus = 'P'
	WHERE [ID] IN (SELECT ID
									FROM OPENQUERY (MOONRAKER, 'SELECT ID, EncID, Name, SSNumber, Address1, Address2, City, State, Phone, Zip FROM Ricoh.dbo.Invoices WHERE AgressoStatus = ''V''') i
									INNER JOIN OPENQUERY (MOONRAKER, 'SELECT SSN, UserID FROM Ricoh.dbo.Users WHERE Terminated = 0 AND Disabled = 0 AND FirstName <> ''RBS Team''') u ON i.EncID = u.SSN
									INNER JOIN Agresso.dbo.asuheader a ON apar_gr_id = 'S3' AND client = 'MP' AND a.comp_reg_no = dbo.ufCreateUserID(REPLACE(i.SSNumber, '-', '')))

	-- Set agresso status to failure (F) for any orphans.  This is usually because of a 
	-- user being disabled or terminated
	UPDATE MOONRAKER.Ricoh.dbo.Invoices SET AgressoStatus = 'F' WHERE AgressoStatus = 'V'

	-- Insert check requests into Agresso DB

	-- Success
	SET @Result = 'S'

	RETURN 0

END

GO