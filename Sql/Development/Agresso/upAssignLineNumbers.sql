USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061011
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upAssignLineNumbers(
	@Result char(1) output
)
AS
BEGIN

	DECLARE @Counter int
	DECLARE @LineNum int

	-- Initialize settings
	SET NOCOUNT ON

	
	-- Initialize variables
	SET @Result = 'E'
	SET @Counter = 1
	SET @LineNum = 0

	-- Create temporary table for processing
	CREATE TABLE #Temp (RowID int identity(1,1), agrid int, linenum int)

	-- Populate temporary table
	INSERT INTO #Temp (agrid, linenum)
	SELECT agrtid, line_no
	FROM Agresso.dbo.algbatchinput
	ORDER BY order_id, line_no

	-- Process temporary table
	WHILE EXISTS (SELECT 1 FROM #TEMP WHERE RowID = @Counter)
		BEGIN
			SELECT @LineNum = (@LineNum + 1) * linenum FROM #TEMP WHERE RowID = @Counter
			UPDATE Agresso.dbo.algbatchinput SET line_no = @LineNum
			WHERE agrtid = (SELECT agrid FROM #TEMP WHERE RowID = @Counter)
			SET @Counter = @Counter + 1
		END

	-- Set flag
	SET @Result = 'S'

	-- Delete temporary table
	DROP TABLE #Temp

END

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

