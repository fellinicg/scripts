USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION dbo.ufSettingValue (@SettingLabel varchar(80))
RETURNS varchar(1024)
AS
BEGIN
DECLARE
  @SettingValue varchar(1024)
  
  SELECT @SettingValue = Value FROM tbSettings WHERE Label = @SettingLabel

  RETURN(@SettingValue)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF