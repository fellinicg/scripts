USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061018
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upGetFileTypeS
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Retrieve data from working table
	SELECT 
		batch_id, 
		interface, 
		voucher_type, 
		trans_type, 
		client, 
		account, 
		dim_1, 
		dim_2, 
		dim_3, 
		dim_4, 
		dim_5, 
		dim_6, 
		dim_7, 
		tax_code, 
		tax_system, 
		currency, 
		dc_flag, 
		cur_amount, 
		amount, 
		number_1, 
		value_1, 
		value_2, 
		value_3, 
		description, 
		trans_date, 
		voucher_date, 
		voucher_no, 
		period, 
		tax_id, 
		ext_inv_ref, 
		ext_ref, 
		due_date, 
		disc_date, 
		discount, 
		commitment, 
		order_id, 
		kid, 
		pay_transfer, 
		status, 
		apar_type, 
		apar_id, 
		pay_flag, 
		voucher_ref, 
		sequence_ref, 
		intrule_id, 
		factor_short, 
		responsible, 
		apar_name, 
		address, 
		province, 
		place, 
		bank_account, 
		pay_method, 
		vat_reg_no, 
		zip_code, 
		curr_licence, 
		account2, 
		base_amount, 
		base_curr, 
		pay_temp_id, 
		allocation_key, 
		period_no, 
		clearing_code, 
		swift, 
		arrive_id, 
		bank_acc_type
	FROM dbo.tbGL07_55

END

SET ANSI_NULLS OFF
GO
GO
SET QUOTED_IDENTIFIER OFF
GO