USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE dbo.tbLG20_555(
	[amount_set] [char] (1) NOT NULL DEFAULT ('1'),
	[apar_gr_id] [char] (25) NOT NULL DEFAULT (''),
	[apar_id] [char] (25) NOT NULL DEFAULT (''),
	[art_descr] [char] (255) NOT NULL,
	[art_gr_id] [char] (25) NOT NULL,
	[article] [char] (25) NOT NULL,
	[article_2] [char] (50) NOT NULL DEFAULT (''),
	[article_3] [char] (25) NOT NULL DEFAULT (''),
	[batch_id] [char] (25) NOT NULL DEFAULT ('lg2055'),
	[bflag] [char] (1) NOT NULL DEFAULT ('1'),
	[bonus_type] [char] (1) NOT NULL DEFAULT ('0'),
	[client] [char] (25) NOT NULL DEFAULT ('MP'),
	[currency] [char] (25) NOT NULL DEFAULT ('USD'),
	[contract_ctr] [char] (1) NOT NULL DEFAULT (''),
	[contract_id] [char] (25) NOT NULL DEFAULT (''),
	[date_from] [char] (8) NOT NULL DEFAULT ('20060101'),
	[date_limitted] [char] (1) NOT NULL DEFAULT ('0'),
	[date_to] [char] (8) NOT NULL DEFAULT ('20091231'),
	[deliv_day] [char] (8) NOT NULL DEFAULT ('5'),
	[disc_type] [char] (1) NOT NULL DEFAULT ('0'),
	[ean] [char] (50) NOT NULL DEFAULT (''),
	[fixed_contract] [char] (1) NOT NULL DEFAULT (''),
	[guarantee] [char] (8) NOT NULL DEFAULT (''),
	[kit_type] [char] (1) NOT NULL DEFAULT ('0'),
	[language] [char] (2) NOT NULL DEFAULT ('US'),
	[line_no] [char] (8) NOT NULL DEFAULT (''),
	[location] [char] (12) NOT NULL DEFAULT (''),
	[lot_flag] [char] (1) NOT NULL DEFAULT ('0'),
	[message_text] [char] (80) NOT NULL DEFAULT (''),
	[max_level] [char] (20) NOT NULL DEFAULT (''),
	[multiplier] [char] (20) NOT NULL DEFAULT (''),
	[order_level] [char] (20) NOT NULL DEFAULT (''),
	[order_val] [char] (20) NOT NULL DEFAULT (''),
	[overrun] [char] (20) NOT NULL DEFAULT (''),
	[period_from] [char] (6) NOT NULL DEFAULT (''),
	[period_to] [char] (6) NOT NULL DEFAULT (''),
	[price_gr] [char] (4) NOT NULL DEFAULT (''),
	[purch_amt] [char] (20) NOT NULL,
	[purch_price] [char] (20) NOT NULL DEFAULT (''),
	[purch_rule] [char] (4) NOT NULL DEFAULT (''),
	[reference_1] [char] (255) NOT NULL DEFAULT (''),
	[rel_attr_id1] [char] (4) NOT NULL DEFAULT (''),
	[rel_attr_id2] [char] (4) NOT NULL DEFAULT (''),
	[rel_attr_id3] [char] (4) NOT NULL DEFAULT (''),
	[rel_attr_id4] [char] (4) NOT NULL DEFAULT (''),
	[rel_attr_id5] [char] (4) NOT NULL DEFAULT (''),
	[rel_value1] [char] (25) NOT NULL DEFAULT (''),
	[rel_value2] [char] (25) NOT NULL DEFAULT (''),
	[rel_value3] [char] (25) NOT NULL DEFAULT (''),
	[rel_value4] [char] (25) NOT NULL DEFAULT (''),
	[rel_value5] [char] (25) NOT NULL DEFAULT (''),
	[rent_flag] [char] (1) NOT NULL DEFAULT ('2'),
	[sales_price] [char] (20) NOT NULL DEFAULT (''),
	[serial_cat] [char] (4) NOT NULL DEFAULT (''),
	[serial_flag] [char] (1) NOT NULL,
	[short_info] [char] (60) NOT NULL DEFAULT (''),
	[status] [char] (1) NOT NULL DEFAULT ('N'),
	[stock_flag] [char] (1) NOT NULL,
	[tax_code_po] [char] (25) NOT NULL DEFAULT ('0'),
	[text_type] [char] (1) NOT NULL DEFAULT (''),
	[trans_type] [char] (2) NOT NULL DEFAULT ('1'),
	[unit_code] [char] (3) NOT NULL DEFAULT ('EA'),
	[unit_descr] [char] (255) NOT NULL DEFAULT ('Each'),
	[unit_value] [char] (20) NOT NULL DEFAULT (''),
	[update_flag] [char] (1) NOT NULL DEFAULT ('0'),
	[value_1] [char] (20) NOT NULL DEFAULT ('0'),
	[value_from] [char] (20) NOT NULL DEFAULT ('0'),
	[value_to] [char] (20) NOT NULL DEFAULT (''),
	[volume] [char] (20) NOT NULL DEFAULT (''),
	[warehouse] [char] (25) NOT NULL,
	[weight] [char] (20) NOT NULL DEFAULT ('')
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF