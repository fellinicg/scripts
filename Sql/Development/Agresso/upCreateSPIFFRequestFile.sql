USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061019
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upCreateSPIFFRequestFile
AS
BEGIN
	DECLARE @Result int
	DECLARE @FileType char(1)
	DECLARE @DBProcName varchar(40)
	DECLARE @OutputFileName varchar(40)
	DECLARE @SessionID uniqueidentifier

	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 1
	SET @SessionID = dbo.ufSettingValue('ReqSessionID')

	-- Assign necessary info to local variables
	SELECT 
		@FileType = FileType,
		@OutputFileName = OutputFileName,
		@DBProcName = DBProcName
	FROM dbo.tbFileCreation
	WHERE FileType IN ('S')

	-- Populate dbo.tbGL07_55
	EXECUTE dbo.upPopulatetbGL07_55

	-- Create file
	EXECUTE @Result = dbo.upCreateAgressoFile
		@FileType = @FileType,
		@DBProcName = @DBProcName,
		@OutputFileName = @OutputFileName,
		@SessionID = @SessionID

	-- Return result code
	RETURN @Result

END

