USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20070209
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20070221
-- ============================================================
ALTER PROCEDURE dbo.upUpdateCheckNumbers
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Update Check Numbers and AgressoStatus in Ricoh.dbo.Invoices on MOONRAKER
	UPDATE MOONRAKER.Ricoh.dbo.Invoices SET CheckNumber = h.ext_inv_ref
	, DatePaid = h.payment_date
	, AgressoStatus = 'C'
	FROM MOONRAKER.Ricoh.dbo.Invoices i 
	INNER JOIN Agresso.dbo.asuhistr h ON i.ID = h.dim_4
	WHERE i.AgressoStatus = 'R'
		AND h.client = 'MP'
		AND h.account = '2217'
		AND h.pay_flag = 1

END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF