USE [AgressoLink]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
USE [AgressoLink]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbInvoices]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbInvoices]
GO
CREATE TABLE [dbo].[tbInvoices](
	[InvoiceID] [int] NOT NULL,
	[SessionID] [uniqueidentifier] NOT NULL,
	[CheckNumber] [varchar](20) NULL,
 CONSTRAINT [PK_tbInvoices] PRIMARY KEY CLUSTERED 
(
	[InvoiceID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF