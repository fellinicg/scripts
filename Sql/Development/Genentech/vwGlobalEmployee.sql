USE [Genentech]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW dbo.vwGlobalEmployee
AS

	SELECT u.UserAccountID, 
		g.PersonFirstGivenName + ' ' + g.PersonFamilyName AS BuiltFullName,
		g.PersonFirstGivenName AS FirstName, 
		g.PersonFamilyName AS LastName,
		e.EmailAddress AS EMAIL,
		CASE g.AccountStatus WHEN 'E' THEN 0 WHEN 'D' THEN 1 WHEN 'X' THEN 2 ELSE 1 END StatusId
	FROM tbUsers u
		INNER JOIN SQL2KPROD.GlobalUsers.dbo.tbUserAccount g ON (u.UserAccountID = g.UserAccountID)
		INNER JOIN SQL2KPROD.GlobalUsers.dbo.tbEmailAddress e ON (g.UserAccountID = e.UserAccountID AND e.IsPreferred = 1)
