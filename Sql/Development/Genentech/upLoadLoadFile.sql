USE [Genentech]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE dbo.upLoadLoadFile

AS


DECLARE @CreateEmployees table 
(CustEmployeeNum int identity NOT NULL,
FirstName varchar(50) NOT NULL, 
LastName varchar(50) NOT NULL,
TerritoryNBR varchar(5)  NULL,
SalesTitle varchar(5)  NULL,
EMail varchar(50) NOT NULL,
Phone varchar(10)  NULL,
UserAccountID int NULL)

-- STATISTICS
DECLARE @CreateEmployeeCNT int
	
-- PROGRESS MARKERS
DECLARE @CreateGlobalUsers bit

-- OTHER
DECLARE
	@IsFailure bit,
	@AccountType char(1),
	@IsPerson bit,
	@AccountStatus char(1),
	@ClientID int,
	@Result char(1)

	SET NOCOUNT ON

	-- INITIALIZE/SET CONSTANTS
	SET @IsFailure = 0
	SET @CreateGlobalUsers = 0
	SET @AccountType = 'C'
	SET @IsPerson = 1
	SET @AccountStatus = 'E'
	EXEC dbo.upGetSettingValue @SettingLabel = 'GenentechClientID', @SettingValue = @ClientID OUTPUT

	-- POPULATE CONTROL TABLES
	-- TO BE CREATED
	INSERT @CreateEmployees(FirstName, LastName, TerritoryNBR, SalesTitle, EMail, Phone)
	SELECT First_Name, Last_Name, Territory_NBR, Sales_Title, E_Mail, Phone
	FROM tbLoadFileStaging
	LEFT JOIN vwGlobalEmployee ON E_Mail = EMAIL
	WHERE EMAIL IS NULL																												-- WITH NO MATCH IN EMPLOYEE TABLE
	SET @CreateEmployeeCNT = @@ROWCOUNT

	-->> PERFORM GLOBAL USER OPERATIONS FIRST AS THEY CANNOT BE INCLUDED IN TRANSACTION BLOCKING <<--

	DECLARE
		@CurrEmpNum int,
		@UserAccountID int,
		@LastName varchar(50),
		@FirstName varchar(50),
		@EMail varchar(50),
		@EmailReturn int

	-- CREATE NEW GLOBAL USERS
	IF (@CreateEmployeeCNT <> 0)
	BEGIN
		SET @CreateGlobalUsers = 1
		SELECT @CurrEmpNum = MIN(CustEmployeeNum) FROM @CreateEmployees
		WHILE (ISNULL(@CurrEmpNum, '') <> '')
		BEGIN
			-- GET INFO
			SELECT @LastName = LastName, @FirstName = FirstName, @EMail=EMAIL
			FROM @CreateEmployees WHERE CustEmployeeNum = @CurrEmpNum
			-- CREATE GLOBAL USER
			SET @UserAccountID = NULL
			EXECUTE SQL2KPROD.GlobalUsers.dbo.upAddUserAccount
				 @AccountType = @AccountType
				,@IsPerson = @IsPerson
				,@ClientID = @ClientID
				,@CustAccountCode = @CurrEmpNum
				,@PersonFirstGivenName = @FirstName
				,@PersonFamilyName = @LastName
				,@AccountStatus = @AccountStatus
				,@UserAccountID = @UserAccountID OUTPUT
--			EXECUTE SQL2KPROD.GlobalUser.dbo.AddNewGlobalUser
--				 @gu_FirstName = @FirstName
--				,@gu_LastName = @LastName
--				,@gu_cl_ID = @ClientID
--				,@gu_SSN = ''
--				,@UserReturn = @UserAccountID OUTPUT
			-- ERROR HANDLER
			IF (ISNULL(@UserAccountID, 0) = 0)
			BEGIN
				SET @IsFailure = 1
				BREAK
			END
			ELSE
				UPDATE @CreateEmployees SET UserAccountID = @UserAccountID WHERE CustEmployeeNum = @CurrEmpNum
			-- Add Email
			EXECUTE SQL2KPROD.GlobalUsers.dbo.upAddEmailAddress
				@UserAccountID = @UserAccountID,
				@EMailAddressType = 'W',
				@EmailAddress = @EMail,
				@IsPreferred = 1,
				@EmailAddressID = @EmailReturn OUTPUT
--			EXECUTE SQL2KPROD.GlobalUser.dbo.AddNewEMailAddress
--				@em_gu_ID = @UserAccountID,
--				@em_ct_ID = 1,
--				@em_Address= @EMail,
--				@em_Default = 1,
--				@EmailReturn = @EmailReturn OUTPUT
			-- ERROR HANDLER
			IF (ISNULL(@EmailReturn, 0) = 0)
			BEGIN
				SET @IsFailure = 1
				BREAK
			END
			-- GET NEXT CANDIDATE
			SELECT @CurrEmpNum = MIN(CustEmployeeNum) FROM @CreateEmployees WHERE CustEmployeeNum > @CurrEmpNum
		END
	END

	
	-->> PERFORM LOCAL DB OPERATIONS PROTECTED BY TRANSACTION BLOCKING <<--

	-- OPEN TRAN
	IF (@IsFailure = 0)
	BEGIN
		BEGIN TRAN
	END

	-- INSERT EMPLOYEES/ACCTS
	IF (@IsFailure = 0 AND @CreateEmployeeCNT <> 0)
	BEGIN
		-- CREATE USER ACCTS
		INSERT INTO dbo.tbUsers(UserAccountID, Territory_NBR, Sales_Title, [Password], AutGen,Phone)
		SELECT UserAccountID, TerritoryNBR, SalesTitle, dbo.ufGeneratePassword(FirstName, LastName, current_timestamp), 1,Phone
		FROM @CreateEmployees
		-- ERROR HANDLER
		IF (@@ROWCOUNT <> @CreateEmployeeCNT)
		BEGIN
			ROLLBACK TRAN
			SET @IsFailure = 1
		END
	END

	-- COMMIT TRAN
	IF (@IsFailure = 0)
	BEGIN
		COMMIT TRAN
	END

	-- SET SUCCESS STATUS & CLEANUP
	IF (@IsFailure = 0)
	BEGIN
		DELETE tbLoadFileStaging
	END

	-- FAILURE HANDLER: OPS PERFORMED IN REMOTE DB MUST BE EXPLICITLY UNDONE AS THEY COULD NOT PARTICIPATE IN LOCAL TRAN
	IF (@IsFailure = 1)
	BEGIN
		IF (@CreateGlobalUsers = 1)
		BEGIN
			SELECT @CurrEmpNum = MIN(CustEmployeeNum) FROM @CreateEmployees WHERE UserAccountID IS NOT NULL
			WHILE (ISNULL(@CurrEmpNum, '') <> '')
			BEGIN
				-- GET INFO
				SELECT @UserAccountID = UserAccountID
				FROM @CreateEmployees WHERE CustEmployeeNum = @CurrEmpNum

				-- REMOVE FROM GLOBALUSERS
				EXEC SQL2KPROD.GlobalUsers.dbo.upDeleteUserAccount
					@Result OUTPUT,
					@UserAccountID = @UserAccountID

				-- GET NEXT CANDIDATE
				SELECT @CurrEmpNum = MIN(CustEmployeeNum) 
				FROM @CreateEmployees 
				WHERE UserAccountID IS NOT NULL AND CustEmployeeNum > @CurrEmpNum
			END
--			DELETE SQL2KPROD.GlobalUser.dbo.Email
--			FROM SQL2KPROD.GlobalUser.dbo.Email e
--				INNER JOIN @CreateEmployees c ON (e.em_gu_ID = c.UserAccountID)
--			DELETE SQL2KPROD.GlobalUser.dbo.GlobalUsers
--			FROM SQL2KPROD.GlobalUser.dbo.GlobalUsers g
--				INNER JOIN @CreateEmployees c ON (g.gu_ID = c.UserAccountID)

		END
	END

	SET NOCOUNT OFF