USE [Baxter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE dbo.upSendContactUsEmail
	@SessionID uniqueidentifier,
	@ContactUsSubjectType char(1),
	@MessageBody varchar(1000),
	@IsSuccess bit OUTPUT
AS
DECLARE
	@EmpLangCode char(3),
	@FromEmailAddress varchar(120),
	@EmployeeFullName varchar(80),
	@MessageBodyTemplate varchar(6000),
	@MessageSubjectTemplate varchar(120),
	@PendingEmailStatus char(1),
	@ReplyToEmailAddress varchar(120),
	@SMTPContentType varchar(10),
	@SMTPPriority varchar(6),
	@EmployeeID int,
	@ToEmailAddressList varchar(400),
	@EmployeePhoneNum varchar(30),
	@EmployeeEmailAddress varchar(120),
	@EmailID int,
	@CCEmailAddressList varchar(400),
	@BCCEmailAddressList varchar(400),
	@CustEmployeeNum char(6),
	@EmailTemplateID int

	SET NOCOUNT ON

	SET @IsSuccess = 0
	SET @PendingEmailStatus = 'P'

	-- CHECK INPUTS
	IF (@ContactUsSubjectType NOT IN ('R', 'C', 'V', 'N', 'T', 'H', 'O'))  -- CUST SERV TYPE EMAILS
		RETURN
	SELECT @EmployeeID = dbo.ufSessionUserAccount(@SessionID)
	IF (ISNULL(@EmployeeID, 0) = 0)
		RETURN
	SELECT @EmpLangCode = dbo.ufAccountPreferredLanguage(@EmployeeID)
	IF (@EmpLangCode = 'XXX')  -- FUNC FAILURE INDICATOR
		RETURN

	-- GET EMPLOYEE INFO
	SELECT
		@CustEmployeeNum = CustEmployeeNum,
		@EmployeeFullName = dbo.ufEmployeeGUFullName(EmployeeID),
		@EmployeePhoneNum = ISNULL(WorkPhoneNum, 'N/A'),
		@EmployeeEmailAddress = ISNULL(WorkEmailAddress, 'N/A')
	FROM tbEmployee
	WHERE EmployeeID = @EmployeeID
		AND IsDeleted = 0
	IF (@@ROWCOUNT = 0)
		RETURN

	-- GET EMAIL TEMPLATE INFO
	SELECT
		@ToEmailAddressList = ToName + ' <' + ToEmailAddressList + '>',
		@ReplyToEmailAddress = ReplyToEmailAddress,
		@CCEmailAddressList = CCEmailAddressList,
		@BCCEmailAddressList = BCCEmailAddressList,
		@MessageSubjectTemplate = MessageSubjectTemplate,
		@MessageBodyTemplate = MessageBodyTemplate,
		@SMTPContentType = SMTPContentType,
		@SMTPPriority = SMTPPriority,
		@EmailTemplateID = EmailTemplateID
	FROM tbEmailTemplate
	WHERE EmailType = @ContactUsSubjectType  -- SUBJECT ID VALS INHERITED FROM EMAIL TYPES
		AND LanguageCode = @EmpLangCode
	IF (@@ROWCOUNT = 0)
		RETURN

	-- HANDLE CASE WHERE EMPLOYEE HAS NO EMAIL ADDRESS
	IF (dbo.ufEmployeeHasEmailAddress(@EmployeeID) = 0)
		SET @FromEmailAddress = @ReplyToEmailAddress
	ELSE
		SET @FromEmailAddress = @EmployeeEmailAddress

	-- BUILD MESSAGE BODY
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<SessionCustEmployeeNum>', @CustEmployeeNum)
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<SessionEmployeeFullName>', @EmployeeFullName)
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<SessionEmployeePhoneNumber>', @EmployeePhoneNum)
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<SessionEmployeeEmailAddress>', @EmployeeEmailAddress)
	IF (LEN(@MessageBodyTemplate) + LEN(@MessageBody) > 7000)  -- USER INPUT AND TEMPLATE BODY TO BIG
		RETURN
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<InputtedComments>', REPLACE(@MessageBody, '''', ''''''))

	-- CREATE tbOutboundEmail RECORD
	INSERT tbOutboundEmail(EmailTemplateID, ToEmailAddressList, FromName, FromEmailAddress,
		ReplyToEmailAddress, CCEmailAddressList, BCCEmailAddressList, MessageSubject, MessageBody, SMTPContentType,
		SMTPPriority, Status, StatusDateTime, FromEmployeeID)
	VALUES(@EmailTemplateID, @ToEmailAddressList, @EmployeeFullName, @FromEmailAddress,
		@ReplyToEmailAddress, @CCEmailAddressList, @BCCEmailAddressList, @MessageSubjectTemplate, @MessageBodyTemplate,
		@SMTPContentType, @SMTPPriority, @PendingEmailStatus, CURRENT_TIMESTAMP, @EmployeeID)
	SET @EmailID = @@IDENTITY
	IF (ISNULL(@EmailID, 0) = 0)
		RETURN

	-- QUEUE MESSAGE
	EXEC dbo.upSendOutboundEmail
		@EmailID = @EmailID,
		@IsSuccess = @IsSuccess OUTPUT

	SET NOCOUNT OFF
