--==========================================================================
-- Add column template
--
-- This template creates a table, then it adds a new column to the table.
--==========================================================================
USE Baxter
GO

-- Add a new column to the table
ALTER TABLE dbo.tbAwardBehavior
	ADD Retired bit NOT NULL DEFAULT 0
GO

-- Update Retired flag
UPDATE dbo.tbAwardBehavior SET Retired = 1 WHERE BehaviorID IN (6,7,8)

-- Disable UNIQUE KEY constraint
IF EXISTS (SELECT name FROM dbo.sysindexes WHERE name = N'akAwardBehaviorDescr' and id=object_id(N'[dbo].[tbAwardBehavior]') )
ALTER TABLE [dbo].[tbAwardBehavior] DROP CONSTRAINT [akAwardBehaviorDescr]

-- Insert new data
INSERT INTO dbo.tbAwardBehavior (BehaviorID, Title, Description, ResourceFileCode)
VALUES (9, 'Teamwork', 'We work openly and supportively in teams, aiming towards common goals.  We form teams with our customers and suppliers to respond quickly to changing customer needs.  We have fun working with each other and we take pride in our joint accomplishments.', 'teamwork')

INSERT INTO dbo.tbAwardBehavior (BehaviorID, Title, Description, ResourceFileCode)
VALUES (10, 'Innovation', 'Innovation is the key to creating new sources of value for our customers and shareholders.  We must quickly transform new technologies and new ideas into products and services that exceed customer expectations and improve our own effectiveness.  We do this by valuing and rewarding creativity, diverse thinking styles and intelligent risk-taking.  We act to maximize potential success, rather than tominimize potential failure.', 'innovation')
