USE [Baxter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[upGetAwardBehaviors]
AS
SET NOCOUNT ON

  SELECT BehaviorID, Title, Description, ResourceFileCode 
	FROM tbAwardBehavior
	WHERE Retired = 0
	ORDER BY BehaviorID

  SET NOCOUNT OFF
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

