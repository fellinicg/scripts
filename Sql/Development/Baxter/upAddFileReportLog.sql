USE [Baxter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: December 16, 2005
-- Description:	Add log entry to tbFileReportLog
-- =============================================
CREATE PROCEDURE dbo.upAddFileReportLog 
	@FileReportType CHAR(1),
	@ArchiveFileName VARCHAR(80)
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.tbFileReportLog (FileReportType, ArchiveFileName, CreateDateTime)
	VALUES (@FileReportType, @ArchiveFileName, GETDATE())

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF