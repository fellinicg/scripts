USE [Baxter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW dbo.vwGlobalEmployee
AS

	SELECT e.EmployeeID, 
		e.FullName AS OrigFullName, 
		g.PersonFirstGivenName + ' ' + g.PersonFamilyName AS BuiltFullName,
		g.PersonFirstGivenName AS FirstName, 
		g.PersonFamilyName AS LastName,
		g.CustAccountCode AS CustEmployeeNum
	FROM tbEmployee e
		INNER JOIN SQL2KPROD.GlobalUsers.dbo.tbUserAccount g ON (e.EmployeeID = g.UserAccountID)