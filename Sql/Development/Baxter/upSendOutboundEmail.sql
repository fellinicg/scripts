USE [Baxter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.upSendOutboundEmail
	@EmailID int,
	@IsSuccess bit OUTPUT
AS
DECLARE
	@RedirectOutboundEmail bit,
	@EmailType char(1),
	@ToEmailAddressList varchar(400),
	@FromName varchar(80),
	@FromEmailAddress varchar(120),
	@ReplyToEmailAddress varchar(120),
	@CCEmailAddress varchar(120),
	@BCCEmailAddress varchar(120),
	@MessageSubject varchar(120),
	@MessageBody varchar(7000),
	@SMTPPriority char(1),
	@SMTPContentType char(1),
	@MailerJobID int,
	@MessageQueued bit,
	@MessageSent bit,
	@SendImmediately bit,
	@StatusComment varchar(120),
	@PendingStatus char(1),
	@SuccessStatus char(1),
	@FailureStatus char(1),
	@RedirectEmailAddressList varchar(400),
	@SendStatus char(1)

	SET NOCOUNT ON
	
	SET @IsSuccess = 0

	-- CHECK INPUTS
	IF (NOT EXISTS (SELECT 1 FROM tbOutboundEmail WHERE EmailID = @EmailID))
	BEGIN
		RAISERROR('upSendOutboundEmail - @EmailID supplied does not exist', 16, 1)
		RETURN
	END

	-- SET CONSTANTS
	SET @PendingStatus = 'P'
	SET @SuccessStatus = 'S'
	SET @FailureStatus = 'F'

	-- SET VARS
	SET @SendImmediately = dbo.ufSettingValue('OutboundEmailSendImmediately')
	IF (@SendImmediately IS NULL)
	BEGIN
		SET @StatusComment = 'upSendOutboundEmail - failed to obtain value for setting (OutboundEmailSendImmediately)'
		UPDATE tbOutboundEmail SET Status = @FailureStatus, StatusDateTime = CURRENT_TIMESTAMP, StatusComment = @StatusComment
		RETURN
	END
	SET @RedirectOutboundEmail = dbo.ufSettingValue('RedirectOutboundEmail')
	IF (@RedirectOutboundEmail IS NULL)
	BEGIN
		SET @StatusComment = 'upSendOutboundEmail - failed to obtain value for setting (RedirectOutboundEmail)'
		UPDATE tbOutboundEmail SET Status = @FailureStatus, StatusDateTime = CURRENT_TIMESTAMP, StatusComment = @StatusComment
		RETURN
	END
	IF (@RedirectOutboundEmail = 1)
	BEGIN
		SET @RedirectEmailAddressList = dbo.ufSettingValue('RedirectOutboundEmailToAddressList')
		IF (ISNULL(@RedirectEmailAddressList, '') = '')
		BEGIN
			SET @StatusComment = 'upSendOutboundEmail - failed to obtain value for setting (RedirectOutboundEmailToAddressList)'
			UPDATE tbOutboundEmail SET Status = @FailureStatus, StatusDateTime = CURRENT_TIMESTAMP, StatusComment = @StatusComment
			RETURN
		END
	END

	-- GET EMAIL JOB INFO
	SELECT
		@ToEmailAddressList = ToEmailAddressList,
		@FromName = FromName,
		@FromEmailAddress = FromEmailAddress,
		@ReplyToEmailAddress = ReplyToEmailAddress,
		@CCEmailAddress = CCEmailAddressList,
		@BCCEmailAddress = BCCEmailAddressList,
		@MessageSubject = MessageSubject,
		@MessageBody = MessageBody,
		@SMTPContentType = CASE SMTPContentType WHEN 'text/plain' THEN 'P' WHEN 'text/html' THEN 'H' END,
		@SMTPPriority = CASE SMTPPriority WHEN 'HIGH' THEN 'H' WHEN 'NORMAL' THEN 'N' WHEN 'LOW' THEN 'L' END
	FROM tbOutboundEmail (UPDLOCK HOLDLOCK)
	WHERE EmailID = @EmailID
		AND Status = @PendingStatus
	IF (@@ROWCOUNT = 0)
	BEGIN
		SET @StatusComment = 'upSendOutboundEmail - record does not exist for ID given or has status other than Pending'
		UPDATE tbOutboundEmail SET Status = @FailureStatus, StatusDateTime = CURRENT_TIMESTAMP, StatusComment = @StatusComment
		COMMIT TRAN
		RETURN
	END

	-- REDIRECT MODE: REDIRECTS ALL MESSAGES TO DESIGNATED ADDRESS LIST
	IF (@RedirectOutboundEmail = 1)
	BEGIN
		SET @ToEmailAddressList = @RedirectEmailAddressList
		SET @CCEmailAddress = NULL
		SET @BCCEMailAddress = NULL
		SET @ReplyToEmailAddress = NULL
	END

	-- SEND MESSAGE
	EXEC [mail.madisonpg.com].OutboundEmail.dbo.upQueueMessage
		@EmailID = @MailerJobID OUTPUT,
		@ToEmailAddressList = @ToEmailAddressList,
		@FromName = @FromName,
		@FromEmailAddress = @FromEmailAddress,
		@ReplyToEmailAddress = @ReplyToEmailAddress,
		@CCEmailAddressList = @CCEmailAddress,
		@BCCEmailAddressList = @BCCEmailAddress,
		@MessageSubject = @MessageSubject,
		@MessageBody = @MessageBody,
		@SMTPContentType = @SMTPContentType,
		@SMTPPriority = @SMTPPriority,
		@SendImmediately = @SendImmediately,
		@MessageQueued = @MessageQueued OUTPUT,
		@MessageSent = @MessageSent OUTPUT
	IF (@MessageQueued = 0 AND @MessageSent = 0)
	BEGIN
		SET @SendStatus = @FailureStatus
		SET @StatusComment = 'upSendOutboundEmail - call to upQueueMessage on mail server failed'
	END
	ELSE
		BEGIN
			SET @SendStatus = @SuccessStatus
			SET @IsSuccess = 1
		END

	-- UPDATE RECORD STATUS
	UPDATE tbOutboundEmail SET
		MailerJobID = @MailerJobID,
		Status = @SendStatus,
		StatusDateTime = CURRENT_TIMESTAMP,
		StatusComment = @StatusComment
	WHERE EmailID = @EmailID
	
	SET NOCOUNT OFF



GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF