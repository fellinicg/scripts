USE [Baxter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20051214
-- LastEditedByFullName: Todd Fellini 	
-- LastEditDate: 20060829
-- Description:	Get data for EDI file report
-- ============================================================

CREATE PROCEDURE dbo.upGetFileReportTypeE
	@DaysPriorToEndOfMonth INT = 4,
	@CountryCode VARCHAR(5) = 'USA'
AS
BEGIN

	DECLARE @Blank VARCHAR(10)
	DECLARE @Today VARCHAR(8)
	DECLARE @Remarks VARCHAR(30)
	DECLARE @PeriodBegin DATETIME
	DECLARE @PeriodEnd DATETIME
	DECLARE @DebitCardActivityYTD MONEY
	DECLARE @GiftCertFulfillmentFee SMALLMONEY
	DECLARE @DebitCardFee SMALLMONEY

	SET NOCOUNT ON

	-- Initialize variables
	SET @GiftCertFulfillmentFee = .09
	SET @Blank = 'NULL'
  SET @Today = REPLACE(CONVERT(VARCHAR, CONVERT(DATETIME, dbo.ufGetPriorBusinessDate(DATEPART(mm, CURRENT_TIMESTAMP), DATEPART(yy, CURRENT_TIMESTAMP), @CountryCode, @DaysPriorToEndOfMonth)), 10), '-', '/')
	IF (SELECT DATEPART(mm, CURRENT_TIMESTAMP)) = 1 
		SET @PeriodBegin = (SELECT dbo.ufGetPriorBusinessDate(DATEPART(mm, CURRENT_TIMESTAMP) + 11, DATEPART(yy, CURRENT_TIMESTAMP)-1, @CountryCode, @DaysPriorToEndOfMonth))
	ELSE 
		SET @PeriodBegin = (SELECT dbo.ufGetPriorBusinessDate(DATEPART(mm, CURRENT_TIMESTAMP) - 1, DATEPART(yy, CURRENT_TIMESTAMP), @CountryCode, @DaysPriorToEndOfMonth))	
	SET @PeriodEnd = DATEADD(ss, -1 , CONVERT(DATETIME, CONVERT(VARCHAR, dbo.ufGetPriorBusinessDate(DATEPART(mm, CURRENT_TIMESTAMP), DATEPART(yy, CURRENT_TIMESTAMP), @CountryCode, @DaysPriorToEndOfMonth), 101)))
	SET @Remarks = 'RR ' + REPLACE(CONVERT(VARCHAR, @PeriodBegin, 7), ',', '') + ' thru ' + REPLACE(CONVERT(VARCHAR, @PeriodEnd, 7), ',', '')

	-- Get YTD debit card activity prior to @PeriodBegin
	SELECT @DebitCardActivityYTD = SUM(AwardAmtUSD)
	FROM dbo.tbAward
	WHERE StatusDateTime < @PeriodEnd --@PeriodBegin
		AND DATEPART(YEAR, StatusDateTime) = DATEPART(YEAR, @PeriodBegin)

	-- Set debit card fee
	IF (@DebitCardActivityYTD < 4000000) 
		SET @DebitCardFee = .09
	IF (@DebitCardActivityYTD > 4000001) AND (@DebitCardActivityYTD < 6000000) 
		SET @DebitCardFee = .0895
	IF (@DebitCardActivityYTD > 6000001) AND (@DebitCardActivityYTD < 8000000) 
		SET @DebitCardFee = .0885
	IF (@DebitCardActivityYTD > 8000001) 
		SET @DebitCardFee = .0875

	SELECT 
		UPPER(a.AwardID)
		,'EDIMPG'
		,'36881870'
		,'Madison Performance Group'
		,recip.LastName
		,recip.FirstName
		,CONVERT(VARCHAR, a.AwardAmtUSD, 0)
		,CASE a.AwardType 
			WHEN 'G' THEN 'GC'
			WHEN 'D' THEN 'DC'
		 END
		,CONVERT(VARCHAR, dbo.ufShipChargeByLocSetCode(a.RecipLocationSetCode), 0)
		,CASE a.AwardType 
			WHEN 'G' THEN CONVERT(VARCHAR, (a.AwardAmtUSD + (a.AwardAmtUSD * @GiftCertFulfillmentFee)), 0)
			WHEN 'D' THEN CONVERT(VARCHAR, (a.AwardAmtUSD * @DebitCardFee), 0)
		 END
		,CASE a.AwardType 
			WHEN 'G' THEN CONVERT(VARCHAR, (a.AwardAmtUSD + (a.AwardAmtUSD * @GiftCertFulfillmentFee) + dbo.ufShipChargeByLocSetCode(a.RecipLocationSetCode)), 0)
			WHEN 'D' THEN CONVERT(VARCHAR, (a.AwardAmtUSD * @DebitCardFee) + dbo.ufShipChargeByLocSetCode(a.RecipLocationSetCode), 0)
		 END
		,'A'
		,@Today
		,giver.LegalEntity
		,@Today
		,@Blank
		,@Remarks
		,giver.JDECode
		,'80256'
		,givername.LastName
		,givername.FirstName
		,'Award from Madison EDIMPG'
		,@Blank
		,'USD'
		,@Blank
		,@Blank
		,@Blank
		,@Blank
	FROM dbo.tbAward a
		LEFT JOIN dbo.tbEmployee giver ON (a.GiverEmployeeID = giver.EmployeeID)
		LEFT JOIN dbo.vwGlobalEmployee givername ON (a.GiverEmployeeID = givername.EmployeeID)
		LEFT JOIN dbo.vwGlobalEmployee recip ON (a.RecipEmployeeID = recip.EmployeeID)
	WHERE a.StatusDateTime BETWEEN @PeriodBegin AND @PeriodEnd
		AND a.AwardType IN ('D', 'G')
		AND a.Status = 'A'
	ORDER BY a.StatusDateTime

END
