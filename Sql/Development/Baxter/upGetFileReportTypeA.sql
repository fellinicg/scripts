USE [Baxter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20051216
-- LastEditedByFullName: Todd Fellini 	
-- LastEditDate: 20060829
-- Description:	Get data for Abandoned Property Active Debit Cards file report
-- ============================================================
CREATE PROCEDURE dbo.upGetFileReportTypeA
	@DaysPriorToEndOfMonth INT = 4,
	@CountryCode VARCHAR(5) = 'USA'
AS
BEGIN

	DECLARE @Blank VARCHAR(10)
	DECLARE @PeriodBegin SMALLDATETIME
	DECLARE @PeriodEnd SMALLDATETIME

	SET NOCOUNT ON

	-- Initialize variables
	SET @Blank = 'NULL'
	IF (SELECT DATEPART(mm, CURRENT_TIMESTAMP)) = 1 
		SET @PeriodBegin = (SELECT dbo.ufGetPriorBusinessDate(DATEPART(mm, CURRENT_TIMESTAMP) + 11, DATEPART(yy, CURRENT_TIMESTAMP)-1, @CountryCode, @DaysPriorToEndOfMonth))
	ELSE 
		SET @PeriodBegin = (SELECT dbo.ufGetPriorBusinessDate(DATEPART(mm, CURRENT_TIMESTAMP) - 1, DATEPART(yy, CURRENT_TIMESTAMP), @CountryCode, @DaysPriorToEndOfMonth))	
	SET @PeriodEnd = DATEADD(ss, -1 , CONVERT(DATETIME, CONVERT(VARCHAR, dbo.ufGetPriorBusinessDate(DATEPART(mm, CURRENT_TIMESTAMP), DATEPART(yy, CURRENT_TIMESTAMP), @CountryCode, @DaysPriorToEndOfMonth), 101)))

	-- Get data for report
	SELECT DISTINCT
		gu.LastName
		,gu.FirstName
		,a.RecipCustEmployeeNum
		,e.EmployeeStatus
		,dc.CardActive
		,REPLACE(ISNULL(dc.ac_expDate, @Blank), '/20', '/')
		,REPLACE(CONVERT(VARCHAR, aae.LastAppAwdInitDate, 10), '-', '/')
		,CONVERT(VARCHAR, dc.TotalSpent, 0)
		,CONVERT(VARCHAR, dc.s_AvailBal, 0)
		,aae.LastAppAwdGiverCustEmployeeNum
	FROM dbo.tbAward a
		LEFT JOIN dbo.tbEmployee e ON a.RecipEmployeeID = e.EmployeeID
		LEFT JOIN dbo.vwGlobalEmployee gu ON e.EmployeeID = gu.EmployeeID
		LEFT JOIN dbo.tfApprovedAwardEmployeeInfo (@PeriodBegin, @PeriodEnd) aae ON e.EmployeeID = aae.RecipEmployeeID
		LEFT JOIN dbo.vwEmployeeDebitCardInfo dc ON e.EmployeeID = dc.u_MPGProgUserID
	WHERE a.StatusDateTime BETWEEN @PeriodBegin AND @PeriodEnd
		AND a.AwardType IN ('D')
		AND a.Status = 'A'
		AND ISNULL(dc.CardActive, 'N') = 'Y'

END
