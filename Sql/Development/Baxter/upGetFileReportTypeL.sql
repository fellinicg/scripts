USE [Baxter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20051216
-- LastEditedByFullName: Todd Fellini 	
-- LastEditDate: 20060829
-- Description:	Get data for General Ledger file report
-- ============================================================

CREATE PROCEDURE dbo.upGetFileReportTypeL
	@DaysPriorToEndOfMonth INT = 4,
	@CountryCode VARCHAR(5) = 'USA'
AS
BEGIN

	DECLARE @Blank VARCHAR(10)
	DECLARE @PeriodBegin DATETIME
	DECLARE @PeriodEnd DATETIME

	SET NOCOUNT ON

	-- Initialize variables
	SET @Blank = 'NULL'
	IF (SELECT DATEPART(mm, CURRENT_TIMESTAMP)) = 1 
		SET @PeriodBegin = (SELECT dbo.ufGetPriorBusinessDate(DATEPART(mm, CURRENT_TIMESTAMP) + 11, DATEPART(yy, CURRENT_TIMESTAMP)-1, @CountryCode, @DaysPriorToEndOfMonth))
	ELSE 
		SET @PeriodBegin = (SELECT dbo.ufGetPriorBusinessDate(DATEPART(mm, CURRENT_TIMESTAMP) - 1, DATEPART(yy, CURRENT_TIMESTAMP), @CountryCode, @DaysPriorToEndOfMonth))
	SET @PeriodEnd = DATEADD(ss, -1 , CONVERT(DATETIME, CONVERT(VARCHAR, dbo.ufGetPriorBusinessDate(DATEPART(mm, CURRENT_TIMESTAMP), DATEPART(yy, CURRENT_TIMESTAMP), @CountryCode, @DaysPriorToEndOfMonth), 101)))

	SELECT
		UPPER(a.AwardID)
		,giver.JDECode
		,'80256'
		,CONVERT(VARCHAR, a.AwardAmtUSD, 0)
		,CONVERT(VARCHAR, dbo.ufShipChargeByLocSetCode(a.RecipLocationSetCode), 0)
		,CONVERT(VARCHAR, (a.AwardAmtUSD + dbo.ufShipChargeByLocSetCode(a.RecipLocationSetCode)), 0)
		,'Baxter Rewards Award Charge'
	FROM dbo.tbAward a
		LEFT JOIN dbo.tbEmployee giver ON (a.GiverEmployeeID = giver.EmployeeID)
	WHERE a.StatusDateTime BETWEEN @PeriodBegin AND @PeriodEnd
		AND a.AwardType = 'D'
		AND a.Status = 'A'
	ORDER BY a.StatusDateTime

END
