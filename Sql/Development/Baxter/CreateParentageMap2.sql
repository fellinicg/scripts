SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: October 24, 2005
-- Description:	Create parentage mappings
-- =============================================
ALTER PROCEDURE CreateParentageMap2
AS
BEGIN

	DECLARE @count int
	DECLARE @max int

--	SET NOCOUNT ON

	-- Clear parentage table.
	--
	TRUNCATE TABLE dbo.tbParentage2

	-- Populate parentage table with valid active users.
	--
	INSERT INTO dbo.tbParentage2 (position, parent)
	SELECT DISTINCT position_nbr, reports_to
	FROM Baxter.dbo.tbLoadFile
	WHERE (empl_status IN ('A', 'L', 'P', 'S', 'W') OR empl_status IS NULL OR LEN(empl_status) = 0)
--	AND user_type IN ('E', 'M')
	AND (position_nbr IS NOT NULL AND LEN(position_nbr) > 0)
--	AND empl_rcd = 0

	SET @count = 1

	-- Add orphan's parents to parentage table.
	--
	WHILE (@count > 0)
		BEGIN
--			INSERT INTO dbo.tbParentage2 (position, parent, lvl, isactive)
--			SELECT DISTINCT src.position_nbr, reports_to, -99, 0
			INSERT INTO dbo.tbParentage2 (position, parent)
			SELECT DISTINCT src.position_nbr, reports_to
			FROM dbo.tbLoadFile src
			INNER JOIN (SELECT DISTINCT org.position_nbr, MAX(ISNULL(org.termination_dt, '1/1/1900')) termination_dt
									FROM dbo.tbParentage2 child
									LEFT JOIN dbo.tbParentage2 parent ON child.parent = parent.position
									INNER JOIN dbo.tbLoadFile org ON child.parent = org.position_nbr
									WHERE parent.id IS NULL
									GROUP BY org.position_nbr) filter
			ON src.position_nbr = filter.position_nbr AND ISNULL(src.termination_dt, '1/1/1900') = ISNULL(filter.termination_dt, '1/1/1900')

				SET @count = @@ROWCOUNT
		END

	-- Assign the highest level.
	--
	UPDATE dbo.tbParentage2 SET lvl = 0
	FROM dbo.tbParentage2 
	WHERE position = parent

	-- Populate local variables for loop.
	--
	SELECT @count = MIN(lvl) FROM dbo.tbParentage2 WHERE lvl IS NOT NULL

	-- Loop over parentage table and populate Level until there are no more Nulls in lvl field.
	--
	WHILE (EXISTS (SELECT 1 FROM dbo.tbParentage2
										INNER JOIN dbo.tbParentage2 parent ON dbo.tbParentage2.parent = parent.position AND parent.lvl = @count
										WHERE dbo.tbParentage2.lvl IS NULL))
		BEGIN
			-- Assign lvl to one greater then previous lvl.
			--
			UPDATE dbo.tbParentage2 SET lvl = parent.lvl + 1
			FROM dbo.tbParentage2
			INNER JOIN dbo.tbParentage2 parent ON dbo.tbParentage2.parent = parent.position AND parent.lvl = @count
			WHERE dbo.tbParentage2.lvl IS NULL

			-- Increment counter.
			--
			SET @count = @count + 1
		END


	-- Assign highest level for orphans.
	--
	UPDATE dbo.tbParentage2 SET lvl = -100
	FROM dbo.tbParentage2
	INNER JOIN (SELECT parent.parent
							FROM dbo.tbParentage2 child
							RIGHT JOIN (SELECT DISTINCT parent FROM dbo.tbParentage2 WHERE lvl IS NULL AND parent IS NOT NULL) parent
							ON child.position = parent.parent
							WHERE child.id IS NULL) orphans
	ON dbo.tbParentage2.parent = orphans.parent
	WHERE dbo.tbParentage2.lvl IS NULL

	-- Reset counter.
	--
	SELECT @count = MIN(lvl) FROM dbo.tbParentage2 WHERE lvl IS NOT NULL

	-- Loop over parentage table and populate Level until there are no more Nulls in lvl field.
	--
	WHILE (EXISTS (SELECT 1 FROM dbo.tbParentage2
										INNER JOIN dbo.tbParentage2 parent ON dbo.tbParentage2.parent = parent.position AND parent.lvl = @count
										WHERE dbo.tbParentage2.lvl IS NULL))
		BEGIN
			-- Assign lvl to one greater then previous lvl.
			--
			UPDATE dbo.tbParentage2 SET lvl = parent.lvl - 1
			FROM dbo.tbParentage2
			INNER JOIN dbo.tbParentage2 parent ON dbo.tbParentage2.parent = parent.position AND parent.lvl = @count
			WHERE dbo.tbParentage2.lvl IS NULL

			-- Decrement counter.
			--
			SET @count = @count - 1
		END


END
GO
