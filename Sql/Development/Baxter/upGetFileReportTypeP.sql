SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:			Todd Fellini
-- Create date: December 19, 2005
-- Description:	Get data for Payroll file report
-- =============================================
ALTER PROCEDURE dbo.upGetFileReportTypeP
	@CountryCode VARCHAR(5) = 'USA'
AS
BEGIN

	DECLARE @Blank VARCHAR(10)
	DECLARE @PeriodBegin DATETIME
	DECLARE @PeriodEnd DATETIME

	SET NOCOUNT ON

	-- Initialize variables
	SET @Blank = 'NULL'
	SET @PeriodBegin = DATEADD(dd, -7 , CONVERT(DATETIME, CONVERT(VARCHAR, GETDATE(), 101)))
	SET @PeriodEnd = DATEADD(ss, -1 , CONVERT(DATETIME, CONVERT(VARCHAR, GETDATE(), 101)))

	SELECT 
	e.CustEmployeeNum
	,'0'
	,CASE e.LocationSetCode 
		WHEN 'USA01' THEN 'RRP'
		WHEN 'PRI01' THEN 'PRR'
	 END
	,CONVERT(VARCHAR, a.AwardAmtUSD, 0)
	FROM dbo.tbAward a 
	LEFT JOIN dbo.tbEmployee e ON a.RecipEmployeeID = e.EmployeeID
	WHERE a.StatusDateTime BETWEEN @PeriodBegin AND @PeriodEnd
	AND a.AwardType IN ('D', 'G')
	AND a.Status = 'A'
	ORDER BY a.StatusDateTime

END
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

