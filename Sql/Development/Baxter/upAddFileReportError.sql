SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: December 16, 2005
-- Description:	Add error entry to tbFileReportErrors
-- =============================================
CREATE PROCEDURE dbo.upAddFileReportError 
	@Description VARCHAR(500)
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.tbFileReportErrors (Description)
	VALUES (@Description)

END
GO
