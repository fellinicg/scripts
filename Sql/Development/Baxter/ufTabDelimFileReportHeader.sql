SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Todd Fellini
-- Create date: December 14, 2005
-- Description:	Tab delimit column headers for file report
-- =============================================
CREATE FUNCTION dbo.ufTabDelimFileReportHeader 
(
	@FileReportType CHAR(1)
)
RETURNS VARCHAR(5000)
AS
BEGIN
	-- Declare variables.
	DECLARE @Result VARCHAR(5000)
	DECLARE @tblFileReportColumn TABLE (ColumnHdr VARCHAR(40))

	-- Initialize variables.
	SET @Result = ''

	-- Insert column headers into local table variable.
	INSERT INTO @tblFileReportColumn
	SELECT DisplayName
	FROM dbo.tbFileReportColumn
	WHERE FileReportType = @FileReportType
	ORDER BY PostionNum

	-- Populate result variable.
	WHILE (SELECT COUNT(ColumnHdr) FROM @tblFileReportColumn) > 0
		BEGIN
			-- Assign value concatenated with tab to result variable.
			SELECT TOP 1 @Result = @Result + ColumnHdr + CHAR(9) 
			FROM @tblFileReportColumn
			
			-- Remove top row from table variable.
			DELETE FROM @tblFileReportColumn
			WHERE ColumnHdr = (SELECT TOP 1 ColumnHdr FROM @tblFileReportColumn)
		END

	-- Remove final tab.
	SET @Result = SUBSTRING(@Result, 1, LEN(@Result)-1)

	-- Return the result of the function
	RETURN @Result

END
GO

