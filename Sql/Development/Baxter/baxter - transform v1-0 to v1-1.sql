-- ============================================================
-- Title: BAXTER TRANSFORMATION
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060803
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20060803
-- Production Release: (1-1)
-- Notes: first interim release after launch
-- Upgrade performed: ***** 20060803 19:21:00*****
-- Platform: SQL2KPRODZEBRA
-- ============================================================

SET NOCOUNT ON

-- CHANGED TABLES
PRINT CHAR(13) + CHAR(10) + '**** CHANGED TABLES ****'
GO
	-- tbAwardBehavior 
PRINT 'CHANGING TABLE: tbAwardBehavior'
GO
ALTER TABLE dbo.tbAwardBehavior
	ADD Retired bit NOT NULL DEFAULT 0
GO
IF EXISTS (SELECT name FROM dbo.sysindexes WHERE name = N'akAwardBehaviorDescr' and id=object_id(N'[dbo].[tbAwardBehavior]') )
ALTER TABLE [dbo].[tbAwardBehavior] DROP CONSTRAINT [akAwardBehaviorDescr]
GO
	-- tbOutboundEmail 
PRINT 'CHANGING TABLE: tbOutboundEmail'
GO
ALTER TABLE tbOutboundEmail DROP CONSTRAINT fkOutboundEmailEmailTemplate
GO
ALTER TABLE tbOutboundEmail ADD
	EmailTemplateID int NOT NULL CONSTRAINT dfDropMe DEFAULT 0
GO
ALTER TABLE tbOutboundEmail DROP CONSTRAINT dfDropMe
GO
	-- tbEmailTemplate 
PRINT 'CHANGING TABLE: tbEmailTemplate'
GO
ALTER TABLE tbEmailTemplate DROP CONSTRAINT pkEmailTemplate
GO
ALTER TABLE tbEmailTemplate ADD
	EmailTemplateID int IDENTITY(1,1) NOT NULL,
	EmailSubType char(1) NULL
GO
ALTER TABLE tbEmailTemplate ADD CONSTRAINT pkEmailTemplate
	PRIMARY KEY(EmailTemplateID)
GO

-- CHANGED DATA
PRINT CHAR(13) + CHAR(10) + '**** CHANGED DATA ****'
GO
	-- tbOutboundEmail 
PRINT 'UPDATING tbOutboundEmail FIELD: EmailTemplateID'
GO
UPDATE tbOutboundEmail SET EmailTemplateID = t.EmailTemplateID
FROM tbOutboundEmail o
	INNER JOIN tbEmailTemplate t ON (o.EmailType = t.EmailType AND o.LanguageCode = t.LanguageCode)
GO
IF (@@ROWCOUNT <> (SELECT COUNT(*) FROM tbOutboundEmail))
BEGIN
	PRINT 'Unable to update all refs in tbOutboundEmail'
	RETURN
END
GO
PRINT 'ALTERING tbOutboundEmail ADD CONSTRAINT: fkOutboundEmailEmailTemplate'
GO
ALTER TABLE tbOutboundEmail ADD CONSTRAINT fkOutboundEmailEmailTemplate
	FOREIGN KEY (EmailTemplateID)
	REFERENCES tbEmailTemplate (EmailTemplateID)
GO
PRINT 'ALTERING tbOutboundEmail DROPPING: EmailType'
GO
ALTER TABLE tbOutboundEmail DROP COLUMN EmailType
GO
PRINT 'ALTERING tbOutboundEmail DROPPING: LanguageCode'
GO
ALTER TABLE tbOutboundEmail DROP COLUMN LanguageCode
GO
	-- tbAwardBehavior 
PRINT 'UPDATING tbAwardBehavior FIELD: Retired'
GO
UPDATE dbo.tbAwardBehavior SET Retired = 1 WHERE BehaviorID IN (6,7,8)
GO
PRINT 'INSERTING tbAwardBehavior RECORDS'
GO
INSERT INTO dbo.tbAwardBehavior (BehaviorID, Title, Description, ResourceFileCode)
VALUES (9, 'Teamwork', 'We work openly and supportively in teams, aiming towards common goals.  We form teams with our customers and suppliers to respond quickly to changing customer needs.  We have fun working with each other and we take pride in our joint accomplishments.', 'teamwork')
GO
INSERT INTO dbo.tbAwardBehavior (BehaviorID, Title, Description, ResourceFileCode)
VALUES (10, 'Innovation', 'Innovation is the key to creating new sources of value for our customers and shareholders.  We must quickly transform new technologies and new ideas into products and services that exceed customer expectations and improve our own effectiveness.  We do this by valuing and rewarding creativity, diverse thinking styles and intelligent risk-taking.  We act to maximize potential success, rather than tominimize potential failure.', 'innovation')
GO
	-- tbEmailTemplate 
PRINT 'INSERTING tbEmailTemplate RECORDS'
GO
SET IDENTITY_INSERT tbEmailTemplate ON
GO
INSERT INTO dbo.tbEmailTemplate (LanguageCode,EmailType,EmailSubType,EmailTemplateID,ToName,ToEmailAddressList,FromName,FromEmailAddress,ReplyToEmailAddress,CCEmailAddressList,BCCEmailAddressList,MessageSubjectTemplate,SMTPContentType,SMTPPriority,MessageBodyTemplate)
VALUES('ENG','A','D','23','<RecipientFullName>','<RecipientEmailAddress>','Baxter Award Headquarters','Awards@BaxterRewards.com','Awards@BaxterRewards.com','<GiverEmailAddress>','brose@madisonpg.com','New Baxter Award!','text/html','NORMAL','')
GO
INSERT INTO dbo.tbEmailTemplate (LanguageCode,EmailType,EmailSubType,EmailTemplateID,ToName,ToEmailAddressList,FromName,FromEmailAddress,ReplyToEmailAddress,CCEmailAddressList,BCCEmailAddressList,MessageSubjectTemplate,SMTPContentType,SMTPPriority,MessageBodyTemplate)
VALUES('ENG','A','G','24','<RecipientFullName>','<RecipientEmailAddress>','Baxter Award Headquarters','Awards@BaxterRewards.com','Awards@BaxterRewards.com','<GiverEmailAddress>','brose@madisonpg.com','New Baxter Award!','text/html','NORMAL','')
GO
INSERT INTO dbo.tbEmailTemplate (LanguageCode,EmailType,EmailSubType,EmailTemplateID,ToName,ToEmailAddressList,FromName,FromEmailAddress,ReplyToEmailAddress,CCEmailAddressList,BCCEmailAddressList,MessageSubjectTemplate,SMTPContentType,SMTPPriority,MessageBodyTemplate)
VALUES('ENG','A','R','25','<RecipientFullName>','<RecipientEmailAddress>','Baxter Award Headquarters','Awards@BaxterRewards.com','Awards@BaxterRewards.com','<GiverEmailAddress>','brose@madisonpg.com','New Baxter Award!','text/html','NORMAL','')
GO
INSERT INTO dbo.tbEmailTemplate (LanguageCode,EmailType,EmailSubType,EmailTemplateID,ToName,ToEmailAddressList,FromName,FromEmailAddress,ReplyToEmailAddress,CCEmailAddressList,BCCEmailAddressList,MessageSubjectTemplate,SMTPContentType,SMTPPriority,MessageBodyTemplate)
VALUES('ESP','A','D','26','<RecipientFullName>','<RecipientEmailAddress>','Jefaturas De la Concesi�n de Baxter','Awards@BaxterRewards.com','Awards@BaxterRewards.com','<GiverEmailAddress>','brose@madisonpg.com','�Reconocimiento Nuevo de Baxter!','text/html','NORMAL','')
GO
INSERT INTO dbo.tbEmailTemplate (LanguageCode,EmailType,EmailSubType,EmailTemplateID,ToName,ToEmailAddressList,FromName,FromEmailAddress,ReplyToEmailAddress,CCEmailAddressList,BCCEmailAddressList,MessageSubjectTemplate,SMTPContentType,SMTPPriority,MessageBodyTemplate)
VALUES('ESP','A','G','27','<RecipientFullName>','<RecipientEmailAddress>','Jefaturas De la Concesi�n de Baxter','Awards@BaxterRewards.com','Awards@BaxterRewards.com','<GiverEmailAddress>','brose@madisonpg.com','�Reconocimiento Nuevo de Baxter!','text/html','NORMAL','')
GO
INSERT INTO dbo.tbEmailTemplate (LanguageCode,EmailType,EmailSubType,EmailTemplateID,ToName,ToEmailAddressList,FromName,FromEmailAddress,ReplyToEmailAddress,CCEmailAddressList,BCCEmailAddressList,MessageSubjectTemplate,SMTPContentType,SMTPPriority,MessageBodyTemplate)
VALUES('ESP','A','R','28','<RecipientFullName>','<RecipientEmailAddress>','Jefaturas De la Concesi�n de Baxter','Awards@BaxterRewards.com','Awards@BaxterRewards.com','<GiverEmailAddress>','brose@madisonpg.com','�Reconocimiento Nuevo de Baxter!','text/html','NORMAL','')
GO
SET IDENTITY_INSERT tbEmailTemplate OFF
GO
PRINT 'UPDATING tbEmailTemplate FIELD:MessageBodyTemplate'
GO
UPDATE tbEmailTemplate SET MessageBodyTemplate = '<HTML><HEAD><TITLE>Email</TITLE><STYLE><!--.bodytextblack {font-size: 12px;font-weight: normal;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .bodytextboldblack {font-size: 12px;font-weight: bold;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;}--></STYLE></HEAD><BODY><table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td class="bodytextblack">Awarded by: <span class="bodytextboldblack"><GiverFullName></span>
<p class="bodytextboldblack">Congratulations! You have received an award via Baxter Rewards.</p>
<p class="bodytextblack">Award Type: Debit Card<br>
Award Behavior:<br><span class="bodytextboldblack"><AwardBehavior></span></p>
<p class="bodytextblack">The following comments are from the Giver:<br>
<CommentstoRecipient></p>
<p class="bodytextblack">To find out more, please login at <br>
<a href="http://www.baxterrewards.com">http://www.baxterrewards.com</a>.</p>
<p class="bodytextboldblack">Standard Delivery Time:<br>
<span class="bodytextblack">Delivery time for Baxter Rewards debit card is </span><span class="bodytextboldblack">5 to 7 business days via first class mail.*</span></p>
<p class="bodytextblack">*Note:  The Baxter Rewards debit card is a reloadable card product good for 3 years.  <span class="bodytextboldblack">Please do not discard.</span></p>
<p class="bodytextboldblack">***Please do not reply to this email***</p>
<p class="bodytextblack">&nbsp;</p></td></tr></table></BODY></HTML>' 
WHERE EmailType = 'A' AND LanguageCode = 'ENG' AND EmailSubType='D'
GO
UPDATE tbEmailTemplate SET MessageBodyTemplate = '<HTML><HEAD><TITLE>Email</TITLE><STYLE><!--.bodytextblack {font-size: 12px;font-weight: normal;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .bodytextboldblack {font-size: 12px;font-weight: bold;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;}--></STYLE></HEAD><BODY><table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td class="bodytextblack">Reconocimiento de:<span class="bodytextboldblack"><GiverFullName></span>
<p class="bodytextboldblack">�Felicitaciones! Usted ha recibido un reconocimiento v�a Baxter Rewards. </p>
<p class="bodytextblack">Tipo de Recompensa: Tarjeta de D�bito<br>
Conducta Premiada:<br>
<span class="bodytextboldblack"><AwardBehavior></span></p>
<p class="bodytextblack">Mensaje:<br>
<CommentstoRecipient></p>
<p class="bodytextblack">Para descubrir m�s, por favor de entrar a :<br>
<a href="http://www.baxterrewards.com">http://www.baxterrewards.com</a>.</p>
<p class="bodytextboldblack">Plazo de entraga de premios:<br>
<span class="bodytextblack">El plazo de entrega de la tarjeta debit de Baxter Rewards es de </span><span class="bodytextboldblack">5 - 7 d�as laborables enviar por correo.*</span></p>
<p class="bodytextblack">*Nota Importante:  La tarjeta debit de Baxter Rewards es regargable y valida por 3 a�os.  <span class="bodytextboldblack">Por Favor de no desechar.</span></p>
<p class="bodytextboldblack">***No conteste por favor a este email***</p>
<p class="bodytextblack">&nbsp;</p></td></tr></table></BODY></HTML>' 
WHERE EmailType = 'A' AND LanguageCode = 'ESP' AND EmailSubType='D'
GO
UPDATE tbEmailTemplate SET MessageBodyTemplate = '<HTML><HEAD><TITLE>Email</TITLE><STYLE><!--.bodytextblack {font-size: 12px;font-weight: normal;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .bodytextboldblack {font-size: 12px;font-weight: bold;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;}--></STYLE></HEAD><BODY><table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td class="bodytextblack">Awarded by: <span class="bodytextboldblack"><GiverFullName></span>
<p class="bodytextboldblack">Congratulations! You have received an award via Baxter Rewards.</p>
<p class="bodytextblack">Award Type: Gift Certificate<br>Award Behavior:<br>
<span class="bodytextboldblack"><AwardBehavior></span></p>
<p class="bodytextblack">The following comments are from the Giver:<br>
<CommentstoRecipient></p>
<p class="bodytextblack">To find out more, please login at <br>
<a href="http://www.baxterrewards.com">http://www.baxterrewards.com</a>.</p>
<p class="bodytextboldblack">Standard Delivery Time:<br>
<span class="bodytextblack">Delivery time for gift certificates is </span><span class="bodytextboldblack">2 business weeks via first class mail.</span></p>
<p class="bodytextboldblack">***Please do not reply to this email***</p>
<p class="bodytextblack">&nbsp;</p>
</td></tr></table></BODY></HTML>' 
WHERE EmailType = 'A' AND LanguageCode = 'ENG' AND EmailSubType='G'
GO
UPDATE tbEmailTemplate SET MessageBodyTemplate = '<HTML><HEAD><TITLE>Email</TITLE><STYLE><!--.bodytextblack {font-size: 12px;font-weight: normal;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .bodytextboldblack {font-size: 12px;font-weight: bold;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;}--></STYLE></HEAD><BODY><table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td class="bodytextblack">Reconocimiento de: <span class="bodytextboldblack"><GiverFullName></span>
<p class="bodytextboldblack">�Felicitaciones! Usted ha recibido un reconocimiento v�a Baxter Rewards. </p>
<p class="bodytextblack">Tipo de Recompensa: Certificado de Regalo<br>
Conducta Premiada:<br>
<span class="bodytextboldblack"><AwardBehavior></span></p>
<p class="bodytextblack">Mensaje:<br>
<CommentstoRecipient></p>
<p class="bodytextblack">Para descubrir m�s, por favor de entrar a :<br>
<a href="http://www.baxterrewards.com">http://www.baxterrewards.com</a>.</p>
<p class="bodytextboldblack">Plazo de entraga de premios:<br>
<span class="bodytextblack">El plazo de entrega de las tarjetas de regalo es de </span><span class="bodytextboldblack">10 d�as laborables</span> enviar por correo.</p>
<p class="bodytextboldblack">***No conteste por favor a este email***</p>
<p class="bodytextblack">&nbsp;</p></td></tr></table></BODY></HTML>' 
WHERE EmailType = 'A' AND LanguageCode = 'ESP' AND EmailSubType='G'
GO
UPDATE tbEmailTemplate SET MessageBodyTemplate = '<HTML><HEAD><TITLE>Email</TITLE><STYLE><!--.bodytextblack {font-size: 12px;font-weight: normal;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .bodytextboldblack {font-size: 12px;font-weight: bold;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;}--></STYLE></HEAD><BODY><table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td class="bodytextblack">Awarded by: <span class="bodytextboldblack"><GiverFullName></span>
<p class="bodytextboldblack">Congratulations! You have received an award via Baxter Rewards.</p>
<p class="bodytextblack">Award Type: Recognition<br>Award Behavior:<br>
<span class="bodytextboldblack"><AwardBehavior></span></p>
<p class="bodytextblack">The following comments are from the Giver:<br>
<CommentstoRecipient></p>
<p class="bodytextblack">To find out more, please login at <br><a href="http://www.baxterrewards.com">http://www.baxterrewards.com</a>.</p>
<p class="bodytextboldblack">***Please do not reply to this email***</p>
<p class="bodytextblack">&nbsp;</p>
</td></tr></table></BODY></HTML>' 
WHERE EmailType = 'A' AND LanguageCode = 'ENG' AND EmailSubType='R'
GO
UPDATE tbEmailTemplate SET MessageBodyTemplate = '<HTML><HEAD><TITLE>Email</TITLE><STYLE><!--.bodytextblack {font-size: 12px;font-weight: normal;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .bodytextboldblack {font-size: 12px;font-weight: bold;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;}--></STYLE></HEAD><BODY><table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td class="bodytextblack">Reconocimiento de: <span class="bodytextboldblack"><GiverFullName></span>
<p class="bodytextboldblack">�Felicitaciones! Usted ha recibido un reconocimiento v�a Baxter Rewards. </p>
<p class="bodytextblack">Tipo de Recompensa: Reconocimiento<br>
Conducta Premiada:<br>
<span class="bodytextboldblack"><AwardBehavior></span></p>
<p class="bodytextblack">Mensaje:<br>
<CommentstoRecipient></p>
<p class="bodytextblack">Para descubrir m�s, por favor de entrar a :<br>
<a href="http://www.baxterrewards.com">http://www.baxterrewards.com</a>.</p>
<p class="bodytextboldblack">***No conteste por favor a este email***</p>
<p class="bodytextblack">&nbsp;</p></td></tr></table></BODY></HTML>' 
WHERE EmailType = 'A' AND LanguageCode = 'ESP' AND EmailSubType='R'
GO
UPDATE tbEmailTemplate SET MessageBodyTemplate = '<HTML><HEAD><TITLE>Email</TITLE><STYLE><!--.bodytextblack {font-size: 12px;font-weight: normal;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .bodytextboldblack {font-size: 12px;font-weight: bold;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;}--></STYLE></HEAD><BODY><table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td class="bodytextblack">Award Recipient: <span class="bodytextboldblack"><RecipientFullName></span>
<p class="bodytextblack">Denied by: <span class="bodytextboldblack"><ReviewingMgrFullName></span></p>
<p class="bodytextblack">Award Type: <AwardType><br>
Award Behavior:<br>
<span class="bodytextboldblack"><AwardBehavior></span></p>
<p class="bodytextblack">To find out more, please login at <br>
<a href="http://www.baxterrewards.com">http://www.baxterrewards.com</a>.</p>
<p class="bodytextboldblack">***Please do not reply to this email***</p>
<p class="bodytextblack">&nbsp;</p></td></tr></table></BODY></HTML>' 
WHERE EmailType = 'D' AND LanguageCode = 'ENG'
GO
UPDATE tbEmailTemplate SET MessageBodyTemplate = '<HTML><HEAD><TITLE>Email</TITLE><STYLE><!--.bodytextblack {font-size: 12px;font-weight: normal;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .bodytextboldblack {font-size: 12px;font-weight: bold;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;}--></STYLE></HEAD><BODY><table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td class="bodytextblack">Recipiente del Reconocimiento: <span class="bodytextboldblack"><RecipientFullName></span>
<p class="bodytextblack">Nega Por: <span class="bodytextboldblack"><ReviewingMgrFullName></span></p>
<p class="bodytextblack">Tipo de Recompensa: <AwardType><br>
Conducta Premiada:<br>
<span class="bodytextboldblack"><AwardBehavior></span></p>
<p class="bodytextblack">Para descubrir m�s, por favor de entrar a :<br>
<a href="http://www.baxterrewards.com">http://www.baxterrewards.com</a>.</p>
<p class="bodytextboldblack">***No conteste por favor a este email***</p>
<p class="bodytextblack">&nbsp;</p></td></tr></table></BODY></HTML>' 
WHERE EmailType = 'D' AND LanguageCode = 'ESP'
GO
UPDATE tbEmailTemplate SET MessageBodyTemplate = '<HTML><HEAD><TITLE>Email</TITLE><STYLE><!--.bodytextblack {font-size: 12px;font-weight: normal;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .bodytextboldblack {font-size: 12px;font-weight: bold;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .pendingemailtable  {border-top: solid 1px black;border-left: solid 1px black;border-collapse: collapse;} .pendingemailtable td {border-right: solid 1px black;border-bottom: solid 1px black;padding: 8px;margin: 0px;}--></STYLE></HEAD><BODY><table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td class="bodytextblack">You have the following awards pending your review in the Baxter Rewards program.<br>
<p class="bodytextblack">Awards given <span class="bodytextboldblack">by</span> your direct reports:</p>
<TableGivenByDirRpts><p class="bodytextblack">Awards given <span class="bodytextboldblack">to</span> your direct reports:</p>
<TableReceivedByDirRpts><p class="bodytextblack">To deny any award, please login to Baxter Rewards at <a href="http://www.baxterrewards.com">http://www.baxterrewards.com</a>.</p>
<p class="bodytextblack">If no action to deny an award is taken by the recipient''s or giver''s managers, awards will be automatically approved 2 business days after initiation date.</p>
<p class="bodytextboldblack">***Please do not reply to this email***</p>
<p class="bodytextblack">&nbsp;</p></td></tr></table></BODY></HTML>' 
WHERE EmailType = 'P' AND LanguageCode = 'ENG'
GO
UPDATE tbEmailTemplate SET MessageBodyTemplate = '<HTML><HEAD><TITLE>Email</TITLE><STYLE><!--.bodytextblack {font-size: 12px;font-weight: normal;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .bodytextboldblack {font-size: 12px;font-weight: bold;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .pendingemailtable  {border-top: solid 1px black;border-left: solid 1px black;border-collapse: collapse;} .pendingemailtable td {border-right: solid 1px black;border-bottom: solid 1px black;padding: 8px;margin: 0px;}--></STYLE></HEAD><BODY><table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td class="bodytextblack">Usted tiene los siguientes reconocimientos esperando su revision en el programa de Baxter Rewards.<br>
<p class="bodytextblack">Reconocimientos dado <span class="bodytextboldblack">por</span> su empleados:</p>
<TableGivenByDirRpts><p class="bodytextblack">Reconocimiento dado <span class="bodytextboldblack">a</span> su empleados:</p>
<TableReceivedByDirRpts><p class="bodytextblack">Para negar cualquier reconocimiento, por favor de entrar a las paginas de Baxter Rewards: <br>
<a href="http://www.baxterrewards.com">http://www.baxterrewards.com</a>. </p>
<p class="bodytextblack">Los reconocimientos ser�n aprobados autom�ticamente 2 d�as de trabajo despu�s de la fecha de la iniciaci�n en el caso que no se tome ninguna accion para negar los reconocimientos de parte del jefe del recipiente o el jefe del donante.</p>
<p class="bodytextboldblack">***No conteste por favor a este email***</p>
<p class="bodytextblack">&nbsp;</p></td></tr></table></BODY></HTML>' 
WHERE EmailType = 'P' AND LanguageCode = 'ESP'
GO
PRINT 'UPDATING tbEmailTemplate FIELDS:FromName,MessageSubjectTemplate'
GO
UPDATE dbo.tbEmailTemplate 
SET FromName = 'Baxter Award Headquarters',
MessageSubjectTemplate = 'Baxter Denied Award'
WHERE EmailType = 'D'
AND LanguageCode = 'ENG'
GO
UPDATE dbo.tbEmailTemplate 
SET FromName = 'Jefaturas De la Concesi�n de Baxter',
MessageSubjectTemplate = 'Reconocimiento de Baxter Negado'
WHERE EmailType = 'D'
AND LanguageCode = 'ESP'
GO
UPDATE dbo.tbEmailTemplate 
SET FromName = 'Baxter Award Headquarters',
MessageSubjectTemplate = 'Baxter Award Pending Your Review'
WHERE EmailType = 'P'
AND LanguageCode = 'ENG'
GO
UPDATE dbo.tbEmailTemplate 
SET FromName = 'Jefaturas De la Concesi�n de Baxter',
MessageSubjectTemplate = 'Reconocimiento de Baxter Esperando Su Revisi�n'
WHERE EmailType = 'P'
AND LanguageCode = 'ESP'
GO

-- CHANGED FUNCTIONS
PRINT CHAR(13) + CHAR(10) + '**** CHANGED FUNCTIONS ****'
GO
PRINT 'ALTERING FUNC: ufDirectReportsPendingAwardsTable'
GO
ALTER FUNCTION dbo.ufDirectReportsPendingAwardsTable (@EmployeeID int, @GivRec char(1), @Date datetime)
RETURNS varchar(8000)
AS
BEGIN
DECLARE
	@HTMLTable varchar(8000),
	@EmpLangCode char(3),
	@PendingAwardStatus char(1),
	@RecipNameTitle varchar(40),
	@GiverNameTitle varchar(40),
	@AwardBehaviorTitle varchar(40),
	@AwardAmtTitle varchar(40),
	@ApproveDateTitle varchar(40),
	@CurrAwardID int,
	@TableRecordLimit int,
	@TableOpenTag varchar(40),
	@Today datetime, 
	@Yesterday datetime

	SET @PendingAwardStatus = 'P'
	SET @TableRecordLimit = 30  -- MORE RECORDS WOULD RESULT IN TRUNCATED TABLE
	SET @TableOpenTag = '<TABLE CLASS="pendingemailtable">'

	-- GET DATE FILTERS
	SET @Today = CONVERT(VARCHAR, @Date, 101)
	SET @Yesterday = CONVERT(VARCHAR, (DATEADD(DAY, -1, @Date)), 101)

	-- CHECK INPUTS
	SELECT @EmpLangCode = dbo.ufAccountPreferredLanguage(@EmployeeID)
	IF (@EmpLangCode = 'XXX')  -- FUNC FAILURE INDICATOR
		RETURN(@HTMLTable)
	IF (@GivRec NOT IN ('G', 'R'))
		RETURN(@HTMLTable)

	-- IDENTIFY QUALIFIED AWARDS
	DECLARE @QualAwd TABLE (AwardID int NOT NULL)
	IF (@GivRec = 'R')
		INSERT @QualAwd(AwardID)
		SELECT a.AwardID
		FROM tfEmployeeDirectReports(@EmployeeID) dr
			INNER JOIN tbAward a ON (dr.EmployeeID = a.RecipEmployeeID AND a.Status = @PendingAwardStatus
																AND a.RecordDateTime BETWEEN @Yesterday AND @Today)
	ELSE
		INSERT @QualAwd(AwardID)
		SELECT a.AwardID
		FROM tfEmployeeDirectReports(@EmployeeID) dr
			INNER JOIN tbAward a ON (dr.EmployeeID = a.GiverEmployeeID AND a.Status = @PendingAwardStatus
																AND a.RecordDateTime BETWEEN @Yesterday AND @Today)

	-- BUILD TABLE HEADER
	SET @RecipNameTitle = CASE @EmpLangCode WHEN 'ESP' THEN 'Recipiente' ELSE 'Award Recipient' END
	SET @GiverNameTitle = CASE @EmpLangCode WHEN 'ESP' THEN 'Donante' ELSE 'Giver' END
	SET @AwardBehaviorTitle = CASE @EmpLangCode WHEN 'ESP' THEN 'Conducta Premiada' ELSE 'Award Behavior' END
	SET @AwardAmtTitle = CASE @EmpLangCode WHEN 'ESP' THEN 'Cantidade Recompensa' ELSE 'Award Amount' END
	SET @ApproveDateTitle = CASE @EmpLangCode WHEN 'ESP' THEN 'Fecha Autom�tica de Aprobaci�n' ELSE 'Automatic Approval Date' END
	SET @HTMLTable = @TableOpenTag + '<TR>'
		+ '<TD>' + @RecipNameTitle + '</TD>'
		+ '<TD>' + @GiverNameTitle + '</TD>'
		+ '<TD>' + @AwardBehaviorTitle + '</TD>'
		+ '<TD>' + @AwardAmtTitle + '</TD>'
		+ '<TD>' + @ApproveDateTitle + '</TD>'
		+ '</TR>'

	-- BUILD TABLE BODY
	SELECT @CurrAwardID = MIN(AwardID) FROM @QualAwd
	IF (ISNULL(@CurrAwardID, 0) = 0)
	BEGIN
		SET @HTMLTable = NULL
		RETURN(@HTMLTable)
	END
	WHILE (ISNULL(@CurrAwardID, 0) <> 0 AND @TableRecordLimit > 0)
	BEGIN
		-- ADD ROW FOR CURR AWARD
		SELECT @HTMLTable = @HTMLTable + '<TR>'
			+ '<TD>' + dbo.ufEmployeeGUFullName(a.RecipEmployeeID) + '</TD>'
			+ '<TD>' + dbo.ufEmployeeGUFullName(a.GiverEmployeeID) + '</TD>'
			+ '<TD>' + b.Title + '</TD>'
			+ '<TD>' + '$' + CONVERT(varchar, a.AwardAmtUSD, 1) + '</TD>'
			+ '<TD>' + CONVERT(varchar, dbo.ufAwardApprovalDate(a.AwardID), 101) + '</TD>'
			+ '</TR>'
		FROM @QualAwd q
			INNER JOIN tbAward a ON (q.AwardID = a.AwardID)
			INNER JOIN tbAwardBehavior b ON (a.BehaviorID = b.BehaviorID)
		WHERE q.AwardID = @CurrAwardID
		-- GET NEXT AWARD
		SELECT @CurrAwardID = MIN(AwardID) FROM @QualAwd WHERE AwardID > @CurrAwardID
	END

	-- CLOSE TABLE
	SET @HTMLTable = @HTMLTable + '</TABLE>'

	-- CHECK RESULT
	IF (LEFT(@HTMLTable, LEN(@TableOpenTag)) <> @TableOpenTag OR RIGHT(@HTMLTable, 8) <> '</TABLE>')
		SET @HTMLTable = NULL

	RETURN(@HTMLTable)

END
GO

-- CHANGED PROCS
PRINT CHAR(13) + CHAR(10) + '**** CHANGED PROCS ****'
GO
  --upSendAwardApprovedEmail
PRINT 'ALTERING PROC: upSendAwardApprovedEmail'
GO
ALTER PROCEDURE dbo.upSendAwardApprovedEmail
	@AwardID int,
	@IsSuccess bit OUTPUT
AS
DECLARE
	@RecipientFullName varchar(80),
	@RecipientEmailAddress varchar(120),
	@GiverFullName varchar(80),
	@GiverEmailAddress varchar(120),
	@CommentsToRecipient varchar(300),
	@ToEmailAddressList varchar(400),
	@FromName varchar(80),
	@FromEmailAddress varchar(120),
	@ReplyToEmailAddress varchar(120),
	@CCEmailAddressList varchar(400),
	@BCCEmailAddressList varchar(40),
	@MessageSubjectTemplate varchar(120),
	@MessageBodyTemplate varchar(7000),
	@SMTPContentType varchar(10),
	@SMTPPriority varchar(6),
	@AwardType varchar(20),
	@AwardTypeCode char(1),
	@AwardBehavior varchar(50),
	@RecipEmployeeID int,
	@GiverEmployeeID int,
	@LanguageCode char(3),
	@EmailID int,
	@EmailType char(1),
	@PendingEmailStatus char(1),
	@ApprovedAwardStatus char(1),
	@EmailTemplateID int

	/* ********************************************************************************************************* */
	/* 2006-01-17 JMC																																														*/
	/*								This proc is intended for use by other procedures only. It has no transacation or failsafe */
	/*								control. It is the responsibility of the calling process to evaluate the outcome of a call */
	/*								to this procedure and rollback the transaction if necessary. This procedure was written		*/
	/*								this way to avoid transaction count mismatches where calling procedures also require			 */
	/*								consistent transaction control.																														*/
	/* ********************************************************************************************************* */

	SET NOCOUNT ON

	-- VALIDATION
		-- VAIDATE AWARD/USERS/EMAILS
	SELECT @RecipEmployeeID = RecipEmployeeID, @GiverEmployeeID = GiverEmployeeID FROM tbAward WHERE AwardID = @AwardID
	IF (dbo.ufEmployeeHasEmailAddress(@RecipEmployeeID) <> 1)
		RETURN
	-- GET RECIPIENT LANGUAGE CODE
	SELECT @LanguageCode = dbo.ufAccountPreferredLanguage(@RecipEmployeeID)
	IF (@LanguageCode = 'XXX')	-- FUNC FAILURE INDICATOR
		RETURN

	--SET VARS
	SET @IsSuccess = 0
	SET @EmailType = 'A'	 -- AWARD APPROVAL
	SET @ApprovedAwardStatus = 'A' -- APPROVED STATUS
	SET @PendingEmailStatus = 'P' -- PENDING EMAIL STATUS

	-- GET GIVER AND RECIPIENT INFO
	SELECT
		@RecipientFullName = dbo.ufEmployeeGUFullName(@RecipEmployeeID),
		@RecipientEmailAddress = r.WorkEmailAddress,
		@GiverFullName = dbo.ufEmployeeGUFullName(@GiverEmployeeID),
		@GiverEmailAddress = g.WorkEmailAddress,
		@CCEmailAddressList = g.WorkEmailAddress,
		@CommentsToRecipient = a.GiverComments,
		@AwardType = t.Description,
		@AwardTypeCode = t.AwardType,
		@AwardBehavior = b.Title
	FROM tbAward a
		INNER JOIN tbEmployee g ON a.GiverEmployeeID = g.EmployeeID
		INNER JOIN tbEmployee r ON a.RecipEmployeeID = r.EmployeeID
		INNER JOIN tbAwardType t ON a.AwardType = t.AwardType
		INNER JOIN tbAwardBehavior b ON a.BehaviorID = b.BehaviorID
	WHERE a.AwardID = @AwardID
		AND a.Status = @ApprovedAwardStatus
	IF (@@ROWCOUNT = 0)
	BEGIN
		RETURN
	END

	-- GET TEMPLATE FOR AWARD TYPE
	SELECT
		@ToEmailAddressList = @RecipientFullName	 + ' <' + @RecipientEmailAddress + '>',
		@FromName = FromName,
		@FromEmailAddress = FromEmailAddress,
		@ReplyToEmailAddress = ReplyToEmailAddress,
		@CCEmailAddressList = @GiverEmailAddress,
		@BCCEmailAddressList = BCCEmailAddressList,
		@MessageSubjectTemplate = MessageSubjectTemplate,
		@MessageBodyTemplate = MessageBodyTemplate,
		@SMTPContentType = SMTPContentType,
		@SMTPPriority = SMTPPriority,
		@EmailTemplateID = EmailTemplateID
	FROM tbEmailTemplate
	WHERE EmailType = @EmailType
		AND LanguageCode = @LanguageCode
		AND EmailSubType = @AwardTypeCode
	IF (@@ROWCOUNT = 0)
			RETURN

	-- BUILD MESSAGE BODY
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<GiverFullName>', @GiverFullName)
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<AwardType>', @AwardType)
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<AwardBehavior>', @AwardBehavior)
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<CommentsToRecipient>', @CommentsToRecipient)

	-- CREATE tbOutboundEmail RECORD
	INSERT tbOutboundEmail(EmailTemplateID, ToEmailAddressList, FromName, FromEmailAddress,
		ReplyToEmailAddress, CCEmailAddressList, BCCEmailAddressList, MessageSubject, MessageBody, SMTPContentType,
		SMTPPriority, RecordDateTime, Status, StatusDateTime, FromEmployeeID, ToEmployeeID, AwardID)
	VALUES(@EmailTemplateID, @ToEmailAddressList, @FromName, @FromEmailAddress,
		@ReplyToEmailAddress, @CCEmailAddressList, @BCCEmailAddressList, @MessageSubjectTemplate, @MessageBodyTemplate, @SMTPContentType,
		@SMTPPriority, CURRENT_TIMESTAMP, @PendingEmailStatus, CURRENT_TIMESTAMP, @GiverEmployeeID, @RecipEmployeeID, @AwardID)
	SET @EmailID = @@IDENTITY
	IF (ISNULL(@EmailID, 0) = 0)
		RETURN

	-- QUEUE MESSAGE
	EXEC dbo.upSendOutboundEmail
		@EmailID = @EmailID,
		@IsSuccess = @IsSuccess OUTPUT

GO
PRINT 'ALTERING PROC: upSendPendingAwardsEmail'
GO
ALTER PROCEDURE dbo.upSendPendingAwardsEmail
	@EmployeeID int,
	@IsSuccess bit OUTPUT
AS
DECLARE
	@EmailType char(1),
	@EmpLangCode char(3),
	@EmployeeFullName varchar(80),
	@EmployeeEmailAddress varchar(120),
	@MessageBodyTemplate varchar(6000),
	@MessageSubjectTemplate varchar(120),
	@PendingEmailStatus char(1),
	@SMTPContentType varchar(10),
	@SMTPPriority varchar(6),
	@ToEmailAddressList varchar(400),
	@FromName varchar(80),
	@FromEmailAddress varchar(120),
	@ReplyToEmailAddress varchar(120),
	@CCEmailAddressList varchar(400),
	@BCCEmailAddressList varchar(400),
	@EmailID int,
	@EmailTemplateID int

	SET NOCOUNT ON

	SET @IsSuccess = 0
	SET @EmailType = 'P'  -- PENDING AWARD NOTICE
	SET @PendingEmailStatus = 'P'

	-- CHECK INPUTS
	SELECT @EmpLangCode = dbo.ufAccountPreferredLanguage(@EmployeeID)
	IF (@EmpLangCode = 'XXX')  -- FUNC FAILURE INDICATOR
		RETURN

	-- GET EMPLOYEE INFO
	SELECT
		@EmployeeFullName = dbo.ufEmployeeGUFullName(EmployeeID),
		@EmployeeEmailAddress = WorkEmailAddress
	FROM tbEmployee
	WHERE EmployeeID = @EmployeeID
		AND WorkEmailAddress IS NOT NULL
		AND IsDeleted = 0
	IF (@@ROWCOUNT = 0)
		RETURN
	SET @ToEmailAddressList = @EmployeeFullName + ' <' + @EmployeeEmailAddress + '>'

	-- GET EMAIL TEMPLATE INFO
	SELECT
		@FromName = FromName,
		@FromEmailAddress = FromEmailAddress,
		@ReplyToEmailAddress = ReplyToEmailAddress,
		@CCEmailAddressList = CCEmailAddressList,
		@BCCEmailAddressList = BCCEmailAddressList,
		@MessageSubjectTemplate = MessageSubjectTemplate,
		@MessageBodyTemplate = MessageBodyTemplate,
		@SMTPContentType = SMTPContentType,
		@SMTPPriority = SMTPPriority,
		@EmailTemplateID = EmailTemplateID
	FROM tbEmailTemplate
	WHERE EmailType = @EmailType
		AND LanguageCode = @EmpLangCode
	IF (@@ROWCOUNT = 0)
		RETURN

	-- BUILD MESSAGE BODY
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<TableGivenByDirRpts>', ISNULL(dbo.ufDirectReportsPendingAwardsTable(@EmployeeID, 'G', CURRENT_TIMESTAMP), 'NONE'))
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<TableReceivedByDirRpts>', ISNULL(dbo.ufDirectReportsPendingAwardsTable(@EmployeeID, 'R', CURRENT_TIMESTAMP), 'NONE'))

	-- CREATE tbOutboundEmail RECORD
	INSERT tbOutboundEmail(EmailTemplateID, ToEmailAddressList, FromName, FromEmailAddress,
		ReplyToEmailAddress, CCEmailAddressList, BCCEmailAddressList, MessageSubject, MessageBody, SMTPContentType,
		SMTPPriority, Status, StatusDateTime, ToEmployeeID)
	VALUES(@EmailTemplateID, @ToEmailAddressList, @FromName, @FromEmailAddress,
		@ReplyToEmailAddress, @CCEmailAddressList, @BCCEmailAddressList, @MessageSubjectTemplate, @MessageBodyTemplate,
		@SMTPContentType, @SMTPPriority, @PendingEmailStatus, CURRENT_TIMESTAMP, @EmployeeID)
	SET @EmailID = @@IDENTITY
	IF (ISNULL(@EmailID, 0) = 0)
		RETURN

	-- QUEUE MESSAGE
	EXEC dbo.upSendOutboundEmail
		@EmailID = @EmailID,
		@IsSuccess = @IsSuccess OUTPUT

	SET NOCOUNT OFF
GO
PRINT 'ALTERING PROC: upGetAwardBehaviors'
GO
ALTER PROCEDURE dbo.upGetAwardBehaviors
AS
SET NOCOUNT ON

  SELECT BehaviorID, Title, Description, ResourceFileCode 
	FROM tbAwardBehavior
	WHERE Retired = 0
	ORDER BY BehaviorID

  SET NOCOUNT OFF
GO
PRINT 'ALTERING PROC: upSendAwardDeniedEmail'
GO
ALTER PROCEDURE dbo.upSendAwardDeniedEmail
	@AwardID int,
	@IsSuccess bit OUTPUT
AS
DECLARE
	@RecipEmployeeID int,
	@RecipFullName varchar(80),
	@RecipMgrEmployeeID int,
	@RecipMgrFullName varchar(80),
	@RecipMgrEmailAddress varchar(120),
	@GiverEmployeeID int,
	@GiverFullName varchar(80),
	@GiverEmailAddress varchar(120),
	@GiverMgrEmployeeID int,
	@GiverMgrFullName varchar(80),
	@GiverMgrEmailAddress varchar(120),
	@DeniedByEmployeeID int,
	@DeniedByFullName varchar(120),
	@LanguageCode char(3),
	@ToEmailAddressList varchar(400),
	@FromName varchar(80),
	@FromEmailAddress varchar(120),
	@ReplyToEmailAddress varchar(120),
	@CCEmailAddressList varchar(400),
	@BCCEmailAddressList varchar(400),
	@MessageSubjectTemplate varchar(120),
	@MessageBodyTemplate varchar(7000),
	@SMTPContentType varchar(10),
	@SMTPPriority varchar(6),
	@AwardType varchar(20),
	@AwardBehavior varchar(50),
	@EmailID int,
	@EmailType char(1),
	@PendingEmailStatus char(1),
	@DeniedAwardStatus char(1),
	@EmailTemplateID int

	/* ********************************************************************************************************* */
	/* 2006-01-17 JMC																																														*/
	/*								This proc is intended for use by other procedures only. It has no transacation or failsafe */
	/*								control. It is the responsibility of the calling process to evaluate the outcome of a call */
	/*								to this procedure and rollback the transaction if necessary. This procedure was written		*/
	/*								this way to avoid transaction count mismatches where calling procedures also require			*/
	/*								consistent transaction control.																														*/
	/* ********************************************************************************************************* */

	SET NOCOUNT ON

	-- VALIDATION
		-- GET GIVER/RECIPIENT EMPLOYEEIDS
	SELECT
		@RecipEmployeeID = RecipEmployeeID,
		@RecipMgrEmployeeID = TermRecipMgrEmployeeID,
		@GiverEmployeeID = GiverEmployeeID,
		@GiverMgrEmployeeID = TermGiverMgrEmployeeID,
		@DeniedByEmployeeID = DeniedByEmployeeID
	FROM tbAward
	WHERE AwardID = @AwardID
		-- ENSURE RECIPIENT HAS AN EMAIL ADDRESS IN THE SYSTEM
	IF (dbo.ufEmployeeHasEmailAddress(@GiverEmployeeID) <> 1)
		RETURN
	-- GET RECIPIENT LANGUAGE CODE
	SELECT @LanguageCode = dbo.ufAccountPreferredLanguage(@GiverEmployeeID)
	IF (@LanguageCode = 'XXX')	-- FUNC FAILURE INDICATOR
		RETURN

	--SET VARS
	SET @IsSuccess = 0
	SET @EmailType = 'D' -- AWARD DENIED EMAIL
	SET @DeniedAwardStatus = 'D'
	SET @PendingEmailStatus = 'P'

	-- GET GIVER AND RECIPIENT INFO
	SELECT
		@GiverFullName = dbo.ufEmployeeGUFullName(@GiverEmployeeID),
		@GiverEmailAddress = g.WorkEmailAddress,
		@GiverMgrEmailAddress = gm.WorkEmailAddress,
		@RecipMgrEmailAddress = rm.WorkEmailAddress,
		@RecipFullName = dbo.ufEmployeeGUFullName(@RecipEmployeeID),
		@DeniedByFullName = dbo.ufEmployeeGUFullName(@DeniedByEmployeeID),
		@AwardType = t.Description,
		@AwardBehavior = b.Title
	FROM tbAward a
		INNER JOIN tbEmployee g ON (a.GiverEmployeeID = g.EmployeeID)
		INNER JOIN tbEmployee gm ON (a.TermGiverMgrEmployeeID = gm.EmployeeID)
		INNER JOIN tbEmployee rm ON (a.TermRecipMgrEmployeeID = rm.EmployeeID)
		INNER JOIN tbAwardType t ON (a.AwardType = t.AwardType)
		INNER JOIN tbAwardBehavior b ON a.BehaviorID = b.BehaviorID
	WHERE a.AwardID = @AwardID
		AND a.Status = @DeniedAwardStatus
	IF (@@ROWCOUNT = 0)
		RETURN

	-- FORMAT @CCEmailAddressList
	IF (ISNULL(@RecipMgrEmailAddress, '') <> '')
		SET @CCEmailAddressList = @RecipMgrEmailAddress

	IF (ISNULL(@GiverMgrEmailAddress, '') <> '')
		IF (ISNULL(@CCEmailAddressList, '') <> '')
			SET @CCEmailAddressList = @CCEmailAddressList + ', ' + @GiverMgrEmailAddress
		ELSE
			SET @CCEmailAddressList = @GiverMgrEmailAddress

	-- GET TEMPLATE FOR AWARD TYPE
	SELECT
		@ToEmailAddressList = @GiverFullName	+ ' <' + @GiverEmailAddress + '>',
		@FromName = FromName,
		@FromEmailAddress = FromEmailAddress,
		@ReplyToEmailAddress = ReplyToEmailAddress,
		@BCCEmailAddressList = BCCEmailAddressList,
		@MessageSubjectTemplate = MessageSubjectTemplate,
		@MessageBodyTemplate = MessageBodyTemplate,
		@SMTPContentType = SMTPContentType,
		@SMTPPriority = SMTPPriority,
		@EmailTemplateID = EmailTemplateID
	FROM tbEmailTemplate
	WHERE EmailType = @EmailType
		AND LanguageCode = @LanguageCode
	IF (@@ROWCOUNT = 0)
		RETURN

	-- BUILD MESSAGE BODY
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<RecipientFullName>', @RecipFullName)
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<ReviewingMgrFullName>', @DeniedByFullName)
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<AwardType>', @AwardType)
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<AwardBehavior>', @AwardBehavior)

	-- CREATE tbOutboundEmail RECORD
	INSERT tbOutboundEmail(EmailTemplateID, ToEmailAddressList, FromName, FromEmailAddress,
		ReplyToEmailAddress, CCEmailAddressList, BCCEmailAddressList, MessageSubject, MessageBody, SMTPContentType,
		SMTPPriority, RecordDateTime, Status, StatusDateTime, FromEmployeeID, ToEmployeeID, AwardID)
	VALUES(@EmailTemplateID, @ToEmailAddressList, @FromName, @FromEmailAddress,
		@ReplyToEmailAddress, @CCEmailAddressList, @BCCEmailAddressList, @MessageSubjectTemplate, @MessageBodyTemplate, @SMTPContentType,
		@SMTPPriority, CURRENT_TIMESTAMP, @PendingEmailStatus, CURRENT_TIMESTAMP, @GiverEmployeeID, @RecipEmployeeID, @AwardID)
	SET @EmailID = @@IDENTITY
	IF (ISNULL(@EmailID, 0) = 0)
		RETURN

	-- QUEUE MESSAGE
	EXEC dbo.upSendOutboundEmail
		@EmailID = @EmailID,
		@IsSuccess = @IsSuccess OUTPUT

	SET NOCOUNT OFF
GO
PRINT 'ALTERING PROC: upSendOutboundEmail'
GO
ALTER PROCEDURE dbo.upSendOutboundEmail
	@EmailID int,
	@IsSuccess bit OUTPUT
AS
DECLARE
	@RedirectOutboundEmail bit,
	@EmailType char(1),
	@ToEmailAddressList varchar(400),
	@FromName varchar(80),
	@FromEmailAddress varchar(120),
	@ReplyToEmailAddress varchar(120),
	@CCEmailAddress varchar(120),
	@BCCEmailAddress varchar(120),
	@MessageSubject varchar(120),
	@MessageBody varchar(7000),
	@SMTPPriority char(1),
	@SMTPContentType char(1),
	@MailerJobID int,
	@MessageQueued bit,
	@MessageSent bit,
	@SendImmediately bit,
	@StatusComment varchar(120),
	@PendingStatus char(1),
	@SuccessStatus char(1),
	@FailureStatus char(1),
	@RedirectEmailAddressList varchar(400),
	@SendStatus char(1)

	SET NOCOUNT ON
	
	SET @IsSuccess = 0

	-- CHECK INPUTS
	IF (NOT EXISTS (SELECT 1 FROM tbOutboundEmail WHERE EmailID = @EmailID))
	BEGIN
		RAISERROR('upSendOutboundEmail - @EmailID supplied does not exist', 16, 1)
		RETURN
	END

	-- SET CONSTANTS
	SET @PendingStatus = 'P'
	SET @SuccessStatus = 'S'
	SET @FailureStatus = 'F'

	-- SET VARS
	SET @SendImmediately = dbo.ufSettingValue('OutboundEmailSendImmediately')
	IF (@SendImmediately IS NULL)
	BEGIN
		SET @StatusComment = 'upSendOutboundEmail - failed to obtain value for setting (OutboundEmailSendImmediately)'
		UPDATE tbOutboundEmail SET Status = @FailureStatus, StatusDateTime = CURRENT_TIMESTAMP, StatusComment = @StatusComment
		RETURN
	END
	SET @RedirectOutboundEmail = dbo.ufSettingValue('RedirectOutboundEmail')
	IF (@RedirectOutboundEmail IS NULL)
	BEGIN
		SET @StatusComment = 'upSendOutboundEmail - failed to obtain value for setting (RedirectOutboundEmail)'
		UPDATE tbOutboundEmail SET Status = @FailureStatus, StatusDateTime = CURRENT_TIMESTAMP, StatusComment = @StatusComment
		RETURN
	END
	IF (@RedirectOutboundEmail = 1)
	BEGIN
		SET @RedirectEmailAddressList = dbo.ufSettingValue('RedirectOutboundEmailToAddressList')
		IF (ISNULL(@RedirectEmailAddressList, '') = '')
		BEGIN
			SET @StatusComment = 'upSendOutboundEmail - failed to obtain value for setting (RedirectOutboundEmailToAddressList)'
			UPDATE tbOutboundEmail SET Status = @FailureStatus, StatusDateTime = CURRENT_TIMESTAMP, StatusComment = @StatusComment
			RETURN
		END
	END

	-- GET EMAIL JOB INFO
	SELECT
		@ToEmailAddressList = ToEmailAddressList,
		@FromName = FromName,
		@FromEmailAddress = FromEmailAddress,
		@ReplyToEmailAddress = ReplyToEmailAddress,
		@CCEmailAddress = CCEmailAddressList,
		@BCCEmailAddress = BCCEmailAddressList,
		@MessageSubject = MessageSubject,
		@MessageBody = MessageBody,
		@SMTPContentType = CASE SMTPContentType WHEN 'text/plain' THEN 'P' WHEN 'text/html' THEN 'H' END,
		@SMTPPriority = CASE SMTPPriority WHEN 'HIGH' THEN 'H' WHEN 'NORMAL' THEN 'N' WHEN 'LOW' THEN 'L' END
	FROM tbOutboundEmail (UPDLOCK HOLDLOCK)
	WHERE EmailID = @EmailID
		AND Status = @PendingStatus
	IF (@@ROWCOUNT = 0)
	BEGIN
		SET @StatusComment = 'upSendOutboundEmail - record does not exist for ID given or has status other than Pending'
		UPDATE tbOutboundEmail SET Status = @FailureStatus, StatusDateTime = CURRENT_TIMESTAMP, StatusComment = @StatusComment
		COMMIT TRAN
		RETURN
	END

	-- REDIRECT MODE: REDIRECTS ALL MESSAGES TO DESIGNATED ADDRESS LIST
	IF (@RedirectOutboundEmail = 1)
	BEGIN
		SET @ToEmailAddressList = @RedirectEmailAddressList
		SET @CCEmailAddress = NULL
		SET @BCCEMailAddress = NULL
		SET @ReplyToEmailAddress = NULL
	END

	-- SEND MESSAGE
	EXEC [mail.madisonpg.com].OutboundEmail.dbo.upQueueMessage
		@EmailID = @MailerJobID OUTPUT,
		@ToEmailAddressList = @ToEmailAddressList,
		@FromName = @FromName,
		@FromEmailAddress = @FromEmailAddress,
		@ReplyToEmailAddress = @ReplyToEmailAddress,
		@CCEmailAddressList = @CCEmailAddress,
		@BCCEmailAddressList = @BCCEmailAddress,
		@MessageSubject = @MessageSubject,
		@MessageBody = @MessageBody,
		@SMTPContentType = @SMTPContentType,
		@SMTPPriority = @SMTPPriority,
		@SendImmediately = @SendImmediately,
		@MessageQueued = @MessageQueued OUTPUT,
		@MessageSent = @MessageSent OUTPUT
	IF (@MessageQueued = 0 AND @MessageSent = 0)
	BEGIN
		SET @SendStatus = @FailureStatus
		SET @StatusComment = 'upSendOutboundEmail - call to upQueueMessage on mail server failed'
	END
	ELSE
		BEGIN
			SET @SendStatus = @SuccessStatus
			SET @IsSuccess = 1
		END

	-- UPDATE RECORD STATUS
	UPDATE tbOutboundEmail SET
		MailerJobID = @MailerJobID,
		Status = @SendStatus,
		StatusDateTime = CURRENT_TIMESTAMP,
		StatusComment = @StatusComment
	WHERE EmailID = @EmailID
	
	SET NOCOUNT OFF
GO

-- REINDEX TABLES
PRINT CHAR(13) + CHAR(10) + '**** INDEXES ****'
GO

PRINT ''
PRINT '**** TRANSFORM v1-0 TO v1-1 COMPLETE ****'
GO

SET NOCOUNT OFF