select empl_status, count(empl_status)
from tbloadfile
where reports_to in (
select distinct position_nbr
from tbloadfile
where reports_to in (
select b.reports_to
from tbloadfile a
right outer join (
	SELECT DISTINCT reports_to
	FROM Baxter.dbo.tbLoadFile
	WHERE (empl_status IN ('A', 'L', 'P', 'S', 'W') OR empl_status IS NULL OR LEN(empl_status) = 0)
	AND (position_nbr IS NOT NULL AND LEN(position_nbr) > 0)
) b on b.reports_to = a.position_nbr
where a.position_nbr is null
)
)
group by empl_status

=================================
select distinct position_nbr, reports_to
into #temp
from tbloadfile
where len(position_nbr) > 0

select distinct(position_nbr)
from #temp
group by position_nbr
having count(position_nbr) > 1
order by position_nbr

drop table #temp
===================================

select distinct(position)
from tbparentage
group by position
having count(position) > 1
order by position

====================================

select * from tbparentage
where parent in (
select position from tbparentage where lvl = -100
)

select * from tbparentage2
where parent in (
select position from tbparentage2 where lvl = -100
)

select * from tbparentage where lvl < 0 order by lvl

select *
from tbloadfile
where position_nbr in (
select b.position
from tbparentage a
right join (
select distinct position from tbparentage where lvl = -100
) b on b.position = a.parent
where a.id is null)

=========================================

--update tbparentage set lvl = null
	DECLARE @count int

--	SELECT @count = MIN(lvl) FROM dbo.tbParentage WHERE lvl IS NOT NULL
--
--	WHILE (EXISTS (SELECT 1 FROM dbo.tbParentage
--										INNER JOIN dbo.tbParentage parent ON dbo.tbParentage.parent = parent.position AND parent.lvl = @count
--										WHERE dbo.tbParentage.lvl IS NULL))
--		BEGIN
--			-- Assign lvl to one greater then previous lvl.
--			--
--			UPDATE dbo.tbParentage SET lvl = parent.lvl + 1
--			FROM dbo.tbParentage
--			INNER JOIN dbo.tbParentage parent ON dbo.tbParentage.parent = parent.position AND parent.lvl = @count
--			WHERE dbo.tbParentage.lvl IS NULL
--
--			-- Increment counter.
--			--
--			SET @count = @count + 1
--		END


	-- Assign highest level for orphans.
	--
	UPDATE dbo.tbParentage SET lvl = -100
	FROM dbo.tbParentage
	INNER JOIN (SELECT parent.parent
							FROM dbo.tbParentage child
							RIGHT JOIN (SELECT DISTINCT parent FROM dbo.tbParentage WHERE lvl IS NULL AND parent IS NOT NULL) parent
							ON child.position = parent.parent
							WHERE child.id IS NULL) orphans
	ON dbo.tbParentage.parent = orphans.parent
	WHERE dbo.tbParentage.lvl IS NULL

	SELECT @count = MIN(lvl) FROM dbo.tbParentage WHERE lvl IS NOT NULL

	-- Loop over parentage table and populate Level until there are no more Nulls in lvl field.
	--
	WHILE (EXISTS (SELECT 1 FROM dbo.tbParentage
										INNER JOIN dbo.tbParentage parent ON dbo.tbParentage.parent = parent.position AND parent.lvl = @count
										WHERE dbo.tbParentage.lvl IS NULL))
		BEGIN
			-- Assign lvl to one greater then previous lvl.
			--
			UPDATE dbo.tbParentage SET lvl = parent.lvl - 1
			FROM dbo.tbParentage
			INNER JOIN dbo.tbParentage parent ON dbo.tbParentage.parent = parent.position AND parent.lvl = @count
			WHERE dbo.tbParentage.lvl IS NULL

			-- Decrement counter.
			--
			SET @count = @count - 1
		END
