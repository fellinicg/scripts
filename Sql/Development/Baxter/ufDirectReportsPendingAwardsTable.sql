USE [Baxter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION dbo.ufDirectReportsPendingAwardsTable (@EmployeeID int, @GivRec char(1), @Date datetime)
RETURNS varchar(8000)
AS
BEGIN
DECLARE
	@HTMLTable varchar(8000),
	@EmpLangCode char(3),
	@PendingAwardStatus char(1),
	@RecipNameTitle varchar(40),
	@GiverNameTitle varchar(40),
	@AwardBehaviorTitle varchar(40),
	@AwardAmtTitle varchar(40),
	@ApproveDateTitle varchar(40),
	@CurrAwardID int,
	@TableRecordLimit int,
	@TableOpenTag varchar(40),
	@Today datetime, 
	@Yesterday datetime

	SET @PendingAwardStatus = 'P'
	SET @TableRecordLimit = 30  -- MORE RECORDS WOULD RESULT IN TRUNCATED TABLE
	SET @TableOpenTag = '<TABLE CLASS="pendingemailtable">'

	-- GET DATE FILTERS
	SET @Today = CONVERT(VARCHAR, @Date, 101)
	SET @Yesterday = CONVERT(VARCHAR, (DATEADD(DAY, -1, @Date)), 101)

	-- CHECK INPUTS
	SELECT @EmpLangCode = dbo.ufAccountPreferredLanguage(@EmployeeID)
	IF (@EmpLangCode = 'XXX')  -- FUNC FAILURE INDICATOR
		RETURN(@HTMLTable)
	IF (@GivRec NOT IN ('G', 'R'))
		RETURN(@HTMLTable)

	-- IDENTIFY QUALIFIED AWARDS
	DECLARE @QualAwd TABLE (AwardID int NOT NULL)
	IF (@GivRec = 'R')
		INSERT @QualAwd(AwardID)
		SELECT a.AwardID
		FROM tfEmployeeDirectReports(@EmployeeID) dr
			INNER JOIN tbAward a ON (dr.EmployeeID = a.RecipEmployeeID AND a.Status = @PendingAwardStatus
																AND a.RecordDateTime BETWEEN @Yesterday AND @Today)
	ELSE
		INSERT @QualAwd(AwardID)
		SELECT a.AwardID
		FROM tfEmployeeDirectReports(@EmployeeID) dr
			INNER JOIN tbAward a ON (dr.EmployeeID = a.GiverEmployeeID AND a.Status = @PendingAwardStatus
																AND a.RecordDateTime BETWEEN @Yesterday AND @Today)

	-- BUILD TABLE HEADER
	SET @RecipNameTitle = CASE @EmpLangCode WHEN 'ESP' THEN 'Recipiente' ELSE 'Award Recipient' END
	SET @GiverNameTitle = CASE @EmpLangCode WHEN 'ESP' THEN 'Donante' ELSE 'Giver' END
	SET @AwardBehaviorTitle = CASE @EmpLangCode WHEN 'ESP' THEN 'Conducta Premiada' ELSE 'Award Behavior' END
	SET @AwardAmtTitle = CASE @EmpLangCode WHEN 'ESP' THEN 'Cantidade Recompensa' ELSE 'Award Amount' END
	SET @ApproveDateTitle = CASE @EmpLangCode WHEN 'ESP' THEN 'Fecha Automática de Aprobación' ELSE 'Automatic Approval Date' END
	SET @HTMLTable = @TableOpenTag + '<TR>'
		+ '<TD>' + @RecipNameTitle + '</TD>'
		+ '<TD>' + @GiverNameTitle + '</TD>'
		+ '<TD>' + @AwardBehaviorTitle + '</TD>'
		+ '<TD>' + @AwardAmtTitle + '</TD>'
		+ '<TD>' + @ApproveDateTitle + '</TD>'
		+ '</TR>'

	-- BUILD TABLE BODY
	SELECT @CurrAwardID = MIN(AwardID) FROM @QualAwd
	IF (ISNULL(@CurrAwardID, 0) = 0)
	BEGIN
		SET @HTMLTable = NULL
		RETURN(@HTMLTable)
	END
	WHILE (ISNULL(@CurrAwardID, 0) <> 0 AND @TableRecordLimit > 0)
	BEGIN
		-- ADD ROW FOR CURR AWARD
		SELECT @HTMLTable = @HTMLTable + '<TR>'
			+ '<TD>' + dbo.ufEmployeeGUFullName(a.RecipEmployeeID) + '</TD>'
			+ '<TD>' + dbo.ufEmployeeGUFullName(a.GiverEmployeeID) + '</TD>'
			+ '<TD>' + b.Title + '</TD>'
			+ '<TD>' + '$' + CONVERT(varchar, a.AwardAmtUSD, 1) + '</TD>'
			+ '<TD>' + CONVERT(varchar, dbo.ufAwardApprovalDate(a.AwardID), 101) + '</TD>'
			+ '</TR>'
		FROM @QualAwd q
			INNER JOIN tbAward a ON (q.AwardID = a.AwardID)
			INNER JOIN tbAwardBehavior b ON (a.BehaviorID = b.BehaviorID)
		WHERE q.AwardID = @CurrAwardID
		-- GET NEXT AWARD
		SELECT @CurrAwardID = MIN(AwardID) FROM @QualAwd WHERE AwardID > @CurrAwardID
	END

	-- CLOSE TABLE
	SET @HTMLTable = @HTMLTable + '</TABLE>'

	-- CHECK RESULT
	IF (LEFT(@HTMLTable, LEN(@TableOpenTag)) <> @TableOpenTag OR RIGHT(@HTMLTable, 8) <> '</TABLE>')
		SET @HTMLTable = NULL

	RETURN(@HTMLTable)

END
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

