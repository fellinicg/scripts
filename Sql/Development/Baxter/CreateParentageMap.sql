SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: October 24, 2005
-- Description:	Create parentage mappings
-- =============================================
ALTER PROCEDURE CreateParentageMap 
AS
BEGIN

	DECLARE @count int
	DECLARE @count2 int
	DECLARE @diff int

	SET NOCOUNT ON

	-- Clear parentage table.
	--
	TRUNCATE TABLE dbo.tbParentage

	-- Initialize counter.
	--
	SET @count = 1

	-- Populate parentage table with valid active users' with only 1 employee record.
	--
	INSERT INTO dbo.tbParentage (position, ancestor, diff)
	SELECT DISTINCT position_nbr, reports_to, 1
	FROM dbo.tbLoadFile src
	LEFT JOIN (SELECT emplid FROM dbo.tbLoadFile WHERE empl_rcd = 1) filter ON src.emplid = filter.emplid
	WHERE (empl_status IN ('A', 'L', 'P', 'S', 'W') OR empl_status IS NULL OR LEN(empl_status) = 0)
	AND (position_nbr IS NOT NULL AND LEN(position_nbr) > 0)
	AND empl_rcd = 0
	AND filter.emplid IS NULL

	-- Populate parentage table with valid active users' with 2 employee records.
	--
	INSERT INTO dbo.tbParentage (position, ancestor, diff)
	SELECT DISTINCT position_nbr, reports_to, 1
	FROM dbo.tbLoadFile
	LEFT JOIN dbo.tbParentage ON position_nbr = position AND reports_to = ancestor
	WHERE (empl_status IN ('A', 'L', 'P', 'S', 'W') OR empl_status IS NULL OR LEN(empl_status) = 0)
	AND (position_nbr IS NOT NULL AND LEN(position_nbr) > 0)
	AND empl_rcd = 1
	AND position IS NULL

	-- Add orphan's parents to parentage table.
	--
	WHILE (@count > 0)
		BEGIN
			INSERT INTO dbo.tbParentage (position, ancestor, diff)
			SELECT DISTINCT src.position_nbr, reports_to, 1
			FROM dbo.tbLoadFile src
			INNER JOIN (SELECT DISTINCT org.position_nbr, MAX(ISNULL(org.termination_dt, '1/1/1900')) termination_dt
									FROM dbo.tbParentage child
									LEFT JOIN dbo.tbParentage parent ON child.ancestor = parent.position
									INNER JOIN dbo.tbLoadFile org ON child.ancestor = org.position_nbr
									WHERE parent.id IS NULL
									GROUP BY org.position_nbr) filter
			ON src.position_nbr = filter.position_nbr AND ISNULL(src.termination_dt, '1/1/1900') = ISNULL(filter.termination_dt, '1/1/1900')

				SET @count = @@ROWCOUNT
		END

	-- Assign the highest level.
	--
	UPDATE dbo.tbParentage SET lvl = 0
	FROM dbo.tbParentage 
	WHERE position = ancestor

	-- Populate local variable for loop.
	--
	SELECT @count = MIN(lvl) FROM dbo.tbParentage WHERE lvl IS NOT NULL

	-- Loop over parentage table and populate Level until there are no more Nulls in lvl field.
	--
	WHILE (EXISTS (SELECT 1 FROM dbo.tbParentage
										INNER JOIN dbo.tbParentage parent ON dbo.tbParentage.ancestor = parent.position AND parent.lvl = @count
										WHERE dbo.tbParentage.lvl IS NULL))
		BEGIN
			-- Assign lvl to one greater then previous lvl.
			--
			UPDATE dbo.tbParentage SET lvl = parent.lvl + 1
			FROM dbo.tbParentage
			INNER JOIN dbo.tbParentage parent ON dbo.tbParentage.ancestor = parent.position AND parent.lvl = @count
			WHERE dbo.tbParentage.lvl IS NULL

			-- Increment counter.
			--
			SET @count = @count + 1
		END


	-- Assign highest level for orphans.
	--
	UPDATE dbo.tbParentage SET lvl = -100
	FROM dbo.tbParentage
	INNER JOIN (SELECT parent.ancestor
							FROM dbo.tbParentage child
							RIGHT JOIN (SELECT DISTINCT ancestor FROM dbo.tbParentage WHERE lvl IS NULL AND ancestor IS NOT NULL) parent
							ON child.position = parent.ancestor
							WHERE child.id IS NULL) orphans
	ON dbo.tbParentage.ancestor = orphans.ancestor
	WHERE dbo.tbParentage.lvl IS NULL

	-- Reset counter.
	--
	SELECT @count = MIN(lvl) FROM dbo.tbParentage WHERE lvl IS NOT NULL

	-- Loop over parentage table for orphans and populate Level until there are no more Nulls in lvl field.
	--
	WHILE (EXISTS (SELECT 1 FROM dbo.tbParentage
										INNER JOIN dbo.tbParentage parent ON dbo.tbParentage.ancestor = parent.position AND parent.lvl = @count
										WHERE dbo.tbParentage.lvl IS NULL))
		BEGIN
			-- Assign lvl to one greater then previous lvl.
			--
			UPDATE dbo.tbParentage SET lvl = parent.lvl - 1
			FROM dbo.tbParentage
			INNER JOIN dbo.tbParentage parent ON dbo.tbParentage.ancestor = parent.position AND parent.lvl = @count
			WHERE dbo.tbParentage.lvl IS NULL

			-- Decrement counter.
			--
			SET @count = @count - 1
		END

	-- Reset counter to lowest level (highest level number).
	--
	SELECT @count = MAX(lvl) FROM dbo.tbParentage WHERE lvl IS NOT NULL

	-- Populate family tree starting at lowest level working backwards.
	--
	WHILE (@count > 1)
		BEGIN
			-- Initialize counters.
			--
			SET @count2 = @count - 2

			-- Populate family for all members at level equal to outer loop (@count).
			--
			WHILE (@count2 > -1)
				BEGIN
					-- Initialize counters.
					--
					SET @diff = @count - @count2

					-- Insert data into tbParentage.
					--
					INSERT INTO dbo.tbParentage (position, ancestor, lvl)
					SELECT DISTINCT assignto.position, value.position, assignto.lvl
					FROM dbo.tbParentage assignto
					LEFT JOIN dbo.tbParentage link ON assignto.ancestor = link.position
					LEFT JOIN dbo.tbParentage value ON link.ancestor = value.position AND value.lvl = @count2
					WHERE assignto.lvl = @count
					AND value.position IS NOT NULL

					-- Update diff field.
					--
					UPDATE dbo.tbParentage SET diff = @diff
					WHERE diff IS NULL
					AND lvl = @count

					-- Decrement counter.
					--
					SET @count2 = @count2 - 1
				END

			-- Decrement counter.
			--
			SET @count = @count - 1

		END
END
GO
