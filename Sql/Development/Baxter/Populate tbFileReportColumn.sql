USE Baxter
GO
TRUNCATE TABLE baxter.dbo.tbFileReportColumn
GO
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (1,'E',1,'Feeder ID','Fixed value “EDIMPG”')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (2,'E',2,'Supplier Number','Fixed value “36881870”')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (3,'E',3,'Supplier Name','Fixed value “Madison Performance Group”')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (4,'E',4,'Invoice Number','Variable value that Madison uses for this award. NO Dup values,dashes or slashes in this field, no length requirements  Any Alpha characters included in the invoice number MUST be capitalized')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (5,'E',5,'Invoice Amount','Variable value containing the dollar amount for the gift cert award + 9% fulfill fee, or the fulfill fee for an individual debit card award (excl the debit card award value)  2 dec places, no commas  ')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (6,'E',6,'Payment Terms','Fixed value “A”')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (7,'E',7,'Invoice Date','Variable value containing the date that this file is generated  Format is MMDDYY, any single digit months should contain a leading zero')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (8,'E',8,'Comp Code','Variable value containing the four digit legal entity ({LEGAL_ENTITY}  field) of the giver at the time this file is generated')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (9,'E',9,'GL Date','Variable value containing the date that this file is generated  Format is MMDDYY, any single digit months should contain a leading zero')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (10,'E',10,'SPH Code','Blank value')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (11,'E',11,'Payment Remarks','Fixed value containing “RR” and the “[MonthName DD YY] thru [MonthName DD YY]”  No dashes, slashes or symbols can be used in this field  ')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (12,'E',12,'Business Unit','Variable value containing the ten digit JDE number ({JDE_NUM} field) of the giver at the time this file is generated')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (13,'E',13,'Object Account','Fixed value “80256”')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (14,'E',14,'Sub Account','Blank value')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (15,'E',15,'Explanation','Fixed value “Award from Madison [Feeder ID]”')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (16,'E',16,'Purchase Order','Blank value')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (17,'E',17,'Currency Code','Fixed value “USD”')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (18,'E',18,'Units','Blank value')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (19,'E',19,'UOM','Blank value')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (20,'E',20,'Subledger','Blank value')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (21,'E',21,'Subledger Type','Blank value')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (22,'L',1,'Business Unit','Variable value containing the ten digit JDE number ({JDE_NUM} field) of the giver at the time this file is generated.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (23,'L',2,'Obj Acct','Fixed value “80256”')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (24,'L',3,'Sub Acct','Blank value')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (25,'L',4,'Debit Award Amount','Variable value containing the dollar amount for the award. The field should be formatted to 2 decimal places with no commas.  ')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (26,'L',5,'Explanation','Fixed value “Baxter Rewards Award Charge”')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (27,'R',1,'Recipient Last Name','Variable value containing the last name (derived from the {NAME}  field) of the recipient.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (28,'R',2,'Recipient First Name ','Variable value containing the name (derived from the {NAME}  field) of the recipient.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (29,'R',3,'Email','Variable value containing the email ({BX_BUSN_EMAIL_ADDR } field) of the recipient.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (30,'R',4,'Emp ID','Variable value containing the six digit employee ID ({EMPLID} field) of the recipient.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (31,'R',5,'Active Card','Variable value containing the value of “Y” or “N”, where “Y” is when the recipient has an activated Baxter Rewards debit card.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (32,'R',6,'Card Expiration Date','Variable value containing the MM/YYYY of the date which the recipients debit card expires.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (33,'R',7,'Award Issued Date','Variable value containing the date that this award is initiated by giver.  Format is MMDDYY, any single digit months should contain a leading zero.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (34,'R',8,'Amount Awarded','Variable value containing the dollar amount for the award. The field should be formatted to 2 decimal places with no commas.  ')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (35,'R',9,'Amount Spend YTD','Variable value containing the total amount the recipient has spent on their debit card at the time this file is generated for the current calendar year. 2 dec, no commas.  ')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (36,'R',10,'Current Debit Card Balance','Variable value containing the total amount the recipient has remaining on their debit card at the time this file is generated. The field should be formatted to 2 decimal places with no commas.  ')
GO	     
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (37,'R',11,'Giver ID','Variable value containing the six digit employee ID ({EMPLID} field) of the giver.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (38,'R',12,'Giver Name','Variable value containing the name ({NAME}  field) of the giver.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (39,'R',13,'Giver PS Dept ID','Variable value containing the PS Dept ID ({DEPTID}  field) of the giver.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (40,'R',14,'Giver JDE Cost Center','Variable value containing the JDE Cost Center Number ({JDE_NUM}  field) of the giver.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (41,'R',15,'Giver Company Code','Variable value containing the Company ({COMPANY}  field) of the giver.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (42,'R',16,'Giver Legal Entity','Variable value containing the Legal Entity ({LEGAL_ENTITY}  field) of the giver.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (43,'R',17,'Award Approved By (Giver’s Manager)','Variable value containing the name ({NAME}  field) of the giver’s manager.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (44,'R',18,'Giver’s Manager JDE Cost Center','Variable value containing the JDE Cost Center Number ({JDE_NUM}  field) of the giver’s manager.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (45,'R',19,'Amount Approved','Variable value containing the dollar amount for the award. The field should be formatted to 2 decimal places with no commas')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (46,'R',20,'Approved Date','Variable value containing the date that this award is approved by the system.  Format is MMDDYY, any single digit months should contain a leading zero')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (47,'A',1,'Last Name','Variable value containing the last name (derived from the {NAME}  field) of each user with an active debit card.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (48,'A',2,'First Name','Variable value containing the name (derived from the {NAME}  field) of each user with an active debit card.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (49,'A',3,'Emp ID','Variable value containing the six digit employee ID ({EMPLID} field) of each user with an active debit card.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (50,'A',4,'Employee Status','Variable value containing the employee status ({EMPL_STATUS} field) of each user with an active debit card.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (51,'A',5,'Active Card','Variable value containing the value of “Y” or “N”, where “Y” is when the recipient has an activated Baxter Rewards debit card (only “Y” values should be present in this report due to the filter).')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (52,'A',6,'Card Expiration Date','Variable value containing the MM/YYYY of the date which the recipients debit card expires.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (53,'A',7,'Last Award Date','Variable value containing the date that this user last received a debit card award.  Format is MMDDYY, any single digit months should contain a leading zero.')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (54,'A',8,'Amount Spend YTD','Variable value containing the total amount the recipient has spent on their debit card at the time this file is generated for the current calendar year. 2 dec, no commas.  ')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (55,'A',9,'Current Debit Card Balance','Variable value containing the total amount the recipient has remaining on their debit card at the time this file is generated. The field should be formatted to 2 decimal places with no commas.  ')
GO	   
INSERT INTO baxter.dbo.tbFileReportColumn (ColumnID, FileReportType, PostionNum, DisplayName, Comment)
VALUES (56,'A',10,'Last Award Giver ID','Variable value containing the six digit employee ID ({EMPLID} field) of the giver for the last debit card award that this user received.')
GO