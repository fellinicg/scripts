-- Temporarily allow nulls for MessageBodyTemplate
--ALTER TABLE dbo.tbEmailTemplate ALTER COLUMN MessageBodyTemplate VARCHAR(7000) NULL

-- Insert new records into tbEmailTemplate
INSERT INTO dbo.tbEmailTemplate (LanguageCode,EmailType,EmailSubType,EmailTemplateID,ToName,ToEmailAddressList,FromName,FromEmailAddress,ReplyToEmailAddress,CCEmailAddressList,BCCEmailAddressList,MessageSubjectTemplate,SMTPContentType,SMTPPriority,MessageBodyTemplate)
VALUES('ENG','A','D','23','<RecipientFullName>','<RecipientEmailAddress>','Baxter Award Headquarters','Awards@BaxterRewards.com','Awards@BaxterRewards.com','<GiverEmailAddress>','brose@madisonpg.com','New Baxter Award!','text/html','NORMAL','')
GO
INSERT INTO dbo.tbEmailTemplate (LanguageCode,EmailType,EmailSubType,EmailTemplateID,ToName,ToEmailAddressList,FromName,FromEmailAddress,ReplyToEmailAddress,CCEmailAddressList,BCCEmailAddressList,MessageSubjectTemplate,SMTPContentType,SMTPPriority,MessageBodyTemplate)
VALUES('ENG','A','G','24','<RecipientFullName>','<RecipientEmailAddress>','Baxter Award Headquarters','Awards@BaxterRewards.com','Awards@BaxterRewards.com','<GiverEmailAddress>','brose@madisonpg.com','New Baxter Award!','text/html','NORMAL','')
GO
INSERT INTO dbo.tbEmailTemplate (LanguageCode,EmailType,EmailSubType,EmailTemplateID,ToName,ToEmailAddressList,FromName,FromEmailAddress,ReplyToEmailAddress,CCEmailAddressList,BCCEmailAddressList,MessageSubjectTemplate,SMTPContentType,SMTPPriority,MessageBodyTemplate)
VALUES('ENG','A','R','25','<RecipientFullName>','<RecipientEmailAddress>','Baxter Award Headquarters','Awards@BaxterRewards.com','Awards@BaxterRewards.com','<GiverEmailAddress>','brose@madisonpg.com','New Baxter Award!','text/html','NORMAL','')
GO
INSERT INTO dbo.tbEmailTemplate (LanguageCode,EmailType,EmailSubType,EmailTemplateID,ToName,ToEmailAddressList,FromName,FromEmailAddress,ReplyToEmailAddress,CCEmailAddressList,BCCEmailAddressList,MessageSubjectTemplate,SMTPContentType,SMTPPriority,MessageBodyTemplate)
VALUES('ESP','A','D','26','<RecipientFullName>','<RecipientEmailAddress>','Jefaturas De la Concesión de Baxter','Awards@BaxterRewards.com','Awards@BaxterRewards.com','<GiverEmailAddress>','brose@madisonpg.com','¡Reconocimiento Nuevo de Baxter!','text/html','NORMAL','')
GO
INSERT INTO dbo.tbEmailTemplate (LanguageCode,EmailType,EmailSubType,EmailTemplateID,ToName,ToEmailAddressList,FromName,FromEmailAddress,ReplyToEmailAddress,CCEmailAddressList,BCCEmailAddressList,MessageSubjectTemplate,SMTPContentType,SMTPPriority,MessageBodyTemplate)
VALUES('ESP','A','G','27','<RecipientFullName>','<RecipientEmailAddress>','Jefaturas De la Concesión de Baxter','Awards@BaxterRewards.com','Awards@BaxterRewards.com','<GiverEmailAddress>','brose@madisonpg.com','¡Reconocimiento Nuevo de Baxter!','text/html','NORMAL','')
GO
INSERT INTO dbo.tbEmailTemplate (LanguageCode,EmailType,EmailSubType,EmailTemplateID,ToName,ToEmailAddressList,FromName,FromEmailAddress,ReplyToEmailAddress,CCEmailAddressList,BCCEmailAddressList,MessageSubjectTemplate,SMTPContentType,SMTPPriority,MessageBodyTemplate)
VALUES('ESP','A','R','28','<RecipientFullName>','<RecipientEmailAddress>','Jefaturas De la Concesión de Baxter','Awards@BaxterRewards.com','Awards@BaxterRewards.com','<GiverEmailAddress>','brose@madisonpg.com','¡Reconocimiento Nuevo de Baxter!','text/html','NORMAL','')
GO
UPDATE tbEmailTemplate SET MessageBodyTemplate = '<HTML><HEAD><TITLE>Email</TITLE><STYLE><!--.bodytextblack {font-size: 12px;font-weight: normal;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .bodytextboldblack {font-size: 12px;font-weight: bold;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;}--></STYLE></HEAD><BODY><table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td class="bodytextblack">Awarded by: <span class="bodytextboldblack"><GiverFullName></span>
<p class="bodytextboldblack">Congratulations! You have received an award via Baxter Rewards.</p>
<p class="bodytextblack">Award Type: Debit Card<br>
Award Behavior:<br><span class="bodytextboldblack"><AwardBehavior></span></p>
<p class="bodytextblack">The following comments are from the Giver:<br>
<CommentstoRecipient></p>
<p class="bodytextblack">To find out more, please login at <br>
<a href="http://www.baxterrewards.com">http://www.baxterrewards.com</a>.</p>
<p class="bodytextboldblack">Standard Delivery Time:<br>
<span class="bodytextblack">Delivery time for Baxter Rewards debit card is </span><span class="bodytextboldblack">5 to 7 business days via first class mail.*</span></p>
<p class="bodytextblack">*Note:  The Baxter Rewards debit card is a reloadable card product good for 3 years.  <span class="bodytextboldblack">Please do not discard.</span></p>
<p class="bodytextboldblack">***Please do not reply to this email***</p>
<p class="bodytextblack">&nbsp;</p></td></tr></table></BODY></HTML>' 
WHERE EmailType = 'A' AND LanguageCode = 'ENG' AND EmailSubType='D'

UPDATE tbEmailTemplate SET MessageBodyTemplate = '<HTML><HEAD><TITLE>Email</TITLE><STYLE><!--.bodytextblack {font-size: 12px;font-weight: normal;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .bodytextboldblack {font-size: 12px;font-weight: bold;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;}--></STYLE></HEAD><BODY><table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td class="bodytextblack">Reconocimiento de:<span class="bodytextboldblack"><GiverFullName></span>
<p class="bodytextboldblack">¡Felicitaciones! Usted ha recibido un reconocimiento vía Baxter Rewards. </p>
<p class="bodytextblack">Tipo de Recompensa: Tarjeta de Débito<br>
Conducta Premiada:<br>
<span class="bodytextboldblack"><AwardBehavior></span></p>
<p class="bodytextblack">Mensaje:<br>
<CommentstoRecipient></p>
<p class="bodytextblack">Para descubrir más, por favor de entrar a :<br>
<a href="http://www.baxterrewards.com">http://www.baxterrewards.com</a>.</p>
<p class="bodytextboldblack">Plazo de entraga de premios:<br>
<span class="bodytextblack">El plazo de entrega de la tarjeta debit de Baxter Rewards es de </span><span class="bodytextboldblack">5 - 7 días laborables enviar por correo.*</span></p>
<p class="bodytextblack">*Nota Importante:  La tarjeta debit de Baxter Rewards es regargable y valida por 3 años.  <span class="bodytextboldblack">Por Favor de no desechar.</span></p>
<p class="bodytextboldblack">***No conteste por favor a este email***</p>
<p class="bodytextblack">&nbsp;</p></td></tr></table></BODY></HTML>' 
WHERE EmailType = 'A' AND LanguageCode = 'ESP' AND EmailSubType='D'

UPDATE tbEmailTemplate SET MessageBodyTemplate = '<HTML><HEAD><TITLE>Email</TITLE><STYLE><!--.bodytextblack {font-size: 12px;font-weight: normal;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .bodytextboldblack {font-size: 12px;font-weight: bold;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;}--></STYLE></HEAD><BODY><table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td class="bodytextblack">Awarded by: <span class="bodytextboldblack"><GiverFullName></span>
<p class="bodytextboldblack">Congratulations! You have received an award via Baxter Rewards.</p>
<p class="bodytextblack">Award Type: Gift Certificate<br>Award Behavior:<br>
<span class="bodytextboldblack"><AwardBehavior></span></p>
<p class="bodytextblack">The following comments are from the Giver:<br>
<CommentstoRecipient></p>
<p class="bodytextblack">To find out more, please login at <br>
<a href="http://www.baxterrewards.com">http://www.baxterrewards.com</a>.</p>
<p class="bodytextboldblack">Standard Delivery Time:<br>
<span class="bodytextblack">Delivery time for gift certificates is </span><span class="bodytextboldblack">2 business weeks via first class mail.</span></p>
<p class="bodytextboldblack">***Please do not reply to this email***</p>
<p class="bodytextblack">&nbsp;</p>
</td></tr></table></BODY></HTML>' 
WHERE EmailType = 'A' AND LanguageCode = 'ENG' AND EmailSubType='G'

UPDATE tbEmailTemplate SET MessageBodyTemplate = '<HTML><HEAD><TITLE>Email</TITLE><STYLE><!--.bodytextblack {font-size: 12px;font-weight: normal;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .bodytextboldblack {font-size: 12px;font-weight: bold;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;}--></STYLE></HEAD><BODY><table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td class="bodytextblack">Reconocimiento de: <span class="bodytextboldblack"><GiverFullName></span>
<p class="bodytextboldblack">¡Felicitaciones! Usted ha recibido un reconocimiento vía Baxter Rewards. </p>
<p class="bodytextblack">Tipo de Recompensa: Certificado de Regalo<br>
Conducta Premiada:<br>
<span class="bodytextboldblack"><AwardBehavior></span></p>
<p class="bodytextblack">Mensaje:<br>
<CommentstoRecipient></p>
<p class="bodytextblack">Para descubrir más, por favor de entrar a :<br>
<a href="http://www.baxterrewards.com">http://www.baxterrewards.com</a>.</p>
<p class="bodytextboldblack">Plazo de entraga de premios:<br>
<span class="bodytextblack">El plazo de entrega de las tarjetas de regalo es de </span><span class="bodytextboldblack">10 días laborables</span> enviar por correo.</p>
<p class="bodytextboldblack">***No conteste por favor a este email***</p>
<p class="bodytextblack">&nbsp;</p></td></tr></table></BODY></HTML>' 
WHERE EmailType = 'A' AND LanguageCode = 'ESP' AND EmailSubType='G'

UPDATE tbEmailTemplate SET MessageBodyTemplate = '<HTML><HEAD><TITLE>Email</TITLE><STYLE><!--.bodytextblack {font-size: 12px;font-weight: normal;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .bodytextboldblack {font-size: 12px;font-weight: bold;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;}--></STYLE></HEAD><BODY><table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td class="bodytextblack">Awarded by: <span class="bodytextboldblack"><GiverFullName></span>
<p class="bodytextboldblack">Congratulations! You have received an award via Baxter Rewards.</p>
<p class="bodytextblack">Award Type: Recognition<br>Award Behavior:<br>
<span class="bodytextboldblack"><AwardBehavior></span></p>
<p class="bodytextblack">The following comments are from the Giver:<br>
<CommentstoRecipient></p>
<p class="bodytextblack">To find out more, please login at <br><a href="http://www.baxterrewards.com">http://www.baxterrewards.com</a>.</p>
<p class="bodytextboldblack">***Please do not reply to this email***</p>
<p class="bodytextblack">&nbsp;</p>
</td></tr></table></BODY></HTML>' 
WHERE EmailType = 'A' AND LanguageCode = 'ENG' AND EmailSubType='R'

UPDATE tbEmailTemplate SET MessageBodyTemplate = '<HTML><HEAD><TITLE>Email</TITLE><STYLE><!--.bodytextblack {font-size: 12px;font-weight: normal;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .bodytextboldblack {font-size: 12px;font-weight: bold;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;}--></STYLE></HEAD><BODY><table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td class="bodytextblack">Reconocimiento de: <span class="bodytextboldblack"><GiverFullName></span>
<p class="bodytextboldblack">¡Felicitaciones! Usted ha recibido un reconocimiento vía Baxter Rewards. </p>
<p class="bodytextblack">Tipo de Recompensa: Reconocimiento<br>
Conducta Premiada:<br>
<span class="bodytextboldblack"><AwardBehavior></span></p>
<p class="bodytextblack">Mensaje:<br>
<CommentstoRecipient></p>
<p class="bodytextblack">Para descubrir más, por favor de entrar a :<br>
<a href="http://www.baxterrewards.com">http://www.baxterrewards.com</a>.</p>
<p class="bodytextboldblack">***No conteste por favor a este email***</p>
<p class="bodytextblack">&nbsp;</p></td></tr></table></BODY></HTML>' 
WHERE EmailType = 'A' AND LanguageCode = 'ESP' AND EmailSubType='R'

UPDATE tbEmailTemplate SET MessageBodyTemplate = '<HTML><HEAD><TITLE>Email</TITLE><STYLE><!--.bodytextblack {font-size: 12px;font-weight: normal;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .bodytextboldblack {font-size: 12px;font-weight: bold;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;}--></STYLE></HEAD><BODY><table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td class="bodytextblack">Award Recipient: <span class="bodytextboldblack"><RecipientFullName></span>
<p class="bodytextblack">Denied by: <span class="bodytextboldblack"><ReviewingMgrFullName></span></p>
<p class="bodytextblack">Award Type: <AwardType><br>
Award Behavior:<br>
<span class="bodytextboldblack"><AwardBehavior></span></p>
<p class="bodytextblack">To find out more, please login at <br>
<a href="http://www.baxterrewards.com">http://www.baxterrewards.com</a>.</p>
<p class="bodytextboldblack">***Please do not reply to this email***</p>
<p class="bodytextblack">&nbsp;</p></td></tr></table></BODY></HTML>' 
WHERE EmailType = 'D' AND LanguageCode = 'ENG'

UPDATE tbEmailTemplate SET MessageBodyTemplate = '<HTML><HEAD><TITLE>Email</TITLE><STYLE><!--.bodytextblack {font-size: 12px;font-weight: normal;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .bodytextboldblack {font-size: 12px;font-weight: bold;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;}--></STYLE></HEAD><BODY><table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td class="bodytextblack">Recipiente del Reconocimiento: <span class="bodytextboldblack"><RecipientFullName></span>
<p class="bodytextblack">Nega Por: <span class="bodytextboldblack"><ReviewingMgrFullName></span></p>
<p class="bodytextblack">Tipo de Recompensa: <AwardType><br>
Conducta Premiada:<br>
<span class="bodytextboldblack"><AwardBehavior></span></p>
<p class="bodytextblack">Para descubrir más, por favor de entrar a :<br>
<a href="http://www.baxterrewards.com">http://www.baxterrewards.com</a>.</p>
<p class="bodytextboldblack">***No conteste por favor a este email***</p>
<p class="bodytextblack">&nbsp;</p></td></tr></table></BODY></HTML>' 
WHERE EmailType = 'D' AND LanguageCode = 'ESP'

UPDATE tbEmailTemplate SET MessageBodyTemplate = '<HTML><HEAD><TITLE>Email</TITLE><STYLE><!--.bodytextblack {font-size: 12px;font-weight: normal;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .bodytextboldblack {font-size: 12px;font-weight: bold;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .pendingemailtable  {border-top: solid 1px black;border-left: solid 1px black;border-collapse: collapse;} .pendingemailtable td {border-right: solid 1px black;border-bottom: solid 1px black;padding: 8px;margin: 0px;}--></STYLE></HEAD><BODY><table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td class="bodytextblack">You have the following awards pending your review in the Baxter Rewards program.<br>
<p class="bodytextblack">Awards given <span class="bodytextboldblack">by</span> your direct reports:</p>
<TableGivenByDirRpts><p class="bodytextblack">Awards given <span class="bodytextboldblack">to</span> your direct reports:</p>
<TableReceivedByDirRpts><p class="bodytextblack">To deny any award, please login to Baxter Rewards at <a href="http://www.baxterrewards.com">http://www.baxterrewards.com</a>.</p>
<p class="bodytextblack">If no action to deny an award is taken by the recipient''s or giver''s managers, awards will be automatically approved 2 business days after initiation date.</p>
<p class="bodytextboldblack">***Please do not reply to this email***</p>
<p class="bodytextblack">&nbsp;</p></td></tr></table></BODY></HTML>' 
WHERE EmailType = 'P' AND LanguageCode = 'ENG'

UPDATE tbEmailTemplate SET MessageBodyTemplate = '<HTML><HEAD><TITLE>Email</TITLE><STYLE><!--.bodytextblack {font-size: 12px;font-weight: normal;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .bodytextboldblack {font-size: 12px;font-weight: bold;color: #000000;text-align: left;font-family: Arial, Helvetica, sans-serif;} .pendingemailtable  {border-top: solid 1px black;border-left: solid 1px black;border-collapse: collapse;} .pendingemailtable td {border-right: solid 1px black;border-bottom: solid 1px black;padding: 8px;margin: 0px;}--></STYLE></HEAD><BODY><table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td class="bodytextblack">Usted tiene los siguientes reconocimientos esperando su revision en el programa de Baxter Rewards.<br>
<p class="bodytextblack">Reconocimientos dado <span class="bodytextboldblack">por</span> su empleados:</p>
<TableGivenByDirRpts><p class="bodytextblack">Reconocimiento dado <span class="bodytextboldblack">a</span> su empleados:</p>
<TableReceivedByDirRpts><p class="bodytextblack">Para negar cualquier reconocimiento, por favor de entrar a las paginas de Baxter Rewards: <br>
<a href="http://www.baxterrewards.com">http://www.baxterrewards.com</a>. </p>
<p class="bodytextblack">Los reconocimientos serán aprobados automáticamente 2 días de trabajo después de la fecha de la iniciación en el caso que no se tome ninguna accion para negar los reconocimientos de parte del jefe del recipiente o el jefe del donante.</p>
<p class="bodytextboldblack">***No conteste por favor a este email***</p>
<p class="bodytextblack">&nbsp;</p></td></tr></table></BODY></HTML>' 
WHERE EmailType = 'P' AND LanguageCode = 'ESP'

-- Disallow nulls for MessageBodyTemplate
--ALTER TABLE dbo.tbEmailTemplate ALTER COLUMN MessageBodyTemplate VARCHAR(7000) NOT NULL

UPDATE dbo.tbEmailTemplate 
SET FromName = 'Baxter Award Headquarters',
MessageSubjectTemplate = 'Baxter Denied Award'
WHERE EmailType = 'D'
AND LanguageCode = 'ENG'

UPDATE dbo.tbEmailTemplate 
SET FromName = 'Jefaturas De la Concesión de Baxter',
MessageSubjectTemplate = 'Reconocimiento de Baxter Negado'
WHERE EmailType = 'D'
AND LanguageCode = 'ESP'

UPDATE dbo.tbEmailTemplate 
SET FromName = 'Baxter Award Headquarters',
MessageSubjectTemplate = 'Baxter Award Pending Your Review'
WHERE EmailType = 'P'
AND LanguageCode = 'ENG'

UPDATE dbo.tbEmailTemplate 
SET FromName = 'Jefaturas De la Concesión de Baxter',
MessageSubjectTemplate = 'Reconocimiento de Baxter Esperando Su Revisión'
WHERE EmailType = 'P'
AND LanguageCode = 'ESP'

