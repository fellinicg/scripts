-- Get Children
--
--SELECT DISTINCT position
SELECT *
FROM dbo.tbParentage
WHERE parent = '00004244'
AND diff = 1

-- Get Descendants
--
--SELECT DISTINCT position
SELECT *
FROM dbo.tbParentage
WHERE parent = '00004244'

-- Get Descendants at a certain level
--
--SELECT DISTINCT position
SELECT *
FROM dbo.tbParentage
WHERE parent = '00004244'
AND lvl = 4

-- Get Descendants at a certain depth
--
--SELECT DISTINCT position
SELECT *
FROM dbo.tbParentage
WHERE parent = '00004244'
AND diff = 3

-- Get Parent
--
--SELECT parent
SELECT *
FROM dbo.tbParentage
WHERE position = '00004244'
AND diff = 1

-- Get Ancestors
--
--SELECT DISTINCT parent
SELECT *
FROM dbo.tbParentage
WHERE position = '00007380'

-- Get Ancestors at a certain level
--
--SELECT DISTINCT parent
SELECT *
FROM dbo.tbParentage
WHERE position = '00007380'
AND (lvl - diff) = 2

-- Get Ancestors at a certain depth
--
--SELECT DISTINCT parent
SELECT *
FROM dbo.tbParentage
WHERE position = '00007380'
AND diff = 1
