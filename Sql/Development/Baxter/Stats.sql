SELECT 'Recognition Awards' AS 'Award', COUNT(AwardID) AS 'Awards Given', SUM(AwardAmtUSD) AS 'Total $ Awarded'
FROM dbo.tbAward
WHERE AwardType = 'R'
UNION
SELECT 'Pending Gift Cert Awards', COUNT(AwardID), SUM(AwardAmtUSD)
FROM dbo.tbAward
WHERE AwardType = 'G'
AND Status = 'P'
UNION
SELECT 'Approved Gift Cert Awards', COUNT(AwardID), ISNULL(SUM(AwardAmtUSD), 0)
FROM dbo.tbAward
WHERE AwardType = 'G'
AND Status = 'A'
UNION
SELECT 'Pending Debit Card Awards', COUNT(AwardID), SUM(AwardAmtUSD)
FROM dbo.tbAward
WHERE AwardType = 'D'
AND Status = 'P'
UNION
SELECT 'Approved Debit Card Awards', COUNT(AwardID), ISNULL(SUM(AwardAmtUSD), 0)
FROM dbo.tbAward
WHERE AwardType = 'D'
AND Status = 'A'

SELECT COUNT(SessionOwnerID) AS 'Number of Users', CONVERT(VARCHAR, StartDateTime, 101) AS 'Login Date'
FROM dbo.tbSession
WHERE SessionOwnerType = 'U'
GROUP BY CONVERT(VARCHAR, StartDateTime, 101)
ORDER BY CONVERT(VARCHAR, StartDateTime, 101)