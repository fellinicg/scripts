USE [Baxter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.upLoadLoadFile
 @LoadFileID int,
 @LoadFileStatus char(1) OUTPUT,
 @Verbose bit = 0
AS
DECLARE @UpdateEmployees table (EmployeeID int NOT NULL, RevokeMgrAccess bit, GrantMgrAccess bit, OldLastName varchar(30) NULL, OldFirstName varchar(30) NULL, OldCustEmployeeNum int NULL)
DECLARE @ReinstateEmployees table (EmployeeID int NOT NULL, CustEmployeeNum char(6) NOT NULL)
DECLARE @DeleteEmployees table (EmployeeID int NOT NULL)
DECLARE @CreateEmployees table (CustEmployeeNum char(6) NOT NULL, GrantMgrAccess bit, UserAccountID int NULL)
DECLARE @StatusComment varchar(800)
-- STATISTICS
DECLARE
	@AwardsCancelledCNT int,
	@CreateEmployeeCNT int,
	@DeleteEmployeeCNT int,
	@HierarchyRecCNT int,
	@LoadFileEligibleEmpCNT int,
	@LoadFileRecCNT int,
	@ReinstateEmployeeCNT int,
	@UpdateEmployeeCNT int,
	@ExistingUserMgrGrantCNT int,
	@ExistingUserMgrRevokeCNT int,
	@NewUserMgrGrantCNT int
-- STATUSES
DECLARE
	@GlobalUserEnabledStatus int,
	@GlobalUserDisabledStatus int,
	@UserAcctEnabledStatus char(1),
	@UserAcctDisabledStatus char(1),
	@UserAcctPendingStatus char(1),
	@AwardCancelledStatus char(1)
-- PROGRESS MARKERS
DECLARE
	@CreateGlobalUsers bit,
	@UpdateGlobalUsers bit,
	@EnableGlobalUsers bit,
	@DisableGlobalUsers bit
-- OTHER
DECLARE
	@IsFailure bit,
	@AllAccountGroupID int,
	@MgrAccountGroupID int,
	@AccountType char(1),
	@IsPerson bit,
	@AccountStatus char(1),
	@ClientID int,
	@Result char(1)

	SET NOCOUNT ON

	-- INITIALIZE/SET CONSTANTS
	SET @LoadFileStatus = 'E'
	SET @IsFailure = 0
	SET @CreateGlobalUsers = 0
	SET @UpdateGlobalUsers = 0
	SET @EnableGlobalUsers = 0
	SET @DisableGlobalUsers = 0
	SET @AllAccountGroupID = 1	-- FROM tbAccountGroup
	SET @MgrAccountGroupID = 2	-- FROM tbAccountGroup
	SET @AccountType = 'C'
	SET @IsPerson = 1
	SET @AccountStatus = 'E'
	EXEC dbo.upGetSettingValue @SettingLabel = 'BaxterClientID', @SettingValue = @ClientID OUTPUT

	-- VALIDATE INPUT PARAMS
	IF dbo.ufLoadFileStatus(@LoadFileID) <> 'H'	-- HIERARCHY VALIDATED
	BEGIN
		RAISERROR('upLoadLoadFile - @LoadFileID supplied does not have a record in tbLoadFile or does not have status (H)', 16, 1)
		RETURN
	END
	IF (EXISTS (SELECT 1 FROM tbLoadFileStaging WHERE LoadFileID <> @LoadFileID))
	BEGIN
		RAISERROR('upLoadLoadFile - records exist in tbLoadFileStaging for load file other than LoadFileID given', 16, 1)
		RETURN
	END
	IF (NOT EXISTS (SELECT 1 FROM tbLoadFileStaging))
	BEGIN
		RAISERROR('upLoadLoadFile - no records exist in tbLoadFileStaging', 16, 1)
		RETURN
	END
	IF (NOT EXISTS (SELECT 1 FROM tbPositionHierarchyStaging))
	BEGIN
		RAISERROR('upLoadLoadFile - no records exist in tbPositionHierarchyStaging', 16, 1)
		RETURN
	END

	-- SET VARS & CONSTANTS
	SET @GlobalUserEnabledStatus = 0
	SET @GlobalUserDisabledStatus = 1
	SET @UserAcctEnabledStatus = 'E'
	SET @UserAcctDisabledStatus = 'D'
	SET @UserAcctPendingStatus = 'P'
	SET @AwardCancelledStatus = 'C'

	-- POPULATE CONTROL TABLES
		-- TO BE UPDATED
	INSERT @UpdateEmployees(EmployeeID, RevokeMgrAccess, GrantMgrAccess)
	SELECT l.EmployeeID,
		CASE WHEN e.CustUserType = 'M' AND l.CustUserType = 'E' THEN 1 ELSE 0 END AS RevokeMgrAccess,
		CASE WHEN e.CustUserType = 'E' AND l.CustUserType = 'M' THEN 1 ELSE 0 END AS GrantMgrAccess
	FROM tbLoadFileStaging l
		INNER JOIN tbEmployee e ON (l.EmployeeID = e.EmployeeID)
	WHERE l.EmployeeID IS NOT NULL											-- MATCHES EMPLOYEE IN tbEmployee
		AND l.IsEligibleEmployee = 1											-- IS ELIGIBLE FOR PROGRAM PARTICIPATION
		AND l.NewValsCheckSum <> l.ExistingValsCheckSum		-- SOME VALUES HAVE CHANGED
	SET @UpdateEmployeeCNT = @@ROWCOUNT
		-- TO BE REINSTATED
	INSERT @ReinstateEmployees(EmployeeID, CustEmployeeNum)
	SELECT u.EmployeeID, e.CustEmployeeNum
	FROM @UpdateEmployees u
		INNER JOIN tbEmployee e ON (u.EmployeeID = e.EmployeeID AND e.IsDeleted = 1)	-- HAS MATCHING RECORD BUT IS DELETED
	SET @ReinstateEmployeeCNT = @@ROWCOUNT
		-- TO BE DELETED
	INSERT @DeleteEmployees(EmployeeID)
	SELECT e.EmployeeID
	FROM tbEmployee e
		LEFT OUTER JOIN tbLoadFileStaging l ON (e.EmployeeID = l.EmployeeID)
	WHERE e.IsDeleted = 0																													 -- CURRENTLY ACTIVE EMPLOYEE
		AND l.EmployeeID IS NULL																											-- NOT AN ELIGIBLE EMPLOYEE IN LOAD FILE
	SET @DeleteEmployeeCNT = @@ROWCOUNT
		-- TO BE CREATED
	INSERT @CreateEmployees(CustEmployeeNum, GrantMgrAccess)
	SELECT CustEmployeeNum, CASE CustUserType WHEN 'M' THEN 1 ELSE 0 END AS GrantMgrAccess
	FROM tbLoadFileStaging
	WHERE IsEligibleEmployee = 1																										-- ELIGIBLE EMPLOYEE IN LOAD FILE
		AND EmployeeID IS NULL																												-- WITH NO MATCH IN EMPLOYEE TABLE
	SET @CreateEmployeeCNT = @@ROWCOUNT

	-- GET ACCT GROUP ACCESS STATS
	SELECT @ExistingUserMgrRevokeCNT = COUNT(*) FROM @UpdateEmployees WHERE RevokeMgrAccess = 1
	SELECT @ExistingUserMgrGrantCNT = COUNT(*) FROM @UpdateEmployees WHERE GrantMgrAccess = 1
	SELECT @NewUserMgrGrantCNT = COUNT(*) FROM @CreateEmployees WHERE GrantMgrAccess = 1

	-- TEST OUTPUT
	IF (@Verbose = 1)
	BEGIN
		SELECT
			@UpdateEmployeeCNT AS UpdateEmployeeCNT,
			@ReinstateEmployeeCNT AS ReinstateEmployeeCNT,
			@DeleteEmployeeCNT AS DeleteEmployeeCNT,
			@CreateEmployeeCNT AS CreateEmployeeCNT
		IF (@UpdateEmployeeCNT <> 0)
		BEGIN
			PRINT 'UpdateEmployeesList'
			SELECT EmployeeID FROM @UpdateEmployees
		END
		IF (@ReinstateEmployeeCNT <> 0)
		BEGIN
			PRINT 'ReinstateEmployeeList'
			SELECT EmployeeID, CustEmployeeNum FROM @ReinstateEmployees
		END
		IF (@DeleteEmployeeCNT <> 0)
		BEGIN
			PRINT 'DeleteEmployeeList'
			SELECT EmployeeID FROM @DeleteEmployees
		END
		IF (@CreateEmployeeCNT <> 0)
		BEGIN
			PRINT 'CreateEmployeeList'
			SELECT CustEmployeeNum FROM @CreateEmployees
		END
	END

	-->> PERFORM GLOBAL USER OPERATIONS FIRST AS THEY CANNOT BE INCLUDED IN TRANSACTION BLOCKING <<--

	DECLARE
		@CurrEmpNum char(6),
		@UserAccountID int,
		@LastName varchar(30),
		@FirstName varchar(30)

	-- CREATE NEW GLOBAL USERS
	IF (@CreateEmployeeCNT <> 0)
	BEGIN
		SET @CreateGlobalUsers = 1
		SELECT @CurrEmpNum = MIN(CustEmployeeNum) FROM @CreateEmployees
		IF (@Verbose = 1)
		BEGIN
			PRINT 'CREATE: GLOBAL USERS'
			SELECT @CurrEmpNum as InitialCurrEmpNum
		END
		WHILE (ISNULL(@CurrEmpNum, '') <> '')
		BEGIN
			-- GET INFO
			SELECT @LastName = LastName, @FirstName = FirstName
			FROM tbLoadFileStaging WHERE CustEmployeeNum = @CurrEmpNum
			-- CREATE GLOBAL USER
			SET @UserAccountID = NULL
			EXECUTE SQL2KPROD.GlobalUsers.dbo.upAddUserAccount
				 @AccountType = @AccountType
				,@IsPerson = @IsPerson
				,@ClientID = @ClientID
				,@CustAccountCode = @CurrEmpNum
				,@PersonFirstGivenName = @FirstName
				,@PersonFamilyName = @LastName
				,@AccountStatus = @AccountStatus
				,@UserAccountID = @UserAccountID OUTPUT
			-- ERROR HANDLER
			IF (ISNULL(@UserAccountID, 0) = 0)
			BEGIN
				IF (@Verbose = 1)
					PRINT 'CALL FAILURE: upAddUserAccount'
				SET @IsFailure = 1
				SET @StatusComment = 'upLoadLoadFile - failed to create a record in GlobalUser'
				BREAK
			END
			ELSE
				UPDATE @CreateEmployees SET UserAccountID = @UserAccountID WHERE CustEmployeeNum = @CurrEmpNum
			-- GET NEXT CANDIDATE
			SELECT @CurrEmpNum = MIN(CustEmployeeNum) FROM @CreateEmployees WHERE CustEmployeeNum > @CurrEmpNum
			IF (@Verbose = 1)
				SELECT @CurrEmpNum AS NextEmpNum
		END
	END

	DECLARE
		@CurrEmployeeID int,
		@OldLastName varchar(30),
		@OldFirstName varchar(30),
		@CustEmployeeNum int

	-- UPDATE EXISTING GLOBAL USERS
  IF (@IsFailure = 0 AND @UpdateEmployeeCNT <> 0)
  BEGIN
    SET @UpdateGlobalUsers = 1
    IF (@Verbose = 1)
    BEGIN
      PRINT 'UPDATE: GLOBAL USERS'
      SELECT @CurrEmployeeID as InitialEmployeeID
    END    
		-- SET OLD VALUES FOR CLEANUP
		UPDATE @UpdateEmployees SET
			OldLastName = ge.LastName,
			OldFirstName = ge.FirstName,
			OldCustEmployeeNum = ge.CustEmployeeNum
		FROM @UpdateEmployees u
			INNER JOIN vwGlobalEmployee ge ON (u.EmployeeID = ge.EmployeeID)
		IF (@@ROWCOUNT <> @UpdateEmployeeCNT)
		BEGIN
			IF (@Verbose = 1)
				PRINT 'FAILURE: setting old values for update global users'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadLoadFile - failed to get old values from GlobalUser'
		END    
    SELECT @CurrEmployeeID = MIN(EmployeeID) FROM @UpdateEmployees
    WHILE (ISNULL(@CurrEmployeeID, 0) <> 0)
    BEGIN
      SET @UserAccountID = NULL
      SET @UserAccountID = @CurrEmployeeID
      -- GET NEW VALUES
      SELECT @LastName = LastName, @FirstName = FirstName, @CustEmployeeNum = CustEmployeeNum
      FROM tbLoadFileStaging WHERE EmployeeID = @CurrEmployeeID
      -- UPDATE GLOBAL USER
			EXECUTE SQL2KPROD.GlobalUsers.dbo.upUpdateUserAccount
				 @Result = @Result OUTPUT
				,@UserAccountID = @UserAccountID
				,@UpdCustAccountCode = 1
				,@CustAccountCode = @CustEmployeeNum
				,@UpdPersonFirstGivenName = 1
				,@PersonFirstGivenName = @FirstName
				,@UpdPersonFamilyName = 1
				,@PersonFamilyName = @LastName
      -- ERROR HANDLER
      IF (@UserAccountID = 0)
      BEGIN
        IF (@Verbose = 1)
          PRINT 'CALL FAILURE: upUpdateUserAccount'
        SET @IsFailure = 1
        SET @StatusComment = 'upLoadLoadFile - failed to update a record in GlobalUser'
        BREAK
      END
      -- GET NEXT CANDIDATE
      SELECT @CurrEmployeeID = MIN(EmployeeID) FROM @UpdateEmployees WHERE EmployeeID > @CurrEmployeeID
      IF (@Verbose = 1)
        SELECT @CurrEmployeeID AS NextEmployeeID
    END
  END

	-- DISABLE GLOBAL USERS
	IF (@IsFailure = 0 AND @DeleteEmployeeCNT <> 0)
	BEGIN
		SET @DisableGlobalUsers = 1
		IF (@Verbose = 1)
			PRINT 'DISABLE: GLOBAL USERS'
		UPDATE SQL2KPROD.GlobalUsers.dbo.tbUserAccount SET 
			AccountStatus = 'D', -- DISABLED
			StatusDateTime = CURRENT_TIMESTAMP,
			UpdateDateTime = CURRENT_TIMESTAMP
		FROM SQL2KPROD.GlobalUsers.dbo.tbUserAccount u
			INNER JOIN @DeleteEmployees d ON (u.UserAccountID = d.EmployeeID)
		-- ERROR HANDLER
		IF (@@ROWCOUNT <> @DeleteEmployeeCNT)
		BEGIN
			IF (@Verbose = 1)
				PRINT 'FAILURE: DELETE COUNT MISMATCH'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadLoadFile - failed to disable a record in GlobalUser'
		END
	END

	-- ENABLE GLOBAL USERS
	IF (@IsFailure = 1 AND @ReinstateEmployeeCNT <> 0)
	BEGIN
		SET @EnableGlobalUsers = 1
		IF (@Verbose = 1)
			PRINT 'ENABLE: GLOBAL USERS'
		UPDATE SQL2KPROD.GlobalUsers.dbo.tbUserAccount SET 
			AccountStatus = 'E', 	-- ENABLED
			StatusDateTime = CURRENT_TIMESTAMP,
			UpdateDateTime = CURRENT_TIMESTAMP
		FROM SQL2KPROD.GlobalUsers.dbo.tbUserAccount u
			INNER JOIN @ReinstateEmployees r ON (u.UserAccountID = r.EmployeeID)
		-- ERROR HANDLER
		IF (@@ROWCOUNT <> @ReinstateEmployeeCNT)
		BEGIN
			IF (@Verbose = 1)
				PRINT 'FAILURE: REINSTATE COUNT MISMATCH'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadLoadFile - failed to enable a record in GlobalUser'
		END
	END

	-->> PERFORM LOCAL DB OPERATIONS PROTECTED BY TRANSACTION BLOCKING <<--

	-- OPEN TRAN
	IF (@IsFailure = 0)
	BEGIN
		IF (@Verbose = 1)
			PRINT 'STARTING TRAN'
		BEGIN TRAN
	END

	-- UPDATE EMPLOYEES/ACCTS
	IF (@IsFailure = 0 AND @UpdateEmployeeCNT <> 0)
	BEGIN
		IF (@Verbose = 1)
			PRINT 'UPDATES: EMPLOYEES'
		UPDATE tbEmployee SET
			CustEmployeeNum = l.CustEmployeeNum,
			FullName = l.FullName,
			EmployeeStatus = l.EmployeeStatus,
			MonetaryAwdRcvIneligible = l.MonetaryAwdRcvIneligible,
			WorkPhoneNum = l.WorkPhoneNum,
			WorkEmailAddress = l.WorkEmailAddress,
			WorkAddressLine1 = l.WorkAddressLine1,
			WorkAddressLine2 = l.WorkAddressLine2,
			WorkAddressLine3 = l.WorkAddressLine3,
			WorkAddressLine4 = l.WorkAddressLine4,
			WorkAddressCity = l.WorkAddressCity,
			WorkAddressState = l.WorkAddressState,
			WorkAddressPostalCode = l.WorkAddressPostalCode,
			WorkAddressCountryCode = l.WorkAddressCountryCode,
			HomeAddressLine1 = l.HomeAddressLine1,
			HomeAddressLine2 = l.HomeAddressLine2,
			HomeAddressLine3 = l.HomeAddressLine3,
			HomeAddressLine4 = l.HomeAddressLine4,
			HomeAddressCity = l.HomeAddressCity,
			HomeAddressState = l.HomeAddressState,
			HomeAddressPostalCode = l.HomeAddressPostalCode,
			HomeAddressCountryCode = l.HomeAddressCountryCode,
			CustUserType = l.CustUserType,
			PayGroup = l.PayGroup,
			PaySystemFlag = l.PaySystemFlag,
			BxSMTName = l.BxSMTName,
			CustPositionNum = l.CustPositionNum,
			CorpJobCode = l.CorpJobCode,
			CorpJobDesc = l.CorpJobDesc,
			JobCode = l.JobCode,
			JobDesc = l.JobDesc,
			MgrLevel = l.MgrLevel,
			MgrLevelDesc = l.MgrLevelDesc,
			DeptSetCode = l.DeptSetCode,
			DeptCode = l.DeptCode,
			DeptName = l.DeptName,
			LocationSetCode = l.LocationSetCode,
			LocationCode = l.LocationCode,
			LocationName = l.LocationName,
			CompanyCode = l.CompanyCode,
			LegalEntity = l.LegalEntity,
			JDECode = l.JDECode,
			MasterBPG = l.MasterBPG,
			BxPlanFlag = l.BxPlanFlag,
			BxOptionTarget = l.BxOptionTarget,
			BxUnitDesc = l.BxUnitDesc,
			BxBusDesc = l.BxBusDesc,
			BxSubBusDesc = l.BxSubBusDesc,
			BxGroupDesc = l.BxGroupDesc,
			BusinessTitle = l.BusinessTitle,
			UpdateDateTime = CURRENT_TIMESTAMP
		FROM @UpdateEmployees u
			INNER JOIN tbLoadFileStaging l ON (u.EmployeeID = l.EmployeeID)
			INNER JOIN tbEmployee e ON (l.EmployeeID = e.EmployeeID)
		-- ERROR HANDLER
		IF (@@ROWCOUNT <> @UpdateEmployeeCNT)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: UPDATES COUNT MISMATCH'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadLoadFile - failed to update a record in tbEmployee'
		END
		-- REVOKE MANAGER ACCESS
		IF (@Verbose = 1)
			PRINT 'UPDATES: REVOKE MANAGER ACCESS'
		DELETE tbAccountGroupMember
		FROM tbAccountGroupMember agm
			INNER JOIN @UpdateEmployees e ON (agm.UserAccountID = e.EmployeeID
																					AND e.RevokeMgrAccess = 1)
		WHERE agm.AccountGroupID = @MgrAccountGroupID
		-- ERROR HANDLER
		IF (@@ROWCOUNT <> @ExistingUserMgrRevokeCNT)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: EXISTING USER REVOKE MANAGER ACCESS'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadLoadFile - failed to revoke manager access for existing user'
		END
		-- GRANT MANAGER ACCESS
		IF (@Verbose = 1)
			PRINT 'UPDATES: GRANT MANAGER ACCESS'
		INSERT tbAccountGroupMember(AccountGroupID, UserAccountID)
		SELECT @MgrAccountGroupID, EmployeeID FROM @UpdateEmployees WHERE GrantMgrAccess = 1
		-- ERROR HANDLER
		IF (@@ROWCOUNT <> @ExistingUserMgrGrantCNT)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: EXISTING USER GRANT MANAGER ACCESS'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadLoadFile - failed to grant manager access for existing user'
		END
	END

	-- DELETE EMPLOYEES/ACCTS
	IF (@IsFailure = 0 AND @DeleteEmployeeCNT <> 0)
	BEGIN
		-- DISABLE USER ACCTS
		IF (@Verbose = 1)
			PRINT 'DELETES: DISABLE ACCT'
		UPDATE tbUserAccount SET Status = @UserAcctDisabledStatus, StatusDateTime = CURRENT_TIMESTAMP
		FROM tbUserAccount a
			INNER JOIN @DeleteEmployees d ON (a.UserAccountID = d.EmployeeID)
		-- ERROR HANDLER
		IF (@@ROWCOUNT <> @DeleteEmployeeCNT)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: DISABLE ACCT COUNT MISMATCH'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadLoadFile - failed to disable a record in tbUserAccount'
		END
		-- DELETE EMPLOYEES
		IF (@IsFailure = 0)
		BEGIN
			IF (@Verbose = 1)
				PRINT 'DELETES: DELETE EMPLOYEE'
			UPDATE tbEmployee SET IsDeleted = 1, UpdateDateTime = CURRENT_TIMESTAMP
			FROM tbEmployee e
				INNER JOIN @DeleteEmployees d ON (e.EmployeeID = d.EmployeeID)
			-- ERROR HANDLER
			IF (@@ROWCOUNT <> @DeleteEmployeeCNT)
			BEGIN
				ROLLBACK TRAN
				IF (@Verbose = 1)
					PRINT 'FAILURE: DELETE EMP COUNT MISMATCH'
				SET @IsFailure = 1
				SET @StatusComment = 'upLoadLoadFile - failed to delete a record in tbEmployee'
			END
		END
		-- CANCEL PENDING AWARDS
		IF (@IsFailure = 0)
		BEGIN
			IF (@Verbose = 1)
				PRINT 'DELETES: CANCEL AWARDS'
			UPDATE tbAward SET
				TermRecipMgrEmployeeID = InitRecipMgrEmployeeID,
				TermRecipMgrCustEmployeeNum = InitRecipMgrCustEmployeeNum,
				TermRecipMgrCustPositionNum = InitRecipMgrCustPositionNum,
				TermRecipMgrLocationSetCode = InitRecipMgrLocationSetCode,
				TermRecipMgrBusinessTitle = InitRecipMgrBusinessTitle,
				TermGiverMgrEmployeeID = InitGiverMgrEmployeeID,
				TermGiverMgrCustEmployeeNum = InitGiverMgrCustEmployeeNum,
				TermGiverMgrCustPositionNum = InitGiverMgrCustPositionNum,
				TermGiverMgrLocationSetCode = InitGiverMgrLocationSetCode,
				TermGiverMgrBusinessTitle = InitGiverMgrBusinessTitle,
				Status = @AwardCancelledStatus,
				StatusDateTime = CURRENT_TIMESTAMP
			FROM tbAward a
				INNER JOIN @DeleteEmployees d ON (a.RecipEmployeeID = d.EmployeeID)
			WHERE a.Status = 'P'	-- PENDING
			SET @AwardsCancelledCNT = @@ROWCOUNT
		END
		-- REVOKE ACCT GROUP ACCESS
		IF (@Verbose = 1)
			PRINT 'DELETES: REVOKE ACCT GROUP ACCESS'
		DELETE tbAccountGroupMember
		FROM tbAccountGroupMember m
			INNER JOIN @DeleteEmployees e ON (m.UserAccountID = e.EmployeeID)
		-- ERROR HANDLER
		IF (@@ERROR <> 0)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: REVOKE ACCT GROUP ACCESS'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadLoadFile - failed to delete memberships for a deleted employee'
		END
	END

	-- REINSTATE EMPLOYEES/ACCTS
	IF (@IsFailure = 0 AND @ReinstateEmployeeCNT <> 0)
	BEGIN
		-- REINITIALIZE USER ACCT
		IF (@Verbose = 1)
			PRINT 'REINSTATES: REINIT ACCT'
		UPDATE tbUserAccount SET
			Status = @UserAcctPendingStatus,	-- REINSTATED ACCTS MUST BE SETUP BY USER AGAIN
			StatusDateTime = CURRENT_TIMESTAMP,
			LoginName = dbo.ufNewLoginName(r.CustEmployeeNum),
			PasswordCrypted = NULL,
			PasswordChangedDateTime = NULL,
			SecurityQuestion = NULL,
			SecurityAnswer = NULL,
			PreferredLanguageCode = DEFAULT
		FROM tbUserAccount a
			INNER JOIN @ReinstateEmployees r ON (a.UserAccountID = r.EmployeeID)
		-- ERROR HANDLER
		IF (@@ROWCOUNT <> @ReinstateEmployeeCNT)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: REINIT ACCT COUNT MISMATCH'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadLoadFile - failed to reinitialize a record in tbUserAccount'
		END
		-- UNDELETE EMPLOYEE
		IF (@IsFailure = 0)
		BEGIN
			IF (@Verbose = 1)
				PRINT 'REINSTATES: UNDELETE EMP'
			UPDATE tbEmployee SET IsDeleted = 0, UpdateDateTime = CURRENT_TIMESTAMP
			FROM tbEmployee e
				INNER JOIN @ReinstateEmployees r ON (e.EmployeeID = r.EmployeeID)
			-- ERROR HANDLER
			IF (@@ROWCOUNT <> @ReinstateEmployeeCNT)
			BEGIN
				ROLLBACK TRAN
				IF (@Verbose = 1)
					PRINT 'FAILURE: UNDELETE EMP COUNT MISMATCH'
				SET @IsFailure = 1
				SET @StatusComment = 'upLoadLoadFile - failed to undelete a record in tbEmployee'
			END
		END
	END

	-- INSERT EMPLOYEES/ACCTS
	IF (@IsFailure = 0 AND @CreateEmployeeCNT <> 0)
	BEGIN
		-- CREATE USER ACCTS
		IF (@Verbose = 1)
			PRINT 'CREATES: CREATE ACCT'
		INSERT tbUserAccount(UserAccountID, LoginName, PreferredCurrencyCode, Status, StatusDateTime, CreateDateTime)
		SELECT UserAccountID, dbo.ufNewLoginName(CustEmployeeNum), 'USD', @UserAcctPendingStatus, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
		FROM @CreateEmployees
		-- ERROR HANDLER
		IF (@@ROWCOUNT <> @CreateEmployeeCNT)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: CREATE ACCT COUNT MISMATCH'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadLoadFile - failed to create a record in tbUserAccount'
			RETURN
		END
		-- CREATE EMPLOYEES
		IF (@Verbose = 1)
			PRINT 'CREATES: CREATE EMP'
		INSERT tbEmployee(EmployeeID, CustEmployeeNum, FullName, EmployeeStatus, MonetaryAwdRcvIneligible, WorkPhoneNum,
			WorkEmailAddress, WorkAddressLine1, WorkAddressLine2, WorkAddressLine3, WorkAddressLine4, WorkAddressCity,
			WorkAddressState, WorkAddressPostalCode, WorkAddressCountryCode, HomeAddressLine1, HomeAddressLine2,
			HomeAddressLine3, HomeAddressLine4, HomeAddressCity, HomeAddressState, HomeAddressPostalCode,
			HomeAddressCountryCode, CustUserType, PayGroup, PaySystemFlag, BxSMTName, CustPositionNum, CorpJobCode,
			CorpJobDesc, JobCode, JobDesc, MgrLevel, MgrLevelDesc, DeptSetCode, DeptCode, DeptName, LocationSetCode,
			LocationCode, LocationName, CompanyCode, LegalEntity, JDECode, MasterBPG, BxPlanFlag, BxOptionTarget,
			BxUnitDesc, BxBusDesc, BxSubBusDesc, BxGroupDesc, BusinessTitle, CreateDateTime, UpdateDateTime, IsDeleted)
		SELECT c.UserAccountID, l.CustEmployeeNum, l.FullName, l.EmployeeStatus, l.MonetaryAwdRcvIneligible, l.WorkPhoneNum,
			l.WorkEmailAddress, l.WorkAddressLine1, l.WorkAddressLine2, l.WorkAddressLine3, l.WorkAddressLine4,
			l.WorkAddressCity, l.WorkAddressState, l.WorkAddressPostalCode, l.WorkAddressCountryCode, l.HomeAddressLine1,
			l.HomeAddressLine2, l.HomeAddressLine3, l.HomeAddressLine4, l.HomeAddressCity, l.HomeAddressState,
			l.HomeAddressPostalCode, l.HomeAddressCountryCode, l.CustUserType, l.PayGroup, l.PaySystemFlag, l.BxSMTName,
			l.CustPositionNum, l.CorpJobCode, l.CorpJobDesc, l.JobCode, l.JobDesc, l.MgrLevel, l.MgrLevelDesc, l.DeptSetCode,
			l.DeptCode, l.DeptName, l.LocationSetCode, l.LocationCode, l.LocationName, l.CompanyCode, l.LegalEntity,
			l.JDECode, l.MasterBPG, l.BxPlanFlag, l.BxOptionTarget, l.BxUnitDesc, l.BxBusDesc, l.BxSubBusDesc, l.BxGroupDesc,
			l.BusinessTitle, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 0
		FROM @CreateEmployees c
			INNER JOIN tbLoadFileStaging l ON (c.CustEmployeeNum = l.CustEmployeeNum)
		-- ERROR HANDLER
		IF (@@ROWCOUNT <> @CreateEmployeeCNT)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: CREATE EMP COUNT MISMATCH'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadLoadFile - failed to create a record in tbEmployee'
		END
		-- GRANT ALL GROUP ACCESS
		IF (@Verbose = 1)
			PRINT 'CREATES: GRANT ALL GROUP ACCESS'
		INSERT tbAccountGroupMember(AccountGroupID, UserAccountID)
		SELECT @AllAccountGroupID, UserAccountID FROM @CreateEmployees
		-- ERROR HANDLER
		IF (@@ROWCOUNT <> @CreateEmployeeCNT)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: NEW USER GRANT ALL GROUP ACCESS'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadLoadFile - failed to grant all group access for a new employee'
		END
		-- GRANT MANAGER ACCESS
		IF (@Verbose = 1)
			PRINT 'CREATES: NEW USER GRANT MANAGER ACCESS'
		INSERT tbAccountGroupMember(AccountGroupID, UserAccountID)
		SELECT @MgrAccountGroupID, UserAccountID FROM @CreateEmployees WHERE GrantMgrAccess = 1
		-- ERROR HANDLER
		IF (@@ROWCOUNT <> @NewUserMgrGrantCNT)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: NEW USER GRANT MANAGER ACCESS'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadLoadFile - failed to grant manager access for a new employee'
		END
	END

	-- REBUILD POSITION HIERARCHY
	IF (@IsFailure = 0)
	BEGIN
		SELECT @HierarchyRecCNT = COUNT(*) FROM tbPositionHierarchyStaging
		IF (@Verbose = 1)
			PRINT 'HIERARCHY: DELETE'
		DELETE tbPositionHierarchy
		IF (@Verbose = 1)
			PRINT 'HIERARCHY: CREATE'
		INSERT tbPositionHierarchy(CustPositionNum, AncestorCustPositionNum, Tier, Diff, HasChildren)
		SELECT CustPositionNum, AncestorCustPositionNum, Tier, Diff, HasChildren FROM tbPositionHierarchyStaging
		-- ERROR HANDLER
		IF (@@ROWCOUNT <> @HierarchyRecCNT)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: CREATE HIERARCHY COUNT MISMATCH'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadLoadFile - failed to create a record in tbPositionHierarchy'
		END
	END

	-- COMMIT TRAN
	IF (@IsFailure = 0)
	BEGIN
		IF (@Verbose = 1)
			PRINT 'COMMITTING TRAN'
		COMMIT TRAN
	END

	-- BUILD STATISTICS
	IF (@IsFailure = 0)
	BEGIN
		SELECT @LoadFileRecCNT = COUNT(*) FROM tbLoadFileStaging
		SELECT @LoadFileEligibleEmpCNT = COUNT(*) FROM tbLoadFileStaging WHERE IsEligibleEmployee = 1
		SET @StatusComment = 'FileRecords - ' + CONVERT(varchar(20), @LoadFileRecCNT)
			+ '; FileEligibleEmployees - ' + CONVERT(varchar(20), @LoadFileEligibleEmpCNT)
			+ '; CreatedEmployees - ' + CONVERT(varchar(20), @CreateEmployeeCNT)
			+ '; UpdatedEmployees - ' + CONVERT(varchar(20), @UpdateEmployeeCNT)
			+ '; DeletedEmployees - ' + CONVERT(varchar(20), @DeleteEmployeeCNT)
			+ '; ReinstatedEmployees - ' + CONVERT(varchar(20), @ReinstateEmployeeCNT)
			+ '; PendingAwardsCancelled - ' + CONVERT(varchar(20), ISNULL(@AwardsCancelledCNT, 0))
			+ '; HierarchyRecordsCreated - ' + CONVERT(varchar(20), ISNULL(@HierarchyRecCNT, 0))
			+ '; ExistingUserManagerGrants - ' + CONVERT(varchar(20), ISNULL(@ExistingUserMgrGrantCNT, 0))
			+ '; ExistingUserManagerRevokes - ' + CONVERT(varchar(20), ISNULL(@ExistingUserMgrRevokeCNT, 0))
			+ '; NewUserManagerGrants - ' + CONVERT(varchar(20), ISNULL(@NewUserMgrGrantCNT, 0))
		IF (@Verbose = 1)
		BEGIN
			PRINT 'STATISTICS: BUILD'
			SELECT @StatusComment AS StatusComment
		END
	END

	-- SET SUCCESS STATUS & CLEANUP
	IF (@IsFailure = 0)
	BEGIN
		IF (@Verbose = 1)
			PRINT 'SETTING SUCCESS STATUS'
		SET @LoadFileStatus = 'L'	-- LOADED
		IF (@Verbose = 1)
			PRINT 'CLEANUP: DEL HIERARCHY STAGING'
		DELETE tbPositionHierarchyStaging
		IF (@Verbose = 1)
			PRINT 'CLEANUP: DEL LOAD FILE STAGING'
		DELETE tbLoadFileStaging
	END

	-- FAILURE HANDLER: OPS PERFORMED IN REMOTE DB MUST BE EXPLICITLY UNDONE AS THEY COULD NOT PARTICIPATE IN LOCAL TRAN
	IF (@IsFailure = 1)
	BEGIN
		IF (@CreateGlobalUsers = 1)
		BEGIN
			IF (@Verbose = 1)
				PRINT 'CREATE GLOBAL USERS: CLEANUP'
			EXEC SQL2KPROD.GlobalUsers.dbo.upSettdUserAccount 'DISABLE'
			DELETE SQL2KPROD.GlobalUsers.dbo.tbUserAccount
			FROM SQL2KPROD.GlobalUsers.dbo.tbUserAccount g
				INNER JOIN @CreateEmployees c ON (g.UserAccountID = c.UserAccountID)
			EXEC SQL2KPROD.GlobalUsers.dbo.upSettdUserAccount 'ENABLE'
		END
		IF (@UpdateGlobalUsers = 1)
		BEGIN
			IF (@Verbose = 1)
				PRINT 'UPDATE GLOBAL USERS: CLEANUP'
			-- SET NAMES BACK TO OLD VALUES
			UPDATE SQL2KPROD.GlobalUsers.dbo.tbUserAccount SET
				PersonFirstGivenName = u.OldLastName,
				PersonFamilyName = u.OldFirstName,
				UpdateDateTime = CURRENT_TIMESTAMP
			FROM SQL2KPROD.GlobalUsers.dbo.tbUserAccount g
				INNER JOIN @UpdateEmployees u ON (g.UserAccountID = u.EmployeeID AND u.OldLastName IS NOT NULL)
		END
		IF (@EnableGlobalUsers = 1)
		BEGIN
			IF (@Verbose = 1)
				PRINT 'ENABLE GLOBAL USERS: CLEANUP'
			UPDATE SQL2KPROD.GlobalUsers.dbo.tbUserAccount SET 
				AccountStatus= 'E',	-- ENABLED
				StatusDateTime = CURRENT_TIMESTAMP,
				UpdateDateTime = CURRENT_TIMESTAMP
			FROM SQL2KPROD.GlobalUsers.dbo.tbUserAccount u
				INNER JOIN @DeleteEmployees d ON (u.UserAccountID = d.EmployeeID)
		END
		IF (@DisableGlobalUsers = 1)
		BEGIN
			IF (@Verbose = 1)
				PRINT 'DISABLE GLOBAL USERS: CLEANUP'
			UPDATE SQL2KPROD.GlobalUsers.dbo.tbUserAccount SET 
				AccountStatus= 'D',	-- DISABLED
				StatusDateTime = CURRENT_TIMESTAMP,
				UpdateDateTime = CURRENT_TIMESTAMP
			FROM SQL2KPROD.GlobalUsers.dbo.tbUserAccount u
				INNER JOIN @ReinstateEmployees r ON (u.UserAccountID = r.EmployeeID)
		END
	END

	-- SET LOAD FILE STATUS
	EXEC dbo.upSetLoadFileStatus @LoadFileID = @LoadFileID, @LoadFileStatus = @LoadFileStatus, @StatusComment = @StatusComment

	SET NOCOUNT OFF


