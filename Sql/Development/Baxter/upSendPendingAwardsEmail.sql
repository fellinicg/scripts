USE [Baxter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE dbo.upSendPendingAwardsEmail
	@EmployeeID int,
	@IsSuccess bit OUTPUT
AS
DECLARE
	@EmailType char(1),
	@EmpLangCode char(3),
	@EmployeeFullName varchar(80),
	@EmployeeEmailAddress varchar(120),
	@MessageBodyTemplate varchar(6000),
	@MessageSubjectTemplate varchar(120),
	@PendingEmailStatus char(1),
	@SMTPContentType varchar(10),
	@SMTPPriority varchar(6),
	@ToEmailAddressList varchar(400),
	@FromName varchar(80),
	@FromEmailAddress varchar(120),
	@ReplyToEmailAddress varchar(120),
	@CCEmailAddressList varchar(400),
	@BCCEmailAddressList varchar(400),
	@EmailID int,
	@EmailTemplateID int

	SET NOCOUNT ON

	SET @IsSuccess = 0
	SET @EmailType = 'P'  -- PENDING AWARD NOTICE
	SET @PendingEmailStatus = 'P'

	-- CHECK INPUTS
	SELECT @EmpLangCode = dbo.ufAccountPreferredLanguage(@EmployeeID)
	IF (@EmpLangCode = 'XXX')  -- FUNC FAILURE INDICATOR
		RETURN

	-- GET EMPLOYEE INFO
	SELECT
		@EmployeeFullName = dbo.ufEmployeeGUFullName(EmployeeID),
		@EmployeeEmailAddress = WorkEmailAddress
	FROM tbEmployee
	WHERE EmployeeID = @EmployeeID
		AND WorkEmailAddress IS NOT NULL
		AND IsDeleted = 0
	IF (@@ROWCOUNT = 0)
		RETURN
	SET @ToEmailAddressList = @EmployeeFullName + ' <' + @EmployeeEmailAddress + '>'

	-- GET EMAIL TEMPLATE INFO
	SELECT
		@FromName = FromName,
		@FromEmailAddress = FromEmailAddress,
		@ReplyToEmailAddress = ReplyToEmailAddress,
		@CCEmailAddressList = CCEmailAddressList,
		@BCCEmailAddressList = BCCEmailAddressList,
		@MessageSubjectTemplate = MessageSubjectTemplate,
		@MessageBodyTemplate = MessageBodyTemplate,
		@SMTPContentType = SMTPContentType,
		@SMTPPriority = SMTPPriority,
		@EmailTemplateID = EmailTemplateID
	FROM tbEmailTemplate
	WHERE EmailType = @EmailType
		AND LanguageCode = @EmpLangCode
	IF (@@ROWCOUNT = 0)
		RETURN

	-- BUILD MESSAGE BODY
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<TableGivenByDirRpts>', ISNULL(dbo.ufDirectReportsPendingAwardsTable(@EmployeeID, 'G', CURRENT_TIMESTAMP), 'NONE'))
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<TableReceivedByDirRpts>', ISNULL(dbo.ufDirectReportsPendingAwardsTable(@EmployeeID, 'R', CURRENT_TIMESTAMP), 'NONE'))

	-- CREATE tbOutboundEmail RECORD
	INSERT tbOutboundEmail(EmailTemplateID, ToEmailAddressList, FromName, FromEmailAddress,
		ReplyToEmailAddress, CCEmailAddressList, BCCEmailAddressList, MessageSubject, MessageBody, SMTPContentType,
		SMTPPriority, Status, StatusDateTime, ToEmployeeID)
	VALUES(@EmailTemplateID, @ToEmailAddressList, @FromName, @FromEmailAddress,
		@ReplyToEmailAddress, @CCEmailAddressList, @BCCEmailAddressList, @MessageSubjectTemplate, @MessageBodyTemplate,
		@SMTPContentType, @SMTPPriority, @PendingEmailStatus, CURRENT_TIMESTAMP, @EmployeeID)
	SET @EmailID = @@IDENTITY
	IF (ISNULL(@EmailID, 0) = 0)
		RETURN

	-- QUEUE MESSAGE
	EXEC dbo.upSendOutboundEmail
		@EmailID = @EmailID,
		@IsSuccess = @IsSuccess OUTPUT

	SET NOCOUNT OFF
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

