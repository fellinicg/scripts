SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: December 14, 2005
-- Description:	Run monthly report files
-- =============================================
ALTER PROCEDURE dbo.upRunMonthlyFileReports 
	@DaysPriorToEndOfMonth INT = 4,
	@CountryCode VARCHAR(5) = 'USA'
AS
BEGIN
	DECLARE @ct INT
	DECLARE @RC INT
	DECLARE @Result INT
	DECLARE @bcpcmd VARCHAR(5000)
	DECLARE @FileReportType CHAR(1)
	DECLARE @DBProcName VARCHAR(40)
	DECLARE @OutputFileName VARCHAR(40)
	DECLARE @ArchiveDate VARCHAR(8)
	DECLARE @ParamStr VARCHAR(500)

	SET NOCOUNT ON

	-- Initialize variables
	SET @ct = 1
	SET @Result = 1
	SET @ArchiveDate = (SELECT CONVERT(VARCHAR, GETDATE(), 112))

	IF dbo.ufGetPriorBusinessDate(DATEPART(mm, GETDATE()), DATEPART(yy, GETDATE()), @CountryCode, @DaysPriorToEndOfMonth) = CONVERT(VARCHAR, GETDATE(), 101)
		BEGIN
			-- Map Z: drive to share
			SET @bcpcmd = 'net use z: /DELETE&net use z: ' + dbo.ufSettingValue('FileReportDir')
			EXECUTE @RC = master..xp_cmdshell @bcpcmd

			-- Error check
			IF @RC <> 0 
				BEGIN
					EXECUTE dbo.upAddFileReportError 'Error occured while mapping share'
					RETURN
				END

			-- Create FTP input file
			SET @bcpcmd = 'echo sys_feeds > z:\tmpinfo' +
					'&echo sdf#$%aa10022 >> z:\tmpinfo' +
					'&echo cd baxter\reportfiles >> z:\tmpinfo' +
					'&echo ascii >> z:\tmpinfo' +
					'&echo mput rr*.pgp >> z:\tmpinfo' +
					'&echo quit >> z:\tmpinfo'
			EXECUTE @RC = master..xp_cmdshell @bcpcmd

			-- Check for error
			IF @RC <> 0 
				BEGIN
					EXECUTE dbo.upAddFileReportError 'Error occured while creating FTP input file'
					GOTO CLEANUP
				END

			-- Create temporary table
			CREATE TABLE #Reports
			(
				ID INT IDENTITY NOT NULL, 
				FileReportType CHAR(1) NOT NULL, 
				Description VARCHAR(40) NOT NULL, 
				DBProcName VARCHAR(40) NOT NULL, 
				OutputFileName VARCHAR(40) NOT NULL
			)

			-- Insert appropriate rows into temporary table
			INSERT INTO #Reports (FileReportType, Description, DBProcName, OutputFileName)
			SELECT FileReportType, Description, DBProcName, OutputFileName
			FROM dbo.tbFileReport
--			WHERE FileReportType IN ('A')
			WHERE FileReportType IN ('E', 'L', 'R', 'A')

			-- Loop over temporary table to process report files
			WHILE (SELECT MAX(ID) FROM #Reports) >= @ct
				BEGIN
					-- Assign necessary info from temporary table to local variables
					SELECT 
						@FileReportType = FileReportType,
						@OutputFileName = OutputFileName,
						@DBProcName = DBProcName
					FROM #Reports
					WHERE ID = @ct

					-- Get report column headers
					SET @bcpcmd = 'bcp "SELECT baxter.dbo.ufTabDelimFileReportHeader(''' + @FileReportType + ''')" queryout "z:\output\monthly_reports\' + @OutputFileName + '" -S ' + @@SERVERNAME + ' -T -c'
					EXECUTE @RC = master..xp_cmdshell @bcpcmd

					-- Check for error
					IF @RC <> 0 
						BEGIN
							SET @ParamStr = 'Error occured while retrieving column headers for report type ' + @FileReportType
							EXECUTE dbo.upAddFileReportError @ParamStr
							GOTO CLEANUP
						END

					-- Get report data
					SET @bcpcmd = 'bcp "EXEC Baxter.dbo.' + @DBProcName + ' ' + CONVERT(VARCHAR, @DaysPriorToEndOfMonth) + '" queryout "z:\working\monthly_reports\monthly_report.txt" -S ' + @@SERVERNAME + ' -T -c'
					EXECUTE @RC = master..xp_cmdshell @bcpcmd

					-- Check for error
					IF @RC <> 0 
						BEGIN
							SET @ParamStr = 'Error occured while executing ' + @DBProcName
							EXECUTE dbo.upAddFileReportError @ParamStr
							GOTO CLEANUP
						END

					-- Append report data to destination file
					SET @bcpcmd = 'type z:\working\monthly_reports\monthly_report.txt >> z:\output\monthly_reports\' + @OutputFileName
					EXECUTE @RC = master..xp_cmdshell @bcpcmd

					-- Check for error
					IF @RC <> 0 
						BEGIN
							SET @ParamStr = 'Error occured while appending data to ' + @OutputFileName
							EXECUTE dbo.upAddFileReportError @ParamStr
							GOTO CLEANUP
						END

					-- Archive file
					SET @bcpcmd = 'copy z:\output\monthly_reports\' + @OutputFileName + ' z:\archive\monthly_reports\' + @ArchiveDate + '-' + @OutputFileName
					EXECUTE @RC = master..xp_cmdshell @bcpcmd

					-- Check for error
					IF @RC <> 0 
						BEGIN
							SET @ParamStr = 'Error occured while archiving ' + @OutputFileName
							EXECUTE dbo.upAddFileReportError @ParamStr
							SET @bcpcmd = 'del z:\archive\monthly_reports\' + @ArchiveDate + '*.* /F /Q'
							EXECUTE master..xp_cmdshell @bcpcmd
							GOTO CLEANUP
						END

					-- Move file to be encrypted
					SET @bcpcmd = 'move z:\output\monthly_reports\' + @OutputFileName + ' \\reports\pgpcmd\input\unencrypted'
					EXECUTE @RC = master..xp_cmdshell @bcpcmd

					-- Check for error
					IF @RC <> 0 
						BEGIN
							SET @ParamStr = 'Error occured while moving file ' + @OutputFileName + ' to REPORTS server'
							EXECUTE dbo.upAddFileReportError @ParamStr
							GOTO CLEANUP
						END

					-- Map Z: drive to share on REPORTS server for PGP
					EXECUTE @RC = Reports.master..xp_cmdshell 'net use z: /DELETE&net use z: \\reports\pgpcmd'

					-- Error check
					IF @RC <> 0 
						BEGIN
							SET @ParamStr = 'Error occured while mapping share on REPORTS server'
							EXECUTE dbo.upAddFileReportError @ParamStr
							GOTO CLEANUP
						END

					-- Encrypt file
					SET @bcpcmd = 'pgp.exe --encrypt z:\input\unencrypted\' + @OutputFileName + ' --output z:\output\encrypted --r ' + dbo.ufSettingValue('FileReportCPK') + ' --overwrite remove --verbose --input-cleanup remove'
					EXECUTE @RC = Reports.master..xp_cmdshell @bcpcmd

					-- Error check
					IF @RC <> 0 
						BEGIN
							SET @ParamStr = 'Error occured while encrypting file ' + @OutputFileName + ' on REPORTS server'
							EXECUTE dbo.upAddFileReportError @ParamStr
							GOTO CLEANUP
						END

					-- Move encrypted file back
					SET @bcpcmd = 'move \\reports\pgpcmd\output\encrypted\' + @OutputFileName + '.pgp z:\output\monthly_reports'
					EXECUTE @RC = master..xp_cmdshell @bcpcmd

					-- Error check
					IF @RC <> 0 
						BEGIN
							SET @ParamStr = 'Error occured while moving encrypted file ' + @OutputFileName + ' from REPORTS server'
							EXECUTE dbo.upAddFileReportError @ParamStr
							GOTO CLEANUP
						END

					-- Increment counter
					SET @ct = @ct + 1
				END

			-- FTP destination file
			EXECUTE @RC = master..xp_cmdshell 'z:&cd output\monthly_reports&ftp -i -s:z:\tmpinfo ftp.madisonpg.com'

			-- Check for error
			IF @RC <> 0 
				BEGIN
					EXECUTE dbo.upAddFileReportError 'Error occured while ftping monthly file reports'
					SET @bcpcmd = 'del z:\archive\monthly_reports\' + @ArchiveDate + '*.* /F /Q'
					EXECUTE master..xp_cmdshell @bcpcmd
					GOTO CLEANUP
				END

			-- If all went well, set result variable to 0
			SET @Result = 0

			-- Loop over temporary table to add log entries
			SET @ct = 1
			WHILE (SELECT MAX(ID) FROM #Reports) >= @ct
				BEGIN
					-- Assign necessary info from temporary table to local variables
					SELECT 
						@FileReportType = FileReportType,
						@OutputFileName = OutputFileName
					FROM #Reports
					WHERE ID = @ct
				
					-- Add entry
					SET @ParamStr = @ArchiveDate + '-' + @OutputFileName
					EXECUTE dbo.upAddFileReportLog @FileReportType, @ParamStr

					-- Increment counter
					SET @ct = @ct + 1
				END

CLEANUP:

			-- Delete FTP input file
			EXECUTE @RC = master..xp_cmdshell 'del z:\tmpinfo /F /Q'

			-- Delete all files in working directory
			EXECUTE master..xp_cmdshell 'del z:\working\monthly_reports\*.* /F /Q'

			-- Delete all files in output directory
			EXECUTE master..xp_cmdshell 'del z:\output\monthly_reports\*.* /F /Q'

			-- Delete mapped drive
			EXECUTE @RC = master..xp_cmdshell 'net use z: /DELETE'

			-- Drop temporary table
			DROP TABLE #Reports

			-- Return result code
			RETURN @Result
		END

	RETURN 0

END

GO