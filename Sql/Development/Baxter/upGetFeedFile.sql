SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: January 17, 2006
-- Description:	Download and unencrypt Baxter feed
-- =============================================
ALTER PROCEDURE dbo.upGetFeedFile
AS
BEGIN

	DECLARE @RC INT
	DECLARE @Result INT
	DECLARE @bcpcmd VARCHAR(5000)
	DECLARE @ArchiveDate VARCHAR(8)

	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 1
	SET @ArchiveDate = (SELECT CONVERT(VARCHAR, GETDATE(), 112))

	-- Map Z: drive to share
	SET @bcpcmd = 'net use z: /DELETE&net use z: ' +	dbo.ufSettingValue('LoadSystemUnprocessedDir')
	EXECUTE @RC = master..xp_cmdshell @bcpcmd

	-- Error check
	IF @RC <> 0 
		BEGIN
			RAISERROR('upGetFeedFile - error occured while mapping share', 16, 1)
			RETURN
		END

	-- Create FTP input file
	SET @bcpcmd = 'echo sys_feeds > z:\tmpinfo' +
			'&echo sdf#$%aa10022 >> z:\tmpinfo' +
			'&echo cd baxter >> z:\tmpinfo' +
			'&echo get madison.dat.pgp >> z:\tmpinfo' +
			'&echo rename madison.dat.pgp madison.dat.pgp.' + @ArchiveDate + ' >> z:\tmpinfo' +
			'&echo quit >> z:\tmpinfo'
	EXECUTE @RC = master..xp_cmdshell @bcpcmd

	-- Check for error
	IF @RC <> 0 
		BEGIN
			RAISERROR('upGetFeedFile - error occured while creating FTP input file', 16, 1)
			GOTO CLEANUP
		END

	-- Retrieve file via ftp
	EXECUTE @RC = master..xp_cmdshell 'z:&ftp -i -s:z:\tmpinfo ftp.madisonpg.com'

	-- Check for error
	IF @RC <> 0 
		BEGIN
			RAISERROR('upGetFeedFile - error occured while retrieving file via FTP', 16, 1)
			GOTO CLEANUP
		END

	-- Check for file to be decrypted.  If File Not Found, exit gracefully as per JM's request
	SET @bcpcmd = 'dir z:\madison.dat.pgp'
	EXECUTE @RC = master..xp_cmdshell @bcpcmd

	-- Check for error
	IF @RC <> 0 
		BEGIN
			GOTO CLEANUP
		END

	-- Move file to be decrypted
	SET @bcpcmd = 'move z:\madison.dat.pgp \\reports\pgpcmd\input\encrypted'
	EXECUTE @RC = master..xp_cmdshell @bcpcmd

	-- Check for error
	IF @RC <> 0 
		BEGIN
			RAISERROR('upGetFeedFile - error occured while moving file to REPORTS server', 16, 1)
			GOTO CLEANUP
		END

	-- Map Z: drive to share on REPORTS server for PGP
	EXECUTE @RC = Reports.master..xp_cmdshell 'net use z: /DELETE&net use z: \\reports\pgpcmd'

	-- Error check
	IF @RC <> 0 
		BEGIN
			RAISERROR('upGetFeedFile - error occured while mapping share on REPORTS server', 16, 1)
			GOTO CLEANUP
		END

	-- Decrypt file
	EXECUTE @RC = Reports.master..xp_cmdshell 'pgp.exe --decrypt z:\input\encrypted\madison.dat.pgp --output z:\output\unencrypted --symmetric-passphrase "jack daniels on the rocks please" --overwrite remove --verbose'

	-- Error check
	IF @RC <> 0 
		BEGIN
			RAISERROR('upGetFeedFile - error occured while decrypting file on REPORTS server', 16, 1)
			GOTO CLEANUP
		END

	-- Move encrypted file back
	SET @bcpcmd = 'move \\reports\pgpcmd\output\unencrypted\madison.dat z:\' + @ArchiveDate + '_baxter.dat'
	EXECUTE @RC = master..xp_cmdshell @bcpcmd

	-- Error check
	IF @RC <> 0 
		BEGIN
			RAISERROR('upGetFeedFile - error occured while moving decrypted file from REPORTS server', 16, 1)
			GOTO CLEANUP
		END

	-- If all went well, set result variable to 0
	SET @Result = 0

CLEANUP:

	-- Delete FTP input file
	EXECUTE @RC = master..xp_cmdshell 'del z:\tmpinfo /F /Q'

	-- Delete feed file remenants
	EXECUTE @RC = master..xp_cmdshell 'del z:\madison.dat /F /Q&del z:\madison.dat.pgp /F /Q'

	-- Delete mapped drive on REPORTS server
	EXECUTE @RC = Reports.master..xp_cmdshell 'net use z: /DELETE'

	-- Delete mapped drive
	EXECUTE @RC = master..xp_cmdshell 'net use z: /DELETE'

	-- Return result code
	RETURN @Result

END
GO
