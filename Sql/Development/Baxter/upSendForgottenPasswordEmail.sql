USE [Baxter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE dbo.upSendForgottenPasswordEmail
	@LoginName varchar(20),
	@SecurityAnswer varchar(40),
	@ProcStatus char(1) OUTPUT  -- SEE CONTANTS DECLARATION FOR RETURN VALUES
AS
DECLARE
	-- VARS
	@EmployeeID int,
	@EmpLangCode char(3),
	@EmployeeFullName varchar(80),
	@EmployeeEmailAddress varchar(120),
	@FromName varchar(80),
	@FromEmailAddress varchar(120),
	@ReplyToEmailAddress varchar(120),
	@CCEmailAddressList varchar(400),
	@BCCEmailAddressList varchar(400),
	@MessageBodyTemplate varchar(6000),
	@MessageSubjectTemplate varchar(120),
	@SMTPContentType varchar(10),
	@SMTPPriority varchar(6),
	@NewAcctPassword char(8),
	@ToEmailAddressList varchar(400),
	@EmailID int,
	@IsSuccess bit,
	-- CONSTANTS
	@ProcStatusSuccess char(1),
	@ProcStatusInvalidLoginName char(1),
	@ProcStatusInvalidAnswer char(1),
	@ProcStatusNoEmail char(1),
	@ProcStatusAcctNotEnabled char(1),
	@ProcStatusOtherError char(1),
	@PendingEmailStatus char(1),
	@ForgottenPasswordEmailType char(1),
	@EmailTemplateID int

	SET NOCOUNT ON

	-- SET CONSTANTS
	SET @ProcStatusSuccess = 'S'						-- SUCCESS
	SET @ProcStatusInvalidLoginName = 'L'		-- LOGIN NAME DOES NOT EXIST
	SET @ProcStatusInvalidAnswer = 'A'			-- SECURITY ANSWER IS INCORRECT
	SET @ProcStatusNoEmail = 'E'						-- USER HAS NO EMAIL ADDRESS
	SET @ProcStatusAcctNotEnabled = 'N'			-- USER ACCOUNT IS NOT ENABLED
	SET @ProcStatusOtherError = 'O'					-- OTHER ERROR
	SET @PendingEmailStatus = 'P'
	SET @ForgottenPasswordEmailType = 'F'

	-- CHECK INPUTS
	SELECT
		@EmployeeID = UserAccountID,
		@EmpLangCode = PreferredLanguageCode
	FROM tbUserAccount
	WHERE LoginName = @LoginName
	IF (ISNULL(@EmployeeID, 0) = 0)
	BEGIN
		SET @ProcStatus = @ProcStatusInvalidLoginName
		RETURN
	END
	IF (NOT EXISTS (SELECT 1 FROM tbUserAccount WHERE UserAccountID = @EmployeeID AND SecurityAnswer = @SecurityAnswer))
	BEGIN
		SET @ProcStatus = @ProcStatusInvalidAnswer
		RETURN
	END
	IF (dbo.ufEmployeeHasEmailAddress(@EmployeeID) = 0)
	BEGIN
		SET @ProcStatus = @ProcStatusNoEmail
		RETURN
	END
	IF (NOT EXISTS (SELECT 1 FROM tbUserAccount WHERE UserAccountID = @EmployeeID AND Status = 'E'))  -- ACCOUNT ENABLED
	BEGIN
		SET @ProcStatus = @ProcStatusAcctNotEnabled
		RETURN
	END

	-- GET EMPLOYEE INFO
	SELECT
		@EmployeeFullName = dbo.ufEmployeeGUFullName(EmployeeID),
		@EmployeeEmailAddress = WorkEmailAddress
	FROM tbEmployee
	WHERE EmployeeID = @EmployeeID
		AND IsDeleted = 0
	IF (@@ROWCOUNT = 0)
	BEGIN
		SET @ProcStatus = @ProcStatusOtherError
		RETURN
	END
	SET @ToEmailAddressList = @EmployeeFullName + ' ' + '<' + @EmployeeEmailAddress + '>'

	-- GET EMAIL TEMPLATE INFO
	SELECT
		@FromName = FromName,
		@FromEmailAddress = FromEmailAddress,
		@ReplyToEmailAddress = ReplyToEmailAddress,
		@CCEmailAddressList = CCEmailAddressList,
		@BCCEmailAddressList = BCCEmailAddressList,
		@MessageSubjectTemplate = MessageSubjectTemplate,
		@MessageBodyTemplate = MessageBodyTemplate,
		@SMTPContentType = SMTPContentType,
		@SMTPPriority = SMTPPriority,
		@EmailTemplateID = EmailTemplateID
	FROM tbEmailTemplate
	WHERE EmailType = @ForgottenPasswordEmailType  -- SUBJECT ID VALS INHERITED FROM EMAIL TYPES
		AND LanguageCode = @EmpLangCode
	IF (@@ROWCOUNT = 0)
	BEGIN
		SET @ProcStatus = @ProcStatusOtherError
		RETURN
	END

	-- RESET ACCOUNT
	SET @NewAcctPassword = LEFT(CONVERT(varchar(40), NEWID()), 8)
	UPDATE tbUserAccount SET
		PasswordCrypted = master.dbo.ud_CCEncrypt(@NewAcctPassword),
		PasswordChangedDateTime = CURRENT_TIMESTAMP
	WHERE UserAccountID = @EmployeeID
	IF (@@ERROR <> 0 OR @@ROWCOUNT <> 1)
	BEGIN
		SET @ProcStatus = @ProcStatusOtherError
		RETURN
	END

	-- BUILD MESSAGE BODY
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<NewPassword>', @NewAcctPassword)

	-- CREATE tbOutboundEmail RECORD
	INSERT tbOutboundEmail(EmailTemplateID, ToEmailAddressList, FromName, FromEmailAddress,
		ReplyToEmailAddress, CCEmailAddressList, BCCEmailAddressList, MessageSubject, MessageBody, SMTPContentType,
		SMTPPriority, Status, StatusDateTime, ToEmployeeID)
	VALUES(@EmailTemplateID, @ToEmailAddressList, @FromName, @FromEmailAddress,
		@ReplyToEmailAddress, @CCEmailAddressList, @BCCEmailAddressList, @MessageSubjectTemplate, @MessageBodyTemplate,
		@SMTPContentType, @SMTPPriority, @PendingEmailStatus, CURRENT_TIMESTAMP, @EmployeeID)
	SET @EmailID = @@IDENTITY
	IF (ISNULL(@EmailID, 0) = 0)
	BEGIN
		SET @ProcStatus = @ProcStatusOtherError
		RETURN
	END

	-- QUEUE MESSAGE
	EXEC dbo.upSendOutboundEmail
		@EmailID = @EmailID,
		@IsSuccess = @IsSuccess OUTPUT

	-- SET PROC STATUS
	IF (@IsSuccess = 1)
		SET @ProcStatus = @ProcStatusSuccess
	ELSE
		SET @ProcStatus = @ProcStatusOtherError

	SET NOCOUNT OFF
