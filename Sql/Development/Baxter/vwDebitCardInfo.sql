USE [Baxter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW dbo.vwEployeeDebitCardInfo
AS

	SELECT *
	FROM OPENQUERY(GOLDFINGER, 'SELECT b.u_MPGProgUserID, CASE a.ac_Status WHEN ''ACCOUNT_OPEN'' THEN ''Y'' ELSE ''N'' END CardActive, a.ac_ExpDate, s.s_AvailBal
	FROM ClarityCards.dbo.Statements s
	INNER JOIN (SELECT MAX(u_MPGProgUserID) u_MPGProgUserID, MAX(s_RecID) s_RecID
			FROM ClarityCards.dbo.Statements
			LEFT JOIN ClarityCards.dbo.Accounts ON ac_internalkey = s_internalkey
			LEFT JOIN ClarityCards.dbo.Users ON u_externalkey = ac_externalkey
			WHERE u_p_id = 13
			GROUP BY s_InternalKey) b ON s.s_RecID = b.s_RecID
	LEFT JOIN ClarityCards.dbo.Users u ON b.u_MPGProgUserID = u.u_MPGProgUserID
	LEFT JOIN ClarityCards.dbo.Accounts a ON u.u_ExternalKey = a.ac_ExternalKey
	WHERE u.u_p_ID = 13')

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF