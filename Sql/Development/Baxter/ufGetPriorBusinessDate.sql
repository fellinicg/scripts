SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: December 13, 2005
-- Description:	Return date of specified number of 
--							days prior to end of month
-- =============================================
ALTER FUNCTION dbo.ufGetPriorBusinessDate 
(
	@MonthNum INT,
	@Year VARCHAR(4),
	@CountryCode VARCHAR(3),
	@PriorDays INT
)
RETURNS VARCHAR(10)
AS
BEGIN

	DECLARE @Date VARCHAR(10)
	DECLARE @tblDayList TABLE (ID INT IDENTITY, DayIDs INT)

	INSERT INTO @tblDayList (DayIDs)
	SELECT d.DayID 
	FROM dbo.tbDay d 
	INNER JOIN dbo.tbBusinessDay bd ON d.DayID = bd.DayID AND bd.CountryCode = @CountryCode 
	WHERE d.MonthNum = @MonthNum AND d.Year = @Year 
	ORDER BY d.DOY DESC

	DELETE FROM @tblDayList WHERE ID > @PriorDays

	SELECT @Date = CONVERT(VARCHAR, MIN(Date), 101) 
	FROM dbo.tbDay 
	INNER JOIN @tblDayList ON DayID = DayIDs 

	RETURN @Date

END
GO

