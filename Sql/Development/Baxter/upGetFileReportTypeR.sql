USE [Baxter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20051216
-- LastEditedByFullName: Todd Fellini 	
-- LastEditDate: 20060829
-- Description:	Get data for Finance Reconciliation file report
-- ============================================================
CREATE PROCEDURE dbo.upGetFileReportTypeR
	@DaysPriorToEndOfMonth INT = 4,
	@CountryCode VARCHAR(5) = 'USA'
AS
BEGIN

	DECLARE @Blank VARCHAR(10)
	DECLARE @PeriodBegin SMALLDATETIME
	DECLARE @PeriodEnd SMALLDATETIME

	SET NOCOUNT ON

	-- Initialize variables
	SET @Blank = 'NULL'
	IF (SELECT DATEPART(mm, CURRENT_TIMESTAMP)) = 1 
		SET @PeriodBegin = (SELECT dbo.ufGetPriorBusinessDate(DATEPART(mm, CURRENT_TIMESTAMP) + 11, DATEPART(yy, CURRENT_TIMESTAMP)-1, @CountryCode, @DaysPriorToEndOfMonth))
	ELSE 
		SET @PeriodBegin = (SELECT dbo.ufGetPriorBusinessDate(DATEPART(mm, CURRENT_TIMESTAMP) - 1, DATEPART(yy, CURRENT_TIMESTAMP), @CountryCode, @DaysPriorToEndOfMonth))	
	SET @PeriodEnd = DATEADD(ss, -1 , CONVERT(DATETIME, CONVERT(VARCHAR, dbo.ufGetPriorBusinessDate(DATEPART(mm, CURRENT_TIMESTAMP), DATEPART(yy, CURRENT_TIMESTAMP), @CountryCode, @DaysPriorToEndOfMonth), 101)))

	SELECT
		UPPER(a.AwardID)
		,gu.LastName
		,gu.FirstName
		,e.WorkEmailAddress
		,e.CustEmployeeNum
		,ISNULL(dc.CardActive, 'N')
		,REPLACE(ISNULL(dc.ac_expDate, @Blank), '/20', '/')
		,REPLACE(CONVERT(VARCHAR, a.RecordDateTime, 10), '-', '/')
		,CONVERT(VARCHAR, a.AwardAmtUSD, 0)
		,CONVERT(VARCHAR, dc.TotalSpent, 0)
		,CONVERT(VARCHAR, dc.s_AvailBal, 0)
		,giver.CustEmployeeNum
		,giver.FullName
		,giver.DeptCode
		,giver.JDECode
		,giver.CompanyCode
		,giver.LegalEntity
		,manager.FullName
		,manager.JDECode
		,CONVERT(VARCHAR, a.AwardAmtUSD, 0)
		,REPLACE(CONVERT(VARCHAR, a.StatusDateTime, 10), '-', '/')
	FROM dbo.tbAward a
		LEFT JOIN dbo.tbEmployee e ON a.RecipEmployeeID = e.EmployeeID
		LEFT JOIN dbo.tbEmployee giver ON a.GiverEmployeeID = giver.EmployeeID
		LEFT JOIN dbo.tbEmployee manager ON a.InitGiverMgrEmployeeID = manager.EmployeeID
		LEFT JOIN dbo.vwGlobalEmployee gu ON e.EmployeeID = gu.EmployeeID
		LEFT JOIN dbo.vwEmployeeDebitCardInfo dc ON e.EmployeeID = dc.u_MPGProgUserID
	WHERE a.StatusDateTime BETWEEN @PeriodBegin AND @PeriodEnd
		AND a.AwardType IN ('D')
		AND a.Status = 'A'
	ORDER BY a.StatusDateTime

END
