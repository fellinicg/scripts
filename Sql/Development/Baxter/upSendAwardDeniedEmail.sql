USE [Baxter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE dbo.upSendAwardDeniedEmail
	@AwardID int,
	@IsSuccess bit OUTPUT
AS
DECLARE
	@RecipEmployeeID int,
	@RecipFullName varchar(80),
	@RecipMgrEmployeeID int,
	@RecipMgrFullName varchar(80),
	@RecipMgrEmailAddress varchar(120),
	@GiverEmployeeID int,
	@GiverFullName varchar(80),
	@GiverEmailAddress varchar(120),
	@GiverMgrEmployeeID int,
	@GiverMgrFullName varchar(80),
	@GiverMgrEmailAddress varchar(120),
	@DeniedByEmployeeID int,
	@DeniedByFullName varchar(120),
	@LanguageCode char(3),
	@ToEmailAddressList varchar(400),
	@FromName varchar(80),
	@FromEmailAddress varchar(120),
	@ReplyToEmailAddress varchar(120),
	@CCEmailAddressList varchar(400),
	@BCCEmailAddressList varchar(400),
	@MessageSubjectTemplate varchar(120),
	@MessageBodyTemplate varchar(7000),
	@SMTPContentType varchar(10),
	@SMTPPriority varchar(6),
	@AwardType varchar(20),
	@AwardBehavior varchar(50),
	@EmailID int,
	@EmailType char(1),
	@PendingEmailStatus char(1),
	@DeniedAwardStatus char(1),
	@EmailTemplateID int

	/* ********************************************************************************************************* */
	/* 2006-01-17 JMC																																														*/
	/*								This proc is intended for use by other procedures only. It has no transacation or failsafe */
	/*								control. It is the responsibility of the calling process to evaluate the outcome of a call */
	/*								to this procedure and rollback the transaction if necessary. This procedure was written		*/
	/*								this way to avoid transaction count mismatches where calling procedures also require			*/
	/*								consistent transaction control.																														*/
	/* ********************************************************************************************************* */

	SET NOCOUNT ON

	-- VALIDATION
		-- GET GIVER/RECIPIENT EMPLOYEEIDS
	SELECT
		@RecipEmployeeID = RecipEmployeeID,
		@RecipMgrEmployeeID = TermRecipMgrEmployeeID,
		@GiverEmployeeID = GiverEmployeeID,
		@GiverMgrEmployeeID = TermGiverMgrEmployeeID,
		@DeniedByEmployeeID = DeniedByEmployeeID
	FROM tbAward
	WHERE AwardID = @AwardID
		-- ENSURE RECIPIENT HAS AN EMAIL ADDRESS IN THE SYSTEM
	IF (dbo.ufEmployeeHasEmailAddress(@GiverEmployeeID) <> 1)
		RETURN
	-- GET RECIPIENT LANGUAGE CODE
	SELECT @LanguageCode = dbo.ufAccountPreferredLanguage(@GiverEmployeeID)
	IF (@LanguageCode = 'XXX')	-- FUNC FAILURE INDICATOR
		RETURN

	--SET VARS
	SET @IsSuccess = 0
	SET @EmailType = 'D' -- AWARD DENIED EMAIL
	SET @DeniedAwardStatus = 'D'
	SET @PendingEmailStatus = 'P'

	-- GET GIVER AND RECIPIENT INFO
	SELECT
		@GiverFullName = dbo.ufEmployeeGUFullName(@GiverEmployeeID),
		@GiverEmailAddress = g.WorkEmailAddress,
		@GiverMgrEmailAddress = gm.WorkEmailAddress,
		@RecipMgrEmailAddress = rm.WorkEmailAddress,
		@RecipFullName = dbo.ufEmployeeGUFullName(@RecipEmployeeID),
		@DeniedByFullName = dbo.ufEmployeeGUFullName(@DeniedByEmployeeID),
		@AwardType = t.Description,
		@AwardBehavior = b.Title
	FROM tbAward a
		INNER JOIN tbEmployee g ON (a.GiverEmployeeID = g.EmployeeID)
		INNER JOIN tbEmployee gm ON (a.TermGiverMgrEmployeeID = gm.EmployeeID)
		INNER JOIN tbEmployee rm ON (a.TermRecipMgrEmployeeID = rm.EmployeeID)
		INNER JOIN tbAwardType t ON (a.AwardType = t.AwardType)
		INNER JOIN tbAwardBehavior b ON a.BehaviorID = b.BehaviorID
	WHERE a.AwardID = @AwardID
		AND a.Status = @DeniedAwardStatus
	IF (@@ROWCOUNT = 0)
		RETURN

	-- FORMAT @CCEmailAddressList
	IF (ISNULL(@RecipMgrEmailAddress, '') <> '')
		SET @CCEmailAddressList = @RecipMgrEmailAddress

	IF (ISNULL(@GiverMgrEmailAddress, '') <> '')
		IF (ISNULL(@CCEmailAddressList, '') <> '')
			SET @CCEmailAddressList = @CCEmailAddressList + ', ' + @GiverMgrEmailAddress
		ELSE
			SET @CCEmailAddressList = @GiverMgrEmailAddress

	-- GET TEMPLATE FOR AWARD TYPE
	SELECT
		@ToEmailAddressList = @GiverFullName	+ ' <' + @GiverEmailAddress + '>',
		@FromName = FromName,
		@FromEmailAddress = FromEmailAddress,
		@ReplyToEmailAddress = ReplyToEmailAddress,
		@BCCEmailAddressList = BCCEmailAddressList,
		@MessageSubjectTemplate = MessageSubjectTemplate,
		@MessageBodyTemplate = MessageBodyTemplate,
		@SMTPContentType = SMTPContentType,
		@SMTPPriority = SMTPPriority,
		@EmailTemplateID = EmailTemplateID
	FROM tbEmailTemplate
	WHERE EmailType = @EmailType
		AND LanguageCode = @LanguageCode
	IF (@@ROWCOUNT = 0)
		RETURN

	-- BUILD MESSAGE BODY
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<RecipientFullName>', @RecipFullName)
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<ReviewingMgrFullName>', @DeniedByFullName)
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<AwardType>', @AwardType)
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<AwardBehavior>', @AwardBehavior)

	-- CREATE tbOutboundEmail RECORD
	INSERT tbOutboundEmail(EmailTemplateID, ToEmailAddressList, FromName, FromEmailAddress,
		ReplyToEmailAddress, CCEmailAddressList, BCCEmailAddressList, MessageSubject, MessageBody, SMTPContentType,
		SMTPPriority, RecordDateTime, Status, StatusDateTime, FromEmployeeID, ToEmployeeID, AwardID)
	VALUES(@EmailTemplateID, @ToEmailAddressList, @FromName, @FromEmailAddress,
		@ReplyToEmailAddress, @CCEmailAddressList, @BCCEmailAddressList, @MessageSubjectTemplate, @MessageBodyTemplate, @SMTPContentType,
		@SMTPPriority, CURRENT_TIMESTAMP, @PendingEmailStatus, CURRENT_TIMESTAMP, @GiverEmployeeID, @RecipEmployeeID, @AwardID)
	SET @EmailID = @@IDENTITY
	IF (ISNULL(@EmailID, 0) = 0)
		RETURN

	-- QUEUE MESSAGE
	EXEC dbo.upSendOutboundEmail
		@EmailID = @EmailID,
		@IsSuccess = @IsSuccess OUTPUT

	SET NOCOUNT OFF
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

