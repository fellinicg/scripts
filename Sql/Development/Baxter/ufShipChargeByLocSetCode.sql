USE [Baxter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060719
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER FUNCTION dbo.ufShipChargeByLocSetCode (@LocationSetCode varchar(5))
RETURNS smallmoney
AS
BEGIN
  
	DECLARE @ShippingCharge smallmoney

  SELECT @ShippingCharge = CASE UPPER(@LocationSetCode)
					WHEN 'USA01' THEN .39
					WHEN 'PRI01' THEN .84
				 END

	RETURN(@ShippingCharge)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.ufShipChargeByLocSetCode
TO system