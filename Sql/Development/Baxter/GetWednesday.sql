DECLARE @Year VARCHAR(4)			--Param
DECLARE @Date SMALLDATETIME		--Param
DECLARE @PeriodBegin DATETIME
DECLARE @PeriodEnd DATETIME

SET @Year = '2005'
SET @Date = '12/16/2005'

SELECT @PeriodBegin = Date - 7, @PeriodEnd = DATEADD(ss, -1, Date)
FROM dbo.tbDay
WHERE Year = @Year
AND DATEPART(WEEK, Date) = DATEPART(WEEK, @Date)
AND DOW = 4

SELECT @PeriodBegin, @PeriodEnd
