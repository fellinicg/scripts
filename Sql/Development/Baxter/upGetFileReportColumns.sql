SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: December 14, 2005
-- Description:	Get column headers for file report
-- =============================================
CREATE PROCEDURE dbo.upGetFileReportColumns 
	@FileReportType CHAR(1) 
AS
BEGIN
	SET NOCOUNT ON

	SELECT DisplayName
	FROM dbo.tbFileReportColumn
	WHERE FileReportType = @FileReportType
	ORDER BY PostionNum

END
GO
