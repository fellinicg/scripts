USE [Baxter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.upSendAwardApprovedEmail
	@AwardID int,
	@IsSuccess bit OUTPUT
AS
DECLARE
	@RecipientFullName varchar(80),
	@RecipientEmailAddress varchar(120),
	@GiverFullName varchar(80),
	@GiverEmailAddress varchar(120),
	@CommentsToRecipient varchar(300),
	@ToEmailAddressList varchar(400),
	@FromName varchar(80),
	@FromEmailAddress varchar(120),
	@ReplyToEmailAddress varchar(120),
	@CCEmailAddressList varchar(400),
	@BCCEmailAddressList varchar(40),
	@MessageSubjectTemplate varchar(120),
	@MessageBodyTemplate varchar(7000),
	@SMTPContentType varchar(10),
	@SMTPPriority varchar(6),
	@AwardType varchar(20),
	@AwardTypeCode char(1),
	@AwardBehavior varchar(50),
	@RecipEmployeeID int,
	@GiverEmployeeID int,
	@LanguageCode char(3),
	@EmailID int,
	@EmailType char(1),
	@PendingEmailStatus char(1),
	@ApprovedAwardStatus char(1),
	@EmailTemplateID int

	/* ********************************************************************************************************* */
	/* 2006-01-17 JMC																																														*/
	/*								This proc is intended for use by other procedures only. It has no transacation or failsafe */
	/*								control. It is the responsibility of the calling process to evaluate the outcome of a call */
	/*								to this procedure and rollback the transaction if necessary. This procedure was written		*/
	/*								this way to avoid transaction count mismatches where calling procedures also require			 */
	/*								consistent transaction control.																														*/
	/* ********************************************************************************************************* */

	SET NOCOUNT ON

	-- VALIDATION
		-- VAIDATE AWARD/USERS/EMAILS
	SELECT @RecipEmployeeID = RecipEmployeeID, @GiverEmployeeID = GiverEmployeeID FROM tbAward WHERE AwardID = @AwardID
	IF (dbo.ufEmployeeHasEmailAddress(@RecipEmployeeID) <> 1)
		RETURN
	-- GET RECIPIENT LANGUAGE CODE
	SELECT @LanguageCode = dbo.ufAccountPreferredLanguage(@RecipEmployeeID)
	IF (@LanguageCode = 'XXX')	-- FUNC FAILURE INDICATOR
		RETURN

	--SET VARS
	SET @IsSuccess = 0
	SET @EmailType = 'A'	 -- AWARD APPROVAL
	SET @ApprovedAwardStatus = 'A' -- APPROVED STATUS
	SET @PendingEmailStatus = 'P' -- PENDING EMAIL STATUS

	-- GET GIVER AND RECIPIENT INFO
	SELECT
		@RecipientFullName = dbo.ufEmployeeGUFullName(@RecipEmployeeID),
		@RecipientEmailAddress = r.WorkEmailAddress,
		@GiverFullName = dbo.ufEmployeeGUFullName(@GiverEmployeeID),
		@GiverEmailAddress = g.WorkEmailAddress,
		@CCEmailAddressList = g.WorkEmailAddress,
		@CommentsToRecipient = a.GiverComments,
		@AwardType = t.Description,
		@AwardTypeCode = t.AwardType,
		@AwardBehavior = b.Title
	FROM tbAward a
		INNER JOIN tbEmployee g ON a.GiverEmployeeID = g.EmployeeID
		INNER JOIN tbEmployee r ON a.RecipEmployeeID = r.EmployeeID
		INNER JOIN tbAwardType t ON a.AwardType = t.AwardType
		INNER JOIN tbAwardBehavior b ON a.BehaviorID = b.BehaviorID
	WHERE a.AwardID = @AwardID
		AND a.Status = @ApprovedAwardStatus
	IF (@@ROWCOUNT = 0)
	BEGIN
		RETURN
	END

	-- GET TEMPLATE FOR AWARD TYPE
	SELECT
		@ToEmailAddressList = @RecipientFullName	 + ' <' + @RecipientEmailAddress + '>',
		@FromName = FromName,
		@FromEmailAddress = FromEmailAddress,
		@ReplyToEmailAddress = ReplyToEmailAddress,
		@CCEmailAddressList = @GiverEmailAddress,
		@BCCEmailAddressList = BCCEmailAddressList,
		@MessageSubjectTemplate = MessageSubjectTemplate,
		@MessageBodyTemplate = MessageBodyTemplate,
		@SMTPContentType = SMTPContentType,
		@SMTPPriority = SMTPPriority,
		@EmailTemplateID = EmailTemplateID
	FROM tbEmailTemplate
	WHERE EmailType = @EmailType
		AND LanguageCode = @LanguageCode
		AND EmailSubType = @AwardTypeCode
	IF (@@ROWCOUNT = 0)
			RETURN

	-- BUILD MESSAGE BODY
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<GiverFullName>', @GiverFullName)
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<AwardType>', @AwardType)
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<AwardBehavior>', @AwardBehavior)
	SET @MessageBodyTemplate = REPLACE(@MessageBodyTemplate, '<CommentsToRecipient>', @CommentsToRecipient)

	-- CREATE tbOutboundEmail RECORD
	INSERT tbOutboundEmail(EmailTemplateID, ToEmailAddressList, FromName, FromEmailAddress,
		ReplyToEmailAddress, CCEmailAddressList, BCCEmailAddressList, MessageSubject, MessageBody, SMTPContentType,
		SMTPPriority, RecordDateTime, Status, StatusDateTime, FromEmployeeID, ToEmployeeID, AwardID)
	VALUES(@EmailTemplateID, @ToEmailAddressList, @FromName, @FromEmailAddress,
		@ReplyToEmailAddress, @CCEmailAddressList, @BCCEmailAddressList, @MessageSubjectTemplate, @MessageBodyTemplate, @SMTPContentType,
		@SMTPPriority, CURRENT_TIMESTAMP, @PendingEmailStatus, CURRENT_TIMESTAMP, @GiverEmployeeID, @RecipEmployeeID, @AwardID)
	SET @EmailID = @@IDENTITY
	IF (ISNULL(@EmailID, 0) = 0)
		RETURN

	-- QUEUE MESSAGE
	EXEC dbo.upSendOutboundEmail
		@EmailID = @EmailID,
		@IsSuccess = @IsSuccess OUTPUT

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF