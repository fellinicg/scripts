USE [Pfizer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC dbo.upReinstateEmployee
(
	@CustEmployeeCode VARCHAR(20),
	@IsSuccess BIT OUTPUT
) AS

-- ============================================================
-- OrigAuthorFullName: Joe Genovese
-- OrigCreateDate: 20060713
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20060828
-- ============================================================

	SET NOCOUNT ON

	DECLARE @EmployeeStatusActive CHAR(1)
	DECLARE @UserAccountStatusEnabled CHAR(1)
	DECLARE @AccountGroupIDAll INT
	DECLARE @UserReturn INT
	DECLARE @UserAccountID INT
	DECLARE @Result CHAR(1)

	SET @EmployeeStatusActive = 'A'
	SET @UserAccountStatusEnabled = 'E'
	SET @AccountGroupIDAll = 1

	SET @IsSuccess = 0

	SET @UserAccountID = dbo.ufEmployeeID (@CustEmployeeCode)
	IF @UserAccountID = 0 BEGIN
			RAISERROR('upReinstateEmployee - EMPLOYEE REFERENCED BY @CustEmployeeCode DOES NOT EXIST', 16, 1)
			RETURN
	END

	BEGIN TRAN

	UPDATE tbemployee
		SET
			Status = @EmployeeStatusActive,
			IsDeleted = 0,
			StatusDateTime = CURRENT_TIMESTAMP,
			UpdateDateTime = CURRENT_TIMESTAMP
		WHERE EmployeeID = @UserAccountID 
	IF @@ROWCOUNT = 0 BEGIN
			ROLLBACK TRAN
			RAISERROR('upReinstateEmployee - UNABLE TO UPDATE EMPLOYEE STATUS', 16, 1)
			RETURN
	END

	UPDATE tbUserAccount
		SET 
			Status = @UserAccountStatusEnabled,
			StatusDateTime = CURRENT_TIMESTAMP,
			UpdateDateTime = CURRENT_TIMESTAMP
		WHERE UserAccountID = @UserAccountID
	IF @@ROWCOUNT = 0 BEGIN
			ROLLBACK TRAN
			RAISERROR('upReinstateEmployee - UNABLE TO UPDATE USER ACCOUNT STATUS', 16, 1)
			RETURN
	END

	IF NOT EXISTS (
			SELECT TOP 1 1 
			FROM tbAccountGroupMember 
			WHERE (AccountGroupID = @AccountGroupIDAll AND UserAccountID = @UserAccountID)
		) BEGIN
		INSERT INTO tbAccountGroupMember
			(AccountGroupID, UserAccountID)
			VALUES (@AccountGroupIDAll, @UserAccountID)
		IF @@ROWCOUNT <> 1 BEGIN
				ROLLBACK TRAN
				RAISERROR('upReinstateEmployee - UNABLE TO DELETE REVOKE GROUP MEMBERSHIP', 16, 1)
				RETURN
		END
	END

	COMMIT

	EXEC SQL2KPROD.GlobalUsers.dbo.upUpdateUserAccount
		@Result = @Result,
		@UserAccountID = @UserAccountID,
		@UpdAccountStatus = 1,
		@AccountStatus = 'E'

	IF @Result = 'F'
		BEGIN
			RAISERROR('upReinstateEmployee - UNABLE TO UPDATE GLOBAL USER STATUS', 16, 1)
			RETURN
		END

	SET @IsSuccess = 1

	SET NOCOUNT OFF

	RETURN

