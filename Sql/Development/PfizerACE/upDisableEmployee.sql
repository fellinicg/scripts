USE [Pfizer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC dbo.upDisableEmployee
(
	@CustEmployeeCode VARCHAR(20),
	@Status char(1),
	@IsSuccess BIT OUTPUT
) AS

-- ============================================================
-- OrigAuthorFullName: Joe Genovese
-- OrigCreateDate: 20060718
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20060828
-- ============================================================

	SET NOCOUNT ON

	SET @IsSuccess = 0

	DECLARE @UserAccountID INT
	DECLARE @EmployeeStatusTerminated CHAR(1)
	DECLARE @EmployeeStatusOnLeave CHAR(1)
	DECLARE @EmployeeStatusRetired CHAR(1)

	DECLARE @UserAccountStatusDisabled CHAR(1)
	DECLARE @Result CHAR(1)

	SET @EmployeeStatusTerminated= 'X'
	SET @EmployeeStatusOnLeave = 'L'
	SET @EmployeeStatusRetired = 'R'

	SET @UserAccountStatusDisabled = 'D'

	IF (@Status NOT IN (@EmployeeStatusTerminated, @EmployeeStatusOnLeave, @EmployeeStatusRetired)) BEGIN
			RAISERROR('upDisableEmployee- INVALID VALUE FOR STATUS', 16, 1)
			RETURN
	END

	SET @UserAccountID = dbo.ufEmployeeID (@CustEmployeeCode)
	IF @UserAccountID = 0 BEGIN
			RAISERROR('upDisableEmployee- EMPLOYEE REFERENCED BY @CustEmployeeCode DOES NOT EXIST', 16, 1)
			RETURN
	END

	BEGIN TRAN

	UPDATE tbemployee
		SET
			Status = @Status,
			IsDeleted = 1,
			StatusDateTime = CURRENT_TIMESTAMP,
			UpdateDateTime = CURRENT_TIMESTAMP
		WHERE EmployeeID = @UserAccountID
	IF @@ROWCOUNT = 0 BEGIN
			ROLLBACK TRAN
			RAISERROR('upDisableEmployee- UNABLE TO UPDATE EMPLOYEE STATUS', 16, 1)
			RETURN
	END

	UPDATE tbUserAccount
		SET
			Status = @UserAccountStatusDisabled,
			StatusDateTime = CURRENT_TIMESTAMP,
			UpdateDateTime = CURRENT_TIMESTAMP
		WHERE UserAccountID = @UserAccountID
	IF @@ROWCOUNT = 0 BEGIN
			ROLLBACK TRAN
			RAISERROR('upDisableEmployee- UNABLE TO UPDATE USER ACCOUNT STATUS', 16, 1)
			RETURN
	END

	DELETE FROM tbAccountGroupMember
	WHERE UserAccountID = @UserAccountID
	IF @@ERROR <> 0 BEGIN
			ROLLBACK TRAN
			RAISERROR('upDisableEmployee- UNABLE TO DELETE REVOKE GROUP MEMBERSHIP', 16, 1)
			RETURN
	END

	COMMIT

	EXEC SQL2KPROD.GlobalUsers.dbo.upUpdateUserAccount
		@Result = @Result,
		@UserAccountID = @UserAccountID,
		@UpdAccountStatus = 1,
		@AccountStatus = 'D'

	IF @Result = 'F'
		BEGIN
			RAISERROR('upDisableEmployee- UNABLE TO UPDATE GLOBAL USER STATUS', 16, 1)
			RETURN
		END

	SET @IsSuccess = 1

	SET NOCOUNT OFF

	RETURN

