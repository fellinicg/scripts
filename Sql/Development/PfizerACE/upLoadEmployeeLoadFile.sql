USE [Pfizer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Joe Genovese
-- OrigCreateDate: 20060414
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20060828
-- ============================================================
CREATE PROC dbo.upLoadEmployeeLoadFile
	@LoadFileID int,
	@SessionID uniqueidentifier,
	@LoadFileStatus char(1) OUTPUT,
	@StatusComment varchar(800)	OUTPUT,
	@Verbose bit = 0
AS

	SET NOCOUNT ON

	-- VARS
	DECLARE	@IsFailure bit
	DECLARE @RecordID int
	DECLARE @UserReturn int
	DECLARE @Result char(1)

	DECLARE @FileRecCNT int
	DECLARE @CreateCNT int
	DECLARE @DeleteCNT int
	DECLARE @UpdateCNT int
	DECLARE @UpdateNameCNT int
	DECLARE @IgnoreAdminCNT int

	DECLARE @Updates TABLE (EmployeeID int NOT NULL, RecordID int NOT NULL PRIMARY KEY)
	DECLARE @UpdateNames TABLE (EmployeeID int NOT NULL, RecordID int NOT NULL PRIMARY KEY, OldLastName varchar(30) NULL, OldFirstName varchar(30) NULL, OldCustEmployeeCode char(20) NULL)
	DECLARE @Deletes TABLE (EmployeeID int NOT NULL PRIMARY KEY, gu_rs_id int)
	DECLARE @Creates TABLE (CustEmployeeCode char(20) NOT NULL, RecordID int PRIMARY KEY, UserAccountID int, WasCreated bit)
	DECLARE @WasCreated BIT

	DECLARE @UserAccountID int
	DECLARE @LastName varchar(30)
	DECLARE @FirstName varchar(30)
	DECLARE @CustEmployeeCode varchar(20)
	DECLARE @ClientID int

	-- CONSTANTS
	DECLARE	@AcctGrpUser int
	DECLARE	@AcctGrpAdmin int
	DECLARE @AcctStatDisabled char(1)
	DECLARE @AcctStatEnabled  char(1)
	DECLARE @AcctTypeClient char(1)
	DECLARE @EmpStatActive char(1)
	DECLARE @EmpStatSuspended char(1)
	DECLARE @LFStatError char(1)
	DECLARE @LFStatLoaded char(1)
	DECLARE @AccountType char(1)
	DECLARE @IsPerson bit
	DECLARE @AccountStatus char(1)

	SET @AcctGrpUser = 1
	SET @AcctGrpAdmin = 3
	SET @AcctStatEnabled = 'E'
	SET @AcctStatDisabled = 'D'
	SET @AcctTypeClient = 'C'
	SET @EmpStatActive = 'A'
	SET @EmpStatSuspended = 'S'
	SET @LFStatError = 'E'
	SET @LFStatLoaded = 'L'
	SET @AccountType = 'C'
	SET @IsPerson = 1
	SET @AccountStatus = 'E'

	-- PROGRESS MARKERS
	DECLARE @CreateGlobalUsers bit
	DECLARE @UpdateGlobalUsersName bit
	DECLARE @DisableGlobalUsers bit

	EXEC dbo.upGetSettingValue @SettingLabel = 'PfizerACEClientID', @SettingValue = @ClientID OUTPUT
	IF (ISNULL(@ClientID, 0) = 0)
	BEGIN
		RAISERROR('upLoadEmployeeLoadFile - failed to obtain value FROM tbSettings for PfizerACEClientID', 16, 1)
		RETURN
	END

	-- DEFAULTS
	SET @IsFailure = 0
	SET @LoadFileStatus = @LFStatError


	-->> POPULATE CONTROL TABLES <<--

	SELECT @FileRecCNT = COUNT(*) FROM tbLFStagingEmployee

		-- CREATES
	INSERT @Creates(CustEmployeeCode, RecordID)
		SELECT EMPLID, RecordID
		FROM tbLFStagingEmployee
		WHERE EmployeeID IS NULL						-- WITH NO MATCH IN EMPLOYEE TABLE
		AND LEN(ISNULL(RejectReason,'')) = 0
	SET @CreateCNT = @@ROWCOUNT

		-- UPDATES
	INSERT @Updates(EmployeeID, RecordID)
	SELECT l.EmployeeID, RecordID
	FROM tbLFStagingEmployee l
		INNER JOIN tbEmployee e ON (l.EmployeeID = e.EmployeeID)
	WHERE l.EmployeeID IS NOT NULL											-- MATCHES EMPLOYEE IN tbEmployee
		AND (l.NewValsCheckSum <> l.ExistingValsCheckSum)		-- SOME VALUES HAVE CHANGED (EXCLUSIVE OF FIRST/LAST NAME)
		AND (LEN(ISNULL(RejectReason,'')) = 0)
	SET @UpdateCNT = @@ROWCOUNT

		-- NAME UPDATES
	INSERT @UpdateNames(EmployeeID, RecordID)
	SELECT l.EmployeeID, RecordID
	FROM tbLFStagingEmployee l
		INNER JOIN tbEmployee e ON (l.EmployeeID = e.EmployeeID)
	WHERE l.EmployeeID IS NOT NULL											-- MATCHES EMPLOYEE IN tbEmployee
		AND l.NewValsCheckSumName <> l.ExistingValsCheckSumName		--  FIRST AND/OR LAST NAME HAVE CHANGED
		AND LEN(ISNULL(RejectReason,'')) = 0
	SET @UpdateNameCNT = @@ROWCOUNT

		-- JOE GENOVESE 2006-07-12 -- NO RE-INSTATEMENT HERE -- MUST BE DONE MANUALLY

		-- DELETES
	INSERT @Deletes(EmployeeID, gu_rs_id)
	SELECT e.EmployeeID, e.gu_rs_id
	FROM vwGlobalEmployee e
		LEFT OUTER JOIN tbLFStagingEmployee l ON (e.EmployeeID = l.EmployeeID)
	WHERE e.IsDeleted = 0							-- CURRENTLY ACTIVE EMPLOYEE
		AND l.EmployeeID IS NULL				-- NOT AN ELIGIBLE EMPLOYEE IN LOAD FILE
		AND LEN(ISNULL(RejectReason,'')) = 0
	SET @DeleteCNT = @@ROWCOUNT

		-- ADMIN IGNORES
	DELETE @Deletes
	FROM @Deletes d
		INNER JOIN tbAccountGroupMember agm ON (d.EmployeeID = agm.UserAccountID AND agm.AccountGroupID = @AcctGrpAdmin)
	SET @IgnoreAdminCNT = @@ROWCOUNT
	SET @DeleteCNT = @DeleteCNT - @IgnoreAdminCNT

	-- BUILD STATUS COMMENT
	SET @StatusComment =
		'FileRecords - ' + CONVERT(varchar, @FileRecCNT) + '; ' +
		'CreatedEmployees - ' + CONVERT(varchar, @CreateCNT) + '; ' +
		'UpdatedEmployees - ' + CONVERT(varchar, @UpdateCNT) + '; ' +
		'DeletedEmployees - ' + CONVERT(varchar, @DeleteCNT) + '; ' +
		'IgnoredAdminEmployees - ' + CONVERT(varchar, @IgnoreAdminCNT)

	-- VERBOSE OUTPUT
	IF (@Verbose = 1)
	BEGIN
		PRINT @StatusComment
		-- DUMP VARS
		SELECT
			@CreateCNT AS CreateEmpCNT,
			@UpdateCNT AS UPDATEEmpCNT,
			@UpdateNameCNT AS UPDATEEmpNameCNT,
			@DeleteCNT AS DeleteEmpCNT,
			@IgnoreAdminCNT AS SparedAdminsCNT
		-- DUMP CONTROL TABLES
		IF (@CreateCNT <> 0)
		BEGIN
			PRINT 'CreateEmployeeList'
			SELECT * FROM @Creates
		END
		IF (@UpdateCNT <> 0)
		BEGIN
			PRINT 'UpdateEmployeesList'
			SELECT * FROM @Updates
		END
		IF (@UpdateNameCNT <> 0)
		BEGIN
			PRINT 'UpdateEmployeeNamesList (Name)'
			SELECT * FROM @UpdateNames
		END
		IF (@DeleteCNT <> 0)
		BEGIN
			PRINT 'DeleteEmployeeList'
			SELECT * FROM @Deletes
		END
	END

	-- EXIT IF NO WORK TO DO
	IF (@CreateCNT + @UpdateCNT + @UpdateNameCNT + @DeleteCNT = 0)
	BEGIN
		SET @LoadFileStatus = @LFStatLoaded
		RETURN
	END

	-->> PERFORM GLOBAL USER OPERATIONS FIRST AS THEY CANNOT BE INCLUDED IN TRANSACTION BLOCKING <<--

	-- CREATE NEW GLOBAL USERS (FOR NEW EMPS)
	IF (@CreateCNT <> 0)
	BEGIN

		IF (@Verbose = 1)
			PRINT 'CREATES: CREATE GLOBAL USERS'

		SET @CreateGlobalUsers = 1

		SELECT @RecordID = MIN(e.RecordID)
		FROM tbLFStagingEmployee e
			INNER JOIN @Creates c ON (e.RecordID = c.RecordID)

		IF (@Verbose = 1)
			SELECT @RecordID as InitialStagingRecord

		WHILE ((@RecordID is not NULL) AND (@IsFailure = 0))
		BEGIN

			-- GET INFO
			SELECT @LastName = Last_Name, @FirstName = First_Name, @CustEmployeeCode = EMPLID
			FROM tbLFStagingEmployee WHERE RecordID = @RecordID

			-- CREATE GLOBAL USER
			SET @UserAccountID = NULL
			EXECUTE SQL2KPROD.GlobalUsers.dbo.upAddUserAccount
				 @AccountType = @AccountType
				,@IsPerson = @IsPerson
				,@ClientID = @ClientID
				,@CustAccountCode = @CustEmployeeCode
				,@PersonFirstGivenName = @FirstName
				,@PersonFamilyName = @LastName
				,@AccountStatus = @AccountStatus
				,@UserAccountID = @UserAccountID OUTPUT
			-- ERROR HANDLER
			IF (ISNULL(@UserAccountID, 0) = 0)
			BEGIN
				IF (@Verbose = 1)
					PRINT 'CALL FAILURE: SQL2KPROD.GlobalUsers.dbo.upAddUserAccount'
				SET @IsFailure = 1
				SET @StatusComment = 'upLoadEmployeeLoadFile - failed to create a record in GlobalUser'
				BREAK
			END
			ELSE
				UPDATE @Creates
					SET UserAccountID = @UserAccountID, WasCreated = 1
					WHERE RecordID = @RecordID

			-- GET NEXT CANDIDATE
			SELECT @RecordID = MIN(e.RecordID)
			FROM tbLFStagingEmployee e
				INNER JOIN @Creates c ON (e.RecordID = c.RecordID)
				WHERE e.RecordID > @RecordID

			IF (@Verbose = 1)
				SELECT @RecordID as NextStagingRecord

		END -- WHILE

	END -- @CreateCNT <> 0

	-- UPDATE EXISTING GLOBAL USERS (FOR EXISTING EMPS)

	IF (@IsFailure = 0 AND @UpdateNameCNT <> 0)
	BEGIN
		SET @UpdateGlobalUsersName = 1
		IF (@Verbose = 1)
		BEGIN
			PRINT 'UPDATE: GLOBAL USERS'
		END
		-- SET OLD VALUES FOR CLEANUP
		UPDATE @UpdateNames SET
			OldLastName = ge.LastName,
			OldFirstName = ge.FirstName,
			OldCustEmployeeCode = ge.CustEmployeeCode
		FROM @UpdateNames u
			INNER JOIN vwGlobalEmployee ge ON (u.EmployeeID = ge.EmployeeID)
		IF (@@ROWCOUNT <> @UpdateNameCNT)
		BEGIN
			IF (@Verbose = 1)
				PRINT 'FAILURE: setting old values for UPDATE global users'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadEmployeeLoadFile - failed to get old values FROM GlobalUser'
		END
		SELECT @RecordID = MIN(RecordID) FROM @UpdateNames
		IF (@Verbose = 1)
			SELECT @RecordID as InitialStagingRecordID
		WHILE (ISNULL(@RecordID, 0) <> 0)
		BEGIN
			-- GET NEW VALUES
			SELECT @LastName = Last_Name, @FirstName = First_Name, @CustEmployeeCode = EMPLID, @UserAccountID = EmployeeID
			FROM tbLFStagingEmployee WHERE RecordID = @RecordID
			-- UPDATE GLOBAL USER
			EXECUTE SQL2KPROD.GlobalUsers.dbo.upUpdateUserAccount
				 @Result = @Result OUTPUT
				,@UserAccountID = @UserAccountID
				,@UpdCustAccountCode = 1
				,@CustAccountCode = @CustEmployeeCode
				,@UpdPersonFirstGivenName = 1
				,@PersonFirstGivenName = @FirstName
				,@UpdPersonFamilyName = 1
				,@PersonFamilyName = @LastName
			-- ERROR HANDLER
			IF (@Result = 'F')
			BEGIN
				IF (@Verbose = 1)
					PRINT 'CALL FAILURE: SQL2KPROD.GlobalUsers.dbo.upUpdateUserAccount'
				SET @IsFailure = 1
				SET @StatusComment = 'upLoadEmployeeLoadFile - failed to UPDATE a record in GlobalUser'
				BREAK
			END
			-- GET NEXT CANDIDATE
			SELECT @RecordID = MIN(RecordID) FROM @UpdateNames WHERE RecordID > @RecordID
			IF (@Verbose = 1)
				SELECT @RecordID AS NextRecordID
		END
	END

	-- DISABLE GLOBAL USERS (FOR DELETED EMPS)
	IF (@IsFailure = 0 AND @DeleteCNT <> 0)
	BEGIN
		SET @DisableGlobalUsers = 1
		IF (@Verbose = 1)
			PRINT 'DISABLE: GLOBAL USERS'
		SELECT @RecordID = MIN(EmployeeID) FROM @Deletes WHERE gu_rs_id = 0
		IF (@Verbose = 1)
			SELECT @RecordID as InitialStagingEmployeeID
		WHILE (ISNULL(@RecordID, 0) <> 0)
		BEGIN
			SET @UserAccountID = @RecordID
			-- UPDATE GLOBAL USER
			EXEC SQL2KPROD.GlobalUsers.dbo.upUpdateUserAccount
				@Result = @Result,
				@UserAccountID = @UserAccountID,
				@UpdAccountStatus = 1,
				@AccountStatus = 'D'
			-- ERROR HANDLER
			IF @Result = 'F'
			BEGIN
				IF (@Verbose = 1)
					PRINT 'CALL FAILURE: SQL2KPROD.GlobalUsers.dbo.upUpdateUserAccount'
				SET @IsFailure = 1
				SET @StatusComment = 'upLoadEmployeeLoadFile - failed to disable a record in GlobalUser'
				BREAK
			END
			-- GET NEXT CANDIDATE
			SELECT @RecordID = MIN(EmployeeID) FROM @Deletes WHERE gu_rs_id = 0 AND EmployeeID > @RecordID
			IF (@Verbose = 1)
				SELECT @RecordID AS NextEmployeeID
		END
	END

	-- OPEN TRAN
	IF (@IsFailure = 0)
	BEGIN
		IF (@Verbose = 1)
			PRINT 'STARTING TRAN'
		BEGIN TRAN
	END

	IF (@Verbose = 1)
		PRINT 'UPDATE LOOKUP TABLE: EMPLOYEE STATUS'

	CREATE TABLE #t (EMPL_STATUS char(1), EMPL_STATUS_DESC varchar(60))

	IF (@IsFailure = 0)
	BEGIN
		INSERT INTO #t
		SELECT EMPL_STATUS, MIN(EMPL_STATUS_DESC)
		FROM tbLFStagingEmployee e
			INNER JOIN tbCustEmployeeStatus s ON (e.EMPL_STATUS = s.CustEmployeeStatusCode)
		WHERE LEN(ISNULL(RejectReason,'')) = 0
		AND e.EMPL_STATUS_DESC <> s.CustEmployeeStatusDesc
		GROUP BY EMPL_STATUS
		-- ERROR HANDLER
		IF (@@error <> 0)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: UPDATE tbCustEmployeeStatus'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadEmployeeLoadFile - failed to UPDATE tbCustEmployeeStatus'
		END
	END

	IF (@IsFailure = 0)
	BEGIN
		UPDATE tbCustEmployeeStatus
		SET CustEmployeeStatusDesc = t.EMPL_STATUS_DESC, UpdateDateTime = CURRENT_TIMESTAMP
		FROM tbCustEmployeeStatus s
			INNER JOIN #t t ON (s.CustEmployeeStatusCode = t.EMPL_STATUS)
		-- ERROR HANDLER
		IF (@@error <> 0)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: UPDATE tbCustEmployeeStatus'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadEmployeeLoadFile - failed to UPDATE tbCustEmployeeStatus'
		END
	END

	DROP TABLE #t

	IF (@IsFailure = 0)
	BEGIN
		INSERT INTO tbCustEmployeeStatus
			(CustEmployeeStatusCode, CustEmployeeStatusDesc)
		SELECT EMPL_STATUS, MIN(EMPL_STATUS_DESC)
		FROM tbLFStagingEmployee e
			left JOIN tbCustEmployeeStatus s ON (e.EMPL_STATUS = s.CustEmployeeStatusCode)
		WHERE LEN(ISNULL(RejectReason,'')) = 0
		AND s.CustEmployeeStatusCode is NULL
		GROUP BY EMPL_STATUS
		-- ERROR HANDLER
		IF (@@error <> 0)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: INSERT INTO tbCustEmployeeStatus'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadEmployeeLoadFile - failed to INSERT INTO tbCustEmployeeStatus'
		END
	END

	IF (@Verbose = 1)
		PRINT 'UPDATE LOOKUP TABLE: JOB'

	CREATE TABLE #t2 (JOB_CODE char(6), JOB_TITLE varchar(10), JOB_DESC varchar(40))

	IF (@IsFailure = 0)
	BEGIN
		INSERT INTO #t2
		SELECT JOB_CODE, MIN(JOB_TITLE), MIN(JOB_DESC)
		FROM tbLFStagingEmployee e
			INNER JOIN tbJob j ON (e.JOB_CODE = j.jobcode)
		WHERE LEN(ISNULL(RejectReason,'')) = 0
		AND (e.JOB_DESC <> j.jobdesc or e.JOB_TITLE <> j.jobtitle)
		GROUP BY e.JOB_CODE
		-- ERROR HANDLER
		IF (@@error <> 0)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: UPDATE tbJob'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadEmployeeLoadFile - failed to UPDATE tbJob'
		END
	END

	IF (@IsFailure = 0)
	BEGIN
		UPDATE tbJob
		SET jobtitle = t.JOB_TITLE, jobdesc = t.JOB_DESC, UpdateDateTime = CURRENT_TIMESTAMP
		FROM tbJob j
			INNER JOIN #t2 t ON (j.jobcode = t.JOB_CODE)
		-- ERROR HANDLER
		IF (@@error <> 0)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: UPDATE tbJob'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadEmployeeLoadFile - failed to UPDATE tbJob'
		END
	END

	DROP TABLE #t2

	IF (@IsFailure = 0)
	BEGIN
		INSERT INTO tbJob
			(JobCode, JobTitle , JobDesc)
		SELECT JOB_CODE, MIN(JOB_TITLE) , MIN(JOB_DESC)
		FROM tbLFStagingEmployee e
			left JOIN tbJob j ON (e.JOB_CODE = j.JobCode)
		WHERE LEN(ISNULL(RejectReason,'')) = 0
		AND j.JobCode is NULL
		GROUP BY JOB_CODE
		-- ERROR HANDLER
		IF (@@error <> 0)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: INSERT INTO tbJob'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadEmployeeLoadFile - failed to INSERT INTO tbJob'
		END
	END

	IF (@IsFailure = 0)
	BEGIN
		IF (@Verbose = 1)
			PRINT 'COMMITTING TRAN...'
		COMMIT
		IF (@Verbose = 1)
			PRINT 'STARTING TRAN'
		begin tran
	END

	-- UPDATE EMPLOYEES
	IF (@IsFailure = 0 AND @UpdateCNT <> 0)
	BEGIN

		IF (@Verbose = 1)
			PRINT 'UPDATES: EMPLOYEES'

		UPDATE tbemployee
				SET
					PreferredName           = l.PREFERRED_NAME
					,HomeAddress1           = l.HOME_ADDRESS1
					,HomeAddress2     = l.HOME_ADDRESS2
					,HomeCity               = l.HOME_CITY
					,HomeState              = l.HOME_STATE
					,HomeZip                = l.HOME_ZIP
					,Email                  = l.EMAIL
					,JobCode                = l.JOB_CODE
					,CustEmployeeStatusCode = l.EMPL_STATUS
					,Thumb512Filename       = l.THUMB512_FILENAME
					,ThumbWebFilename       = l.THUMBWEB_FILENAME
					,HireDate               = l.HIRE_DATE
					,UpdateDateTime         = CURRENT_TIMESTAMP
		FROM tbemployee
			INNER JOIN tblfstagingemployee l ON (tbemployee.employeeid = l.employeeid)
			INNER JOIN @Updates u ON (u.recordid = l.recordid)
		-- ERROR HANDLER
		IF (@@ROWCOUNT <> @UpdateCNT)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: UPDATES COUNT MISMATCH'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadEmployeeLoadFile - failed to UPDATE a record in tbEmployee'
		END

	END

	-- DELETE EMPLOYEES/ACCTS
	IF (@IsFailure = 0 AND @DeleteCNT <> 0)
	BEGIN

		-- DISABLE USER ACCTS
		IF (@Verbose = 1)
			PRINT 'DELETES: DISABLE ACCT'
		UPDATE tbUserAccount
			SET	Status = @AcctStatDisabled,
					StatusDateTime = CURRENT_TIMESTAMP,
					UpdateDateTime = CURRENT_TIMESTAMP
			FROM tbUserAccount a
				INNER JOIN @Deletes d ON (a.UserAccountID = d.EmployeeID)
		-- ERROR HANDLER
		IF (@@ROWCOUNT <> @DeleteCNT)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: DISABLE ACCT COUNT MISMATCH'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadEmployeeLoadFile - failed to disable a record in tbUserAccount'
		END

		-- DELETE EMPLOYEES
		IF (@IsFailure = 0)
		BEGIN
			IF (@Verbose = 1)
				PRINT 'DELETES: DELETE EMPLOYEE'
			UPDATE tbEmployee
				SET
					IsDeleted = 1,
					Status = @EmpStatSuspended,
					StatusDateTime = CURRENT_TIMESTAMP,
					UpdateDateTime = CURRENT_TIMESTAMP
				FROM tbEmployee e
					INNER JOIN @Deletes d ON (e.EmployeeID = d.EmployeeID)
			-- ERROR HANDLER
			IF (@@ROWCOUNT <> @DeleteCNT)
			BEGIN
				ROLLBACK TRAN
				IF (@Verbose = 1)
					PRINT 'FAILURE: DELETE EMP COUNT MISMATCH'
				SET @IsFailure = 1
				SET @StatusComment = 'upLoadEmployeeLoadFile - failed to delete a record in tbEmployee'
			END
		END

		-- REVOKE ACCT GROUP ACCESS
		IF (@Verbose = 1)
			PRINT 'DELETES: REVOKE ACCT GROUP ACCESS'
		DELETE tbAccountGroupMember
		FROM tbAccountGroupMember m
			INNER JOIN @Deletes e ON (m.UserAccountID = e.EmployeeID)
		-- ERROR HANDLER
		IF (@@ROWCOUNT < @DeleteCNT)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: REVOKE ACCT GROUP ACCESS'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadEmployeeLoadFile - failed to delete memberships for a deleted employee'
		END
	END

	-- CREATE EMPLOYEES
	IF (@IsFailure = 0 AND @CreateCNT <> 0)
	BEGIN

		IF (@Verbose = 1)
			PRINT 'CREATES: CREATE ACCTS'

		INSERT tbUserAccount(UserAccountID, LoginName, Status, StatusDateTime, AccountType)
		SELECT UserAccountID, dbo.ufNewLoginName(CustEmployeeCode), @AcctStatEnabled, CURRENT_TIMESTAMP, @AcctTypeClient
		FROM @Creates

		-- ERROR HANDLER
		IF (@@rowcount <> @CreateCNT)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: ERROR ON USER ACCT CREATION'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadEmployeeLoadFile - failed to create a record in tbUserAccount'
		END

	END

	IF (@IsFailure = 0)
	BEGIN
		IF (@Verbose = 1)
			PRINT 'CREATES: CREATE EMPLOYEES'
		INSERT INTO tbemployee
			(
			EmployeeID
			,CustEmployeeCode
			,PreferredName
			,HomeAddress1
			,HomeAddress2
			,HomeCity
			,HomeState
			,HomeZip
			,Email
			,JobCode
			,CustEmployeeStatusCode
			,Thumb512Filename
			,ThumbWebFilename
			,HireDate
			,Status
			)
		SELECT
			c.UserAccountID
			,e.EMPLID
			,PREFERRED_NAME
			,HOME_ADDRESS1
			,HOME_ADDRESS2
			,HOME_CITY
			,HOME_STATE
			,HOME_ZIP
			,EMAIL
			,JOB_CODE
			,EMPL_STATUS
			,THUMB512_FILENAME
			,THUMBWEB_FILENAME
			,e.HIRE_DATE
			,@EmpStatActive
		FROM tbLFStagingEmployee e
			INNER JOIN @Creates c ON (e.RecordID = c.RecordID)
		-- ERROR HANDLER
		IF (@@rowcount <> @CreateCNT)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: ERROR ON EMPLOYEE CREATION'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadEmployeeLoadFile - failed to create a record in tbEmployee'
		END
	END

	-- GRANT ALL GROUP ACCESS
	IF (@IsFailure = 0)
	BEGIN
		IF (@Verbose = 1)
			PRINT 'CREATES: GRANT ALL GROUP ACCESS'
		INSERT tbAccountGroupMember (AccountGroupID, UserAccountID)
			SELECT @AcctGrpUser, UserAccountID
			FROM @Creates
		-- ERROR HANDLER
		IF (@@rowcount <> @CreateCNT)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: NEW USER GRANT ALL GROUP ACCESS'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadEmployeeLoadFile - failed to grant all group access for a new employee'
		END
	END

	IF (@IsFailure = 0)
	BEGIN
		IF (@Verbose = 1)
			PRINT 'UPDATE POSITION EMPLOYEE'

		-- CREATE NEW POSITION ASSIGNMENT WHERE ASSIGNEE HAS CHANGED
		UPDATE tbposition
			SET
				employeeid = e.employeeid,
				proxyemployeeid = NULL,  -- "Feeds Trumps All - A user may be assigned as a proxy until they are manually removed as the proxy by a stored procedure, or IF a new user is attached to that position(TERRITORY_ID) in the subsequent feeds"
				UpdateDateTime = CURRENT_TIMESTAMP
			FROM tbposition p
				INNER JOIN tbLFStagingEmployee se ON (se.TERRITORY_ID = p.custPositionCode)
				INNER JOIN tbemployee e ON (se.emplid = e.custemployeecode)
			WHERE ISNULL(p.employeeid,0) <> ISNULL(e.employeeid, 0)
			AND p.isdeleted = 0
		IF (@@error <> 0)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: UPDATE EMPLOYEE POSITION'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadEmployeeLoadFile - failed to UPDATE one or more records (employeeid) in tbPosition'
		END

		-- DESTROY POSITION ASSIGNMENT WHERE ASSIGNEE IS NOT LONGER ASSIGNED (AND IS NOT AN ADMIN)
		UPDATE tbposition
			SET
				employeeid = NULL,
				proxyemployeeid = NULL,  -- "Feeds Trumps All - A user may be assigned as a proxy until they are manually removed as the proxy by a stored procedure, or IF a new user is attached to that position(TERRITORY_ID) in the subsequent feeds"
				UpdateDateTime = CURRENT_TIMESTAMP
			FROM tbposition p
				LEFT JOIN tbLFStagingEmployee se ON (se.TERRITORY_ID = p.custPositionCode)
				LEFT JOIN tbAccountGroupMember agm ON ((p.EmployeeID = agm.UserAccountID OR p.ProxyEmployeeID = agm.UserAccountID) AND AccountGroupID = @AcctGrpAdmin)
			WHERE (se.TERRITORY_ID is NULL)  																		-- POSITIONS THAT ARE NOT REFERENCED BY ANY EMPLOYEES
			AND NOT ((p.employeeid is NULL) AND (p.proxyemployeeid is NULL))  	-- POSITIONS THAT ARE NOT VACANT
			AND p.isdeleted = 0  																								-- POSITIONS THAT ARE NOT ALREADY DELETED
			AND agm.UserAccountID IS NULL																				-- POSITIONS THAT ARE NOT NATIVELY HELD OR PROXIED BY ADMINS
		IF (@@error <> 0)
		BEGIN
			ROLLBACK TRAN
			IF (@Verbose = 1)
				PRINT 'FAILURE: UPDATE EMPLOYEE POSITION (UNASSIGN)'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadEmployeeLoadFile - failed to UPDATE one or more records (employeeid) in tbPosition (unassign)'
		END

	END

	-- SET SUCCESS STATUS & CLEANUP
	IF (@IsFailure = 0)
	BEGIN
		IF (@Verbose = 1)
			PRINT 'COMMITTING TRAN...'
		COMMIT
		IF (@Verbose = 1)
			PRINT 'SETTING SUCCESS STATUS'
		SET @LoadFileStatus = @LFStatLoaded
		IF (@Verbose = 1)
			PRINT 'CLEANUP: DEL Employee STAGING'
			DELETE tbLFStagingEmployee
	END

	-- FAILURE HANDLER: OPS PERFORMED IN REMOTE DB MUST BE EXPLICITLY UNDONE AS THEY COULD NOT PARTICIPATE IN LOCAL TRAN
	IF (@IsFailure = 1)
	BEGIN

		-- UNDO CREATES
		IF (@CreateGlobalUsers = 1)
		BEGIN

			SELECT @RecordID = MIN(RecordID)
			FROM @Creates

			IF (@Verbose = 1)
				PRINT 'CREATE GLOBAL USERS: CLEANUP'

			IF (@Verbose = 1)
				SELECT @RecordID as InitialStagingRecord

			WHILE (@RecordID is not NULL)
			BEGIN

				-- GET INFO
				SELECT @UserAccountID = UserAccountID
				FROM @Creates
				WHERE RecordID = @RecordID
				AND WasCreated = 1

				-- DELETE GLOBAL USER
				EXEC SQL2KPROD.GlobalUsers.dbo.upDeleteUserAccount
					@Result = @Result,
					@UserAccountID = @UserAccountID

				-- GET NEXT CANDIDATE
				SELECT @RecordID = MIN(RecordID)
				FROM @Creates
				WHERE RecordID > @RecordID
				AND WasCreated = 1

				IF (@Verbose = 1)
					SELECT @RecordID as NextStagingRecord

			END -- WHILE

		END

		-- UNDO UPDATES
		IF (@UpdateGlobalUsersName = 1)
		BEGIN

			SELECT @RecordID = MIN(RecordID)
			FROM @UpdateNames

			IF (@Verbose = 1)
				PRINT 'UPDATE GLOBAL USERS: CLEANUP'
			-- SET NAMES BACK TO OLD VALUES

			IF (@Verbose = 1)
				SELECT @RecordID as InitialStagingRecord

			WHILE (@RecordID is not NULL)
			BEGIN

				-- GET INFO
				SELECT @LastName = OldLastName, @FirstName = OldFirstName, @CustEmployeeCode = OldCustEmployeeCode, @UserAccountID = EmployeeID
				FROM @UpdateNames
				WHERE RecordID = @RecordID

				-- UPDATE GLOBAL USER
				EXECUTE SQL2KPROD.GlobalUsers.dbo.upUpdateUserAccount
					 @Result = @Result OUTPUT
					,@UserAccountID = @UserAccountID
					,@UpdCustAccountCode = 1
					,@CustAccountCode = @CustEmployeeCode
					,@UpdPersonFirstGivenName = 1
					,@PersonFirstGivenName = @FirstName
					,@UpdPersonFamilyName = 1
					,@PersonFamilyName = @LastName

				-- GET NEXT CANDIDATE
				SELECT @RecordID = MIN(RecordID)
				FROM @UpdateNames
				WHERE RecordID > @RecordID

				IF (@Verbose = 1)
					SELECT @RecordID as NextStagingRecord

			END -- WHILE

		END -- @CreateGlobalUsers = 1

		-- UNDO DISABLES
		IF (@DisableGlobalUsers = 1)
		BEGIN

			SELECT @RecordID = MIN(EmployeeID)
			FROM @Deletes
			WHERE gu_rs_id = 0

			IF (@Verbose = 1)
				PRINT 'DISABLE GLOBAL USERS: CLEANUP'

			-- SET GU_RS_ID BACK TO OLD VALUE

			IF (@Verbose = 1)
				SELECT @RecordID as InitialStagingEmployeeID

			WHILE (@RecordID is not NULL)
			BEGIN

				SET @UserAccountID = @RecordID
				-- UPDATE GLOBAL USER
				EXEC SQL2KPROD.GlobalUsers.dbo.upUpdateUserAccount
					@Result = @Result,
					@UserAccountID = @UserAccountID,
					@UpdAccountStatus = 1,
					@AccountStatus = 'E'

				-- GET NEXT CANDIDATE
				SELECT @RecordID = MIN(EmployeeID)
				FROM @Deletes
				WHERE gu_rs_id = 0 AND EmployeeID > @RecordID

				IF (@Verbose = 1)
					SELECT @RecordID as NextStagingEmployeeID

			END -- WHILE

		END -- @DisableGlobalUsers = 1

	END

	SET NOCOUNT OFF

