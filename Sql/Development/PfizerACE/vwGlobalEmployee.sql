USE [Pfizer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW dbo.vwGlobalEmployee AS
SELECT
		e.EmployeeID,
		e.CustEmployeeCode,
		g.PersonFirstGivenName AS FirstName,
		e.PreferredName,
		g.PersonSecondGivenName as MiddleName,
		g.PersonFamilyName AS LastName,
		COALESCE(e.PreferredName, g.PersonFirstGivenName) + ' ' + g.PersonFamilyName AS BuiltFullName,  -- PFIZER DESIRES PREFERRED NAME WHERE EXISTS
		g.PersonFamilyName + ', ' + COALESCE(e.PreferredName, g.PersonFirstGivenName) AS BuiltLastNameFirst,
		e.HomeAddress1,
		e.HomeAddress2,
		e.HomeCity,
		e.HomeState,
		e.HomeZip,
		e.Email,
		e.JobCode,
		e.CustEmployeeStatusCode,
		e.Thumb512Filename,
		e.ThumbWebFilename,
		e.HireDate,
		e.ReceiveEmailOnCatalogAddition,
		e.ReceiveTextMsgOnAward,
		e.TextMsgPhoneNum,
		e.WishListVisibleToMgmt,
		e.PrefRewardCatVisibletoOthers,
		e.Status,
		e.StatusDateTime,
		e.IsDeleted,
		e.CreateDateTime,
		e.UpdateDateTime,
		CASE g.AccountStatus WHEN 'E' THEN 0 WHEN 'D' THEN 1 WHEN 'X' THEN 2 ELSE 1 END gu_rs_ID
FROM tbEmployee e
		INNER JOIN SQL2KPROD.GlobalUsers.dbo.tbUserAccount g ON (e.EmployeeID = g.UserAccountID)
WHERE g.ClientID = 3
