USE [Pfizer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE dbo.DisableGlobalUser
(
@gu_ID	int,
@UserReturn	int = 0 OUTPUT
)

AS

	DECLARE @Result char(1)

	EXEC SQL2KPROD.GlobalUsers.dbo.upUpdateUserAccount
		@Result = @Result,
		@UserAccountID = @gu_ID,
		@UpdAccountStatus = 1,
		@AccountStatus = 'D'

	IF @Result = 'F'
		BEGIN
			SET @UserReturn = 0
			RETURN
		END

	SET @UserReturn = 1

