SET NOCOUNT ON

-- DECLARATIONS
DECLARE 
	@ClientID int,
	@AccountType char(1)

-- INITIALIZE VARS
SET @ClientID = 7
SET @AccountType = 'C'

-- ENABLED ACCOUNTS
ALTER TABLE GlobalUsers.dbo.tbUserAccount DISABLE TRIGGER tiUserAccount

INSERT INTO GlobalUsers.dbo.tbUserAccount
(
	AccountType,
	IsPerson,
	ClientID,
	AltAccountCode,
	PersonFirstGivenName,
	PersonFamilyName,
	PersonBuiltFullName,
	AccountStatus,
	StatusDateTime,
	CreateDateTime,
	UpdateDateTime
)
SELECT 
	@AccountType AS AccountType,
	1 AS IsPerson,
	@ClientID AS ClientID,
	u_ID AS AltAccountCode,
	CASE LEN(RTRIM(LTRIM(u_FirstName))) WHEN 0 THEN 'NONE' ELSE RTRIM(LTRIM(u_FirstName)) END AS PersonFirstGivenName,
	CASE LEN(RTRIM(LTRIM(u_LastName))) WHEN 0 THEN 'NONE' ELSE RTRIM(LTRIM(u_LastName)) END AS PersonFamilyName,
	CASE LEN(RTRIM(LTRIM(u_FirstName))) WHEN 0 THEN NULL ELSE RTRIM(LTRIM(u_FirstName)) + ' ' + RTRIM(LTRIM(u_LastName)) END AS PersonBuiltFullName,
	CASE u_Disabled WHEN 0 THEN 'E' ELSE 'D' END AS AccountStatus,
	u_Updated AS StatusDateTime,
	u_Updated AS UpdateDateTime,
	u_Added AS CreateDateTime
FROM dbo.Users
LEFT JOIN GlobalUsers.dbo.tbUserAccount ON u_ID = AltAccountCode AND ClientID = @ClientID
WHERE UserAccountID IS NULL
AND u_Disabled = 0

PRINT CONVERT(varchar, @@ROWCOUNT) + ' enabled users added to tbUserAccount...'

ALTER TABLE GlobalUsers.dbo.tbUserAccount ENABLE TRIGGER tiUserAccount

