USE [Rewards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.RewardGetToBeApprovedAwards 

AS

SELECT p_Id, 
	dtp_Desc,
	dbo.ufGetPersonsFullName(p_UserID) AS [Awardee],
	dbo.ufGetPersonsFullName(p_OriginCode)  AS [Awarder],
	dbo.RewardGetRemBudgetForDepAndPeriod (p_PointsPeriod, p_Origin) AS RemBudget,
	p_Points
FROM dbo.Points
INNER JOIN dbo.Departments ON p_Origin = dpt_ID
WHERE p_Status = 1

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.RewardGetToBeApprovedAwards
TO portaluser