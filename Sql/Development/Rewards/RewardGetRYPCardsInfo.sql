USE [Rewards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.RewardGetRYPCardsInfo 

AS

SELECT 
	u_ExternalKey, 
	ua.UserAccountID AS gu_ID, 
	ua.PersonFirstGivenName AS gu_FirstName, 
	ua.PersonFamilyName AS gu_LastName, 
	ea.EmailAddress AS em_Address 
FROM SQL2KPROD.GlobalUsers.dbo.tbUserAccount ua
INNER JOIN SQL2KPROD.MadisonPortal.dbo.Users ON ua.UserAccountID = u_ID AND u_RecState = 0
INNER JOIN SQL2KPROD.GlobalUsers.dbo.tbEmailAddress ea ON u_ID = ea.UserAccountID AND ea.EMailAddressType = 'W'
LEFT JOIN SQL2KPROD.ClarityCards.dbo.Users ON convert(varchar,u_ID) = convert(varchar,u_MPGProgUserID)
WHERE ua.AccountStatus = 'E' 
AND ua.ClientID = 5 
ORDER BY u_ExternalKey

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.RewardGetRYPCardsInfo
TO portaluser