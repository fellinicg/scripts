USE [Rewards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.RewardGetRYPCardUserInfo
(
@gu_ID int
)
AS

SELECT 
	ua.UserAccountID AS gu_ID, 
	ua.PersonFirstGivenName AS gu_FirstName, 
	ua.PersonFamilyName AS gu_LastName, 
	ea.EmailAddress AS em_Address,
	'488 Madison Avenue' AS Add1,
	'3rd Floor' AS Add2,
	'New York' AS City,
	'NY' AS State,
	'10022' AS Zip,
	'USA' AS Country,
	'physical' AS CardType
FROM SQL2KPROD.GlobalUsers.dbo.tbUserAccount ua
INNER JOIN SQL2KPROD.GlobalUsers.dbo.tbEmailAddress ea ON ua.UserAccountID = ea.UserAccountID
WHERE ua.UserAccountID = @gu_ID 

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.RewardGetRYPCardUserInfo
TO portaluser