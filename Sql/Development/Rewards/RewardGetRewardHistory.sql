USE [Rewards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.RewardGetRewardHistory 
(
@p_PointsPeriod int,
@p_UserID int,
@u_ID int
)

AS

--declare variables for the individual groups
DECLARE @APPROVER int
DECLARE @DH int
DECLARE @USER int
SET @APPROVER = 16
SET @DH = 17
SET @USER = 18

--in which group is the user?
--search top to bottom
DECLARE @GROUP int
--approver
SET @GROUP = (SELECT ug_gr_ID FROM MadisonPortal.dbo.UserGroups WHERE ug_u_ID = @u_ID AND ug_gr_ID = @APPROVER)
IF @GROUP IS NULL
	--not an approver
	SET @GROUP = (SELECT ug_gr_ID FROM MadisonPortal.dbo.UserGroups WHERE ug_u_ID = @u_ID AND ug_gr_ID = @DH)
IF @GROUP IS NULL
	--not a dh
	SET @GROUP = (SELECT ug_gr_ID FROM MadisonPortal.dbo.UserGroups WHERE ug_u_ID = @u_ID AND ug_gr_ID = @USER)
IF @GROUP IS NULL
--does not have access at all
RETURN

--create a where addition if filter on user
DECLARE @WHERE varchar(1000)
DECLARE @PERIOD varchar(100)
DECLARE @WHERESTART varchar(10)

IF @p_PointsPeriod = 0
	SET @PERIOD = ' p_PointsPeriod > 0 '
ELSE
	SET @PERIOD =' p_PointsPeriod = ' + CONVERT(varchar,@p_PointsPeriod) + ' '

IF @p_UserID = 0
	SET @WHERE = ''
ELSE
	BEGIN
	IF @PERIOD = ''
		SET @WHERE = ' p_UserID = ' + convert(varchar,@p_UserID)
	ELSE
		SET @WHERE = ' AND p_UserID = ' + convert(varchar,@p_UserID)
	END 
--If requester is in group dh, add another where clause
IF @GROUP = @DH
	BEGIN
	IF @PERIOD = '' AND @WHERE = ''
		SET @WHERE = ' UserAccountID IN (SELECT u_ID FROM MadisonPortal.dbo.tfRewardGetUsersInRYPDep(' + convert(varchar,@u_ID) + ',0)) '
	ELSE
		SET @WHERE = @WHERE + ' AND UserAccountID IN (SELECT u_ID FROM MadisonPortal.dbo.tfRewardGetUsersInRYPDep(' + convert(varchar,@u_ID) + ',0)) '
	END

IF @PERIOD = '' AND @WHERE = ''
	SET @WHERESTART = ''
ELSE
	SET @WHERESTART = ' WHERE '

--get the data
EXECUTE ('SELECT 
	UserAccountID,
	p_Id, 
	dbo.ufGetPersonsFullName(UserAccountID) AS [Name],
	p_Points,
	p_DateAdded,
	CASE p_Status WHEN -1 THEN ''Denied''
			WHEN 1 THEN ''Pending''
			WHEN 0 THEN ''Approved''
			ELSE ''Unknown'' END AS Status
	FROM dbo.Points 
	INNER JOIN GlobalUsers.dbo.tbUserAccount ON p_UserID = UserAccountID '
	+ @WHERESTART + @PERIOD + @WHERE + '
	ORDER BY p_DateAdded DESC')
	RETURN
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.RewardGetRewardHistory
TO portaluser