USE [Rewards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.RewardGetActiveDepUserCount 
(
@dpt_ID int
)

AS

	SELECT count(*) 
	FROM dbo.DepartmentUsers
	INNER JOIN dbo.Departments ON (depu_dep_ID = dpt_ID)
	INNER JOIN GlobalUsers.dbo.tbUserAccount ON (depu_u_ID = UserAccountID)
	INNER JOIN MadisonPortal.dbo.Users ON (UserAccountID = u_ID)
	WHERE dtp_DH_ID <> depu_u_ID
	AND AccountStatus = 'E'
	AND u_RecState = 0
	AND dpt_ID = @dpt_ID

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.RewardGetActiveDepUserCount
TO portaluser
