USE [Rewards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.RewardGetBudgetDetailsByDepartmentAndPeriod
(
@b_bdr_id int,
@b_Dep_ID int
)

AS

	SELECT
		b_Budget As Budget, 
		dbo.ufGetPersonsFullName(b_ModifiedBy) As Modifier, 
		b_Comment As Comment, 
		b_Stamp As [Date Changed]
	FROM dbo.Budget
	WHERE b_bdr_id = @b_bdr_id AND  b_Dep_ID = @b_Dep_ID
	ORDER BY b_Stamp

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.RewardGetBudgetDetailsByDepartmentAndPeriod
TO portaluser
