USE [Rewards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.RewardGetUserEmailAddress
(
@depu_u_ID int,
@em_gu_ID int
)

AS

DECLARE @DH int

--get the sender first, will be the first table to return
SELECT TOP 1 EmailAddress AS em_Address 
FROM GlobalUsers.dbo.tbEmailAddress
WHERE UserAccountID = @em_gu_ID
AND EMailAddressType = 'W'
ORDER BY IsPreferred DESC

--now get the recipaint
SET @DH = 
(
SELECT dtp_DH_ID
FROM dbo.Departments
INNER JOIN dbo.DepartmentUsers ON dpt_ID = depu_dep_ID
WHERE depu_u_ID = @depu_u_ID
)

--set the bcc
DECLARE @BCC varchar(100)
SET @BCC = (SELECT pv_Value FROM MadisonPortal.dbo.PortalVariables WHERE pv_Desc = 'PYPEmailBccAddress') 

-- will be the second table to return
SELECT TOP 1 EmailAddress AS em_Address, 'Reward A Peer Suggestion' AS Subject, @BCC
FROM GlobalUsers.dbo.tbEmailAddress
WHERE UserAccountID = @DH
AND EMailAddressType = 'W'
ORDER BY IsPreferred DESC

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.RewardGetUserEmailAddress
TO portaluser