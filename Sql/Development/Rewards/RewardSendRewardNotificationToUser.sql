USE [Rewards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.RewardSendRewardNotificationToUser
(
@u_ID int,
@p_ID int
)

AS

--Find out if awardee has an e-mail for work
DECLARE @em_Address varchar(200)

SELECT TOP 1 @em_Address = EmailAddress 
FROM GlobalUsers.dbo.tbEmailAddress 
WHERE EMailAddressType = 'W' 
AND UserAccountID = @u_ID 
ORDER BY IsPreferred DESC

IF (@em_Address IS NULL OR @em_Address = 'Unknown')
	BEGIN
		SELECT 'XXX','','',''
		RETURN
	END

--construct the e-mail body
DECLARE @p_Commentary varchar(5000)
DECLARE @p_Comment varchar(1000)
DECLARE @TABLEHTML varchar(8000)
DECLARE @p_OriginCode varchar(250)
DECLARE @p_Points varchar(10)

SET @p_Commentary = (SELECT p_Commentary FROM dbo.Points WHERE p_ID = @p_ID)

SET @p_Comment = (SELECT p_Comment FROM dbo.Points WHERE p_ID = @p_ID)

IF @p_Comment IS NULL OR @p_Comment = ''
	SET @p_Comment = 'None'

SELECT @p_OriginCode = dbo.ufGetPersonsFullName(p_OriginCode)
FROM dbo.Points
WHERE p_Id = @p_Id

SET @p_Points = CONVERT(varchar,(SELECT p_Points FROM dbo.Points WHERE p_Id = @p_Id))

SET @TABLEHTML = '<TABLE width="700" border="1" cellspacing="0" cellpadding="0"><TR><TD>' --wrapper table start
 + '<TABLE border="0" cellspacing="0" cellpadding="0">'
 + '<TR valign="top">'
 + '<TD colspan="2" height="150">' + '<img src="http://www.madisonpg.com/images/email_header.jpg" width="700" height="150">' + '</TD>' --top image location
 + '</TR>'
 + '<TR>'
 + '<TD width="205" height="200" valign="top">' + '<img src="http://www.madisonpg.com/images/email_left_top.jpg" width="205" height="200">' + '</TD>' --left image location
 + '<TD width="495" height="100%" rowspan="2" valign="top">'
 + '<TABLE width="495" height="100%" border="0" cellspacing="0" cellpadding="10">'
 + '<TR>'
 + '<TD align="center"><font size="3" face="Arial"><b>' + 'CONGRATULATIONS' + '</b></font></TD>' --header line
 + '</TR>'
 + '<TR>'
 + '<TD><font size="2" face="Arial">' + 'You have just received an Award of $ '  + @p_Points + ' from the Reward Your Performance program.' + '</font></TD>' --award info line
 + '</TR>'
 + '<TR>'
 + '<TD><font size="2" face="Arial"><b>' + 'Award Criteria:' + '</b></font></TD>' --award criteria header line
 + '</TR>'
 + '<TR>'
 + '<TD><font size="2" face="Arial">' + @p_Commentary +  '</font></TD>' --AWARD CRITERIA
 + '</TR>'
 + '<TR>'
 + '<TD><font size="2" face="Arial"><b>' + 'Award Comments:' + '</b></font></TD>' --award comment header line
 + '</TR>'
 + '<TR>'
 + '<TD><font size="2" face="Arial">' + @p_Comment + '</font></TD>' --AWARD COMMENTS
 + '</TR>'
 + '<TR>'
 + '<TD><font size="2" face="Arial">' + @p_OriginCode + '</font></TD>' --signature
 + '</TR>'
 + '<TR>'
 + '<TD valign="bottom" height="100%"><font size="2" face="Arial">'  --standard text on bottom
 + 'Reward Your Performance is a program that recognizes everyday hard work and going that extra mile. '
 + 'Department Heads will be looking for demonstrated initiative, productivity, adaptability, and teamwork. '
 + 'Individuals may be rewarded for generating innovative solutions for business challenges. '
 + 'They might be recognized for working effectively and cooperatively with others in order to achieve goals. '
 + 'Maybe they will be given an award for adjusting effectively to major changes in work tasks or the work environment. '
 + 'Or they could be singled out for taking action to achieve goals beyond what is required.'
 + '<br><br>'
 + 'There are many different ways to be recognized for the exceptional work you do on a daily basis. '
 + 'And, Reward Your Performance is virtually instantaneous and your opportunities are endless! '
 + 'Nothing is Impossible at MADISON!'
 + '</font></TD>' 
 + '</TR>'
 + '</TABLE>'
 + '</TD>'
 + '</TR>'
 + '<TR>'
 + '<TD width="205" height="572" background="http://www.madisonpg.com/images/email_left_repeat.jpg">&nbsp;</TD>'
 + '</TR>'
 + '</TABLE>'
 + '</TD></TR></TABLE>' --wrapper table end

--get the top 1 e-mail address of the sender
DECLARE @em_AddressSENDER varchar(200)
SELECT TOP 1 @em_AddressSENDER  = EmailAddress 
FROM GlobalUsers.dbo.tbEmailAddress 
INNER JOIN dbo.Points ON UserAccountID = p_OriginCode
WHERE p_Id = @p_Id 
ORDER BY IsPreferred DESC

IF (@em_AddressSENDER IS NULL OR @em_AddressSENDER = 'Unknown')
	SET @em_AddressSENDER = ''

--set the bcc
DECLARE @BCC varchar(100)
SET @BCC = (SELECT pv_Value FROM MadisonPortal.dbo.PortalVariables WHERE pv_Desc = 'PYPEmailBccAddress') 

--select the info for output
SELECT @em_AddressSENDER AS [Sender], @em_Address AS [Receiver], 'RYP Notification' AS [Subject], @TABLEHTML AS [Body], @BCC AS [TheBcc]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.RewardSendRewardNotificationToUser
TO portaluser