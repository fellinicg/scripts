USE [Rewards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.RewardGetUserAccountInfo
(
@u_MPGProgUserID int
)

AS

	SELECT dbo.ufGetPersonsFullName(@u_MPGProgUserID) AS [Name],
		s_GenerateDate, 
		s_AvailBal, 
		Case When s_tranAmount < 0 THEN '-' WHEN s_tranAmount > 0 THEN '+' ELSE '' END AS Balance,
		CASE WHEN s_tranAmount < 0 THEN s_tranAmount * -1 ELSE s_tranAmount END AS s_tranAmount, 
		s_tranDescr, 
		s_tranDate,
		s_tranType
	FROM ClarityCards.dbo.Statements
	INNER JOIN ClarityCards.dbo.Accounts ON s_InternalKey = ac_InternalKey
	INNER JOIN ClarityCards.dbo.ProgramUser ON ac_PUID = pu_PUID
	INNER JOIN ClarityCards.dbo.Users ON pu_ExternalKey = u_ExternalKey
	WHERE u_MPGProgUserID = @u_MPGProgUserID
	AND s_tranType IN (4010,4012,4040)
	AND s_Hidden = 0
	ORDER BY s_tranDate DESC, s_RecID DESC

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.RewardGetUserAccountInfo
TO portaluser