USE [Rewards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.RewardModifyReward
(
@p_Id int,
@p_ModifiedBy varchar(50),
@p_Points int,
@p_Comment varchar(500)
)
AS


DECLARE @Modifier varchar(200)

SET @Modifier = dbo.ufGetPersonsFullName(@p_ModifiedBy)

IF (@Modifier IS NULL OR @Modifier = 'Unknown') RETURN

UPDATE dbo.Points

SET p_Status = 0,
	p_DateAdded=GETDATE(),
	p_ModifiedBy = @p_ModifiedBy,
	p_Points = @p_Points,
	p_Appover_Comment = ' Modified by ' + @Modifier + ': '  +  @p_Comment
WHERE p_Id = @p_Id
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.RewardModifyReward
TO portaluser