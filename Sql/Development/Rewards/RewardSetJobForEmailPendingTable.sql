USE [Rewards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.RewardSetJobForEmailPendingTable

AS

--this is the approver group in the portal
DECLARE @APPROVER int
SET @APPROVER = 16

DECLARE @VALIDEMAILS int
DECLARE @EBODY varchar(150)
--get the count of the pending 
DECLARE @COUNTER int
SET @COUNTER = 
(
SELECT COUNT(p_Id)
FROM dbo.Points
WHERE p_Status = 1
)

IF @COUNTER = 0
	RETURN

SELECT ug_u_ID, EmailAddress AS em_Address, dbo.ufGetPersonsFullName(ug_u_ID) AS [Name]
INTO #TMP
FROM MadisonPortal.dbo.UserGroups
INNER JOIN GlobalUsers.dbo.tbEmailAddress ON ug_u_ID = UserAccountID
WHERE ug_gr_ID = @APPROVER
AND EMailAddressType = 'W'
ORDER BY IsPreferred DESC

--find out if there is at least one valid e-mail available for an approver
SET @VALIDEMAILS = 
(
SELECT COUNT(ug_u_ID)
FROM #TMP
)
IF @VALIDEMAILS = 0
	BEGIN
	--no valid email available, set an entry to be sent to Admin
	DROP TABLE #TMP
	INSERT INTO dbo.PendingEmails
		(pe_OriginCode, pe_Type, pe_FullName, pe_Email, pe_Body, pe_SendEmail)
	VALUES (1,'System','System Admin','ischobert@madisonpg.com','No valid e-mail address found for RYP approver',0)
	RETURN
	END

--have pending Rewards and found at least one approver
IF @COUNTER = 1
	SET @EBODY = 'You have 1 award pending for approval in the RYP program. '
ELSE
	SET @EBODY = 'You have ' + CONVERT(varchar,@COUNTER) + ' awards pending for approval in the RYP program. '

SET @EBODY = @EBODY + CHAR(13) + CHAR(10)
SET @EBODY = @EBODY + CHAR(13) + CHAR(10)
SET @EBODY = @EBODY + 'Please login to MADISON Portal at http://portal.madisonpg.com to approve, modify or deny this/these award(s). '
SET @EBODY = @EBODY + CHAR(13) + CHAR(10)
SET @EBODY = @EBODY + CHAR(13) + CHAR(10)
SET @EBODY = @EBODY + 'NOTE: You must be connected to the internet to access the above link.  If you cannot link from above, copy and paste the URL into your browser.'

--add the records to the pendingemail table
INSERT INTO dbo.PendingEmails
		(pe_OriginCode, pe_Type, pe_FullName, pe_Email, pe_Body, pe_SendEmail)
SELECT ug_u_ID, 'Approver', [Name], em_Address, @EBODY, 0
FROM #TMP

DROP TABLE #TMP
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.RewardSetJobForEmailPendingTable
TO portaluser