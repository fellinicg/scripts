USE [Rewards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.RewardGetDepartmentUsers 
@dpt_ID int
AS

--is the user group
DECLARE @USERS int
SET @USERS = 18

	SELECT 
		ug_u_ID AS gu_ID, 
		dbo.ufGetPersonsFullName(ug_u_ID) AS [Name]
	FROM MadisonPortal.dbo.UserGroups
	INNER JOIN MadisonPortal.dbo.Users ON ug_u_ID = u_ID
	WHERE ug_gr_ID = @USERS
	AND ug_u_ID NOT IN (SELECT depu_u_ID FROM dbo.DepartmentUsers)
	AND u_RecState = 0

	SELECT 
		depu_ID, 
		dbo.ufGetPersonsFullName(depu_u_ID) AS [Name]
	FROM dbo.DepartmentUsers 
	INNER JOIN MadisonPortal.dbo.Users ON depu_u_ID = u_ID
	WHERE depu_dep_ID = @dpt_ID
	AND u_RecState = 0

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.RewardGetDepartmentUsers
TO portaluser