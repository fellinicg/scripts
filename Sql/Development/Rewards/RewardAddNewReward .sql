USE [Rewards]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.RewardAddNewReward 
(
@p_UserID int,
@p_OriginCode int,
@p_Points int,
@p_Commentary varchar(8000),
@p_Comment varchar(1000)
)
AS

DECLARE @p_Origin int --dep head
DECLARE @p_PointsPeriod int --current period
DECLARE @p_Status int --status > 100 = 1 else 0
DECLARE @proc_error bit
DECLARE @Approver varchar(200)

SET @proc_error = 0

--set the status
IF @p_Points > 100
	SET @p_Status = 1
ELSE
	SET @p_Status = 0

--get the current points period
SET @p_PointsPeriod = 
	(
	SELECT bdr_id 
	FROM dbo.BudgetDateRanges 
	WHERE GETDATE() BETWEEN bdr_BudgetStart AND bdr_BudgetEnd
	)
IF @p_PointsPeriod IS NULL
  SET @proc_error = 1

--get the department id based on user id
SET @p_Origin = 
	(
	SELECT dpt_ID 
	FROM dbo.Departments
	WHERE dtp_DH_ID = @p_OriginCode
	)
IF @p_Origin IS NULL
  SET @proc_error = 1

--add the approvers name to the criteria field
SET @Approver = 
(
SELECT dbo.ufGetPersonsFullName(@p_OriginCode)
)
IF @Approver IS NULL
  SET @proc_error = 1
ELSE
  SET @p_Comment = @p_Comment   + char(10) + char(13) + ' By ' + @Approver

--now, insert into the table
IF @proc_error <> 1
BEGIN
	BEGIN TRANSACTION
	INSERT INTO dbo.Points WITH (TABLOCK)
		(
		p_UserID,
		p_Points,
		p_DK,
		p_ProgramID,
		p_Category, 
		p_Description, 
		p_Origin, 
		p_OriginCode, 
		p_Commentary, 
		p_Status, 
		p_TranID, 
		p_PointsDate, 
		p_PointsPeriod, 
		p_ExpDate, 
		p_DateAdded, 
		p_Comment
		)
	VALUES
		(
		@p_UserID,
		@p_Points,
		5981,
		2027,
		'MPG Rewards', 
		'Cash', 
		@p_Origin, 
		@p_OriginCode, 
		@p_Commentary, 
		@p_Status, 
		0, 
		GETDATE(), 
		@p_PointsPeriod, 
		'1/1/2020', 
		GETDATE(), 
		@p_Comment
		)
	IF @@ROWCOUNT > 0 AND @@ERROR = 0 
		BEGIN
			SELECT MAX(p_ID) FROM dbo.Points
			COMMIT TRANSACTION
		END
	ELSE
		BEGIN
			SELECT 0
			ROLLBACK TRANSACTION
		END
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.RewardAddNewReward
TO portaluser
