USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER  PROCEDURE dbo.POPGetVendors
(
@HierCode VARCHAR(11),
@POState INT =0,
@VendorNo INT =-1
)

AS

DECLARE @Where VARCHAR(2000)

/* Set WHERE clause. */
SET @Where = 'WHERE ps_Status = ' + CONVERT(VARCHAR, @POState) + ' AND u_RecState = 0'

/* Only filter by vendor if not called internally */
IF LEFT(@HierCode, 1) <> 'M' SET @Where = @Where + ' AND vst_sendto = ' + CONVERT(VARCHAR, @VendorNo)

/* Filter on null fields etc if not postate = 2 */
IF @POState < 3 SET @Where = @Where + ' AND ProcessDate >ISNULL(SettingsDescription, ''1/1/2005'')
AND al.Void = 0 AND al.VoidDate IS NULL AND VoidedBy IS NULL'

EXEC(
'SELECT DISTINCT VendNo, Company 
FROM dbo.PurchaseOrders
INNER JOIN dbo.POStatus ON po_ps_ID = ps_ID
INNER JOIN dbo.ItemLine il ON po_ID = il.PONumber
INNER JOIN dbo.AwardLine al ON il.AwardLineID = al.AwardLineID
INNER JOIN dbo.vwVendors ON VendorID = vendno
INNER JOIN SQL2KPROD.Catalog.dbo.VendorSendTo ON il.VendorID = vst_vendno 
INNER JOIN SQL2KPROD.MadisonPortal.dbo.Users ON vst_sendto = u_AltID 
LEFT JOIN tbSettings ON SettingsName = ''POBeginDate'' '
+ @Where + ' ORDER BY Company'
)