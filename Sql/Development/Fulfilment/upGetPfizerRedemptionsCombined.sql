USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.upGetPfizerRedemptionsCombined
	@DKNo int,
	@StartDate varchar(20) = NULL,
	@AggregateCriteria char(1) = 'D',
	@ItemType varchar(5) = '',
	@ShowVoids int = 0,
	@Verbose bit = 0
AS

SET NOCOUNT ON

	-- LOCAL VARIABLES
	DECLARE @EndDate datetime
	DECLARE	@GiftCertLineItemFee money
	DECLARE	@GiftCertServiceFee float
	DECLARE	@GiftCertShipping float
	DECLARE	@AmexLineItemFee money
	DECLARE	@AmexServiceFee float
	DECLARE	@AmexShipping float
	DECLARE @CreditsTotalPoints int
	DECLARE @CreditsTotalShipping money
	DECLARE @CreditsTotalFee money
	DECLARE @VoidsTotalPoints int
	DECLARE @VoidsTotalShipping money
	DECLARE @VoidsTotalFee money
	DECLARE @MonthlyRedemptionTotal money
	DECLARE @MonthlyVoidTotal money
	DECLARE @SQL nvarchar(4000)
	DECLARE @Return varchar(5)

	-- CONSTANTS
	SET @GiftCertLineItemFee = 4.65
	SET @GiftCertServiceFee = .0465
	SET @GiftCertShipping = 0.39
	SET @AmexLineItemFee = 6.00
	SET @AmexServiceFee = .0600
	SET @AmexShipping = 0.39
	SET @Return = CHAR(10) + CHAR(13)


	-- FORMAT START AND END DATES
	IF ISDATE ( @StartDate )  = 1
	BEGIN
		SET @StartDate = CONVERT(datetime, CONVERT(varchar(20), @StartDate, 101))
		SET @EndDate = DATEADD(s, -1, CONVERT(datetime, CONVERT(varchar(20), DATEADD(m,1,@StartDate), 101)))
	END

	IF ((IsDate(@StartDate) = 1) AND (isDate(@EndDate) = 1 ))
	BEGIN
		IF (@Verbose = 1)
		BEGIN
			Print 'Start Date: ' + CAST(@StartDate as varchar(20))
			Print 'End Date: ' + CAST(@EndDate as varchar(20))
		END
	END
	ELSE
	BEGIN
		RAISERROR ('upGetPfizerRedemptionsCombined - Invalid date range', 16, 1)
		RETURN
	END

	-- CREATE TABLE VARIABLES
	CREATE TABLE ##RedemptionsAll
	(
		ItemType char(1), 
		UserId int,  
		OrderId int, 
		OrderDate datetime, 
		ShipName varchar(50), 
		CostPerUnit money, 
		Quantity int,
		OrderCost money, 
		LineItemFee money, 
		ServiceFee varchar(10), 
		TotalFee money, 
		MerchantFee money, 
		TotalShipping money, 
		TotalTax money,
		TotalPoints money, 
		TotalCost money, 
		Void int, 
		VoidDate datetime,
		AwardId int, 
		AwardTitle varchar(100),
		Company varchar(50),
		VendorId varchar(50), 
		ItemModel varchar(100),
		DeptCode varchar(20), 
		ClusterCode varchar(20), 
		AreaCode varchar(20) 
		
	)

	DECLARE @Credits TABLE
	(
		DeptCode varchar(20),
		ClusterCode varchar(20),
		AreaCode varchar(20),
		TotalPoints int,
		TotalShipping money,
		TotalTax money,
		TotalFee money,
		MonthlyTotal money

	)

	DECLARE @Voids TABLE
	(
		DeptCode varchar(20),
		ClusterCode varchar(20),
		AreaCode varchar(20),
		TotalPoints int,
		TotalShipping money,
		TotalTax money,
		TotalFee money,
		MonthlyTotal money
	)


	-- GIFT CERTIFICATES
	INSERT INTO ##RedemptionsAll ( ItemType, UserId, OrderId, OrderDate, ShipName, CostPerUnit, Quantity, LineItemFee, ServiceFee, MerchantFee, TotalShipping, TotalTax, TotalPoints, Void, VoidDate, AwardId, AwardTitle, Company, VendorId, ItemModel, DeptCode, ClusterCode, AreaCode )
	(
	SELECT
		'G' as ItemType,
		r.UserAccountId as UserId,
		r.OrderId as OrderId,
		r.OrderDate as OrderDate,
		r.ShipName as ShipName,
		il.CostPerUnit as CostPerUnit,
		al.Quantity as Quantity,
		@GiftCertLineItemFee as LineItemFee,
		@GiftCertServiceFee as ServiceFee,
		0.00 as MerchantFee,
		( @GiftCertShipping * al.Quantity) as TotalShipping,
		0.00 as TotalTax,
		al.TotalPoints as TotalPoints,
		al.Void,
		al.VoidDate,
		al.AwardId,
		al.AwardTitle, 
		v.Company,
		il.VendorId, 
		il.ItemModel, 
		ISNULL(r.DeptCode,'UNKNOWN') DeptCode,
		ISNULL(r.ClusterCode,'UNKNOWN') ClusterCode,
		ISNULL(r.AreaCode,'UNKNOWN') AreaCode
	FROM 	vwPfizerExtendedFulfilmentOrder r
	JOIN AwardLine al ON (r.OrderId = al.OrderId)
	JOIN ItemLine il ON (al.AwardLineId = il.AwardLineId)
	LEFT OUTER JOIN dbo.vwVendors AS v ON (il.VendorID = v.vendno)
WHERE 
		r.projectnumber = @DKNo
		AND r.ExtensionRecMissing = 0
		AND il.itemtype = 'G'
		AND al.awardtitle NOT LIKE '%AMEX%'
	)

	-- AMEX
	INSERT INTO ##RedemptionsAll( ItemType, UserId, OrderId, OrderDate, ShipName, CostPerUnit, Quantity, LineItemFee, ServiceFee, MerchantFee, TotalShipping, TotalTax, TotalPoints, Void, VoidDate, AwardId, AwardTitle, Company, VendorId, ItemModel, DeptCode, ClusterCode, AreaCode    )
	(
	SELECT
		'A' as ItemType,
		r.UserAccountId as UserId,
		r.OrderId as OrderId,
		r.OrderDate as OrderDate,
		r.ShipName as ShipName,
		il.CostPerUnit as CostPerUnit,
		al.Quantity as Quantity,
		@AmexLineItemFee as LineItemFee,
		@AmexServiceFee as ServiceFee,
		0.00 as MerchantFee,
		( @AmexShipping * al.Quantity) as TotalShipping,
		0.00 as TotalTax,
		al.TotalPoints as TotalPoints,
		al.Void,
		al.VoidDate, 
		al.AwardId,
		al.AwardTitle, 
		v.Company,
		il.VendorId, 
		il.ItemModel, 
		ISNULL(r.DeptCode,'UNKNOWN') DeptCode,
		ISNULL(r.ClusterCode,'UNKNOWN') ClusterCode,
		ISNULL(r.AreaCode,'UNKNOWN') AreaCode
	FROM 	vwPfizerExtendedFulfilmentOrder r
	JOIN AwardLine al ON (r.OrderId = al.OrderId)
	JOIN ItemLine il ON (al.AwardLineId = il.AwardLineId)
	LEFT OUTER JOIN dbo.vwVendors AS v ON (il.VendorID = v.vendno)
	WHERE	
		r.projectnumber = @DKNo
		AND r.ExtensionRecMissing = 0
		AND il.itemtype = 'G'
		AND al.awardtitle LIKE '%AMEX%'
	)

	-- MERCHANDISE
	INSERT INTO ##RedemptionsAll ( ItemType, UserId, OrderId, OrderDate, ShipName, CostPerUnit, Quantity, LineItemFee, ServiceFee, MerchantFee, TotalShipping, TotalTax, TotalPoints, Void, VoidDate, AwardId, AwardTitle, Company, VendorId, ItemModel, DeptCode, ClusterCode, AreaCode   )
	(
	SELECT
		'M' as ItemType,
		r.UserAccountId as UserId,
		r.OrderId as OrderId,
		r.OrderDate as OrderDate,
		r.ShipName as ShipName,
		il.CostPerUnit as CostPerUnit,
		al.Quantity as Quantity,
		0.00 as LineItemFee,
		'0.00' as ServiceFee,
		0.00 as MerchantFee,
		il.TotalShipping as TotalShipping,
		il.TotalTax as TotalTax,
		al.TotalPoints as TotalPoints,
		al.Void,
		al.VoidDate, 
		al.AwardId,
		al.AwardTitle, 
		v.Company,
		il.VendorId, 
		il.ItemModel, 
		ISNULL(r.DeptCode,'UNKNOWN') DeptCode,
		ISNULL(r.ClusterCode,'UNKNOWN') ClusterCode,
		ISNULL(r.AreaCode,'UNKNOWN') AreaCode
	FROM 	vwPfizerExtendedFulfilmentOrder r
	JOIN AwardLine al ON (r.OrderId = al.OrderId)
	JOIN ItemLine il ON (al.AwardLineId = il.AwardLineId)
	LEFT OUTER JOIN dbo.vwVendors AS v ON (il.VendorID = v.vendno)
	WHERE	
		r.projectnumber = @DKNo
		AND r.ExtensionRecMissing = 0
		AND il.itemtype = 'M'
	)

	-- FIX CUSTOM ORDERS
	--UPDATE ##RedemptionsAll
	--SET CostPerUnit = CostPerUnit / 100
	--WHERE ITEMTYPE = 'G'
	--AND PATINDEX('%,%', ShipName) > 0

	-- CALCULATE ORDER COST
	UPDATE ##RedemptionsAll
	SET OrderCost = ( CostPerUnit * Quantity )

	-- CALCULATE TOTAL COST
	UPDATE ##RedemptionsAll
	SET TotalCost = (CostPerUnit * Quantity) + (TotalShipping)  + (TotalTax)

	-- APPLY FIXES TO GIFT CERTIFICATES
	UPDATE ##RedemptionsAll
	SET TotalFee = ( OrderCost * @GiftCertServiceFee ) + @GiftCertLineItemFee
	WHERE ITEMTYPE = 'G'

	-- APPLY FIXES TO AMEX
	UPDATE ##RedemptionsAll
	SET TotalFee = ( OrderCost * @AmexServiceFee ) + @AmexLineItemFee
	WHERE ITEMTYPE = 'A'

	-- APPLY FIXES TO MERCHANDISE
	UPDATE ##RedemptionsAll
	SET MerchantFee = ( CostPerUnit / 100 ) - TotalCost
	WHERE ITEMTYPE = 'M'

	-- FIX NULLS
	UPDATE ##RedemptionsAll
			SET TotalFee = 0 
	WHERE TotalFee IS NULL

	UPDATE ##RedemptionsAll
			SET TotalShipping = 0 
	WHERE TotalShipping IS NULL

	UPDATE ##RedemptionsAll
			SET TotalPoints = 0 
	WHERE TotalPoints IS NULL

	IF (@AggregateCriteria = 'D')
	BEGIN
		-- GROUP BY DEPARTMENTS

		-- INSERT CREDITS
		INSERT INTO @Credits ( DeptCode, TotalPoints, TotalShipping, TotalTax, TotalFee, MonthlyTotal )
		SELECT
				Redemptions.DeptCode,
				SUM(Redemptions.TotalPoints) TotalPoints,
				SUM(Redemptions.TotalShipping) TotalShipping,
				SUM(Redemptions.TotalTax) TotalTax,
				SUM(Redemptions.TotalFee) TotalFee,
				SUM(Redemptions.TotalPoints/100 + Redemptions.TotalShipping + Redemptions.TotalFee) as MonthlyTotal
		FROM ##RedemptionsAll Redemptions
		WHERE Redemptions.OrderDate BETWEEN @StartDate AND @EndDate 
		GROUP BY Redemptions.DeptCode
		ORDER BY Redemptions.DeptCode
		

		-- INSERT VOIDS
		INSERT INTO @Voids ( DeptCode, TotalPoints, TotalShipping, TotalTax, TotalFee, MonthlyTotal )
		SELECT
				Redemptions.DeptCode,
				SUM(Redemptions.TotalPoints) TotalPoints,
				SUM(Redemptions.TotalShipping) TotalShipping,
				SUM(Redemptions.TotalTax) TotalTax,
				SUM(Redemptions.TotalFee) TotalFee,
				SUM(Redemptions.TotalPoints/100 + Redemptions.TotalShipping + Redemptions.TotalFee) as MonthlyTotal
		FROM ##RedemptionsAll Redemptions
		WHERE Redemptions.VoidDate BETWEEN @StartDate AND @EndDate
		GROUP BY Redemptions.DeptCode
		ORDER BY Redemptions.DeptCode
		-- RESULTSET
		SELECT
				ISNULL(Credits.DeptCode,Voids.DeptCode) DeptCode,
				--ISNULL(Credits.MonthlyTotal,0) Credits, 
				--ISNULL(Voids.MonthlyTotal,0) Voids, 
				ISNULL(Credits.MonthlyTotal,0) - ISNULL(Voids.MonthlyTotal,0) as Total
		FROM @Credits Credits
		FULL OUTER JOIN @Voids Voids ON(Voids.DeptCode = Credits.DeptCode)
		ORDER BY Credits.DeptCode
	END
	ELSE IF ( @AggregateCriteria = 'C' )
	BEGIN

		-- GROUP BY CLUSTER/AREA

		-- INSERT CREDITS
		INSERT INTO @Credits ( ClusterCode, AreaCode, TotalPoints, TotalShipping, TotalTax, TotalFee, MonthlyTotal )
		SELECT
				Redemptions.ClusterCode ClusterCode,
				Redemptions.AreaCode AreaCode,
				SUM(Redemptions.TotalPoints) TotalPoints,
				SUM(Redemptions.TotalShipping) TotalShipping,
				SUM(Redemptions.TotalTax) TotalTax,
				SUM(Redemptions.TotalFee) TotalFee,
				SUM(Redemptions.TotalPoints/100 + Redemptions.TotalShipping + Redemptions.TotalFee) as MonthlyTotal
		FROM ##RedemptionsAll Redemptions
		WHERE Redemptions.OrderDate BETWEEN @StartDate AND @EndDate
		GROUP BY Redemptions.ClusterCode, Redemptions.AreaCode
		ORDER BY Redemptions.ClusterCode, Redemptions.AreaCode

		-- INSERT VOIDS
		INSERT INTO @Voids ( ClusterCode, AreaCode, TotalPoints, TotalShipping, TotalTax, TotalFee, MonthlyTotal )
		SELECT
				--ISNULL(Positions.DeptCode,'UNKNOWN') Department,
				Redemptions.ClusterCode ClusterCode, 
				Redemptions.AreaCode AreaCode,
				SUM(Redemptions.TotalPoints) TotalPoints,
				SUM(Redemptions.TotalShipping) TotalShipping,
				SUM(Redemptions.TotalTax) TotalTax,
				SUM(Redemptions.TotalFee) TotalFee,
				SUM(Redemptions.TotalPoints/100 + Redemptions.TotalShipping + Redemptions.TotalFee) as MonthlyTotal
		FROM ##RedemptionsAll Redemptions
		WHERE (Redemptions.VoidDate BETWEEN @StartDate AND @EndDate)
		GROUP BY Redemptions.ClusterCode, Redemptions.AreaCode

		-- RESULTSET
		SELECT
				Credits.ClusterCode,
				Credits.AreaCode,
				(ISNULL(Credits.MonthlyTotal,0)) - (ISNULL(Voids.MonthlyTotal,0)) as Total
		FROM @Credits Credits
		LEFT JOIN @Voids Voids ON(Credits.ClusterCode = Voids.ClusterCode) AND (Credits.AreaCode = Voids.AreaCode)
		ORDER BY Credits.ClusterCode, Credits.AreaCode

	END
	ELSE IF ( @AggregateCriteria = 'A' )
	BEGIN
		SET @SQL = N'SELECT r.orderid as OrderId, ' + @Return
		SET @SQL = @SQL + N'r.AwardId as AwardId,'  + @Return
		SET @SQL = @SQL + N'r.orderdate as OrderDate, '  + @Return
		SET @SQL = @SQL + N'r.userid as UserId, '  + @Return
		SET @SQL = @SQL + N'r.shipname as ShipName,'  + @Return
		SET @SQL = @SQL + N'r.itemtype as ItemType, '  + @Return
		SET @SQL = @SQL + N'r.awardtitle as AwardTitle, '  + @Return
		SET @SQL = @SQL + N'r.company as VendorName,'  + @Return
		SET @SQL = @SQL + N'r.VoidDate as VoidDate,'  + @Return
		SET @SQL = @SQL + N'r.vendorid as VendorId,'  + @Return
		SET @SQL = @SQL + N'r.itemmodel as ItemModel,'  + @Return
		SET @SQL = @SQL + N'r.costperunit as CostPerUnit, '  + @Return
		SET @SQL = @SQL + N'r.lineitemfee as LineItemFee, '  + @Return
		SET @SQL = @SQL + N'r.servicefee as ServiceFee, '  + @Return
		SET @SQL = @SQL + N'r.merchantfee as MerchantFee, '  + @Return
		SET @SQL = @SQL + N'r.totalcost as TotalCost, '  + @Return
		SET @SQL = @SQL + N'r.totaltax as TotalTax, '  + @Return
		SET @SQL = @SQL + N'r.totalshipping as TotalShipping,'  + @Return
		SET @SQL = @SQL + N'r.totalpoints as TotalPoints, '  + @Return
		SET @SQL = @SQL + N'ISNULL(r.totalfee,0) as TotalFee, '  + @Return
		SET @SQL = @SQL + N'(ISNULL(r.totalpoints,0)/100) + (ISNULL(r.totalshipping,0)) + (isnull(r.totalfee,0)) as Total, '  + @Return
		SET @SQL = @SQL + N'r.quantity as Quantity,'  + @Return
		SET @SQL = @SQL + N'r.void as void,'  + @Return
		SET @SQL = @SQL + N'ISNULL(r.DeptCode,'''') as DeptCode,'  + @Return
		SET @SQL = @SQL + N'ISNULL(r.ClusterCode,'''') as Cluster,'  + @Return
		SET @SQL = @SQL + N'ISNULL(r.AreaCode,'''') as AreaCode'  + @Return
		SET @SQL = @SQL + N'FROM ##RedemptionsAll r'  + @Return
		SET @SQL = @SQL + N'WHERE 1=1 '  + @Return
		IF ( @ShowVoids = 1 )
		BEGIN
			SET @SQL = @SQL + N'AND r.VoidDate BETWEEN ''' + CAST(@StartDate as nvarchar(20)) + N''' AND ''' + CAST(@EndDate as nvarchar(20)) + N''''  + @Return
		END
		ELSE
		BEGIN
			SET @SQL = @SQL + N'AND r.OrderDate BETWEEN ''' + CAST(@StartDate as nvarchar(20)) + N''' AND ''' + CAST(@EndDate as nvarchar(20)) + N''''  + @Return
		END

		IF ( @ItemType IN ('CO') )
		BEGIN
			SET @SQL = @SQL + N'AND PATINDEX ( ''%,%'', r.shipname ) > 0'  + @Return
		END

		IF ( @ItemType IN ('M','G') )
		BEGIN
			SET @SQL = @SQL + N'AND r.itemtype = ''' + CAST(@ItemType as nvarchar(3)) + N'''' + @Return
		END

		SET @SQL = @SQL + N'ORDER BY r.orderdate desc, r.orderid'  + @Return
		
		--IF (@Verbose = 1)
			PRINT @SQL
			
		EXEC sp_executesql @SQL


	END


		SELECT @CreditsTotalPoints = SUM(TotalPoints), @CreditsTotalShipping = SUM(TotalShipping), @CreditsTotalFee = SUM(TotalTax) FROM @Credits
		SELECT @VoidsTotalPoints = SUM(TotalPoints), @VoidsTotalShipping = SUM(TotalShipping), @VoidsTotalFee = SUM(TotalTax) FROM @Voids
		SELECT @MonthlyRedemptionTotal = SUM(MonthlyTotal) FROM @Credits
		SELECT @MonthlyVoidTotal = SUM(MonthlyTotal) FROM @Voids

		IF (@Verbose = 1)
		BEGIN
			Print '-------------------------------------------------'
			Print @AggregateCriteria
			Print 'Redemption Points: ' + CAST(@CreditsTotalPoints as varchar(10))
			Print 'Redemption Shipping: ' + CAST(@CreditsTotalShipping as varchar(10))
			Print 'Redemption Tax: ' + CAST(@CreditsTotalFee as varchar(10))
			Print 'Total Redemption: ' + CAST(@MonthlyRedemptionTotal as varchar(20))
			Print '-------------------------------------------------'
			Print 'Total Voided Points: ' + CAST(@VoidsTotalPoints as varchar(10))
			Print 'Total Voided Shipping: ' + CAST(@VoidsTotalShipping as varchar(10))
			Print 'Total Voided Tax: ' + CAST(@VoidsTotalFee as varchar(10))
			Print 'Total Redemption: ' + CAST(@MonthlyVoidTotal as varchar(20))
			Print '-------------------------------------------------'
			SELECT @MonthlyRedemptionTotal - @MonthlyVoidTotal as TotalCost
			Print '-------------------------------------------------'
		END

	-- DROP GLOBAL TABLE
	DROP table ##RedemptionsAll

	SET NOCOUNT OFF





