USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROC dbo.POPCreatePODownloadDelayNotifications 
AS
DECLARE 
  @POAdminName varchar(200),
  @POAdminPhoneNum varchar(20),
  @POAdminEmailAddress varchar(200),
  @PortalAdminName varchar(200),
  @PortalAdminEmailAddress varchar(200),
  @PortalMessageArchiveEmailAddress varchar(200),
  @NormalMessageSubject varchar(200),
  @NormalMessageBody varchar(1024),
  @ErrorMessageSubject varchar(200),
  @ErrorMessageBody varchar(1024),
  @PODownloadDelayNotificationDays int,
  @PortalURL varchar(200),
  @RecipientEmailAddressList varchar(1024),
  @CurrVendorID int,
  @CurrVendorName varchar(35),
  @CurrEmailAddressID int,
  @DoubleCRLF char(4)
DECLARE @EligiblePOVendor table 
  (
   VendorID int not null, 
   RecipientEmailAddressList varchar(200) null,
   MessageSubject varchar(200) null,
   MessageBody varchar(1024) null,
   HasNoEmailAddress bit not null default 0
  )
DECLARE @VendorEmailAddress table
  (
   EmailAddressID int identity,
   VendorID int not null,
   EmailAddress varchar(200) not null
  )
  
SET NOCOUNT ON
  
-- set vars & constants
SET @POAdminName = (SELECT pv_Value FROM SQL2KPROD.MadisonPortal.dbo.PortalVariables WHERE pv_Desc = 'POAdminName')
SET @POAdminPhoneNum = (SELECT pv_Value FROM SQL2KPROD.MadisonPortal.dbo.PortalVariables WHERE pv_Desc = 'POAdminPhoneNum')
SET @POAdminEmailAddress = (SELECT pv_Value FROM SQL2KPROD.MadisonPortal.dbo.PortalVariables WHERE pv_Desc = 'POAdminEmailAddress') 
SET @PortalAdminName = (SELECT pv_Value FROM SQL2KPROD.MadisonPortal.dbo.PortalVariables WHERE pv_Desc = 'PortalAdminName')
SET @PortalAdminEmailAddress = (SELECT pv_Value FROM SQL2KPROD.MadisonPortal.dbo.PortalVariables WHERE pv_Desc = 'PortalAdminEmailAddress')
SET @PortalMessageArchiveEmailAddress = (SELECT pv_Value FROM SQL2KPROD.MadisonPortal.dbo.PortalVariables WHERE pv_Desc = 'POEmailBccAddress') 
SET @NormalMessageSubject = 'Unprocessed Purchase Orders Detected'
SET @ErrorMessageSubject = 'PO System Error - VendorID (<VendorID>) has no email address'
SET @PODownloadDelayNotificationDays = (SELECT convert(int, pv_Value) FROM SQL2KPROD.MadisonPortal.dbo.PortalVariables WHERE pv_Desc = 'PODownloadDelayNotificationDays')
SET @PortalURL = (SELECT pv_Value FROM SQL2KPROD.MadisonPortal.dbo.PortalVariables WHERE pv_Desc = 'PortalURL')
SET @DoubleCRLF = CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10)
 
SET @NormalMessageBody = 'Dear Vendor, '
  + @DoubleCRLF
  + 'Our records indicate that at least ' 
  + convert(varchar(20), @PODownloadDelayNotificationDays) 
  + ' days have passed and orders have yet to be downloaded. '
  + 'We ask that you please process the purchase order(s) immediately. If you have any questions, '
  + 'please contact '
  + @POAdminName
  + ', Merchandising Administrator '
  + @POAdminPhoneNum
  + ' or e-mail '
  + @POAdminEmailAddress
  + '. If you experience any technical difficulties while using this portal, send an e-mail to '
  + @PortalAdminEmailAddress
  + '. '
  + @DoubleCRLF
  + 'In order to access the PO Platform, please click on the below link or copy and paste onto your web browser. '
  + @DoubleCRLF
  + @PortalURL
  + @DoubleCRLF
  + 'Thank you, '
  + @DoubleCRLF
  + @POAdminName
  + ' '
  + @DoubleCRLF
  + 'Madison Performance Group'
  
SET @ErrorMessageBody = 'A Delayed PO download notification could not be sent to the following vendor as the '
  + 'vendor has no email address defined. Please provide the Madison Portal administrator (' 
  + @PortalAdminName 
  + ') with an email address for this vendor.'
  + @DoubleCRLF
  + 'VendorID: <VendorID>'
  + @DoubleCRLF
  + 'Vendor Name: <VendorName>'

-- populate qualified vendors table
INSERT @EligiblePOVendor(VendorID)
SELECT DISTINCT vst_sendto
FROM dbo.POStatus
  INNER JOIN dbo.PurchaseOrders ON (po_ps_ID = ps_ID)
  INNER JOIN dbo.ItemLine il ON (PONumber = po_ID)
  INNER JOIN dbo.AwardLine al ON (il.AwardLineID = al.AwardLineID)
  INNER JOIN SQL2KPROD.Catalog.dbo.VendorSendTo ON (VendorID = vst_vendno)
  INNER JOIN SQL2KPROD.MadisonPortal.dbo.Users ON (vst_sendto = u_AltID AND u_RecState = 0)
WHERE ps_Status = 1
AND al.Void = 0 AND al.VoidDate IS NULL AND VoidedBy IS NULL 
  AND DATEDIFF(dd, ps_TimeStamp, CURRENT_TIMESTAMP) > @PODownloadDelayNotificationDays
  
-- populate vendor email addresses table
INSERT @VendorEmailAddress(VendorID, EmailAddress)
SELECT v.VendorID, e.EmailAddress
FROM @EligiblePOVendor v
  INNER JOIN SQL2KPROD.GlobalUsers.dbo.tbUserAccount u ON (u.AltAccountCode = v.VendorID)
  INNER JOIN SQL2KPROD.GlobalUsers.dbo.tbEmailAddress e ON (e.UserAccountID = u.UserAccountID and e.EmailAddress <> '')

-- set HasNoEmailAddress for vendors with no email address
UPDATE @EligiblePOVendor
SET HasNoEmailAddress = 1
FROM @EligiblePOVendor v
WHERE NOT EXISTS (SELECT 1 from @VendorEmailAddress e WHERE v.VendorID = e.VendorID)

-- set RecipientEmailAddressList for vendors with email address
SELECT @CurrVendorID = MIN(VendorID) FROM @EligiblePOVendor WHERE HasNoEmailAddress = 0
WHILE @CurrVendorID is not null
BEGIN
  -- get first email address for current vendor
  SELECT @CurrEmailAddressID = MIN(EmailAddressID)
  FROM @VendorEmailAddress 
  WHERE VendorID = @CurrVendorID
  SELECT @RecipientEmailAddressList = EmailAddress
  FROM @VendorEmailAddress 
  WHERE EmailAddressID = @CurrEmailAddressID
  -- get second email address for current vendor  
  SELECT 
    @CurrEmailAddressID = MIN(EmailAddressID)
  FROM @VendorEmailAddress
  WHERE VendorID = @CurrVendorID
    AND EmailAddressID > @CurrEmailAddressID
  -- add additional email addresses to address list if any
  WHILE @CurrEmailAddressID is not null
  BEGIN
    SELECT @RecipientEmailAddressList = @RecipientEmailAddressList + ', ' + EmailAddress 
    FROM @VendorEmailAddress 
    WHERE EmailAddressID = @CurrEmailAddressID
    SELECT 
      @CurrEmailAddressID = MIN(EmailAddressID)
    FROM @VendorEmailAddress
    WHERE VendorID = @CurrVendorID
      AND EmailAddressID > @CurrEmailAddressID
  END
  -- update eligible vendor record
  UPDATE @EligiblePOVendor 
  SET 
    RecipientEmailAddressList = @RecipientEmailAddressList,
    MessageSubject = @NormalMessageSubject,
    MessageBody = @NormalMessageBody
  WHERE VendorID = @CurrVendorID
  SET @RecipientEmailAddressList = null
  -- get next vendor
  SELECT @CurrVendorID = MIN(VendorID) FROM @EligiblePOVendor WHERE HasNoEmailAddress = 0 AND VendorID > @CurrVendorID
END

-- handle vendors with no email address
SELECT @CurrVendorID = MIN(VendorID) FROM @EligiblePOVendor WHERE HasNoEmailAddress = 1
WHILE @CurrVendorID is not null
BEGIN
  SELECT @CurrVendorName = Company
  FROM dbo.vwVendors
	WHERE vendno = @CurrVendorID
  -- set error condition MessageSubject and MessageBody
  UPDATE @EligiblePOVendor
  SET 
    RecipientEmailAddressList = @POAdminEmailAddress,
    MessageSubject = REPLACE(@ErrorMessageSubject, '<VendorID>', convert(varchar(20), @CurrVendorID)),
    MessageBody = REPLACE(REPLACE(@ErrorMessageBody, '<VendorName>', @CurrVendorName), '<VendorID>', convert(varchar(20), @CurrVendorID))
  WHERE VendorID = @CurrVendorID
  -- get next eligible vendor
  SELECT @CurrVendorID = MIN(VendorID) 
  FROM @EligiblePOVendor 
  WHERE HasNoEmailAddress = 1
    AND VendorID > @CurrVendorID
END

-- create records in tbPODownloadDelayEmail
INSERT dbo.tbPODownloadDelayEmail (pdd_VendorID, pdd_SenderEmailAddress, pdd_RecipientEmailAddressList, pdd_BCCEmailAddress,
  pdd_MessageSubject, pdd_MessageBody, pdd_RecordDateTime, pdd_Status, pdd_StatusDateTime) 
SELECT VendorID, @POAdminEmailAddress, RecipientEmailAddressList, @PortalMessageArchiveEmailAddress, MessageSubject, 
  MessageBody, CURRENT_TIMESTAMP, 'P', CURRENT_TIMESTAMP
FROM @EligiblePOVendor