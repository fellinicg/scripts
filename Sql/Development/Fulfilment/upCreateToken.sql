USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061122
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upCreateToken 
	@ProgramID int, 
	@UserId int,
	@ClientID int, 
	@AccountID int,
	@DKNumber int,
	@Token varchar(36) OUTPUT
AS
BEGIN
	SET NOCOUNT ON

	SET @Token = NEWID()

	INSERT INTO dbo.StoreFrontTokens (TokenID, PID, UID, ClientID, AccountID, DKNumber)
	VALUES (@Token, @ProgramID, @UserId, @ClientID, @AccountID, @DKNumber)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upCreateToken
TO system