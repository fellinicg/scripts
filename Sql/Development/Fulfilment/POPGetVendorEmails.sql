USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE dbo.POPGetVendorEmails 

AS
SELECT 
	ua.AltAccountCode AS [ID], 
	ua.AccountName AS [Vendor],
	company AS [Name in SBT], 
	em.EmailAddress AS [E-Mail Address],
	CASE em.EMailAddressType 
		WHEN 'H' THEN 'Home'
		WHEN 'W' THEN 'Work'
		WHEN 'M' THEN 'Mobile'
		WHEN 'O' THEN 'Other'
		ELSE 'Unknown'
	END AS [Contact Type]
FROM SQL2KPROD.GlobalUsers.dbo.tbUserAccount ua
INNER JOIN SQL2KPROD.MadisonPortal.dbo.Users u ON ua.UserAccountID = u.u_ID
INNER JOIN dbo.vwVendors ON ua.AltAccountCode = vendno
LEFT JOIN SQL2KPROD.GlobalUsers.dbo.tbEmailAddress em ON ua.UserAccountID = em.UserAccountID
WHERE u_Type = 'V'
AND AccountStatus = 'E'
AND u_RecState = 0
ORDER BY ua.AccountName
