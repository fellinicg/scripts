USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20070216
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upSetToVoidForAgresso 
AS
BEGIN
	SET NOCOUNT ON

	UPDATE dbo.ItemLine SET AgressoStatus = 'V'
	FROM dbo.ItemLine il
	INNER JOIN dbo.AwardLine al ON il.AwardLineID = al.AwardLineID AND al.Void = 1
	WHERE il.AgressoStatus = 'P'

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
