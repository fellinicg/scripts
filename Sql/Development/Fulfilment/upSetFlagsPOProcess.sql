USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20070221
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upSetFlagsPOProcess 
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Set AgressoStatus to Void
	UPDATE dbo.ItemLine SET AgressoStatus = 'V'
	FROM dbo.ItemLine il
	INNER JOIN dbo.AwardLine al ON il.AwardLineID = al.AwardLineID AND al.Void = 1
	WHERE il.AgressoStatus = 'P'

	-- Set Processed and PrecessDate in Awardline
	UPDATE dbo.AwardLine SET Processed = 1, ProcessDate = CURRENT_TIMESTAMP
	FROM dbo.ItemLine il
	INNER JOIN dbo.AwardLine al ON il.AwardlineID = al.AwardLineID
	WHERE il.AgressoStatus = 'P'

END

