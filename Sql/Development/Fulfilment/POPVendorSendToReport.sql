USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.POPVendorSendToReport

AS

SELECT 
vst_vendno AS [Vendor ID], 
VEND.company AS [Vendor],
CASE WHEN  vst_sendto = vst_vendno THEN 'Self' ELSE 'Other' End AS [Sends To],
vst_sendto AS [Send To ID], 
SEND.company AS [Send To Vendor]
FROM SQL2KPROD.Catalog.dbo.VendorSendTo
INNER JOIN dbo.vwVendors VEND ON vst_vendno = VEND.vendno 
INNER JOIN dbo.vwVendors SEND ON vst_sendto = SEND.vendno 
ORDER BY VEND.company, SEND.company
