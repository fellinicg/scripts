USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE dbo.POPUpdateItemDetail
(
@ShipVia varchar(100) = NULL,
@ShippingStatus varchar(200) = NULL,
@TrackingNumber varchar(100) = NULL,
@POP_Notes varchar(500) = NULL,
@PONumber int,
@POLine int,
@pin_Type varchar(20) = NULL,
@pin_Notes varchar(5000) = NULL,
@pin_gu_ID int = NULL
)
AS

DECLARE @GetMCID int

BEGIN TRANSACTION

UPDATE dbo.ItemLine
SET	ShipVia = @ShipVia, 
	ShippingStatus = @ShippingStatus, 
	TrackingNumber = @TrackingNumber, 
	POP_Notes = @POP_Notes

WHERE PONumber = @PONumber AND POLine = @POLine

IF @@ROWCOUNT = 0 OR @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN
END

SET @GetMCID = (SELECT ItemID FROM dbo.ItemLine WHERE PONumber = @PONumber AND POLine = @POLine)
IF @GetMCID IS NULL
BEGIN
	ROLLBACK TRANSACTION
	RETURN
END

--deleted first if it exists in POPItemNotes
DELETE FROM dbo.POPItemNotes WHERE pin_mc_ID = @GetMCID 
IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN
END

--add new record into POPItemNotes, if provided
IF @pin_Type IS NOT NULL OR @pin_Notes IS NOT NULL
BEGIN	
	INSERT INTO dbo.POPItemNotes
		(pin_mc_ID, pin_Type, pin_Notes, pin_PONumber, pin_POLine, pin_gu_ID)
	VALUES
		(@GetMCID, @pin_Type, @pin_Notes, @PONumber, @POLine, @pin_gu_ID)
	IF @@ROWCOUNT = 0 OR @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
END

COMMIT TRANSACTION
