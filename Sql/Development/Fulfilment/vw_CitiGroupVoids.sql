USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW dbo.vw_CitiGroupVoids
AS
	/*
	SELECT 	CONVERT(VARCHAR, oh.OrderDate, 101) AS 'OrderDate',
		oh.ProjectNumber,
		oh.OrderID,
		oh.ShipName,
		cu.u_GEID,
		cu.u_LastName+' '+u_FirstName AS FullName,
		cc.CountryName,
		CONVERT(VARCHAR, al.VoidDate, 101) AS 'Void Date',
		al.AwardTitle,
		il.PONumber,
		il.POLine,
		al.VoidDate
	*/

	SELECT u_ID, u_GEID,(u_FirstName + ' ' + u_LastName) as FullName, CountryName,
	     oh.*, 
	    al.AwardID, al.AwardTitle, al.Quantity, al.UnitPoints, 
	    al.TotalPoints, al.Void, al.VoidDate, SUBSTRING(il.ItemType, 1, 1) AS ItemType, 
	    il.ItemModel,il.CostPerUnit, il.ShippingPerUnit, 
	    il.TaxPerUnit, il.TotalCost, il.TotalShipping, il.TotalTax, 
	    il.ShippingFlag, il.ItemID,  il.VendorID, 
	    IsNull(v.company,'Invalid Vendor! ID:'+cast(il.VendorID as varchar(10))) 'company'
	FROM OrderHeader oh 
		INNER JOIN AwardLine al ON oh.OrderID = al.OrderID 
		INNER JOIN ItemLine il ON al.AwardLineID = il.AwardLineID
		LEFT JOIN dbo.vwVendors AS v ON (il.VendorID = v.vendno)
		INNER JOIN SQL2KPRODZEBRA.CITIGroup.dbo.citiUser cu ON oh.UserID = cu.u_ID
		INNER JOIN SQL2KPRODZEBRA.CITIGroup.dbo.citiCountry cc ON cu.u_CountryID = cc.CountryID
	WHERE al.Void = 1 AND ClientID=19 


