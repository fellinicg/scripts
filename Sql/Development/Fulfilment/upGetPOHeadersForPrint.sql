USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20070215
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upGetPOHeadersForPrint 
	@POBegin int, 
	@POEnd int
AS
BEGIN

	SET NOCOUNT ON

	SELECT DISTINCT
		il.PONumber,
		il.SBTStatusDate,
		il.VendorID,
		ISNULL(ag.description, '') 'Vendor',
		RTRIM(ISNULL(ag.address, '')) 'Address',
		RTRIM(ISNULL(ag.place, '')) 'City',
		RTRIM(ISNULL(ag.province, '')) 'State',
		RTRIM(ISNULL(ag.zip_code, '')) 'Zip',
		RTRIM(ISNULL(ag.telephone_7, '')) 'Contact',
		RTRIM(ISNULL(ag.telephone_1, '')) 'Phone',
		RTRIM(ISNULL(ag.telephone_2, '')) 'Fax'
	FROM FulFilment.dbo.ItemLine il 
	LEFT JOIN SQL2KPROD2.Agresso.dbo.agladdress ag ON ag.client = 'MP' AND CONVERT(varchar, il.VendorID) = ag.dim_value
	WHERE il.PONumber BETWEEN @POBegin AND @POEnd
	ORDER BY il.PONumber

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upGetPOHeadersForPrint
TO system