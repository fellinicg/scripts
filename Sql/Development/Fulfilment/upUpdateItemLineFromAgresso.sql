USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20070220
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20070221
-- ============================================================
CREATE PROCEDURE dbo.upUpdateItemLineFromAgresso 
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Store info in local temporary table to avoid Fulfilment crash
	SELECT ItemLineID AS ItmLineID, order_id, line_no, deliv_date
	INTO #Temp
	FROM dbo.ItemLine
	INNER JOIN SQL2KPROD2.Agresso.dbo.apodetail ON CONVERT(varchar, ItemLineID) = sup_article
	WHERE AgressoStatus = 'P'

	-- Update Processing POs to Request Processed (R)
	UPDATE dbo.ItemLine SET AgressoStatus = 'R'
	FROM dbo.ItemLine
	INNER JOIN #Temp ON ItemLineID = ItmLineID

	-- Update info for items that were successfully processed
	UPDATE dbo.ItemLine SET AgressoStatus = 'C'
	, PONumber = order_id
	, POLine = line_no
	,	SBTStatusDate = deliv_date
	,	SBTStatus = 'O'
	FROM dbo.ItemLine
	INNER JOIN #Temp ON ItemLineID = ItmLineID

	-- Clean up
	DROP TABLE #Temp


END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
