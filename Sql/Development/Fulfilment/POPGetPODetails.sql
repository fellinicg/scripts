USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE dbo.POPGetPODetails
(
@PONum 	INT,
@ShowVoided	INT = 0
)

AS

DECLARE @WHERE varchar(500)

SET @WHERE =' WHERE ' + CASE @ShowVoided WHEN 0 THEN 'al.Void = 0 AND '
		ELSE '' END
SET @WHERE = @WHERE + 'PONumber = ' + convert(varchar,@PONum) + ' '

/* Retrieve details for specified PO.*/
EXECUTE (
'SELECT DISTINCT 
il.VendorID, 
COMP.Company, 
il.POLine, 
CASE LEN(ISNULL(CONVERT(VARCHAR, al.POP_Quantity), '''')) WHEN 0 THEN al.Quantity ELSE al.POP_Quantity END AS Quantity,
oh.ProjectNumber, 
CASE LEN(ISNULL(CONVERT(VARCHAR, il.POP_ItemTitle), '''')) WHEN 0 THEN il.ItemTitle ELSE il.POP_ItemTitle END AS ItemTitle,
CASE LEN(ISNULL(CONVERT(VARCHAR, il.POP_ItemModel), '''')) WHEN 0 THEN il.ItemModel ELSE il.POP_ItemModel END AS ItemModel,
CASE LEN(RTRIM(ISNULL(il.POP_ItemOptions, ''''))) WHEN 0 THEN il.ItemOptions ELSE il.POP_ItemOptions END AS ItemOptions,
oh.ShipName, 
oh.ShipAddress1, 
oh.ShipAddress2, 
oh.ShipCity, 
oh.ShipState, 
oh.ShipPostal, 
oh.ShipCountry, 
oh.ShipPhone,
oh.InternationalAddress, 
CASE LEN(ISNULL(CONVERT(VARCHAR, il.POP_CostPerUnit), '''')) WHEN 0 THEN il.CostPerUnit ELSE il.POP_CostPerUnit END AS CostPerUnit,
il.TotalCost, 
al.Void, 
al.VoidDate,
al.VoidedBy,
SENDTO.Company AS SendCompany,
PIT.pit_AddlPOInfo 
FROM ItemLine il 
INNER JOIN AwardLine al ON il.AwardLineID = al.AwardLineID
INNER JOIN OrderHeader oh ON al.OrderID = oh.OrderID
INNER JOIN dbo.vwVendors AS COMP ON convert(varchar(6),VendorID) = convert(varchar(6),COMP.vendno)
LEFT JOIN SQL2KPROD.Catalog.dbo.VendorSendTo ON convert(varchar(6),COMP.vendno) = convert(varchar(6),vst_vendno) 
LEFT JOIN dbo.vwVendors AS SENDTO ON convert(varchar(6),vst_sendto) = convert(varchar(6),SENDTO.vendno) 
LEFT JOIN SQL2KPROD.Catalog.dbo.Programs Prog ON oh.ProjectNumber  = Prog.p_DKNo
LEFT JOIN SQL2KPROD.Catalog.dbo.ProgramItemTypes PIT ON il.ItemType = PIT.pit_ItemType AND Prog.p_ID = PIT.pit_ProgramID ' 
+ @WHERE + 'ORDER BY POLine')

