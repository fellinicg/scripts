USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20061121
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upGetTokenInfo
	@Token varchar(36),
	@ProgramID int OUTPUT, 
	@UserId int OUTPUT,
	@ClientID int OUTPUT, 
	@AccountID bigint OUTPUT,
	@DKNumber int OUTPUT

AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON
	
	-- Retrieve information associated with token.
	--
	SELECT @ProgramID = PID, @UserId = UID, @ClientID = ClientID, @AccountID = AccountID, @DKNumber = DKNumber
	FROM dbo.StoreFrontTokens
  WHERE TokenID = @Token
	AND CreatedOn > DATEADD(mi, -5, CURRENT_TIMESTAMP)

	-- Delete token.
	--
	DELETE FROM dbo.StoreFrontTokens
  WHERE TokenID = @Token

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upGetTokenInfo
TO system