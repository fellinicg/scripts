USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20070215
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upGetPOsForPrint 
	@PONumber int,
	@Void bit = 0
AS
BEGIN

	SET NOCOUNT ON

	SELECT 
		il.POLine,
		oh.ProjectNumber,
		CASE CHARINDEX('Full Amount', il.ItemOptions)
		 WHEN 0 THEN al.Quantity
		 ELSE 1
		END AS 'Quantity',
		LEFT(CASE CHARINDEX('Full Amount', il.ItemOptions)
					 WHEN 0 THEN il.ItemTitle
					 ELSE SUBSTRING(il.ItemTitle, 1, CHARINDEX('$', il.ItemTitle)) + CONVERT(VARCHAR, CONVERT(INT, il.CostPerUnit * al.Quantity)) +
								SUBSTRING(il.ItemTitle, CHARINDEX(' ', il.ItemTitle, CHARINDEX('$', il.ItemTitle)), 1000)
				 END, 60) AS 'Description',
		(CASE il.ItemModel
		 WHEN '' THEN ''
		 ELSE CASE CHARINDEX('Full Amount', il.ItemOptions)
					 WHEN 0 THEN 'MODEL: ' + il.ItemModel
					 ELSE 'MODEL: $' + CONVERT(VARCHAR, CONVERT(INT, il.CostPerUnit * al.Quantity)) +
								SUBSTRING(il.ItemModel, CHARINDEX(' ', il.ItemModel, CHARINDEX('$', il.ItemModel)), 1000)
					END  + CHAR(13) + CHAR(10)
		END) + 
		(CASE il.ItemOptions
			 WHEN '' THEN ''
			 ELSE il.ItemOptions
		 END) AS 'LineMemo',
		oh.ShipName + CHAR(13) + CHAR(10) + oh.ShipAddress1 + CHAR(13) + CHAR(10) +
		(CASE ISNULL(oh.ShipAddress2, '')
		 WHEN '' THEN ''
		 ELSE oh.ShipAddress2 + CHAR(13) + CHAR(10)
		END) +
		oh.ShipCity + ' ' + oh.ShipState + '  ' + oh.ShipPostal + CHAR(13) + CHAR(10) + ISNULL(oh.ShipCountry, '')  +  
		ISNULL(oh.InternationalAddress, '') +  'PHONE: ' + ISNULL(oh.ShipPhone, '') + CASE ISNULL(PIT.pit_AddlPOInfo ,'') WHEN '' THEN '' ELSE CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10) + PIT.pit_AddlPOInfo END
		AS 'Address',
		CONVERT(DECIMAL(20,2), 
		 CASE CHARINDEX('Full Amount', il.ItemOptions)
			WHEN 0 THEN il.CostPerUnit
			ELSE il.CostPerUnit * al.Quantity
		 END) AS 'Amount'
	FROM FulFilment.dbo.AwardLine AL
	INNER JOIN FulFilment.dbo.ItemLine IL ON al.AwardLineID = il.AwardLineID
	INNER JOIN FulFilment.dbo.OrderHeader OH ON al.OrderID = oh.OrderID
	LEFT JOIN OPENQUERY(SQL2KPROD, 'SELECT p_ID, p_Code FROM Catalog.dbo.Programs') P ON oh.ProgramCode = P.p_Code
	LEFT JOIN OPENQUERY(SQL2KPROD, 'SELECT pit_programID, pit_ItemType, pit_AddlPOInfo FROM Catalog.dbo.ProgramItemTypes') PIT ON P.p_ID = PIT.pit_programID AND il.ItemType = PIT.pit_ItemType
	WHERE il.PONumber = @PONumber
	AND al.Void = @Void
	ORDER BY il.PONumber, il.POLine

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upGetPOsForPrint
TO system