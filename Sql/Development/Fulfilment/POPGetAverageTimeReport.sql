USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.POPGetAverageTimeReport 
(
@TheType int,
@STARTDATE smalldatetime,
@ENDDATE smalldatetime
)
AS

SET @ENDDATE =  DATEADD(day,1,@ENDDATE)

IF @TheType = 0 --all vendors
BEGIN
	SELECT 
	VendorID AS [ID],
	company AS [Vendor],
	AVG(DATEDIFF(dd, ProcessDate, ShippingStatus)) + 1 AS [Process to Ship in Days],
	dbo.fn_POP_AvgVendorDownload(VendorID, @STARTDATE, @ENDDATE) as [Process to Download in Days]
	FROM dbo.ItemLine il
	INNER JOIN dbo.AwardLine al ON il.AwardLineID = al.AwardLineID 
	AND ISDATE(ShippingStatus) = 1 
	AND ProcessDate BETWEEN @STARTDATE AND @ENDDATE
	INNER JOIN dbo.vwVendors ON VendorID = vendno
	GROUP BY VendorID, company
	ORDER BY [Vendor], [Process to Ship in Days]
END
IF @TheType = 1 --all DKs
BEGIN
	SELECT 
	oh.ProjectNumber [DK Number],
	AVG(DATEDIFF(dd, al.ProcessDate, il.ShippingStatus)) AS [Process to Ship in Days],
	dbo.fn_POP_AvgDKDownload(oh.ProjectNumber, @STARTDATE, @ENDDATE) as [Process to Download in Days]
	FROM dbo.ItemLine il
	INNER JOIN dbo.AwardLine al ON il.AwardLineID = al.AwardLineID
	AND ISDATE(ShippingStatus) = 1 
	AND ProcessDate BETWEEN @STARTDATE AND @ENDDATE
	INNER JOIN dbo.OrderHeader oh on al.OrderID = oh.OrderID
	GROUP BY oh.ProjectNumber
	ORDER BY oh.ProjectNumber
END
