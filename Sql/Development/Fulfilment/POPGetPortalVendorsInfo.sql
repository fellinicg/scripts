USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.POPGetPortalVendorsInfo

AS

SELECT 
	AltAccountCode as [Vend ID], 
	AccountName AS [Portal Name], 
	company AS [SBT Name], 
	u_UserName AS [User Name], 
	master.dbo.ud_CCDecrypt(u_Password) AS [Password],
	CASE WHEN u_RecState = 0 THEN 'Active' ELSE 'Not Active' END AS [Status]
FROM SQL2KPROD.MadisonPortal.dbo.Users
INNER JOIN SQL2KPROD.GlobalUsers.dbo.tbUserAccount ON u_ID = UserAccountID
INNER JOIN dbo.vwVendors ON AltAccountCode = vendno
WHERE u_Type = 'V'
ORDER BY [Status], [SBT Name]
