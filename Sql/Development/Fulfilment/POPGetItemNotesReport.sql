USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE dbo.POPGetItemNotesReport
(
@STARTDate smalldatetime = '01/01/2005',
@ENDDate smalldatetime = '12/31/2030'
)
 
AS

SET @ENDDate = DATEADD(day,1,@ENDDate)
SELECT 
	'Item' as [Cause],
	LTRIM(RTRIM(vendno)) + ' ' +  LTRIM(RTRIM(company)) AS [Vendor],
	pin_mc_ID AS [Item ID], 
	mc_SDesc AS [Desc],
	pin_Type AS [Issue], 
	pin_Notes AS [Notes] , 
	IL.PONumber AS [PO #],
	IL.POLine AS [Line #],
	OH.ShipName AS [Recipient]
FROM dbo.POPItemNotes
INNER JOIN SQL2KPROD.Catalog.dbo.MasterCatalog ON pin_mc_ID = mc_ID
INNER JOIN SQL2KPROD.MadisonPortal.dbo.Users ON pin_gu_ID = u_ID
INNER JOIN dbo.ItemLine IL ON pin_mc_ID = ItemID AND (IL.TrackingNumber IS NULL OR IL.TrackingNumber = '')
INNER JOIN dbo.AwardLine AL ON IL.AwardLineID = AL.AwardLineID AND AL.ProcessDate BETWEEN @STARTDate AND @ENDDate
INNER JOIN dbo.OrderHeader OH ON AL.OrderID = OH.OrderID
LEFT JOIN dbo.vwVendors ON u_AltID = vendno 
UNION
SELECT
	'Line' as [Cause], 
	LTRIM(RTRIM(vendno)) + ' ' +  LTRIM(RTRIM(company)) AS [Vendor],
	mc_ID AS [Item ID], 
	mc_SDesc AS [Desc],
	'Line Item Specific' AS [Issue], 
	IL.POP_Notes AS [Notes] , 
	IL.PONumber AS [PO #],
	IL.POLine AS [Line #],
	OH.ShipName AS [Recipient]
FROM dbo.ItemLine AS IL
INNER JOIN SQL2KPROD.Catalog.dbo.MasterCatalog ON ItemID = mc_ID
INNER JOIN dbo.AwardLine AL ON IL.AwardLineID = AL.AwardLineID AND AL.ProcessDate BETWEEN @STARTDate AND @ENDDate
INNER JOIN dbo.OrderHeader OH ON AL.OrderID = OH.OrderID 
LEFT JOIN dbo.vwVendors ON VendorID = vendno 
WHERE POP_Notes IS NOT NULL AND POP_Notes <> ''

ORDER BY [Cause], [Vendor], [Issue], [PO #], [Line #]