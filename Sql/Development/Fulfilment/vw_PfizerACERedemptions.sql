USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW dbo.vw_PfizerACERedemptions
AS
SELECT oh.*, al.AwardID, al.AwardTitle, al.Quantity, al.UnitPoints, 
    al.TotalPoints, al.Void, al.VoidDate, SUBSTRING(il.ItemType, 1, 
    1) AS ItemType, il.ItemModel,il.CostPerUnit, il.ShippingPerUnit, 
    il.TaxPerUnit, il.TotalCost, il.TotalShipping, il.TotalTax, 
    il.ShippingFlag, il.ItemID,  il.VendorID, IsNull(v.company,'Invalid Vendor! ID:'+cast(il.VendorID as varchar(10))) 'company'
FROM OrderHeader oh INNER JOIN
    AwardLine al ON oh.OrderID = al.OrderID INNER JOIN
    ItemLine il ON al.AwardLineID = il.AwardLineID 
	LEFT JOIN dbo.vwVendors AS v ON (il.VendorID = v.vendno)
