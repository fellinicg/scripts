USE [Tap]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Jason McElroy
-- OrigCreateDate: 20060821
-- LastEditedByFullName: Jason McElroy
-- LastEditDate: 20060919
-- ============================================================
ALTER VIEW dbo.vwGlobalEmployee
AS

	SELECT
		e.EmployeeID,
		e.CustEmployeeCode,
		g.PersonFirstGivenName AS FirstName,
		g.PersonSecondGivenName as MiddleName,
		g.PersonFamilyName AS LastName,
		g.PersonFirstGivenName + ' ' + g.PersonFamilyName AS BuiltFullNameFirstLast,
		g.PersonFamilyName + ', ' + g.PersonFirstGivenName AS BuiltFullNameLastFirst,
		g.PersonFamilyName + ', ' + g.PersonFirstGivenName + CASE WHEN ISNULL(g.PersonSecondGivenName, '') = '' THEN '' ELSE ' ' + LEFT(g.PersonSecondGivenName, 1) END AS DebitCardName,
		e.AddressLine1,
		e.AddressLine2,
		e.City,
		e.State,
		e.PostalCode,
		e.EmailAddress,
		e.Phone,
		e.Status,
		e.StatusDateTime,
		e.BlockedDateTime,
		e.CreateDateTime,
		e.UpdateDateTime,
		CASE g.AccountStatus WHEN 'E' THEN 0 WHEN 'D' THEN 1 WHEN 'X' THEN 2 ELSE 1 END AS gu_rs_ID
	FROM tbEmployee e
		INNER JOIN sql2kprod.GlobalUsers.dbo.tbUserAccount g ON (e.EmployeeID = g.UserAccountID)
	WHERE g.ClientID = 25

	-- ===============================================================================
	-- Jason McElroy - It would be better to obtain ClientID dynamically
	--                 using dbo.ufSettingValue('TAPRewardCentralClientID').
	--                 However, it is poor performer and is unlikely to ever change.
	-- ===============================================================================

