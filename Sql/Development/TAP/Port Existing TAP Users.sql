USE GlobalUsers
GO
SET NOCOUNT ON

-- ACCOUNTS
ALTER TABLE tbUserAccount DISABLE TRIGGER tiUserAccount
GO
SET IDENTITY_INSERT tbUserAccount ON
GO
	-- NON SYSTEM ACCOUNTS
INSERT tbUserAccount
(
	UserAccountID,
	AccountType,
	IsPerson,
	ClientID,
	CustAccountCode,
	AltAccountCode,
	AccountName,
	PersonNamePrefix,
	PersonFirstGivenName,
	PersonSecondGivenName,
	PersonFamilyName,
	PersonNameSuffix,
	PersonNaturalFullName,
	PersonBuiltFullName,
	PersonGovtIDType,
	PersonGovtIDCodeOneWayCrypted,
	PersonGovtIDCodeTwoWayCrypted,
	AccountStatus,
	StatusDateTime,
	CreateDateTime,
	UpdateDateTime
)
SELECT
	gu_ID AS UserAccountID,
	CASE WHEN gu_cl_ID = 5 THEN 'M' WHEN gu_FirstName = 'Vendor' THEN 'V' ELSE 'C' END AS AccountType,
	CASE WHEN gu_FirstName = 'Vendor' THEN 0 ELSE 1 END AS IsPerson,
	NULLIF(gu_cl_ID, 0) AS ClientID,
	CASE WHEN gu_cl_ID = 5 THEN NULL WHEN gu_FirstName = 'Vendor' THEN NULL ELSE gu_CustUserCode END AS CustAccountCode,
	CASE WHEN gu_cl_ID = 5 THEN NULL WHEN gu_FirstName = 'Vendor' THEN NULLIF(gu_OldID, 0) ELSE NULL END AS AltAccountCode,
	CASE WHEN gu_FirstName = 'Vendor' THEN NULLIF(gu_LastName, '') ELSE NULL END AS AccountName,
	NULL AS NamePrefix,
	CASE WHEN gu_FirstName <> 'Vendor' THEN NULLIF(gu_FirstName, '') ELSE NULL END AS FirstGivenName,
	CASE WHEN gu_FirstName <> 'Vendor' THEN NULLIF(gu_MiddleName, '') ELSE NULL END AS SecondGivenName,
	CASE WHEN gu_FirstName <> 'Vendor' THEN NULLIF(gu_LastName, '') ELSE NULL END AS FamilyName,
	NULL AS NameSuffix,
	NULL AS PersonNaturalFullName,
	CASE WHEN gu_FirstName <> 'Vendor' THEN NULLIF(gu_FirstName, '') + ' ' + ISNULL(gu_MiddleName + ' ', '') + NULLIF(gu_LastName, '') ELSE NULL END AS PersonBuiltFullName,
	CASE WHEN ISNULL(gu_SSN, '') <> '' THEN 'S' ELSE NULL END AS PersonGovtIDType,
	NULL AS PersonGovtIDCodeOneWayCrypted,
	CASE WHEN ISNULL(gu_SSN, '') <> '' THEN master.dbo.ud_CCEncrypt(gu_SSN) ELSE NULL END AS PersonGovtIDCodeTwoWayCrypted,
	CASE gu_rs_ID WHEN 0 THEN 'E' WHEN 1 THEN 'D' ELSE 'X' END AS AccountStatus,
	CURRENT_TIMESTAMP AS StatusDateTime,
	gu_PostDate AS CreateDateTime,
	CURRENT_TIMESTAMP AS UpdateDateTime
FROM GlobalUser.dbo.GlobalUsers  -- SQL2KPROD
WHERE gu_ID <> 1  -- SPECIAL RECORD TO BE SETUP MANUALLY
AND gu_cl_ID IN (25)
GO

SET IDENTITY_INSERT tbUserAccount OFF
GO
ALTER TABLE tbUserAccount ENABLE TRIGGER tiUserAccount
GO

-- PHONE NUMBERS
ALTER TABLE tbPhoneNum DISABLE TRIGGER tiPhoneNum
GO
SET IDENTITY_INSERT tbPhoneNum ON
GO

INSERT tbPhoneNum
(
	PhoneNumID,
	UserAccountID,
	PhoneNumType,
	IDDCode,
	CityAreaCode,
	PhoneNum,
	Extension,
	OrigPhoneNum,
	TxtMsgCapable,
	IsPreferred,
	CreateDateTime,
	UpdateDateTime
)
SELECT
	cn_ID AS PhoneNumID,
	cn_gu_ID AS UserAccountID,
	CASE WHEN cn_ct_ID = 3 THEN 'W' WHEN cn_ct_ID = 4 THEN 'H' WHEN cn_ct_ID = 8 THEN 'M' ELSE 'U' END AS PhoneNumType,
	NULL AS IDDCode,
	LEFT(cn_Number, 3) AS CityAreaCode,
	RIGHT(cn_Number, 7) AS PhoneNum,
	cn_Extension AS Extension,
	NULL AS OrigPhoneNum,
	0 AS TxtMsgCapable,
	CASE WHEN cn_Default = 1 THEN 1 ELSE 0 END AS IsPreferred,
	cn_PostDate AS CreateDateTime,
	CURRENT_TIMESTAMP AS UpdateDateTime
FROM GlobalUser.dbo.ContactNumbers  -- SQL2KPROD
INNER JOIN GlobalUser.dbo.GlobalUsers ON cn_gu_ID = gu_ID
WHERE gu_ID <> 1  -- SPECIAL RECORD TO BE SETUP MANUALLY
AND gu_cl_ID IN (25)
GO

SET IDENTITY_INSERT tbPhoneNum OFF
GO
ALTER TABLE tbPhoneNum ENABLE TRIGGER tiPhoneNum
GO

	-- CHECK RESULTS (ONLY ONE PREFERRED NUMBER)
IF (EXISTS (SELECT UserAccountID FROM tbPhoneNum WHERE IsPreferred = 1 GROUP BY UserAccountID HAVING COUNT(*) > 1))
BEGIN
	PRINT 'DUPLICATE PREFERRED PHONE NUMBERS'
	RETURN
END
GO

-- EMAIL ADDRESSES
ALTER TABLE tbEmailAddress DISABLE TRIGGER tiEmailAddress
GO
SET IDENTITY_INSERT tbEmailAddress ON
GO

INSERT tbEmailAddress
(
	EmailAddressID,
	UserAccountID,
	EmailAddressType,
	EmailAddress,
	IsPreferred,
	CreateDateTime,
	UpdateDateTime
)
SELECT
	em_ID AS EmailAddressID,
	em_gu_ID AS UserAccountID,
	CASE em_ct_ID WHEN 1 THEN 'W' WHEN 2 THEN 'H' ELSE 'U' END AS EmailAddressType,
	NULLIF(em_Address, '') AS EmailAddress,
	CASE em_Default WHEN 1 THEN 1 ELSE 0 END AS IsPreferred,
	em_PostDate AS CreateDateTime,
	CURRENT_TIMESTAMP AS UpdateDateTime
FROM GlobalUser.dbo.Email  -- SQL2KPROD
INNER JOIN GlobalUser.dbo.GlobalUsers ON em_gu_ID = gu_ID
WHERE gu_cl_ID IN (25)
GO

SET IDENTITY_INSERT tbEmailAddress OFF
GO
ALTER TABLE tbEmailAddress ENABLE TRIGGER tiEmailAddress
GO

	-- CHECK RESULTS (ONLY ONE PREFERRED EMAIL ADDRESS)
IF (EXISTS (SELECT UserAccountID FROM tbEmailAddress WHERE IsPreferred = 1 GROUP BY UserAccountID HAVING COUNT(*) > 1))
BEGIN
	PRINT 'DUPLICATE PREFERRED EMAIL ADDRESSES'
	RETURN
END
GO

-- EMERGENCY CONTACTS
SET IDENTITY_INSERT tbEmergencyContact ON
GO

INSERT tbEmergencyContact
(
	EmergencyContactID,
	UserAccountID,
	FullName,
	RelationshipDesc,
	IDDCode,
	CityAreaCode,
	PhoneNum,
	Extension,
	CreateDateTime,
	UpdateDateTime
)
SELECT
	emc_ID AS EmergencyContactID,
	emc_gu_ID AS UserAccountID,
	NULLIF(emc_Name, '') AS FullName,
	emc_Relationship AS RelationshipDesc,
	CASE WHEN LEN(emc_Phone) = 13 THEN LEFT(emc_Phone, 3) ELSE NULL END AS IDDCode,
	CASE WHEN LEN(emc_Phone) = 13 THEN SUBSTRING(emc_Phone, 4, 3) ELSE LEFT(emc_Phone, 3) END AS CityAreaCode,
	RIGHT(emc_Phone, 7) AS PhoneNum,
	NULLIF(emc_Extension, '') AS Extension,
	CURRENT_TIMESTAMP AS CreateDateTime,
	CURRENT_TIMESTAMP AS UpdateDateTime
FROM GlobalUser.dbo.EmergencyContacts  -- SQL2KPROD
INNER JOIN GlobalUser.dbo.GlobalUsers ON emc_gu_ID = gu_ID
WHERE gu_cl_ID IN (25)
GO

SET IDENTITY_INSERT tbEmergencyContact OFF
GO

-- MAIL ADDRESSES
ALTER TABLE tbMailAddress DISABLE TRIGGER tiMailAddress
GO
SET IDENTITY_INSERT tbMailAddress ON
GO

INSERT tbMailAddress
(
	MailAddressID,
	UserAccountID,
	MailAddressType,
	Line1,
	Line2,
	Line3,
	Line4,
	CityTown,
	StateProvince,
	PostalCode,
	Country,
	UseCityLine,
	IsPreferred,
	CreateDateTime,
	UpdateDateTime
)
SELECT 
	ad_ID AS MailAddressID,
	ad_gu_ID AS UserAccountID,
	CASE ad_ct_ID
		WHEN 5 THEN 'H'
		WHEN 7 THEN 'W'
		ELSE 'O'
	END AS MailAddressType,
	ad_StreetAddr1 AS Line1,
	ad_StreetAddr2 AS Line2,
	ad_StreetAddr3 AS Line3,
	ad_StreetAddr4 AS Line4,
	ad_City AS CityTown,
	ad_State AS StateProvince,
	ad_PostalCode AS PostalCode,
	CASE ad_Country
		WHEN 'US' THEN 'USA'
		WHEN '' THEN NULL
	END AS Country,
	0 AS UseCityLine,
	ad_Default AS IsPreferred,
	ad_PostDate AS CreateDateTime,
	CURRENT_TIMESTAMP AS UpdateDateTime
FROM GlobalUser.dbo.Addresses
INNER JOIN GlobalUser.dbo.GlobalUsers ON ad_gu_ID = gu_ID
WHERE gu_cl_ID IN (25)
GO

SET IDENTITY_INSERT tbMailAddress OFF
GO
ALTER TABLE tbMailAddress ENABLE TRIGGER tiMailAddress
GO

	-- CHECK RESULTS (ONLY ONE PREFERRED MAIL ADDRESS)
IF (EXISTS (SELECT UserAccountID FROM tbMailAddress WHERE IsPreferred = 1 GROUP BY UserAccountID HAVING COUNT(*) > 1))
BEGIN
	PRINT 'DUPLICATE PREFERRED MAIL ADDRESSES'
	RETURN
END


/*
ISSUES
--------------------------
1) All non-MPG employees require a customer identifier (OldID) value. Many records for people who do not appear to be employees do not have this.
2) There are records for clients that do not currently have systems that use global users in the current table. They are for Pfizer, Abbott, and Citigroup.
		Do these users have corresponding records in client systems that need to be matched? What systems create these records? Can they populate the OldID values?

*/