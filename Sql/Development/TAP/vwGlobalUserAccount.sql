USE [Tap]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Mark Yachnin
-- OrigCreateDate: 20061005
-- LastEditedByFullName: Mark Yachnin
-- LastEditDate: 20061005
-- ============================================================
ALTER VIEW dbo.vwGlobalUserAccount
AS
	--NOTE: THIS IS PRIMARILY USED FOR 'ADMIN' USERS WHO ARE NOT IN tbEmployee.

	SELECT
		u.UserAccountID,
		g.CustAccountCode AS CustEmployeeCode,
		g.PersonFirstGivenName AS FirstName,
		g.PersonSecondGivenName as MiddleName,
		g.PersonFamilyName AS LastName,
		g.PersonFirstGivenName + ' ' + g.PersonFamilyName AS BuiltFullNameFirstLast,
		g.PersonFamilyName + ', ' + g.PersonFirstGivenName AS BuiltFullNameLastFirst,
		g.PersonFamilyName + ', ' + g.PersonFirstGivenName + CASE WHEN ISNULL(g.PersonSecondGivenName, '') = '' THEN '' ELSE ' ' + LEFT(g.PersonSecondGivenName, 1) END AS DebitCardName,
		u.Status,
		u.StatusDateTime,
		u.IsAdmin,
		u.LockedDateTime,
		u.CreateDateTime,
		u.UpdateDateTime,
		CASE g.AccountStatus WHEN 'E' THEN 0 WHEN 'D' THEN 1 WHEN 'X' THEN 2 ELSE 1 END AS gu_rs_ID
	FROM tbuserAccount u
		INNER JOIN sql2kprod.GlobalUsers.dbo.tbUserAccount g ON (u.UserAccountID = g.UserAccountID)
	WHERE g.ClientID = 25

	-- ===============================================================================
	-- Jason McElroy - It would be better to obtain ClientID dynamically
	--                 using dbo.ufSettingValue('TAPRewardCentralClientID').
	--                 However, it is poor performer and is unlikely to ever change.
	-- ===============================================================================
