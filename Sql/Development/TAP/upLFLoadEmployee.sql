USE [Tap]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Mohammad Masud Rahman
-- OrigCreateDate: 20060823
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20061030
-- ============================================================
ALTER PROCEDURE dbo.upLFLoadEmployee
		@LoadFileID int,
 		@LoadFileStatus char(1) OUTPUT,
		@Verbose bit = 0
AS

	SET NOCOUNT ON

	-- CONSTANTS
	DECLARE @LFStatError char(1)
	DECLARE @LFStatLoaded char(1)
	DECLARE @LFProcessControl char(1)
	DECLARE @LFTypeEmployee char(1)
	DECLARE @NotAdmin bit

	SET @LFStatError = 'E'
	SET @LFStatLoaded = 'L'
	SET @LFProcessControl = 'O'
	SET @LFTypeEmployee = 'E'
	SET @NotAdmin = 0

	DECLARE @StatusComment varchar(800)

	-- VARS
	DECLARE @ClientID int

	--GET @ClientID
	SET @ClientID = dbo.ufSettingValue('TAPRewardCentralClientID')

	IF (ISNULL(@ClientID, 0) = 0)
	BEGIN
		RAISERROR('upLFLoadEmployee - failed to obtain value from tbSettings for TAPClientID', 16, 1)
		RETURN
	END

	-- VALID CONDITIONS
	IF (NOT EXISTS (SELECT 1 FROM tbLoadFile WHERE LoadFileID = @LoadFileID AND LoadFileType = @LFTypeEmployee AND Status = @LFProcessControl))
	BEGIN
		RAISERROR('upLFLoadEmployee - @LoadFileID supplied does not exist, is not an Employee Load file, or has status other than (O)taged', 16, 1)
		RETURN
	END

	IF (NOT EXISTS (SELECT 1 FROM tbLFStagingEmployee) OR EXISTS (SELECT 1 FROM tbLFStagingEmployee WHERE LoadFileID <> @LoadFileID))
	BEGIN
		RAISERROR('upLFLoadEmployee - tbLFStagingEmployee is empty or contains records for another LoadFileID', 16, 1)
		RETURN
	END

	--TABLES TO LOAD DATA 
	DECLARE @CreateEmployees table (SALES_REP_ID int NOT NULL, UserAccountID int NULL)
	DECLARE @UpdateEmployees table (EmployeeID int NOT NULL, FirstName varchar(50), MiddleName varchar(1), LastName varchar(50), CustEmployeeCode varchar(20))

	-- STATISTICS
	DECLARE
		@CreateEmployeeCNT int,
		@UpdateEmployeeCNT int,
		@FallOffEmployeesCNT int,
		@UserAccountDisabledCNT int,
		@CBDProgramUserCreatCNT int,
		@ImpactProgramUserCreatCNT int,
		@FileRecCNT int,
		@LogInUpdateCNT int,
		@UserAccStatusUpdateCNT int,
		@EmployeeStatusUpdateCNT int,
		@PositionAssignUpdateCNT int,
		@actualUpdateRecord int

	-- PROGRESS MARKERS
	DECLARE
		@CreateGlobalUsers bit,
		@UpdateGlobalUsers bit

	-- OTHER
	DECLARE
		@IsFailure bit,
		@ROWCOUNT int,
		@ProgramIDCBD int,
		@ProgramIDImpact int,
		@ERROR int,
		@AccountType char(1),
		@IsPerson bit,
		@AccountStatus char(1),
		@Result char(1)

	-- INITIALIZE/SET CONSTANTS
	SET @LoadFileStatus = @LFStatError
	SET @IsFailure = 0
	SET @CreateGlobalUsers = 0
	SET @UpdateGlobalUsers = 0
	SET @AccountType = 'C'
	SET @IsPerson = 1
	SET @AccountStatus = 'E'
	

	-- GET STATISTICS
	SELECT @CreateEmployeeCNT = COUNT(*) FROM tbLFStagingEmployee WHERE CreateUserAccount = 1
	SELECT @UpdateEmployeeCNT = COUNT(*) FROM tbLFStagingEmployee WHERE UpdateRecord = 1 AND IsCurrentRecord = 1

	IF (@Verbose = 1)
	BEGIN
		PRINT 'Create Employee Count' 
		PRINT @CreateEmployeeCNT 
		PRINT 'Update Employee Count'
		PRINT @UpdateEmployeeCNT
	END
	
	--POPULATE @CreateEmployees TABLE
	IF (@CreateEmployeeCNT <> 0)
	BEGIN
		INSERT @CreateEmployees (SALES_REP_ID)
		SELECT SALES_REP_ID from tbLFStagingEmployee WHERE CreateUserAccount = 1
	END

	IF (@Verbose = 1)
	BEGIN
		PRINT 'Table Variable populated for Create employee'
		SELECT @ROWCOUNT = COUNT(*) FROM @CreateEmployees
		PRINT @ROWCOUNT
	END

	--POPULATE @UpdateEmployees TABLE
	IF (@UpdateEmployeeCNT <> 0)
	BEGIN
		INSERT @UpdateEmployees (EmployeeID, FirstName, MiddleName, LastName, CustEmployeeCode)
		SELECT EmployeeID, SALES_REP_FNAME,  SALES_REP_MNAME, SALES_REP_LNAME, SALES_REP_ID 
		FROM tbLFStagingEmployee 
		WHERE UpdateRecord = 1
	END	

	IF (@Verbose = 1)
	BEGIN
		PRINT 'Table Variable populated for update employee'
		SELECT @ROWCOUNT = COUNT(*) FROM @UpdateEmployees
		PRINT @ROWCOUNT
	END


	-->> PERFORM GLOBAL USER OPERATIONS FIRST OUTSIDE TRANSACTION BLOCKING <<--

	DECLARE
		@CurrSALES_REP_ID varchar(20),
		@CurrEmployeeID int,
		@UserAccountID int,
		@FirstName varchar(30),
		@MiddleName varchar(1),
		@LastName varchar(30),
		@CustEmployeeCode int,
		@Loop int

	-- CREATE  NEW GLOBAL USERS
	IF (@CreateEmployeeCNT <> 0)
	BEGIN
		SET @CreateGlobalUsers = 1
		SET @Loop = 0
		SELECT @CurrSALES_REP_ID = MIN(SALES_REP_ID) FROM @CreateEmployees

		IF (@Verbose = 1)
		BEGIN
			PRINT 'CREATE: GLOBAL USERS INSERT START'

		END

		WHILE (@Loop < @CreateEmployeeCNT)
		BEGIN
		-- GET INFO
			SELECT @FirstName = SALES_REP_FNAME, @MiddleName = SALES_REP_MNAME, @LastName = SALES_REP_LNAME
			FROM tbLFStagingEmployee WHERE SALES_REP_ID = @CurrSALES_REP_ID AND CreateUserAccount = 1
			
			-- ALTER  GLOBAL USER
			SET @UserAccountID = NULL
--			SET @IsSuccess = NULL

			EXECUTE SQL2KPROD.GlobalUsers.dbo.upAddUserAccount
				 @AccountType = @AccountType
				,@IsPerson = @IsPerson
				,@ClientID = @ClientID
				,@CustAccountCode = @CurrSALES_REP_ID
				,@PersonFirstGivenName = @FirstName
				,@PersonSecondGivenName = @MiddleName
				,@PersonFamilyName = @LastName
				,@AccountStatus = @AccountStatus
				,@UserAccountID = @UserAccountID OUTPUT

--				EXEC SQL2KPROD.GlobalUser.dbo.upInsertUpdateGlobalUser
--				@UserAccountID = @UserAccountID OUTPUT,
--				@LastName = @LastName,
--				@FirstName = @FirstName,
--				@MiddleName = @MiddleName,
--				@ClientID = @ClientID,
--				@CustUserCode = @CurrSALES_REP_ID,
--				@IsSuccess = @IsSuccess OUTPUT

			-- ERROR HANDLER
--			IF (ISNULL(@UserAccountID, 0) = 0 OR @IsSuccess = 0)
			IF (ISNULL(@UserAccountID, 0) = 0)
			BEGIN
				PRINT 'CALL FAILURE: SQL2KPROD.GlobalUsers.dbo.upAddUserAccount'
				SET @IsFailure = 1
				SET @StatusComment = 'upLFLoadEmployee - failed to create a record in GlobalUser'
				UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
				RETURN
			END
			ELSE
			BEGIN
				UPDATE @CreateEmployees SET UserAccountID = @UserAccountID WHERE SALES_REP_ID = @CurrSALES_REP_ID
				
				--GET NEXT RECORD FROM @CreateEmployees
				SELECT @CurrSALES_REP_ID = MIN(SALES_REP_ID) FROM @CreateEmployees WHERE SALES_REP_ID > @CurrSALES_REP_ID
				SET @Loop = @Loop + 1
			END
		END
	END

		IF (@Verbose = 1)
		BEGIN
			PRINT 'CREATE: GLOBAL USERS INSERT END. LOOP COUNT'
			PRINT @Loop
		END

	-- TEST OUTPUT
	IF (@Verbose = 1)
  	BEGIN
  		IF (@CreateEmployeeCNT <> 0)
  		BEGIN
			Print 'CreateEmployee information'
			select SALES_REP_ID as SALES_REP_ID, UserAccountID as UserAccountID from @CreateEmployees
    		END
	END

		IF (@Loop <> @CreateEmployeeCNT)
		BEGIN
			PRINT 'FAILURE: INSERT GLOBALUSER COUNT MISMATCH'
			SET @IsFailure = 1
			SET @StatusComment = 'upLFLoadEmployee - failed to insert a record in GlobalUsers'
			UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
			RETURN
		END


	-- UPDATE GLOBAL USERS
	IF (@UpdateEmployeeCNT <> 0)
	BEGIN

		IF (@Verbose = 1)
	  	BEGIN
			PRINT 'UPDATE GLOBAL USERS START'
		END

		SET @Result = 'F'
		SET @UpdateGlobalUsers = 1
		SET @Loop = 0
		SELECT @CurrEmployeeID = MIN(EmployeeID) FROM @UpdateEmployees

		WHILE (@Loop < @UpdateEmployeeCNT)
		BEGIN
			-- GET INFO
			SELECT @FirstName = FirstName, @MiddleName = MiddleName, @LastName = LastName, @CustEmployeeCode = CustEmployeeCode
			FROM @UpdateEmployees WHERE EmployeeID = @CurrEmployeeID

			EXECUTE SQL2KPROD.GlobalUsers.dbo.upUpdateUserAccount
				 @Result = @Result OUTPUT
				,@UserAccountID = @CurrEmployeeID
				,@UpdCustAccountCode = 1
				,@CustAccountCode = @CustEmployeeCode
				,@UpdPersonFirstGivenName = 1
				,@PersonFirstGivenName = @FirstName
				,@UpdPersonFamilyName = 1
				,@PersonFamilyName = @LastName
				,@UpdPersonSecondGivenName = 1
				,@PersonSecondGivenName = @MiddleName

--			EXEC SQL2KPROD.GlobalUser.dbo.upInsertUpdateGlobalUser
--				@UserAccountID = @CurrEmployeeID OUTPUT,
--				@LastName = @LastName,
--				@FirstName = @FirstName,
--				@MiddleName = @MiddleName,
--				@ClientID = @ClientID,
--				@CustUserCode = @CustEmployeeCode,
--				@IsSuccess = @IsSuccess OUTPUT
		
--			IF (@IsSuccess = 0)
			IF (@Result = 'F')
			BEGIN
				PRINT 'CALL FAILURE: upUpdateUserAccount'
				SET @IsFailure = 1
				SET @StatusComment = 'upLFLoadEmployee - failed to update a record in GlobalUser'
				UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
				RETURN
			END
			ELSE
			BEGIN
				SELECT @CurrEmployeeID = MIN(EmployeeID) FROM @UpdateEmployees WHERE EmployeeID > @CurrEmployeeID
				SET @Loop = @Loop + 1
				SET @actualUpdateRecord = @actualUpdateRecord + 1
			END
		END
	
		IF (@Verbose = 1)
		BEGIN
			PRINT 'Actual Update global user count'
			PRINT @actualUpdateRecord
		END

		IF (@actualUpdateRecord <> @UpdateEmployeeCNT)
		BEGIN
			PRINT 'FAILURE: UPDATE GLOBALUSER COUNT MISMATCH'
			SET @IsFailure = 1
			SET @StatusComment = 'upLFLoadEmployee - failed to update a record in GlobalUsers'
			UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
			RETURN
		END
	END

	-->> PERFORM LOCAL DB OPERATIONS PROTECTED BY TRANSACTION BLOCKING <<--

	-- GET CBD & IMPACT PROGRAM ID
	SET @ProgramIDCBD = 8889
	SET @ProgramIDImpact = 8888

	-- ERROR HANDLER
	IF (@ProgramIDCBD = 0 OR @ProgramIDImpact = 0)
	BEGIN
		SET @IsFailure = 1
		SET @StatusComment = 'Failed to find program ID from tbProgram'
		UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
		RAISERROR('upLFLoadEmployee - Failed to find program ID from tbProgram', 16, 1)
		RETURN
	END

	-- OPEN TRAN
	IF (@IsFailure = 0)
	BEGIN
		BEGIN TRAN
	END

	IF (@Verbose = 1)
  		PRINT 'STARTING TRAN'
	
	--INSERT tbUserAccount / tbEmployee / tbProgramUserAccount AND UPDATE tbPosition
	IF (@IsFailure = 0 AND @CreateEmployeeCNT <> 0)
	BEGIN
		
		IF (@Verbose = 1)
	  		PRINT 'STARTING insert tbUserAccount'

		-- CREATE USER ACCTS
		INSERT tbUserAccount (UserAccountID, LoginName, PasswordCrypted, Status, StatusDateTime,
			IsAdmin, CreateDateTime, UpdateDateTime)
		SELECT c.UserAccountID, l.NODE_NAME, master.dbo.ud_CCEncrypt(l.TempPassword), l.NewUserAccountStatus, CURRENT_TIMESTAMP, 
			@NotAdmin, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
		FROM @CreateEmployees c INNER JOIN tbLFStagingEmployee l ON (c.SALES_REP_ID = l.SALES_REP_ID)
		WHERE l.CreateUserAccount = 1
		
		SET @ROWCOUNT = @@rowcount

		IF (@Verbose = 1)
		BEGIN
			PRINT 'insert # record in tbUserAccount'
			PRINT @ROWCOUNT
			SELECT c.UserAccountID, l.NODE_NAME, master.dbo.ud_CCEncrypt(l.TempPassword), l.NewUserAccountStatus
			FROM @CreateEmployees c INNER JOIN tbLFStagingEmployee l ON (c.SALES_REP_ID = l.SALES_REP_ID)
			WHERE l.CreateUserAccount = 1 
		END

		-- ERROR HANDLER
		IF (@ROWCOUNT <> @CreateEmployeeCNT)
 		BEGIN
 			ROLLBACK TRAN
 			PRINT 'FAILURE: CREATE ACCT COUNT MISMATCH'
 			SET @IsFailure = 1
 			SET @StatusComment = 'upLFLoadEmployee - failed to create a record in tbUserAccount'
			UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
 			RETURN
 		END

		-- CREATE EMPLOYEES
		IF (@Verbose = 1)
			PRINT 'STARTING insert tbEmployee'
			
		INSERT tbEmployee (EmployeeID, CustEmployeeCode, AddressLine1, AddressLine2, City, State,
			PostalCode, EmailAddress, Phone, TerminationDate, Status, StatusDateTime, HistoryCutOffDateTime, CreateDateTime, UpdateDateTime)
		SELECT c.UserAccountID, l.SALES_REP_ID, l.SALES_REP_MAIL_ADDRESS, l.SALES_REP_MAIL_ATTENTION, l.SALES_REP_MAIL_CITY, l.SALES_REP_MAIL_STATE_CODE,
			l.SALES_REP_MAIL_ZIP_CODE, MAIL_ADDRESS_EXT, l.SALES_REP_PHONE, l.TerminationDate, l.NewEmployeeStatus, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
		FROM @CreateEmployees c 
		INNER JOIN tbLFStagingEmployee l ON (c.SALES_REP_ID = l.SALES_REP_ID)
		WHERE l.CreateUserAccount = 1
		
		SET @ROWCOUNT = @@rowcount

		IF (@Verbose = 1)
		BEGIN
			PRINT 'insert # record in tbEmployee'
  			PRINT @ROWCOUNT
		END

		-- ERROR HANDLER
		IF (@ROWCOUNT <> @CreateEmployeeCNT)
		BEGIN
			ROLLBACK TRAN
			PRINT 'FAILURE: CREATE EMPLOYEES COUNT MISMATCH'
			SET @IsFailure = 1
			SET @StatusComment = 'upLFLoadEmployee - failed to create a record in tbEmployee'
			UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
			RETURN
		END

		--UPDATE EmployeeID IN tbLFStagingEmployee FOR THE NEW EMPLOYEE
		IF (@Verbose = 1)
		BEGIN
			PRINT 'Start updating EmployeeID in tbLFStagingEmployee for new employee'
		END

		UPDATE tbLFStagingEmployee SET
			EmployeeID = c.UserAccountID
		FROM @CreateEmployees c INNER JOIN tbLFStagingEmployee lf ON (c.SALES_REP_ID = lf.SALES_REP_ID) 
		WHERE lf.CreateUserAccount = 1
		
		SET @ROWCOUNT = @@rowcount

		IF (@Verbose = 1)
		BEGIN
			PRINT 'Updated EmployeeID in tbLFStagingEmployee count'
			PRINT @ROWCOUNT
		END

		--UPDATE tbPosition TO ASSIGN AssignedEmployeeID AND Dates
		IF (@Verbose = 1)
		BEGIN
			PRINT 'Start updating tbPosition for new employee'
		END

		-- CREATE THE SET OF NEW EMPLOYEES WITH CURRENT POSITION ASSIGNMENTS
		DECLARE @AssignPosition table (SALES_REP_ID int NOT NULL, UserAccountID int not null)
		DECLARE @AssignPositionCNT int 
		
		INSERT @AssignPosition
		SELECT SALES_REP_ID, EmployeeID 
		FROM tbLFStagingEmployee 
		WHERE CreateUserAccount = 1 
			and AssignEndDateTime > CURRENT_TIMESTAMP
	
		SELECT @AssignPositionCNT  = COUNT(*) FROM @AssignPosition

		-- UPDATE tbPosition FOR NEW EMPLOYEES
 		UPDATE tbPosition SET
			AssignedEmployeeID = c.UserAccountID,
			AssignedStartDate = l.EFFECTIVE_DATE_FROM,
			AssignedEndDate = l.EFFECTIVE_DATE_TO
		FROM @AssignPosition c
			INNER JOIN tbLFStagingEmployee l ON (c.SALES_REP_ID = l.SALES_REP_ID)
			INNER JOIN tbPosition p ON (p.CustPositionCode = l.SF_TERRITORY_ID)
		WHERE l.CreateUserAccount = 1
		
		SET @ROWCOUNT = @@rowcount

		IF (@Verbose = 1)
		BEGIN
			PRINT 'Updated tbPosition count for new employee'
			PRINT @ROWCOUNT
		END

		IF (@ROWCOUNT <> @AssignPositionCNT)
		BEGIN
			ROLLBACK TRAN
			PRINT 'FAILURE: UPDATE POSITION COUNT MISMATCH'
			SET @IsFailure = 1
			SET @StatusComment = 'upLFLoadEmployee - failed to update a record in tbPosition'
			UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
			RETURN
		END


		--CREATE tbProgramUserAccount(CBDAccess)
		IF (@Verbose = 1)
		BEGIN
			PRINT 'Start creating tbProgramUserAccount(CBDAccess)for new employee'
		END

		INSERT tbProgramUserAccount (ProgramID, UserAccountID, CreateDateTime)
		SELECT @ProgramIDCBD, c.UserAccountID, CURRENT_TIMESTAMP
		FROM @CreateEmployees c 
		INNER JOIN tbLFStagingEmployee l ON (c.SALES_REP_ID = l.SALES_REP_ID)
		WHERE l.CreateUserAccount = 1 AND l.HasCBDAccess = 1
		
		SELECT @CBDProgramUserCreatCNT = @@Rowcount, @ERROR = @@ERROR

		IF (@ERROR <> 0)
		BEGIN
			ROLLBACK TRAN
			PRINT 'FAILURE: INSERT IN tbProgramUserAccount FOR CBD' 
			SET @IsFailure = 1
			SET @StatusComment = 'upLFLoadEmployee - failed to insert CBD record to tbProgramUserAccount'
			UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
			RETURN
		END

		IF (@Verbose = 1)
		BEGIN
			PRINT 'CBDAccess created for new employee'
			PRINT @CBDProgramUserCreatCNT
		END


		--CREATE tbProgramUserAccount(ImpactAccess)
		IF (@Verbose = 1)
		BEGIN
			PRINT 'Start creating tbProgramUserAccount(ImpactAccess)for new employee'
		END

		INSERT tbProgramUserAccount (ProgramID, UserAccountID, CreateDateTime)
		SELECT @ProgramIDImpact, c.UserAccountID, CURRENT_TIMESTAMP
		FROM @CreateEmployees c INNER JOIN tbLFStagingEmployee l ON (c.SALES_REP_ID = l.SALES_REP_ID)
		WHERE l.CreateUserAccount = 1 AND l.HasImpactAccess = 1
		
		SELECT @ImpactProgramUserCreatCNT = @@Rowcount, @ERROR = @@ERROR

		IF (@ERROR <> 0)
		BEGIN
			ROLLBACK TRAN
			PRINT 'FAILURE: INSERT IN tbProgramUserAccount FOR IMPACT'
			SET @IsFailure = 1
			SET @StatusComment = 'upLFLoadEmployee - failed to insert Impact record to tbProgramUserAccount'
			UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
			RETURN
		END

		IF (@Verbose = 1)
		BEGIN
			PRINT 'ImpactAccess created for new employee'
			PRINT @ImpactProgramUserCreatCNT
		END

	END
	
		IF (@Verbose = 1)
		BEGIN
			PRINT 'END OF CREATE EMPLOYEE PROCESS'
			PRINT 'START UPDATE PROCESS'
		END

	--UPDATE tbUserAccount / tbEmployee / tbProgramUserAccount AND UPDATE tbPosition
	IF (@IsFailure = 0 AND @UpdateEmployeeCNT <> 0)
	-- UPDATE USER ACCTS
	BEGIN
		--UPDATE LOGIN NAME IN tbUserAccount
		IF (@Verbose = 1)
		BEGIN
			PRINT 'Begin update LoginName in tbUserAccount'
		END
		
		--Get count for LoginName update
		SELECT @LogInUpdateCNT = COUNT(*) FROM tbLFStagingEmployee WHERE UpdateRecord = 1
		
		UPDATE tbUserAccount SET
			LoginName = l.NODE_NAME,
			UpdateDateTime = CURRENT_TIMESTAMP
		FROM tbLFStagingEmployee l INNER JOIN tbUserAccount u ON (u.UserAccountID =  l.EmployeeID) 
		WHERE l.UpdateRecord = 1
		
		-- ERROR HANDLER
		IF (@@Rowcount <> @LogInUpdateCNT)
		BEGIN
			ROLLBACK TRAN
			PRINT 'FAILURE: UPDATE THE LOGIN NAME IN tbUserAccount'
			SET @IsFailure = 1
			SET @StatusComment = 'upLFLoadEmployee - failed to update a record in tbUserAccount'
			UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
			RETURN
		END
		
		IF (@Verbose = 1)
		BEGIN
			PRINT 'Update LoginName in tbUserAccount count'
			PRINT @LogInUpdateCNT
		END

		--UPDATE STATUS IN tbUserAccount
		IF (@Verbose = 1)
		BEGIN
			PRINT 'Begin update Status in tbUserAccount'
		END
		
		--Get count for Status update
		SELECT @UserAccStatusUpdateCNT = COUNT(*) FROM tbLFStagingEmployee WHERE UpdateRecord = 1 AND ChangeUserAccountStatus = 1
		
		UPDATE tbUserAccount SET
			Status = l.NewUserAccountStatus,
			StatusDateTime = CURRENT_TIMESTAMP
		FROM tbLFStagingEmployee l INNER JOIN tbUserAccount u ON (u.UserAccountID =  l.EmployeeID) 
		WHERE l.ChangeUserAccountStatus = 1 AND l.UpdateRecord = 1
		
		-- ERROR HANDLER
		IF (@@Rowcount <> @UserAccStatusUpdateCNT)
		BEGIN
			ROLLBACK TRAN
			PRINT 'FAILURE: UPDATE THE STATUS IN tbUserAccount'
			SET @IsFailure = 1
			SET @StatusComment = 'upLoadLoadFile - failed to update a Status in tbUserAccount'
			UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
			RETURN
		END

		IF (@Verbose = 1)
		BEGIN
			PRINT 'Update Status in tbUserAccount count'
			PRINT @UserAccStatusUpdateCNT
		END

		--UPDATE tbEmployee
		IF (@Verbose = 1)
		BEGIN
			PRINT 'Begin update tbEmployee'
		END

		UPDATE tbEmployee SET
			AddressLine1 = l.SALES_REP_MAIL_ADDRESS,
			AddressLine2 = l.SALES_REP_MAIL_ATTENTION,
			City = l.SALES_REP_MAIL_CITY,
			State = l.SALES_REP_MAIL_STATE_CODE,
			PostalCode = l.SALES_REP_MAIL_ZIP_CODE,
			EmailAddress = l.MAIL_ADDRESS_EXT,
			Phone = l.SALES_REP_PHONE,
			TerminationDate = l.SALES_REP_TERMINATION_DATE,
			HistoryCutOffDateTime = CASE WHEN l.RestartHistory = 1 THEN CURRENT_TIMESTAMP ELSE t.HistoryCutOffDateTime END,
			UpdateDateTime = CURRENT_TIMESTAMP
		FROM tbLFStagingEmployee l INNER JOIN tbEmployee t ON (l.EmployeeID =  t.EmployeeID)
		WHERE l.UpdateRecord = 1
		
		SET @ROWCOUNT = @@Rowcount

		IF (@Verbose = 1)
		BEGIN
			PRINT 'Update tbEmployee count'
			PRINT @ROWCOUNT
		END

		-- ERROR HANDLER
		IF (@ROWCOUNT <> @UpdateEmployeeCNT)
		BEGIN
			ROLLBACK TRAN
			PRINT 'FAILURE: UPDATE EMPLOYEE INFORMATION IN tbEmployee count mismatch'
			SET @IsFailure = 1
			SET @StatusComment = 'upLFLoadEmployee - failed to update a record in tbEmployee'
			UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
			RETURN
		END

		--UPDATE tbEmployee Status
		IF (@Verbose = 1)
		BEGIN
			PRINT 'Begin update tbEmployee Status'
		END
		
		--Get count for Status update
		SELECT @EmployeeStatusUpdateCNT = COUNT(*) FROM tbLFStagingEmployee WHERE ChangeEmployeeStatus = 1 AND UpdateRecord = 1
		UPDATE tbEmployee SET
			Status = NewEmployeeStatus,
			StatusDateTime = CURRENT_TIMESTAMP
		FROM tbLFStagingEmployee l INNER JOIN tbEmployee t ON (l.EmployeeID =  t.EmployeeID)
		WHERE l.ChangeEmployeeStatus = 1 AND l.UpdateRecord = 1
		
		-- ERROR HANDLER
		IF (@@Rowcount <> @EmployeeStatusUpdateCNT)
		BEGIN
			ROLLBACK TRAN
			PRINT 'FAILURE: UPDATE THE STATUS IN tbEmployee'
			SET @IsFailure = 1
			SET @StatusComment = 'upLFLoadEmployee - failed to update a status in tbEmployee'
			UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
			RETURN
		END

		IF (@Verbose = 1)
		BEGIN
			PRINT 'Update tbEmployee Status count'
			PRINT @EmployeeStatusUpdateCNT
		END

		--UPDATE POSITION PROCESS(VACATE & ASSIGN)
		--VACATE tbPosition
		IF (@Verbose = 1)
		BEGIN
			PRINT 'Vacate AssignedEmployeeID from tbPosition start'
		END

		UPDATE tbPosition SET
			AssignedEmployeeID = NULL,
			AssignedStartDate = NULL,
			AssignedEndDate = NULL
		FROM tbLFStagingEmployee l INNER JOIN tbPosition p ON (p.AssignedEmployeeID = l.EmployeeID)
		WHERE l.UpdateRecord = 1 AND l.ChangePositionAssignment = 1
		
		-- ERROR HANDLER
		IF (@@ERROR <> 0)
		BEGIN
			ROLLBACK TRAN
			PRINT 'FAILURE: VACATE ASSIGNEDEMPLOYEEID FROM tbPosition'
			SET @IsFailure = 1
			SET @StatusComment = 'upLFLoadEmployee - failed to Vacate AssignedEmployeeID from tbPosition'
			UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
			RETURN
		END

		--ASSIGN tbPosition
		UPDATE tbPosition SET
			AssignedEmployeeID = EmployeeID,
			AssignedStartDate = EFFECTIVE_DATE_FROM,
			AssignedEndDate = EFFECTIVE_DATE_TO,
			UpdateDateTime = CURRENT_TIMESTAMP
		FROM tbLFStagingEmployee l WHERE CustPositionCode = SF_TERRITORY_ID AND UpdateRecord = 1 AND l.ChangePositionAssignment = 1
		
		SELECT @PositionAssignUpdateCNT = @@Rowcount, @ERROR = @@ERROR

		IF (@Verbose = 1)
		BEGIN
			PRINT 'AssignedEmployeeID updated in tbEmployee count'
			PRINT @PositionAssignUpdateCNT
		END
		IF (@ERROR <> 0)
		BEGIN
			ROLLBACK TRAN
			PRINT 'FAILURE: UPDATE ASSIGNEDEMPLOYEEID IN tbPosition'
			SET @IsFailure = 1
			SET @StatusComment = 'upLFLoadEmployee - failed to update AssignedEmployeeID to tbPosition'
			UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
			RETURN
		END


		--DELETE tbProgramUserAccount: CBD
		DELETE tbProgramUserAccount
		FROM tbProgramUserAccount pa INNER JOIN tbLFStagingEmployee lf ON (pa.UserAccountID = lf.EmployeeID)
		WHERE lf.UpdateRecord = 1 AND lf.ChangeCBDAccess = 1 AND lf.HasCBDAccess = 0 AND pa.ProgramID = @ProgramIDCBD
		
		SELECT @ROWCOUNT = @@Rowcount, @ERROR = @@ERROR

		IF (@Verbose = 1)
		BEGIN
			PRINT 'tbProgramUserAccount delete count'
			PRINT @ROWCOUNT
		END
		IF (@ERROR <> 0)
		BEGIN
			ROLLBACK TRAN
			PRINT 'FAILURE: DELETE FROM tbProgramUserAccount: CBD'
			SET @IsFailure = 1
			SET @StatusComment = 'upLFLoadEmployee - failed to delete record from tbProgramUserAccount: CBD'
			UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
			RETURN
		END
		
		--DELETE tbProgramUserAccount: IMPACT
		DELETE tbProgramUserAccount
		FROM tbProgramUserAccount pa INNER JOIN tbLFStagingEmployee lf ON (pa.UserAccountID = lf.EmployeeID)
		WHERE UpdateRecord = 1 AND ChangeImpactAccess = 1 AND HasImpactAccess = 0 AND pa.ProgramID = @ProgramIDImpact

		SELECT @ROWCOUNT = @@Rowcount, @ERROR = @@ERROR

		IF (@Verbose = 1)
		BEGIN
			PRINT 'tbProgramUserAccount delete count'
			PRINT @ROWCOUNT
		END
		IF (@ERROR <> 0)
		BEGIN
			ROLLBACK TRAN
			PRINT 'FAILURE: DELETE FROM tbProgramUserAccount: IMPACT'
			SET @IsFailure = 1
			SET @StatusComment = 'upLFLoadEmployee - failed to delete record from tbProgramUserAccount: IMPACT'
			UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
					RETURN
		END


		--INSERT tbProgramUserAccount (CBDAccess)
		INSERT tbProgramUserAccount (ProgramID, UserAccountID, CreateDateTime)
		SELECT @ProgramIDCBD, EmployeeID, CURRENT_TIMESTAMP
		FROM tbLFStagingEmployee WHERE  (UpdateRecord = 1 AND ChangeCBDAccess = 1 AND HasCBDAccess = 1)
		SELECT @ROWCOUNT = @@Rowcount, @ERROR = @@ERROR

		IF (@Verbose = 1)
		BEGIN
			PRINT 'CBDAccess  inserted in  tbProgramUserAccount for update record count'
			PRINT @ROWCOUNT
		END
		IF (@ERROR <> 0)
		BEGIN
			ROLLBACK TRAN
			PRINT 'FAILURE: INSERT IN tbProgramUserAccount FOR CBD'
			SET @IsFailure = 1
			SET @StatusComment = 'upLFLoadEmployee - failed to insert CBD record to tbProgramUserAccount'
			UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
			RETURN
		END


		--INSERT tbProgramUserAccount (ImpactAccess)
		INSERT tbProgramUserAccount (ProgramID, UserAccountID, CreateDateTime)
		SELECT @ProgramIDImpact, EmployeeID, CURRENT_TIMESTAMP
		FROM tbLFStagingEmployee WHERE  (UpdateRecord = 1 AND ChangeImpactAccess = 1 AND HasImpactAccess = 1)
		
		SELECT @ROWCOUNT = @@Rowcount, @ERROR = @@ERROR

		IF (@Verbose = 1)
		BEGIN
			PRINT 'ImpactAccess  inserted in  tbProgramUserAccount for update record count'
			PRINT @ROWCOUNT
		END

		IF (@ERROR <> 0)
		BEGIN
			ROLLBACK TRAN
			PRINT 'FAILURE: INSERT IN tbProgramUserAccount FOR IMPACT'
			SET @IsFailure = 1
			SET @StatusComment = 'upLFLoadEmployee - failed to insert Impact record to tbProgramUserAccount'
			UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
			RETURN
		END

	END

 	--DELETE RECORDS FROM tbPositionHistory
 	IF (@IsFailure = 0 AND @Verbose = 0)
 	BEGIN
 		PRINT 'Delete all the record from tbPositionHistory'
 		DELETE tbPositionHistory
 		IF (EXISTS (SELECT 1 FROM tbPositionHistory)) 
 		BEGIN
 			ROLLBACK TRAN
 			PRINT 'FAILURE: DELETE tbPositionHistory'
 			SET @IsFailure = 1
 			SET @StatusComment = 'upLFLoadEmployee - failed to delete tbPositionHistory'
			UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
			RETURN
		END
 	END


 	--INSERT RECORDS TO tbPositionHistory  
 	IF (@IsFailure = 0 AND @Verbose = 0)
 	BEGIN
 		INSERT tbPositionHistory ( CustPositionCode, AssignedEmployeeID, AssignedCustEmployeeCode,
 			ParentCustPositionCode,AssignedStartDate, AssignedEndDate, CustLevelCode,TerritoryCode, SalesForceCode, AlignPeriodCode)
 		SELECT CustPositionCode, AssignedEmployeeID, AssignedCustEmployeeCode,
 			ParentCustPositionCode, AssignedStartDate, AssignedEndDate, CustLevelCode, TerritoryCode, SalesForceCode, AlignPeriodCode
 		FROM dbo.tbLFStagingPositionHistory
 		
 		IF (@@ERROR <> 0)
 		BEGIN
 			ROLLBACK TRAN
 			PRINT 'FAILURE: INSERT tbPositionHistory'
 			SET @IsFailure = 1
 			SET @StatusComment = 'upLFLoadEmployee - failed to insert tbPositionHistory'
			UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID
			RETURN
		END
 	END


	-- UPDATE tbEmployee / tbUserAccount Status
	DECLARE
		@EmployeeStatusFellOffTheFeed char(1),
		@UserAccountStatusDisabled char(1)
	
	-- CONSTANTS
	SET @EmployeeStatusFellOffTheFeed = 'F'
	SET @UserAccountStatusDisabled = 'D'
	
	IF (@IsFailure = 0)
	BEGIN
		-- UPDATE tbEmployee
		UPDATE tbEmployee SET
			Status = @EmployeeStatusFellOffTheFeed, 
			StatusDateTime = CURRENT_TIMESTAMP
		FROM tbEmployee e LEFT OUTER JOIN tbLFStagingEmployee lf ON (e.EmployeeID = lf.EmployeeID) 
		WHERE lf.EmployeeID IS NULL
		
		SET @FallOffEmployeesCNT = @@ROWCOUNT

		IF (@Verbose = 1)
		BEGIN
			PRINT 'tbEmployee Status set to @EmployeeStatusFellOffTheFeed count'
			PRINT @FallOffEmployeesCNT
		END

		-- UPDATE tbUserAccount
		UPDATE tbUserAccount SET 
			Status = @UserAccountStatusDisabled,
			StatusDateTime = CURRENT_TIMESTAMP
		FROM tbUserAccount ua  LEFT OUTER JOIN tbLFStagingEmployee lf ON (ua.UserAccountID = lf.EmployeeID) 
		WHERE lf.EmployeeID IS NULL 
			AND IsAdmin = 0

		SET @UserAccountDisabledCNT = @@ROWCOUNT

		IF (@Verbose = 1)
		BEGIN
			PRINT 'tbUserAccount Status set to @UserAccountStatus count'
			PRINT @UserAccountDisabledCNT
		END
	END


	-- CHECK OUTCOME
	IF (@IsFailure = 0)  
	BEGIN
		COMMIT TRAN

		SET @LoadFileStatus = @LFStatLoaded
		SET @StatusComment = 'Employee created - ' + ISNULL(CONVERT(varchar, @CreateEmployeeCNT), '0')
			+ '; Employee Updated - ' + ISNULL(CONVERT(varchar, @UpdateEmployeeCNT), '0')
			+ '; Employee Fall Off - ' + ISNULL(CONVERT(varchar, @FallOffEmployeesCNT), '0')
			+ '; User Account Disabled - ' + ISNULL(CONVERT(varchar, @FallOffEmployeesCNT), '0')
			+ '; CBD program account created - ' + ISNULL(CONVERT(varchar, @CBDProgramUserCreatCNT), '0')
			+ '; Impact program account created - ' + ISNULL(CONVERT(varchar, @ImpactProgramUserCreatCNT), '0')
			+ '; Login name updated - ' + ISNULL(CONVERT(varchar, @LogInUpdateCNT), '0')
			+ '; User account status updated - ' + ISNULL(CONVERT(varchar, @UserAccStatusUpdateCNT), '0')
			+ '; Employee status updated - ' + ISNULL(CONVERT(varchar, @EmployeeStatusUpdateCNT), '0')
			+ '; Position Assignment updated - ' + ISNULL(CONVERT(varchar, @PositionAssignUpdateCNT), '0')

		-- UPDATE RECORD
		UPDATE tbLoadFile SET Status = @LoadFileStatus, StatusComment = @StatusComment, StatusDateTime = DEFAULT WHERE LoadFileID = @LoadFileID

	END

	IF (@Verbose = 1)
	BEGIN
  		PRINT @StatusComment
	END
	
	SET NOCOUNT OFF
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO