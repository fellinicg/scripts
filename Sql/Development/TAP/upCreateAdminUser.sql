USE [Tap]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Mark Yachnin
-- OrigCreateDate: 20061004
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20061023
-- ============================================================
ALTER PROCEDURE dbo.upCreateAdminUser
	@CustEmployeeCode varchar(40),
	@FirstName varchar(50),
	@MiddleName varchar(50),
	@LastName varchar(100),
	@LoginName varchar(10)

AS

	-- NOTE: THIS PROC IS BEST EXECUTED BY PASSING THE INPUT PARAMETERS IN BY NAME
	--       (THIS WILL HANDLE @MiddleName = NULL)

	SET NOCOUNT ON
		
	-- GET ClientID
	DECLARE @ClientID int

	SET @ClientID = dbo.ufSettingValue('TAPRewardCentralClientID')


	-- ADD THE ADMIN TO GLOBALUSER
	DECLARE 
		@UserAccountID int,
		@IsSuccessGlobalUser bit,
		@AccountType char(1),
		@IsPerson bit,
		@AccountStatus char(1)

		SET @IsSuccessGlobalUser = 0
		SET @AccountType = 'C'
		SET @IsPerson = 1
		SET @AccountStatus = 'E'
	
	EXECUTE SQL2KPROD.GlobalUsers.dbo.upAddUserAccount
		 @AccountType = @AccountType
		,@IsPerson = @IsPerson
		,@ClientID = @ClientID
		,@CustAccountCode = @CustEmployeeCode
		,@PersonFirstGivenName = @FirstName
		,@PersonSecondGivenName = @MiddleName
		,@PersonFamilyName = @LastName
		,@AccountStatus = @AccountStatus
		,@UserAccountID = @UserAccountID OUTPUT

--	EXEC SQL2KPROD.GlobalUser.dbo.upInsertUpdateGlobalUser
--				@UserAccountID = @UserAccountID OUTPUT,
--				@LastName = @LastName,
--				@FirstName = @FirstName,
--				@MiddleName = @MiddleName,
--				@ClientID = @ClientID,
--				@CustUserCode = @CustEmployeeCode,
--				@IsSuccess = @IsSuccessGlobalUser OUTPUT
--
--	IF (@IsSuccessGlobalUser <> 1)
	IF (ISNULL(@UserAccountID, 0) = 0)
	BEGIN
		RAISERROR('upCreateAdminUser - Call to SQL2KPROD.GlobalUsers.dbo.upAddUserAccount failed.', 16, 1)
		RETURN
	END
	
	-- CREATE TEMPORARY PASSWORD
	DECLARE 
		@TempPassword char(6),
		@TempPasswordCrypted varchar(50)

	SET @TempPassword = (Left(NewID(), 3) + RIGHT(NewID(), 3))
	SET @TempPasswordCrypted = master.dbo.ud_CCEncrypt(@TempPassword)


	-- ADD THE ADMIN TO tbUserAccount
	DECLARE
		@UserAccountStatusEnabled char(1),
		@IsAdminTrue bit,
		@ResetPasswordTrue bit
		

	-- CONSTANTS
	SET @UserAccountStatusEnabled = 'E'
	SET @IsAdminTrue = 1
	SET @ResetPasswordTrue = 1

	INSERT INTO tbUserAccount(
		UserAccountID, 
		LoginName, 
		PasswordCrypted,  
		Status, 
		StatusDateTime, 
		IsAdmin, 
		ResetPassword, 
		CreateDateTime, 
		UpdateDateTime
		)
	VALUES(
		@UserAccountID, 
		@LoginName, 
		@TempPasswordCrypted,
		@UserAccountStatusEnabled, 
		CURRENT_TIMESTAMP, 
		@IsAdminTrue, 
		@ResetPasswordTrue, 
		CURRENT_TIMESTAMP, 
		CURRENT_TIMESTAMP
		)

	IF (@@ROWCOUNT <> 1)
	BEGIN
		RAISERROR('upCreateAdminUser - Error inserting into tbUserAccount.', 16, 1)
		RETURN
	END
	

	-- OUTPUT TO REPORT TO THE NEW ADMIN
	SELECT 
			u.UserAccountID,
			g.CustEmployeeCode,
			g.FirstName, 
			ISNULL(g.MiddleName,'') as MiddleName, 
			g.LastName, 
			u.LoginName, 
			master.dbo.ud_ccdecrypt(u.passwordCrypted) AS TemporaryPassword
	FROM tbUserAccount u JOIN dbo.vwGlobalUserAccount g ON (u.UserAccountID = g.UserAccountID)
	WHERE u.UserAccountID = @UserAccountID

	SET NOCOUNT OFF

