
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER  PROCEDURE [dbo].[Transfer2POHeader] AS
INSERT INTO poheader ( ReferenceNo, VendorNo, VendorName, SBTCompany )
SELECT IL.ItemLineID, IL.VendorID, IL.VendorInfo, '02' AS SBTCompany
FROM Curly.FulFilment.dbo.AwardLine AL
INNER JOIN Curly.FulFilment.dbo.ItemLine IL ON AL.AwardLineID = IL.AwardLineID
INNER JOIN Curly.FulFilment.dbo.OrderHeader OH ON AL.OrderID = OH.OrderID
WHERE AL.Void = 0
AND AL.Processed = 0
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

