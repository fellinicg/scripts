
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER  PROCEDURE [dbo].[UpdateAwardLine_Processed] AS
UPDATE Curly.FulFilment.dbo.AwardLine SET Processed = 1, ProcessDate = GETDATE()
FROM Curly.FulFilment.dbo.AwardLine AL
INNER JOIN Curly.FulFilment.dbo.ItemLine IL ON AL.AwardLineID = IL.AwardLineID
WHERE (IL.PONumber = 0 OR IL.PONumber IS NULL) 
AND AL.Processed = 0 
AND AL.Void = 0
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

