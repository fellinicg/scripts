
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER   PROCEDURE [dbo].[PostProcessForPOP] AS
UPDATE CURLY.FulFilment.dbo.ItemLine SET 
POP_ItemTitle =	SUBSTRING(IL.ItemTitle, 1, CHARINDEX('$', IL.ItemTitle)) + CONVERT(VARCHAR, CONVERT(INT, IL.CostPerUnit * AL.Quantity)) + SUBSTRING(IL.ItemTitle, CHARINDEX(' ', IL.ItemTitle, CHARINDEX('$', IL.ItemTitle)), 1000),
POP_ItemModel = CASE IL.ItemModel
		 WHEN '' THEN ''
		 ELSE '$' + CONVERT(VARCHAR, CONVERT(INT, IL.CostPerUnit * AL.Quantity)) +
		      SUBSTRING(IL.ItemModel, CHARINDEX(' ', IL.ItemModel, CHARINDEX('$', IL.ItemModel)), 1000) END,
POP_CostPerUnit = CONVERT(DECIMAL(20,2), IL.CostPerUnit * AL.Quantity),
POP_ModifiedBy = 1,
POP_ModifiedDate = GETDATE()
FROM poline
INNER JOIN CURLY.FulFilment.dbo.ItemLine IL ON poline.ReferenceNo = IL.ItemLineID
INNER JOIN CURLY.FulFilment.dbo.AwardLine AL ON IL.AwardLineID = AL.AwardLineID
INNER JOIN CURLY.FulFilment.dbo.OrderHeader OH ON AL.OrderID = OH.OrderID
WHERE ItemOptions like '%Full Dollar Amount%'
AND poline.UpdatedProg = 0

UPDATE CURLY.FulFilment.dbo.AwardLine SET 
POP_AwardTitle = SUBSTRING(IL.ItemTitle, 1, CHARINDEX('$', IL.ItemTitle)) + CONVERT(VARCHAR, CONVERT(INT, IL.CostPerUnit * AL.Quantity)) + SUBSTRING(IL.ItemTitle, CHARINDEX(' ', IL.ItemTitle, CHARINDEX('$', IL.ItemTitle)), 1000),
POP_Quantity = 1
FROM poline
INNER JOIN CURLY.FulFilment.dbo.ItemLine IL ON poline.ReferenceNo = IL.ItemLineID
INNER JOIN CURLY.FulFilment.dbo.AwardLine AL ON IL.AwardLineID = AL.AwardLineID
INNER JOIN CURLY.FulFilment.dbo.OrderHeader OH ON AL.OrderID = OH.OrderID
WHERE ItemOptions like '%Full Dollar Amount%'
AND poline.UpdatedProg = 0
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

