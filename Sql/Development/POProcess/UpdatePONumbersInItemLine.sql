
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER  PROCEDURE [dbo].[UpdatePONumbersInItemLine] AS
UPDATE Curly.FulFilment.dbo.ItemLine SET PONumber = poheader.PoNumber
FROM Curly.FulFilment.dbo.ItemLine IL INNER JOIN poheader ON IL.ItemLineID = poheader.ReferenceNo 
WHERE poheader.UpdatedProg = 0
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

