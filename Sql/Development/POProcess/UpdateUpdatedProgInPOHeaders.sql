
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[UpdateUpdatedProgInPOHeaders] AS
UPDATE poheader SET poheader.UpdatedProg = 1
WHERE poheader.UpdatedProg = 0
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

