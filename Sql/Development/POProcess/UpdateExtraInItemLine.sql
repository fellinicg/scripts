
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER  PROCEDURE [dbo].[UpdateExtraInItemLine] AS
UPDATE Curly.FulFilment.dbo.ItemLine SET SBTStatus = poline.Status, InvoiceDate = poLine.InvoiceDate, InvoiceNumber = poline.InvoiceNumber, SBTStatusDate = poline.StatusDate
FROM Curly.FulFilment.dbo.ItemLine IL INNER JOIN poline ON IL.ItemLineID = poline.ReferenceNo 
WHERE ((IL.SBTStatusDate <>  poLine.StatusDate) or (IL.SBTStatusDate is NULL and poline.StatusDate is not NULL)) 
and ItemLineID  >= 62044
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

