
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER  PROCEDURE [dbo].[UpdatePOLineNo] AS
UPDATE Curly.FulFilment.dbo.ItemLine SET POLine = poline.[LineNO]
FROM Curly.FulFilment.dbo.ItemLine IL
INNER JOIN poline ON IL.ItemLineID = poline.ReferenceNo 
WHERE poline.UpdatedProg = 0
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

