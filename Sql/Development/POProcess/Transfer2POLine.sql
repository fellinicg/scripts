USE [POProcess]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER  PROCEDURE [dbo].[Transfer2POLine] AS
INSERT INTO PoLine ( ReferenceNo, LineReferenceNo, Description, LineMemo, Quantity, Cost, Account )
SELECT 
IL.ItemLineID, 
OH.ProjectNumber,
LEFT(CASE CHARINDEX('Full Amount', IL.ItemOptions)
 WHEN 0 THEN IL.ItemTitle
 ELSE SUBSTRING(IL.ItemTitle, 1, CHARINDEX('$', IL.ItemTitle)) + CONVERT(VARCHAR, CONVERT(INT, IL.CostPerUnit * AL.Quantity)) +
      SUBSTRING(IL.ItemTitle, CHARINDEX(' ', IL.ItemTitle, CHARINDEX('$', IL.ItemTitle)), 1000)
END, 60) AS 'Expr6',
(CASE IL.ItemModel
 WHEN '' THEN ''
 ELSE CASE CHARINDEX('Full Amount', IL.ItemOptions)
       WHEN 0 THEN 'MODEL: ' + IL.ItemModel
       ELSE 'MODEL: $' + CONVERT(VARCHAR, CONVERT(INT, IL.CostPerUnit * AL.Quantity)) +
            SUBSTRING(IL.ItemModel, CHARINDEX(' ', IL.ItemModel, CHARINDEX('$', IL.ItemModel)), 1000)
      END  + CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10)
END) + 
(CASE IL.ItemOptions
 WHEN '' THEN ''
 ELSE IL.ItemOptions + CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10)
END) +
'SHIP TO:' + CHAR(13) + CHAR(10) + '   ' + OH.ShipName + CHAR(13) + CHAR(10) + '   ' + OH.ShipAddress1 + CHAR(13) + CHAR(10) +
(CASE ISNULL(OH.ShipAddress2, '')
 WHEN '' THEN ''
 ELSE '   ' + OH.ShipAddress2 + CHAR(13) + CHAR(10)
END) +
'   ' + OH.ShipCity + ' ' + OH.ShipState + '  ' + OH.ShipPostal + CHAR(13) + CHAR(10) + ISNULL(OH.ShipCountry, '')  +  
ISNULL(OH.InternationalAddress, '') +  '   PHONE: ' + ISNULL(OH.ShipPhone, '') + CASE ISNULL(PIT.pit_AddlPOInfo ,'') WHEN '' THEN '' ELSE CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10) + PIT.pit_AddlPOInfo END
AS 'EXP1',
CASE CHARINDEX('Full Amount', IL.ItemOptions)
 WHEN 0 THEN AL.Quantity
 ELSE 1
END AS 'Expr3',
CONVERT(DECIMAL(20,2), 
 CASE CHARINDEX('Full Amount', IL.ItemOptions)
  WHEN 0 THEN IL.CostPerUnit
  ELSE IL.CostPerUnit * AL.Quantity
 END) AS 'Expr4',
'1450-' + RTRIM(LTRIM(OH.ProjectNumber)) + '-700' AS 'Exp5'
FROM Curly.FulFilment.dbo.AwardLine AL
INNER JOIN Curly.FulFilment.dbo.ItemLine IL ON AL.AwardLineID = IL.AwardLineID
INNER JOIN Curly.FulFilment.dbo.OrderHeader OH ON AL.OrderID = OH.OrderID
LEFT JOIN SQL2KPROD.Catalog.dbo.Programs P ON OH.ProgramCode = P.p_Code
LEFT JOIN SQL2KPROD.Catalog.dbo.ProgramItemTypes PIT ON P.p_ID = PIT.pit_programID AND IL.ItemType = PIT.pit_ItemType
WHERE AL.Processed = 0
AND AL.Void = 0
