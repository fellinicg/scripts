
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE [dbo].[UpdateFromSBT] AS
EXEC UpdatePOLineNo


EXEC UpdatePONumbersInItemLine


EXEC UpdateExtraInItemLine


EXEC PostProcessForPOP


EXEC UpdateUpdatedProgInPOHeaders


EXEC UpdateUpdatedProgInPOLine


EXEC CURLY.Fulfilment.dbo.POP_ImpPONumbers


EXEC CURLY.Fulfilment.dbo.POP_ClosePOsFromSBT


EXEC PORejects
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

