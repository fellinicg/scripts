
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[InsertToCheckPrice] AS
INSERT INTO FulFilment.dbo.PriceCheck ( pc_OrderID, pc_ProgramCode, pc_AwardTitle, pc_DollarCost, pc_PointLevelFullfilment, pc_PointLevelCatalog )
SELECT FulFilment.dbo.OrderHeader.OrderID, FulFilment.dbo.OrderHeader.ProgramCode, FulFilment.dbo.AwardLine.AwardTitle, FulFilment.dbo.ItemLine.CostPerUnit, FulFilment.dbo.AwardLine.UnitPoints, OldCatalog.dbo.ProgramAwards.pa_PointLevel
FROM ((FulFilment.dbo.OrderHeader INNER JOIN FulFilment.dbo.AwardLine ON FulFilment.dbo.OrderHeader.OrderID = FulFilment.dbo.AwardLine.OrderID) 
INNER JOIN FulFilment.dbo.ItemLine ON FulFilment.dbo.AwardLine.AwardLineID = FulFilment.dbo.ItemLine.AwardLineID) INNER JOIN OldCatalog.dbo.ProgramAwards ON FulFilment.dbo.AwardLine.AwardID = OldCatalog.dbo.ProgramAwards.pa_AwardID
WHERE (((FulFilment.dbo.AwardLine.Processed)=0) AND ((FulFilment.dbo.AwardLine.Void)=0))
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

