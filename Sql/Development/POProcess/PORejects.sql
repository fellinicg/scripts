
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE [dbo].[PORejects] AS
SELECT *
FROM dbo.poheader ph
LEFT JOIN dbo.Poline pl ON ph.ReferenceNo = pl.ReferenceNo
WHERE 
CONVERT(VARCHAR, ph.PoDate, 101) = CONVERT(VARCHAR, GETDATE(), 101)
AND (ph.VendorNo = 0
OR LEN(ph.REMARKS) > 0
OR ph.UpdatedSBT = 0
OR ph.UpdatedProg = 0
OR ph.PoNumber IS NULL)
ORDER BY ph.ReferenceNo

select ponumber, [lineno], count([lineno]) 'Count'
from poline
where statusdate >= '3/1/2005'
group by ponumber, [lineno]
having count([lineno]) > 1
order by ponumber, [lineno]

select * 
from poline 
inner join (
select ponumber 'po#', [lineno] 'linenum'
from poline
where statusdate >= '3/1/2005'
group by ponumber, [lineno]
having count([lineno]) > 1) a on ponumber = a.po# and [lineno] = a.linenum
order by ponumber, [lineno]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

