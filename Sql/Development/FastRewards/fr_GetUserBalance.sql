USE [FastRewards]
GO
/****** Object:  StoredProcedure [dbo].[fr_GetUserBalance]    Script Date: 09/08/2006 11:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[fr_GetUserBalance]
(
@gu_ID int
)

AS

DECLARE @ID int

SELECT @ID = ClientID 
FROM GlobalUsers.dbo.tbUserAccount
WHERE UserAccountID = @gu_ID

IF @ID IS NULL RETURN
IF @ID = 5 SET @ID = (SELECT frv_Value FROM dbo.FastRewardsVariables WHERE frv_Desc = 'FastRewardsProgramID')
IF @ID = 3 SET @ID = (SELECT frv_Value FROM dbo.FastRewardsVariables WHERE frv_Desc = 'FastRewardsProgramID')
IF @ID = 26 SET @ID = (SELECT frv_Value FROM dbo.FastRewardsVariables WHERE frv_Desc = 'FastRewardsProgramID')
IF @ID = 6 SET @ID = (SELECT frv_Value FROM dbo.FastRewardsVariables WHERE frv_Desc = 'FastRewardsProgramIDAbbott')

SELECT s_TranDate AS TranDate, 
	s_TranID AS TranID,
	t_Description AS TranDescription,
	UPPER(s_tranDescr) AS Descript, 
	Case When s_tranAmount < 0 THEN s_tranAmount * -1 ELSE NULL END AS Debit,
	CASE WHEN s_tranAmount > 0 THEN s_tranAmount ELSE NULL END AS Credit,
	s_tranType AS TranType,
	s_AvailBal As Avail,
	s_LedgBal AS Ledger,
	u_FirstName + ' ' + u_LastName AS UserName,
	ac_CNum AS CardNo,
	ac_ExpDate AS ExpDate,
	RIGHT(ac_Status, LEN(ac_Status) - 8) AS Status,
	s_GenerateDate AS GenDate
FROM GOLDFINGER.ClarityCards.dbo.Statements
INNER JOIN GOLDFINGER.ClarityCards.dbo.Accounts ON s_InternalKey = ac_InternalKey
INNER JOIN GOLDFINGER.ClarityCards.dbo.Users ON ac_ExternalKey = u_ExternalKey 
	AND u_MPGProgUserID = @gu_ID
	AND u_p_ID = @ID
INNER JOIN GOLDFINGER.ClarityCards.dbo.TransactionTypes ON s_tranType = t_Type
WHERE s_tranType IN (4010,4011,4012,4040)
AND s_Hidden = 0
ORDER BY s_tranDate DESC, s_RecID DESC