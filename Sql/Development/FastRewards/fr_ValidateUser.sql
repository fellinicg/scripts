USE [FastRewards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.fr_ValidateUser
(
@u_Card_No varchar(50),
@u_Password varchar(50)
)

AS

DECLARE @u_ID int

SELECT TOP 1 @u_ID = u_ID
FROM dbo.Users
INNER JOIN GlobalUsers.dbo.tbUserAccount ON u_ID = UserAccountID
WHERE u_Card_No = @u_Card_No AND Master.dbo.ud_CCDecrypt(u_Password) = @u_Password

SELECT u_ID, dbo.ufGetPersonsFullName(u_ID) AS uName
FROM dbo.Users
WHERE u_Card_No = @u_Card_No AND Master.dbo.ud_CCDecrypt(u_Password) = @u_Password

INSERT INTO AccessLog (al_u_ID, al_Action, al_Success) VALUES (@u_ID, 'Login attempt', @@ROWCOUNT)

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF