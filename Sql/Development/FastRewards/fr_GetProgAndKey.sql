USE [FastRewards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.fr_GetProgAndKey 
(
@gu_ID int
)

AS

DECLARE @ID int
SELECT @ID = ClientID 
FROM GlobalUsers.dbo.tbUserAccount
WHERE UserAccountID = @gu_ID

IF @ID IS NULL RETURN
IF @ID = 5 SET @ID = (SELECT frv_Value FROM dbo.FastRewardsVariables WHERE frv_Desc = 'FastRewardsProgramID')
IF @ID = 6 SET @ID = (SELECT frv_Value FROM dbo.FastRewardsVariables WHERE frv_Desc = 'FastRewardsProgramIDAbbott')


SELECT @ID AS PROG, ac_InternalKey AS IK
FROM GOLDFINGER.ClarityCards.dbo.Accounts
INNER JOIN GOLDFINGER.ClarityCards.dbo.Users ON ac_ExternalKey = u_ExternalKey 
	AND u_MPGProgUserID = @gu_ID
	AND u_p_ID = @ID
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF