USE [FastRewards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: June 21, 2005
-- Description:	Return full name from GlobalUsers
-- =============================================
CREATE FUNCTION dbo.ufGetPersonsFullName 
(
	@UserAccountID int
)
RETURNS varchar(200)
AS
BEGIN
	DECLARE @Result varchar(200)
 
	SET @Result = 'Unknown'

	SELECT @Result = ISNULL(PersonBuiltFullName, ISNULL(PersonFirstGivenName + ' ' + PersonFamilyName, ISNULL(AccountName, '')))
	FROM GlobalUsers.dbo.tbUserAccount
	WHERE UserAccountID = @UserAccountID

	RETURN @Result

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.ufGetPersonsFullName
TO fastrewardsuser
