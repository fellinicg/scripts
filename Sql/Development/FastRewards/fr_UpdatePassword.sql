USE [FastRewards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.fr_UpdatePassword
(
@u_ID int,
@u_Password varchar(50),
@u_Password_New varchar(50),
@em_Address varchar(200)
)
AS

DECLARE @VALID int

SELECT @VALID = u_ID 
FROM dbo.Users
INNER JOIN GlobalUsers.dbo.tbEmailAddress ON u_ID = UserAccountID AND EmailAddress = @em_Address
WHERE u_ID = @u_ID
AND Master.dbo.ud_CCDecrypt(u_Password) = @u_Password

IF @VALID IS NOT NULL
	BEGIN
		BEGIN TRANSACTION
		UPDATE dbo.Users
		SET u_Password = Master.dbo.ud_CCEncrypt(@u_Password_New)
		WHERE u_ID = @u_ID
		IF @@ROWCOUNT > 0 AND @@ERROR = 0
			BEGIN
				INSERT INTO dbo.FirstLogin (fl_ID)
					VALUES (@u_ID)
				IF @@ROWCOUNT= 0 OR @@ERROR <> 0
					ROLLBACK TRANSACTION
			END
		ELSE
			ROLLBACK TRANSACTION
		COMMIT TRANSACTION
	END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF