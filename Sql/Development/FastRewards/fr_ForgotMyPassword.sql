USE [FastRewards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.fr_ForgotMyPassword 
(
@u_Card_No varchar(50),
@em_Address	varchar(200),
@RETURN varchar(200)  OUTPUT
)

AS

DECLARE @Valid int
DECLARE @ID int
DECLARE @RETURNTEMP varchar(50)
DECLARE @Failure varchar(50)

SET @RETURNTEMP = ''
SET @Valid = -1
SET @ID = -1
SET @Failure = '\n\nInvalid Username or E-Mail.'

--check if valid user
SET @ID = (SELECT u_ID 
FROM dbo.Users
WHERE u_Card_No = @u_Card_No
AND u_RecState = 0)
IF @ID IS NULL OR @ID = -1
	BEGIN
	SET @RETURN = @Failure
	RETURN
	END

--check if valid e-mail
SELECT u_ID
FROM dbo.Users
INNER JOIN GlobalUsers.dbo.tbEmailAddress ON u_ID = UserAccountID
WHERE u_Card_No = @u_Card_No
AND EmailAddress = @em_Address
IF @@ROWCOUNT = 0 OR @@ERROR <> 0
	BEGIN
	SET @RETURN = @Failure
	RETURN
	END

--check if valid global user
SELECT UserAccountID
FROM GlobalUsers.dbo.tbUserAccount
WHERE AccountStatus = 'E'
AND UserAccountID = @ID
IF @@ROWCOUNT = 0 OR @@ERROR <> 0
	BEGIN
	SET @RETURN = @Failure
	RETURN
	END

--if we make it this far, then the user 
--is authenticated and authorized
--return the existing password
SET @RETURNTEMP =  (SELECT master.dbo.ud_CCDecrypt(u_Password) 
			FROM dbo.Users 
			WHERE u_ID = @ID)
IF @RETURNTEMP = NULL OR @RETURNTEMP = ''
	BEGIN
		SET @RETURN = '\n\nERROR DURING PASSWORD ACCESS'
		RETURN
	END 

--Is a valid request, define and return the fields
DECLARE @THESENDER varchar(100)
DECLARE @THEBCC varchar(100)
DECLARE @THESUBJECT varchar(100)
DECLARE @THEBODY varchar(1500)
DECLARE @PORTALADMIN varchar(100)
DECLARE @SingleCRLF char(2)
DECLARE @DoubleCRLF char(4)

SET @THESENDER = (SELECT frv_Value FROM dbo.FastRewardsVariables WHERE frv_Desc = 'FastRewardsAdminEmailAddress')
SET @THEBCC = (SELECT frv_Value FROM dbo.FastRewardsVariables WHERE frv_Desc = 'FastRewardsEmailBccAddress')
SET @THESUBJECT = (SELECT frv_Value FROM dbo.FastRewardsVariables WHERE frv_Desc = 'FastRewardsPasswordSubjectLine')
SET @PORTALADMIN = (SELECT frv_Value FROM dbo.FastRewardsVariables WHERE frv_Desc = 'FastRewardAdminName')

-- set vars
SET @SingleCRLF = char(13) + char(10)
SET @DoubleCRLF = char(13) + char(10) + char(13) + char(10)

SET @THEBODY = 'You requested to have your Fast Rewards password sent to you.' + @SingleCRLF
 + 'Your password is: ' + @DoubleCRLF  
 + @RETURNTEMP  + @DoubleCRLF
 + 'Please delete this e-mail as soon as possible to avoid misuse of your account.'  + @DoubleCRLF   
 + @PORTALADMIN  + @SingleCRLF 
 + 'Madison Performance Group'   + @SingleCRLF
 + 'Fast Rewards Site Administrator'
 
--SET @RETURN = @em_Address + @THESENDER + @THEBCC + @THESUBJECT + @THEBODY

DECLARE @EmailIDReturn int
DECLARE @MessageQueuedReturn bit
DECLARE @MessageSentReturn int

EXECUTE [mail.madisonpg.com].OutboundEmail.dbo.upQueueMessage
  @EmailID = @EmailIDReturn OUTPUT, 
  @ToEmailAddressList = @em_Address,
  @FromEmailAddress = @THESENDER,
  @BCCEmailAddressList = @THEBCC,
  @MessageSubject = @THESUBJECT,
  @MessageBody = @THEBODY,
  @SendImmediately = 1,
  @MessageQueued = @MessageQueuedReturn OUTPUT,
  @MessageSent = @MessageSentReturn OUTPUT

IF @MessageSentReturn = 1
	SET @RETURN = 'E-Mail Sent'
ELSE
	SET @RETURN = '\n\nE-Mail cannot be mailed'

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF