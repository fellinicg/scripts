USE [FastRewards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.fr_IsUserValid
(
@u_ID int,
@uname varchar(200)
)
AS

SELECT COUNT(u_ID)
FROM dbo.Users
INNER JOIN GlobalUsers.dbo.tbUserAccount ON u_ID = UserAccountID
WHERE u_ID = @u_ID
AND REPLACE(dbo.ufGetPersonsFullName(u_ID), ' ', '') = REPLACE(@uname, ' ', '')

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF