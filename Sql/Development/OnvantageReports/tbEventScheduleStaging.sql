USE [OnvantageReports]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbEventScheduleStaging]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbEventScheduleStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbEventScheduleStaging](
	[ActivityDate] [datetime] NULL,
	[ActivityTime] [varchar](50) NULL,
	[Type] [varchar](100) NOT NULL,
	[ActivityName] [varchar](100) NOT NULL,
	[ProgramName] [varchar](100) NOT NULL,
	[AttendeeCount] [varchar](10) NOT NULL,
	[Capacity][varchar](10) NULL,
	[DKNumber] [varchar](20) NOT NULL,
	[Comments] [varchar](500) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF