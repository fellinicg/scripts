USE [OnvantageReports]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060915
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upGetReportList 
AS
BEGIN

	-- Local variable declarations

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables

	-- Return data
	SELECT 
		ReportID, 
		ReportName,
		LoadProc,
		ExecProc
	FROM dbo.tbReport
	WHERE IsDeleted = 0
	ORDER BY ReportName

END
GO
GRANT EXECUTE
ON dbo.upGetReportList
TO system