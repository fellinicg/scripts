USE [OnvantageReports]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbReport]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbReport]
GO
CREATE TABLE [dbo].[tbReport](
	[ReportID] [int] IDENTITY(1,1) NOT NULL,
	[ReportName] [varchar](40) NOT NULL,
	[ReportTable] [varchar](40) NOT NULL,
	[LoadProc] [varchar](40) NOT NULL,
	[ExecProc] [varchar](40) NOT NULL,
	[IsDeleted] [bit] NOT NULL
 CONSTRAINT [PK_tbReport] PRIMARY KEY CLUSTERED 
(
	[ReportID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF