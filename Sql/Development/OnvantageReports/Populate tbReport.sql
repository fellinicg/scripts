USE [OnvantageReports]
GO
SET IDENTITY_INSERT dbo.tbReport ON
GO
INSERT INTO dbo.tbReport (ReportID, ReportName, ReportTable, LoadProc, ExecProc, IsDeleted)
VALUES (1, 'Activity Roster', 'tbActivityRoster', 'upLoadActivityRoster', 'upActivityRoster', 0)
GO
INSERT INTO dbo.tbReport (ReportID, ReportName, ReportTable, LoadProc, ExecProc, IsDeleted)
VALUES (2, 'Schedule of Events', 'tbEventSchedule', 'upLoadEventSchedule', 'upEventSchedule', 0)
GO
INSERT INTO dbo.tbReport (ReportID, ReportName, ReportTable, LoadProc, ExecProc, IsDeleted)
VALUES (3, 'Primary/Guest List', 'tbPaxList', 'upLoadPaxList', 'upPaxList', 0)
GO
INSERT INTO dbo.tbReport (ReportID, ReportName, ReportTable, LoadProc, ExecProc, IsDeleted)
VALUES (4, 'Golf', 'tbGolf', 'upLoadGolf', 'upGolf', 0)
GO
INSERT INTO dbo.tbReport (ReportID, ReportName, ReportTable, LoadProc, ExecProc, IsDeleted)
VALUES (5, 'Hotel Room List', 'tbRoomList', 'upLoadRoomList', 'upRoomList', 0)
GO
INSERT INTO dbo.tbReport (ReportID, ReportName, ReportTable, LoadProc, ExecProc, IsDeleted)
VALUES (6, 'Activities', 'tbActivities', 'upLoadActivities', 'upActivities', 0)
GO
SET IDENTITY_INSERT dbo.tbReport OFF
