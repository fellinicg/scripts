USE OnvantageReports
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060915
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upGetActivityDateTimes
(
	@Activity varchar(200),
	@Report varchar(100),
	@DKNumber varchar(20)
)
AS

	-- Return data
	IF (@Report = 'upActivityRoster')
		SELECT DISTINCT
			ActivityGroup,
			StartDate,
			EndDate,
			CONVERT(varchar, StartDate, 100) + ' - ' + CONVERT(varchar, EndDate, 100) TimeSpan
		FROM dbo.tbActivityRoster
		WHERE Activity = @Activity
		AND DKNumber = @DKNumber
		ORDER BY StartDate, EndDate 

	IF (@Report = 'upGolf')
		SELECT DISTINCT
			Activity AS ActivityGroup,
			StartDate,
			EndDate,
			CONVERT(varchar, StartDate, 100) + ' - ' + CONVERT(varchar, EndDate, 100) TimeSpan
		FROM dbo.tbGolf
		WHERE Activity = @Activity
		AND DKNumber = @DKNumber
		ORDER BY StartDate, EndDate 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
GRANT EXECUTE
ON dbo.upGetActivityDateTimes
TO system