USE OnvantageReports
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060915
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upActivityRoster
(
	@DKNumber varchar(20),
	@Activity varchar(100),
	@StartDate datetime
)
AS

	-- Return data
	SELECT 
		LastName AS LastName,
		FirstName AS FirstName,
		PaxType AS PaxType,
		Area AS Area,
		Region AS Region,
		District AS District,
		Title AS Title,
		ActivityGroup AS ActivityDate,
		StartDate AS StartDate,
		EndDate AS EndDate
	FROM dbo.tbActivityRoster
	WHERE DKNumber = @DKNumber
		AND Activity = @Activity
		AND StartDate = @StartDate
	ORDER BY LastName ASC, PaxType DESC

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
GRANT EXECUTE
ON dbo.upActivityRoster
TO system