USE [OnvantageReports]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbProgram]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbProgram]
GO
CREATE TABLE [dbo].[tbProgram](
	[DKNumber] [int] NOT NULL,
	[ProgramName] [varchar](40) NOT NULL
 CONSTRAINT [PK_tbProgram] PRIMARY KEY CLUSTERED 
(
	[DKNumber] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF