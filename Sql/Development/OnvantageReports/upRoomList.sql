USE OnvantageReports
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060920
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upRoomList
(
	@DKNumber varchar(20)
)
AS

	-- Return data
	SELECT 
		LastName AS LastName,
		FirstName AS FirstName,
		BadgeName AS BadgeName,
		Area AS Area,
		Region AS Region,
		District AS District, 
		Title AS Title, 
		Guest AS Guest,
		CONVERT(varchar, CheckIn, 101) AS CheckIn,
		CONVERT(varchar, CheckOut, 101) AS CheckOut,
		RoomType AS RoomType,
		RoomQuality AS RoomQuality,
		BillingInstructions AS BillingInstructions,
		Smoking AS Smoking,
		SpecialRequests AS SpecialRequests,
		RoomNotes AS RoomNotes,
		HotelName AS HotelName,
		DKNumber AS DKNumber,
		ProgramName AS ProgramName,
		ProgramDates AS ProgramDates
	FROM dbo.tbRoomList
	WHERE DKNumber = @DKNumber
	ORDER BY LastName, FirstName

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
GRANT EXECUTE
ON dbo.upRoomList
TO system