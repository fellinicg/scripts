USE [OnvantageReports]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbActivityRoster]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbActivityRoster]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbActivityRoster](
	[ProgramName] [varchar](200) NOT NULL,
	[DKNumber] [varchar](20) NOT NULL,
	[ProgramDates] [varchar](100) NOT NULL,
	[District] [varchar](100) NOT NULL,
	[Region] [varchar](100) NOT NULL,
	[Title] [varchar](100) NOT NULL,
	[FirstName] [varchar](100) NOT NULL,
	[LastName] [varchar](100) NOT NULL,
	[BadgeName] [varchar](100) NOT NULL,
	[PaxID] [int] NOT NULL,
	[PrimaryID] [int] NOT NULL,
	[Area] [varchar](200) NOT NULL,
	[PaxType] [varchar](30) NOT NULL,
	[ActivityGroup] [varchar](500) NOT NULL,
	[Activity] [varchar](500) NOT NULL,
	[StartDate] [smalldatetime] NOT NULL,
	[EndDate] [smalldatetime] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
GRANT SELECT
ON dbo.tbActivityRoster
TO system