USE OnvantageReports
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060920
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upEventSchedule
(
	@DKNumber varchar(20)
)
AS

	-- Return data
	SELECT 
		ProgramDates AS ProgramDates,
		ActivityDate AS ActivityDate,
		ActivityTime AS ActivityTime,
		ActivityName AS ActivityName,
		ProgramName AS ProgramName,
		Capacity AS Capacity,
		AttendeeCount AS AttendeeCount,
		Available AS Available,
		DKNumber AS DKNumber,
		Comments AS Comments
	FROM dbo.tbEventSchedule
	WHERE DKNumber = @DKNumber
	ORDER BY ActivityDate,
		CONVERT(varchar, CONVERT(smalldatetime, RTRIM(LEFT(ActivityTime, CHARINDEX('-', ActivityTime)-1))), 120),
		CONVERT(varchar, CONVERT(smalldatetime, LTRIM(SUBSTRING(ActivityTime, CHARINDEX('-', ActivityTime)+1, 8))), 120),
		ActivityName

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
GRANT EXECUTE
ON dbo.upEventSchedule
TO system