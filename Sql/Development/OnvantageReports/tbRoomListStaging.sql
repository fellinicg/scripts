USE [OnvantageReports]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbRoomListStaging]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbRoomListStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbRoomListStaging](
	[LastName] [varchar](100) NULL,
	[FirstName] [varchar](100) NULL,
	[BadgeName] [varchar](100) NULL,
	[Area] [varchar](100) NULL,
	[PrimaryReg] [varchar](100) NULL,
	[Guest] [varchar](100) NULL,
	[CheckIn] [datetime] NULL,
	[CheckOut] [datetime] NULL,
	[RoomType] [varchar](100) NULL,
	[RoomQuality] [varchar](100) NULL,
	[BillingInstructions] [varchar](1000) NULL,
	[Smoking] [varchar](100) NULL,
	[SpecialRequests] [varchar](1000) NULL,
	[RoomNotes] [varchar](500) NULL,
	[ProgramName] [varchar](200) NULL,
	[DKNumber] [varchar](20) NULL,
	[ProgramDates] [varchar](100) NULL,
	[HotelName] [varchar](100) NULL,
	[District] [varchar](100) NULL,
	[Region] [varchar](100) NULL,
	[Title] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF