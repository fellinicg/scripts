USE OnvantageReports
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060915
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upGetActivitiesForDK
(
	@DKNumber varchar(20),
	@Report varchar(100)
)
AS

	-- Return data
	IF (@Report = 'upActivityRoster')
		SELECT DISTINCT
			Activity
		FROM dbo.tbActivityRoster
		WHERE DKNumber = @DKNumber
		ORDER BY Activity

	IF (@Report = 'upGolf')
		SELECT DISTINCT
			Activity
		FROM dbo.tbGolf
		WHERE DKNumber = @DKNumber
		ORDER BY Activity


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
GRANT EXECUTE
ON dbo.upGetActivitiesForDK
TO system