USE OnvantageReports
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060919
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upPaxList
(
	@DKNumber varchar(20)
)
AS

	-- Return data
	SELECT 
		WinnerLast AS WinnerLast,
		WinnerFirst AS WinnerFirst,
		WinnerBadge AS WinnerBadge,
		Area AS Area,
		District AS District, 
		Region AS Region, 
		Title AS Title, 
		GuestLast AS GuestLast,
		GuestFirst AS GuestFirst,
		GuestBadge AS GuestBadge,
		Relation AS Relation,
		Comments AS Comments,
		ProgramName AS ProgramName,
		DKNumber AS DKNumber,
		ProgramDates AS ProgramDates
	FROM dbo.tbPaxList
	WHERE DKNumber = @DKNumber
	ORDER BY WinnerLast, WinnerFirst

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
GRANT EXECUTE
ON dbo.upPaxList
TO system