USE OnvantageReports
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060915
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upGetProgramsByReport
(
	@ExecProc varchar(40)
)
AS

	DECLARE @ReportTable	varchar(40)

	-- Initialize Variables
	SELECT @ReportTable = ReportTable
	FROM dbo.tbReport
	WHERE ExecProc = @ExecProc

	-- Return data
	EXEC ('SELECT DISTINCT 
					DKNumber,
					ProgramName
				FROM dbo.' + @ReportTable + ' 
				ORDER BY ProgramName')

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
GRANT EXECUTE
ON dbo.upGetProgramsByReport
TO system