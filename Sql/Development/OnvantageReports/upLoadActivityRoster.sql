USE OnvantageReports
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060915
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upLoadActivityRoster 
(
	@InputFile VARCHAR(50),
	@Development BIT = 0,
	@Result CHAR(1) OUTPUT
)
AS

	DECLARE @TabName VARCHAR(50)
	DECLARE @bcpcmd VARCHAR(5000)
	DECLARE @timestamp VARCHAR(20)
	
	-- Initialize variables
	SET @TabName = RTRIM(LEFT(@InputFile, CHARINDEX('.', @InputFile) - 1))
	SET @timestamp = CONVERT(VARCHAR, CURRENT_TIMESTAMP, 112) + '_' + CONVERT(VARCHAR, DATEPART(hh, CURRENT_TIMESTAMP)) + 'h' + CONVERT(VARCHAR, DATEPART(mi, CURRENT_TIMESTAMP)) + 'm' + CONVERT(VARCHAR, DATEPART(ss, CURRENT_TIMESTAMP)) + 's'
	SET @Result = 'F'

	-- Map drive
	IF (@Development = 1)
		SET @bcpcmd = 'net use z: /delete&net use z: \\TODD\OnvantageReports$\SourceFiles'
	ELSE
		SET @bcpcmd = 'net use z: /delete&net use z: \\web2000dev\OnvantageReports$\SourceFiles'
	EXECUTE master.dbo.xp_cmdshell @bcpcmd
	
	-- Copy file to SQL server
	SET @bcpcmd = 'copy z:\' + @InputFile + ' d:\' + @InputFile
	EXECUTE master.dbo.xp_cmdshell @bcpcmd

	-- Clear table
	TRUNCATE TABLE dbo.tbActivityRosterStaging
	
	-- Import file
	EXEC ('INSERT INTO dbo.tbActivityRosterStaging
	select * FROM OPENROWSET(''Microsoft.Jet.OLEDB.4.0'', 
	''Excel 8.0;DATABASE=d:\' + @InputFile + ''', ''select * from [' + @TabName + '$]'')')

	-- Make sure records where imported
	IF (@@ROWCOUNT < 1)	
		GOTO CleanUp

	-- Archive files
	SET @bcpcmd = 'move z:\' + @InputFile + ' z:\Archive\' + @timestamp + '_' + @InputFile
	EXECUTE master.dbo.xp_cmdshell @bcpcmd
	
	-- Delete records in tbActivityRoster for this DKNumber
	DELETE FROM dbo.tbActivityRoster
	WHERE DKNumber = (SELECT TOP 1 DKNumber FROM dbo.tbActivityRosterStaging)

	-- Insert records into ActivityRoster table
	INSERT INTO dbo.tbActivityRoster
	SELECT 
		ProgramName, 
		DKNumber, 
		ProgramDates,
		ISNULL(District, ''), 
		ISNULL(Region, ''), 
		ISNULL(Title, ''), 
		ISNULL(FirstName, ''), 
		ISNULL(LastName, ''), 
		ISNULL(BadgeName, ''), 
		PaxID, 
		PrimaryID, 
		Area, 
		ISNULL(CASE PaxType WHEN 'Primary' THEN 'P' WHEN 'Guest' THEN 'G' END, ''),
		ISNULL(LTRIM(REPLACE(ActivityGroup, 'Activity Selection for ', '')), ''),
		ISNULL(Activity, ''), 
		StartDate, 
		EndDate
	FROM dbo.tbActivityRosterStaging
	WHERE StartDate IS NOT NULL OR EndDate IS NOT NULL

	-- Make sure records where imported
	IF (@@ROWCOUNT < 1)	
		GOTO CleanUp

	-- Success
	SET @Result = 'S'

CleanUp:
	-- Remove mapped drive
	EXECUTE master.dbo.xp_cmdshell 'net use z: /delete'
	
	-- Delete file from SQL server
	SET @bcpcmd = 'del d:\' + @InputFile
	EXECUTE master.dbo.xp_cmdshell @bcpcmd

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
GRANT EXECUTE
ON dbo.upLoadActivityRoster
TO system