USE [OnvantageReports]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbGolf]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbGolf]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbGolf](

	[LastName] [varchar](100) NOT NULL,
	[FirstName] [varchar](100) NOT NULL,
	[PaxType] [char](1) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[Activity] [varchar](100) NOT NULL,
	[Handicap] [varchar](100) NOT NULL,
	[ClubRental] [varchar](100) NOT NULL,
	[ShoeRental] [varchar](100) NOT NULL,
	[ShoeSize] [varchar](100) NOT NULL,
	[Comments] [varchar](100) NOT NULL,
	[Area] [varchar](100) NOT NULL,
	[District] [varchar](100) NOT NULL,
	[Region] [varchar](100) NOT NULL,
	[Title] [varchar](100) NOT NULL,
	[ProgramName] [varchar](200) NOT NULL,
	[DKNumber] [varchar](20) NOT NULL,
	[ProgramDates] [varchar](100) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
GRANT SELECT
ON dbo.tbGolf
TO system