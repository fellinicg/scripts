USE [OnvantageReports]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbPaxList]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbPaxList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbPaxList](
	[WinnerLast] [varchar](100) NOT NULL,
	[WinnerFirst] [varchar](100) NOT NULL,
	[WinnerBadge] [varchar](100) NOT NULL,
	[Area] [varchar](100) NOT NULL,
	[District] [varchar](100) NOT NULL,
	[Region] [varchar](100) NOT NULL,
	[Title] [varchar](100) NOT NULL,
	[GuestLast] [varchar](100) NOT NULL,
	[GuestFirst] [varchar](100) NOT NULL,
	[GuestBadge] [varchar](100) NOT NULL,
	[Relation] [varchar](100) NOT NULL,
	[Comments] [varchar](500) NOT NULL,
	[ProgramName] [varchar](200) NOT NULL,
	[DKNumber] [varchar](20) NOT NULL,
	[ProgramDates] [varchar](100) NOT NULL,
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
GRANT SELECT
ON dbo.tbPaxList
TO system