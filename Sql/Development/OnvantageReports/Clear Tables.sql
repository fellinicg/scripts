USE OnvantageReports
TRUNCATE TABLE dbo.tbActivityRoster
TRUNCATE TABLE dbo.tbActivityRosterStaging
TRUNCATE TABLE dbo.tbEventSchedule
TRUNCATE TABLE dbo.tbEventScheduleStaging
TRUNCATE TABLE dbo.tbGolf
TRUNCATE TABLE dbo.tbGolfStaging
TRUNCATE TABLE dbo.tbPaxList
TRUNCATE TABLE dbo.tbPaxListStaging
TRUNCATE TABLE dbo.tbRoomList
TRUNCATE TABLE dbo.tbRoomListStaging
