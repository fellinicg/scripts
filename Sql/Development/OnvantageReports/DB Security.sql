USE OnvantageReports
GO

-- Create SQL Login
EXEC sp_addlogin @loginame    = N'sys_onvantage',
	    @passwd      = '3KHJ6dhx',
	    @defdb       = N'OnvantageReports',
	    @deflanguage = N'English'
GO

-- Grant dbaccess to login
EXEC sp_grantdbaccess @loginame   = N'sys_onvantage', 
		 @name_in_db = N'sys_onvantage'
GO

-- Add an existing user to the role
EXEC sp_addrolemember N'system', N'sys_onvantage'
GO


 