USE [OnvantageReports]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbRoomList]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbRoomList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbRoomList](
	[LastName] [varchar](100) NOT NULL,
	[FirstName] [varchar](100) NOT NULL,
	[BadgeName] [varchar](100) NOT NULL,
	[Area] [varchar](100) NOT NULL,
	[Region] [varchar](100) NOT NULL,
	[District] [varchar](100) NULL,
	[Title] [varchar](100) NULL,
	[Guest] [varchar](100) NOT NULL,
	[CheckIn] [datetime] NOT NULL,
	[CheckOut] [datetime] NOT NULL,
	[RoomType] [varchar](100) NOT NULL,
	[RoomQuality] [varchar](100) NOT NULL,
	[BillingInstructions] [varchar](1000) NOT NULL,
	[Smoking] [varchar](100) NOT NULL,
	[SpecialRequests] [varchar](1000) NOT NULL,
	[RoomNotes] [varchar](500) NOT NULL,
	[DKNumber] [varchar](20) NOT NULL,
	[ProgramName] [varchar](200) NOT NULL,
	[ProgramDates] [varchar](100) NOT NULL,
	[HotelName] [varchar](100) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
GRANT SELECT
ON dbo.tbRoomList
TO system