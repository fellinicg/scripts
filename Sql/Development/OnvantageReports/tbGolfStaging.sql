USE [OnvantageReports]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbGolfStaging]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbGolfStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbGolfStaging](
	[LastName] [varchar](100) NULL,
	[FirstName] [varchar](100) NULL,
	[Area] [varchar](100) NULL,
	[PaxType] [varchar](100) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Activity] [varchar](100) NULL,
	[Handicap] [varchar](100) NULL,
	[ClubRental] [varchar](100) NULL,
	[ShoeRental] [varchar](100) NULL,
	[ShoeSize] [varchar](100) NULL,
	[Comments] [varchar](100) NULL,
	[District] [varchar](100) NULL,
	[Region] [varchar](100) NULL,
	[Title] [varchar](100) NULL,
	[ProgramName] [varchar](200) NULL,
	[DKNumber] [varchar](20) NULL,
	[ProgramDates] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF