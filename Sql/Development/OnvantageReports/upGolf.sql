USE OnvantageReports
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060925
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upGolf
(
	@DKNumber int,
	@Activity varchar(100),
	@StartDate datetime
)
AS

	-- Return data
	SELECT 
		LastName AS LastName,
		FirstName AS FirstName,
		PaxType AS PaxType,
		StartDate AS StartDate,
		EndDate AS EndDate,
		Activity AS Activity,
		Handicap AS Handicap,
		ClubRental AS ClubRental,
		ShoeRental AS ShoeRental,
		ShoeSize AS ShoeSize,
		Area AS Area, 
		Region AS Region, 
		District AS District, 
		Title AS Title, 
		Comments AS Comments,
		DKNumber AS DKNumber,
		ProgramName AS ProgramName
	FROM dbo.tbGolf
	WHERE DKNumber = @DKNumber
		AND Activity = @Activity
		AND StartDate = @StartDate
	ORDER BY LastName ASC, PaxType DESC

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
GRANT EXECUTE
ON dbo.upGolf
TO system