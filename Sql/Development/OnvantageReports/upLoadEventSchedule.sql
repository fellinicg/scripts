USE OnvantageReports
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060919
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upLoadEventSchedule
(
	@InputFile VARCHAR(50),
	@Development BIT = 0,
	@Result CHAR(1) OUTPUT
)
AS

	DECLARE @TabName VARCHAR(50)
	DECLARE @bcpcmd VARCHAR(5000)
	DECLARE @timestamp VARCHAR(20)
	
	-- Initialize variables
	SET @TabName = RTRIM(LEFT(@InputFile, CHARINDEX('.', @InputFile) - 1))
	SET @timestamp = CONVERT(VARCHAR, CURRENT_TIMESTAMP, 112) + '_' + CONVERT(VARCHAR, DATEPART(hh, CURRENT_TIMESTAMP)) + 'h' + CONVERT(VARCHAR, DATEPART(mi, CURRENT_TIMESTAMP)) + 'm' + CONVERT(VARCHAR, DATEPART(ss, CURRENT_TIMESTAMP)) + 's'
	SET @Result = 'F'

	-- Map drive
	IF (@Development = 1)
		SET @bcpcmd = 'net use z: /delete&net use z: \\TODD\OnvantageReports$\SourceFiles'
	ELSE
		SET @bcpcmd = 'net use z: /delete&net use z: \\web2000dev\OnvantageReports$\SourceFiles'
	EXECUTE master.dbo.xp_cmdshell @bcpcmd
	
	-- Copy file to SQL server
	SET @bcpcmd = 'copy z:\' + @InputFile + ' d:\' + @InputFile
	EXECUTE master.dbo.xp_cmdshell @bcpcmd

	-- Clear table
	TRUNCATE TABLE dbo.tbEventScheduleStaging
	
	-- Import file
	EXEC ('INSERT INTO dbo.tbEventScheduleStaging
	SELECT * FROM OPENROWSET(''Microsoft.Jet.OLEDB.4.0'', 
	''Excel 8.0;DATABASE=d:\' + @InputFile + ''', ''select * from [' + @TabName + '$]'')')

	-- Make sure records where imported
	IF (@@ROWCOUNT < 1)	
		GOTO CleanUp

	-- Archive files
	SET @bcpcmd = 'move z:\' + @InputFile + ' z:\Archive\' + @timestamp + '_' + @InputFile
	EXECUTE master.dbo.xp_cmdshell @bcpcmd
	
	-- Delete records in tbEventSchedule for this DKNumber
	DELETE FROM dbo.tbEventSchedule
	WHERE DKNumber = (SELECT TOP 1 DKNumber FROM dbo.tbEventScheduleStaging)

	-- Insert records into tbEventSchedule table
	INSERT INTO dbo.tbEventSchedule
	SELECT 
		(SELECT DATENAME(MM, MIN(ActivityDate)) + ' ' + DATENAME(DD, MIN(ActivityDate)) + ' - ' + DATENAME(MM, MAX(ActivityDate)) + ' ' + DATENAME(DD, MAX(ActivityDate)) + ', ' + DATENAME(YYYY, MAX(ActivityDate)) FROM tbEventScheduleStaging) AS ProgramDates,
		ActivityDate, 
		ActivityTime, 
		ActivityName, 
		ProgramName, 
		ISNULL(Capacity, 'N/A'), 
		AttendeeCount, 
		ISNULL(CONVERT(varchar, CONVERT(int, Capacity) - CONVERT(int, AttendeeCount)), 'N/A'),
		DKNumber, 
		Comments
	FROM dbo.tbEventScheduleStaging
	WHERE ActivityDate IS NOT NULL
		AND ActivityTime IS NOT NULL

	-- Make sure records where imported
	IF (@@ROWCOUNT < 1)	
		GOTO CleanUp

	-- Success
	SET @Result = 'S'

CleanUp:
	-- Remove mapped drive
	EXECUTE master.dbo.xp_cmdshell 'net use z: /delete'
	
	-- Delete file from SQL server
	SET @bcpcmd = 'del d:\' + @InputFile
	EXECUTE master.dbo.xp_cmdshell @bcpcmd

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
GRANT EXECUTE
ON dbo.upLoadEventSchedule
TO system