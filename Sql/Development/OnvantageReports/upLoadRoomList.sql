USE OnvantageReports
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060920
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upLoadRoomList
(
	@InputFile VARCHAR(50),
	@Development BIT = 0,
	@Result CHAR(1) OUTPUT
)
AS

	DECLARE @TabName VARCHAR(50)
	DECLARE @bcpcmd VARCHAR(5000)
	DECLARE @timestamp VARCHAR(20)
	
	-- Initialize variables
	SET @TabName = RTRIM(LEFT(@InputFile, CHARINDEX('.', @InputFile) - 1))
	SET @timestamp = CONVERT(VARCHAR, CURRENT_TIMESTAMP, 112) + '_' + CONVERT(VARCHAR, DATEPART(hh, CURRENT_TIMESTAMP)) + 'h' + CONVERT(VARCHAR, DATEPART(mi, CURRENT_TIMESTAMP)) + 'm' + CONVERT(VARCHAR, DATEPART(ss, CURRENT_TIMESTAMP)) + 's'
	SET @Result = 'F'

	-- Map drive
	IF (@Development = 1)
		SET @bcpcmd = 'net use z: /delete&net use z: \\TODD\OnvantageReports$\SourceFiles'
	ELSE
		SET @bcpcmd = 'net use z: /delete&net use z: \\web2000dev\OnvantageReports$\SourceFiles'
	EXECUTE master.dbo.xp_cmdshell @bcpcmd
	
	-- Copy file to SQL server
	SET @bcpcmd = 'copy z:\' + @InputFile + ' d:\' + @InputFile
	EXECUTE master.dbo.xp_cmdshell @bcpcmd

	-- Clear table
	TRUNCATE TABLE dbo.tbRoomListStaging
	
	-- Import file
	EXEC ('INSERT INTO dbo.tbRoomListStaging
	SELECT * FROM OPENROWSET(''Microsoft.Jet.OLEDB.4.0'', 
	''Excel 8.0;DATABASE=d:\' + @InputFile + ''', ''select * from [' + @TabName + '$]'')')

	-- Make sure records where imported
	IF (@@ROWCOUNT < 1)	
		GOTO CleanUp

	-- Archive files
	SET @bcpcmd = 'move z:\' + @InputFile + ' z:\Archive\' + @timestamp + '_' + @InputFile
	EXECUTE master.dbo.xp_cmdshell @bcpcmd
	
	-- Delete records in tbRoomList for this DKNumber
	DELETE FROM dbo.tbRoomList
	WHERE DKNumber = (SELECT TOP 1 DKNumber FROM dbo.tbRoomListStaging)

	-- Insert records into tbRoomList table
	INSERT INTO dbo.tbRoomList
	SELECT
		ISNULL(LastName, ''), 
		ISNULL(FirstName, ''), 
		ISNULL(BadgeName, ''), 
		ISNULL(Area, ''), 
		ISNULL(Region, ''), 
		ISNULL(District, ''), 
		ISNULL(Title, ''), 
		ISNULL(Guest, ''), 
		ISNULL(CheckIn, ''), 
		ISNULL(CheckOut, ''), 
		ISNULL(RoomType, ''), 
		ISNULL(RoomQuality, ''), 
		ISNULL(BillingInstructions, ''), 
		ISNULL(Smoking, ''), 
		ISNULL(SpecialRequests, ''), 
		ISNULL(RoomNotes, ''), 
		ISNULL(DKNumber, ''), 
		ISNULL(ProgramName, ''), 
		ISNULL(ProgramDates, ''), 
		ISNULL(HotelName, '')
	FROM dbo.tbRoomListStaging

	-- Make sure records where imported
	IF (@@ROWCOUNT < 1)	
		GOTO CleanUp

	-- Success
	SET @Result = 'S'

CleanUp:
	-- Remove mapped drive
	EXECUTE master.dbo.xp_cmdshell 'net use z: /delete'
	
	-- Delete file from SQL server
	SET @bcpcmd = 'del d:\' + @InputFile
	EXECUTE master.dbo.xp_cmdshell @bcpcmd

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
GRANT EXECUTE
ON dbo.upLoadRoomList
TO system