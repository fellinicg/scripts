USE [OnvantageReports]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbActivityRosterStaging]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbActivityRosterStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbActivityRosterStaging](
	[ProgramName] [varchar](200) NULL,
	[DKNumber] [varchar](20) NULL,
	[ProgramDates] [varchar](100) NULL,
	[District] [varchar](100) NULL,
	[Region] [varchar](100) NULL,
	[Title] [varchar](100) NULL,
	[FirstName] [varchar](100) NULL,
	[LastName] [varchar](100) NULL,
	[BadgeName] [varchar](100) NULL,
	[PaxID] [int] NULL,
	[PrimaryID] [int] NULL,
	[Area] [varchar](200) NULL,
	[GuestOf] [varchar](100) NULL,
	[PaxType] [varchar](30) NULL,
	[ActivityGroup] [varchar](500) NULL,
	[Activity] [varchar](500) NULL,
	[StartDate] [smalldatetime] NULL,
	[EndDate] [smalldatetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF