USE [OnvantageReports]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbPaxListStaging]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbPaxListStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbPaxListStaging](
	[PrimaryReg] [varchar](10) NULL,
	[LastName] [varchar](100) NULL,
	[FirstName] [varchar](100) NULL,
	[BadgeName] [varchar](100) NULL,
	[Area] [varchar](100) NULL,
	[Relation] [varchar](100) NULL,
	[TransactionID] [int] NULL,
	[PaxID] [int] NULL,
	[ProgramName] [varchar](200) NULL,
	[DKNumber] [varchar](20) NULL,
	[ProgramDates] [varchar](100) NULL,
	[District] [varchar](100) NULL,
	[Region] [varchar](100) NULL,
	[Title] [varchar](100) NULL,
	[Comments] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF