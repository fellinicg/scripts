USE OnvantageReports
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20060928
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upActivities
(
	@DKNumber varchar(20)
)
AS

	DECLARE @NumActivities int
	DECLARE @CurrActivity varchar(2)
	DECLARE @Sql varchar(8000)
	DECLARE @RowID int
	DECLARE @PaxID int
	DECLARE @CurrPaxID int
	DECLARE @WinnerLast varchar(100)
	DECLARE @WinnerFirst varchar(100)
	DECLARE @WinnerBadge varchar(100)
	DECLARE @Area varchar(100)
	DECLARE @Region varchar(100)
	DECLARE @District varchar(100)
	DECLARE @Title varchar(100)
	DECLARE @WinnerActivity varchar(100)
	DECLARE @WinnerDate varchar(100)
	DECLARE @WinnerTime varchar(100)
	DECLARE @GuestLast varchar(100)
	DECLARE @GuestFirst varchar(100)
	DECLARE @GuestBadge varchar(100)
	DECLARE @GuestActivity varchar(100)
	DECLARE @GuestDate varchar(100)
	DECLARE @GuestTime varchar(100)

	SET NOCOUNT ON

	-- Create temporary table
	CREATE TABLE #Report (
		PaxID int,
		WinnerLastName varchar(100),
		WinnerFirstName varchar (100),
		WinnerBadgeName varchar(100),
		Area varchar(100),
		Region varchar (100),
		District varchar(100),
		Title varchar(100))

	-- Get maximum number of activities for the report
	SELECT TOP 1 @NumActivities = COUNT(Activity)
	FROM dbo.tbActivityRoster
	WHERE DKNumber = @DKNumber
	GROUP BY PaxID
	ORDER BY COUNT(Activity) DESC

	-- Build SQL to alter temporary table to add all winner columns
	SET @CurrActivity = 1
	SET @Sql = 'ALTER TABLE #Report ADD '
	WHILE @CurrActivity <= @NumActivities
		BEGIN
			-- Set SQL
			SET @Sql = @Sql + 'WinnerDate' + @CurrActivity + ' varchar(100), '
			SET @Sql = @Sql + 'WinnerTime' + @CurrActivity + ' varchar(100), '
			SET @Sql = @Sql + 'WinnerActivity' + @CurrActivity + ' varchar(100), '
			SET @Sql = @Sql + 'WinnerNotes' + @CurrActivity + ' varchar(100), '

			-- Increment counter
			SET @CurrActivity = @CurrActivity + 1
		END

	-- Build SQL to alter temporary table to add all winner columns
	SET @CurrActivity = 1
	SET @Sql = @Sql + 'GuestLastName varchar(100), '
	SET @Sql = @Sql + 'GuestFirstName varchar (100), '
	SET @Sql = @Sql + 'GuestBadgeName varchar(100), '

	WHILE @CurrActivity <= @NumActivities
		BEGIN
			-- Set SQL
			SET @Sql = @Sql + 'GuestDate' + @CurrActivity + ' varchar(100), '
			SET @Sql = @Sql + 'GuestTime' + @CurrActivity + ' varchar(100), '
			SET @Sql = @Sql + 'GuestActivity' + @CurrActivity + ' varchar(100), '
			SET @Sql = @Sql + 'GuestNotes' + @CurrActivity + ' varchar(100), '

			-- Increment counter
			SET @CurrActivity = @CurrActivity + 1
		END

	-- Remove trailing comma
	SET @Sql = LEFT(@Sql, LEN(@Sql)-1)

	-- Execute SQL statement
	EXECUTE(@Sql)

	-- Create temporary table
	CREATE TABLE #Activities (
		RowID int identity,
		PaxID int,
		WinnerLast varchar(100),
		WinnerFirst varchar(100),
		WinnerBadge varchar(100),
		Area varchar(100),
		Region varchar (100),
		District varchar(100),
		Title varchar(100),
		WinnerActivity varchar(100),
		WinnerStartDate datetime,
		WinnerEndDate datetime,
		GuestLast varchar(100),
		GuestFirst varchar(100),
		GuestBadge varchar(100),
		GuestActivity varchar(100),
		GuestStartDate datetime,
		GuestEndDate datetime)
	
	-- Store data from tbActivityRoster in temporary table
	INSERT INTO #Activities (PaxID,WinnerLast,WinnerFirst,WinnerBadge,Area,Region,District,Title,
													WinnerActivity,WinnerStartDate,WinnerEndDate,GuestLast,
													GuestFirst,GuestBadge,GuestActivity,GuestStartDate,GuestEndDate)
	SELECT
		a.PaxID,
		a.LastName,
		a.FirstName,
		a.BadgeName,
		a.Area,
		a.Region,
		a.District,
		a.Title,
		a.Activity,
		a.StartDate,
		a.EndDate,
		b.LastName,
		b.FirstName,
		b.BadgeName,
		b.Activity,
		b.StartDate,
		b.EndDate
	FROM dbo.tbActivityRoster a
	LEFT JOIN (SELECT
							PaxID,
							LastName,
							FirstName,
							BadgeName,
							Activity,
							StartDate,
							EndDate,
							PrimaryID,
							DKNumber,
							Activitygroup
						FROM dbo.tbActivityRoster
						WHERE PaxType = 'G') b ON a.PaxID = b.PrimaryID AND a.ActivityGroup = b.ActivityGroup AND a.DKNumber = b.DKNumber
	WHERE a.PaxType = 'P'
		AND a.DKNumber = @DKNumber
	ORDER BY a.PaxID, a.StartDate

	-- Loop over #Activities and populate #Report with data
	SET @CurrActivity = 1
	SET @PaxID = 0
	WHILE EXISTS (SELECT 1 FROM #Activities)
		BEGIN
			-- Get Row to work on
			SELECT @RowID = MIN(RowID) FROM #Activities

			-- Get data
			SELECT 
				@CurrPaxID = PaxID,
				@WinnerLast = WinnerLast,
				@WinnerFirst = WinnerFirst,
				@WinnerBadge = WinnerBadge,
				@Area = Area,
				@Region = Region,
				@District = District,
				@Title = Title,
				@WinnerActivity = REPLACE(ISNULL(WinnerActivity, ''), CHAR(39), CHAR(39) + Char(39)),
				@WinnerDate = ISNULL(CONVERT(varchar, WinnerStartDate, 101), ''),
				@WinnerTime = ISNULL(LTRIM(RIGHT(CONVERT(varchar, WinnerStartDate), 7)) + ' - ' + LTRIM(RIGHT(CONVERT(varchar, WinnerEndDate), 7)), ''),
				@GuestLast = REPLACE(GuestLast, CHAR(39), CHAR(39) + Char(39)),
				@GuestFirst = REPLACE(GuestFirst, CHAR(39), CHAR(39) + Char(39)),
				@GuestBadge = REPLACE(GuestBadge, CHAR(39), CHAR(39) + Char(39)),
				@GuestActivity = REPLACE(ISNULL(GuestActivity, ''), CHAR(39), CHAR(39) + Char(39)),
				@GuestDate = ISNULL(CONVERT(varchar, GuestStartDate, 101), ''),
				@GuestTime = ISNULL(LTRIM(RIGHT(CONVERT(varchar, GuestStartDate), 7)) + ' - ' + LTRIM(RIGHT(CONVERT(varchar, GuestEndDate), 7)), '')
			FROM #Activities 
			WHERE RowID = @RowID

			-- Insert data if current pax is not the same as the last pax we processed
			IF (@PaxID <> @CurrPaxID)
				INSERT INTO #Report (PaxID, WinnerLastName, WinnerFirstName, WinnerBadgeName, Area, Region, District, Title, GuestLastName, GuestFirstName, GuestBadgeName)
				VALUES (@CurrPaxID, @WinnerLast, @WinnerFirst, @WinnerBadge, @Area, @Region, @District, @Title, ISNULL(@GuestLast, ''), ISNULL(@GuestFirst, ''), ISNULL(@GuestBadge, ''))
			
			-- Update Guest name info
			UPDATE #Report SET GuestLastName = ISNULL(@GuestLast, GuestLastName), GuestFirstName = ISNULL(@GuestFirst, GuestFirstName), GuestBadgeName = ISNULL(@GuestBadge, GuestBadgeName)
			WHERE PaxID = @CurrPaxID

			-- Update activity info
			SET @Sql = 'UPDATE #Report SET WinnerDate' + @CurrActivity + '=''' + @WinnerDate + ''','
				+ 'WinnerTime' + @CurrActivity + '=''' + @WinnerTime + ''','
				+ 'WinnerActivity' + @CurrActivity + '=''' + @WinnerActivity + ''','
				+ 'WinnerNotes' + @CurrActivity + '=' + CHAR(39) + Char(39) + ','
				+ 'GuestDate' + @CurrActivity + '=''' + @GuestDate + ''','
				+ 'GuestTime' + @CurrActivity + '=''' + @GuestTime + ''','
				+ 'GuestActivity' + @CurrActivity + '=''' + @GuestActivity + ''','
				+ 'GuestNotes' + @CurrActivity + '=' + CHAR(39) + Char(39) + ' '
				+ 'WHERE PaxID = ' + CONVERT(varchar, @CurrPaxID)
			EXECUTE (@Sql)

			-- Get PaxID for working row
			SELECT @PaxID = PaxID FROM #Activities WHERE RowID = @RowID

			-- Delete Row just worked on
			DELETE FROM #Activities WHERE RowID = @RowID

			-- Adjust counter (Increment or Reset)
			IF (@CurrActivity < @NumActivities)
				SET @CurrActivity = @CurrActivity + 1
			ELSE
				SET @CurrActivity = 1
	
		END

	-- Return data
	SELECT * FROM #Report
	ORDER BY WinnerLastName, WinnerFirstName

	-- Cleanup
	DROP TABLE #Activities
	DROP TABLE #Report

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
GRANT EXECUTE
ON dbo.upActivities
TO system