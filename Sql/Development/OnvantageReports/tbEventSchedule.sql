USE [OnvantageReports]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbEventSchedule]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbEventSchedule]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbEventSchedule](
	[ProgramDates] [varchar](50) NOT NULL,
	[ActivityDate] [datetime] NOT NULL,
	[ActivityTime] [varchar](50) NOT NULL,
	[ActivityName] [varchar](100) NOT NULL,
	[ProgramName] [varchar](100) NOT NULL,
	[Capacity][varchar](10) NOT NULL,
	[AttendeeCount] [varchar](10) NOT NULL,
	[Available] [varchar](10) NOT NULL,
	[DKNumber] [varchar](20) NOT NULL,
	[Comments] [varchar](500) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
GRANT SELECT
ON dbo.tbEventSchedule
TO system