DELETE FROM [CMZebra].[dbo].[TemplatePage]

DELETE FROM [CMZebra].[dbo].[ProgramCategory]

DELETE FROM [CMZebra].[dbo].[BlockObject]

DELETE FROM [CMZebra].[dbo].[ObjectScheme]

DELETE FROM [CMZebra].[dbo].[ObjectHistory]

DELETE FROM [CMZebra].[dbo].[ObjectProperty]

DELETE FROM [CMZebra].[dbo].[ObjectTypeAreaBlock]

DELETE FROM [CMZebra].[dbo].[ObjectTypeArea]

DELETE FROM [CMZebra].[dbo].[ObjectTypeBlock]

DELETE FROM [CMZebra].[dbo].[ObjectTypePage]

DELETE FROM [CMZebra].[dbo].[ObjectTypeSite]

DELETE FROM [CMZebra].[dbo].[Object]

DELETE FROM [CMZebra].[dbo].[ObjectType]

DELETE FROM [CMZebra].[dbo].[Role]

DELETE FROM [CMZebra].[dbo].[RolePermission]

DELETE FROM [CMZebra].[dbo].[RolePermissionType]

DELETE FROM [CMZebra].[dbo].[OrganizationLevel]

DELETE FROM [CMZebra].[dbo].[Organization]

DELETE FROM [CMZebra].[dbo].[Client]

DELETE FROM [CMZebra].[dbo].[Person]

DELETE FROM [CMZebra].[dbo].[ContactType]

DELETE FROM [CMZebra].[dbo].[User]

DELETE FROM [CMZebra].[dbo].[CachedUserRoles]
