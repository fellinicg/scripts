USE [CMZebra]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[BlockObjectHistory]    Script Date: 09/28/2005 14:15:05 ******/
CREATE TABLE [dbo].[BlockObjectHistory](
	[ID] [int] NOT NULL,
	[ObjectID] [int] NULL,
	[ObjectTypeBlockID] [int] NULL,
	[DisplayOrder] [int] NULL,
	[UpdateStamp] [bigint] NOT NULL,
 CONSTRAINT [PK_sBlockObject] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BlockObjectDeletionHistory]    Script Date: 09/28/2005 14:17:52 ******/
CREATE TABLE [dbo].[BlockObjectDeletionHistory](
	[ID] [int] NOT NULL,
	[ObjectID] [int] NOT NULL,
	[ObjectTypeBlockID] [int] NOT NULL,
	[DisplayOrder] [int] NULL,
	[DeletionDate] [datetime] NOT NULL,
	[UpdateStamp] [bigint] NOT NULL,
 CONSTRAINT [PK_BlockObjectDeletionHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BlockObject]    Script Date: 09/28/2005 14:19:09 ******/
CREATE TABLE [dbo].[BlockObject](
	[ID] [int] NOT NULL,
	[ObjectID] [int] NOT NULL,
	[ObjectTypeBlockID] [int] NOT NULL,
	[DisplayOrder] [int] NULL,
	[UpdateStamp] [bigint] NOT NULL,
 CONSTRAINT [PK_ObjectBlock] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContentHistory]    Script Date: 09/28/2005 14:21:09 ******/
CREATE TABLE [dbo].[ContentHistory](
	[ID] [int] NOT NULL,
	[SiteID] [int] NOT NULL,
	[ObjectID] [int] NOT NULL,
	[PropertyID] [int] NOT NULL,
	[ApprovalID] [int] NOT NULL,
	[OriginalValue] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NewValue] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UpdateStamp] [bigint] NOT NULL,
 CONSTRAINT [PK_ContentHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Budget]    Script Date: 09/28/2005 14:22:28 ******/
CREATE TABLE [dbo].[Budget](
	[ID] [int] NOT NULL,
	[Name] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ProgramID] [int] NOT NULL,
	[OwnerID] [int] NULL,
	[Amount] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[UpdateStamp] [bigint] NOT NULL,
 CONSTRAINT [PK_Budget] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BudgetAllocation]    Script Date: 09/28/2005 14:23:20 ******/
CREATE TABLE [dbo].[BudgetAllocation](
	[ID] [int] NOT NULL,
	[BudgetID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[Amount] [int] NULL,
	[ModifiedByID] [int] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[UpdateStamp] [bigint] NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__BudgetAll__Descr__52050254]  DEFAULT (''),
 CONSTRAINT [PK_BudgetAllocation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PointBoundary]    Script Date: 09/28/2005 14:25:02 ******/
CREATE TABLE [dbo].[PointBoundary](
	[ID] [int] NOT NULL,
	[ClientID] [int] NOT NULL,
	[ProgramTypeID] [int] NULL,
	[ProgramID] [int] NULL,
	[RoleID] [int] NULL,
	[Minimum] [int] NULL,
	[Maximum] [int] NULL,
	[UpdateStamp] [bigint] NOT NULL,
 CONSTRAINT [PK_PointBoundary] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProgramScheme]    Script Date: 09/28/2005 14:25:43 ******/
CREATE TABLE [dbo].[ProgramScheme](
	[ID] [int] NOT NULL,
	[ProgramID] [int] NOT NULL,
	[Name] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[RecipientRoleID] [int] NULL,
	[RequestingRoleID] [int] NULL,
	[ApprovingRoleID] [int] NULL,
	[AccessStatus] [int] NULL,
	[ApprovalStatus] [int] NULL,
	[MinAmount] [int] NULL,
	[MaxAmount] [int] NULL,
	[UpdateStamp] [bigint] NOT NULL,
 CONSTRAINT [PK_ProgramScheme] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Object]    Script Date: 09/28/2005 14:37:48 ******/
CREATE TABLE [dbo].[Object](
	[ID] [int] NOT NULL,
	[ObjectGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Object_ObjectGUID]  DEFAULT (newid()),
	[ParentID] [int] NULL,
	[ObjectTypeID] [int] NULL,
	[Name] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[PendingApproval] [tinyint] NULL CONSTRAINT [DF_Object_PendingDelete]  DEFAULT (0),
	[Deleted] [tinyint] NULL CONSTRAINT [DF_Object_Deleted]  DEFAULT (0),
	[LastModified] [datetime] NULL,
	[UpdateStamp] [bigint] NOT NULL,
 CONSTRAINT [PK_Object] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ObjectApprovalHistory]    Script Date: 09/28/2005 14:38:40 ******/
CREATE TABLE [dbo].[ObjectApprovalHistory](
	[ID] [int] NOT NULL,
	[ObjectHistoryID] [int] NOT NULL,
	[PreviousStatus] [int] NULL,
	[CurrentStatus] [int] NULL,
	[ModifiedByID] [int] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[UpdateStamp] [bigint] NOT NULL,
 CONSTRAINT [PK_ObjectApprovalHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ObjectHierarchy]    Script Date: 09/28/2005 14:39:13 ******/
CREATE TABLE [dbo].[ObjectHierarchy](
	[ID] [int] NOT NULL,
	[ObjectID] [int] NOT NULL,
	[HierarchyID] [int] NOT NULL,
	[UpdateStamp] [bigint] NULL,
 CONSTRAINT [PK_ObjectHierarchy] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ObjectHistory]    Script Date: 09/28/2005 14:39:42 ******/
CREATE TABLE [dbo].[ObjectHistory](
	[ID] [int] NOT NULL,
	[ObjectID] [int] NOT NULL,
	[ObjectTypeID] [int] NOT NULL,
	[ObjectTypePageID] [int] NULL,
	[ObjectXml] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CurrentStatus] [int] NULL,
	[ModifierID] [int] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[UpdateStamp] [bigint] NOT NULL,
 CONSTRAINT [PK_ObjectHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ObjectProperty]    Script Date: 09/28/2005 14:43:06 ******/
CREATE TABLE [dbo].[ObjectProperty](
	[ID] [int] NOT NULL,
	[ObjectID] [int] NOT NULL,
	[Name] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[PropertyValue] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UpdateStamp] [bigint] NOT NULL,
	[Deleted] [bit] NOT NULL CONSTRAINT [DF_ObjectProperty_Deleted]  DEFAULT (0),
 CONSTRAINT [PK_ExtendedProperties] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ObjectPropertyType]    Script Date: 09/28/2005 14:43:38 ******/
CREATE TABLE [dbo].[ObjectPropertyType](
	[ID] [int] NOT NULL,
	[Name] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ObjectTypeID] [int] NOT NULL,
	[Required] [tinyint] NOT NULL,
 CONSTRAINT [pkObjectPropertyType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ObjectResponse]    Script Date: 09/28/2005 14:44:00 ******/
CREATE TABLE [dbo].[ObjectResponse](
	[ID] [int] NOT NULL,
	[ObjectID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[Response] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UpdateStamp] [bigint] NULL,
 CONSTRAINT [PK_ObjectResponse] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ObjectRole]    Script Date: 09/28/2005 14:44:41 ******/
CREATE TABLE [dbo].[ObjectRole](
	[ID] [int] NOT NULL,
	[ObjectID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[Access] [tinyint] NOT NULL CONSTRAINT [DF_ObjectRole_Access]  DEFAULT (0),
	[EditFlg] [tinyint] NOT NULL CONSTRAINT [DF_ObjectRole_EditFlg]  DEFAULT (0),
	[MaxApprovalLevel] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[UpdateStamp] [bigint] NOT NULL,
 CONSTRAINT [PK_ObjectRole] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ObjectScheme]    Script Date: 09/28/2005 14:45:54 ******/
CREATE TABLE [dbo].[ObjectScheme](
	[ID] [int] NOT NULL,
	[ObjectID] [int] NOT NULL,
	[Description] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ReceivingRoleID] [int] NULL,
	[RequestingRoleID] [int] NULL,
	[ApprovingRoleID] [int] NOT NULL,
	[AccessStatus] [int] NULL,
	[ApprovalStatus] [int] NULL,
	[UpdateStamp] [bigint] NOT NULL,
 CONSTRAINT [PK_ObjectScheme] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ObjectType]    Script Date: 09/28/2005 14:46:16 ******/
CREATE TABLE [dbo].[ObjectType](
	[ID] [int] NOT NULL,
	[Name] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UpdateStamp] [bigint] NOT NULL,
 CONSTRAINT [PK_tlkpObjectTypes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ObjectTypeArea]    Script Date: 09/28/2005 14:46:40 ******/
CREATE TABLE [dbo].[ObjectTypeArea](
	[ID] [int] NOT NULL,
	[ObjectID] [int] NOT NULL,
	[ObjectTypePageID] [int] NULL,
	[TemplateAreaID] [int] NULL,
	[TemplateName] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UpdateStamp] [bigint] NOT NULL,
 CONSTRAINT [PK_Area] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ObjectTypeAreaBlock]    Script Date: 09/28/2005 14:47:09 ******/
CREATE TABLE [dbo].[ObjectTypeAreaBlock](
	[ID] [int] NOT NULL,
	[ObjectTypeAreaID] [int] NOT NULL,
	[ObjectTypeBlockID] [int] NOT NULL,
	[TemplateAreaBlockID] [int] NULL,
	[DisplayOrder] [int] NULL,
	[UpdateStamp] [bigint] NOT NULL,
 CONSTRAINT [PK_ObjectTypeBlockArea] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ObjectTypeAwardCriteria]    Script Date: 09/28/2005 14:49:21 ******/
CREATE TABLE [dbo].[ObjectTypeAwardCriteria](
	[ID] [int] NOT NULL,
	[ObjectID] [int] NULL,
	[ParentID] [int] NOT NULL CONSTRAINT [DF_ObjectTypeAwardCriteria_ParentID]  DEFAULT (0),
	[ProgramID] [int] NOT NULL,
	[RecipientRoleID] [int] NOT NULL,
	[Description] [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[UpdateStamp] [bigint] NULL,
 CONSTRAINT [PK_ObjectTypeAwardCriteria] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ObjectTypeBlock]    Script Date: 09/28/2005 14:49:53 ******/
CREATE TABLE [dbo].[ObjectTypeBlock](
	[ID] [int] NOT NULL,
	[ObjectID] [int] NULL,
	[TemplateBlockID] [int] NULL,
	[UpdateStamp] [bigint] NOT NULL,
	[SourceBlockID] [int] NULL,
 CONSTRAINT [PK_Block] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ObjectTypeFAQ]    Script Date: 09/28/2005 14:50:32 ******/
CREATE TABLE [dbo].[ObjectTypeFAQ](
	[ID] [int] NOT NULL,
	[ObjectID] [int] NOT NULL,
	[UpdateStamp] [bigint] NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Question] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Answer] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [pkObjectTypeFAQ] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ObjectTypeIncentiveProgram]    Script Date: 09/28/2005 14:50:43 ******/
CREATE TABLE [dbo].[ObjectTypeIncentiveProgram](
	[ID] [int] NOT NULL,
	[ObjectID] [int] NOT NULL,
	[ClientID] [int] NULL,
	[SiteID] [int] NULL,
	[ProgramCategoryID] [int] NULL,
	[ProgramTypeID] [int] NULL,
	[DKNumber] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description] [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ObjectTypeIncentiveProgram_Description]  DEFAULT (''),
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[UpdateStamp] [bigint] NOT NULL,
	[HierarchyID] [int] NULL,
 CONSTRAINT [PK_ObjectTypeIncentiveProgram] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ObjectTypePage]    Script Date: 09/28/2005 14:51:45 ******/
CREATE TABLE [dbo].[ObjectTypePage](
	[ID] [int] NOT NULL,
	[ObjectID] [int] NOT NULL,
	[ObjectTypeSiteID] [int] NULL,
	[TemplatePageID] [int] NULL,
	[TemplateBody] [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UpdateStamp] [bigint] NOT NULL,
	[SourcePageID] [int] NULL,
 CONSTRAINT [PK_Pages] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ObjectTypeSite]    Script Date: 09/28/2005 14:52:15 ******/
CREATE TABLE [dbo].[ObjectTypeSite](
	[ID] [int] NOT NULL,
	[ObjectID] [int] NOT NULL,
	[ClientID] [int] NOT NULL,
	[TemplateSiteID] [int] NULL,
	[Stylesheet] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SessionTimeout] [int] NULL,
	[EnableLogging] [bit] NULL,
	[UserLogin] [bit] NULL,
	[SSL] [bit] NULL,
	[ShowLiveHelp] [bit] NULL,
	[ShowCopyright] [bit] NULL,
	[ClientPortal] [bit] NULL,
	[DebitCardProgram] [bit] NULL,
	[PublishControl] [bit] NULL,
	[UpdateStamp] [bigint] NOT NULL,
 CONSTRAINT [PK_Site] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER TABLE [dbo].[BlockObjectHistory]  WITH CHECK ADD  CONSTRAINT [FK_sBlockObject_Object] FOREIGN KEY(	[ObjectID])
REFERENCES [dbo].[Object] (	[ID])
GO
ALTER TABLE [dbo].[BlockObjectHistory]  WITH CHECK ADD  CONSTRAINT [FK_sBlockObject_ObjectTypeBlock] FOREIGN KEY(	[ObjectTypeBlockID])
REFERENCES [dbo].[ObjectTypeBlock] (	[ID])
GO
ALTER TABLE [dbo].[BlockObject]  WITH CHECK ADD  CONSTRAINT [FK_BlockObject_Object] FOREIGN KEY(	[ObjectID])
REFERENCES [dbo].[Object] (	[ID])
GO
ALTER TABLE [dbo].[BlockObject]  WITH CHECK ADD  CONSTRAINT [FK_ObjectBlock_ObjectTypeBlock] FOREIGN KEY(	[ObjectTypeBlockID])
REFERENCES [dbo].[ObjectTypeBlock] (	[ID])
GO
ALTER TABLE [dbo].[ContentHistory]  WITH CHECK ADD  CONSTRAINT [FK_ContentHistory_Object] FOREIGN KEY(	[ObjectID])
REFERENCES [dbo].[Object] (	[ID])
GO
ALTER TABLE [dbo].[ContentHistory]  WITH CHECK ADD  CONSTRAINT [FK_ContentHistory_ObjectProperty] FOREIGN KEY(	[PropertyID])
REFERENCES [dbo].[ObjectProperty] (	[ID])
GO
ALTER TABLE [dbo].[ContentHistory]  WITH CHECK ADD  CONSTRAINT [FK_ContentHistory_ObjectTypeSite] FOREIGN KEY(	[SiteID])
REFERENCES [dbo].[ObjectTypeSite] (	[ID])
GO
ALTER TABLE [dbo].[Budget]  WITH CHECK ADD  CONSTRAINT [FK_Budget_ObjectTypeIncentiveProgram] FOREIGN KEY(	[ProgramID])
REFERENCES [dbo].[ObjectTypeIncentiveProgram] (	[ID])
GO
ALTER TABLE [dbo].[Budget]  WITH NOCHECK ADD  CONSTRAINT [FK_Budget_User] FOREIGN KEY(	[OwnerID])
REFERENCES [dbo].[User] (	[ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Budget] CHECK CONSTRAINT [FK_Budget_User]
GO
ALTER TABLE [dbo].[BudgetAllocation]  WITH CHECK ADD  CONSTRAINT [FK_BudgetAllocation_Budget] FOREIGN KEY(	[BudgetID])
REFERENCES [dbo].[Budget] (	[ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BudgetAllocation]  WITH NOCHECK ADD  CONSTRAINT [FK_BudgetAllocation_Modifier] FOREIGN KEY(	[ModifiedByID])
REFERENCES [dbo].[User] (	[ID])
GO
ALTER TABLE [dbo].[BudgetAllocation] CHECK CONSTRAINT [FK_BudgetAllocation_Modifier]
GO
ALTER TABLE [dbo].[BudgetAllocation]  WITH NOCHECK ADD  CONSTRAINT [FK_BudgetAllocation_User] FOREIGN KEY(	[UserID])
REFERENCES [dbo].[User] (	[ID])
GO
ALTER TABLE [dbo].[BudgetAllocation] CHECK CONSTRAINT [FK_BudgetAllocation_User]
GO
ALTER TABLE [dbo].[PointBoundary]  WITH CHECK ADD  CONSTRAINT [FK_PointBoundary_ObjectTypeIncentiveProgram] FOREIGN KEY(	[ProgramID])
REFERENCES [dbo].[ObjectTypeIncentiveProgram] (	[ID])
GO
ALTER TABLE [dbo].[PointBoundary]  WITH CHECK ADD  CONSTRAINT [FK_PointBoundary_ProgramType] FOREIGN KEY(	[ProgramTypeID])
REFERENCES [dbo].[ProgramType] (	[ID])
GO
ALTER TABLE [dbo].[ProgramScheme]  WITH CHECK ADD  CONSTRAINT [FK_ProgramScheme_ObjectTypeIncentiveProgram] FOREIGN KEY(	[ProgramID])
REFERENCES [dbo].[ObjectTypeIncentiveProgram] (	[ID])
GO
ALTER TABLE [dbo].[Object]  WITH CHECK ADD  CONSTRAINT [FK_Object_Object] FOREIGN KEY(	[ParentID])
REFERENCES [dbo].[Object] (	[ID])
GO
ALTER TABLE [dbo].[Object]  WITH CHECK ADD  CONSTRAINT [FK_Object_ObjectType] FOREIGN KEY(	[ObjectTypeID])
REFERENCES [dbo].[ObjectType] (	[ID])
GO
ALTER TABLE [dbo].[ObjectApprovalHistory]  WITH CHECK ADD  CONSTRAINT [FK_ObjectApprovalHistory_ObjectHistory] FOREIGN KEY(	[ObjectHistoryID])
REFERENCES [dbo].[ObjectHistory] (	[ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ObjectApprovalHistory]  WITH NOCHECK ADD  CONSTRAINT [FK_ObjectApprovalHistory_User] FOREIGN KEY(	[ModifiedByID])
REFERENCES [dbo].[User] (	[ID])
GO
ALTER TABLE [dbo].[ObjectApprovalHistory] CHECK CONSTRAINT [FK_ObjectApprovalHistory_User]
GO
ALTER TABLE [dbo].[ObjectHierarchy]  WITH CHECK ADD  CONSTRAINT [FK_ObjectHierarchy_Hierarchy] FOREIGN KEY(	[HierarchyID])
REFERENCES [dbo].[Hierarchy] (	[ID])
GO
ALTER TABLE [dbo].[ObjectHierarchy]  WITH CHECK ADD  CONSTRAINT [FK_ObjectHierarchy_Object] FOREIGN KEY(	[ObjectID])
REFERENCES [dbo].[Object] (	[ID])
GO
ALTER TABLE [dbo].[ObjectHistory]  WITH CHECK ADD  CONSTRAINT [FK_ObjectHistory_Object] FOREIGN KEY(	[ObjectID])
REFERENCES [dbo].[Object] (	[ID])
GO
ALTER TABLE [dbo].[ObjectHistory]  WITH CHECK ADD  CONSTRAINT [FK_ObjectHistory_ObjectType] FOREIGN KEY(	[ObjectTypeID])
REFERENCES [dbo].[ObjectType] (	[ID])
GO
ALTER TABLE [dbo].[ObjectHistory]  WITH NOCHECK ADD  CONSTRAINT [FK_ObjectHistory_User] FOREIGN KEY(	[ModifierID])
REFERENCES [dbo].[User] (	[ID])
GO
ALTER TABLE [dbo].[ObjectHistory] CHECK CONSTRAINT [FK_ObjectHistory_User]
GO
ALTER TABLE [dbo].[ObjectProperty]  WITH CHECK ADD  CONSTRAINT [FK_ObjectProperty_Object] FOREIGN KEY(	[ObjectID])
REFERENCES [dbo].[Object] (	[ID])
GO
ALTER TABLE [dbo].[ObjectResponse]  WITH NOCHECK ADD  CONSTRAINT [FK_ObjectResponse_User] FOREIGN KEY(	[UserID])
REFERENCES [dbo].[User] (	[ID])
GO
ALTER TABLE [dbo].[ObjectResponse] CHECK CONSTRAINT [FK_ObjectResponse_User]
GO
ALTER TABLE [dbo].[ObjectRole]  WITH CHECK ADD  CONSTRAINT [FK_ObjectAccess_Object] FOREIGN KEY(	[ObjectID])
REFERENCES [dbo].[Object] (	[ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ObjectRole]  WITH CHECK ADD  CONSTRAINT [FK_ObjectAccess_Role] FOREIGN KEY(	[RoleID])
REFERENCES [dbo].[Role] (	[ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ObjectTypeArea]  WITH CHECK ADD  CONSTRAINT [FK_ObjectTypeArea_Object] FOREIGN KEY(	[ObjectID])
REFERENCES [dbo].[Object] (	[ID])
GO
ALTER TABLE [dbo].[ObjectTypeArea]  WITH CHECK ADD  CONSTRAINT [FK_ObjectTypeArea_ObjectTypePage] FOREIGN KEY(	[ObjectTypePageID])
REFERENCES [dbo].[ObjectTypePage] (	[ID])
GO
ALTER TABLE [dbo].[ObjectTypeAreaBlock]  WITH CHECK ADD  CONSTRAINT [FK_ObjectTypeBlockArea_ObjectTypeArea] FOREIGN KEY(	[ObjectTypeAreaID])
REFERENCES [dbo].[ObjectTypeArea] (	[ID])
GO
ALTER TABLE [dbo].[ObjectTypeAreaBlock]  WITH CHECK ADD  CONSTRAINT [FK_ObjectTypeBlockArea_ObjectTypeBlock] FOREIGN KEY(	[ObjectTypeBlockID])
REFERENCES [dbo].[ObjectTypeBlock] (	[ID])
GO
ALTER TABLE [dbo].[ObjectTypeAwardCriteria]  WITH CHECK ADD  CONSTRAINT [FK_ObjectTypeAwardCriteria_Object] FOREIGN KEY(	[ObjectID])
REFERENCES [dbo].[Object] (	[ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ObjectTypeAwardCriteria]  WITH CHECK ADD  CONSTRAINT [FK_ObjectTypeAwardCriteria_ObjectTypeIncentiveProgram] FOREIGN KEY(	[ProgramID])
REFERENCES [dbo].[ObjectTypeIncentiveProgram] (	[ID])
GO
ALTER TABLE [dbo].[ObjectTypeBlock]  WITH CHECK ADD  CONSTRAINT [FK_ObjectTypeBlock_Object] FOREIGN KEY(	[ObjectID])
REFERENCES [dbo].[Object] (	[ID])
GO
ALTER TABLE [dbo].[ObjectTypeIncentiveProgram]  WITH CHECK ADD  CONSTRAINT [FK_ObjectTypeIncentiveProgram_Client] FOREIGN KEY(	[ClientID])
REFERENCES [dbo].[Client] (	[ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ObjectTypeIncentiveProgram]  WITH CHECK ADD  CONSTRAINT [FK_ObjectTypeIncentiveProgram_Hierarchy] FOREIGN KEY(	[HierarchyID])
REFERENCES [dbo].[Hierarchy] (	[ID])
GO
ALTER TABLE [dbo].[ObjectTypeIncentiveProgram]  WITH CHECK ADD  CONSTRAINT [FK_ObjectTypeIncentiveProgram_Object] FOREIGN KEY(	[ObjectID])
REFERENCES [dbo].[Object] (	[ID])
GO
ALTER TABLE [dbo].[ObjectTypeIncentiveProgram]  WITH CHECK ADD  CONSTRAINT [FK_ObjectTypeIncentiveProgram_ProgramCategory] FOREIGN KEY(	[ProgramCategoryID])
REFERENCES [dbo].[ProgramCategory] (	[ID])
GO
ALTER TABLE [dbo].[ObjectTypeIncentiveProgram]  WITH CHECK ADD  CONSTRAINT [FK_ObjectTypeIncentiveProgram_ProgramType] FOREIGN KEY(	[ProgramTypeID])
REFERENCES [dbo].[ProgramType] (	[ID])
GO
ALTER TABLE [dbo].[ObjectTypePage]  WITH CHECK ADD  CONSTRAINT [FK_ObjectTypePage_Object] FOREIGN KEY(	[ObjectID])
REFERENCES [dbo].[Object] (	[ID])
GO
ALTER TABLE [dbo].[ObjectTypePage]  WITH CHECK ADD  CONSTRAINT [FK_ObjectTypePage_ObjectTypeSite] FOREIGN KEY(	[ObjectTypeSiteID])
REFERENCES [dbo].[ObjectTypeSite] (	[ID])
GO
ALTER TABLE [dbo].[ObjectTypeSite]  WITH CHECK ADD  CONSTRAINT [FK_ObjectTypeSite_Object] FOREIGN KEY(	[ObjectID])
REFERENCES [dbo].[Object] (	[ID])
ON DELETE CASCADE