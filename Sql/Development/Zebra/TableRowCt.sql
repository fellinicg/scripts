DECLARE @ExecStr VARCHAR(1000)

SELECT name 
INTO #Temp
FROM sysobjects
WHERE xtype = 'U'
ORDER BY name

WHILE EXISTS (SELECT 1 FROM #Temp)
	BEGIN
		SELECT TOP 1 @ExecStr = 'IF (SELECT COUNT(*) [' + name + '] FROM [' + name + ']) > 0 SELECT COUNT(*) [' + name + '] FROM [' + name + ']'
--		SELECT TOP 1 @ExecStr = 'IF (SELECT COUNT(*) [' + name + '] FROM [' + name + ']) = 0 SELECT COUNT(*) [' + name + '] FROM [' + name + ']'
		FROM #Temp
		ORDER BY name
		EXEC(@ExecStr)
		DELETE FROM #Temp
		WHERE name = (
									SELECT TOP 1 name
									FROM #Temp
									ORDER BY name)
	END

DROP TABLE #Temp