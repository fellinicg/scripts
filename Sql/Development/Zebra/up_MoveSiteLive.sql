SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: September 29, 2005
-- Description:	Move site from development to production
-- =============================================
ALTER PROCEDURE dbo.up_MoveSiteLive 
	@SiteID int
AS
BEGIN

	DECLARE @RowsInserted INT
	DECLARE @SiteName VARCHAR(100)
	DECLARE @ClientID INT
	DECLARE @RowCt INT

	SET NOCOUNT ON

	-- Create Temporary tables.
	CREATE TABLE #Temp(
		ObjID INT  NOT NULL,
		Dltd TINYINT
	)
	CREATE TABLE #Report(
		Info VARCHAR(500)  NOT NULL
	)
	CREATE TABLE #Users(
		UserID INT  NOT NULL,
		PersonID INT  NOT NULL
	)

	-- Initialize variables.
	SET @RowsInserted = 0
	SET @RowCt = 0
	SET @SiteName = ''
	SET @ClientID = 0

	-- Check that ID sent in is actually a site
	IF (SELECT ObjectTypeID FROM dbo.Object WHERE ID = @SiteID) = 1
		SELECT @SiteName = o.Name, @ClientID = ots.ClientID
		FROM dbo.Object o
		INNER JOIN dbo.ObjectTypeSite ots ON o.ID = ots.ObjectID
		WHERE o.ID = @SiteID
	ELSE
		BEGIN
			-- Add record to Report table
			INSERT INTO #Report
			SELECT @SiteName + ' (id=' + CONVERT(VARCHAR, @SiteID) + ') is NOT a valid site.'
			-- Get out
			GOTO FINALLY
		END

	-- Insert ID of Site into temporary table.
	INSERT INTO #Temp
	SELECT ID, Deleted
	FROM dbo.Object 
	WHERE ID = @SiteID
	AND Deleted = 0

	-- Set variable to number of rows affected.
	SET @RowsInserted = @@ROWCOUNT

	-- If local variable is still 0, means site was deleted.
	IF @RowsInserted = 0
		BEGIN
			-- Add record to Report table
			INSERT INTO #Report
			SELECT @SiteName + ' (id=' + CONVERT(VARCHAR, @SiteID) + ') is a DELETED site.'
			-- Get out
			GOTO FINALLY
		END

	-- Loop until no more rows have been affected.
	WHILE @RowsInserted > 0
		BEGIN
			INSERT INTO #Temp (ObjID, Dltd)
			SELECT DISTINCT obj.ID, obj.Deleted
			FROM dbo.Object obj
			INNER JOIN #Temp t ON obj.ParentID = t.ObjID
			LEFT JOIN #Temp t2 ON obj.ID = t2.ObjID
			WHERE t2.ObjID IS NULL
			AND obj.PendingApproval = 0
			SET @RowsInserted = @@ROWCOUNT
		END
	END

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, COUNT(ObjID)) + ' rows identified for the ' + @SiteName + ' site.'
	FROM #Temp

	-- Begin transaction
--	BEGIN TRAN

	-- Store users for this site in temporary table for use later in this procedure.
	INSERT INTO #Users
	SELECT DISTINCT u.ID, src.ID
	FROM #Temp
	INNER JOIN dbo.ObjectRole objrole ON objrole.ObjectID = ObjID AND Dltd = 0
	INNER JOIN dbo.RoleUser ru ON objrole.RoleID = ru.RoleID
	INNER JOIN dbo.[User] u ON ru.UserID = u.ID
	INNER JOIN dbo.Person src ON u.PersonID = src.ID
	UNION
	SELECT u.ID, src.ID
	FROM dbo.Organization org
	INNER JOIN dbo.OrganizationLevel ol ON org.ID = ol.OrganizationID AND org.ClientID = @ClientID
	INNER JOIN dbo.UserOrganizationLevel uol ON ol.ID = uol.OrganizationLevelID
	INNER JOIN dbo.UserOrganizationLevelGroup uolg ON uol.UserOrganizationLevelGroupID = uolg.ID
	INNER JOIN dbo.[User] u ON uolg.UserID = u.ID
	INNER JOIN dbo.Person src ON u.PersonID = src.ID

-- ===================================================
-- TemplatePage Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.TemplatePage
	SELECT src.*
	FROM dbo.TemplatePage src
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.TemplatePage dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to TemplatePage table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.TemplatePage 
  SET TemplateSiteID = src.TemplateSiteID
    ,Name = src.Name
    ,TemplateBody = src.TemplateBody
    ,BGColor = src.BGColor
    ,BGImage = src.BGImage
    ,SessionTimeout = src.SessionTimeout
    ,EnableLogging = src.EnableLogging
    ,IsHomepage = src.IsHomepage
    ,IsLoginPage = src.IsLoginPage
    ,IsGatewayPage = src.IsGatewayPage
    ,ThumbnailImage = src.ThumbnailImage
    ,Description = src.Description
    ,OriginatingObjectTypePageID = src.OriginatingObjectTypePageID
    ,Head = src.Head
    ,SingleViewPopup = src.SingleViewPopup
    ,RepeatingPopup = src.RepeatingPopup
    ,PopupWidth = src.PopupWidth
    ,PopupHeight = src.PopupHeight
    ,LoginRequired = src.LoginRequired
    ,UpdateStamp = src.UpdateStamp
 	FROM dbo.TemplatePage src
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.TemplatePage dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in TemplatePage table.'

-- ===================================================
-- ProgramCategory Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.ProgramCategory
	SELECT src.*
	FROM dbo.ProgramCategory src
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.ProgramCategory dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to ProgramCategory table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.TemplatePage 
  SET Name = src.Name
    ,UpdateStamp = src.UpdateStamp
 	FROM dbo.ProgramCategory src
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.ProgramCategory dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in ProgramCategory table.'

-- ===================================================
-- Client Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.Client
	SELECT src.*
	FROM dbo.Client src
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.Client dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	AND src.ID = @ClientID
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to Client table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.Client 
  SET Name = src.Name
    ,UpdateStamp = src.UpdateStamp
	FROM dbo.Client src
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.Client dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp
	AND src.ID = @ClientID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in Client table.'

-- ===================================================
-- ProgramType Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.ProgramType
	SELECT src.*
	FROM dbo.ProgramType src
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.ProgramType dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	AND src.ID = @ClientID
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to ProgramType table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.ProgramType 
  SET Name = src.Name
    ,UpdateStamp = src.UpdateStamp
	FROM dbo.ProgramType src
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.ProgramType dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp
	AND src.ID = @ClientID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in ProgramType table.'

-- ===================================================
-- Organization Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.Organization
	SELECT src.*
	FROM dbo.Organization src 
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.Organization dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	AND src.ClientID = @ClientID
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to Organization table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.Organization 
  SET Name = src.Name
		,SourceColumn = src.SourceColumn
    ,UpdateStamp = src.UpdateStamp
	FROM dbo.Organization src
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.Organization dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp
	AND src.ClientID = @ClientID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in Organization table.'

-- ===================================================
-- OrganizationLevel Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.OrganizationLevel
	SELECT src.*
	FROM dbo.Organization org 
	INNER JOIN dbo.OrganizationLevel src ON org.ID = src.OrganizationID AND org.ClientID = @ClientID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.OrganizationLevel dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to OrganizationLevel table.'

--	-- Update data that has been updated in development
--	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.OrganizationLevel 
--  SET OrganizationID = src.OrganizationID
--		,Name = src.Name
--    ,UpdateStamp = src.UpdateStamp
--	FROM dbo.OrganizationLevel src
--	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.OrganizationLevel dest ON src.ID = dest.ID
--	WHERE src.UpdateStamp > dest.UpdateStamp
--
--	-- Add record to Report table
--	INSERT INTO #Report
--	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in OrganizationLevel table.'

-- ===================================================
-- Role Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.Role
	SELECT DISTINCT src.*
	FROM #Temp
	INNER JOIN dbo.ObjectRole objrole ON objrole.ObjectID = ObjID AND Dltd = 0
	INNER JOIN dbo.Role src ON objrole.RoleID = src.ID AND src.IsDeleted = 0
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.Role dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to Role table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.Role 
	SET Name = src.Name
		,IsDeleted = src.IsDeleted
		,StartDate = src.StartDate
		,EndDate = src.EndDate
		,IsZebraRole = src.IsZebraRole
		,IsInternal = src.IsInternal
		,IsAdmin = src.IsAdmin
		,UpdateStamp = src.UpdateStamp
		,UserView = src.UserView
	FROM #Temp
	INNER JOIN dbo.ObjectRole objrole ON objrole.ObjectID = ObjID
	INNER JOIN dbo.Role src ON objrole.RoleID = src.ID
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.Role dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in Role table.'

-- ===================================================
-- RolePermissionType Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.RolePermissionType
	SELECT src.*
	FROM dbo.RolePermissionType src
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.RolePermissionType dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to RolePermissionType table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.RolePermissionType 
  SET Name = src.Name
    ,Description = src.Description
	FROM dbo.RolePermissionType src
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.RolePermissionType dest ON src.ID = dest.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in RolePermissionType table.'

-- ===================================================
-- RolePermission Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.RolePermission
	SELECT DISTINCT src.*
	FROM #Temp
	INNER JOIN dbo.ObjectRole objrole ON objrole.ObjectID = #Temp.ObjID AND #Temp.Dltd = 0
	INNER JOIN dbo.RolePermission src ON objrole.ID = src.ID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.RolePermission dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to RolePermission table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.RolePermission 
	SET RoleID = src.RoleID
		,Name = src.Name
		,UpdateStamp = src.UpdateStamp
	FROM #Temp
	INNER JOIN dbo.ObjectRole objrole ON objrole.ObjectID = #Temp.ObjID
	INNER JOIN dbo.RolePermission src ON objrole.ID = src.ID
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.RolePermission dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in RolePermission table.'

-- ===================================================
-- RoleOrganizationLevel Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.RoleOrganizationLevel
	SELECT DISTINCT src.*
	FROM #Temp
	INNER JOIN dbo.ObjectRole objrole ON objrole.ObjectID = #Temp.ObjID AND #Temp.Dltd = 0
	INNER JOIN dbo.Organization org ON org.ClientID = @ClientID
	INNER JOIN dbo.OrganizationLevel ol ON org.ID = ol.OrganizationID  
	INNER JOIN dbo.RoleOrganizationLevel src ON objrole.ID = src.RoleID AND ol.ID = OrganizationLevelID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.RoleOrganizationLevel dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to RoleOrganizationLevel table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.RoleOrganizationLevel 
  SET RoleID = src.RoleID
    ,OrganizationLevelID = src.OrganizationLevelID
    ,UpdateStamp = src.UpdateStamp
	FROM #Temp
	INNER JOIN dbo.ObjectRole objrole ON objrole.ObjectID = #Temp.ObjID
	INNER JOIN dbo.Organization org ON org.ClientID = @ClientID
	INNER JOIN dbo.OrganizationLevel ol ON org.ID = ol.OrganizationID  
	INNER JOIN dbo.RoleOrganizationLevel src ON objrole.ID = src.RoleID AND ol.ID = OrganizationLevelID
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.RoleOrganizationLevel dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in RoleOrganizationLevel table.'

-- ===================================================
-- ContactType Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.ContactType
	SELECT DISTINCT src.*
	FROM dbo.ContactType src
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.ContactType dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to ContactType table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.ContactType 
  SET TableName = src.TableName
    ,ShortDescription = src.ShortDescription
    ,LongDescription = src.LongDescription
    ,TypeDescription = src.TypeDescription
    ,UpdateStamp = src.UpdateStamp
	FROM dbo.ContactType src
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.ContactType dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated to ContactType table.'

-- ===================================================
-- Person Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.Person
	SELECT DISTINCT src.*
	FROM #Users u
	INNER JOIN dbo.Person src ON u.PersonID = src.ID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.Person dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to Person table.'

-- ===================================================
-- Address Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.Address
	SELECT DISTINCT src.*
	FROM #Users u
	INNER JOIN dbo.Address src ON u.PersonID = src.PersonID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.Address dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to Address table.'

-- ===================================================
-- EmailAddress Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.EmailAddress
	SELECT DISTINCT src.*
	FROM #Users u
	INNER JOIN dbo.EmailAddress src ON u.PersonID = src.PersonID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.EmailAddress dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to EmailAddress table.'

-- ===================================================
-- EmergencyContacts Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.EmergencyContacts
	SELECT DISTINCT src.*
	FROM #Users u
	INNER JOIN dbo.EmergencyContacts src ON u.PersonID = src.PersonID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.EmergencyContacts dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to EmergencyContacts table.'

-- ===================================================
-- Phone Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.Phone
	SELECT DISTINCT src.*
	FROM #Users u
	INNER JOIN dbo.Phone src ON u.PersonID = src.PersonID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.Phone dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to Phone table.'

-- ===================================================
-- Hierarchy Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.Hierarchy
	SELECT DISTINCT src.*
	FROM #Temp
	INNER JOIN dbo.Hierarchy src ON src.ObjectID = #Temp.ObjID AND #Temp.Dltd = 0
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.Hierarchy dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to Hierarchy table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.Hierarchy 
	SET ObjectID = src.ObjectID
		,Name = src.Name
		,UpdateStamp = src.UpdateStamp
	FROM dbo.Hierarchy src
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.Hierarchy dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated to Hierarchy table.'

-- ===================================================
-- User Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.[User]
	SELECT DISTINCT src.*
	FROM #Users u
	INNER JOIN dbo.[User] src ON u.UserID = src.ID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.[User] dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to User table.'

-- ===================================================
-- RoleUser Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.RoleUser
	SELECT DISTINCT src.*
	FROM #Temp
	INNER JOIN dbo.ObjectRole objrole ON objrole.ObjectID = #Temp.ObjID AND #Temp.Dltd = 0
	INNER JOIN dbo.RoleUser src ON src.RoleID = objrole.RoleID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.RoleUser dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to RoleUser table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.RoleUser 
  SET RoleID = src.RoleID
    ,UserID = src.UserID
    ,UpdateStamp = src.UpdateStamp
	FROM #Temp
	INNER JOIN dbo.ObjectRole objrole ON objrole.ObjectID = #Temp.ObjID
	INNER JOIN dbo.RoleUser src ON src.RoleID = objrole.RoleID
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.RoleUser dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in RoleUser table.'

-- ===================================================
-- RoleUserByProcedure Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.RoleUserByProcedure
	SELECT DISTINCT src.*
	FROM #Temp
	INNER JOIN dbo.ObjectRole objrole ON objrole.ObjectID = #Temp.ObjID AND #Temp.Dltd = 0
	INNER JOIN dbo.RoleUserByProcedure src ON src.RoleID = objrole.RoleID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.RoleUserByProcedure dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to RoleUserByProcedure table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.RoleUserByProcedure 
  SET RoleID = src.RoleID
    ,UserID = src.UserID
    ,UpdateStamp = src.UpdateStamp
	FROM #Temp
	INNER JOIN dbo.ObjectRole objrole ON objrole.ObjectID = #Temp.ObjID
	INNER JOIN dbo.RoleUserByProcedure src ON src.RoleID = objrole.RoleID
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.RoleUserByProcedure dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in RoleUserByProcedure table.'

-- ===================================================
-- HierarchyUser Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.HierarchyUser
	SELECT DISTINCT src.*
	FROM #Temp
	INNER JOIN dbo.Hierarchy h ON h.ObjectID = #Temp.ObjID AND #Temp.Dltd = 0
	INNER JOIN dbo.HierarchyUser src ON src.HierarchyID = h.ID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.HierarchyUser dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to HierarchyUser table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.HierarchyUser 
  SET HierarchyID = src.HierarchyID
    ,UserID = src.UserID
    ,ParentUserID = src.ParentUserID
    ,UpdateStamp = src.UpdateStamp
	FROM #Temp
	INNER JOIN dbo.Hierarchy h ON h.ObjectID = #Temp.ObjID
	INNER JOIN dbo.HierarchyUser src ON src.HierarchyID = h.ID
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.HierarchyUser dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated to HierarchyUser table.'

-- ===================================================
-- UserAttributes Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.UserAttributes
	SELECT DISTINCT src.*
	FROM #Users
	INNER JOIN dbo.UserAttributes src ON src.UserID = #Users.UserID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.UserAttributes dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to UserAttributes table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.UserAttributes 
  SET UserID = src.UserID
    ,AttributeName = src.AttributeName
    ,AttributeValue = src.AttributeValue
    ,UpdateStamp = src.UpdateStamp
	FROM #Users
	INNER JOIN dbo.UserAttributes src ON src.UserID = #Users.UserID
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.UserAttributes dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated to UserAttributes table.'

-- ===================================================
-- UserOrganizationLevelGroup Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.UserOrganizationLevelGroup
	SELECT DISTINCT src.*
	FROM #Users
	INNER JOIN dbo.UserOrganizationLevelGroup src ON src.UserID = #Users.UserID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.UserOrganizationLevelGroup dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to UserOrganizationLevelGroup table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.UserOrganizationLevelGroup 
  SET UserID = src.UserID
    ,Name = src.Name
	FROM #Users
	INNER JOIN dbo.UserOrganizationLevelGroup src ON src.UserID = #Users.UserID
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.UserOrganizationLevelGroup dest ON src.ID = dest.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated to UserOrganizationLevelGroup table.'

-- ===================================================
-- UserOrganizationLevel Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.UserOrganizationLevel
	SELECT DISTINCT src.*
	FROM dbo.Organization org
	INNER JOIN dbo.OrganizationLevel ol ON org.ID = ol.OrganizationID AND org.ClientID = @ClientID
	INNER JOIN dbo.UserOrganizationLevel src ON src.OrganizationLevelID = ol.ID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.UserOrganizationLevel dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to UserOrganizationLevel table.'

--	-- Update data that has been updated in development
--	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.UserOrganizationLevel 
--  SET UserOrganizationLevelGroupID = src.UserOrganizationLevelGroupID
--    ,OrganizationLevelID = src.OrganizationLevelID
--    ,StartDate = src.StartDate
--    ,EndDate = src.EndDate
--    ,UpdateStamp = src.UpdateStamp
--	FROM dbo.UserOrganizationLevel src
--	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.UserOrganizationLevel dest ON src.ID = dest.ID
--	WHERE src.UpdateStamp > dest.UpdateStamp
--
--	-- Add record to Report table
--	INSERT INTO #Report
--	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated to UserOrganizationLevel table.'

-- ===================================================
-- ObjectType Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.ObjectType
	SELECT DISTINCT src.*
	FROM #Temp
	INNER JOIN dbo.Object obj ON obj.ID = ObjID AND #Temp.Dltd = 0
	INNER JOIN dbo.ObjectType src ON obj.ObjectTypeID = src.ID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectType dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to ObjectType table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.ObjectType 
	SET Name = src.Name
		,UpdateStamp = src.UpdateStamp
	FROM #Temp
	INNER JOIN dbo.Object obj ON obj.ID = ObjID
	INNER JOIN dbo.ObjectType src ON obj.ObjectTypeID = src.ID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectType dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in ObjectType table.'

-- ===================================================
-- ObjectScheme Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.ObjectScheme
	SELECT src.*
	FROM #Temp
	INNER JOIN dbo.ObjectScheme src ON #Temp.ObjID = src.ObjectID AND #Temp.Dltd = 0
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectScheme dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to ObjectScheme table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.ObjectScheme 
	SET Description = src.Description,
			ReceivingRoleID = src.ReceivingRoleID,
			RequestingRoleID = src.RequestingRoleID,
			ApprovingRoleID = src.ApprovingRoleID,
			AccessStatus = src.AccessStatus,
			ApprovalStatus = src.ApprovalStatus,
			UpdateStamp = src.UpdateStamp
	FROM #Temp
	INNER JOIN dbo.ObjectScheme src ON #Temp.ObjID = src.ObjectID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectScheme dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in ObjectScheme table.'

-- ===================================================
-- Object Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.Object 
	SELECT src.*
	FROM #Temp 
	INNER JOIN dbo.Object src ON #Temp.ObjID = src.ID AND #Temp.Dltd = 0
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.Object dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY #Temp.ObjID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to Object table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.Object 
  SET ObjectGUID = src.ObjectGUID
    ,ParentID = src.ParentID
    ,ObjectTypeID = src.ObjectTypeID
    ,Name = src.Name
    ,StartDate = src.StartDate
    ,EndDate = src.EndDate
    ,PendingApproval = src.PendingApproval
    ,Deleted = src.Deleted
    ,LastModified = src.LastModified
    ,UpdateStamp = src.UpdateStamp
	FROM #Temp
	INNER JOIN dbo.Object src ON #Temp.ObjID = src.ID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.Object dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in Object table.'

-- ===================================================
-- ObjectRole Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.ObjectRole 
	SELECT src.*
	FROM #Temp 
	INNER JOIN dbo.ObjectRole src ON #Temp.ObjID = src.ObjectID AND #Temp.Dltd = 0
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectRole dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to ObjectRole table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.ObjectRole 
  SET ObjectID = src.ObjectID
    ,RoleID = src.RoleID
    ,Access = src.Access
    ,EditFlg = src.EditFlg
    ,MaxApprovalLevel = src.MaxApprovalLevel
    ,StartDate = src.StartDate
    ,EndDate = src.EndDate
    ,UpdateStamp = src.UpdateStamp
	FROM #Temp 
	INNER JOIN dbo.ObjectRole src ON #Temp.ObjID = src.ObjectID
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectRole dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in ObjectRole table.'

-- ===================================================
-- ObjectHierarchy Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.ObjectHierarchy 
	SELECT src.*
	FROM #Temp 
	INNER JOIN dbo.ObjectHierarchy src ON #Temp.ObjID = src.ObjectID AND #Temp.Dltd = 0
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectHierarchy dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to ObjectHierarchy table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.ObjectHierarchy 
  SET ObjectID = src.ObjectID
    ,HierarchyID = src.HierarchyID
    ,UpdateStamp = src.UpdateStamp
	FROM #Temp 
	INNER JOIN dbo.ObjectHierarchy src ON #Temp.ObjID = src.ObjectID 
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectHierarchy dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in ObjectHierarchy table.'

-- ===================================================
-- ObjectHistory Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.ObjectHistory 
	SELECT src.*
	FROM #Temp 
	INNER JOIN dbo.ObjectHistory src ON #Temp.ObjID = src.ObjectID AND #Temp.Dltd = 0
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectHistory dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to ObjectHistory table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.ObjectHistory 
  SET ObjectID = src.ObjectID
    ,ObjectTypeID = src.ObjectTypeID
    ,ObjectTypePageID = src.ObjectTypePageID
    ,ObjectXml = src.ObjectXml
    ,CurrentStatus = src.CurrentStatus
    ,ModifierID = src.ModifierID
    ,ModifiedDate = src.ModifiedDate
    ,UpdateStamp = src.UpdateStamp
	FROM #Temp 
	INNER JOIN dbo.ObjectHistory src ON #Temp.ObjID = src.ObjectID
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectHistory dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in ObjectHistory table.'

---- ===================================================
---- ObjectPropertyType Table
---- ===================================================
--
--	-- Add data that doesn't exist in production
--	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.ObjectPropertyType 
--	SELECT src.*
--	FROM dbo.ObjectPropertyType src 
--	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectPropertyType dest ON src.ID = dest.ID
--	WHERE dest.ID IS NULL
--	ORDER BY src.ID
--
--	-- Add record to Report table
--	INSERT INTO #Report
--	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to ObjectPropertyType table.'
--
--	-- Update data that has been updated in development
--	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.ObjectPropertyType 
--  SET Name = src.Name
--    Description, = src.Description
--    ObjectTypeID, = src.ObjectTypeID
--    Required, = src.Required
--	FROM dbo.ObjectPropertyType dest 
--	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectPropertyType src ON src.ID = dest.ID
--
--	-- Add record to Report table
--	INSERT INTO #Report
--	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in ObjectPropertyType table.'

-- ===================================================
-- ObjectProperty Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.[ObjectProperty] 
	SELECT src.*
	FROM #Temp 
	INNER JOIN dbo.[ObjectProperty] src ON #Temp.ObjID = src.ObjectID AND #Temp.Dltd = 0
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.[ObjectProperty] dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to ObjectProperty table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.[ObjectProperty] 
  SET ObjectID = src.ObjectID
    ,Name = src.Name
    ,PropertyValue = src.PropertyValue
    ,UpdateStamp = src.UpdateStamp
    ,Deleted = src.Deleted
	FROM #Temp 
	INNER JOIN dbo.[ObjectProperty] src ON #Temp.ObjID = src.ObjectID AND #Temp.Dltd = 0
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.[ObjectProperty] dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in ObjectProperty table.'

-- ===================================================
-- ObjectTypeBlock Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeBlock 
	SELECT DISTINCT src.*
	FROM #Temp 
	INNER JOIN dbo.ObjectTypeBlock src ON #Temp.ObjID = src.ObjectID AND #Temp.Dltd = 0
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeBlock dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to ObjectTypeBlock table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeBlock 
  SET ObjectID = src.ObjectID
    ,TemplateBlockID = src.TemplateBlockID
    ,UpdateStamp = src.UpdateStamp
    ,SourceBlockID = src.SourceBlockID
	FROM #Temp 
	INNER JOIN dbo.ObjectTypeBlock src ON #Temp.ObjID = src.ObjectID
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeBlock dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in ObjectTypeBlock table.'

-- ===================================================
-- ObjectTypeSite Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeSite 
	SELECT src.*
	FROM #Temp 
	INNER JOIN dbo.ObjectTypeSite src ON #Temp.ObjID = src.ObjectID AND #Temp.Dltd = 0
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeSite dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to ObjectTypeSite table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeSite 
  SET ObjectID = src.ObjectID
    ,ClientID = src.ClientID
    ,TemplateSiteID = src.TemplateSiteID
    ,Stylesheet = src.Stylesheet
    ,SessionTimeout = src.SessionTimeout
    ,EnableLogging = src.EnableLogging
    ,UserLogin = src.UserLogin
    ,SSL = src.SSL
    ,ShowLiveHelp = src.ShowLiveHelp
    ,ShowCopyright = src.ShowCopyright
    ,ClientPortal = src.ClientPortal
    ,DebitCardProgram = src.DebitCardProgram
    ,PublishControl = src.PublishControl
    ,UpdateStamp = src.UpdateStamp
	FROM #Temp 
	INNER JOIN dbo.ObjectTypeSite src ON #Temp.ObjID = src.ObjectID
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeSite dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in ObjectTypeSite table.'

-- ===================================================
-- ObjectTypePage Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypePage 
	SELECT src.*
	FROM #Temp 
	INNER JOIN dbo.ObjectTypePage src ON #Temp.ObjID = src.ObjectID AND #Temp.Dltd = 0
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypePage dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to ObjectTypePage table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypePage 
  SET ObjectID = src.ObjectID
    ,ObjectTypeSiteID = src.ObjectTypeSiteID
    ,TemplatePageID = src.TemplatePageID
    ,TemplateBody = src.TemplateBody
    ,UpdateStamp = src.UpdateStamp
    ,SourcePageID = src.SourcePageID
	FROM #Temp 
	INNER JOIN dbo.ObjectTypePage src ON #Temp.ObjID = src.ObjectID
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypePage dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in ObjectTypePage table.'

-- ===================================================
-- ObjectTypeArea Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeArea 
	SELECT src.*
	FROM #Temp 
	INNER JOIN dbo.ObjectTypeArea src ON #Temp.ObjID = src.ObjectID AND #Temp.Dltd = 0
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeArea dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to ObjectTypeArea table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeArea 
  SET ObjectID = src.ObjectID
    ,ObjectTypePageID = src.ObjectTypePageID
    ,TemplateAreaID = src.TemplateAreaID
    ,TemplateName = src.TemplateName
    ,UpdateStamp = src.UpdateStamp
	FROM #Temp 
	INNER JOIN dbo.ObjectTypeArea src ON #Temp.ObjID = src.ObjectID
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeArea dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in ObjectTypeArea table.'

-- ===================================================
-- ObjectTypeAreaBlock Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeAreaBlock 
	SELECT src.*
	FROM #Temp 
	INNER JOIN dbo.ObjectTypeArea ota ON #Temp.ObjID = ota.ObjectID AND #Temp.Dltd = 0
	INNER JOIN dbo.ObjectTypeAreaBlock src ON ota.ID = src.ObjectTypeAreaID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeAreaBlock dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to ObjectTypeAreaBlock table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeAreaBlock 
  SET ObjectTypeAreaID = src.ObjectTypeAreaID
    ,ObjectTypeBlockID = src.ObjectTypeBlockID
    ,TemplateAreaBlockID = src.TemplateAreaBlockID
    ,DisplayOrder = src.DisplayOrder
    ,UpdateStamp = src.UpdateStamp
	FROM #Temp 
	INNER JOIN dbo.ObjectTypeArea ota ON #Temp.ObjID = ota.ObjectID
	INNER JOIN dbo.ObjectTypeAreaBlock src ON ota.ID = src.ObjectTypeAreaID
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeAreaBlock dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in ObjectTypeAreaBlock table.'

-- ===================================================
-- ObjectTypeFAQ Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeFAQ 
	SELECT src.*
	FROM #Temp 
	INNER JOIN dbo.ObjectTypeFAQ src ON #Temp.ObjID = src.ObjectID AND #Temp.Dltd = 0
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeFAQ dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to ObjectTypeFAQ table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeFAQ 
  SET ObjectID = src.ObjectID
    ,UpdateStamp = src.UpdateStamp
    ,StartDate = src.StartDate
    ,EndDate = src.EndDate
    ,Question = src.Question
    ,Answer = src.Answer
	FROM #Temp 
	INNER JOIN dbo.ObjectTypeFAQ src ON #Temp.ObjID = src.ObjectID
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeFAQ dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in ObjectTypeFAQ table.'

-- ===================================================
-- ObjectTypeIncentiveProgram Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeIncentiveProgram 
	SELECT src.*
	FROM #Temp 
	INNER JOIN dbo.ObjectTypeIncentiveProgram src ON #Temp.ObjID = src.ObjectID AND #Temp.Dltd = 0
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeIncentiveProgram dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to ObjectTypeIncentiveProgram table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeIncentiveProgram 
  SET ObjectID = src.ObjectID
    ,ClientID = src.ClientID
    ,SiteID = src.SiteID
    ,ProgramCategoryID = src.ProgramCategoryID
    ,ProgramTypeID = src.ProgramTypeID
    ,DKNumber = src.DKNumber
    ,Description = src.Description
    ,StartDate = src.StartDate
    ,EndDate = src.EndDate
    ,UpdateStamp = src.UpdateStamp
    ,HierarchyID = src.HierarchyID
	FROM #Temp 
	INNER JOIN dbo.ObjectTypeIncentiveProgram src ON #Temp.ObjID = src.ObjectID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeIncentiveProgram dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in ObjectTypeIncentiveProgram table.'

-- ===================================================
-- BlockObject Table
-- ===================================================

	-- Add data that doesn't exist in production
	INSERT INTO SQL2KPRODZEBRA.CMZebra.dbo.BlockObject 
	SELECT DISTINCT src.*
	FROM #Temp 
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeBlock otb ON #Temp.ObjID = otb.ObjectID AND #Temp.Dltd = 0
	INNER JOIN dbo.BlockObject src ON otb.ID = src.ObjectTypeBlockID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.BlockObject dest ON src.ID = dest.ID
	WHERE dest.ID IS NULL
	ORDER BY src.ID

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) added to BlockObject table.'

	-- Update data that has been updated in development
	UPDATE SQL2KPRODZEBRA.CMZebra.dbo.BlockObject 
  SET ObjectID = src.ObjectID
    ,ObjectTypeBlockID = src.ObjectTypeBlockID
    ,DisplayOrder = src.DisplayOrder
    ,UpdateStamp = src.UpdateStamp
	FROM #Temp 
	INNER JOIN SQL2KPRODZEBRA.CMZebra.dbo.ObjectTypeBlock otb ON #Temp.ObjID = otb.ObjectID
	INNER JOIN dbo.BlockObject src ON otb.ID = src.ObjectTypeBlockID
	LEFT JOIN SQL2KPRODZEBRA.CMZebra.dbo.BlockObject dest ON src.ID = dest.ID
	WHERE src.UpdateStamp > dest.UpdateStamp

	-- Add record to Report table
	INSERT INTO #Report
	SELECT CONVERT(VARCHAR, @@Rowcount) + ' record(s) updated in BlockObject table.'

	-- Commit transaction
--	COMMIT TRAN

	-- Refresh entire cache
	exec SQL2KPRODZEBRA.CMZebra.dbo.up_CachedUserRolesInsert '<CachedUserRoles Delete="1"/>'

FINALLY:
	-- Display report
	SELECT * FROM #Report

	-- Clean up
	IF OBJECT_ID('tempdb..#Temp') IS NOT NULL	DROP TABLE #Temp
	IF OBJECT_ID('tempdb..#Report') IS NOT NULL DROP TABLE #Report
	IF OBJECT_ID('tempdb..#Users') IS NOT NULL DROP TABLE #Users

GO
