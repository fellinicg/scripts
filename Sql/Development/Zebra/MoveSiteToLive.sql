DECLARE @RowsInserted INT

-- Initialize variable.
SET @RowsInserted = 0

-- Create Temporary table.
CREATE TABLE [#Temp](
	[ObjID] [int]  NOT NULL
)

-- Insert ID of Site into temporary table.
INSERT INTO #Temp
VALUES (1)

-- Set variable to number of rows affected.
SET @RowsInserted = @@ROWCOUNT

-- Loop until no more rows have been affected.
WHILE @RowsInserted > 0
	BEGIN
		INSERT INTO #Temp
		SELECT DISTINCT obj.ID
		FROM dbo.Object obj
		INNER JOIN #Temp t ON obj.ParentID = t.ObjID
		LEFT JOIN #Temp t2 ON obj.ID = t2.ObjID
		WHERE t2.ObjID IS NULL
		AND obj.Deleted = 0

		SET @RowsInserted = @@ROWCOUNT
	END

INSERT INTO [SQL2KPRODZEBRA].[CMZebra].[dbo].[Object] 
--(
--Deleted, 
--EndDate, 
--ID, 
--LastModified, 
--Name, 
--ObjectGUID, 
--ObjectTypeID, 
--ParentID, 
--PendingApproval, 
--StartDate, 
--UpdateStamp)
SELECT dbo.Object.*
--dbo.Object.Deleted, 
--dbo.Object.EndDate, 
--dbo.Object.ID, 
--dbo.Object.LastModified, 
--dbo.Object.Name, 
--dbo.Object.ObjectGUID, 
--dbo.Object.ObjectTypeID, 
--dbo.Object.ParentID, 
--dbo.Object.PendingApproval, 
--dbo.Object.StartDate, 
--dbo.Object.UpdateStamp
----CONVERT(BIGINT, dbo.Object.UpdateStamp)
FROM #Temp 
INNER JOIN dbo.Object ON ObjID = ID
LEFT JOIN [SQL2KPRODZEBRA].[CMZebra].[dbo].[Object] dest ON dbo.Object.ID = dest.ID
WHERE dest.ID IS NULL
ORDER BY ObjID

--SELECT *
--FROM #Temp 
--INNER JOIN dbo.BlockObject ON ObjID = ObjectID
--ORDER BY ID
--
DROP TABLE #Temp

--SELECT DISTINCT obj.ID 
--FROM dbo.Object obj
--INNER JOIN #temp t ON obj.ParentID = t.objid
--LEFT JOIN #temp t2 ON obj.ID = t2.objid
--WHERE t2.objid IS NULL
 
