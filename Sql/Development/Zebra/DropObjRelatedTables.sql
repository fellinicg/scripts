/****** Object:  Table [dbo].[BlockObjectHistory]    Script Date: 09/28/2005 14:03:14 ******/
USE [CMZebra]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_sBlockObject_Object]') AND type = 'F')
ALTER TABLE [dbo].[BlockObjectHistory] DROP CONSTRAINT [FK_sBlockObject_Object]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_sBlockObject_ObjectTypeBlock]') AND type = 'F')
ALTER TABLE [dbo].[BlockObjectHistory] DROP CONSTRAINT [FK_sBlockObject_ObjectTypeBlock]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BlockObjectHistory]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[BlockObjectHistory]

/****** Object:  Table [dbo].[BlockObjectDeletionHistory]    Script Date: 09/28/2005 14:03:33 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BlockObjectDeletionHistory]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[BlockObjectDeletionHistory]

/****** Object:  Table [dbo].[BlockObject]    Script Date: 09/28/2005 14:03:47 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_BlockObject_Object]') AND type = 'F')
ALTER TABLE [dbo].[BlockObject] DROP CONSTRAINT [FK_BlockObject_Object]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ObjectBlock_ObjectTypeBlock]') AND type = 'F')
ALTER TABLE [dbo].[BlockObject] DROP CONSTRAINT [FK_ObjectBlock_ObjectTypeBlock]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BlockObject]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[BlockObject]

/****** Object:  Table [dbo].[ObjectApprovalHistory]    Script Date: 09/28/2005 13:53:26 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ObjectApprovalHistory_ObjectHistory]') AND type = 'F')
ALTER TABLE [dbo].[ObjectApprovalHistory] DROP CONSTRAINT [FK_ObjectApprovalHistory_ObjectHistory]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ObjectApprovalHistory_User]') AND type = 'F')
ALTER TABLE [dbo].[ObjectApprovalHistory] DROP CONSTRAINT [FK_ObjectApprovalHistory_User]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ObjectApprovalHistory]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ObjectApprovalHistory]

/****** Object:  Table [dbo].[ObjectHistory]    Script Date: 09/28/2005 13:54:04 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ObjectHistory_Object]') AND type = 'F')
ALTER TABLE [dbo].[ObjectHistory] DROP CONSTRAINT [FK_ObjectHistory_Object]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ObjectHistory_ObjectType]') AND type = 'F')
ALTER TABLE [dbo].[ObjectHistory] DROP CONSTRAINT [FK_ObjectHistory_ObjectType]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ObjectHistory_User]') AND type = 'F')
ALTER TABLE [dbo].[ObjectHistory] DROP CONSTRAINT [FK_ObjectHistory_User]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ObjectHistory]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ObjectHistory]

/****** Object:  Table [dbo].[ObjectType]    Script Date: 09/28/2005 13:54:53 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ObjectType]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ObjectType]

/****** Object:  Table [dbo].[ObjectHierarchy]    Script Date: 09/28/2005 13:56:13 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ObjectHierarchy_Hierarchy]') AND type = 'F')
ALTER TABLE [dbo].[ObjectHierarchy] DROP CONSTRAINT [FK_ObjectHierarchy_Hierarchy]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ObjectHierarchy_Object]') AND type = 'F')
ALTER TABLE [dbo].[ObjectHierarchy] DROP CONSTRAINT [FK_ObjectHierarchy_Object]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ObjectHierarchy]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ObjectHierarchy]

/****** Object:  Table [dbo].[ObjectTypeFAQ]    Script Date: 09/28/2005 13:57:30 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ObjectTypeFAQ]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ObjectTypeFAQ]

/****** Object:  Table [dbo].[ObjectPropertyType]    Script Date: 09/28/2005 13:58:20 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ObjectPropertyType]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ObjectPropertyType]

/****** Object:  Table [dbo].[ObjectResponse]    Script Date: 09/28/2005 13:59:13 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ObjectResponse_User]') AND type = 'F')
ALTER TABLE [dbo].[ObjectResponse] DROP CONSTRAINT [FK_ObjectResponse_User]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ObjectResponse]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ObjectResponse]

/****** Object:  Table [dbo].[ObjectTypeAwardCriteria]    Script Date: 09/28/2005 13:59:36 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ObjectTypeAwardCriteria_Object]') AND type = 'F')
ALTER TABLE [dbo].[ObjectTypeAwardCriteria] DROP CONSTRAINT [FK_ObjectTypeAwardCriteria_Object]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ObjectTypeAwardCriteria_ObjectTypeIncentiveProgram]') AND type = 'F')
ALTER TABLE [dbo].[ObjectTypeAwardCriteria] DROP CONSTRAINT [FK_ObjectTypeAwardCriteria_ObjectTypeIncentiveProgram]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ObjectTypeAwardCriteria]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ObjectTypeAwardCriteria]

/****** Object:  Table [dbo].[ObjectRole]    Script Date: 09/28/2005 14:01:13 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ObjectAccess_Object]') AND type = 'F')
ALTER TABLE [dbo].[ObjectRole] DROP CONSTRAINT [FK_ObjectAccess_Object]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ObjectAccess_Role]') AND type = 'F')
ALTER TABLE [dbo].[ObjectRole] DROP CONSTRAINT [FK_ObjectAccess_Role]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ObjectRole]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ObjectRole]

/****** Object:  Table [dbo].[ObjectScheme]    Script Date: 09/28/2005 14:01:38 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ObjectScheme]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ObjectScheme]

/****** Object:  Table [dbo].[ObjectTypeAreaBlock]    Script Date: 09/28/2005 14:04:30 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ObjectTypeBlockArea_ObjectTypeArea]') AND type = 'F')
ALTER TABLE [dbo].[ObjectTypeAreaBlock] DROP CONSTRAINT [FK_ObjectTypeBlockArea_ObjectTypeArea]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ObjectTypeBlockArea_ObjectTypeBlock]') AND type = 'F')
ALTER TABLE [dbo].[ObjectTypeAreaBlock] DROP CONSTRAINT [FK_ObjectTypeBlockArea_ObjectTypeBlock]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ObjectTypeAreaBlock]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ObjectTypeAreaBlock]

/****** Object:  Table [dbo].[ObjectTypeBlock]    Script Date: 09/28/2005 14:02:20 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ObjectTypeBlock]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ObjectTypeBlock]

/****** Object:  Table [dbo].[ObjectTypeArea]    Script Date: 09/28/2005 14:04:58 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ObjectTypeArea_Object]') AND type = 'F')
ALTER TABLE [dbo].[ObjectTypeArea] DROP CONSTRAINT [FK_ObjectTypeArea_Object]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ObjectTypeArea_ObjectTypePage]') AND type = 'F')
ALTER TABLE [dbo].[ObjectTypeArea] DROP CONSTRAINT [FK_ObjectTypeArea_ObjectTypePage]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ObjectTypeArea]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ObjectTypeArea]

/****** Object:  Table [dbo].[ObjectTypePage]    Script Date: 09/28/2005 14:06:26 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ObjectTypePage_Object]') AND type = 'F')
ALTER TABLE [dbo].[ObjectTypePage] DROP CONSTRAINT [FK_ObjectTypePage_Object]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ObjectTypePage_ObjectTypeSite]') AND type = 'F')
ALTER TABLE [dbo].[ObjectTypePage] DROP CONSTRAINT [FK_ObjectTypePage_ObjectTypeSite]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ObjectTypePage]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ObjectTypePage]

/****** Object:  Table [dbo].[ContentHistory]    Script Date: 09/28/2005 14:07:39 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ContentHistory_Object]') AND type = 'F')
ALTER TABLE [dbo].[ContentHistory] DROP CONSTRAINT [FK_ContentHistory_Object]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ContentHistory_ObjectProperty]') AND type = 'F')
ALTER TABLE [dbo].[ContentHistory] DROP CONSTRAINT [FK_ContentHistory_ObjectProperty]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ContentHistory_ObjectTypeSite]') AND type = 'F')
ALTER TABLE [dbo].[ContentHistory] DROP CONSTRAINT [FK_ContentHistory_ObjectTypeSite]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ContentHistory]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ContentHistory]

/****** Object:  Table [dbo].[ObjectTypeSite]    Script Date: 09/28/2005 14:06:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ObjectTypeSite_Object]') AND type = 'F')
ALTER TABLE [dbo].[ObjectTypeSite] DROP CONSTRAINT [FK_ObjectTypeSite_Object]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ObjectTypeSite]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ObjectTypeSite]

/****** Object:  Table [dbo].[ObjectProperty]    Script Date: 09/28/2005 14:08:58 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ObjectProperty]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ObjectProperty]

/****** Object:  Table [dbo].[Object]    Script Date: 09/28/2005 14:09:14 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Object]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Object]

/****** Object:  Table [dbo].[PointBoundary]    Script Date: 09/28/2005 14:10:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_PointBoundary_ObjectTypeIncentiveProgram]') AND type = 'F')
ALTER TABLE [dbo].[PointBoundary] DROP CONSTRAINT [FK_PointBoundary_ObjectTypeIncentiveProgram]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_PointBoundary_ProgramType]') AND type = 'F')
ALTER TABLE [dbo].[PointBoundary] DROP CONSTRAINT [FK_PointBoundary_ProgramType]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PointBoundary]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[PointBoundary]

/****** Object:  Table [dbo].[ProgramScheme]    Script Date: 09/28/2005 14:10:51 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ProgramScheme_ObjectTypeIncentiveProgram]') AND type = 'F')
ALTER TABLE [dbo].[ProgramScheme] DROP CONSTRAINT [FK_ProgramScheme_ObjectTypeIncentiveProgram]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ProgramScheme]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ProgramScheme]

/****** Object:  Table [dbo].[BudgetAllocation]    Script Date: 09/28/2005 14:11:25 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_BudgetAllocation_Budget]') AND type = 'F')
ALTER TABLE [dbo].[BudgetAllocation] DROP CONSTRAINT [FK_BudgetAllocation_Budget]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_BudgetAllocation_Modifier]') AND type = 'F')
ALTER TABLE [dbo].[BudgetAllocation] DROP CONSTRAINT [FK_BudgetAllocation_Modifier]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_BudgetAllocation_User]') AND type = 'F')
ALTER TABLE [dbo].[BudgetAllocation] DROP CONSTRAINT [FK_BudgetAllocation_User]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BudgetAllocation]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[BudgetAllocation]

/****** Object:  Table [dbo].[Budget]    Script Date: 09/28/2005 14:11:35 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_Budget_ObjectTypeIncentiveProgram]') AND type = 'F')
ALTER TABLE [dbo].[Budget] DROP CONSTRAINT [FK_Budget_ObjectTypeIncentiveProgram]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_Budget_User]') AND type = 'F')
ALTER TABLE [dbo].[Budget] DROP CONSTRAINT [FK_Budget_User]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Budget]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Budget]

/****** Object:  Table [dbo].[ObjectTypeIncentiveProgram]    Script Date: 09/28/2005 14:12:00 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ObjectTypeIncentiveProgram]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ObjectTypeIncentiveProgram]
