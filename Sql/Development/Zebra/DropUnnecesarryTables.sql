USE [CMZebra]

/****** Object:  Table [dbo].[BatchConfig]    Script Date: 09/28/2005 11:36:11 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BatchConfig]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[BatchConfig]

/****** Object:  Table [dbo].[BatchConfigFields]    Script Date: 09/28/2005 11:36:35 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BatchConfigFields]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[BatchConfigFields]

/****** Object:  Table [dbo].[BatchConfigMap]    Script Date: 09/28/2005 11:37:06 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BatchConfigMap]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[BatchConfigMap]

/****** Object:  Table [dbo].[BatchConfigOrganizations]    Script Date: 09/28/2005 11:37:19 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BatchConfigOrganizations]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[BatchConfigOrganizations]

/****** Object:  Table [dbo].[BatchHistory]    Script Date: 09/28/2005 11:37:32 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BatchHistory]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[BatchHistory]

/****** Object:  Table [dbo].[BatchOverride]    Script Date: 09/28/2005 11:37:41 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BatchOverride]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[BatchOverride]

/****** Object:  Table [dbo].[BatchUserImport]    Script Date: 09/28/2005 11:37:49 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BatchUserImport]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[BatchUserImport]

/****** Object:  Table [dbo].[BatchUserImport_9_16]    Script Date: 09/28/2005 11:37:56 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BatchUserImport_9_16]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[BatchUserImport_9_16]

/****** Object:  Table [dbo].[BatchUserTerritory]    Script Date: 09/28/2005 11:38:07 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BatchUserTerritory]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[BatchUserTerritory]

/****** Object:  Table [dbo].[Import_Address]    Script Date: 09/28/2005 11:38:57 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Import_Address]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Import_Address]

/****** Object:  Table [dbo].[Import_Email]    Script Date: 09/28/2005 11:39:06 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Import_Email]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Import_Email]

/****** Object:  Table [dbo].[Import_Organization]    Script Date: 09/28/2005 11:39:14 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Import_Organization]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Import_Organization]

/****** Object:  Table [dbo].[Import_OrganizationLevel]    Script Date: 09/28/2005 11:39:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Import_OrganizationLevel]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Import_OrganizationLevel]

/****** Object:  Table [dbo].[Import_Person]    Script Date: 09/28/2005 11:39:31 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Import_Person]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Import_Person]

/****** Object:  Table [dbo].[Import_Phone]    Script Date: 09/28/2005 11:39:38 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Import_Phone]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Import_Phone]

/****** Object:  Table [dbo].[Import_User]    Script Date: 09/28/2005 11:39:45 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Import_User]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Import_User]

/****** Object:  Table [dbo].[Import_UserAttributes]    Script Date: 09/28/2005 11:39:52 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Import_UserAttributes]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Import_UserAttributes]

/****** Object:  Table [dbo].[Import_UserOrganizationLevel]    Script Date: 09/28/2005 11:40:02 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Import_UserOrganizationLevel]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Import_UserOrganizationLevel]

/****** Object:  Table [dbo].[Import_UserOrganizationLevelGroup]    Script Date: 09/28/2005 11:40:10 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Import_UserOrganizationLevelGroup]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Import_UserOrganizationLevelGroup]

/****** Object:  Table [dbo].[personbackup9_16_erasingadvo]    Script Date: 09/28/2005 11:40:40 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[personbackup9_16_erasingadvo]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[personbackup9_16_erasingadvo]

/****** Object:  Table [dbo].[TemplateAreaBlock]    Script Date: 09/28/2005 11:41:02 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_TemplateBlockArea_TemplateArea]') AND type = 'F')
ALTER TABLE [dbo].[TemplateAreaBlock] DROP CONSTRAINT [FK_TemplateBlockArea_TemplateArea]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_TemplateBlockArea_TemplateBlock]') AND type = 'F')
ALTER TABLE [dbo].[TemplateAreaBlock] DROP CONSTRAINT [FK_TemplateBlockArea_TemplateBlock]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TemplateAreaBlock]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[TemplateAreaBlock]

/****** Object:  Table [dbo].[TemplateArea]    Script Date: 09/28/2005 11:41:34 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_TemplateArea_TemplatePage]') AND type = 'F')
ALTER TABLE [dbo].[TemplateArea] DROP CONSTRAINT [FK_TemplateArea_TemplatePage]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TemplateArea]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[TemplateArea]

/****** Object:  Table [dbo].[TemplateBlockObject]    Script Date: 09/28/2005 11:42:24 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_TemplateBlockObject_TemplateBlock]') AND type = 'F')
ALTER TABLE [dbo].[TemplateBlockObject] DROP CONSTRAINT [FK_TemplateBlockObject_TemplateBlock]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_TemplateBlockObject_TemplateObject]') AND type = 'F')
ALTER TABLE [dbo].[TemplateBlockObject] DROP CONSTRAINT [FK_TemplateBlockObject_TemplateObject]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TemplateBlockObject]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[TemplateBlockObject]


/****** Object:  Table [dbo].[TemplateObjectProperty]    Script Date: 09/28/2005 11:43:12 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_TemplateObjectProperty_TemplateObject]') AND type = 'F')
ALTER TABLE [dbo].[TemplateObjectProperty] DROP CONSTRAINT [FK_TemplateObjectProperty_TemplateObject]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TemplateObjectProperty]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[TemplateObjectProperty]

/****** Object:  Table [dbo].[TemplateBlock]    Script Date: 09/28/2005 11:44:37 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TemplateBlock]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[TemplateBlock]

/****** Object:  Table [dbo].[TemplateObject]    Script Date: 09/28/2005 11:45:16 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_TemplateObject_ObjectType]') AND type = 'F')
ALTER TABLE [dbo].[TemplateObject] DROP CONSTRAINT [FK_TemplateObject_ObjectType]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_TemplateObject_TemplateObject]') AND type = 'F')
ALTER TABLE [dbo].[TemplateObject] DROP CONSTRAINT [FK_TemplateObject_TemplateObject]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TemplateObject]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[TemplateObject]

/****** Object:  Table [dbo].[TemplateSite]    Script Date: 09/28/2005 11:51:16 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_TemplatePage_TemplateSite]') AND type = 'F')
ALTER TABLE [dbo].[TemplatePage] DROP CONSTRAINT [FK_TemplatePage_TemplateSite]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[fkTemplateSiteTemplateSiteType]') AND type = 'F')
ALTER TABLE [dbo].[TemplateSite] DROP CONSTRAINT [fkTemplateSiteTemplateSiteType]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TemplateSite]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[TemplateSite]

/****** Object:  Table [dbo].[TemplateSiteType]    Script Date: 09/28/2005 11:50:28 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TemplateSiteType]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[TemplateSiteType]

/****** Object:  Table [dbo].[TempPerson]    Script Date: 09/28/2005 15:31:44 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TempPerson]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[TempPerson]