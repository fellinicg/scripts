--Remove identity on ID column in Address table--ALTER TABLE dbo.AddressALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in BlockObject table--ALTER TABLE dbo.BlockObjectALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in BlockObjectDeletionHistory table--ALTER TABLE dbo.BlockObjectDeletionHistoryALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in BlockObjectHistory table--ALTER TABLE dbo.BlockObjectHistoryALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in Budget table--ALTER TABLE dbo.BudgetALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in BudgetAllocation table--ALTER TABLE dbo.BudgetAllocationALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in Client table--ALTER TABLE dbo.ClientALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ContactType table--ALTER TABLE dbo.ContactTypeALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ContentHistory table--ALTER TABLE dbo.ContentHistoryALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in EmailAddress table--ALTER TABLE dbo.EmailAddressALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in EmergencyContacts table--ALTER TABLE dbo.EmergencyContactsALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in Hierarchy table--ALTER TABLE dbo.HierarchyALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in HierarchyUser table--ALTER TABLE dbo.HierarchyUserALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in Object table--ALTER TABLE dbo.ObjectALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ObjectApprovalHistory table--ALTER TABLE dbo.ObjectApprovalHistoryALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ObjectHierarchy table--ALTER TABLE dbo.ObjectHierarchyALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ObjectHistory table--ALTER TABLE dbo.ObjectHistoryALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ObjectProperty table--ALTER TABLE dbo.ObjectPropertyALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ObjectPropertyType table--ALTER TABLE dbo.ObjectPropertyTypeALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ObjectResponse table--ALTER TABLE dbo.ObjectResponseALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ObjectRole table--ALTER TABLE dbo.ObjectRoleALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ObjectScheme table--ALTER TABLE dbo.ObjectSchemeALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ObjectType table--ALTER TABLE dbo.ObjectTypeALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ObjectTypeArea table--ALTER TABLE dbo.ObjectTypeAreaALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ObjectTypeAreaBlock table--ALTER TABLE dbo.ObjectTypeAreaBlockALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ObjectTypeAwardCriteria table--ALTER TABLE dbo.ObjectTypeAwardCriteriaALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ObjectTypeBlock table--ALTER TABLE dbo.ObjectTypeBlockALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ObjectTypeFAQ table--ALTER TABLE dbo.ObjectTypeFAQALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ObjectTypeIncentiveProgram table--ALTER TABLE dbo.ObjectTypeIncentiveProgramALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ObjectTypePage table--ALTER TABLE dbo.ObjectTypePageALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ObjectTypeSite table--ALTER TABLE dbo.ObjectTypeSiteALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in Organization table--ALTER TABLE dbo.OrganizationALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in OrganizationLevel table--ALTER TABLE dbo.OrganizationLevelALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in Person table--ALTER TABLE dbo.PersonALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in Phone table--ALTER TABLE dbo.PhoneALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ProgramCategory table--ALTER TABLE dbo.ProgramCategoryALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ProgramScheme table--ALTER TABLE dbo.ProgramSchemeALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in ProgramType table--ALTER TABLE dbo.ProgramTypeALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in Role table--ALTER TABLE dbo.RoleALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in RoleOrganizationLevel table--ALTER TABLE dbo.RoleOrganizationLevelALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in RolePermission table--ALTER TABLE dbo.RolePermissionALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in RolePermissionType table--ALTER TABLE dbo.RolePermissionTypeALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in RoleUser table--ALTER TABLE dbo.RoleUserALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in RoleUserByProcedure table--ALTER TABLE dbo.RoleUserByProcedureALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in State table--ALTER TABLE dbo.StateALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in TemplatePage table--ALTER TABLE dbo.TemplatePageALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in User table--ALTER TABLE dbo.UserALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in UserAttributes table--ALTER TABLE dbo.UserAttributesALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in UserOrganizationLevel table--ALTER TABLE dbo.UserOrganizationLevelALTER COLUMN ID INT NOT NULL
--Remove identity on ID column in UserOrganizationLevelGroup table--ALTER TABLE dbo.UserOrganizationLevelGroupALTER COLUMN ID INT NOT NULL