USE [ClarityCards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.sp_FastRewardsGetNewCardData
as

--FOR SENDING CARDS TO MADISON
/*
select gu_ID as Userid, replace(u.gu_LastName,' ','') as lastname ,rtrim(u.gu_FirstName) as firstname , rtrim(isnull(em_Address,'fake_' + cast(gu_id as varchar(10)) + '@fake.com')) as email,
'488 Madison Ave.' as STREET1,'3rd floor' as STREET2,'New York' as City,'NY' as State,'10022' as Zip5 from 
SQL2kProd.GlobalUser.dbo.GlobalUsers u
left  JOIN SQL2kProd.GlobalUser.dbo.Email ON gu_ID = em_gu_ID
left join (select u_mpgProgUserid from users where u_p_id =11) U1 on gu_id = u_mpgProgUserid
--WHERE gu_cl_ID = 3 and u_mpgProgUserid is null
WHERE (gu_id between 218 and 254)  and u_mpgProgUserid is null
*/

--new version, by alex
/*
select gu_ID as Userid, replace(u.gu_LastName,' ','') as lastname ,rtrim(u.gu_FirstName) as firstname , 
rtrim(isnull(em_Address,'fake_' + cast(gu_id as varchar(10)) + '@fake.com')) as email,
ADDRESS1 as STREET1,ADDRESS2 as STREET2,City,State,Zip as Zip5 from 
SQL2kProd.GlobalUser.dbo.GlobalUsers u 
join NewFastRewardCards on u_id = gu_id
left  JOIN SQL2kProd.GlobalUser.dbo.Email ON gu_ID = em_gu_ID
left join (select u_mpgProgUserid from users where u_p_id =11) U1 on gu_id = u_mpgProgUserid
--WHERE gu_cl_ID = 3 and u_mpgProgUserid is null
WHERE (gu_id between 218 and 254)  and u_mpgProgUserid is null


--new version by ingo
select gu_ID as Userid, replace(u.gu_LastName,' ','') as lastname ,rtrim(u.gu_FirstName) as firstname , 
rtrim(isnull(em_Address,'fake_' + cast(gu_id as varchar(10)) + '@fake.com')) as email,
ad_StreetAddr1 as STREET1,ad_StreetAddr2 as STREET2,ad_City as City ,ad_State as State ,ad_PostalCode as Zip5 
from SQL2kProd.GlobalUser.dbo.GlobalUsers u 
--join NewFastRewardCards on u_id = gu_id
left  JOIN SQL2kProd.GlobalUser.dbo.Email ON gu_ID = em_gu_ID
left  JOIN SQL2kProd.GlobalUser.dbo.Addresses ON gu_ID = ad_gu_ID
left join (select u_mpgProgUserid from users where u_p_id =11) U1 on gu_id = u_mpgProgUserid
--WHERE gu_cl_ID = 3 and u_mpgProgUserid is null
--WHERE (gu_id between 21630 and 21631)  and u_mpgProgUserid is null
WHERE gu_id = 22030  and u_mpgProgUserid is null
*/

--new version by TMF
SELECT 
	u.UserAccountID as Userid, 
	replace(u.PersonFamilyName,' ','') as lastname,
	rtrim(u.PersonFirstGivenName) as firstname, 
	rtrim(isnull(e.EmailAddress,'fake_' + cast(u.UserAccountID as varchar(10)) + '@fake.com')) as email,
	m.Line1 as STREET1,
	m.Line2 as STREET2,
	m.CityTown as City,
	m.StateProvince as State,
	m.PostalCode as Zip5 
FROM SQL2KPROD.GlobalUsers.dbo.tbUserAccount u 
LEFT JOIN SQL2KPROD.GlobalUsers.dbo.tbEmailAddress e ON u.UserAccountID = e.UserAccountID
LEFT JOIN SQL2KPROD.GlobalUsers.dbo.tbMailAddress m ON e.UserAccountID = m.UserAccountID
LEFT JOIN (SELECT u_mpgProgUserid FROM users WHERE u_p_id = 11) U1 ON u.UserAccountID = U1.u_mpgProgUserid
--WHERE u.ClientID = 3 and u_mpgProgUserid is null
--WHERE (u.UserAccountID between 21630 and 21631)  and u_mpgProgUserid is null
WHERE u.UserAccountID = 34959 
AND U1.u_mpgProgUserid IS NULL

/*
select 10000 as Userid, 'Humbert' as lastname , 'Ann M.' as firstname , 'FakeEmail@FakeDomain.com' as email,
'139 Stephens Lane.' as STREET1,'' as STREET2,'Mahwah' as City,'NJ' as State,'07430' as Zip5
*/

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
