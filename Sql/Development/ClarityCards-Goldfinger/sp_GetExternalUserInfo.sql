USE [ClarityCards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.sp_GetExternalUserInfo @UID varchar(30), @PID int
as
	CREATE TABLE #Users (
	
	[u_MPGProgUserID] [varchar] (30) NULL ,
	[u_p_id]  int  NULL ,
	[u_LastName] [varchar] (50)  NULL ,
	[u_FirstName] [varchar] (50)  NULL ,
	[u_Address1] [varchar] (50)  NULL ,
	[u_Address2] [varchar] (50) NULL ,
	[u_City] [varchar] (50)  NULL ,
	[u_State] [varchar] (50)  NULL ,
	[u_Zip] [varchar] (9)  NULL ,
	[u_Email] [varchar] (50) NULL)
if @PID= 1
	begin
		insert into #Users
		SELECT @UID,@PID,LASTNAME,FIRSTNAME,STREET1,STREET2,CITY,STATE,ZIP5,EMAILID 
		FROM Moonraker.Abbott.DBO.Users where u_id=@UID and len(u_ssn) = 9
	end
if @PID = 5
	begin
		insert into #Users
		SELECT @UID,@PID,u_FirstName,u_LastName,u_Address1,u_Address2,u_CITY,u_STATE,u_ZIP,u_Email
		FROM Roche.DBO.USERS where u_employeeid=@UID
	end
if @PID in (7, 8)
	begin
		select  @UID,@PID ,  FullName as  u_FirstName , 
		u_LastName = '' , u_Address1='',u_Address2='',u_CITY='',u_STATE='',u_ZIP='',u_Email=''
		from Moonraker.Points.DBO.vw_DistinctMCIPeople where p_Userid = @UID
	end
if @PID = 12
	begin
		select  @UID,@PID ,  FirstName as u_FirstName , 
		LastName as u_LastName  , Address1 as u_Address1 , Address2 as u_Address2 , CITY as u_City,STATE as u_State,ZIP as u_Zip,Email as u_Email
		from Moonraker.Matrix.DBO.Users where userid = @UID
	end
if @PID = 13 
	BEGIN
		insert into #Users
		SELECT @UID, @PID, ge.LastName AS u_LastName, ge.FirstName AS u_FirstName, e.HomeAddressLine1 AS u_Address1, e.HomeAddressLine2 AS u_Address2,
		e.HomeAddressCity AS u_City, e.HomeAddressState AS u_State, LEFT(e.HomeAddressPostalCode, 5) AS u_Zip, e.WorkEmailAddress AS u_Email
		FROM SQL2KPRODZEBRA.Baxter.dbo.tbEmployee e
		INNER JOIN SQL2KPRODZEBRA.Baxter.dbo.vwGlobalEmployee ge ON (e.EmployeeID = ge.EmployeeID)
		WHERE e.EmployeeID = @UID
	END

SELECT * FROM #Users

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF