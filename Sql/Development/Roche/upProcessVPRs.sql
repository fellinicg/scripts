SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: March 6, 2006
-- Description:	Process Roche VPRs
-- =============================================
ALTER PROCEDURE dbo.upProcessVPRs 
AS
BEGIN

	DECLARE @bcpcmd VARCHAR(5000)
	DECLARE @masterfile VARCHAR(50)
	DECLARE @RC INT
	DECLARE @vprshare VARCHAR(50)
	DECLARE @vprdir VARCHAR(50)
	DECLARE @vprarchivedir VARCHAR(50)
	DECLARE @vprstagingdir VARCHAR(50)
	DECLARE @srcfiledir VARCHAR(50)
	DECLARE @zipexe VARCHAR(50)
	DECLARE @unzipexe VARCHAR(50)
	DECLARE @timestamp VARCHAR(20)

	-- Initialize variables
	SET NOCOUNT ON
	SET @timestamp = CONVERT(VARCHAR, DATEPART(hh, CURRENT_TIMESTAMP)) + 'h' + CONVERT(VARCHAR, DATEPART(mi, CURRENT_TIMESTAMP)) + 'm' + CONVERT(VARCHAR, DATEPART(ss, CURRENT_TIMESTAMP)) + 's'
	SET @vprshare = '\\thunderball\RocheRewardCentral\RewardCentral'
	SET @vprdir = 'z:\VPR\'
	SET @vprarchivedir = @vprdir + 'Archive\' + @timestamp
	SET @vprstagingdir = 'z:\VPRTEST\'
	SET @srcfiledir = @vprstagingdir + 'SourceFiles\'
	SET @zipexe = @vprstagingdir + 'Winzip\wzzip'
	SET @unzipexe = @vprstagingdir + 'Winzip\wzunzip'

	-- Map share
	SET @bcpcmd = 'net use z: /delete&net use z: ' + @vprshare
	EXECUTE @RC = master..xp_cmdshell @bcpcmd

	-- Delete files in directory
	SET @bcpcmd = 'del ' + @vprstagingdir + '*.* /F /Q'
	EXECUTE @RC = master..xp_cmdshell @bcpcmd

	-- Get latest Master VPR file
	SET @bcpcmd = 'master.dbo.xp_cmdshell "dir ' + @srcfiledir + '*.zip /B /O-D"'
	IF OBJECT_ID('tempdb..#TMP') IS NULL CREATE TABLE #TMP (info varchar(200)) 
	ELSE TRUNCATE TABLE #TMP
	INSERT #TMP EXEC (@bcpcmd)
	SELECT TOP 1 @masterfile = info FROM #TMP
	TRUNCATE TABLE #TMP
	
	-- Unzip Master VPR file
	SET @bcpcmd = @unzipexe + ' "' + @srcfiledir + @masterfile + '" ' + @vprstagingdir
	EXECUTE @RC = master..xp_cmdshell @bcpcmd

	-- Clear RESTORE version of tables
	TRUNCATE TABLE dbo.VPRInfo_RESTORE
	TRUNCATE TABLE dbo.VPR_RESTORE
	TRUNCATE TABLE dbo.UserVPR_RESTORE

	-- Backup PRODUCTION tables in case we need to restore
	INSERT INTO dbo.VPRInfo_RESTORE
	SELECT * FROM dbo.VPRInfo

	INSERT INTO dbo.VPR_RESTORE
	SELECT * FROM dbo.VPR

	INSERT INTO dbo.UserVPR_RESTORE
	SELECT * FROM dbo.UserVPR

  -- Clear VPRInfo_STAGING table
	TRUNCATE TABLE VPRInfo_STAGING

  -- Populate VPRInfo_STAGING table with a union of Production table and new XLS
	SET @bcpcmd = 'INSERT INTO dbo.VPRInfo_STAGING (FileDate, Territory, EmployeeID, Startdate, Enddate, First_name, Last_name, Title, JBID)
	SELECT FileDate, Territory, Startdate, Enddate, First_name, Last_name, Title, JBID
	FROM dbo.VPRInfo
	UNION ALL
	SELECT FileDate, Territory, EmployeeID, Startdate, Enddate, First_name, Last_name, Title, 0 
	FROM 
	OPENROWSET
	(
	''Microsoft.Jet.OLEDB.4.0'',
	''Excel 8.0;Database=' + @vprstagingdir + 'EISRoster.xls; HDR=yes'',
	''SELECT * FROM [a:o20000]''
	)'
	EXECUTE (@bcpcmd)

	-- Rebuild VPR_STAGING table
	TRUNCATE TABLE dbo.VPR_STAGING

	SET IDENTITY_INSERT dbo.VPR_STAGING ON

	INSERT INTO dbo.VPR_STAGING (vpr_Id, vpr_URL, vpr_Code)
	SELECT * 
	FROM dbo.VPR

	SET IDENTITY_INSERT dbo.VPR_STAGING OFF

	-- Append XLS data to VPR_STAGING table
	SET @bcpcmd = 'master.dbo.xp_cmdshell "dir ' + @vprstagingdir + 'RR_*.* /B"'
--	IF OBJECT_ID('tempdb..#TMP') IS NULL CREATE TABLE #TMP (info varchar(200)) 
--	ELSE TRUNCATE TABLE #TMP
	INSERT #TMP EXEC (@bcpcmd)
	INSERT INTO dbo.VPR_STAGING (vpr_URL)
	SELECT info 
	FROM #TMP 
	WHERE info IS NOT NULL
	DROP TABLE #TMP

	-- Clear local UserVPR_STAGING table
	TRUNCATE TABLE dbo.UserVPR_STAGING

	-- Individual report
	INSERT INTO dbo.UserVPR_STAGING
	SELECT * 
	FROM (
		SELECT 
		LEFT(RIGHT(vpr_url,10),6) employeeid, UPPER(SUBSTRING(vpr_url,11,8)) terrcode, 
		SUBSTRING(vpr_url,4,6) vprdate, 
		vpr_id vprid, 9 vprtype 
		FROM VPR_STAGING) t2 
	WHERE employeeid >'0' 
	AND employeeid NOT LIKE '%.%' 
	AND employeeid NOT LIKE '%[a-z]%'
	ORDER BY employeeid, vprdate

  -- Terranalysis regular report
	INSERT INTO dbo.UserVPR_STAGING
  SELECT DISTINCT uv_employeeid, t.* 
	FROM (
		SELECT UPPER(LEFT(RIGHT(vpr_url,12),8)) terrcode, 
		SUBSTRING(vpr_url,4,6) vprdate, 
		vpr_id vprid, 
		13 vprtype 
		FROM VPR_STAGING 
		WHERE vpr_url LIKE '%terranalysis%' 
		AND UPPER(LEFT(RIGHT(vpr_url,12),8)) NOT LIKE 'IS%') t 
		JOIN UserVPR_STAGING ON terrcode = uv_terrcode AND vprdate = uv_vprdate
  
  -- Terranalysis dsm report
	INSERT INTO dbo.UserVPR_STAGING
  SELECT uv_EmployeeID, t.* 
	FROM (
		SELECT UPPER(LEFT(RIGHT(vpr_url,9),5)) + '00' terrcode, 
		SUBSTRING(vpr_url,4,6) vprdate, 
		vpr_id vprid, 13 vprtype 
		FROM VPR_STAGING 
		WHERE vpr_url LIKE '%terranalysis%' 
		AND UPPER(LEFT(RIGHT(vpr_url,12),8)) LIKE 'IS%') t 
	JOIN UserVPR_STAGING ON terrcode = SUBSTRING(uv_terrcode,1,7) AND vprdate = uv_vprdate
  
  -- Divperformance report
	INSERT INTO dbo.UserVPR_STAGING
  SELECT uv_employeeid, t.* 
	FROM (
		SELECT UPPER(LEFT(RIGHT(vpr_url,9),5)) + '00' terrcode, 
		SUBSTRING(vpr_url,4,6) vprdate, 
		vpr_id vprid, 
		12 vprtype 
		FROM VPR_STAGING
		WHERE vpr_url LIKE '%divp%') t 
	JOIN UserVPR_STAGING ON terrcode = substring(uv_terrcode,1,7) AND vprdate = uv_vprdate 
	WHERE LEN(uv_terrcode)=8
  
  -- Summary dm report
	INSERT INTO dbo.UserVPR_STAGING
  SELECT uv_employeeid, t.* 
	FROM (
		SELECT SUBSTRING(vpr_url,11,5) + '00' terrcode, 
		SUBSTRING(vpr_url,4,6) vprdate, 
		vpr_id vprid, 
		11 vprtype 
		FROM vpr WHERE vpr_url LIKE '%_dsm.%') t 
	JOIN UserVPR_STAGING ON terrcode = SUBSTRING(uv_terrcode,1,7) AND vprdate = uv_vprdate AND LEN(uv_Terrcode) = 8
	ORDER BY vprdate,uv_employeeid


  -- Create zip files
	SELECT DISTINCT 'master.dbo.xp_cmdshell "' + @zipexe + ' -a ' + @vprstagingdir + SUBSTRING(uv_terrcode,1,5) + '.zip ' + @vprstagingdir + vpr_url + '"' zipcmd
	INTO #PDF
	FROM dbo.UserVPR_STAGING 
	JOIN dbo.VPR_STAGING ON uv_vprid = vpr_id 
	WHERE uv_terrcode LIKE SUBSTRING(uv_terrcode,1,5) +'%' 
	AND uv_VPRDate = (SELECT MAX(uv_VPRDate) FROM dbo.UserVPR_STAGING)

	WHILE EXISTS (SELECT 1 FROM #PDF)
		BEGIN
			SELECT TOP 1 @bcpcmd = zipcmd FROM #PDF ORDER BY zipcmd
			EXECUTE (@bcpcmd)
			DELETE FROM #PDF WHERE zipcmd = (SELECT TOP 1 zipcmd FROM #PDF ORDER BY zipcmd)
		END

	DROP TABLE #PDF

	-- Archive PRODUCTION zips
	SET @bcpcmd = 'mkdir ' + @vprarchivedir + '&move ' + @vprdir + '*.zip ' + @vprarchivedir + '\'
	EXECUTE @RC = master..xp_cmdshell @bcpcmd

	-- Copy Files to PRODUCTION
	SET @bcpcmd = 'copy ' + @vprstagingdir + '*.* ' + @vprdir + ' /Y'
	EXECUTE @RC = master..xp_cmdshell @bcpcmd

	-- Clear PRODUCTION version of tables
	TRUNCATE TABLE dbo.VPRInfo
	TRUNCATE TABLE dbo.VPR
	TRUNCATE TABLE dbo.UserVPR

	-- Update PRODUCTION tables
	INSERT INTO dbo.VPRInfo
	SELECT * FROM dbo.VPRInfo_STAGING

	SET IDENTITY_INSERT dbo.VPR ON
	INSERT INTO dbo.VPR (vpr_Id, vpr_URL, vpr_Code)
	SELECT * FROM dbo.VPR_STAGING
	SET IDENTITY_INSERT dbo.VPR OFF

	INSERT INTO dbo.UserVPR
	SELECT * FROM dbo.UserVPR_STAGING

	-- Run some stored proc Ross ran in VB
	EXECUTE dbo.sp_AddJBCodeToVPRInfo

END
GO
