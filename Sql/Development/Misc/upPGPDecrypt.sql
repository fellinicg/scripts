SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: January 26, 2006
-- Description:	Decrypt PGP file
-- =============================================
CREATE PROCEDURE upPGPDecrypt 
	@File VARCHAR(200)
	, @Passphrase VARCHAR(200)
AS
BEGIN
	DECLARE @cmd VARCHAR(5000)

	SET @cmd = 'net use z: /DELETE&net use z: \\reports\pgpcmd'
	EXECUTE Reports.master..xp_cmdshell @cmd

	SET @cmd = 'pgp.exe --decrypt z:\input\encrypted\' + @File + ' --output z:\output\unencrypted --symmetric-passphrase "' + @Passphrase + '" --overwrite remove --verbose'
	EXECUTE Reports.master..xp_cmdshell @cmd

	SET @cmd = 'net use z: /DELETE'
	EXECUTE Reports.master..xp_cmdshell @cmd

END
GO
