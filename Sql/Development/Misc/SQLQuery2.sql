DECLARE @cmd VARCHAR(5000)

TRUNCATE TABLE tbUrl
TRUNCATE TABLE tbPax

SET @cmd = 'bcp Onvantage.dbo.tbUrl in D:\url.csv -w -F2 -T -STODD'
EXECUTE master..xp_cmdshell @cmd

UPDATE tbUrl SET 
	FirstName = LTRIM(RTRIM(REPLACE(FirstName, '"', '')))
	,LastName = LTRIM(RTRIM(REPLACE(LastName, '"', '')))
	,BusEmail = LTRIM(RTRIM(REPLACE(BusEmail, '"', '')))
	,HomeEmail = LTRIM(RTRIM(REPLACE(HomeEmail, '"', '')))
	,Url = LTRIM(RTRIM(REPLACE(Url, '"', '')))

SET @cmd = 'bcp Onvantage.dbo.tbPax in D:\report.csv -w -F2 -T -STODD'
EXECUTE master..xp_cmdshell @cmd

UPDATE tbPax SET 
	TransactionNum = LTRIM(RTRIM(REPLACE(TransactionNum, '"', '')))
	,PaxID = LTRIM(RTRIM(REPLACE(PaxID, '"', '')))
	,FirstName = LTRIM(RTRIM(REPLACE(FirstName, '"', '')))
	,LastName = LTRIM(RTRIM(REPLACE(LastName, '"', '')))
	,GuestList = LTRIM(RTRIM(REPLACE(GuestList, '"', '')))
	,GuestOf = LTRIM(RTRIM(REPLACE(GuestOf, '"', '')))
	,PrimaryReg = LTRIM(RTRIM(REPLACE(PrimaryReg, '"', '')))
	,RSVPStatus = LTRIM(RTRIM(REPLACE(RSVPStatus, '"', '')))
	,BusEmail = LTRIM(RTRIM(REPLACE(BusEmail, '"', '')))
	,BusPhone = LTRIM(RTRIM(REPLACE(BusPhone, '"', '')))
	,SeatPref = LTRIM(RTRIM(REPLACE(SeatPref, '"', '')))
	,Airline1 = LTRIM(RTRIM(REPLACE(Airline1, '"', '')))
	,Airline2 = LTRIM(RTRIM(REPLACE(Airline2, '"', '')))
