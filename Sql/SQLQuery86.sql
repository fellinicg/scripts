declare	@ListingCategory int = 8
declare @ActiveInactive varchar(3) = '0,1'
declare @ListingStatus varchar(max) = '163,157' --null
declare @WebOrListingNum varchar(max) = null
declare @PriceMin int = null
declare @PriceMax int = null
declare @Bedrooms float = null
declare @Bathrooms float = null
declare @HalfBathrooms float = null
declare @Rooms float = null
declare @OwnershipType varchar(max) = null
declare @ListDateStart date = null
declare @ListDateEnd date = null
declare @UpdateDateStart date = null
declare @UpdateDateEnd date = null
declare @ListUpdateDateStart date = null
declare @ListUpdateDateEnd date = null
declare @OpenHouseStart date = null
declare @OpenHouseEnd date = null
declare @MinSqFt int = null
declare @MaxPriceSqFt int = null
declare @IncMissingSqFt bit = 1
declare @MaxMthlyExp int = null
declare @Neighborhood varchar(max) = null
declare @BuildingPersonnel varchar(max) = null
declare @BuildingAllow varchar(max) = null
declare @BuildingNotAllow varchar(max) = null
declare @ListActivityStart date = null
declare @ListActivityEnd date = null
declare @ListActivityStatus int = null
declare @OutdoorSpace varchar(max) = null
declare @Views varchar(max) = null
declare @Exposure varchar(max) = null
declare @MinYearBuilt int = null
declare @ListingType varchar(max) = null
declare @AddressID varchar(max) = null


	-- Declarations
	declare @@TblListingCategory table (ListingID int index ix1 clustered, IsRemoved bit)
	declare @TblAddress table (ListingID int index ix1 clustered)
	declare @TblWebOrListingNum table (ListingID int index ix1 clustered)
	declare @TblMinYearBuilt table (ListingID int index ix1 clustered)
	declare @TblBed table (ListingID int index ix1 clustered)
	declare @TblBath table (ListingID int index ix1 clustered)
	declare @@TblStatus table (ListingID int index ix1 clustered)

print cast(SYSDATETIME() as varchar) + ' => START FILTERING'

	-- Build result table
	-- ListingCategory is required, so this becomes our universe
	insert into @@TblListingCategory 
	select ListingID, 0
	from dbo.tbListing 
		inner join dbo.tfCommaListToTable(@ActiveInactive) on IsActive = ListItem
	where IsDeleted = 0
		and ListingCategoryID = @ListingCategory
		and CurrentPrice between isnull(@PriceMin, 0) and isnull(@PriceMax, 1000000000)

print cast(SYSDATETIME() as varchar) + ' => FINISHED REQUIRED FILTERING'

	-- AddressID
	if @AddressID is not null 
		begin
			insert into @TblAddress 
			select l.ListingID
			from dbo.tbPropertyAddress pa
				inner join dbo.tfcommalisttotable(@AddressID) a on pa.PropertyAddressID = a.ListItem
				inner join dbo.tbProperty p on pa.PropertyID = p.ParentPropertyID
				inner join dbo.tbListing l on p.PropertyID = l.PropertyID
			union
			select l.ListingID
			from dbo.tbPropertyAddress pa
				inner join dbo.tfcommalisttotable(@AddressID) a on pa.PropertyAddressID = a.ListItem
				inner join dbo.tbProperty p on pa.PropertyID = p.PropertyID
				inner join dbo.tbListing l on p.PropertyID = l.PropertyID
		end
print cast(SYSDATETIME() as varchar) + ' => FINISHED AddressID FILTERING'
	
	-- WebOrListingNum
	if @WebOrListingNum is not null 
		begin
			insert into @TblWebOrListingNum 
			select ListingID
			from dbo.tfGetListingAttributeText(53)
				inner join dbo.tfCommaListToTable(@WebOrListingNum) on charindex(ListItem, AttributeValue) > 0		--AttributeValue = ListItem
			union
			select ListingID
			from dbo.tfGetListingAttributeText(54)
				inner join dbo.tfCommaListToTable(@WebOrListingNum) on charindex(ListItem, AttributeValue) > 0		--AttributeValue = ListItem
		end
print cast(SYSDATETIME() as varchar) + ' => FINISHED WebOrListingNum FILTERING'
	
	-- MinYearBuilt
	if @MinYearBuilt is not null 
		begin
			insert into @TblMinYearBuilt 
			select ListingID 
			from dbo.tfPropertyAttributeNumeric(106)
			where AttributeValue >= @MinYearBuilt
		end
print cast(SYSDATETIME() as varchar) + ' => FINISHED MinYearBuilt FILTERING'
	
	-- Bedrooms
	if @Bedrooms is not null 
		begin
			insert into @TblBed 
			select ListingID 
			from dbo.tfPropertyAttributeNumeric(11)
			where AttributeValue >= @Bedrooms
		end
	
print cast(SYSDATETIME() as varchar) + ' => FINISHED Bedrooms FILTERING'
	-- Bathrooms
	if @Bathrooms is not null 
		begin
			insert into @TblBath 
			select ListingID 
			from dbo.tfPropertyAttributeNumeric(10)
			where AttributeValue >= @Bathrooms
		end

	-- ListingStatus
	if @ListingStatus is not null 
		begin
			insert into @@TblStatus 
			select a.ListingID 
			from dbo.tbListing a
				inner join dbo.tbListingStatus s on a.CurrentStatusID = s.ListingStatusID
				inner join (select ListItem from dbo.tfCommaListToTable(@ListingStatus)) b on s.StatusID = b.ListItem
		end
print cast(SYSDATETIME() as varchar) + ' => FINISHED ListingStatus FILTERING'

declare @sql varchar(max)

set @sql = 'select ListingID from @@TblListingCategory intersect select ListingID from @@TblStatus'

exec(@sql)
--select a.ListingID
--from @TblListingCategory a
--	inner join @TblStatus b on isnull(a.ListingID, 1) = isnull(b.ListingID, 1)
--	inner join @TblMinYearBuilt c on isnull(b.ListingID, 1) = isnull(c.ListingID, 1)
