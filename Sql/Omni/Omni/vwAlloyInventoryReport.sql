use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwAlloyInventoryReport'))
drop view dbo.vwAlloyInventoryReport
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170331
-- ============================================================
create view dbo.vwAlloyInventoryReport
as
	select 
		Name
		, UDF_MajorApplications
		, Description
		, OS_Name
		, UDF_SCCMHeartbeatDDR
	from AN7.dbo.Computers
	where Type_ID in (
		'629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3' --Server
		,'E00622AC-074D-4F49-9654-8CEA4FC56C4E' --Server ICS
		,'B37E1387-CF14-4562-930D-DD48D5F1837A' --VM Server
		)
go
grant select on dbo.vwAlloyInventoryReport to omni