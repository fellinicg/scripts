use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAcl') and type in (N'U'))
drop table dbo.tbAcl
go

create table dbo.tbAcl(
	AclID varchar(100) not null,
	Acl varchar(500) not null
 constraint PK_tbAcl primary key clustered 
(
	AclID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off