use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbApplication') and type in (N'U'))
drop table dbo.tbApplication
go

create table dbo.tbApplication(
	ApplicationID smallint identity(1,1) not null,
	ApplicationName varchar(100) not null,
	IsDeleted bit not null constraint DF_tbApplication_IsDeleted  default ((0)),
	CreateDT smalldatetime not null constraint DF_tbApplication_CreateDT  default (getdate()),
 constraint PK_tbApplication primary key clustered 
(
	ApplicationID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

