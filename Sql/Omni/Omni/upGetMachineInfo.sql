use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetMachineInfo') and type in (N'P', N'PC'))
drop procedure dbo.upGetMachineInfo
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150320
-- ============================================================
create procedure dbo.upGetMachineInfo
@Machine varchar(20)
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select
		c.Name
		, c.Description
		, c.UDF_MajorApplications
		, s.Status
		, c.Notes
		, c.OS_Name
		, ud.FirstName + ' ' + ud.LastName DetectedUser
		, um.FirstName + ' ' + um.LastName ManualUser
	from AN7.dbo.Computers c
		left join AN7.dbo.Status s on c.Status_ID = s.ID
		left join dbo.tbADUser ud on c.UDF_AMRSUsernameDetected = ud.UserName
		left join dbo.tbADUser um on c.UDF_AMRSUsernameManual = um.UserName
	where charindex(@Machine, Name) > 0

end
go
grant exec on dbo.upGetMachineInfo to omni