use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbADUser') and type in (N'U'))
drop table dbo.tbADUser
go

create table dbo.tbADUser(
	ADUserID int identity(1,1) not null,
	SID varchar(100) not null,
	UserName varchar(200) not null,
	FirstName varchar(100) null,
	LastName varchar(200) null,
	Email varchar(100) null,
	Office varchar(200) null,
	OfficePhone varchar(50) null,
	OU varchar(500) null,
	IsEnabled bit not null constraint DF_tbADUser_IsEnabled  default ((1)),
	CreateDT smalldatetime not null constraint DF_tbADUser_CreateDT  default (getdate()),
	UpdateDT smalldatetime not null constraint DF_tbADUser_UpdateDT  default (getdate()),
 constraint PK_tbADUser primary key clustered 
(
	ADUserID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

