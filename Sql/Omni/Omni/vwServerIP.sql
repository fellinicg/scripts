use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwServerIP'))
drop view dbo.vwServerIP
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140514
-- ============================================================
create view dbo.vwServerIP
as
select c.Name, i.IP_Address 'IPAddress', i.[Primary], i.[Manual], srv.HardwareID, srv.AlloyID
from dbo.vwServerAlloy srv
inner join AN7.dbo.IP_Address_Link_List i on srv.AlloyID = i.Object_ID
inner join AN7.dbo.Computers c on i.Object_ID = c.ID
go
grant select on dbo.vwServerIP to omni