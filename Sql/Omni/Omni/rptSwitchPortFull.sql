select 
	o.Type, c.Name, st.Status, c.MAC, s.SwitchPort, s.InterfaceSpeed, c.Primary_IP_Address
from 
	AN7.dbo.Computers c
	left join dbo.vwSwitchPort s on replace(c.MAC, ':', '') = s.ConnectedMAC
	left join AN7.dbo.Object_Type o on c.Type_ID = o.ID
	left join AN7.dbo.Status st on c.Status_ID = st.ID
where 
	c.MAC is not null
--order by 
--	c.Type_ID, c.Name

select
	o.Type, nn.Name, st.Status, na.MAC_Address, s.SwitchPort, s.IfName, s.InterfaceSpeed, na.Name, na.Type, na.IP_Address
from 
	AD7.dbo.Inv_Network_Adapters na
	inner join dbo.vwSwitchPort s on replace(na.MAC_Address, ':', '') = s.ConnectedMAC
	inner join AD7.dbo.Inv_NetNodes nn on na.Node_ID = nn.ID
	left join AN7.dbo.Computers c on na.Node_ID = c.Import_ID
	left join AN7.dbo.Object_Type o on c.Type_ID = o.ID
	left join AN7.dbo.Status st on c.Status_ID = st.ID

