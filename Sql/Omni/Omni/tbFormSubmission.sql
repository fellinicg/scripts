use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbFormSubmission') and type in (N'U'))
drop table dbo.tbFormSubmission
go

create table dbo.tbFormSubmission(
	FormSubmissionID int identity(1,1) not null,
	TypeID int not null,
	ADUserID int not null,
	IsDeleted bit not null constraint DF_tbFormSubmission_IsDeleted default ((0)),
	CreateDT smalldatetime not null constraint DF_tbFormSubmission_CreateDT  default (getdate()),
 constraint PK_tbFormSubmission primary key clustered 
(
	FormSubmissionID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

