use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddADGroupMembers') and type in (N'P', N'PC'))
drop procedure dbo.upAddADGroupMembers
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20151009
-- ============================================================
create procedure dbo.upAddADGroupMembers
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @usertbl table (SID varchar(100), DistinguishedName varchar(300))
	declare @SID varchar(100), @DistinguishedName varchar(300), @sql varchar(500)	declare @intCharCode int

	-- Initialize variables
	set @intCharCode = 33

	-- Add Primary Group
	insert into dbo.tbADGroupMembers (GroupSID, MemberSID)
	select b.SID, a.SID
	from dbo.tbADUser a
	left join dbo.tbADGroup b on '-' + a.PrimaryGroupID = right(b.SID, charindex('-', reverse(b.SID)) - 1) 
	where PrimaryGroupID is not null

	-- Get all users
	insert into @usertbl
	select SID, DistinguishedName
	from dbo.tbADUser

	-- Add data
	while exists(select 1 from @usertbl)
	begin
		select @SID = SID, @DistinguishedName = DistinguishedName from @usertbl
		set @sql = 'insert into dbo.tbADGroupMember (MemberSID, GroupSID) select ''' + @SID + ''' MemberSID, dbo.sfHexSidToString(objectsid) GroupSID from openquery (ADSI, ''<LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov>;(&(objectClass=group)(member:1.2.840.113556.1.4.1941:=' + @DistinguishedName + '));objectsid;subtree'')'
		begin try
			exec(@sql)
		end try
		begin catch
		end catch
		delete from @usertbl where SID = @SID
	end

end
go