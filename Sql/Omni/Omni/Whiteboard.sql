use Omni

select
	--p.EMPLID
	--, p.NAME
	--, p.USERID
	--, a.UserName
	--, p.status
	--, 
	p.*
from dbo.vwPeoplesoft p
	left join dbo.vwActiveDirectory a on p.EMPLID = a.EmployeeID
where p.Status in ('A','L','P','S','W')
	and a.SID is null

select 
	p.EMPLID
	, p.USERID
	, p.NAME
	, p.[WORK PHONE 1]
	, p.[HIRE DATE]
	, p.STATUS
	, p.[Email Address]
	, a.Name
	, a.GivenName + ' ' + a.Surname
	, a.OfficePhone
	, a.EmployeeID
	, a.Enabled
from dbo.vwPeoplesoft p
	left join dbo.vwActiveDirectory a on p.EMPLID = a.EmployeeID
--inner join dbo.vwActiveDirectory a on p.[Email Address] = a.EmailAddress
where --p.[Email Address] is not null
	--and 
	p.Status in ('A','L','P','S','W')
	and a.employeeID is null
