use Omni
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tfPipeDelimitedToTable') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.tfPipeDelimitedToTable
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160627
-- ============================================================
create function dbo.tfPipeDelimitedToTable (@PipeDelimitedList varchar(max))
returns @ResultTable table (ListItem varchar(500) null index ix1 clustered)
as
begin
	-- Declarations
	declare
		@TokenLen int,
		@Token varchar(120),
		@Remainder varchar(max)

	-- Initialize variables
	set @Remainder = @PipeDelimitedList

	while len(@Remainder) > 0
	begin
		set @TokenLen = case patindex('%|%', @Remainder) 
										when 0 then len(@Remainder) 
										else patindex('%|%', @Remainder) - 1 end
		set @Token = rtrim(ltrim(left(@Remainder, @TokenLen)))
		insert @ResultTable(ListItem) values(@Token)
		set @Remainder = right(@Remainder, len(@Remainder) - @TokenLen)
		if (left(@Remainder, 1) = '|')
			set @Remainder = right(@Remainder, len(@Remainder) - 1)
	end

	if not exists(select 1 from @ResultTable) insert @ResultTable(ListItem) values(null)

	return

end
go