use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwSwitchAlloy'))
drop view dbo.vwSwitchAlloy
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140514
-- ============================================================
create view dbo.vwSwitchAlloy
as
select h.HardwareID, h.AlloyID
from dbo.tbSwitch s
	inner join dbo.tbHardware h on s.HardwareID = h.HardwareID

go
grant select on dbo.vwSwitchAlloy to omni