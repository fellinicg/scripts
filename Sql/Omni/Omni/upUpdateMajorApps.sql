use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateMajorApps') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateMajorApps
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140512
-- ============================================================
create procedure dbo.upUpdateMajorApps
as
begin

	-- Initialize settings
	set nocount on

	-- Update data in Alloy
	update AN7.dbo.Computers set UDF_MajorApplications = convert(nvarchar(max), i.MajorApps)
	from AN7.dbo.Computers c
	inner join openquery(teleas22, 'select MANAGED_OBJ_ID, 
				case charindex(''('', label) 
				when 0 then rtrim(ltrim(label)) else rtrim(ltrim(left(label, charindex(''('', label)-1)))
				end as ServerName, 
				case when charindex(''('', label) > 0 then substring(label, charindex(''('', label) + 1, (charindex('')'', label) - 
				charindex(''('', label))-1) end as MajorApps from IBMDirector.dbo.TWG_MANAGED_OBJECT where STATE = ''ONLINE''') i on i.ServerName = c.Name
	where i.MajorApps <> convert(varchar(max), c.UDF_MajorApplications)

end