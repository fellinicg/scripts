use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfInt8toDateTime') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfInt8toDateTime
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140717
-- ============================================================
go
create function dbo.sfInt8toDateTime(@Int8 bigint)
returns datetime2
as
begin
    return dateadd(mi, (@Int8 / 600000000) + datediff(Minute, GetUTCDate(), GetDate()), cast('1/1/1601' as datetime2)) 
end