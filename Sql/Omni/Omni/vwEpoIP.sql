use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwEpoIP'))
drop view dbo.vwEpoIP
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140904
-- ============================================================
create view dbo.vwEpoIP
as
	select 'ePO' Source, isnull(NetBiosName, IPAddress) Name, IPAddress
	from openquery([TELEDV50], 'select s.NetbiosName, ePO_TELEAV438.dbo.RSDFN_ConvertIntToIPString(i.IPV4) IPAddress
			from ePO_TELEAV438.dbo.RSDInterfaceProperties i
				inner join ePO_TELEAV438.dbo.RSDDetectedSystemProperties s on i.HostID = s.HostID
			union
			select
				NetbiosName
				, ePO_TELEAV438.dbo.RSDFN_ConvertIntToIPString(IPV4) IPAddress
			from ePO_TELEAV438.dbo.RSDDetectedSystemProperties
			union
			select
				ComputerName
				,IPAddress
			from ePO_TELEAV438.dbo.EPOComputerProperties
			where len(isnull(IPAddress, '''')) > 0')
go
grant select on dbo.vwEpoIP to omni