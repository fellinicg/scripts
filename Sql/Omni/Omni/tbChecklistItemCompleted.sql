use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbChecklistItemCompleted') and type in (N'U'))
drop table dbo.tbChecklistItemCompleted
go

create table dbo.tbChecklistItemCompleted(
	ChecklistItemCompletedID int identity(1,1) not null,
	ChecklistItemID int not null,
	Note varchar(1000) not null,
	CompletedBy int not null,
	IsDeleted bit not null constraint DF_tbChecklistItemCompleted_IsDeleted default ((0)),
	CompletedDT smalldatetime not null constraint DF_tbChecklistItemCompleted_CreateDT  default (getdate()),
 constraint PK_tbChecklistItemCompleted primary key clustered 
(
	ChecklistItemCompletedID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

