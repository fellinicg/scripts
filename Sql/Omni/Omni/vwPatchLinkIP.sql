use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwPatchLinkIP'))
drop view dbo.vwPatchLinkIP
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140904
-- ============================================================
create view dbo.vwPatchLinkIP
as

	select 'PatchLink' Source, Name, IPAddress
	from openquery([TELEDV33], 'select distinct AgentName Name, ContactAddress IPAddress from UPCCommon.dbo.EndPointDetail')
go
grant select on dbo.vwPatchLinkIP to omni