use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwSwitchIP'))
drop view dbo.vwSwitchIP
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140514
-- ============================================================
create view dbo.vwSwitchIP
as
select h.Name, h.Primary_IP_Address 'IPAddress', s.HardwareID, s.AlloyID
from dbo.vwSwitchAlloy s
inner join AN7.dbo.Hardware h on s.AlloyID = h.ID
go
grant select on dbo.vwSwitchIP to omni