use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetADUserID') and type in (N'P', N'PC'))
drop procedure dbo.upGetADUserID
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150226
-- ============================================================
create procedure dbo.upGetADUserID
	@SID varchar(100)
	,@UserAccountID int output
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select @UserAccountID = ADUserID
	from dbo.tbADUser
	where SID = @SID
end
go
grant exec on dbo.upGetADUserID to omni