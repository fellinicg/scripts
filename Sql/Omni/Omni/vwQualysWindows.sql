use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwQualysWindows'))
drop view dbo.vwQualysWindows
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150520
-- ============================================================
create view dbo.vwQualysWindows
as
	select sh.QualysScanID, s.ScanDate, s.DataFile, sh.IpAddress, sh.DnsName, sh.Application, sh.OS, sh.AssignedTo, sh.Completed
	from dbo.tbQualysScanHost sh
		inner join dbo.tbQualysScan s on sh.QualysScanID = s.QualysScanID
	where DNSName is not null
		and charindex('windows', OS) <> 0
go