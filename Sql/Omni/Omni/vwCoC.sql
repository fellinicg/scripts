use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwCoC'))
drop view dbo.vwCoC
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140919
-- ============================================================
create view dbo.vwCoC
as
	select n.Name MachineName, d.Name DriveName, d.Serial_Number, c.Description, s.Status
	from AD7.dbo.Inv_Drives_Physical d
	inner join AD7.dbo.Inv_NetNodes n on d.Node_ID = n.ID
	left join AN7.dbo.Computers c on n.ID = c.Import_ID
	left join AN7.dbo.Status s on c.Status_ID = s.ID
	where len(Serial_Number) > 0
		and Media_Type = 'Fixed disk'
go
grant select on dbo.vwCoC to omni