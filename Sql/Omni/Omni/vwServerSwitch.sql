use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwServerSwitch'))
drop view dbo.vwServerSwitch
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140515
-- ============================================================
create view dbo.vwServerSwitch
as
select 
	s.Name 'Server'
	, ssp.MacAddress 'Server Mac Address'
	, ssp.IPAddress 'Server IP Address'
	, sw.Name 'Switch'
	, sp.Port 'Switch Port'
	, sw.SwitchIP 'Switch IP Address'
	, sw.SerialNumber 'Switch Serial Number'
	, sw.Model 'Switch Model'
from dbo.tbServerSwitchPort ssp
	inner join dbo.vwServer s on ssp.HardwareID = s.HardwareID
	inner join dbo.tbSwitchPort sp on ssp.SwitchPortID = sp.SwitchPortID
	inner join dbo.vwSwitch sw on sw.HardwareID = sp.HardwareID
go
grant select on dbo.vwServerSwitch to omni