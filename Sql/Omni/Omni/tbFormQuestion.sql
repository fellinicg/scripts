use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbFormQuestion') and type in (N'U'))
drop table dbo.tbFormQuestion
go

create table dbo.tbFormQuestion(
	FormID int not null,
	QuestionID int not null,
	QuestionNumber tinyint not null,
 constraint PK_tbFormQuestion primary key clustered 
(
	FormID, QuestionID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

