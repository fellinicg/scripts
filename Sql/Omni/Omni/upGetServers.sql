use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetServers') and type in (N'P', N'PC'))
drop procedure dbo.upGetServers
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140221
-- ============================================================
create procedure dbo.upGetServers
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select *
	from dbo.vwServer
end
go
grant exec on dbo.upGetServers to omni