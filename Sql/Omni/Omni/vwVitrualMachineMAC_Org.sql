use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwVitrualMachineMAC_Org'))
drop view dbo.vwVitrualMachineMAC_Org
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140904
-- ============================================================
create view dbo.vwVitrualMachineMAC_Org
as
	select 'VirtualCenter' Source, replace(replace(Name, '.panynj.gov', ''), '.pacorp', '') 'Machine', replace(DEVICE_MAC_ADDRESS, ':', '') 'MACAddress'
	from openquery([PATCDV14], 'select e.VM_ID, v.Name, e.DEVICE_MAC_ADDRESS from dbo.VPX_VIRTUAL_ETHERNET_CARD e inner join VPX_VM_CONFIG_INFO v on e.VM_ID = v.ID
								union
								select e.ID, e.Name, n.MAC_ADDRESS from dbo.VPX_NIC n inner join dbo.VPX_ENTITY e on n.ENTITY_ID = e.ID')
	union
	select 'VirtualCenter' Source, replace(replace(Name, '.panynj.gov', ''), '.pacorp', ''), replace(DEVICE_MAC_ADDRESS, ':', '')
	from openquery([TELEDV12], 'select e.VM_ID, v.Name, e.DEVICE_MAC_ADDRESS from dbo.VPX_VIRTUAL_ETHERNET_CARD e inner join VPX_VM_CONFIG_INFO v on e.VM_ID = v.ID
								union
								select e.ID, e.Name, n.MAC_ADDRESS from dbo.VPX_NIC n inner join dbo.VPX_ENTITY e on n.ENTITY_ID = e.ID')
go
grant select on dbo.vwVitrualMachineMAC_Org to omni

