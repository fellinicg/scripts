use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwPatchLinkMAC'))
drop view dbo.vwPatchLinkMAC
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20141125
-- ============================================================
create view dbo.vwPatchLinkMAC
as

	select 'PatchLink' Source, EPName 'Machine', replace(MACAddress, ':', '') MACAddress, IPAddress
	from openquery([TELEDV33], 'select e.EPName, n.MACAddress, n.IPAddress
								from UPCCommon.dbo.EndpointNetworkAddress n
								inner join dbo.EndPoint e on n.EPID = e.EPID
								where n.MACAddress is not null')
go
grant select on dbo.vwPatchLinkMAC to omni
