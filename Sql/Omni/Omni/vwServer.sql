use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwServer'))
drop view dbo.vwServer
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140221
-- ============================================================
create view dbo.vwServer
as
select h.HardwareID, c.Name, convert(varchar(max), c.Description) 'Description', o.Type, s.Status, c.Serial_Num SerialNumber, c.Computer_Name, c.CPU, 
	c.CPU_Count, c.CPU_Speed, c.Primary_IP_Address 'PrimaryIP', c.MAC, b.Brand, c.Model, c.OS_Name, c.OS_ServicePack, c.OS_Version, c.Product, c.RAM,
	a.Warranty_Exp, c.UDF_MajorApplications, c.Domain, c.ID, c.Import_ID
from dbo.vwServerAlloy h
	inner join AN7.dbo.Computers c on h.AlloyID = c.ID
	left join AN7.dbo.Object_Type o on c.Type_ID = o.id
	left join AN7.dbo.Status s on c.Status_ID = s.ID
	left join AN7.dbo.Brands b on c.Manufacturer_ID = b.ID
	left join AN7.dbo.Assets a on c.ID = a.Associated_CI_ID
go
grant select on dbo.vwServer to omni