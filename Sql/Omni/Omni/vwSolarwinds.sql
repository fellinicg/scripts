use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwSolarwinds'))
drop view dbo.vwSolarwinds
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140829
-- ============================================================
create view dbo.vwSolarwinds
as

	select NodeID, Caption DeviceName, MachineType OS, IP_Address
	from openquery([TELEDB48], 'select distinct NodeID, Caption, MachineType, IP_Address from SolarWindsOrion.dbo.Nodes 
									--where Pomeroy_Support in (''Pomeroy'', ''Pomeroy-Hardware Only'') or Pomeroy_Support is null
									--where charindex(''Unknown'', MachineType) = 0 and charindex(''Cisco'', MachineType) = 0
									--and charindex(''Brocade'', MachineType) = 0 and charindex(''IBM Blade Center'', MachineType) = 0
									--and charindex(''net-snmp'', MachineType) = 0')
go
grant select on dbo.vwSolarwinds to omni