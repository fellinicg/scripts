use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbEquipmentLocation') and type in (N'U'))
drop table dbo.tbEquipmentLocation
go

create table dbo.tbEquipmentLocation(
	EquipmentLocationID int identity(1,1) not null,
	Datacenter char(1) not null,
	Room varchar(10) not null,
	Cabinet tinyint null,
	GridLetter varchar(2) not null,
	GridNumber tinyint not null,
 constraint PK_tbEquipmentLocation primary key clustered 
(
	EquipmentLocationID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

