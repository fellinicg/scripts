use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upSearchIP') and type in (N'P', N'PC'))
drop procedure dbo.upSearchIP
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140904
-- ============================================================
create procedure dbo.upSearchIP
@IP varchar(15)
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select Source, Name, IPAddress
	from (
		select Source, Name, IPAddress
		from dbo.vwAlloyIP
		union
		select Source, Name, IPAddress
		from dbo.vwEpoIP
		union
		select Source, Name, IPAddress
		from dbo.vwEventlogIP
		union
		select Source, Name, IPAddress
		from dbo.vwPatchLinkIP
		union
		select Source, Name, IPAddress
		from dbo.vwSccmIP
		union
		select Source, Name, IPAddress
		from dbo.vwSolarwindsIP
		union
		select Source, Name, IPAddress
		from dbo.vwVirtualMachineIP) a
	where IPAddress = @IP

end
go
grant exec on dbo.upSearchIP to omni