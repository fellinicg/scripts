use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCategory') and type in (N'U'))
drop table dbo.tbCategory
go

create table dbo.tbCategory(
	CategoryID int identity(1,1) not null,
	Description varchar(50) not null,
	IsDeleted bit not null constraint DF_tbCategory_IsDeleted default ((0)),
	CreateDT smalldatetime not null constraint DF_tbCategory_CreateDT  default (getdate()),
 constraint PK_tbCategory primary key clustered 
(
	CategoryID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off