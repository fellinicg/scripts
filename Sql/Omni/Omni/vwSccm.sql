use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwSccm'))
drop view dbo.vwSccm
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140829
-- ============================================================
create view dbo.vwSccm
as

	select MachineID, Name DeviceName, OS
	from openquery([TELEDV10], 'select distinct s.MachineID, s.Name0 Name, o.Caption00 OS from CM_PR1.dbo.System_DATA s inner join CM_PR1.dbo.Operating_System_DATA o on s.MachineID = o.MachineID where SystemRole0 = ''Server''')
go
grant select on dbo.vwSccm to omni