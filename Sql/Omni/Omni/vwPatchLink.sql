use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwPatchLink'))
drop view dbo.vwPatchLink
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140829
-- ============================================================
create view dbo.vwPatchLink
as

	select EPID, AgentName DeviceName, ContactDNS, ContactAddress, OperatingSystem_Name OS, OSVer, OSBuildNo, CurrentOSPackVersion, Description
	from openquery([TELEDV33], 'select EPID, ep.AgentName, ep.ContactDNS, ep.ContactAddress, os.OperatingSystem_Name, ep.OSVer, ep.OSBuildNo, os.CurrentOSPackVersion, ep.Description
								from plus.dbo.EndpointDetail ep inner join (
									select OperatingSystem_ID, OperatingSystem_Name, OperatingSystem_Version, CurrentOSPackVersion, LatestOSPackVersion 
									from plus.dbo.OperatingSystems where charindex(''Server'', OperatingSystem_Name) > 0
								) os on ep.OperatingSystem_ID = os.OperatingSystem_ID')
go
grant select on dbo.vwPatchLink to omni
