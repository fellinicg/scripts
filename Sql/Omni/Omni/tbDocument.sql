use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbDocument') and type in (N'U'))
drop table dbo.tbDocument
go

create table dbo.tbDocument(
	DocumentID int identity(1,1) not null,
	DocumentName varchar(100) not null,
	DocumentPath varchar(300) not null,
	DocumentDescription varchar(max) null,
	DocumentCategoryID int null,
	IsDeleted bit not null constraint DF_tbDocument_IsDeleted default ((0)),
	CreateDT smalldatetime not null constraint DF_tbDocument_CreateDT  default (getdate()),
 constraint PK_tbDocument primary key clustered 
(
	DocumentID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

