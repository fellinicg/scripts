use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetAlloyServers') and type in (N'P', N'PC'))
drop procedure dbo.upGetAlloyServers
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140121
-- ============================================================
create procedure dbo.upGetAlloyServers
as
begin

	-- Initialize settings
	set nocount on

	-- Add to tbHardware
	insert into dbo.tbHardware (AlloyID, HardwareTypeID)
	select src.ID, ht.HardwareTypeID
	from AN7.dbo.Computers src
		left join dbo.tbHardware dst on src.ID = dst.AlloyID
		left join dbo.tbHardwareType ht on ht.HardwareType = 'Server'
	where src.Type_ID in (
		'629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3' --Server
		,'E00622AC-074D-4F49-9654-8CEA4FC56C4E' --Server ICS
		,'B37E1387-CF14-4562-930D-DD48D5F1837A' --VM Server
		)
		and dst.HardwareID is null

	-- Add to tbServer
	insert into dbo.tbServer (HardwareID)
	select src.HardwareID
	from dbo.tbHardware src
		left join dbo.tbServer dst on src.HardwareID = dst.HardwareID
	where src.HardwareTypeID = 1
		and dst.HardwareID is null
end
