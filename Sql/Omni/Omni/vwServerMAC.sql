use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwServerMAC'))
drop view dbo.vwServerMAC
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20141125
-- ============================================================
create view dbo.vwServerMAC
as
select 'Omni' Source, c.Name 'Machine', replace(c.MAC, ':', '') MACAddress
from dbo.vwServerAlloy h
	inner join AN7.dbo.Computers c on h.AlloyID = c.ID
where
	c.Name is not null
	and c.MAC is not null
union
select 'Omni' Source, c.Name, replace(a.MAC_Address, ':', '')
from dbo.vwServerAlloy h
	inner join AN7.dbo.Computers c on h.AlloyID = c.ID
	inner join AD7.dbo.Inv_Network_Adapters a on c.Import_ID = a.Node_ID
go
grant select on dbo.vwServerMAC to omni
