use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbTool') and type in (N'U'))
drop table dbo.tbTool
go

create table dbo.tbTool(
	ToolID smallint identity(1,1) not null,
	ToolName varchar(100) not null,
	PageName varchar(50) not null,
	IsDeleted bit not null constraint DF_tbTools_IsDeleted  default ((0)),
	CreateDT smalldatetime not null constraint DF_tbTools_CreateDT  default (getdate()),
 constraint PK_tbTool primary key clustered 
(
	ToolID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

