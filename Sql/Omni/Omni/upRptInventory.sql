use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upRptInventory') and type in (N'P', N'PC'))
drop procedure dbo.upRptInventory
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140829
-- ============================================================
create procedure dbo.upRptInventory
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @servers table (idx int identity(1,1), name varchar(200))
	declare @alloy table (name varchar(200))
	declare @epo table (name varchar(200))
	declare @sccm table (name varchar(200))
	declare @solarwinds table (name varchar(200))

	-- Alloy
	insert into @alloy
	select DeviceName from dbo.vwAlloy 

	-- ePO
	insert into @epo
	select DeviceName from dbo.vwEpo

	-- SCCM
	insert into @sccm
	select DeviceName from dbo.vwSccm

	-- SolarWinds
	insert into @solarwinds
	select distinct case when charindex('.', DeviceName) > 0 then substring(DeviceName, 1, charindex('.', DeviceName) - 1) else DeviceName end DeviceName from dbo.vwSolarwinds

	-- Combine all unique names
	insert into @servers(name)
	select Name from @alloy
	union
	select Name from @epo
	union
	select Name from @sccm
	union
	select Name from @solarwinds

	-- Return data
	select upper(s.name) 'Name'
		,case isnull(alloy.name, 'xxx') when 'xxx' then '' else 'X' end 'Alloy'
		,case isnull(epo.name, 'xxx') when 'xxx' then '' else 'X' end 'ePO'
		,case isnull(sccm.name, 'xxx') when 'xxx' then '' else 'X' end 'SCCM'
		,case isnull(solarwinds.name, 'xxx') when 'xxx' then '' else 'X' end 'SolarWinds'
		,isnull(replace(replace(a.UDF_MajorApplications, char(10), ''), char(13), ''), '') 'MajorApplications'
		,isnull(replace(replace(a.Description, char(10), ''), char(13), ''), '') 'Description'
		,isnull(replace(replace(a.OS_Name, char(10), ''), char(13), ''), '') 'OS_Name'
		,isnull(replace(replace(a.UDF_SCCMHeartbeatDDR, char(10), ''), char(13), ''), '') 'SCCMHeartbeatDDR'
	from @servers s
		left join @alloy alloy on s.name = alloy.name
		left join @epo epo on s.name = epo.name
		left join @sccm sccm on s.name = sccm.name
		left join @solarwinds solarwinds on s.name = solarwinds.name
		left join dbo.vwAlloyInventoryReport a on s.name = a.Name

end
go
grant exec on dbo.upRptInventory to omni