use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwServerAlloy'))
drop view dbo.vwServerAlloy
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140305
-- ============================================================
create view dbo.vwServerAlloy
as
select h.HardwareID, h.AlloyID
from dbo.tbServer s
	inner join dbo.tbHardware h on s.HardwareID = h.HardwareID

go
grant select on dbo.vwServerAlloy to omni