use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCompletedForm') and type in (N'U'))
drop table dbo.tbCompletedForm
go

create table dbo.tbCompletedForm(
	FormSubmissionID int not null,
	ItemID int not null,
	ItemDetail varchar(1000) not null,
 	CompletedBy int not null,
	IsDeleted bit not null constraint DF_tbCompletedForm_IsDeleted default ((0)),
	CompletedDT smalldatetime not null constraint DF_tbCompletedForm_CreateDT  default (getdate()),
constraint PK_tbCompletedForm primary key clustered
(
	FormSubmissionID, ItemID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

