use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upPopulateServerList') and type in (N'P', N'PC'))
drop procedure dbo.upPopulateServerList
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140829
-- ============================================================
create procedure dbo.upPopulateServerList
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @servers table (idx int identity(1,1), name varchar(200))
	declare @alloy table (name varchar(200))
	declare @patchlink table (name varchar(200))
	declare @epo table (name varchar(200))
	declare @sccm table (name varchar(200))
	declare @vcenter table (name varchar(200))
	declare @solarwinds table (name varchar(200))

	-- Alloy
	insert into @alloy
	select DeviceName from dbo.vwAlloy where charindex('Windows XP', OS) = 0 and charindex('Windows 7', OS) = 0

	-- Patchlink
	insert into @patchlink
	select DeviceName from dbo.vwPatchLink where charindex('Windows XP', OS) = 0 and charindex('Windows 7', OS) = 0

	-- ePO
	insert into @epo
	select DeviceName from dbo.vwEpo where charindex('Windows XP', OS) = 0 and charindex('Windows 7', OS) = 0

	-- SCCM
	insert into @sccm
	select DeviceName from dbo.vwSccm where charindex('Windows XP', OS) = 0 and charindex('Windows 7', OS) = 0

	-- vCenter
	insert into @vcenter
	select distinct DeviceName from dbo.vwEsxHost where charindex('Windows XP', OS) = 0 and charindex('Windows 7', OS) = 0
	union
	select distinct DeviceName from dbo.vwVirtualMachine where charindex('Windows XP', OS) = 0 and charindex('Windows 7', OS) = 0

	-- SolarWinds
	insert into @solarwinds
	select distinct case when charindex('.', DeviceName) > 0 then substring(DeviceName, 1, charindex('.', DeviceName) - 1) else DeviceName end DeviceName from dbo.vwSolarwinds where charindex('Windows XP', OS) = 0 and charindex('Windows 7', OS) = 0

	-- Combine all unique names
	insert into @servers(name)
	select Name from @alloy
	union
	select Name from @patchlink
	union
	select Name from @epo
	union
	select Name from @sccm
	union
	select Name from @vcenter
	union
	select Name from @solarwinds

	-- Clear Table
	truncate table dbo.tbServerList

	-- Populate data
	insert into dbo.tbServerList
	select distinct upper(s.name) 'Name'
		,case isnull(alloy.name, 'xxx') when 'xxx' then 0 else 1 end 'Alloy'
		,case isnull(patchlink.name, 'xxx') when 'xxx' then 0 else 1 end 'Patchlink'
		,case isnull(epo.name, 'xxx') when 'xxx' then 0 else 1 end 'ePO'
		,case isnull(sccm.name, 'xxx') when 'xxx' then 0 else 1 end 'SCCM'
		,case isnull(vcenter.name, 'xxx') when 'xxx' then 0 else 1 end 'vCenter'
		,case isnull(solarwinds.name, 'xxx') when 'xxx' then 0 else 1 end 'SolarWinds'
	from @servers s
		left join @alloy alloy on s.name = alloy.name
		left join @patchlink patchlink on s.name = patchlink.name
		left join @epo epo on s.name = epo.name
		left join @sccm sccm on s.name = sccm.name
		left join @vcenter vcenter on s.name = vcenter.name
		left join @solarwinds solarwinds on s.name = solarwinds.name

end
go
grant exec on dbo.upPopulateServerList to omni