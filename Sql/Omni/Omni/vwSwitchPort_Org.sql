use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwSwitchPort_Org'))
drop view dbo.vwSwitchPort_Org
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20141114
-- ============================================================
create view dbo.vwSwitchPort_Org
as

	select *
	from openquery([TELEDB32], 
		'select distinct i.NodeID, i.InterfaceIndex, i.FullName SwitchPort, i.InterfaceSpeed, i.PhysicalAddress SwitchPortMAC, i.StatusLED, nc.MACAddress ConnectedMAC, 
			n.Caption ConnectedDevice, i.IfName 
		from dbo.Interfaces i inner join dbo.NodePortInterfaceMap ni on i.NodeID = ni.NodeID and i.InterfaceIndex = ni.IfIndex 
		inner join dbo.NodeL2Connections nc on ni.NodeID = nc.NodeID and ni.PortID = nc.PortID and ni.VlanId = nc.VlanId 
		left join dbo.Interfaces ci on nc.MACAddress = ci.PhysicalAddress
		left join dbo.Nodes n on ci.NodeID = n.NodeID where i.IfName not in (''Po1'', ''Po2'') and charindex(''link'', i.FullName) = 0 and charindex(''trunk'', i.FullName) = 0')
	--where 
		--NodeID in (--1027, , 1432, 1433
		--1426, 1427, 1428, 1429, 1430, 1431)
		--and 
		--IfName not in ('Po1', 'Po2')
		--and charindex('uplink', SwitchPort) = 0
go
grant select on dbo.vwSwitchPort_Org to omni