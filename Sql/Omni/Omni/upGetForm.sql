use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetForm') and type in (N'P', N'PC'))
drop procedure dbo.upGetForm
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140923
-- ============================================================
create procedure dbo.upGetForm
@TypeID int
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select FormItemID, Item, SortOrder, HasChoices, IsDependent
	from dbo.tbFormItem
	where TypeID = @TypeID
		and IsDeleted = 0
	order by SortOrder
end
go
grant exec on dbo.upGetForm to omni