use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwVirtualMachine'))
drop view dbo.vwVirtualMachine
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140826
-- ============================================================
create view dbo.vwVirtualMachine
as
	select 'P' + convert(varchar, ID) ID, NAME DeviceName, DNS_NAME, GUEST_FULL_NAME OS, IP_ADDRESS, ANNOTATION, VERSION, TEMPLATE_FLG, POWER_STATE
	from openquery([PATCDV14], 'select i.ID, i.NAME, v.DNS_NAME, v.IP_ADDRESS, i.GUEST_FULL_NAME, i.ANNOTATION, i.VERSION, i.TEMPLATE_FLG, v.POWER_STATE from vmwarevcdb.dbo.VPX_VM_CONFIG_INFO i inner join vmwarevcdb.dbo.VPX_VM v on i.ID = v.ID')
	union all
	select 'T' + convert(varchar, ID) ID, NAME, DNS_NAME, GUEST_FULL_NAME, IP_ADDRESS, ANNOTATION, VERSION, TEMPLATE_FLG, POWER_STATE
	from openquery([TELEDV12], 'select i.ID, i.NAME, v.DNS_NAME, v.IP_ADDRESS, i.GUEST_FULL_NAME, i.ANNOTATION, i.VERSION, i.TEMPLATE_FLG, v.POWER_STATE from vmwarevcdb.dbo.VPX_VM_CONFIG_INFO i inner join vmwarevcdb.dbo.VPX_VM v on i.ID = v.ID')
	union all
	select 'M' + convert(varchar, ID) ID, NAME, DNS_NAME, GUEST_FULL_NAME, IP_ADDRESS, ANNOTATION, VERSION, TEMPLATE_FLG, POWER_STATE
	from openquery([PATCDV08], 'select i.ID, i.NAME, v.DNS_NAME, v.IP_ADDRESS, i.GUEST_FULL_NAME, i.ANNOTATION, i.VERSION, i.TEMPLATE_FLG, v.POWER_STATE from VCDB.dbo.VPX_VM_CONFIG_INFO i inner join vcdb.dbo.VPX_VM v on i.ID = v.ID')
	union all
	select 'CJIS' + convert(varchar, ID) ID, NAME, DNS_NAME, GUEST_FULL_NAME, IP_ADDRESS, ANNOTATION, VERSION, TEMPLATE_FLG, POWER_STATE
	from openquery([TELEAV296\VIM_SQLEXP], 'select i.ID, i.NAME, v.DNS_NAME, v.IP_ADDRESS, i.GUEST_FULL_NAME, i.ANNOTATION, i.VERSION, i.TEMPLATE_FLG, v.POWER_STATE from dbo.VPX_VM_CONFIG_INFO i inner join dbo.VPX_VM v on i.ID = v.ID')
	union all
	select 'OIGT' + convert(varchar, ID) ID, NAME, DNS_NAME, GUEST_FULL_NAME, IP_ADDRESS, ANNOTATION, VERSION, TEMPLATE_FLG, POWER_STATE
	from openquery([TELEIV83\VIM_SQLEXP], 'select i.ID, i.NAME, v.DNS_NAME, v.IP_ADDRESS, i.GUEST_FULL_NAME, i.ANNOTATION, i.VERSION, i.TEMPLATE_FLG, v.POWER_STATE from dbo.VPX_VM_CONFIG_INFO i inner join dbo.VPX_VM v on i.ID = v.ID')
	union all
	select 'OIGP' + convert(varchar, ID) ID, NAME, DNS_NAME, GUEST_FULL_NAME, IP_ADDRESS, ANNOTATION, VERSION, TEMPLATE_FLG, POWER_STATE
	from openquery([PATCIV59\VIM_SQLEXP], 'select i.ID, i.NAME, v.DNS_NAME, v.IP_ADDRESS, i.GUEST_FULL_NAME, i.ANNOTATION, i.VERSION, i.TEMPLATE_FLG, v.POWER_STATE from dbo.VPX_VM_CONFIG_INFO i inner join dbo.VPX_VM v on i.ID = v.ID')

go
grant select on dbo.vwVirtualMachine to omni