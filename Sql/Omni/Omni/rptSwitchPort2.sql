select InterfaceIndex, SwitchPort, InterfaceSpeed, SwitchPortMAC, ConnectedMAC, ConnectedDevice
from dbo.vwSwitchPort
where ConnectedDevice is not null
union
select InterfaceIndex, SwitchPort, InterfaceSpeed, SwitchPortMAC, ConnectedMAC, isnull(ConnectedDevice, Name) ConnectedDevice
from dbo.vwSwitchPort 
inner join AN7.dbo.Computers on ConnectedMAC = replace(MAC, ':', '')
where ConnectedDevice is null
order by SwitchPort, InterfaceIndex