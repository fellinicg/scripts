use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwServerOS'))
drop view dbo.vwServerOS
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140910
-- ============================================================
create view dbo.vwServerOS
as
	select NodeName, OSType
	from dbo.vwEpo
	union
	select Name, OS_Name
	from dbo.vwServer
	union
	select AgentName, OperatingSystem_Name
	from dbo.vwPatchLink
	union
	select Name, GUEST_FULL_NAME
	from dbo.vwVirtualMachine
	union
	select Caption, MachineType
	from dbo.vwSolarwinds
go
grant select on dbo.vwServerOS to omni