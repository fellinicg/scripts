use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwAllMacs'))
drop view dbo.vwAllMacs
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170627
-- ============================================================
create view dbo.vwAllMacs
as
	select Machine, MACAddress
	from dbo.vwAlloyMAC
	union
	select Machine, MACAddress
	from dbo.vwEpoMAC
	union
	select Machine, MACAddress
	from dbo.vwSccmMAC
	union
	select Machine, MACAddress
	from dbo.vwVirtualMachineMAC
go
grant select on dbo.vwAllMacs to omni