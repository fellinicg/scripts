use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upSearchADInfo') and type in (N'P', N'PC'))
drop procedure dbo.upSearchADInfo
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140922
-- ============================================================
create procedure dbo.upSearchADInfo
	@Search varchar(100)
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @filter varchar(500)

	-- Person
	declare @result table (objsid varchar(80), username varchar(50), firstname varchar(100), lastname varchar(100), email varchar(100), phone varchar(50), lastlogon bigint, createdt datetime, modifydt datetime, userAccountControl varbinary(max))
	set @filter = 'where cn="*' + @Search + '*" and objectCategory="person" 
									or givenname="*' + @Search + '*" and objectCategory="person" 
									or sn="*' + @Search + '*" and objectCategory="person"'

	insert @result
	exec dbo.upgetadinfo 'objectsid,cn,givenname,sn,mail,telephoneNumber,lastlogon,createTimeStamp,modifyTimeStamp,userAccountControl', @filter

	select 
		r.objsid SID
		, u.ADUserID
		, r.username
--		, substring(r.objcat, charindex('=', r.objcat) + 1, charindex(',', r.objcat) - charindex('=', r.objcat) - 1) Type
		, r.firstname
		, r.lastname
		, r.email
		, r.phone
		, u.DN Novell
		, convert(varchar, dbo.sfInt8toDateTime(r.lastlogon), 100) LastLogon
		, convert(varchar, r.createdt, 100) Created
		, convert(varchar, r.modifydt, 100) Modified
		, case userAccountControl & 2 when 0 then 0 else 1 end Disabled
	from @result r
		left join dbo.tbADUser u on r.objsid = u.SID
	order by r.username

	-- Group
	declare @result2 table (objsid varchar(80), groupname varchar(200), grouptype int, createdt datetime, modifydt datetime)
	set @filter = 'where cn="*' + @Search + '*" and objectCategory="group"'

	insert @result2
	exec dbo.upgetadinfo 'objectsid,cn,groupType,createTimeStamp,modifyTimeStamp', @filter

	select 
		r.objsid SID
		, r.groupname
		, case grouptype 
			when 2 then 'Global Distribution'
			when 4 then 'Domain Local Distribution'
			when 8 then 'Universal Distribution'
			when -2147483640 then 'Universal Security' 
			when -2147483644 then 'Domain Local Security' 
			when -2147483646 then 'Global Security' 
			else 'Unknown'
		  end grouptype
		, convert(varchar, r.createdt, 100) Created
		, convert(varchar, r.modifydt, 100) Modified
--		, case charindex('cn=group', r.objcat) when 1 then 0 else case userAccountControl & 2 when 0 then 0 else 1 end end Disabled
	from @result2 r
	order by r.groupname

	-- OU
	declare @result3 table (objguid uniqueidentifier, name varchar(200), createdt datetime, modifydt datetime)
	set @filter = 'where name="*' + @Search + '*" and objectCategory="organizationalunit"'

	insert @result3
	exec dbo.upgetadinfo 'objectguid,name,createTimeStamp,modifyTimeStamp', @filter

	select 
		r.objguid GUID
		, r.name
		, convert(varchar, r.createdt, 100) Created
		, convert(varchar, r.modifydt, 100) Modified
	from @result3 r
	order by r.name

end
go
grant exec on dbo.upSearchADInfo to omni