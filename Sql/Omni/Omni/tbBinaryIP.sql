use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbBinaryIP') and type in (N'U'))
drop table dbo.tbBinaryIP
go

create table dbo.tbBinaryIP(
	Octet tinyint not null,
	AsBinary binary(1) not null,
 constraint PK_tbBinaryIP primary key clustered 
(
	Octet asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

