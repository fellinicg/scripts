use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddADGroups') and type in (N'P', N'PC'))
drop procedure dbo.upAddADGroups
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20151009
-- ============================================================
create procedure dbo.upAddADGroups
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @intCharCode int
	declare	@Sql nvarchar(500)

	-- Initialize variables
	set @intCharCode = 33

	-- Add data
	while not (@intCharCode > 90)
	begin
		--set @sql = 'insert into dbo.tbADGroup(SID, GroupName, DN) select dbo.sfHexSidToString(objectsid) objectsid, name, distinguishedname from openrowset(''ADSDSOObject'', ''adsdatasource'', ''select objectsid, name, distinguishedName  from ''''LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov'''' where objectClass =  ''''Group'''' and name = ''''' + char(@intCharCode) + '*'''''') left join dbo.tbADGroup on dbo.sfHexSidToString(objectsid) = SID where SID is null'
		set @sql = 'insert into dbo.tbADGroup (SID, GroupName, DistinguishedName) select dbo.sfHexSidToString(objectsid) sid, cn, distinguishedname from openquery (ADSI, ''<LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov>;(&(objectCategory=group)(cn=' + convert(varchar, char(@intCharCode)) + '*));objectsid,cn,distinguishedname;subtree'') a left join dbo.tbADGroup b on dbo.sfHexSidToString(a.objectsid) = b.SID where b.SID is null'
		begin try
			exec(@sql)
		end try
		begin catch
		end catch
		set @intCharCode = @intCharCode + 1
	end

end
go