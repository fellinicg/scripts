use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwVitrualMachineMAC'))
drop view dbo.vwVitrualMachineMAC
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140904
-- ============================================================
create view dbo.vwVitrualMachineMAC
as
	select 'VirtualCenter' Source, replace(replace(Name, '.panynj.gov', ''), '.pacorp', '') 'DeviceName', replace(MAC_ADDRESS, ':', '') 'MACAddress', OS
	from openquery([PATCDV14], 'select e.NAME, n.MAC_ADDRESS, isnull(v.GUEST_FULL_NAME, ''ESX'') OS from dbo.VPX_ENTITY e
								left join dbo.VPX_VM_CONFIG_INFO v on e.ID = v.ID inner join dbo.VPX_NIC n on e.ID = n.ENTITY_ID
								where TYPE_ID in (0,1)')
	union
	select 'VirtualCenter' Source, replace(replace(Name, '.panynj.gov', ''), '.pacorp', '') 'DeviceName', replace(MAC_ADDRESS, ':', '') 'MACAddress', OS
	from openquery([TELEDV12], 'select e.NAME, n.MAC_ADDRESS, isnull(v.GUEST_FULL_NAME, ''ESX'') OS from dbo.VPX_ENTITY e
								left join dbo.VPX_VM_CONFIG_INFO v on e.ID = v.ID inner join dbo.VPX_NIC n on e.ID = n.ENTITY_ID
								where TYPE_ID in (0,1)')
	union
	select 'VirtualCenter' Source, replace(replace(Name, '.panynj.gov', ''), '.pacorp', '') 'DeviceName', replace(MAC_ADDRESS, ':', '') 'MACAddress', OS
	from openquery([PATCDV08], 'select e.NAME, n.MAC_ADDRESS, isnull(v.GUEST_FULL_NAME, ''ESX'') OS from dbo.VPX_ENTITY e
								left join dbo.VPX_VM_CONFIG_INFO v on e.ID = v.ID inner join dbo.VPX_NIC n on e.ID = n.ENTITY_ID
								where TYPE_ID in (0,1)')
	union
	select 'VirtualCenter' Source, replace(replace(Name, '.panynj.gov', ''), '.pacorp', '') 'DeviceName', replace(MAC_ADDRESS, ':', '') 'MACAddress', OS
	from openquery([TELEAV296\VIM_SQLEXP], 'select e.NAME, n.MAC_ADDRESS, isnull(v.GUEST_FULL_NAME, ''ESX'') OS from dbo.VPX_ENTITY e
								left join dbo.VPX_VM_CONFIG_INFO v on e.ID = v.ID inner join dbo.VPX_NIC n on e.ID = n.ENTITY_ID
								where TYPE_ID in (0,1)')
	union
	select 'VirtualCenter' Source, replace(replace(Name, '.panynj.gov', ''), '.pacorp', '') 'DeviceName', replace(MAC_ADDRESS, ':', '') 'MACAddress', OS
	from openquery([TELEIV83\VIM_SQLEXP], 'select e.NAME, n.MAC_ADDRESS, isnull(v.GUEST_FULL_NAME, ''ESX'') OS from dbo.VPX_ENTITY e
								left join dbo.VPX_VM_CONFIG_INFO v on e.ID = v.ID inner join dbo.VPX_NIC n on e.ID = n.ENTITY_ID
								where TYPE_ID in (0,1)')
	union
	select 'VirtualCenter' Source, replace(replace(Name, '.panynj.gov', ''), '.pacorp', '') 'DeviceName', replace(MAC_ADDRESS, ':', '') 'MACAddress', OS
	from openquery([PATCIV59\VIM_SQLEXP], 'select e.NAME, n.MAC_ADDRESS, isnull(v.GUEST_FULL_NAME, ''ESX'') OS from dbo.VPX_ENTITY e
								left join dbo.VPX_VM_CONFIG_INFO v on e.ID = v.ID inner join dbo.VPX_NIC n on e.ID = n.ENTITY_ID
								where TYPE_ID in (0,1)')
go
grant select on dbo.vwVitrualMachineMAC to omni

