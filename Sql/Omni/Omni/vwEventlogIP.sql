use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwEventlogIP'))
drop view dbo.vwEventlogIP
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140904
-- ============================================================
create view dbo.vwEventlogIP
as
	select 'Eventlog' Source, Name, IPAddress
	from (
		select Machine Name, IPAddress
		from AD_EventLog.dbo.tbMachineIP
		union
		select UserName, IPAddress
		from AD_EventLog.dbo.tbUserIP
		) a
go
grant select on dbo.vwEventlogIP to omni