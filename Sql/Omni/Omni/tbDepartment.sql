use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbDepartment') and type in (N'U'))
drop table dbo.tbDepartment
go

create table dbo.tbDepartment(
	DepartmentID int identity(0,1) not null,
	DepartmentName varchar(50) not null,
	IsDeleted bit not null constraint DF_tbDepartment_IsDeleted default ((0)),
	CreateDT smalldatetime not null constraint DF_tbDepartment_CreateDT  default (getdate()),
 constraint PK_tbDepartment primary key clustered 
(
	DepartmentID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

