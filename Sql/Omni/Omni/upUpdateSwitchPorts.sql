use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateSwitchPorts') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateSwitchPorts
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20141219
-- ============================================================
create procedure dbo.upUpdateSwitchPorts
as
begin

	-- Initialize settings
	set nocount on

	-- Delete temporary table
	if object_id('tempdb..#temp') is not null
		drop table #temp

	-- Create temporary table
	create table #temp(
		NodeID int NOT NULL,
		InterfaceIndex int NOT NULL,
		SwitchName varchar(50) NULL,
		SwitchPort varchar(400) NULL,
		Location varchar(100) NULL,
		InterfaceName varchar(100) NULL,
		InterfaceSpeed float NULL,
		SwitchPortMAC varchar(12) NULL,
		ConnectedMAC varchar(12) NULL,
		ConnectedDevice varchar(200) NULL,
	)

	-- Add base data from Solarwinds
	insert into #temp (NodeID, InterfaceIndex, SwitchPort, InterfaceName, InterfaceSpeed, SwitchPortMAC, ConnectedMAC, ConnectedDevice)
	select distinct NodeID, InterfaceIndex, SwitchPort, InterfaceName, InterfaceSpeed, SwitchPortMAC, ConnectedMAC, ConnectedDevice
	from dbo.vwSwitchPort

	-- Update SwitchName and Location fields
	update #temp set SwitchName = b.Caption, Location = b.Location
	from #temp a
	inner join openquery([TELEDB48], 'select NodeID, Caption, Location from SolarWindsOrion.dbo.Nodes') b on a.NodeID = b.NodeID

	-- Update data using Alloy matches
	update #temp set ConnectedDevice = Machine
	from #temp
	inner join dbo.vwAlloyMAC on ConnectedMAC = MACAddress
	where ConnectedDevice is null

	-- Update data using vCenter matches
	update #temp set ConnectedDevice = Machine
	from #temp
	inner join dbo.vwVirtualMachineMAC on ConnectedMAC = MACAddress
	where ConnectedDevice is null

	-- Update data using SCCM matches
	update #temp set ConnectedDevice = Machine
	from #temp
	inner join dbo.vwSccmMAC on ConnectedMAC = MACAddress
	where ConnectedDevice is null

	-- Update data using Patchlink matches
	update #temp set ConnectedDevice = Machine
	from #temp
	inner join dbo.vwPatchLinkMAC on ConnectedMAC = MACAddress
	where ConnectedDevice is null

	-- Clear table
	truncate table dbo.tbSwitchPort

	-- Populate table
	insert into dbo.tbSwitchPort (NodeID, InterfaceIndex, SwitchPort, InterfaceName, InterfaceSpeed, SwitchPortMAC, ConnectedMAC, ConnectedDevice, Location)
	select NodeID, InterfaceIndex, SwitchPort, InterfaceName, InterfaceSpeed, SwitchPortMAC, ConnectedMAC, ConnectedDevice, Location
	from #temp

end
go
grant exec on dbo.upUpdateSwitchPorts to omni