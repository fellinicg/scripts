use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbSanEnclosure') and type in (N'U'))
drop table dbo.tbSanEnclosure
go

create table dbo.tbSanEnclosure(
	SanEnclosureID int identity(1,1) not null,
	SanID varchar(100) not null,
	Model varchar(50) null, 
	Manuf varchar(20) null, 
	Serial varchar(30) null, 
	ManufDate date null, 
	WarrExp date null,
	Rack varchar(10) null,
	EncNumber varchar(20) null,
	IsDeleted bit not null constraint DF_tbSanEnclosure_IsDeleted default ((0)),
	CreateDT smalldatetime not null constraint DF_tbSanEnclosure_CreateDT  default (getdate()),
 constraint PK_tbSanEnclosure primary key clustered 
(
	SanEnclosureID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off