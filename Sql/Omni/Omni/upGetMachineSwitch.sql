use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetMachineSwitch') and type in (N'P', N'PC'))
drop procedure dbo.upGetMachineSwitch
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20141218
-- ============================================================
create procedure dbo.upGetMachineSwitch
	@Machine varchar(50)
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select ConnectedMAC, SwitchName, SwitchPort, Location, InterfaceSpeed
	from dbo.tbSwitchPort
	where ConnectedDevice = @Machine
	order by ConnectedMAC, SwitchPort
	
end
go
grant exec on dbo.upGetMachineSwitch to omni