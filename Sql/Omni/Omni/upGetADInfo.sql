use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetADInfo') and type in (N'P', N'PC'))
drop procedure dbo.upGetADInfo
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140918
-- ============================================================
create procedure dbo.upGetADInfo
	@Fields varchar(200)
	,@Filter varchar(500) = ''
as
begin

	-- Initialize settings
	set nocount on

	-- Replace double quotes with double single quotes
	set @Filter = replace(@Filter, '"', '''''')

	-- Declarations
	declare @Sql varchar(max) = 'select ' + replace(@Fields, 'objectsid', 'dbo.sfHexSidToString(objectsid) objectsid')  + ' from openrowset(''ADSDSOObject'',''adsdatasource'',''select ' + @Fields + ' from ''''LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov'''' ' + @Filter + ''')'
	declare @Error bit = 0	

	-- Get data
	begin try
		exec(@Sql)
	end try
	begin catch
		set @Error = 1
	end catch

	return @Error
end
go
grant exec on dbo.upGetADInfo to omni