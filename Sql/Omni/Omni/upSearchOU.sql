use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upSearchOU') and type in (N'P', N'PC'))
drop procedure dbo.upSearchOU
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140506
-- ============================================================
create procedure dbo.upSearchOU
@Search varchar(50)
as
begin

	-- Initialize settings
	set nocount on

	-- Get data
	select ADUserID, Username, FirstName, LastName, OU, DN
	from dbo.tbADUser
	where charindex(@Search, UserName) > 0
		or charindex(@Search, FirstName) > 0
		or charindex(@Search, LastName) > 0
end
go
grant exec on dbo.upSearchOU to omni