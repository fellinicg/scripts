use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbHardware') and type in (N'U'))
drop table dbo.tbHardware
go

create table dbo.tbHardware(
	HardwareID int identity(1,1) not null,
	HardwareTypeID int not null,
	HardwareName varchar(50) not null,
	ModelID int null,
	IsDeleted bit not null,
	CreateDT datetime not null,
	UpdateDT datetime not null,
 constraint PK_tbHardware primary key clustered 
(
	HardwareID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off
go

alter table dbo.tbHardware add constraint DF_tbHardware_IsDeleted default ((0)) for IsDeleted
go

alter table dbo.tbHardware add constraint DF_tbHardware_CreateDT  default (getdate()) for CreateDT
go

alter table dbo.tbHardware add constraint DF_tbHardware_UpdateDT  default (getdate()) for UpdateDT
go
