insert into [Omni].[dbo].[tbADUser] (SID, UserName, FirstName, LastName, Email, Office, OfficePhone, OU)
SELECT SID, Name, GivenName, Surname, EmailAddress, Office, OfficePhone, CanonicalName
  FROM [dbo].[adusers]
  