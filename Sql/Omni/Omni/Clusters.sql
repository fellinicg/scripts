select c.ClusterName, b.Name, b.Brand, convert(varchar, b.Warranty_Exp, 101) WarrantyExpiration, b.SerialNumber
from dbo.tbServerDependency a
inner join dbo.vwServer b on a.HardwareID = b.HardwareID
inner join dbo.tbCluster c on a.DependencyID = c.ClusterID
where a.TypeID = 14
order by c.ClusterName, b.Name