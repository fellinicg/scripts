use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbFormItem') and type in (N'U'))
drop table dbo.tbFormItem
go

create table dbo.tbFormItem(
	FormItemID int identity(1,1) not null,
	TypeID int not null,
	Item varchar(500) not null,
	SortOrder tinyint not null constraint DF_tbFormItem_SortOrder default ((1)),
	HasChoices bit not null constraint DF_tbFormItem_HasOptions default ((0)),
	IsDependent bit not null constraint DF_tbFormItem_IsDependent default ((0)),
	IsDeleted bit not null constraint DF_tbFormItem_IsDeleted default ((0)),
	CreateDT smalldatetime not null constraint DF_tbFormItem_CreateDT  default (getdate()),
 constraint PK_tbFormItem primary key clustered 
(
	FormItemID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

