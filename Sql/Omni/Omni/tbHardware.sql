use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbHardware') and type in (N'U'))
drop table dbo.tbHardware
go

create table dbo.tbHardware(
	HardwareID int identity(1,1) not null,
	AlloyID uniqueidentifier null,
	HardwareTypeID int not null,
	IsDeleted bit not null constraint DF_tbHardware_IsDeleted default ((0)),
	CreateDT smalldatetime not null constraint DF_tbHardware_CreateDT  default (getdate()),
	UpdateDT smalldatetime not null constraint DF_tbHardware_UpdateDT  default (getdate()),
 constraint PK_tbHardware primary key clustered 
(
	HardwareID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off