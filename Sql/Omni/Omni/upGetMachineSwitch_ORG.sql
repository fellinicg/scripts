use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetMachineSwitch_ORG') and type in (N'P', N'PC'))
drop procedure dbo.upGetMachineSwitch_ORG
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20141218
-- ============================================================
create procedure dbo.upGetMachineSwitch_ORG
	@Machine varchar(50)
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select @Machine, SwitchPort, ConnectedMAC
	from dbo.vwSwitchPort
	where ConnectedMAC in (
		select MACAddress
		from (
			select Machine, MACAddress
			from dbo.vwAlloyMAC
			union 
			select Machine, MACAddress
			from dbo.vwServerMAC) a
		where Machine = @Machine)
	
end
go
grant exec on dbo.upGetMachineSwitch_ORG to omni