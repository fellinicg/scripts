use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetUser') and type in (N'P', N'PC'))
drop procedure dbo.upGetUser
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140226
-- ============================================================
create procedure dbo.upGetUser
	@ADUserID int
	,@IsLockedOut bit = null
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select SID, UserName, FirstName, LastName, Email, Office, OfficePhone, MobilePhone, PeoplesoftDept, PeoplesoftJobDescription, PeoplesoftStatus, IsEnabled, @IsLockedOut 'IsLockedOut'
	from dbo.tbADUser
	where ADUserID = @ADUserID
end
go
grant exec on dbo.upGetUser to omni