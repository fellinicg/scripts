use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetItemChoices') and type in (N'P', N'PC'))
drop procedure dbo.upGetItemChoices
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140923
-- ============================================================
create procedure dbo.upGetItemChoices
@FormItemID int
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select FormItemChoiceID, Choice, SortOrder
	from dbo.tbFormItemChoice
	where FormItemID = @FormItemID
		and IsDeleted = 0
	order by SortOrder
end
go
grant exec on dbo.upGetItemChoices to omni