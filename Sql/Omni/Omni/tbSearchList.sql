use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbSearchList') and type in (N'U'))
drop table dbo.tbSearchList
go

create table dbo.tbSearchList(
	SearchListID smallint identity(1,1) not null,
	ListItem varchar(30) not null,
	Sort tinyint not null,
	IsDeleted bit not null constraint DF_tbSearchLists_IsDeleted  default ((0)),
	CreateDT smalldatetime not null constraint DF_tbSearchLists_CreateDT  default (getdate()),
 constraint PK_tbSearchList primary key clustered 
(
	SearchListID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

