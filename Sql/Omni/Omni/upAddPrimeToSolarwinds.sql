use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddPrimeToSolarwinds') and type in (N'P', N'PC'))
drop procedure dbo.upAddPrimeToSolarwinds
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170807
-- ============================================================
create procedure dbo.upAddPrimeToSolarwinds
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare	@ips nvarchar(max) = ''

	-- Compile and format list of IPs not in Solarwinds
	select 
		@ips = @ips + coalesce(('<d3p1:string>' + f.ipAddress + '</d3p1:string>'), '')
	from openrowset('Microsoft.ACE.OLEDB.12.0',
		'Text;Database=C:\Feeds\',
		'select * from [PrimeDevices.csv]') f
	left join openquery([TELEDB48], 'select IP_Address, SysName from SolarWindsOrion.dbo.Nodes where vendor = ''Cisco''') d on f.deviceName = d.SysName
	where f.adminStatus = 'Managed'
		and d.SysName is null

	-- Add Profile in Solarwinds
	update [TELEDB48].SolarWindsOrion.dbo.DiscoveryProfiles  
	set PluginConfigurations = left(PluginConfigurations, charindex('<d3p1:string>', PluginConfigurations) - 1) + @ips + right(PluginConfigurations, (len(PluginConfigurations) - charindex('</d2p1:BulkList>', PluginConfigurations)) + 1)
	where ProfileID = 119

end
go