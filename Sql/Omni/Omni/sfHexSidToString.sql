use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfHexSidToString') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfHexSidToString
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140721
-- ============================================================
go
create function dbo.sfHexSidToString(@varbinarySID varbinary(85))
returns varchar(100)
as
begin

	declare @SID varchar(100)
	declare @len int
	declare @loop int
	declare @temp binary (4)

	set @loop = 9
	set @len = len(@varbinarySID)
	set @SID = 'S-' + convert(varchar, convert(int, convert(varbinary, substring(@varbinarySID, 1, 1)))) + '-' + convert(varchar, convert(int, convert(varbinary, substring(@varbinarySID, 3, 6))))

	while @loop < @len
	begin
		set @temp = substring(@varbinarySID, @loop, 4)
		set @SID = @SID + '-' + convert(varchar, convert(bigint, convert(varbinary, reverse(convert(varbinary, @temp)))))
		set @loop = @loop + 4
	end

    return @SID
end