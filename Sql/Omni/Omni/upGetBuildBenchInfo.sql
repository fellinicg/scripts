use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetBuildBenchInfo') and type in (N'P', N'PC'))
drop procedure dbo.upGetBuildBenchInfo
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160419
-- ============================================================
create procedure dbo.upGetBuildBenchInfo
	@SerialNumber varchar(100)
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		t.Type 'MachineType'
		, b.Brand 'Manufacturer'
		, c.Model
		, c.Description
	from AN7.dbo.Computers c
		inner join AN7.dbo.Object_Type t on c.Type_ID = t.ID
		inner join AN7.dbo.Brands b on c.Manufacturer_ID = b.ID
	where c.Serial_Num = @SerialNumber

end
go
grant exec on dbo.upGetBuildBenchInfo to buildbench