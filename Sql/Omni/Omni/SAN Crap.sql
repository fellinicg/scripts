--select Distinct Name
--from (
	select distinct SAN, SAN + ' {' + tray_slot + '}' Name, tray_slot, Capacity, DataRate, Productid, Serial
	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SANs.xlsx', 'select * from [SANDrives$]')
	union
	select distinct SAN, SAN + ' {' + tray_slot + '}' Name, tray_slot, Capacity, DataRate, Productid, Serial
	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SANs.xlsx', 'select * from [TELE-Upstream$]')
	union
	select distinct SAN, SAN + ' {' + tray_slot + '}' Name, tray_slot, Capacity, DataRate, Productid, Serial
	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SANs.xlsx', 'select * from [CJIS$]')
	union
	select distinct SAN, SAN + ' {' + tray_slot + '}' Name, tray_slot, Capacity, DataRate, Productid, Serial
	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SANs.xlsx', 'select * from [v7NewDisks$]')
	union
	select distinct SAN, SAN + ' {' + tray_slot + '}' Name, tray_slot, Capacity, DataRate, Productid, Serial
	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SANs.xlsx', 'select * from [TELEEDV3KDisks$]')
--) a

select distinct a.Host
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\NotInAlloy.xlsx', 'select * from [Pingable$]') a
where charindex('WKS', Host) = 0

select *
from (
	select distinct
		a.Host
		, isnull(isnull(isnull(isnull(b.IPAddress, c.IPAddress), d.IPAddress), e.IPAddress), f.IP_ADDRESS) as IPAddress
		--, b.IPAddress
		--, c.IPAddress
		--, d.IPAddress
		--, e.IPAddress
		--, f.IP_ADDRESS,
	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\NotInAlloy.xlsx', 'select * from [Pingable$]') a
	left join [dbo].[vwSolarwindsIP] b on replace(a.Host, '.PACORP.PANYNJ.GOV ', '') = replace(b.Name, '.PACORP.PANYNJ.GOV ', '')
	left join [dbo].[vwPatchLinkIP] c on replace(a.Host, '.PACORP.PANYNJ.GOV ', '') = replace(c.Name, '.PACORP.PANYNJ.GOV ', '')
	left join [dbo].[vwSccmIP] d on replace(a.Host, '.PACORP.PANYNJ.GOV ', '') = replace(d.Name, '.PACORP.PANYNJ.GOV ', '')
	left join [dbo].[vwVirtualMachineIP] e on replace(a.Host, '.PACORP.PANYNJ.GOV ', '') = replace(e.Name, '.PACORP.PANYNJ.GOV ', '')
	left join [dbo].[vwEsxHost] f on replace(a.Host, '.PACORP.PANYNJ.GOV ', '') = replace(f.[DeviceName], '.PACORP.PANYNJ.GOV ', '')
	where charindex('WKS', Host) = 0
) a
where charindex('169.', IPAddress) <> 1
order by Host

--update dbo.tbSanEnclosure set EncNumber = a.EncNumber
--from (
--	select distinct SAN, Model, Manuf, Serial, ManufDate, WarrExp, null Rack, null EncNumber
--	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SANs.xlsx', 'select * from [SANs$]')
--	union
--	select distinct SAN, Model, null, Serial, null, null, Rack, EncNumber
--	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SANs.xlsx', 'select * from [CJISEnc$]')
--	union
--	select distinct SAN, Model, null, Serial, null, null, Rack, EncNumber
--	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SANs.xlsx', 'select * from [V7Enc$]')
--	union
--	select distinct SAN, Model, null, convert(varchar, Serial), null, WarrExp, Rack, EncNumber
--	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SANs.xlsx', 'select * from [TELEEDV3KEnc$]')
--	union
--	select distinct SAN, Model, Manuf, Serial, ManufDate, WarrExp, null, convert(varchar, EncNumber)
--	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SANs.xlsx', 'select * from [JFKEnc$]')
--) a
--inner join dbo.tbSanEnclosure c on a.Serial = c.Serial
--where a.EncNumber is not null

--insert into dbo.tbSanEnclosure(SanID, Model, Manuf, Serial, ManufDate, WarrExp, Rack, EncNumber)
--select distinct b.SanID, a.Model, a.Manuf, a.Serial, a.ManufDate, a.WarrExp, a.Rack, a.EncNumber
--from (
--	select distinct SAN, Model, Manuf, Serial, ManufDate, WarrExp, null Rack, null EncNumber
--	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SANs.xlsx', 'select * from [SANs$]')
--	union
--	select distinct SAN, Model, null, Serial, null, null, Rack, EncNumber
--	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SANs.xlsx', 'select * from [CJISEnc$]')
--	union
--	select distinct SAN, Model, null, Serial, null, null, Rack, EncNumber
--	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SANs.xlsx', 'select * from [V7Enc$]')
--	union
--	select distinct SAN, Model, null, convert(varchar, Serial), null, WarrExp, Rack, EncNumber
--	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SANs.xlsx', 'select * from [TELEEDV3KEnc$]')
--	union
--	select distinct SAN, Model, Manuf, Serial, ManufDate, WarrExp, null, convert(varchar, EncNumber)
--	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SANs.xlsx', 'select * from [JFKEnc$]')
--) a
--inner join dbo.tbSan b on a.SAN = b.SanName
--left join dbo.tbSanEnclosure c on a.Serial = c.Serial
--where c.Serial is null


--insert into dbo.tbSan(SanName)
--select SAN
--from (
--	select distinct SAN
--	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SANs.xlsx', 'select * from [SANs$]')
--	union
--	select distinct SAN
--	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SANs.xlsx', 'select * from [CJISEnc$]')
--	union
--	select distinct SAN
--	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SANs.xlsx', 'select * from [V7Enc$]')
--	union
--	select distinct SAN
--	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SANs.xlsx', 'select * from [TELEEDV3KEnc$]')
--	union
--	select distinct SAN
--	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SANs.xlsx', 'select * from [JFKEnc$]')
--) a
--left join dbo.tbSan b on a.SAN = b.SanName
--where b.SanName is null
--order by SAN