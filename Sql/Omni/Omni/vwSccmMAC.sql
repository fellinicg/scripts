use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwSccmMAC'))
drop view dbo.vwSccmMAC
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20141125
-- ============================================================
create view dbo.vwSccmMAC
as
	select 'SCCM' Source, Machine, replace(MACAddress, ':', '') MACAddress
	from openquery([TELEDV10], 'select distinct s.Name0 Machine, m.MAC_Addresses0 MACAddress from dbo.System_MAC_Addres_ARR m inner join dbo.System_DISC s on m.ItemKey = s.ItemKey')
go
grant select on dbo.vwSccmMAC to omni