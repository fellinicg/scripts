use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbSwitch') and type in (N'U'))
drop table dbo.tbSwitch
go

create table dbo.tbSwitch(
	HardwareID int not null,
	SwitchName varchar(50) not null,
	IsDeleted bit not null constraint DF_tbSwitch_IsDeleted default ((0)),
	CreateDT smalldatetime not null constraint DF_tbSwitch_CreateDT  default (getdate()),
	UpdateDT smalldatetime not null constraint DF_tbSwitch_UpdateDT  default (getdate()),
 constraint PK_tbSwitch primary key clustered 
(
	HardwareID ASC
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off