use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbSQLInstance') and type in (N'U'))
drop table dbo.tbSQLInstance
go

create table dbo.tbSQLInstance(
	SQLInstanceID tinyint identity(1,1) not null,
	HardwareID int not null,
	SQLInstanceName varchar(50) not null,
	Cluster varchar(50) null,
	IsOnline bit not null constraint DF_tbSQLInstance_IsOnline  default ((1)),
	IsSupported bit not null constraint DF_tbSQLInstance_IsSupported  default ((0)), 
	constraint PK_tbSQLInstance primary key clustered 
(
	SQLInstanceID ASC
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go
set ansi_padding off