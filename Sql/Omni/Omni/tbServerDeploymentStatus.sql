use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbServerDeploymentStatus') and type in (N'U'))
drop table dbo.tbServerDeploymentStatus
go

create table dbo.tbServerDeploymentStatus(
	ServerDeploymentStatusID int identity(1,1) not null,
	HardwareID int not null,
	DeploymentStatusID tinyint not null,
 	CreateDT smalldatetime not null constraint DF_tbServerDeploymentStatus_CreateDT default (getdate()),
constraint PK_tbServerDeploymentStatus primary key clustered 
(
	ServerDeploymentStatusID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go