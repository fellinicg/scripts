if object_id('tempdb..#temp') is not null
    drop table #temp

--declare @Results table (nID int, iIndex int, cMAC varchar(15), cDevice varchar(200))


--insert into @Results(nID, iIndex, cMAC, cDevice)
select distinct NodeID, InterfaceIndex, ConnectedMAC, ConnectedDevice
into #temp
from dbo.vwSwitchPort

--update @Results set cDevice = s.ConnectedDevice
--from @Results r
--inner join dbo.vwSwitchPort s on r.nID = s.NodeID and r.iIndex = s.InterfaceIndex and r.cMAC = s.ConnectedMAC
--where ConnectedDevice is not null

select *
--count(1)
from #temp
where ConnectedDevice is not null
--from @Results
--where cDevice is not null

update #temp set ConnectedDevice = Name
from #temp
inner join dbo.vwServer on ConnectedMAC = replace(MAC, ':', '')
where ConnectedDevice is null

--update @Results set cDevice = Name
--from @Results
--inner join dbo.vwServer on cMAC = replace(MAC, ':', '')
--where cDevice is null

--select count(1)
--from @Results
--where cDevice is not null

select Name
from dbo.vwserver
left join #temp on Name = ConnectedDevice
where ConnectedDevice is null and Name is not null
order by Name

select distinct *
from #temp
where ConnectedDevice is not null

update #temp set ConnectedDevice = DeviceName
from #temp
inner join dbo.vwVitrualMachineMAC on ConnectedMAC = MACAddress
where ConnectedDevice is null and charindex('Windows XP', OS) = 0 and charindex('Windows 7', OS) = 0


select 
--count (distinct a.ConnectedDevice) 
b.SwitchPort, a.ConnectedDevice, b.InterfaceSpeed, b.SwitchPortMAC, a.ConnectedMAC--, InterfaceIndex, InterfaceName, 
from #temp a
inner join dbo.vwSwitchPort b on 
	a.NodeID = b.NodeID 
	and a.InterfaceIndex = b.InterfaceIndex 
	and a.ConnectedMAC = b.ConnectedMAC 
where a.ConnectedDevice = 'teledb23'
--a.ConnectedDevice is not null
--and charindex('-RSA', a.ConnectedDevice) = 0
order by b.SwitchPort, a.ConnectedDevice


--select distinct dbo.vwAlloyMAC.*
--from #temp
--inner join dbo.vwAlloyMAC on ConnectedMAC = MACAddress
--inner join dbo.vwServer on Machine = Name
--where ConnectedDevice is null

select *
from #temp
where ConnectedMAC in (
'40F2E9F26BAA'
,'40F2E9F26BAB'
,'40F2E9F26BAD'
)

select distinct [Device Name], [System - Name], [Chassis - Chassis Model Name], DeviceName
--select distinct [Chassis - Chassis Model Name]
from openrowset('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=C:\Feeds\20141124-Devices.xlsx', 'select * from [Sheet1$]')
left join (
select NodeID, Caption DeviceName, MachineType OS, IP_Address
from openquery([TELEDB32], 'select distinct NodeID, Caption, MachineType, IP_Address from SolarWindsOrion.dbo.Nodes 
								where Pomeroy_Support in (''Networking'')')
) b on [Device Name] = b.IP_Address
where left([Chassis - Chassis Model Name], 4) <> 'AIR-'
	and left([Chassis - Chassis Model Name], 1) <> 'C'
	and left([Chassis - Chassis Model Name], 2) <> 'VG'
	and len(ltrim(rtrim([Chassis - Chassis Model Name]))) > 0
	and [Chassis - Chassis Model Name] <> 'N/A'
	and [Chassis - Chassis Model Name] not in ('132', '4')
	and DeviceName is null
order by [System - Name] --[Chassis - Chassis Model Name] DeviceName, 