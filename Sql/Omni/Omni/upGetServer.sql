use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetServer') and type in (N'P', N'PC'))
drop procedure dbo.upGetServer
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140225
-- ============================================================
create procedure dbo.upGetServer
@HardwareID int
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select d.DepartmentName, s.OnPAWANET
	from dbo.tbServer s
		left join dbo.tbDepartment d on s.DepartmentID = d.DepartmentID
	where s.HardwareID = @HardwareID
end
go
grant exec on dbo.upGetServer to omni