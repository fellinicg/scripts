use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbSan') and type in (N'U'))
drop table dbo.tbSan
go

create table dbo.tbSan(
	SanID int identity(1,1) not null,
	SanName varchar(100) not null,
	AlloyID uniqueidentifier null,
	IsDeleted bit not null constraint DF_tbSan_IsDeleted default ((0)),
	CreateDT smalldatetime not null constraint DF_tbSan_CreateDT  default (getdate()),
 constraint PK_tbSan primary key clustered 
(
	SanID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off