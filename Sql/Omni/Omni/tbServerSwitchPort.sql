use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbServerSwitchPort') and type in (N'U'))
drop table dbo.tbServerSwitchPort
go

create table dbo.tbServerSwitchPort(
	ServerSwitchPortID int identity(1,1) not null,
	HardwareID int not null,
	SwitchPortID int not null,
	MacAddress varchar(20) null,
	IPAddress varchar(20) null,
	IsDeleted bit not null constraint DF_tbServerSwitchPort_IsDeleted  default ((0)),
	CreateDT datetime not null constraint DF_tbServerSwitchPort_CreateDT  default (getdate()),
 constraint PK_tbServerSwitchPort primary key clustered 
(
	ServerSwitchPortID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off