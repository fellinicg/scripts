use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwEpoMAC'))
drop view dbo.vwEpoMAC
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140828
-- ============================================================
create view dbo.vwEpoMAC
as
	select 'ePO' Source, NetbiosName 'Machine', MAC 'MACAddress', IPAddress, Tags
	from openquery([TELEDV36], 'select distinct s.NetbiosName, i.MAC, dbo.RSDFN_ConvertIntToIPString(i.IPV4) IPAddress, n.Tags from dbo.RSDInterfaceProperties i 
		inner join dbo.RSDDetectedSystemProperties s on i.HostID = s.HostID left join dbo.EPOLeafNode n on s.AgentGUID = n.AgentGUID where i.MAC is not null and s.NetbiosName is not null')
go
grant select on dbo.vwEpoMAC to omni