use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfADQuery') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfADQuery
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140918
-- ============================================================
go
create function dbo.sfADQuery(@fields varchar(100), @where varchar(500) = '')
returns varchar(max)
as
begin
	set @where = replace(@where, '"', '''''')
    return 'select ' + @fields + ' from openrowset(''ADSDSOObject'',''adsdatasource'',''select ' + @fields + ' from ''''LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov'''' ' + @where + ''')'
end