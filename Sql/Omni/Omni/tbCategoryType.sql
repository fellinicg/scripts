use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCategoryType') and type in (N'U'))
drop table dbo.tbCategoryType
go

create table dbo.tbCategoryType(
	CategoryTypeID int identity(1,1) not null,
	CategoryID int not null,
	TypeID int not null,
	IsDeleted bit not null constraint DF_tbCategoryType_IsDeleted default ((0)),
	CreateDT datetime not null constraint DF_tbCategoryType_CreateDT  default (getdate()),
 constraint PK_tbCategoryType primary key clustered 
(
	CategoryTypeID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off