use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbServerType') and type in (N'U'))
drop table dbo.tbServerType
go

create table dbo.tbServerType(
	ServerTypeID tinyint not null,
	ServerTypeAbbrev char(1) not null,
	ServerType varchar(30) not null,
	IsDeleted bit not null constraint DF_tbServerType_IsDeleted  default ((0)),
	CreateDT datetime not null constraint DF_tbServerType_CreateDT  default (getdate()),
	UpdateDT datetime not null constraint DF_tbServerType_UpdateDT  default (getdate()),
 constraint PK_tbServerType primary key clustered 
(
	ServerTypeID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

