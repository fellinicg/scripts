use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upSearchIPs') and type in (N'P', N'PC'))
drop procedure dbo.upSearchIPs
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170418
-- ============================================================
create procedure dbo.upSearchIPs
	@IPs varchar(max)
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select Source, Name, IPAddress
	from dbo.vwAllIPs a
	inner join dbo.tfCommaListToTable(@IPs) b on a.IPAddress = b.ListItem

end
go
grant exec on dbo.upSearchIPs to omni