use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwAlloyIP'))
drop view dbo.vwAlloyIP
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140904
-- ============================================================
create view dbo.vwAlloyIP
as
	select Source, ID, Name, IP_Address IPAddress
	from (
			select 'AD7' Source, ID, Name, IP_Address, Last_Date
			from AD7.dbo.Inv_NetNodes
			where len(isnull(IP_Address, '')) > 0
			union
			select 'AD7' Source, n.ID, n.Name, a.IP_Address, n.Last_Date
			from AD7.dbo.Inv_Network_Adapters a
			inner join AD7.dbo.Inv_NetNodes n on a.Node_ID = n.ID
			where len(isnull(a.IP_Address, '')) > 0
			union
			select 'AN7' Source, ID, Name, Primary_IP_Address, Audit_Date
			from AN7.dbo.Computers
			where len(isnull(Primary_IP_Address, '')) > 0
			union
			select 'AN7' Source, c.ID, Name, IP_Address, c.Audit_Date
			from AN7.dbo.IP_Address_Link_List i
			inner join AN7.dbo.Computers c on i.Object_ID = c.ID
			where len(isnull(i.IP_Address, '')) > 0
		) Alloy
go
grant select on dbo.vwAlloyIP to omni