use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwEpo'))
drop view dbo.vwEpo
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140828
-- ============================================================
create view dbo.vwEpo
as
	select NodeName DeviceName, OSType OS, OSVersion
	from openquery([TELEDV50], 'select distinct	n.NodeName, c.OSType, c.OSVersion from ePO_TELEAV438.dbo.EPOLeafNodeMT n
		inner join ePO_TELEAV438.dbo.EPOComputerPropertiesMT c on n.AutoID = c.ParentID where c.OSPlatform = ''Server''')
go
grant select on dbo.vwEpo to omni