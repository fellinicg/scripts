use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetQualysScanResults') and type in (N'P', N'PC'))
drop procedure dbo.upGetQualysScanResults
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150610
-- ============================================================
create procedure dbo.upGetQualysScanResults
	@QualysScanID int
	, @Type char(1) = null
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @DataFile varchar(30)
	declare @Sql varchar(max)

	-- Retrieve DataFile
	select @DataFile = DataFile
	from dbo.tbQualysScan
	where QualysScanID = @QualysScanID

	-- Build Sql
	set @Sql = '
		select 
			r.QualysScanID
			, r.IpAddress
			, r.AssignedTo
			, r.Completed
			, r.Actions
			, r.Notes
			, a.QID
			, a.Title
			, a.Type
			, a.Severity
			, a.Port
			, a.Protocol
			, a.FQDN
			, a.SSL
			, a.[CVE ID]
			, a.[Vendor Reference]
			, a.[Bugtraq ID]
			, replace(replace(replace(convert(varchar(max), a.Threat), char(13), char(32)), char(10), char(32)), char(9), char(32)) Threat
			, replace(replace(replace(convert(varchar(max), a.Impact), char(13), char(32)), char(10), char(32)), char(9), char(32)) Impact
			, replace(replace(replace(convert(varchar(max), a.Solution), char(13), char(32)), char(10), char(32)), char(9), char(32)) Solution
			, replace(replace(replace(convert(varchar(max), a.Exploitability), char(13), char(32)), char(10), char(32)), char(9), char(32)) Exploitability
			, a.[Associated Malware]
			, replace(replace(replace(convert(varchar(max), a.Results), char(13), char(32)), char(10), char(32)), char(9), char(32)) Results	
			, a.[PCI Vuln]
			, a.Instance
			, a.Category
		from dbo.tbQualysScanResult r
			inner join openrowset(''Microsoft.ACE.OLEDB.12.0'',''Excel 12.0;Database=C:\Feeds\' + @DataFile + ''',''select * from [Sheet1$]'') a 
				on r.IpAddress = a.IP 
					and r.QID = a.QID
					and isnull(r.Port, 0) = isnull(a.Port, 0)
		where r.QualysScanID = ' + convert(varchar, @QualysScanID) + '
			and a.Type in (''Vuln'', ''Practice'') 
			$OSTYPE$
		order by r.IpAddress, a.Type desc, a.Severity desc'

	-- Check Type
	if upper(@Type) = 'W' set @Sql = replace(@Sql, '$OSTYPE$', 'and r.IpAddress in (select distinct IpAddress from dbo.vwQualysWindows)')
	else if upper(@Type) = 'U' set @Sql = replace(@Sql, '$OSTYPE$', 'and r.IpAddress in (select distinct IpAddress from dbo.vwQualysUnix)')
	else set @Sql = replace(@Sql, '$OSTYPE$', '')

	-- Return data
	exec (@Sql)

end