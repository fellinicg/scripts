use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetAlloySwitches') and type in (N'P', N'PC'))
drop procedure dbo.upGetAlloySwitches
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140505
-- ============================================================
create procedure dbo.upGetAlloySwitches
as
begin

	-- Initialize settings
	set nocount on

	-- Add to tbHardware
	insert into dbo.tbHardware (AlloyID, HardwareTypeID)
	select src.ID, ht.HardwareTypeID
	from AN7.dbo.Hardware src
		left join dbo.tbHardware dst on src.ID = dst.AlloyID
		left join dbo.tbHardwareType ht on ht.HardwareType = 'Switch'
	where src.Category_ID in ('68566108-4840-4FB0-AD21-118973F795F1', 'AE6AB46F-53F9-4D59-9065-2D521B433A16')
		and dst.HardwareID is null

	-- Add to tbSwitch
	--insert into dbo.tbSwitch (HardwareID)
	--select src.HardwareID
	--from dbo.tbHardware src
	--	left join dbo.tbSwitch dst on src.HardwareID = dst.HardwareID
	--where src.HardwareTypeID = 2
	--	and dst.HardwareID is null
end
