use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwSwitch'))
drop view dbo.vwSwitch
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140515
-- ============================================================
create view dbo.vwSwitch
as
select s.HardwareID, h.ID, Name, Description, Serial_Num 'SerialNumber', Model, Primary_IP_Address 'SwitchIP'
from dbo.vwSwitchAlloy s
	inner join AN7.dbo.Hardware h on s.AlloyID = h.ID
go
grant select on dbo.vwSwitch to omni