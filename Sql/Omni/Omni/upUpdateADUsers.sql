use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateADUsers') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateADUsers
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140304
-- ============================================================
create procedure dbo.upUpdateADUsers
as
begin

	-- Initialize settings
	set nocount on

	-- Update data
	update dbo.tbADUser 
	set EmployeeID = src.employeeID
		, UserName = src.Name
		, FirstName = src.GivenName
		, MiddleInitials = src.initials
		, LastName = src.Surname
		, Email = src.EmailAddress
		, Office = src.Office
		, OfficePhone = src.OfficePhone
		, OU = src.CanonicalName
		, IsEnabled = case src.Enabled when 'true' then 1 else 0 end
		, DistinguishedName = src.DistinguishedName
		, PrimaryGroupID = src.PrimaryGroupID
		, PasswordLastSet = src.PasswordLastSet
		, UpdateDT = getdate()
	from dbo.tbADUser dst 
	inner join openrowset('Microsoft.ACE.OLEDB.12.0',
		'Text;Database=C:\Feeds\', 
		'select * from adusers.txt where sid is not null and name is not null') src on dst.SID = src.SID

end
go
grant exec on dbo.upUpdateADUsers to omni