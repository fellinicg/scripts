use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwSolarwindsIP'))
drop view dbo.vwSolarwindsIP
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140904
-- ============================================================
create view dbo.vwSolarwindsIP
as

	select 'SolarWinds' Source, Name, IPAddress
	from openquery([TELEDB32], 'select n.Caption Name, a.IPAddress 
								from Nodes n
									inner join (
										select NodeID, IPAddress
										from SolarWindsOrion.dbo.DiscoveredNodes
										union
										select NodeID, IP_Address 
										from SolarWindsOrion.dbo.Nodes
										union
										select NodeID, IPAddress 
										from SolarWindsOrion.dbo.ShadowNodes
										union
										select NodeID, IPAddress
										from SolarWindsOrion.dbo.NodeIPAddresses) a on n.NodeID = a.NodeID')
go
grant select on dbo.vwSolarwindsIP to omni
