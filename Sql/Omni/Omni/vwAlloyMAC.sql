use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwAlloyMAC'))
drop view dbo.vwAlloyMAC
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20141125
-- ============================================================
create view dbo.vwAlloyMAC
as
select 'Alloy' Source, replace(replace(replace(Name, '.panynj.gov', ''), '.pacorp', ''), '.wks', '') Machine, replace(MAC, ':', '') MACAddress
from AN7.dbo.Computers
where MAC is not null
union
select 'Alloy' Source, replace(replace(replace(nn.Name, '.panynj.gov', ''), '.pacorp', ''), '.wks', ''), replace(na.MAC_Address, ':', '') MACAddress
from AD7.dbo.Inv_Network_Adapters na inner join AD7.dbo.Inv_NetNodes nn on na.Node_ID = nn.ID
go
grant select on dbo.vwAlloyMAC to omni