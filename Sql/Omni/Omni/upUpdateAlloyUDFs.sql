use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateAlloyUDFs') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateAlloyUDFs
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170324
-- ============================================================
create procedure dbo.upUpdateAlloyUDFs
as
begin

	-- Initialize settings
	set nocount on

	-- Update UDF_InEPO and UDF_InSolarwinds with an X or null depending on whether it exists in the corresponding system
	update AN7.dbo.Computers
		set UDF_InEPO = case when e.DeviceName is not null then 'X' else null end
		, UDF_InSolarwinds = case when w.DeviceName is not null then 'X' else null end
	from AN7.dbo.Computers c
		left join dbo.vwEpo e on c.Name = e.DeviceName
		left join dbo.vwSolarwinds w on c.Name = w.DeviceName
	where c.Type_ID in (
		'B37E1387-CF14-4562-930D-DD48D5F1837A'
		,'629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3'
		,'E00622AC-074D-4F49-9654-8CEA4FC56C4E'
	)

	-- Update UDF_InSCCM with an X or null depending on whether it exists in the corresponding system
	update AN7.dbo.Computers
		set UDF_InSCCM = case when c.UDF_SCCMClient = 'Yes' then 'X' else null end
	from AN7.dbo.Computers c
	where c.Type_ID in (
		'B37E1387-CF14-4562-930D-DD48D5F1837A'
		,'629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3'
		,'E00622AC-074D-4F49-9654-8CEA4FC56C4E')
end