use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwEpoAll'))
drop view dbo.vwEpoAll
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170711
-- ============================================================
create view dbo.vwEpoAll
as
	select NodeName DeviceName, OSType OS, OSVersion, LastUpdate, IPAddress
	from openquery([TELEDV50], 'select distinct	n.NodeName, n.LastUpdate, c.OSType, c.OSVersion, c.IPAddress from ePO_TELEAV438.dbo.EPOLeafNodeMT n
		inner join ePO_TELEAV438.dbo.EPOComputerPropertiesMT c on n.AutoID = c.ParentID where n.AgentGUID is not null')
go
grant select on dbo.vwEpoAll to omni