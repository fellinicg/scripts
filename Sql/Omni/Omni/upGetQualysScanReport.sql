use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetQualysScanReport') and type in (N'P', N'PC'))
drop procedure dbo.upGetQualysScanReport
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150611
-- ============================================================
create procedure dbo.upGetQualysScanReport
	@QualysScanID int
as
begin

	-- Initialize settings
	set nocount on

	-- Windows
	select * from dbo.vwQualysWindows where QualysScanID = @QualysScanID
	exec dbo.upGetQualysScanResults @QualysScanID, 'w'

	--Unix
	select * from dbo.vwQualysUnix where QualysScanID = @QualysScanID
	exec dbo.upGetQualysScanResults @QualysScanID, 'u'


end