use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfBinaryIPv4') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfBinaryIPv4
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140205
-- ============================================================
go
create function dbo.sfBinaryIPv4(@ip varchar(15))
returns binary(4)
as
begin
    declare @ret as binary(4)

    select @ret = cast( cast( parsename( @ip, 4 ) as integer) as binary(1))
                + cast( cast( parsename( @ip, 3 ) as integer) as binary(1))
                + cast( cast( parsename( @ip, 2 ) as integer) as binary(1))
                + cast( cast( parsename( @ip, 1 ) as integer) as binary(1))

    return @ret
end