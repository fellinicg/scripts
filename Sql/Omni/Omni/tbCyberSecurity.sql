use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCyberSecurity') and type in (N'U'))
drop table dbo.tbCyberSecurity
go

create table dbo.tbCyberSecurity(
	CyberSecurityID int identity(1,1) not null,
	TypeID int not null,
	ADUserID int not null,
	Target varchar(500) not null,
	TicketNumber varchar(50) null,
	IsDeleted bit not null constraint DF_tbCyberSecurity_IsDeleted default ((0)),
	CreateDT smalldatetime not null constraint DF_tbCyberSecurity_CreateDT  default (getdate()),
 constraint PK_tbCyberSecurity primary key clustered 
(
	CyberSecurityID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

