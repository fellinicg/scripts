use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwAllIPs'))
drop view dbo.vwAllIPs
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20170418
-- ============================================================
create view dbo.vwAllIPs
as
	select Source, Name, IPAddress
	from (
			select Source, Name, IPAddress from [dbo].[vwAlloyIP] 
			union all
			select * from [dbo].[vwEpoIP]
			union all
			select * from dbo.vwSccmIP
			union all
			select * from dbo.vwSolarwindsIP
			union all
			select * from dbo.vwVirtualMachineIP
		) IPs
go
grant select on dbo.vwAllIPs to omni