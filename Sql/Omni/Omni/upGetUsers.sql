use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetUsers') and type in (N'P', N'PC'))
drop procedure dbo.upGetUsers
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140226
-- ============================================================
create procedure dbo.upGetUsers
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select ADUserID, UserName, FirstName, LastName
	from dbo.tbADUser
end
go
grant exec on dbo.upGetUsers to omni