use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbServer') and type in (N'U'))
drop table dbo.tbServer
go

create table dbo.tbServer(
	HardwareID int not null,
	DepartmentID int not null constraint DF_tbServer_DepartmentID  default ((0)),
	EquipmentLocationID int null,
	ServerTypeID tinyint null,
	OnPAWANET bit not null constraint DF_tbServer_OnPAWANET  default ((1)),
	IsDeleted bit not null constraint DF_tbServer_IsDeleted  default ((0)),
	CreateDT smalldatetime not null constraint DF_tbServer_CreateDT  default (getdate()),
	UpdateDT smalldatetime not null constraint DF_tbServer_UpdateDT  default (getdate()),
 constraint PK_tbServer primary key clustered 
(
	HardwareID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

