use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetEpoOffenders') and type in (N'P', N'PC'))
drop procedure dbo.upGetEpoOffenders
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20141008
-- ============================================================
create procedure dbo.upGetEpoOffenders
	@SendEmail bit = 1
as
begin

	-- Initialize settings
	set nocount on

	-- Declare variables
	declare @recipients varchar(max) = 'tfellini@panynj.gov'
	declare @output varchar(max) = 'More than four hits between ' + convert(varchar, dateadd(day, -30, getdate()), 101) + ' and ' + convert(varchar, getdate(), 101) + char(10) + char(10)

	-- Get data
	set @output = @output + convert(char(20), 'Host Name') + char(9) + char(9) + convert(char(15), 'Host IP') + char(9) + char(9) + 'Hits' + char(10) +
							convert(char(20), '=========') + char(9) + char(9) + convert(char(15), '=======') + char(9) + char(9) + '====' + char(10)

	select @output = @output + convert(char(20), [Host Name]) + char(9) + char(9) + convert(char(15), [Host IP]) + char(9) + char(9) + convert(char(3), [Hits]) + char(10)
	from openquery([TELEDV36], 'select TargetHostName [Host Name], ePO4_TELEAV76.dbo.RSDFN_ConvertIntToIPString(TargetIPV4) [Host IP], count(1) [Hits]
								from ePO4_TELEAV76.dbo.EPOEvents
								where ThreatEventID not in (1051, 1059, 1093, 1094, 1095, 1099, 1088, 1087, 1119, 1035, 1096, 1038, 1118, 1120, 
															1064, 1065, 4700, 1092, 1027, 1091, 1025, 21027, 1318, 1278, 1280, 1028, 1284, 1082)
									and DetectedUTC >= convert(varchar, dateadd(day, -30, getdate()), 101)
									and TargetHostName <> ''CNU7511X''
								group by TargetHostName, TargetIPV4
								having count(1) > 4')
	order by Hits desc, [Host Name]

	set @output = @output + char(10) + datename(dw, getdate()) + ', ' + datename(mm, getdate()) + ' ' + datename(d, getdate()) + ', ' + datename(yy, getdate())

	-- Output data
	if @SendEmail = 1
		-- Send email
		--
		exec msdb.dbo.sp_send_dbmail @profile_name = 'ePO Report', @recipients = @recipients, @subject = 'Rolling 30 Day ePO Offenders', @body = @output
	else
		print @output
end
go
grant exec on dbo.upGetEpoOffenders to omni