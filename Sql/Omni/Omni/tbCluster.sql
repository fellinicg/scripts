use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbCluster') and type in (N'U'))
drop table dbo.tbCluster
go

create table dbo.tbCluster(
	ClusterID int identity(0,1) not null,
	TypeID int not null,
	ClusterName varchar(50) not null,
	IsDeleted bit not null constraint DF_tbCluster_IsDeleted default ((0)),
	CreateDT datetime not null constraint DF_tbCluster_CreateDT  default (getdate()),
 constraint PK_tbCluster primary key clustered 
(
	ClusterID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

