use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwAlloy'))
drop view dbo.vwAlloy
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140829
-- ============================================================
create view dbo.vwAlloy
as
	select distinct case when charindex('.', name) > 0 then substring(name, 1, charindex('.', name) - 1) else name end DeviceName, OS_Name OS, case when charindex('.', Computer_Name) > 0 then substring(Computer_Name, 1, charindex('.', Computer_Name) - 1) else Computer_Name end Computer_Name from dbo.vwserver where Name is not null
go
grant select on dbo.vwAlloy to omni