use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbVault') and type in (N'U'))
drop table dbo.tbVault
go

create table dbo.tbVault(
	VaultID int identity(1,1) not null,
	SourceID int not null,
	SourceType char(1) not null,
	Item varchar(500) not null,
	Value varchar(500) not null,
	CreateDT smalldatetime not null constraint DF_tbVault_CreateDT  default (getdate()),
	UpdateDT smalldatetime not null constraint DF_tbVault_UpdateDT  default (getdate()),
	ModifiedBy int not null constraint DF_tbVault_ModifiedBy  default ((0)),
 constraint PK_tbVault primary key clustered 
(
	VaultID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

