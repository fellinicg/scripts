use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwVirtualMachineIP'))
drop view dbo.vwVirtualMachineIP
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140904
-- ============================================================
create view dbo.vwVirtualMachineIP
as
	select 'VirtualCenter' Source, Name, IPAddress
	from openquery([PATCDV14], 'select DNS_NAME Name, IP_ADDRESS IPAddress from vmwarevcdb.dbo.VPX_VM where len(isnull(IP_ADDRESS, '')) > 0
								union
								select DNS_NAME, IP_ADDRESS from vmwarevcdb.dbo.VPX_HOST where len(isnull(IP_ADDRESS, '')) > 0')
	union
	select 'VirtualCenter' Source, Name, IPAddress
	from openquery([TELEDV12], 'select DNS_NAME Name, IP_ADDRESS IPAddress from vmwarevcdb.dbo.VPX_VM where len(isnull(IP_ADDRESS, '')) > 0
								union
								select DNS_NAME, IP_ADDRESS from vmwarevcdb.dbo.VPX_HOST where len(isnull(IP_ADDRESS, '')) > 0')
	union
	select 'VirtualCenter' Source, Name, IPAddress
	from openquery([PATCDV08], 'select DNS_NAME Name, IP_ADDRESS IPAddress from vcdb.dbo.VPX_VM where len(isnull(IP_ADDRESS, '')) > 0
								union
								select DNS_NAME, IP_ADDRESS from vcdb.dbo.VPX_HOST where len(isnull(IP_ADDRESS, '')) > 0')
	union
	select 'VirtualCenter' Source, Name, IPAddress
	from openquery([PATCDV14], 'select DNS_NAME Name, IP_ADDRESS IPAddress from vmwarevcdb.dbo.VPX_VM where len(isnull(IP_ADDRESS, '')) > 0
								union
								select DNS_NAME, IP_ADDRESS from vmwarevcdb.dbo.VPX_HOST where len(isnull(IP_ADDRESS, '')) > 0')
go
grant select on dbo.vwVirtualMachineIP to omni

