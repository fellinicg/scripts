select distinct 'P' 'Datacenter', null 'Cabinet', [GridLetter], [GridNumber], [Room]
from [dbo].[patc_all]
union
select distinct 'T', [Cabinet], [GridLetter], [GridNumber], [Room]
from [dbo].[tele_all]
where [GridLetter] is not null or [GridNumber] is not null

select cabinet, count(cabinet) from (
select cabinet, gridletter, gridnumber
select * from [dbo].[tele_all]
where cabinet is null
group by cabinet, gridletter, gridnumber) a
group by cabinet
having count(cabinet) > 1

select grid
--,charindex('-', grid)
,ltrim(rtrim(left(grid, charindex('-', grid) - 1)))
,substring(grid, charindex('-', grid) + 1, 10)
--,reverse(left(reverse(location), charindex(' ', ltrim(reverse(location))) - 1))
--,replace(replace(replace(location, ' ', ''), 'nocc,cab', ''), 'cr,cab', '')
--ltrim(rtrim(replace(replace(location, 'CR, CAB', ''), '))
from [dbo].[tele]
where charindex('-', grid) > 0


--update [dbo].[tele] set [cabinet] = replace(replace(replace(location, ' ', ''), 'nocc,cab', ''), 'cr,cab', '')

update [dbo].[tele] set 
	gridletter = ltrim(rtrim(left(grid, charindex('-', grid) - 1)))
	,gridnumber = substring(grid, charindex('-', grid) + 1, 10)
where charindex('-', grid) > 0

--select * from [dbo].[tele]

select cabinet, gridletter, gridnumber, *
from [dbo].[tele_all] 


--,charindex('-', grid)
--,ltrim(rtrim(left(grid, charindex('-', grid) - 1)))
--,substring(grid, charindex('-', grid) + 1, 10)
----,reverse(left(reverse(location), charindex(' ', ltrim(reverse(location))) - 1))
----,replace(replace(replace(location, ' ', ''), 'nocc,cab', ''), 'cr,cab', '')
----ltrim(rtrim(replace(replace(location, 'CR, CAB', ''), '))
--from [dbo].[tele]
--where charindex('-', grid) > 0


--update [dbo].[tele] set [cabinet] = replace(replace(replace(location, ' ', ''), 'nocc,cab', ''), 'cr,cab', '')

--update [dbo].[tele] set 
--	gridletter = ltrim(rtrim(left(grid, charindex('-', grid) - 1)))
--	,gridnumber = substring(grid, charindex('-', grid) + 1, 10)
--where charindex('-', grid) > 0

--select * from [dbo].[tele]

--update [dbo].[tele_all] set Gridletter = left(gridmark, charindex('-', gridmark) - 1), gridnumber = substring(gridmark, charindex('-', gridmark) + 1, 10) where gridmark <> 'J-10,C-16'

--update [dbo].[tele_all] set Cabinet = replace(replace(replace(location, ' ', ''), 'nocc,cab', ''), 'cr,cab', '') where charindex('cab', location) > 0

update [dbo].[patc_all] set Room = left([Location], charindex(',', location)-1)

SELECT distinct left([Location], charindex(',', location)-1)
  FROM [Working].[dbo].[patc_all]
union
SELECT distinct left([Location], charindex(',', location)-1)
  FROM [Working].[dbo].[tele_all]