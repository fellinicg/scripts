use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwSwitchPortAll'))
drop view dbo.vwSwitchPortAll
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20141125
-- ============================================================
create view dbo.vwSwitchPortAll
as

	select *
	from openquery([TELEDB32], 
		'select distinct i.NodeID, i.InterfaceIndex, i.FullName SwitchPort, i.InterfaceSpeed, i.PhysicalAddress SwitchPortMAC, i.StatusLED, nc.MACAddress ConnectedMAC, 
			n.Caption ConnectedDevice, i.IfName 
		from dbo.Interfaces i inner join dbo.NodePortInterfaceMap ni on i.NodeID = ni.NodeID and i.InterfaceIndex = ni.IfIndex 
		inner join dbo.NodeL2Connections nc on ni.NodeID = nc.NodeID and ni.PortID = nc.PortID and ni.VlanId = nc.VlanId 
		left join dbo.Interfaces ci on nc.MACAddress = ci.PhysicalAddress
		left join dbo.Nodes n on ci.NodeID = n.NodeID')
go
grant select on dbo.vwSwitchPortAll to omni