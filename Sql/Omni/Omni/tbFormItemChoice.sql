use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbFormItemChoice') and type in (N'U'))
drop table dbo.tbFormItemChoice
go

create table dbo.tbFormItemChoice(
	FormItemChoiceID int identity(1,1) not null,
	FormItemID int not null,
	Choice varchar(500) not null,
	SortOrder tinyint not null constraint DF_tbFormItemChoice_SortOrder default ((1)),
	IsDeleted bit not null constraint DF_tbFormItemChoice_IsDeleted default ((0)),
	CreateDT smalldatetime not null constraint DF_tbFormItemChoice_CreateDT  default (getdate()),
 constraint PK_tbFormItemChoice primary key clustered 
(
	FormItemChoiceID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

