use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfSamAcctType') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfSamAcctType
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20151009
-- ============================================================
go
create function dbo.sfSamAcctType(@SamAccountType bigint)
returns char(1)
as
begin
   return (case @SamAccountType when 805306368 then 'U' when 268435456 then 'G'  when 805306369 then 'M' else 'Z' end)
end