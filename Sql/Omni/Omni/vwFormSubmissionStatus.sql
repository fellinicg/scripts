use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwFormSubmissionStatus'))
drop view dbo.vwFormSubmissionStatus
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140930
-- ============================================================
create view dbo.vwFormSubmissionStatus
as
	select a.FormSubmissionStatusID, a.FormSubmissionID, c.Description, a.CreateDT, d.FirstName, d.LastName
	from dbo.tbFormSubmissionStatus a
	inner join (
				select FormSubmissionID, max(FormSubmissionStatusID) 'FormSubmissionStatusID'
				from dbo.tbFormSubmissionStatus
				group by FormSubmissionID
				) b on a.FormSubmissionStatusID = b.FormSubmissionStatusID
	left join dbo.tbStatus c on a.StatusID = c.StatusID
	left join dbo.tbADUser d on a.ADUserID = d.ADUserID
go
grant select on dbo.vwFormSubmissionStatus to omni