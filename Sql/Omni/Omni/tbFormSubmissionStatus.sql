use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbFormSubmissionStatus') and type in (N'U'))
drop table dbo.tbFormSubmissionStatus
go

create table dbo.tbFormSubmissionStatus(
	FormSubmissionStatusID int identity(1,1) not null,
	FormSubmissionID int not null,
	StatusID int not null,
	ADUserID int not null,
	CreateDT smalldatetime not null constraint DF_tbFormSubmissionStatus_CreateDT  default (getdate()),
 constraint PK_tbFormSubmissionStatus primary key clustered 
(
	FormSubmissionStatusID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

