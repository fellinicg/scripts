use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upSearchMachine') and type in (N'P', N'PC'))
drop procedure dbo.upSearchMachine
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140627
-- ============================================================
create procedure dbo.upSearchMachine
@MachineList varchar(max)
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select Name, UDF_MajorApplications, UDF_SiteName, UDF_SiteDescription, Model, OS_Name, OS_ServicePack, Primary_IP_Address
	from AN7.dbo.Computers
	inner join dbo.tfCommaListToTable(@MachineList) on Name = ListItem

end
go
grant exec on dbo.upSearchMachine to omni