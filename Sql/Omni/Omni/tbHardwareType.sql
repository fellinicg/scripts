use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbHardwareType') and type in (N'U'))
drop table dbo.tbHardwareType
go

create table dbo.tbHardwareType(
	HardwareTypeID smallint identity(1,1) not null,
	HardwareType varchar(50) not null,
	IsDeleted bit not null constraint DF_tbHardwareType_IsDeleted default ((0)),
	CreateDT datetime not null constraint DF_tbHardwareType_CreateDT  default (getdate()),
 constraint PK_tbHardwareType primary key clustered 
(
	HardwareTypeID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off