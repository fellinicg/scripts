use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddADUsers') and type in (N'P', N'PC'))
drop procedure dbo.upAddADUsers
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140304
-- ============================================================
create procedure dbo.upAddADUsers
as
begin

	-- Initialize settings
	set nocount on

	-- Add data
	insert into dbo.tbADUser (SID, EmployeeID, UserName, FirstName, MiddleInitials, LastName, Email, Office, OfficePhone, OU, IsEnabled, DistinguishedName, PrimaryGroupID, PasswordLastSet)
	select 
		src.SID
		, src.employeeID
		, src.Name
		, src.GivenName
		, src.initials
		, src.Surname
		, src.EmailAddress
		, src.Office
		, src.OfficePhone
		, src.CanonicalName
		, case src.Enabled when 'true' then 1 else 0 end
		, src.DistinguishedName
		, src.PrimaryGroupID
		, src.PasswordLastSet
	from openrowset('Microsoft.ACE.OLEDB.12.0',
		'Text;Database=C:\Feeds\', 
		'select * from adusers.txt where sid is not null and name is not null') src
	left join dbo.tbADUser dst on src.SID = dst.SID
	where dst.SID is null

end
go
grant exec on dbo.upAddADUsers to omni