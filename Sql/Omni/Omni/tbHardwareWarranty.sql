use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbHardwareWarranty') and type in (N'U'))
drop table dbo.tbHardwareWarranty
go

create table dbo.tbHardwareWarranty(
	HardwareID int not null,
	ExpirationDate date not null,
	CreateDT smalldatetime not null constraint DF_tbHardwareWarranty_CreateDT  default (getdate()),
	UpdateDT smalldatetime not null constraint DF_tbHardwareWarranty_UpdateDT  default (getdate()),
 constraint PK_tbHardwareWarranty primary key clustered 
(
	HardwareID ASC
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off