use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbStatus') and Type in (N'U'))
drop table dbo.tbStatus
go

create table dbo.tbStatus(
	StatusID int identity(1,1) not null,
	Description varchar(100) not null,
	IsDeleted bit not null constraint DF_tbStatus_IsDeleted default ((0)),
	CreateDT smalldatetime not null constraint DF_tbStatus_CreateDT  default (getdate()),
 constraint PK_tbStatus primary key clustered 
(
	StatusID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

