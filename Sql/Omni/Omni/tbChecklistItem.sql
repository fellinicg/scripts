use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbChecklistItem') and type in (N'U'))
drop table dbo.tbChecklistItem
go

create table dbo.tbChecklistItem(
	ChecklistItemID int identity(1,1) not null,
	ChecklistItem varchar(500) not null,
	TypeID int not null,
	SortOrder tinyint not null constraint DF_tbChecklistItem_SortOrder default ((1)),
	IsDeleted bit not null constraint DF_tbChecklistItem_IsDeleted default ((0)),
	CreateDT datetime not null constraint DF_tbChecklistItem_CreateDT  default (getdate()),
 constraint PK_tbChecklistItem primary key clustered 
(
	ChecklistItemID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

