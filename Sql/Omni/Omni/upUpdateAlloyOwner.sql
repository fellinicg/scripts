use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateAlloyOwner') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateAlloyOwner
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160301
-- ============================================================
create procedure dbo.upUpdateAlloyOwner
as
begin

	-- Initialize settings
	set nocount on

	-- Update Owner_ID with ScannedUser_ID when they don't match 
	update AN7.dbo.Computers set Owner_ID = UDF_ScannedUser_ID
	from AN7.dbo.Computers a
	where 
		isnull(Owner_ID, '00000000-0000-0000-0000-000000000000') <> isnull(UDF_ScannedUser_ID, '00000000-0000-0000-0000-000000000000')
		and
		a.Status_ID in (
			'CBF98E44-75AD-42A9-AA5D-EBEFD7A26F90'		--Auto-Added
			, '7ED012D2-A4AB-4A12-995D-C769E7346C6A'	--Deployed
			, 'C0B5AB2C-4920-431C-863F-03EB37D65EE5'	--Deployed OFF Network
			, '98F0084C-FDC6-4BA1-96A5-152298363590'	--Deployed PMRY
		)
		and 
		a.Type_ID in (
			'49AB351E-E54A-4498-AA33-213C089732D3'		--WORKSTATION
			, 'ED829940-EA05-4B5C-80B0-43DE6841D2F4'	--Notebook
			, 'B2F31EB3-13D2-4F66-B384-2CDF35627649'	--Tablet
			, '5FA79873-76EB-4A3F-B808-51CD97824EBF'	--Desktop ICS
			, 'B1DCDEE2-7B1F-4558-9BF9-8BE0912FA387'	--Virtual PC
			, '4AB82B56-A00E-43A2-A599-A2A08EE300A3'	--Desktop
		)
		and
		a.UDF_ScannedUser_ID not in ('E3EF474C-63AE-453E-9260-53CDD56B7BB8', 'A0783B6F-A58A-4D3F-A42C-6B18AB7E40A9', '86A82E7C-1C45-486E-BF4A-808CBF2750F8') -- Exclude SYSTEM, MIGUSER, comanche

	-- Update Location_ID and Orgnaization_ID with latest from Persons table 
	update AN7.dbo.Computers set Organization_ID = b.Organization_ID, Location_ID = b.Location_ID
	from AN7.dbo.Computers a
		inner join AN7.dbo.Persons b on  a.Owner_ID = b.ID and (a.Location_ID <> b.Location_ID or a.Organization_ID <> b.Organization_ID)
	where 
		a.Status_ID in (
			'CBF98E44-75AD-42A9-AA5D-EBEFD7A26F90'		--Auto-Added
			, '7ED012D2-A4AB-4A12-995D-C769E7346C6A'	--Deployed
			, 'C0B5AB2C-4920-431C-863F-03EB37D65EE5'	--Deployed OFF Network
			, '98F0084C-FDC6-4BA1-96A5-152298363590'	--Deployed PMRY
		)
		and 
		a.Type_ID in (
			'49AB351E-E54A-4498-AA33-213C089732D3'		--WORKSTATION
			, 'ED829940-EA05-4B5C-80B0-43DE6841D2F4'	--Notebook
			, 'B2F31EB3-13D2-4F66-B384-2CDF35627649'	--Tablet
			, '5FA79873-76EB-4A3F-B808-51CD97824EBF'	--Desktop ICS
			, 'B1DCDEE2-7B1F-4558-9BF9-8BE0912FA387'	--Virtual PC
			, '4AB82B56-A00E-43A2-A599-A2A08EE300A3'	--Desktop
		)


end