use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfDisplayIPv4') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfDisplayIPv4
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140205
-- ============================================================
go
create function dbo.sfDisplayIPv4(@ip binary(4)) 
returns varchar(15)
as
begin
    declare @ret varchar(15)

    select @ret = cast( cast( substring( @ip, 1, 1) as integer) as varchar(3) ) + '.'
                + cast( cast( substring( @ip, 2, 1) as integer) as varchar(3) ) + '.'
                + cast( cast( substring( @ip, 3, 1) as integer) as varchar(3) ) + '.'
                + cast( cast( substring( @ip, 4, 1) as integer) as varchar(3) );

    return @ret
end