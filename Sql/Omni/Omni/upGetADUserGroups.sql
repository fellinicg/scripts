use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetADUserGroups') and type in (N'P', N'PC'))
drop procedure dbo.upGetADUserGroups
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20151008
-- ============================================================
create procedure dbo.upGetADUserGroups
	@LoginName varchar(200)
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare
		@Sql nvarchar(500),
		@DN nvarchar(500)

	-- Retrieve DistinguishedName for LoginName
	set @Sql = 'select @DN = distinguishedName from openrowset(''ADSDSOObject'', ''adsdatasource'', ''select distinguishedName from ''''LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov'''' where objectClass =  ''''User'''' and Name = ''''' + @LoginName + ''''''')'
	exec sp_executesql 
        @Query  = @Sql
      , @Params = N'@DN nvarchar(500) output'
      , @DN = @DN output

	-- Retrieve Groups that the user is a member of
	set @Sql = 'select Name as GroupName from openrowset(''ADSDSOObject'', ''adsdatasource'', ''select name from ''''LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov'''' where objectClass =  ''''Group'''' and member = ''''' + @DN + ''''''')'
	exec sp_executesql 
        @Query  = @Sql
end
go
grant exec on dbo.upGetADUserGroups to omni