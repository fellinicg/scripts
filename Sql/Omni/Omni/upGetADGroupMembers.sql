use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetADGroupMembers') and type in (N'P', N'PC'))
drop procedure dbo.upGetADGroupMembers
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20151008
-- ============================================================
create procedure dbo.upGetADGroupMembers
	@GroupName varchar(200)
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare
		@Sql nvarchar(500),
		@DN nvarchar(500)

	-- Retrieve DistinguishedName for GroupName
	set @Sql = 'select @DN = distinguishedName from openrowset(''ADSDSOObject'', ''adsdatasource'', ''select distinguishedName from ''''LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov'''' where objectClass =  ''''Group'''' and Name = ''''' + @GroupName + ''''''')'
	exec sp_executesql 
        @Query  = @Sql
      , @Params = N'@DN nvarchar(500) output'
      , @DN = @DN output

	-- Retrieve members of the group into global temporary table
	set @Sql = 'select Name as UserName from openrowset(''ADSDSOObject'', ''adsdatasource'', ''select name from ''''LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov'''' where objectCategory = ''''Person'''' and objectClass =  ''''User'''' and memberOf = ''''' + @DN + ''''''')'
	set @Sql = 'select Name as UserName, objectCategory, samaccounttype into ##grpmembers from openrowset(''ADSDSOObject'', ''adsdatasource'', ''select name, objectCategory, samaccounttype from ''''LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov'''' where memberOf = ''''' + @DN + ''''''')'
	exec(@Sql)

end
go
grant exec on dbo.upGetADGroupMembers to omni