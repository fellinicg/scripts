use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upSearchAlloyIP') and type in (N'P', N'PC'))
drop procedure dbo.upSearchAlloyIP
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140627
-- ============================================================
create procedure dbo.upSearchAlloyIP
@IP varchar(15)
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select isnull(b.Name, a.DNS_Name) 'Device', b.Description, b.IP_Address, a.MAC_Address, a.DHCP_Enabled, a.DHCP_Server, a.Last_Audit
	from (
			select b.DNS_Name, a.MAC_Address, a.IP_Address, a.DHCP_Enabled, a.DHCP_Server, b.Last_Audit
			from AD7.dbo.Inv_Network_Adapters a
			left join AD7.dbo.Inv_NetNodes b on a.Node_ID = b.ID
			where charindex(@IP, a.IP_Address) = 1) a
	full outer join (
			select isnull(b.Name, c.Name) 'Name', isnull(b.Model, c.Description) 'Description', c.MAC, a.IP_Address, c.UDF_RAW_AMRSUserFirstName 'FirstName', c.UDF_RAW_AMRSUserLastName 'LastName'
			from AN7.dbo.IP_Address_Link_List a
			left join AN7.dbo.Hardware b on a.Object_ID = b.ID
			left join AN7.dbo.Computers c on a.Object_ID = c.ID
			where charindex(@IP, a.IP_Address) = 1) b on a.IP_Address = b.IP_Address
end
go
grant exec on dbo.upSearchAlloyIP to omni