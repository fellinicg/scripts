use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetAcl') and type in (N'P', N'PC'))
drop procedure dbo.upGetAcl
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150311
-- ============================================================
create procedure dbo.upGetAcl
	@AclID varchar(100)
	,@Acl varchar(500) output
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select @Acl = Acl
	from dbo.tbAcl
	where AclID = @AclID
end
go
grant exec on dbo.upGetAcl to omni