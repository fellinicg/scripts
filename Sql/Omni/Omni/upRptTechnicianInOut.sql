use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upRptTechnicianInOut') and type in (N'P', N'PC'))
drop procedure dbo.upRptTechnicianInOut
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20160801
-- ============================================================
create procedure dbo.upRptTechnicianInOut
	@StartDate datetime
	, @EndDate datetime
	, @Rate money
	, @File varchar(100)
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @start table (Technician varchar(200), StartExtension varchar(15), StartFacility varchar(200), StartDT datetime) 
	declare @stop table (Technician varchar(200), StopExtension varchar(15), StopFacility varchar(200), StopDT datetime)
	declare @MinuteRate money = @Rate / 60

	-- Import data
	insert into @start
	exec('select [Name], Extension, Facility, convert(datetime, replace(replace([Date/Time], '' EST'', ''''), '' EDT'', ''''))
	from openrowset(''Microsoft.ACE.OLEDB.12.0'',''Text;Database=c:\feeds'',''select * from [' + @File + ']'')
	where [Start/Stop] = ''Start''')

	insert into @stop
	exec('select [Name], Extension, Facility, convert(datetime, replace(replace([Date/Time], '' EST'', ''''), '' EDT'', ''''))
	from openrowset(''Microsoft.ACE.OLEDB.12.0'',''Text;Database=c:\feeds'',''select * from [' + @File + ']'')
	where [Start/Stop] = ''Stop''')

	-- Return data
	select 
		a.Technician 'Resource Name'
		--, StartExtension
		--, StartFacility
		--, StopExtension
		--, StopFacility
		, convert(varchar, StartDT, 101) 'Date'
		, format(StartDT, 'hh:mm tt') 'Start Time'
		, format(StopDT, 'hh:mm tt') 'Stop Time'
		, case 
			when (datepart(hh, StartDT) <= 11) and (datepart(hh, StopDT) >= 19) then 1
			when convert(varchar, datediff(mi, StartDT, StopDT) / 60) >= 6 then .75
			else 0
		  end 'Meal Break'
		, round(case 
			when (datepart(hh, StartDT) <= 11) and (datepart(hh, StopDT) >= 19) then ((datediff(mi, StartDT, StopDT) / 60 + cast(datediff(mi, StartDT, StopDT) % 60 as float) / 60) - 1)
			when convert(varchar, datediff(mi, StartDT, StopDT) / 60) >= 6 then ((datediff(mi, StartDT, StopDT) / 60 + cast(datediff(mi, StartDT, StopDT) % 60 as float) / 60) - .75)
			else 0
		  end, 2) 'Billable Hours'
		, @Rate 'Rate'
		, round(case 
			when (datepart(hh, StartDT) <= 11) and (datepart(hh, StopDT) >= 19) then ((datediff(mi, StartDT, StopDT) / 60 + cast(datediff(mi, StartDT, StopDT) % 60 as float) / 60) - 1) * @Rate
			when convert(varchar, datediff(mi, StartDT, StopDT) / 60) >= 6 then ((datediff(mi, StartDT, StopDT) / 60 + cast(datediff(mi, StartDT, StopDT) % 60 as float) / 60) - .75) * @Rate
			else 0
		  end, 2) 'Total'		
		--, round(case 
		--	when (datepart(hh, StartDT) <= 11) and (datepart(hh, StopDT) >= 19) then ((datediff(mi, StartDT, StopDT) - 60) * @MinuteRate)
		--	when convert(varchar, datediff(mi, StartDT, StopDT) / 60) >= 6 then ((datediff(mi, StartDT, StopDT) - 45) * @MinuteRate)
		--	else 0
		--  end, 2) 'Total'
		--, convert(varchar, datediff(mi, StartDT, StopDT) / 60) + ' hr(s) ' + convert(varchar, datediff(second, StartDT, StopDT) % 60) + ' mins(s) ' 'Total Time'
	from @start a
	left join @stop b on convert(varchar, StartDT, 101) = convert(varchar, StopDT, 101) and a.Technician = b.Technician
	where convert(varchar, StartDT, 101) between @StartDate and @EndDate
	order by a.Technician, a.StartDT

end
go
grant exec on dbo.upRptTechnicianInOut to [PANYNJ\rmatchanickal]