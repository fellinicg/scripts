select c.Name, a.Name, a.[Warranty_Exp], s.serverid, w.*
from [PATCDB21\ALLOY_1].AN6.dbo.Computers c
left join [PATCDB21\ALLOY_1].AN6.dbo.Assets a on c.ID = a.ID
left join patcav100.pomeroymgmt.dbo.tbserver s on c.name = s.servername
left join patcav100.pomeroymgmt.[dbo].[tbServerWarranty] w on s.serverid = w.serverid
where c.Type_ID = '629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3'
	and convert(varchar(max), c.UDF_HardwareUnderWarranty) = 'Yes'


