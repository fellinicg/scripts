use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upFullInventory') and type in (N'P', N'PC'))
drop procedure dbo.upFullInventory
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140829
-- ============================================================
create procedure dbo.upFullInventory
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @servers table (idx int identity(1,1), name varchar(200))
	declare @alloy table (name varchar(200))
	declare @patchlink table (name varchar(200))
	declare @epo table (name varchar(200))
	declare @sccm table (name varchar(200))
	declare @vcenter table (name varchar(200))
	declare @solarwinds table (name varchar(200))

	-- Alloy
	insert into @alloy
	select Computer_Name DeviceName from dbo.vwAlloy 

	-- Patchlink
	insert into @patchlink
	select DeviceName from dbo.vwPatchLink

	-- ePO
	insert into @epo
	select DeviceName from dbo.vwEpo

	-- SCCM
	insert into @sccm
	select DeviceName from dbo.vwSccm

	-- vCenter
	insert into @vcenter
	select DeviceName from dbo.vwEsxHost
	union
	select distinct DeviceName from dbo.vwVirtualMachine

	-- SolarWinds
	insert into @solarwinds
	select distinct case when charindex('.', DeviceName) > 0 then substring(DeviceName, 1, charindex('.', DeviceName) - 1) else DeviceName end DeviceName from dbo.vwSolarwinds

	-- Combine all unique names
	insert into @servers(name)
	select Name from @alloy
	union
	select Name from @patchlink
	union
	select Name from @epo
	union
	select Name from @sccm
	union
	select Name from @vcenter
	union
	select Name from @solarwinds

	-- Return data
	select upper(s.name) 'Name'
		,case isnull(alloy.name, 'xxx') when 'xxx' then '' else 'X' end 'Alloy'
		,case isnull(patchlink.name, 'xxx') when 'xxx' then '' else 'X' end 'Patchlink'
		,case isnull(epo.name, 'xxx') when 'xxx' then '' else 'X' end 'ePO'
		,case isnull(sccm.name, 'xxx') when 'xxx' then '' else 'X' end 'SCCM'
		,case isnull(vcenter.name, 'xxx') when 'xxx' then '' else 'X' end 'vCenter'
		,case isnull(solarwinds.name, 'xxx') when 'xxx' then '' else 'X' end 'SolarWinds'
		,isnull(replace(replace(a.UDF_MajorApplications, char(10), ''), char(13), ''), '') 'MajorApplications'
		,isnull(replace(replace(a.Description, char(10), ''), char(13), ''), '') 'Description'
		,isnull(replace(replace(a.OS_Name, char(10), ''), char(13), ''), '') 'OS_Name'
		,isnull(replace(replace(a.UDF_SCCMHeartbeatDDR, char(10), ''), char(13), ''), '') 'SCCMHeartbeatDDR'
	from @servers s
		left join @alloy alloy on s.name = alloy.name
		left join @patchlink patchlink on s.name = patchlink.name
		left join @epo epo on s.name = epo.name
		left join @sccm sccm on s.name = sccm.name
		left join @vcenter vcenter on s.name = vcenter.name
		left join @solarwinds solarwinds on s.name = solarwinds.name
		left join dbo.vwAlloyInventoryReport a on s.name = a.Name

end
go
grant exec on dbo.upFullInventory to omni