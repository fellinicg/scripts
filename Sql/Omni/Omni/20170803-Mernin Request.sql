select 
	ID
	, Serial_Num
	, Name
	, Manufacturer
	, Description
	, Status
	, Type
	, CPU
	, CPU_Count
	, CPU_Speed
	, RAM
	, OS_Name
	, OS_ServicePack
	, OS_Version
	, Primary_IP_Address
--, * 
from dbo.Computer_List
where Type = 'Server'
-- and Status not in ('Retired')

select 
	c.ID
	, s.Product_Name
	, d.Install_Date
	, d.Install_Key
	, d.Serial_Num
	, d.Last_Audit
	, s.Manufacturer
	, s.Platform
from dbo.Computer_List c
inner join dbo.Detected_Software_Products d on c.ID = d.Object_ID
inner join dbo.Soft_Product_List s on d.Soft_Product_ID = s.ID
where c.Type = 'Server'
