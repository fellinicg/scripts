use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbSwitchPort') and type in (N'U'))
drop table dbo.tbSwitchPort
go

create table dbo.tbSwitchPort(
	SwitchPortID int identity(1,1) not null,
	HardwareID int not null,
	Port varchar(20) not null,
	Module tinyint not null constraint DF_tbswitchport_card  default ((0)),
	PortNumber smallint null,
	IsDeleted bit not null constraint DF_tbSwitchPort_IsDeleted default ((0)),
	CreateDT smalldatetime not null constraint DF_tbSwitchPort_CreateDT  default (getdate()),
	UpdateDT smalldatetime not null constraint DF_tbSwitchPort_UpdateDT  default (getdate()),
 constraint PK_tbSwitchPort primary key clustered 
(
	SwitchPortID ASC
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off