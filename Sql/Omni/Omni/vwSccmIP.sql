use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwSccmIP'))
drop view dbo.vwSccmIP
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140904
-- ============================================================
create view dbo.vwSccmIP
as

	select 'SCCM' Source, Name, IPAddress
	from openquery([TELEDV10], 'select distinct s.Name0 Name, i.IP_Addresses0 IPAddress from CM_PR1.dbo.System_IP_Address_ARR i left join CM_PR1.dbo.System_DISC s on i.ItemKey = s.ItemKey where charindex('':'', i.IP_Addresses0) = 0')
go
grant select on dbo.vwSccmIP to omni