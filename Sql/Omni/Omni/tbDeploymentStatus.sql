use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbDeploymentStatus') and type in (N'U'))
drop table dbo.tbDeploymentStatus
go

create table dbo.tbDeploymentStatus(
	DeploymentStatusID tinyint identity(1,1) not null,
	DeploymentStatus varchar(50) not null,
	IsDeleted bit not null constraint DF_tbDeploymentStatus_IsDeleted default ((0)),
 constraint PK_tbDeploymentStatus primary key clustered 
(
	DeploymentStatusID ASC
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off