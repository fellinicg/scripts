use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upImportPeoplesoftData') and type in (N'P', N'PC'))
drop procedure dbo.upImportPeoplesoftData
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140623
-- ============================================================
create procedure dbo.upImportPeoplesoftData
as
begin

	-- Initialize settings
	set nocount on

	-- Update data
	update dbo.tbADUser 
	set PeoplesoftID = p.EMPLID
		, PeoplesoftOrgUnit = p.[ORG UNIT DESCR]
		, OfficePhone = p.[WORK PHONE 1]
		, MobilePhone = p.[CELL PHONE 1]
		, PeoplesoftDept = p.[DEPT DESCR]
		, PeoplesoftJobDescription = p.[JOB DESCR]
		, PeoplesoftLocation = p.[LOCATION]
		, PeoplesoftLocDescription = p.[LOCATION DESCR]
		, PeoplesoftStatus = p.[STATUS]
		, PeoplesoftTermDT = p.[TERMDT]
	from dbo.tbADUser m
		left join openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [AD_LISTING2.TXT]') p on m.EmployeeID = p.EMPLID
	where p.EMPLID is not null


	update dbo.tbADUser 
	set PeoplesoftID = p.EMPLID
		, PeoplesoftOrgUnit = p.[ORG UNIT DESCR]
		, OfficePhone = p.[WORK PHONE 1]
		, MobilePhone = p.[CELL PHONE 1]
		, PeoplesoftDept = p.[DEPT DESCR]
		, PeoplesoftJobDescription = p.[JOB DESCR]
		, PeoplesoftLocation = p.[LOCATION]
		, PeoplesoftLocDescription = p.[LOCATION DESCR]
		, PeoplesoftStatus = p.[STATUS]
		, PeoplesoftTermDT = convert(date, p.[TERMDT])
	from dbo.tbADUser m
		left join openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [AD_LISTING2.TXT]') p on m.Username = p.USERID
	where p.USERID is not null 
		and m.PeoplesoftID is null

	update dbo.tbADUser 
	set PeoplesoftID = p.EMPLID
		, PeoplesoftOrgUnit = p.[ORG UNIT DESCR]
		, OfficePhone = p.[WORK PHONE 1]
		, MobilePhone = p.[CELL PHONE 1]
		, PeoplesoftDept = p.[DEPT DESCR]
		, PeoplesoftJobDescription = p.[JOB DESCR]
		, PeoplesoftLocation = p.[LOCATION]
		, PeoplesoftLocDescription = p.[LOCATION DESCR]
		, PeoplesoftStatus = p.[STATUS]
		, PeoplesoftTermDT = convert(date, p.[TERMDT])
	from dbo.tbADUser m
		left join openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [AD_LISTING2.TXT]') p on m.Username = p.USERID
	where p.USERID is not null 
		and m.EmployeeID is null

end