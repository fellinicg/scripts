use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbServerDependency') and type in (N'U'))
drop table dbo.tbServerDependency
go

create table dbo.tbServerDependency(
	HardwareID int not null,
	DependencyID int not null,
	TypeID int not null,
 constraint PK_tbServerDependency primary key clustered 
(
	HardwareID ASC,
	DependencyID ASC
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

