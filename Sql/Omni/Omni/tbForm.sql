use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbForm') and type in (N'U'))
drop table dbo.tbForm
go

create table dbo.tbForm(
	FormID int not null,
	Name varchar(50) not null,
	Description varchar(500) not null,
	CreateDT datetime not null constraint DF_tbForm_CreateDT  default (getdate()),
 constraint PK_tbForm primary key clustered 
(
	FormID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

