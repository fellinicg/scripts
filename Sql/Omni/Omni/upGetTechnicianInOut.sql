use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetTechnicianInOut') and type in (N'P', N'PC'))
drop procedure dbo.upGetTechnicianInOut
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150511
-- ============================================================
create procedure dbo.upGetTechnicianInOut
@StartDate datetime
,@EndDate datetime
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @start table (Technician varchar(200), StartExtension varchar(15), StartFacility varchar(200), StartDT datetime) 
	declare @stop table (Technician varchar(200), StopExtension varchar(15), StopFacility varchar(200), StopDT datetime) 

	-- Import data
	insert into @start
	select [Name], Extension, Facility, convert(datetime, replace(replace([Date/Time], ' EST', ''), ' EDT', ''))
	from openrowset('Microsoft.ACE.OLEDB.12.0','Text;Database=c:\feeds','select * from [office365.csv]')
	where [Start/Stop] = 'Start'

	insert into @stop
	select [Name], Extension, Facility, convert(datetime, replace(replace([Date/Time], ' EST', ''), ' EDT', ''))
	from openrowset('Microsoft.ACE.OLEDB.12.0','Text;Database=c:\feeds','select * from [office365.csv]')
	where [Start/Stop] = 'stop'

	-- Return data
	select 
		a.Technician
		, StartExtension
		, StartFacility
		, StopExtension
		, StopFacility
		, convert(varchar, StartDT, 100) 'Start'
		, convert(varchar, StopDT, 100) 'Stop'
		, convert(varchar, datediff(mi, StartDT, StopDT) / 60) + ' hr(s) ' + convert(varchar, datediff(second, StartDT, StopDT) % 60) + ' mins(s) ' 'Total Time'
	from @start a
	left join @stop b on convert(varchar, StartDT, 101) = convert(varchar, StopDT, 101) and a.Technician = b.Technician
	where convert(varchar, StartDT, 101) between @StartDate and @EndDate
	order by Technician, StartDT

end
go
grant exec on dbo.upGetTechnicianInOut to omni