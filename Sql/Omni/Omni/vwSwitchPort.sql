use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwSwitchPort'))
drop view dbo.vwSwitchPort
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20141202
-- ============================================================
create view dbo.vwSwitchPort
as

	select *
	from openquery([TELEDB48], 
		'select distinct i.NodeID, x.Sysname, i.InterfaceIndex, i.FullName SwitchPort, i.IfName InterfaceName, i.InterfaceSpeed, i.PhysicalAddress SwitchPortMAC, i.StatusLED,
			nc.MACAddress ConnectedMAC, n.Caption ConnectedDevice, n.Pomeroy_Support
		from SolarWindsOrion.dbo.Interfaces i
		left outer join (select distinct i.NodeID, i.InterfaceID
							from SolarWindsOrion.dbo.Interfaces i 
							inner join SolarWindsOrion.dbo.NodePortInterfaceMap ni on i.NodeID = ni.NodeID and i.InterfaceIndex = ni.IfIndex
							inner join SolarWindsOrion.dbo.NodeL2Connections nc on ni.NodeID = nc.NodeID and ni.PortID = nc.PortID and ni.VlanId = nc.VlanId
							inner join SolarWindsOrion.dbo.Interfaces ci on nc.MACAddress = ci.PhysicalAddress
							inner join SolarWindsOrion.dbo.Nodes n on ci.NodeID = n.NodeID
							where Pomeroy_Support = ''Networking''
							group by i.NodeID, i.InterfaceID) e on i.NodeID = e.NodeID and i.InterfaceID = e.InterfaceID
		inner join SolarWindsOrion.dbo.NodePortInterfaceMap ni on i.NodeID = ni.NodeID and i.InterfaceIndex = ni.IfIndex
		inner join SolarwindsOrion.dbo.Nodes x on i.NodeID = x.NodeID
		--left join SolarWindsOrion.dbo.NodeVlans nv on ni.NodeID = nv.NodeID and isnull(ni.VlanId, -1) = isnull(nv.VlanId, -1)
		inner join SolarWindsOrion.dbo.NodeL2Connections nc on ni.NodeID = nc.NodeID and ni.PortID = nc.PortID and isnull(ni.VlanId, -1) = isnull(nc.VlanId , -1)
		left join SolarWindsOrion.dbo.Interfaces ci on nc.MACAddress = ci.PhysicalAddress
		left join SolarWindsOrion.dbo.Nodes n on ci.NodeID = n.NodeID
		where e.NodeID is null')
go
grant select on dbo.vwSwitchPort to omni