use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upRptAdministrators') and type in (N'P', N'PC'))
drop procedure dbo.upRptAdministrators
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140730
-- ============================================================
create procedure dbo.upRptAdministrators (@RunOnly bit = 0)
as
begin

	-- Declarations
	declare 
		@AdminReportID int
		,@PrevRptID int

	-- Run or create
	if @RunOnly = 0
	begin
		-- Add Admin Report
		insert into dbo.tbAdminReport (CreateDT)
		values (getdate())
		set @AdminReportID = @@IDENTITY

		-- Set IsCurrent
		update dbo.tbAdminReport set IsCurrent = 0
		from dbo.tbAdminReport a
		inner join(
			select CreateDT
			from dbo.tbAdminReport
			where AdminReportID = @AdminReportID) b on datepart(mm, a.CreateDT) = datepart(mm, b.CreateDT) and datepart(yy, a.CreateDT) = datepart(yy, b.CreateDT)
		where a.AdminReportID < @AdminReportID

		-- Domain Admins
		insert into dbo.tbAdminReportDetail (AdminReportID, GroupSID, MemberSID)
		select @AdminReportID, b.SID, dbo.sfHexSidToString(a.objectsid)
		from openquery (
			ADSI, 
			'<LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov>;
			(memberOf:1.2.840.113556.1.4.1941:=CN=Domain Admins,CN=Users,DC=PACORP,DC=PANYNJ,DC=GOV);
			objectsid,samaccounttype;subtree') a
		join dbo.tbADGroup b on GroupName = 'Domain Admins'
		where samaccounttype = 805306368

		-- WorkstationAdmins
		insert into dbo.tbAdminReportDetail (AdminReportID, GroupSID, MemberSID)
		select @AdminReportID, b.SID, dbo.sfHexSidToString(a.objectsid)
		from openquery (
			ADSI, 
			'<LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov>;
			(memberOf:1.2.840.113556.1.4.1941:=CN=WorkstationAdmins,OU=Groups,OU=Network Resources,DC=PACORP,DC=PANYNJ,DC=GOV);
			objectsid,samaccounttype;subtree') a
		join dbo.tbADGroup b on GroupName = 'WorkstationAdmins'
		where samaccounttype = 805306368
	end
	else
	begin
		select @AdminReportID = max(AdminReportID)
		from dbo.tbAdminReport
	end

	-- Get Previous Report ID
	select @PrevRptID = max(AdminReportID)
	from dbo.tbAdminReport
	where AdminReportID < @AdminReportID
		and IsCurrent = 1


	-- Overview

	--select 'Domain Administrators', sum(case isnull(b.MemberSID, '0') when '0' then 0 else 1 end) 'Added', sum(case isnull(a.MemberSID, '0') when '0' then 0 else 1 end) 'Removed'
	--from (
	--		select distinct MemberSID
	--		from dbo.tbAdminReportDetail
	--		where AdminReportID = @PrevRptID 
	--		and GroupSID = 'S-1-5-21-1788821970-1445399052-6498272-512') a
	--full outer join (
	--		select distinct MemberSID
	--		from dbo.tbAdminReportDetail
	--		where AdminReportID = @AdminReportID 
	--		and GroupSID = 'S-1-5-21-1788821970-1445399052-6498272-512') b on a.MemberSID = b.MemberSID
	--where a.MemberSID is null or b.MemberSID is null
	--union
	--select 'Workstation Administrators', sum(case isnull(b.MemberSID, '0') when '0' then 0 else 1 end) 'Added', sum(case isnull(a.MemberSID, '0') when '0' then 0 else 1 end) 'Removed'
	--from (
	--		select distinct MemberSID
	--		from dbo.tbAdminReportDetail
	--		where AdminReportID = @PrevRptID 
	--		and GroupSID = 'S-1-5-21-1788821970-1445399052-6498272-30333') a
	--full outer join	(
	--		select distinct MemberSID
	--		from dbo.tbAdminReportDetail
	--		where AdminReportID = @AdminReportID 
	--		and GroupSID = 'S-1-5-21-1788821970-1445399052-6498272-30333') b on a.MemberSID = b.MemberSID
	--where a.MemberSID is null or b.MemberSID is null
	select [Users Added to Domain Administrators], [Users Removed From Domain Administrators]
	from (
		select isnull(a.Row, isnull(b.Row, 1)) as Row, isnull(a.UserName, '') 'Users Added to Domain Administrators', isnull(b.UserName, '') 'Users Removed From Domain Administrators'
		from (
			select row_number() over (order by b.MemberSID) as Row, b.*
			from (
					select distinct MemberSID, UserName
					from dbo.tbAdminReportDetail
					inner join dbo.tbADUser on MemberSID = SID
					where AdminReportID = @PrevRptID 
					and GroupSID = 'S-1-5-21-1788821970-1445399052-6498272-512') a
			full outer join (
					select distinct MemberSID, UserName
					from dbo.tbAdminReportDetail
					inner join dbo.tbADUser on MemberSID = SID
					where AdminReportID = @AdminReportID 
					and GroupSID = 'S-1-5-21-1788821970-1445399052-6498272-512') b on a.MemberSID = b.MemberSID
			where a.MemberSID is null) a
		full outer join
			(
			select row_number() over (order by a.MemberSID) as Row, a.*
			from (
					select distinct MemberSID, UserName
					from dbo.tbAdminReportDetail
					inner join dbo.tbADUser on MemberSID = SID
					where AdminReportID = @PrevRptID 
					and GroupSID = 'S-1-5-21-1788821970-1445399052-6498272-512') a
			full outer join (
					select distinct MemberSID, UserName
					from dbo.tbAdminReportDetail
					inner join dbo.tbADUser on MemberSID = SID
					where AdminReportID = @AdminReportID 
					and GroupSID = 'S-1-5-21-1788821970-1445399052-6498272-512') b on a.MemberSID = b.MemberSID
			where b.MemberSID is null) b on a.Row = b.Row
		union
		select 99, convert(varchar, sum(case isnull(b.MemberSID, '0') when '0' then 0 else 1 end)) 'Added', convert(varchar, sum(case isnull(a.MemberSID, '0') when '0' then 0 else 1 end)) 'Removed'
		from (
				select distinct MemberSID
				from dbo.tbAdminReportDetail
				where AdminReportID = @PrevRptID 
				and GroupSID = 'S-1-5-21-1788821970-1445399052-6498272-512') a
		full outer join	(
				select distinct MemberSID
				from dbo.tbAdminReportDetail
				where AdminReportID = @AdminReportID 
				and GroupSID = 'S-1-5-21-1788821970-1445399052-6498272-512') b on a.MemberSID = b.MemberSID
		where a.MemberSID is null or b.MemberSID is null) a
	order by a.Row

	select [Users Added to Workstation Administrators], [Users Removed From Workstation Administrators]
	from (
		select isnull(a.Row, isnull(b.Row, 1)) as Row, isnull(a.UserName, '') 'Users Added to Workstation Administrators', isnull(b.UserName, '') 'Users Removed From Workstation Administrators'
		from (
			select row_number() over (order by b.MemberSID) as Row, b.*
			from (
					select distinct MemberSID, UserName
					from dbo.tbAdminReportDetail
					inner join dbo.tbADUser on MemberSID = SID
					where AdminReportID = @PrevRptID 
					and GroupSID = 'S-1-5-21-1788821970-1445399052-6498272-30333') a
			full outer join (
					select distinct MemberSID, UserName
					from dbo.tbAdminReportDetail
					inner join dbo.tbADUser on MemberSID = SID
					where AdminReportID = @AdminReportID 
					and GroupSID = 'S-1-5-21-1788821970-1445399052-6498272-30333') b on a.MemberSID = b.MemberSID
			where a.MemberSID is null) a
		full outer join
			(
			select row_number() over (order by a.MemberSID) as Row, a.*
			from (
					select distinct MemberSID, UserName
					from dbo.tbAdminReportDetail
					inner join dbo.tbADUser on MemberSID = SID
					where AdminReportID = @PrevRptID 
					and GroupSID = 'S-1-5-21-1788821970-1445399052-6498272-30333') a
			full outer join (
					select distinct MemberSID, UserName
					from dbo.tbAdminReportDetail
					inner join dbo.tbADUser on MemberSID = SID
					where AdminReportID = @AdminReportID 
					and GroupSID = 'S-1-5-21-1788821970-1445399052-6498272-30333') b on a.MemberSID = b.MemberSID
			where b.MemberSID is null) b on a.Row = b.Row
		union
		select 99, convert(varchar, sum(case isnull(b.MemberSID, '0') when '0' then 0 else 1 end)) 'Added', convert(varchar, sum(case isnull(a.MemberSID, '0') when '0' then 0 else 1 end)) 'Removed'
		from (
				select distinct MemberSID
				from dbo.tbAdminReportDetail
				where AdminReportID = @PrevRptID 
				and GroupSID = 'S-1-5-21-1788821970-1445399052-6498272-30333') a
		full outer join	(
				select distinct MemberSID
				from dbo.tbAdminReportDetail
				where AdminReportID = @AdminReportID 
				and GroupSID = 'S-1-5-21-1788821970-1445399052-6498272-30333') b on a.MemberSID = b.MemberSID
		where a.MemberSID is null or b.MemberSID is null) a
	order by a.Row

	-- Novell Admins
	select FirstName, LastName, UserName, Description
	from dbo.tbNovellAdmin

	-- Domain Admins
	select distinct 
		b.FirstName
		, b.LastName
		, b.UserName
		, char(34) + c.Description + char(34) 'Description'
		, c.AppOwner
		, case c.PasswordKnown when 1 then 'Yes' when 0 then 'No' else '' end 'PasswordKnown'
		, c.PasswordKnownBy 
		, case isnull(c.Description, '*****') when '*****' then 'Administrator Account' else 'Administrator Process Account' end 'AccountType'
		, b.PasswordLastSet
	from dbo.tbAdminReportDetail a
	inner join dbo.tbADUser b on a.MemberSID = b.SID
	left join dbo.tbAdminReportLookup c on a.MemberSID = c.SID
	where a.AdminReportID = @AdminReportID
		and a.GroupSID = 'S-1-5-21-1788821970-1445399052-6498272-512'
	order by PasswordKnown, b.UserName

	-- Workstation Admins
	select distinct 
		b.FirstName
		, b.LastName
		, b.UserName
		, char(34) + c.Description + char(34) 'Description'
		, c.AppOwner
		, case c.PasswordKnown when 1 then 'Yes' when 0 then 'No' else '' end 'PasswordKnown'
		, c.PasswordKnownBy
		, case isnull(c.Description, '*****') when '*****' then 'Administrator Account' else 'Administrator Process Account' end 'AccountType'
		, b.PasswordLastSet
	from dbo.tbAdminReportDetail a
	inner join dbo.tbADUser b on a.MemberSID = b.SID
	left join dbo.tbAdminReportLookup c on a.MemberSID = c.SID
	where a.AdminReportID = @AdminReportID
		and a.GroupSID = 'S-1-5-21-1788821970-1445399052-6498272-30333'
	order by PasswordKnown, b.UserName

	-- Delete temporary table if it exists
	if object_id('tempdb..#Results') is not null
		drop table #Results

	-- Insert data into temporary table
	select d.Name 'MachineName', d.Serial_Num, isnull(replace(UDF_RAW_AMRSUserDepartment, '<Unable to open registry key>', u.PeoplesoftDept), '') Department, 
	d.OS_Name, d.OS_Version, a.s 'UserName', s.Status, ug.Accounts, d.Audit_Date
	into #Results
	from AD7.dbo.Computers d
		inner join AN7.dbo.Computers n on d.Node_ID = n.Import_ID
		inner join AN7.dbo.Status s on s.ID = n.Status_ID
		inner join AD7.dbo.Inv_User_Groups ug on d.Node_ID = ug.Node_ID
		cross apply AD7.dbo.fnSplitString(ug.Accounts, ',') a
		left join dbo.tbADUser u on replace(a.s, '"', '') = 'PANYNJ\' + u.UserName
	where n.Type_ID not in ('629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3', 'B37E1387-CF14-4562-930D-DD48D5F1837A', 'E00622AC-074D-4F49-9654-8CEA4FC56C4E') -- Servers
		and n.Status_ID not in ('3417FB44-7DF7-4C9F-9A25-AAB4CC4C43E6', '89F5D1A2-4B43-4C40-9E71-1A5BE2EB1C3A') -- Retired and Discovery Delete
	--	and ug.Name in ('Administrators', 'Power Users')
		and ug.Name in ('Administrators')
		and charindex('PANYNJ\', a.s) = 1 -- Domain Users only
		and datediff(day, d.Audit_Date, SYSDATETIME()) < 90 -- Audited in the last 90 days
		and u.IsEnabled = 1 -- AD enabled users only

	-- Windows 7
	select 
		MachineName
		, Serial_Num 'Serial Number'
		, Department
		, UserName
		, Status
		, convert(varchar, Audit_Date, 100) 'AuditDate'
	from #Results
	where left(OS_Version, 1) = '6'
	order by MachineName

	-- Windows XP
	select 
		MachineName
		, Serial_Num 'Serial Number'
		, Department
		, UserName
		, Status
		, convert(varchar, Audit_Date, 100) 'AuditDate'
	from #Results
	where left(OS_Version, 1) = '5'
	order by MachineName

	-- Computers
	select distinct 
		d.Name 'MachineName'
		, ug.Accounts
		, d.OS_Name 'Operating System'
		, s.Status
	from AD7.dbo.Computers d
		inner join AN7.dbo.Computers n on d.Node_ID = n.Import_ID
		inner join AN7.dbo.Status s on s.ID = n.Status_ID
		inner join AD7.dbo.Inv_User_Groups ug on d.Node_ID = ug.Node_ID
	where n.Type_ID <> '629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3' -- Servers
		and n.Status_ID not in ('3417FB44-7DF7-4C9F-9A25-AAB4CC4C43E6', '89F5D1A2-4B43-4C40-9E71-1A5BE2EB1C3A') -- Retired and Discovery Delete
		and ug.Name in ('Administrators')
		and datediff(day, d.Audit_Date, SYSDATETIME()) < 90 -- Audited in the last 90 days
	order by d.OS_Name, MachineName

	-- Cleanup temporary table
	if object_id('tempdb..#Results') is not null
		drop table #Results

end
go
grant exec on dbo.upRptAdministrators to [PANYNJ\rcannon]