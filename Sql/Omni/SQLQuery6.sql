select distinct s.NetbiosName, ePO4_TELEAV76.dbo.RSDFN_ConvertIntToIPString(i.IPV4) IPAddress
from ePO4_TELEAV76.dbo.RSDInterfaceProperties i
inner join ePO4_TELEAV76.dbo.RSDDetectedSystemProperties s on i.HostID = s.HostID

			select distinct s.NetbiosName, ePO4_TELEAV76.dbo.RSDFN_ConvertIntToIPString(i.IPV4) IPAddress, i.LastDetectedTime
			from ePO4_TELEAV76.dbo.RSDInterfaceProperties i
				inner join ePO4_TELEAV76.dbo.RSDDetectedSystemProperties s on i.HostID = s.HostID

			union
			select
				NetbiosName
				, ePO4_TELEAV76.dbo.RSDFN_ConvertIntToIPString(IPV4) IPAddress
			from ePO4_TELEAV76.dbo.RSDDetectedSystemProperties
			union
			select
				ComputerName
				,IPAddress
			from ePO4_TELEAV76.dbo.EPOComputerProperties
			where len(isnull(IPAddress, '''')) > 0