use omni
select 'ePO' Source, isnull(NetBiosName, IPAddress) Name, IPAddress, LastDetectedTime
	from openquery([TELEDV36], '
			select s.NetbiosName, ePO4_TELEAV76.dbo.RSDFN_ConvertIntToIPString(i.IPV4) IPAddress, i.LastDetectedTime
			from ePO4_TELEAV76.dbo.RSDInterfaceProperties i
				inner join ePO4_TELEAV76.dbo.RSDDetectedSystemProperties s on i.HostID = s.HostID
			union
			select NetbiosName, ePO4_TELEAV76.dbo.RSDFN_ConvertIntToIPString(IPV4) IPAddress, LastDetectedTime
			from ePO4_TELEAV76.dbo.RSDDetectedSystemProperties
			union
			select c.ComputerName, c.IPAddress, l.LastUpdate
			from ePO4_TELEAV76.dbo.EPOComputerProperties c
				left join ePO4_TELEAV76.dbo.EPOLeafNode l on c.ParentID = l.AutoID
			where len(isnull(IPAddress, '''')) > 0'
			)


			select *
			from vwEpoIP