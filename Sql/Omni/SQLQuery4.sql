			select 'AD6' Source, ID, Name, IP_Address, Last_Date
			from AD6.dbo.Inv_NetNodes
			where len(isnull(IP_Address, '')) > 0
			union
			select 'AD6' Source, n.ID, n.Name, a.IP_Address, n.Last_Date
			from AD6.dbo.Inv_Network_Adapters a
			inner join AD6.dbo.Inv_NetNodes n on a.Node_ID = n.ID
			where len(isnull(a.IP_Address, '')) > 0
			union
			select 'AN6' Source, ID, Name, Primary_IP_Address, Audit_Date
			from AN6.dbo.Computers
			where len(isnull(Primary_IP_Address, '')) > 0
			union
			select 'AN6' Source, c.ID, Name, IP_Address, c.Audit_Date
			from AN6.dbo.IP_Address_Link_List i
			inner join AN6.dbo.Computers c on i.Object_ID = c.ID
			where len(isnull(i.IP_Address, '')) > 0

			select *
			from Omni.dbo.vwAlloyIP