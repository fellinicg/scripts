select s.NetbiosName, ePO4_TELEAV76.dbo.RSDFN_ConvertIntToIPString(i.IPV4) IPAddress, i.MAC
from ePO4_TELEAV76.dbo.RSDInterfaceProperties i
inner join ePO4_TELEAV76.dbo.RSDDetectedSystemProperties s on i.HostID = s.HostID
union
select NetbiosName, ePO4_TELEAV76.dbo.RSDFN_ConvertIntToIPString(IPV4) IPAddress, MAC
from ePO4_TELEAV76.dbo.RSDDetectedSystemProperties
union
select ComputerName, IPAddress, NetAddress
from ePO4_TELEAV76.dbo.EPOComputerProperties
where len(isnull(IPAddress, '')) > 0