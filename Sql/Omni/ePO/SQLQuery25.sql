select n.NodeName
from dbo.EPOLeafNode n
left join dbo.RSDDetectedSystemProperties s on n.NodeName = s.NetbiosName
where s.NetbiosName is null

select s.NetbiosName
from dbo.EPOLeafNode n
right join dbo.RSDDetectedSystemProperties s on n.NodeName = s.NetbiosName
where n.NodeName is null


select FriendlyName, dbo.RSDFN_ConvertIntToIPString(IPV4) IPAddress
from dbo.RSDDetectedSystemProperties s
where MAC = '0050600D977F'

--NetbiosName is null