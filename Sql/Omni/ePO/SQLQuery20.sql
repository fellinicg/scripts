select distinct 
	s.NetbiosName
	, ePO4_TELEAV76.dbo.RSDFN_ConvertIntToIPString(i.IPV4) IPAddress
	, i.MAC
	, i.LastDetectedTime
from 
	ePO4_TELEAV76.dbo.RSDInterfaceProperties i
inner join 
	ePO4_TELEAV76.dbo.RSDDetectedSystemProperties s on i.HostID = s.HostID
where 
	s.NetbiosName = 'MJMYGFM'


select distinct 
	n.NodeName
	, c.IPAddress
	, c.NetAddress
	, n.LastUpdate
from 
	ePO4_TELEAV76.dbo.EPOComputerProperties c
inner join 
	dbo.EPOLeafNode n on c.ParentID = n.AutoID
where 
	n.NodeName = 'MJMYGFM'

--len(isnull(IPAddress, '')) > 0