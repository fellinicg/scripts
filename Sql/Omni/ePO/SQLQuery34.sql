select a.HostName, a.IPAddress, a.MAC, max(case when a.LastDetectedTime > b.LastUpdate then a.LastDetectedTime else b.LastUpdate end)
from (
	select
		isnull(s.NetbiosName, s.FriendlyName) HostName
		, ePO4_TELEAV76.dbo.RSDFN_ConvertIntToIPString(i.IPV4) IPAddress
		, i.MAC
		, i.LastDetectedTime
		--, 'RSDInterfaceProperties' DataSource
	from 
		ePO4_TELEAV76.dbo.RSDInterfaceProperties i
	inner join 
		ePO4_TELEAV76.dbo.RSDDetectedSystemProperties s on i.HostID = s.HostID
) a
left join (
	select 
		n.NodeName
		, c.IPAddress
		, c.NetAddress
		, n.LastUpdate
		--, 'EPOComputerProperties' DataSource
	from 
		ePO4_TELEAV76.dbo.EPOComputerProperties c
	inner join 
		dbo.EPOLeafNode n on c.ParentID = n.AutoID
	where
		len(isnull(c.IPAddress, '')) > 0
) b on a.IPAddress = b.IPAddress
where a.HostName = 'LKKLBZM'
group by a.HostName, a.IPAddress, a.MAC