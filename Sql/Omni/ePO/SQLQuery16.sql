select distinct isnull(s.NetbiosName, s.FriendlyName), ePO4_TELEAV76.dbo.RSDFN_ConvertIntToIPString(i.IPV4) IPAddress, i.MAC, i.LastDetectedTime, 'top' Location
from ePO4_TELEAV76.dbo.RSDInterfaceProperties i
inner join ePO4_TELEAV76.dbo.RSDDetectedSystemProperties s on i.HostID = s.HostID
--union
--select * --NetbiosName, ePO4_TELEAV76.dbo.RSDFN_ConvertIntToIPString(IPV4) IPAddress
--from ePO4_TELEAV76.dbo.RSDDetectedSystemProperties
union
select distinct n.NodeName, c.IPAddress, c.NetAddress, n.LastUpdate, 'bottom'
from ePO4_TELEAV76.dbo.EPOComputerProperties c
inner join dbo.EPOLeafNode n on c.ParentID = n.AutoID
where len(isnull(IPAddress, '')) > 0