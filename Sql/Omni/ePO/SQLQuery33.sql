select distinct 
	isnull(s.NetbiosName, s.FriendlyName) HostName
	, ePO4_TELEAV76.dbo.RSDFN_ConvertIntToIPString(i.IPV4) IPAddress
	, i.MAC
	, convert(varchar, i.LastDetectedTime, 100) LastDetectedTime
	--, 'RSDInterfaceProperties' DataSource
from 
	ePO4_TELEAV76.dbo.RSDInterfaceProperties i
inner join 
	ePO4_TELEAV76.dbo.RSDDetectedSystemProperties s on i.HostID = s.HostID
union
select distinct 
	n.NodeName
	, c.IPAddress
	, c.NetAddress
	, convert(varchar, n.LastUpdate, 100)
	--, 'EPOComputerProperties' DataSource
from 
	ePO4_TELEAV76.dbo.EPOComputerProperties c
inner join 
	dbo.EPOLeafNode n on c.ParentID = n.AutoID
where
	len(isnull(c.IPAddress, '')) > 0
order by HostName