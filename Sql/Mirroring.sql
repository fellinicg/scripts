alter database PomeroyMgmt set partner failover
--alter database cur_lmrp set partner failover
--alter database cur_lmrp set partner resume
--alter database cur_lmrp set partner suspend

if exists (select 1 from sys.database_mirroring where db_name(database_id) = N'cur_lmrp' 
and mirroring_role = 1 and mirroring_state = 4) alter database cur_lmrp set partner failover