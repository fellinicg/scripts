USE [TransactionSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: July 18, 2006
-- Description:	Delete an account comment
-- =============================================
CREATE PROCEDURE dbo.upDeleteAccountComment 
	@AccountCommentID int,
	@AccountID int,
	@Result char(1) OUTPUT -- (S)uccess or (F)ailure
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'

	-- Delete record in tbEmergencyContact
	DELETE FROM dbo.tbAccountComment
	WHERE AccountCommentID = @AccountCommentID
	AND AccountID = @AccountID

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		RAISERROR('upDeleteAccountComment - account comment was NOT successfully deleted', 16, 1)

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT EXECUTE
ON dbo.upDeleteAccountComment
TO system