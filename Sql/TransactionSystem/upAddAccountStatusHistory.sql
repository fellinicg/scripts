USE [TransactionSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Todd Fellini
-- Create date: July 13, 2006
-- Description:	Add Account Status History
-- =============================================
ALTER PROCEDURE dbo.upAddAccountStatusHistory
	@AccountID int,
	@Status char(1),  -- (O)pen, (C)losed, or (H)old,
	@InitiatorGUID int,
	@Comment varchar(4000) = '',
	@Result char(1) OUTPUT -- (S)uccess or (F)ailure
AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp datetime
	DECLARE @AccountCommentID int
	DECLARE @rc char(1)

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CURRENT_TIMESTAMP
	SET @AccountCommentID = null
	SET @Result = 'F'

	-- ***** Check Inputs *****
	-- 
	-- Validate @AccountID
	IF (NOT EXISTS(SELECT 1 FROM dbo.tbAccount WHERE AccountID = @AccountID))
	BEGIN
		RAISERROR('upAddAccountStatusHistory - @AccountID supplied is not a valid account on the system', 16, 1)
		RETURN
	END

	-- Validate @InitiatorGUID
	IF (NOT EXISTS(SELECT 1 FROM SQL2KPROD.GlobalUsers.dbo.tbUserAccount WHERE UserAccountID = @InitiatorGUID))
	BEGIN
		RAISERROR('upAddAccountStatusHistory - @InitiatorGUID supplied is not a valid user account id on the system', 16, 1)
		RETURN
	END

	-- Validate @Status
	IF (NOT EXISTS(SELECT 1 FROM dbo.ufStatusTypes() WHERE Type = @Status))
	BEGIN
		RAISERROR('upAddAccountStatusHistory - @Status supplied is not a valid status on the system', 16, 1)
		RETURN
	END

	-- Add comment if supplied
	IF (LEN(LTRIM(RTRIM(@Comment))) > 0)
		BEGIN
			-- Add comment
			EXECUTE dbo.upAddAccountComment 
			   @AccountID = @AccountID
			  ,@CommenterGUID = @InitiatorGUID
			  ,@Comment = @Comment
			  ,@AccountCommentID = @AccountCommentID OUTPUT

			-- Check for errors
			IF (@@ROWCOUNT = 0 OR @AccountCommentID IS NULL)
				BEGIN
					RAISERROR('upAddAccountStatusHistory - unable to add comment', 16, 1)
					RETURN
				END
		END

	-- Insert new record into tbAccountComment
	INSERT INTO dbo.tbAccountStatusHistory (
		AccountID, 
		Status,
		StartDateTime,
		InitiatorGUID,
		InitiatorName,
		AccountCommentID)
	VALUES (
		@AccountID, 
		UPPER(@Status), 
		@DateTimeStamp,
		@InitiatorGUID, 
		dbo.ufGetPersonsFullName(@InitiatorGUID), 
		@AccountCommentID)

	-- Verify success
	IF (@@ROWCOUNT = 1)
		SET @Result = 'S'
	ELSE
		BEGIN
			IF (@AccountCommentID IS NOT NULL)
				EXECUTE dbo.upDeleteAccountComment
					@AccountCommentID = @AccountCommentID
					,@AccountID = @AccountID
					,@Result = @rc OUTPUT
			
			RAISERROR('upAddAccountStatusHistory - account status history was NOT successfully added', 16, 1)
		END

END
GO
GRANT EXECUTE
ON dbo.upAddAccountStatusHistory
TO system