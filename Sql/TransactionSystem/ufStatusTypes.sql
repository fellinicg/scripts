USE [TransactionSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: July 12, 2006
-- Description:	Return status types
-- =============================================
CREATE FUNCTION dbo.ufStatusTypes ()
RETURNS TABLE 
AS
RETURN 
(
	SELECT 'O' AS Type, 'Open' AS Description
	UNION
	SELECT 'C', 'Closed'
	UNION
	SELECT 'H', 'Hold'
)

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GRANT SELECT
ON dbo.ufStatusTypes
TO system