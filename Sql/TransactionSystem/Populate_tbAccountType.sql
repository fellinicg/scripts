-- Populate initial account types
USE TransactionSystem
INSERT INTO dbo.tbAccountType (AccountType, Description)
VALUES ('P', 'Points')

INSERT INTO dbo.tbAccountType (AccountType, Description)
VALUES ('C', 'Cash')

INSERT INTO dbo.tbAccountType (AccountType, Description)
VALUES ('D', 'Debit Card')
