-- Populate initial transaction types
USE [TransactionSystem]

INSERT INTO dbo.tbTranType (TranType, Description)
VALUES ('A', 'Award')

INSERT INTO dbo.tbTranType (TranType, Description)
VALUES ('R', 'Redemption')

INSERT INTO dbo.tbTranType (TranType, Description)
VALUES ('F', 'Refund')

INSERT INTO dbo.tbTranType (TranType, Description)
VALUES ('J', 'Adjustment')

INSERT INTO dbo.tbTranType (TranType, Description)
VALUES ('P', 'Purchase')

INSERT INTO dbo.tbTranType (TranType, Description)
VALUES ('E', 'Expiration')

INSERT INTO dbo.tbTranType (TranType, Description)
VALUES ('M', 'Acctount Maintenance')

INSERT INTO dbo.tbTranType (TranType, Description)
VALUES ('T', 'Transfer')

INSERT INTO dbo.tbTranType (TranType, Description)
VALUES ('C', 'Check Issue')
