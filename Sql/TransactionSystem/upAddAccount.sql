USE [TransactionSystem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: July 7, 2006
-- Description:	Add Account
-- =============================================
ALTER PROCEDURE dbo.upAddAccount 
	@AccountType char(1),  -- (P)oints, (C)ash, or (D)ebit Card
	@OwnerGUID int,
	@AccountDesc varchar(40),
	@ClientID int,
	@ClientTranPeriodID int,
	@Status char(1),  -- (O)pen, (C)losed, or (H)old,
	@InitiatorGUID int,
	@Comment varchar(4000) = '',
	@AccountID int OUTPUT
AS
BEGIN

	-- Local variable declarations
	DECLARE @DateTimeStamp datetime
	DECLARE @Result char(1)

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @DateTimeStamp = CURRENT_TIMESTAMP
	SET @Result = 'F'

	-- ***** Check Inputs *****
	-- 
	-- Validate @AccountType
	IF (NOT EXISTS(SELECT 1 FROM dbo.tbAccountType WHERE AccountType = @AccountType))
	BEGIN
		RAISERROR('upAddAccount - @AccountType supplied is not a valid account type on the system', 16, 1)
		RETURN
	END

	-- Validate @ClientID
	IF (NOT EXISTS(SELECT 1 FROM SQL2KPROD.Catalog.dbo.Clients WHERE cl_ID = @ClientID))
	BEGIN
		RAISERROR('upAddAccount - @ClientID supplied is not a valid client id on the system', 16, 1)
		RETURN
	END

	-- Validate @OwnerGUID. Make sure the GlobalUser is a valid user for the supplied client
	IF (NOT EXISTS(SELECT 1 FROM SQL2KPROD.GlobalUsers.dbo.tbUserAccount WHERE UserAccountID = @OwnerGUID AND ClientID = @ClientID))
	BEGIN
		RAISERROR('upAddAccount - @OwnerGUID supplied is not a valid user account id on the system', 16, 1)
		RETURN
	END

	-- Validate @ClientTranPeriodID. Make sure the ClientTranPeriodID is a valid ID for the supplied client
	IF (NOT EXISTS(SELECT 1 FROM dbo.tbClientTranPeriod WHERE ClientTranPeriodID = @ClientTranPeriodID AND ClientID = @ClientID))
	BEGIN
		RAISERROR('upAddAccount - @ClientTranPeriodID supplied is not a valid client transaction period id on the system', 16, 1)
		RETURN
	END

	-- Validate @Status
	IF (NOT EXISTS(SELECT 1 FROM dbo.ufStatusTypes() WHERE Type = @Status))
	BEGIN
		RAISERROR('upAddAccount - @Status supplied is not a valid status on the system', 16, 1)
		RETURN
	END

	-- Insert new record into tbUserAccount
	INSERT INTO dbo.tbAccount (
		AccountType, 
		OwnerGUID, 
		AccountDesc, 
		ClientID, 
		ClientTranPeriodID, 
		BalanceTranID,
		BalanceAmt, 
		Status, 
		StatusDateTime,
		RecordDateTime)
	VALUES (
		UPPER(@AccountType),
		@OwnerGUID, 
		@AccountDesc, 
		@ClientID, 
		@ClientTranPeriodID, 
		0, 
		0, 
		UPPER(@Status), 
		@DateTimeStamp,
		@DateTimeStamp)

	-- Verify success
	IF (@@ROWCOUNT = 1 AND @@IDENTITY > 0)
		SET @AccountID = @@IDENTITY
	ELSE
		RAISERROR('upAddAccount - user account was NOT successfully added', 16, 1)

	-- Add account status history record
	EXECUTE dbo.upAddAccountStatusHistory 
	   @AccountID = @AccountID
	  ,@Status = @Status
	  ,@InitiatorGUID = @InitiatorGUID
	  ,@Comment = @Comment
	  ,@Result = @Result OUTPUT

	-- Verify success
	IF (@Result <> 'S')
		BEGIN
			SET @AccountID = null
			EXECUTE TransactionSystem.dbo.upDeleteAccount 
				@AccountID = @AccountID
				,@OwnerGUID = @OwnerGUID
				,@Result = @Result OUTPUT
			RAISERROR('upAddAccount - user account was NOT successfully added, status history failed', 16, 1)
		END

END
GO
GRANT EXECUTE
ON dbo.upAddAccount
TO system