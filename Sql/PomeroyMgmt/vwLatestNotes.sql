USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'dbo.vwLatestNotes'))
DROP VIEW dbo.vwLatestNotes
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100122
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100122
-- ============================================================
CREATE VIEW dbo.vwLatestNotes
AS
	select a.NoteID, a.SourceID, a.Note, isnull(dbo.sfFullName(a.UserAccountID), 'System') 'Author',
	a.NoteType, a.CreateDateTime
	from dbo.tbNote a
	inner join (select SourceID, max(NoteID) 'NoteID'
							from dbo.tbNote group by SourceID, NoteType) b on a.NoteID = b.NoteID