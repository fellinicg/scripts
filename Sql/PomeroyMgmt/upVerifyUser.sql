USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upVerifyUser') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upVerifyUser
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091112
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091112
-- ============================================================
CREATE PROCEDURE dbo.upVerifyUser
	@WindowsUser varchar(50)
	,@FirstName varchar(50)
	,@LastName varchar(80)
	,@UserAccountID int output
	,@Result char(1) output
AS
BEGIN

	-- Declarations
	declare @AccountStatus char(1)

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @AccountStatus = 'A'
	set @UserAccountID = 0
	set @Result = 'F'

	-- Check if windows user is in dbo.tbUserAccount
	select @UserAccountID = dbo.sfUserAccountID(@WindowsUser)
	if @UserAccountID = 0
		-- User didn't exist, so add
		exec dbo.upAddUserAccount @WindowsUser, @FirstName, @LastName, @AccountStatus,
			@UserAccountID OUTPUT, @Result OUTPUT
	else
		set @Result = 'S'
	
END
GO
grant exec on dbo.upVerifyUser to system