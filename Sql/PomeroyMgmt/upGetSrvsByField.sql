USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetSrvsByField') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetSrvsByField
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100528
-- ============================================================
CREATE PROCEDURE dbo.upGetSrvsByField
	@Field varchar(100)
	,@Search varchar(50)
	,@DeploymentStatusID tinyint = 0
AS
BEGIN

	-- Declarations
	declare @sql varchar(1000)
	
	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @sql = 'select s.ServerID, s.ServerName, s.OnPawanet, s.IsOnline, 
		dbo.sfNumNotes(s.ServerID, ''S'') ''NumSrvNotes'', dbo.sfFullName(sa.UserAccountID) ''SA'',
		dbo.sfFullName(dba.UserAccountID) ''DBA'', sa.UserAccountID ''sa_id'', dba.UserAccountID ''dba_id'',
		ds.DeploymentStatusID, ds.Description ''DeploymentStatus'', s.SupportType, s.OSType,
		case SupportType when ''F'' then 1 when ''D'' then 1 else 0 end ''DBSupported''
	from dbo.tbServer s
	left join dbo.tbUARoleServer sa on s.ServerID = sa.ServerID and sa.UARoleID = 2 and sa.IsPrimary = 1
	left join dbo.tbUARoleServer dba on s.ServerID = dba.ServerID and dba.UARoleID = 1 and dba.IsPrimary = 1
	left join dbo.vwSrvDeploymentStatus ds on s.ServerID = ds.ServerID
	where ' + @Field + ' like ''%' + dbo.sfReplSpcSrchChars(@Search) + '%''
		and ds.DeploymentStatusID <> ' + convert(varchar, @DeploymentStatusID) + '
		and s.IsDeleted = 0
	order by s.ServerName'
	
	-- Return data
	exec (@sql)

END
GO
grant exec on dbo.upGetSrvsByField to system