USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'dbo.vwWOCommentAudit'))
DROP VIEW dbo.vwWOCommentAudit
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100510
-- ============================================================
CREATE VIEW dbo.vwWOCommentAudit
AS
	select WOCommentAuditID, WOCommentID, WorkOrderID, WOComment, OrigAuthor, UpdatedBy, CreateDateTime
	from dbo.tbWOCommentAudit