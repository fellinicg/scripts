USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetServerRoles') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetServerRoles
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091203
-- ============================================================
CREATE PROCEDURE dbo.upGetServerRoles
	@ServerID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select sr.ServerRoleID, sr.RoleID, r.Description
	from dbo.tbServerRole sr
	inner join dbo.tbRole r on sr.RoleID = r.RoleID
	where ServerID = @ServerID
	order by r.Description

END
GO
grant exec on dbo.upGetServerRoles to system