USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.sfReplSpcSrchChars') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.sfReplSpcChars
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100601
-- ============================================================
GO
CREATE FUNCTION dbo.sfReplSpcSrchChars (@Input varchar(max))
RETURNS varchar(max)
AS
BEGIN
  
  return(replace(replace(replace(@Input, '[', '[[]'), '_', '[_]'), '%', '[%]'))

END
