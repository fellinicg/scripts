USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddUARoleServer') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddUARoleServer
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091127
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091127
-- ============================================================
CREATE PROCEDURE dbo.upAddUARoleServer
	@UARoleID int
	,@UserAccountID int
	,@ServerID int
	,@IsPrimary bit
	,@Result char(1) output	
AS
BEGIN

	-- Declarations

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Delete everyone else that has the role on this server (Temporary)
	delete from dbo.tbUARoleServer
	where UARoleID = @UARoleID and ServerID = @ServerID
	
	-- Update if user already has the role, else add
--  if exists(select 1 from dbo.tbUARoleServer where UARoleID = @UARoleID and UserAccountID = @UserAccountID and ServerID = @ServerID)
--		update dbo.tbUARoleServer set UARoleID = @UARoleID, UserAccountID = @UserAccountID, ServerID = @ServerID, IsPrimary = @IsPrimary
--		where UARoleID = @UARoleID and UserAccountID = @UserAccountID and ServerID = @ServerID
--	else
		insert into dbo.tbUARoleServer (UARoleID, UserAccountID, ServerID, IsPrimary)
		values (@UARoleID, @UserAccountID, @ServerID, @IsPrimary)
		
	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'

END
GO
grant exec on dbo.upAddUARoleServer to system