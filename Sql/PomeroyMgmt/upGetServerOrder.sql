USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetServerOrder') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetServerOrder
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091204
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091204
-- ============================================================
CREATE PROCEDURE dbo.upGetServerOrder
	@ServerID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select s.ServerName, convert(varchar, o.OrderDate, 101) 'OrderDate', o.OrderNumber, 
		dbo.sfNumNotes(@ServerID, 'O') 'NumNotes'
	from dbo.tbServer s
		left join dbo.tbServerOrder o on s.ServerID = o.ServerID
	where s.ServerID = @ServerID

END
GO
grant exec on dbo.upGetServerOrder to system