USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.sfNumNotes') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.sfNumNotes
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091117
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091117
-- ============================================================
CREATE FUNCTION dbo.sfNumNotes (@SourceID int, @NoteType char(1))
	RETURNS int AS
BEGIN
	
	-- Declarations
	declare @Ret int

	-- Initialize variables
	set @Ret = 0

	-- Get data
	select @Ret = count(NoteID) 
	from dbo.tbNote 
	where SourceID = @SourceID and NoteType = @NoteType

	return(@Ret)

END
