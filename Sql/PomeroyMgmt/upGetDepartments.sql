USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetDepartments') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetDepartments
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091202
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091202
-- ============================================================
CREATE PROCEDURE dbo.upGetDepartments
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select DepartmentID, Description
	from dbo.tbDepartment
	where IsDeleted = 0
	order by Description

END
GO
grant exec on dbo.upGetDepartments to system