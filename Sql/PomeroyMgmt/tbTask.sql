USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbTask') AND type in (N'U'))
DROP TABLE dbo.tbTask
GO
CREATE TABLE dbo.tbTask(
	TaskID int identity(1,1) not null,
	Description varchar(50) not null,
	IsDeleted bit constraint dftbTaskIsDeleted default 0,
	StoredProc] varchar(50) NULL,
	JSFunc varchar(50) NULL,
	CreateDateTime datetime not null constraint dftbTaskCreateDateTime default (getdate()),
	UpdateDateTime datetime not null constraint dftbTaskUpdateDateTime default (getdate()),
CONSTRAINT PK_tbTask PRIMARY KEY CLUSTERED 
(
	TaskID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
