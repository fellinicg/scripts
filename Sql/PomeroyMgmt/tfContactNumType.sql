USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tfContactNumType') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.tfContactNumType
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091214
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091214
-- ============================================================
CREATE FUNCTION dbo.tfContactNumType ()
RETURNS @ResultTable table (ContactNumType char(2),
														Description varchar(20))
AS
BEGIN

	insert into @ResultTable
	select 'HP', 'Home'
	union
	select 'MP', 'Mobile'
	union
	select 'WP', 'Work'

	return

END