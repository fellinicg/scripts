USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbDBAppNote') AND type in (N'U'))
DROP TABLE dbo.tbDBAppNote
GO
CREATE TABLE dbo.tbDBAppNote(
	DBAppNoteID int identity(1,1) not null,
	DBApplicationID int not null,
	Note varchar(200) not null,
	Author varchar(20) not null,
	IsDeleted bit not null constraint dftbDBAppNoteIsDeleted default 0,
	CreateDateTime datetime not null constraint dftbDBAppNoteCreateDateTime default (getdate()),
 CONSTRAINT PK_tbDBAppNote PRIMARY KEY CLUSTERED 
(
	DBAppNoteID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF