USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddServerTask') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddServerTask
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091119
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091119
-- ============================================================
CREATE PROCEDURE dbo.upAddServerTask
	@ServerIDList	varchar(4000)
	,@TaskID int
	,@Result char(1) = 'F' output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Insert new record into tbServerTask
	insert into dbo.tbServerTask (ServerID, TaskID)
	select ListItem, @TaskID from dbo.tfCommaListToTable(@ServerIDList)
	left join dbo.tbServerTask on ListItem = ServerID and @TaskID = TaskID
	where ServerTaskID is null

	-- Verify success
	if (@@error = 0)
		set @Result = 'S'
	else
		raiserror('upAddServerTask - ServerTask was NOT successfully added', 16, 1)

END
GO
grant exec on dbo.upAddServerTask to system