USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetWorkOrder') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetWorkOrder
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100125
-- ============================================================
CREATE PROCEDURE dbo.upGetWorkOrder
	@WorkOrderID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return Work Order
	select
		w.WorkOrderID
		,w.Title
		,w.Description
		,w.DepartmentID
		,w.ServiceOrder
		,w.ClientProject
		,convert(varchar, w.ReceivedDate, 101) 'ReceivedDate'
		,convert(varchar, w.DesiredCompletionDate, 101) 'DesiredCompletionDate'
		,convert(varchar, w.CompletionDate, 101) 'CompletionDate'
		,convert(varchar, w.RequestDate, 101) 'RequestDate'
		,w.IsBillable
		,dbo.sfWOAssignedTo(w.WorkOrderID, ',') 'AssignedTo'
		,dbo.sfWORequestor(w.WorkOrderID, ',') 'Requestor'
		,s.WOStatusCode 'WOStatusCode'
		,s.Description 'WOStatus'
		,w.EstCost
		,w.FixedCost
	from dbo.tbWorkOrder w
		left join dbo.vwWorkOrderStatus s on w.WorkOrderID = s.WorkOrderID
	where w.WorkOrderID = @WorkOrderID

END
GO
grant exec on dbo.upGetWorkOrder to system