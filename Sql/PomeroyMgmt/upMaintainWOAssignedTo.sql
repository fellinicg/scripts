USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upMaintainWOAssignedTo') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upMaintainWOAssignedTo
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100128
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100128
-- ============================================================
CREATE PROCEDURE dbo.upMaintainWOAssignedTo
	@WorkOrderID int
	,@UserAccounts varchar(1000)
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Begin transaction
	begin tran
	
	begin try
		-- Undelete
		update dbo.tbWorkOrderAssignedTo set IsDeleted = 0
		from dbo.tbWorkOrderAssignedTo
		inner join dbo.tfCommaListToTable(@UserAccounts) on UserAccountID = ListItem
		where WorkOrderID = @WorkOrderID and IsDeleted = 1
	
		-- Delete
		update dbo.tbWorkOrderAssignedTo set IsDeleted = 1
		from dbo.tbWorkOrderAssignedTo
		left join dbo.tfCommaListToTable(@UserAccounts) on UserAccountID = ListItem
		where WorkOrderID = @WorkOrderID and IsDeleted = 0 and ListItem is null
	
		-- Add
		insert into dbo.tbWorkOrderAssignedTo (WorkOrderID, UserAccountID)
		select @WorkOrderID, ListItem
		from dbo.tfCommaListToTable(@UserAccounts)
		left join dbo.tbWorkOrderAssignedTo on ListItem = UserAccountID and WorkOrderID = @WorkOrderID
		where WorkOrderAssignedToID is null
	
		commit tran
		set @Result = 'S'
	end try
	begin catch
		rollback tran
		raiserror('upMaintainWOAssignedTo - work order assigned to was NOT successfully maintained', 16, 1)
	end catch
	
END
GO
grant exec on dbo.upMaintainWOAssignedTo to system