USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateServerHW') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateServerHW
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091209
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091209
-- ============================================================
CREATE PROCEDURE dbo.upUpdateServerHW
	@ServerID int
	,@Manufacturer varchar(50)
	,@Model varchar(50)
	,@SerialNumber varchar(60)
	,@ProcessorInfo varchar(50)
	,@Processors tinyint
	,@MemoryBytes	bigint
	,@IpAddress varchar(1000)
	,@ServerType char(1)
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Record ip address(es)
	delete from dbo.tbServerIP where ServerID = @ServerID
	insert into dbo.tbServerIP
	select @ServerID, ListItem from dbo.tfCommaListToTable(@IpAddress)
	if (@@error <> 0)
	begin
		raiserror('upUpdateServerHW - unable to record ip address(es)', 16, 1)
	end
	
	-- Update server
	update dbo.tbServer set Manufacturer = @Manufacturer, Model = @Model, SerialNumber = @SerialNumber
		,ProcessorInfo = @ProcessorInfo, Processors = @Processors, MemoryBytes = @MemoryBytes
		,ServerType = isnull(@ServerType, ServerType), UpdateDateTime = current_timestamp
	where ServerID = @ServerID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upUpdateServerHW - server hardware was NOT successfully updated', 16, 1)

END
GO
grant exec on dbo.upUpdateServerHW to system