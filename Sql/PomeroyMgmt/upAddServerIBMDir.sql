USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddServerIBMDir') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddServerIBMDir
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100106
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100106
-- ============================================================
CREATE PROCEDURE dbo.upAddServerIBMDir
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Insert new record into tbServerIBMDir
	insert into dbo.tbServerIBMDir (ServerID, IBMDirectorID, IBMDirDateCreated)
	select s.ServerID, i.MANAGED_OBJ_ID, i.DATE_CREATED
	from dbo.tbServer s
	inner join openquery(teleas22, 'select MANAGED_OBJ_ID, case charindex(''('', label) 
			when 0 then rtrim(ltrim(label)) else rtrim(ltrim(left(label, charindex(''('', label)-1)))
			end as ServerName, DATE_CREATED from IBMDirector.dbo.TWG_MANAGED_OBJECT') i on s.ServerName = i.ServerName
	left join dbo.tbServerIBMDir si on s.ServerID = si.ServerID and i.MANAGED_OBJ_ID = si.IBMDirectorID
	where idx is null

END
GO
grant exec on dbo.upAddServerIBMDir to system