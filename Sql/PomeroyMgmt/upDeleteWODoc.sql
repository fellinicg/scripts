USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upDeleteWODoc') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upDeleteWODoc
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100128
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100128
-- ============================================================
CREATE PROCEDURE dbo.upDeleteWODoc
	@WorkOrderDocIDs varchar(1000)
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Delete
	delete dbo.tbWorkOrderDoc 
	from dbo.tbWorkOrderDoc 
	inner join dbo.tfCommaListToTable(@WorkOrderDocIDs) on WorkOrderDocID = ListItem
	
	-- Verify success
	if (@@error = 0)
		set @Result = 'S'
	else
		raiserror('upDeleteWODoc - work order document(s) NOT successfully deleted', 16, 1)
	
END
GO
grant exec on dbo.upDeleteWODoc to system