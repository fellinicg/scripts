USE PomeroyMgmt
GO
SET ANSI_nullS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbContactNum') AND type in (N'U'))
DROP TABLE dbo.tbContactNum
GO
CREATE TABLE dbo.tbContactNum(
	ContactNumID int identity(1,1) not null,
	UserAccountID int not null,
	ContactNumType char(2) not null,
	ContactNum varchar(20) null,
	IsDeleted bit constraint dftbContactNumIsDeleted default 0,
	CreateDateTime datetime not null constraint dftbContactNumCreateDateTime default (getdate()),
	UpdateDateTime datetime not null constraint dftbContactNumUpdateDateTime default (getdate()),
 CONSTRAINT PK_tbContactNum PRIMARY KEY CLUSTERED 
(
	ContactNumID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbContactNum with check add constraint tcContactNumType 
	check ((ContactNumType='HP' OR ContactNumType='MP' OR ContactNumType='WP'))
GO
alter table dbo.tbContactNum with check add constraint fkUserAccountID foreign key(UserAccountID)
references dbo.tbUserAccount (UserAccountID)
