USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddProductIdFromIBMDir') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddProductIdFromIBMDir
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20101018
-- ============================================================
CREATE PROCEDURE dbo.upAddProductIdFromIBMDir
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Update
	update dbo.tbServer set OSProductID = dbo.sfTrim(i.OP_SYS_PRODUCT_ID)
	from dbo.tbServer s
	inner join dbo.vwServerIBMDir si on s.ServerID = si.ServerID
	inner join openquery(teleas22, 'select MANAGED_OBJ_ID, OP_SYS_PRODUCT_ID
											from IBMDirector.dbo.TWG_OPERATING_SYSTEM') i on si.IBMDirectorID = i.MANAGED_OBJ_ID
	where dbo.sfEmptyToNull(s.OSProductID) is null
		and dbo.sfEmptyToNull(i.OP_SYS_PRODUCT_ID) is not null

END
GO
grant exec on dbo.upAddProductIdFromIBMDir to system