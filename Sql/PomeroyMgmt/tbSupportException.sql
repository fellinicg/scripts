USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbSupportException') AND type in (N'U'))
DROP TABLE dbo.tbSupportException
GO
CREATE TABLE dbo.tbSupportException(
	ServerID int not null,
	SupportType char(1) not null,
	SupportBeginDate datetime not null,
	SupportEndDate datetime not null constraint dftbSupportExcSupportEndDate default ('12/31/9999'),
	CreateDateTime datetime not null constraint dftbSupportExcCreateDateTime default (getdate()),
	UpdateDateTime datetime not null constraint dftbSupportExcUpdateDateTime default (getdate()),
) 

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbSupportException with check 
add constraint tcSupportType check ((SupportType='H' OR SupportType='D'))
