USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upRptWorkOrders') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upRptWorkOrders
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100514
-- ============================================================
CREATE PROCEDURE dbo.upRptWorkOrders
	@WOStatusCode char(1)
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	if len(dbo.sfTrim(@WOStatusCode)) = 0 set @WOStatusCode = '%'

	-- Return a list of WorkOrders
	select
		WorkOrder, 
		Title, 
		Description, 
		Department, 
		ServiceOrder, 
		ClientProject, 
		ReceivedDate, 
		DesiredCompletionDate, 
		CompletionDate, 
		RequestDate, 
		IsBillable, 
		EstCost, 
		FixedCost, 
		AssignedTo, 
		Requestor, 
		replace(Doc, '|', '\r\n') 'Doc', 
		CommentDateStamp + ' - ' + WOComment 'LatestComment', 
		WoStatus, 
		dbo.sfWOComment(WorkOrderID, ';') 'Comments'
	from dbo.vwWorkOrder
	where WOStatusCode like (@WOStatusCode)
	order by WorkOrderID desc

END
GO
grant exec on dbo.upRptWorkOrders to system