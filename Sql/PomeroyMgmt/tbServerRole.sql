USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbServerRole') AND type in (N'U'))
DROP TABLE dbo.tbServerRole
GO
CREATE TABLE dbo.tbServerRole(
	ServerRoleID int identity(1,1) not null,
	ServerID int not null,
	RoleID int not null,
 CONSTRAINT PK_tbServerRole PRIMARY KEY CLUSTERED 
(
	ServerRoleID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbServerRole with check add constraint fktbServerRoleServerID
foreign key(ServerID) references dbo.tbServer (ServerID)
GO
alter table dbo.tbServerRole with check add constraint fktbServerRoleRoleID
foreign key(RoleID) references dbo.tbRole (RoleID)