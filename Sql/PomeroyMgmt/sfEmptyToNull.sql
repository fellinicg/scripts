USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.sfEmptyToNull') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.sfEmptyToNull
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100204
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100204
-- ============================================================
GO
CREATE FUNCTION dbo.sfEmptyToNull (@Input varchar(4000))
RETURNS varchar(4000)
AS
BEGIN
  
  return(case when len(ltrim(rtrim(@Input))) = 0 then null else ltrim(rtrim(@Input)) end)

END