USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbTimeEntry') AND type in (N'U'))
DROP TABLE dbo.tbTimeEntry
GO
CREATE TABLE dbo.tbTimeEntry(
	TimeEntryID int identity(1,1) not null,
	UserAccountID int not null,
	WeekID smallint not null,
	TimeTypeID tinyint,
	Info varchar (500),
	Hours float not null,
	StartTime varchar(10) null,
	EndTime varchar(10) null,
	IsDeleted bit constraint dftbTimeEntryIsDeleted default 0,
	CreateDateTime datetime not null constraint dftbTimeEntryCreateDateTime default (getdate()),
	UpdateDateTime datetime not null constraint dftbTimeEntryUpdateDateTime default (getdate()),
 CONSTRAINT PK_tbTimeEntry PRIMARY KEY CLUSTERED 
(
	TimeEntryID ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbTimeEntry with check add constraint fktbTimeEntryUserAccountID
foreign key(UserAccountID) references dbo.tbUserAccount (UserAccountID)
GO
alter table dbo.tbTimeEntry with check add constraint fktbTimeEntryWeekID
foreign key(WeekID) references dbo.tbWeek (WeekID)
GO
alter table dbo.tbTimeEntry with check add constraint fktbTimeEntryTimeTypeID
foreign key(TimeTypeID) references dbo.tbTimeType (TimeTypeID)
