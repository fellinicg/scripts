USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbServerIBMDir') AND type in (N'U'))
DROP TABLE dbo.tbServerIBMDir
GO
CREATE TABLE dbo.tbServerIBMDir(
	Idx int identity (1,1) not null,
	ServerID int not null,
	IBMDirectorID int not null,
	UpdFromIBMDir bit not null constraint dftbSrvUpdFromIBMDir default 0,
	IBMDirDateCreated datetime not null,
 CONSTRAINT PK_tbServerIBMDir PRIMARY KEY CLUSTERED 
(
	ServerID ASC, IBMDirectorID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF