USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetUATask') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetUATask
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091217
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091217
-- ============================================================
CREATE PROCEDURE dbo.upGetUATask
	@UATaskID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select UATaskID, UserAccountID, TaskID, LogFile, TaskStatus, CreateDateTime
	from dbo.tbUATask
	where UATaskID = @UATaskID

END
