USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetSupportTypes') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetSupportTypes
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091224
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091224
-- ============================================================
CREATE PROCEDURE dbo.upGetSupportTypes
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select SupportType, Description
	from dbo.tbSupportType
	where IsDeleted = 0
	order by Description

END
GO
grant exec on dbo.upGetSupportTypes to system