USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateUATaskStatus') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateUATaskStatus
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091223
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091223
-- ============================================================
CREATE PROCEDURE dbo.upUpdateUATaskStatus
	@UATaskID int
	,@TaskStatus char(1)
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Update record
	update dbo.tbUATask set TaskStatus = @TaskStatus
	where UATaskID = @UATaskID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upUpdateUATaskStatus - Task status was NOT successfully updated', 16, 1)

END
GO
grant exec on dbo.upUpdateUATaskStatus to system