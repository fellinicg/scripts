USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbNote') AND type in (N'U'))
DROP TABLE dbo.tbNote
GO
CREATE TABLE dbo.tbNote(
	NoteID int identity(1,1) not null,
	SourceID int not null,
	NoteType char(1) not null,
	Note varchar(1000) not null,
	UserAccountID int not null,
	IsDeleted bit not null constraint dftbNoteIsDeleted default 0,
	CreateDateTime datetime not null constraint dftbNoteCreateDateTime default (getdate()),
 CONSTRAINT PK_tbNote PRIMARY KEY CLUSTERED 
(
	NoteID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
--alter table dbo.tbNote with check add constraint tcNoteType 
--	check ((NoteType='D' OR NoteType='O' OR NoteType='S' OR NoteType='W'))
