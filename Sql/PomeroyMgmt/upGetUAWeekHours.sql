USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetUAWeekHours') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetUAWeekHours
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20121126
-- ============================================================
CREATE PROCEDURE dbo.upGetUAWeekHours
	@UserAccountID int
	,@WeekID smallint
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select sum(t.Hours) 'TotalHours'
	from dbo.tbTimeEntry t inner join dbo.tbTimeType tt on t.TimeTypeID = tt.TimeTypeID
	where t.UserAccountID = @UserAccountID and t.WeekID = @WeekID and t.IsDeleted = 0
	
END
GO
grant exec on dbo.upGetUAWeekHours to system