USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upSetOnPawanet') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upSetOnPawanet
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091203
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091203
-- ============================================================
CREATE PROCEDURE dbo.upSetOnPawanet
	@ServerID int
	,@OnPawanet bit
	,@Result char(1) output	
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Delete user role
	update dbo.tbServer set OnPawanet = @OnPawanet
	where ServerID = @ServerID
	
	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'

END
GO
grant exec on dbo.upSetOnPawanet to system