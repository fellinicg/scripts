USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetTasks') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetTasks
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091217
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091217
-- ============================================================
CREATE PROCEDURE dbo.upGetTasks
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select TaskID, Description, StoredProc, JSFunc, Type
	from dbo.tbTask
	where Type = 'U' and IsDeleted = 0
	order by Description

END
GO
grant exec on dbo.upGetTasks to system