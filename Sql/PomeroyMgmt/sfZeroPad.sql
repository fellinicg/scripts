USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.sfZeroPad') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.sfZeroPad
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091117
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091117
-- ============================================================
CREATE FUNCTION dbo.sfZeroPad (@Value varchar(10), @PadCount tinyint)
	RETURNS varchar(10) AS
BEGIN
	return(replicate('0', @PadCount - len(@Value)) + @Value)
END