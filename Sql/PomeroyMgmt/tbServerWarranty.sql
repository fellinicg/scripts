USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbServerWarranty') AND type in (N'U'))
DROP TABLE dbo.tbServerWarranty
GO
CREATE TABLE dbo.tbServerWarranty(
	ServerID int not null,
	ExpirationDate datetime null,
	Provider varchar(20) null,
	CreateDateTime datetime not null constraint dftbServerWarrantyCreateDateTime default (getdate()),
	UpdateDateTime datetime not null constraint dftbServerWarrantyUpdateDateTime default (getdate()),
 CONSTRAINT PK_tbServerWarranty PRIMARY KEY CLUSTERED 
(
	ServerID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF