USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddProcessorsFromIBMDir') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddProcessorsFromIBMDir
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100526
-- ============================================================
CREATE PROCEDURE dbo.upAddProcessorsFromIBMDir
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Update
	update dbo.tbServer set Processors = i.Processors
	from dbo.vwServerIBMDir v
	inner join dbo.tbServer s on v.ServerID = s.ServerID
	inner join (select MANAGED_OBJ_ID, count(MANAGED_OBJ_ID) 'Processors' 
							from teleas22.IBMDirector.dbo.TWG_PROCESSOR
							group by MANAGED_OBJ_ID) i on v.IBMDirectorID = i.MANAGED_OBJ_ID
	where s.Processors is null

END
GO
grant exec on dbo.upAddProcessorsFromIBMDir to system