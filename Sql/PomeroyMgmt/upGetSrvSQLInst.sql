USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetSrvSQLInst') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetSrvSQLInst
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100803
-- ============================================================
CREATE PROCEDURE dbo.upGetSrvSQLInst
	@ServerID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select si.SQLInstanceID, isnull(si.SQLInstanceName, s.ServerName) 'SQLInstanceName'
	from dbo.tbServer s
		left join dbo.tbSQLInstance si on s.ServerID = si.ServerID
	where s.ServerID = @ServerID
	order by SQLInstanceName

END
GO
grant exec on dbo.upGetSrvSQLInst to system