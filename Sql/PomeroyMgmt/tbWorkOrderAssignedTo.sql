USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbWorkOrderAssignedTo') AND type in (N'U'))
DROP TABLE dbo.tbWorkOrderAssignedTo
GO
CREATE TABLE dbo.tbWorkOrderAssignedTo(
	WorkOrderAssignedToID int identity(1,1) not null,
	WorkOrderID int not null,
	UserAccountID int not null,
	IsDeleted bit not null constraint dftbWorkOrderAssignedToIsDeleted default (0),
	CreateDateTime datetime not null constraint dftbWorkOrderAssignedToCreateDateTime default (getdate()),
 CONSTRAINT PK_tbWorkOrderAssignedTo PRIMARY KEY CLUSTERED 
(
	WorkOrderAssignedToID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbWorkOrderAssignedTo]  WITH CHECK ADD  CONSTRAINT [fktbWorkOrderAssignedToUserAccountID] FOREIGN KEY([UserAccountID])
REFERENCES [dbo].[tbUserAccount] ([UserAccountID])
GO
ALTER TABLE [dbo].[tbWorkOrderAssignedTo]  WITH CHECK ADD  CONSTRAINT [fktbWorkOrderAssignedToWorkOrderID] FOREIGN KEY([WorkOrderID])
REFERENCES [dbo].[tbWorkOrder] ([WorkOrderID])