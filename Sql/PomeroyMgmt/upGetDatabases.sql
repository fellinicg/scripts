USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetDatabases') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetDatabases
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091127
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091127
-- ============================================================
CREATE PROCEDURE dbo.upGetDatabases
	@ServerID int
	,@SQLInstanceID int = null
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select db.DatabaseID, db.DBName, dp.DepartmentID, dp.Description 'Department', c.ClientContactID,
		c.FirstName + ' ' + c.LastName 'ClientContact', dbo.sfFullName(urs.UserAccountID) 'DBA', 
		a.ApplicationID, a.Description 'Application', db.IsOnline, db.IsSupported, 
		dbo.sfNumNotes(db.DatabaseID, 'D') 'NumDBNotes'
	from dbo.tbDatabase db
		left join dbo.tbDepartment dp on db.DepartmentID = dp.DepartmentID
		left join dbo.tbClientContact c on db.ClientContactID = c.ClientContactID
		left join dbo.tbUARoleServer urs on db.ServerID = urs.ServerID and urs.UARoleID = 1 and urs.IsPrimary = 1
		left join dbo.tbDBApplication dba on db.DatabaseID = dba.DatabaseID and dba.IsDeleted = 0
		left join dbo.tbApplication a on dba.ApplicationID = a.ApplicationID and a.IsDeleted = 0
	where db.ServerID = @ServerID and db.IsDeleted = 0 and isnull(db.SQLInstanceID, 0) = isnull(@SQLInstanceID, 0)
	order by db.DBName

END
GO
grant exec on dbo.upGetDatabases to system