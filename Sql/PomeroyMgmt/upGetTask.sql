USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetTask') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetTask
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091221
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091221
-- ============================================================
CREATE PROCEDURE dbo.upGetTask
	@TaskID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select TaskID, Description, StoredProc, JSFunc, Type
	from dbo.tbTask
	where TaskID = @TaskID

END
