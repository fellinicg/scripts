USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetDbSrvInfo') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetDbSrvInfo
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091203
-- ============================================================
CREATE PROCEDURE dbo.upGetDbSrvInfo
	@ServerID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select si.SQLInstanceID, isnull(si.SQLInstanceName, s.ServerName) 'SqlInstance', 
		dp.Description + ' ' + isnull(dp.Edition, '') + ' ' + isnull(dp.ProductLevel, '') 'SqlVer',
		dbo.sfFullName(urs.UserAccountID) 'DBA'
	from dbo.tbServer s
		left join dbo.tbSQLInstance si on s.ServerID = si.ServerID
		left join dbo.tbServerDBPlatform sd on s.ServerID = sd.ServerID and isnull(si.SQLInstanceID, 0) = isnull(sd.SQLInstanceID, 0)
		left join dbo.tbDBPlatform dp on sd.DBPlatformID = dp.DBPlatformID
		left join dbo.tbUARoleServer urs on s.ServerID = urs.ServerID and urs.UARoleID = 1 and urs.IsPrimary = 1
	where s.ServerID = @ServerID
	order by SqlInstance

END
GO
grant exec on dbo.upGetDbSrvInfo to system