USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbWorkOrderDoc') AND type in (N'U'))
DROP TABLE dbo.tbWorkOrderDoc
GO
CREATE TABLE dbo.tbWorkOrderDoc(
	WorkOrderDocID int identity(1,1) not null,
	WorkOrderID int not null,
	WorkOrderDoc varchar(200) not null,
	CreateDateTime datetime not null constraint dftbWorkOrderDocCreateDateTime default (getdate()),
 CONSTRAINT PK_tbWorkOrderDoc PRIMARY KEY CLUSTERED 
(
	WorkOrderDocID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbWorkOrderDoc]  WITH CHECK ADD  CONSTRAINT [fktbWorkOrderDocWorkOrderID] FOREIGN KEY([WorkOrderID])
REFERENCES [dbo].[tbWorkOrder] ([WorkOrderID])