USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetServerTypeList') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetServerTypeList
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100113
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100113
-- ============================================================
CREATE PROCEDURE dbo.upGetServerTypeList
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select Description
	from dbo.vwServerType
	order by Description

END
GO
grant exec on dbo.upGetServerTypeList to system