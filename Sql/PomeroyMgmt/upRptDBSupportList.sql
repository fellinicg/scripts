USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upRptDBSupportList') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upRptDBSupportList
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100105
-- ============================================================
CREATE PROCEDURE dbo.upRptDBSupportList
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		isnull(si.Cluster, isnull(si.SQLInstanceName, s.ServerName)) 'ServerName'
		,db.DBName
		,d.Description 'Department'
		,a.Description 'Application'
		,convert(varchar, s.SupportBeginDate, 101) 'SupportBeginDate'
		,c.FirstName + ' ' + LastName 'ClientContact'
		,isnull(dp.Description, '') + ' ' + isnull(dp.Edition, '') + ' ' + isnull(dp.ProductLevel, '') 'DBVersion'
		,s.Model
		,s.OS
		,s.Processors
		,dbo.sfFullName(dba.UserAccountID) 'DBA'
		,case db.IsOnline when 1 then 'Online' else 'Offline' end 'IsOnline'
		,case s.OnPawanet when 1 then 'Yes' else 'No' end 'OnPawanet'
	from dbo.tbDatabase db
		left join dbo.vwServerInfo s on db.ServerID = s.ServerID
		left join dbo.tbSQLInstance si on db.ServerID = si.ServerID and db.SQLInstanceID = si.SQLInstanceID
		left join dbo.tbDepartment d on db.DepartmentID = d.DepartmentID
		left join dbo.tbDBApplication da on db.DatabaseID = da.DatabaseID and da.IsDeleted = 0
		left join dbo.tbApplication a on da.ApplicationID = a.ApplicationID
		left join dbo.tbClientContact c on db.ClientContactID = c.ClientContactID
		left join dbo.tbServerDBPlatform sdp on s.ServerID = sdp.ServerID and isnull(si.SQLInstanceID, 0) = isnull(sdp.SQLInstanceID, 0)
		left join dbo.tbDBPlatform dp on sdp.DBPlatformID = dp.DBPlatformID
		left join dbo.tbUARoleServer dba on s.ServerID = dba.ServerID and dba.UARoleID = 1 and dba.IsPrimary = 1
	where db.IsSupported = 1 and db.IsDeleted = 0 and s.DeploymentStatusID <> 3 and isnull(si.IsOnline, 1) = 1
	order by ServerName, db.DBName
	
END
GO
grant exec on dbo.upRptDBSupportList to system