USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateWODates') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateWODates
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100127
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100127
-- ============================================================
CREATE PROCEDURE dbo.upUpdateWODates
	@WorkOrderID int
	,@DesiredCompletionDate smalldatetime
	,@CompletionDate smalldatetime
	,@RequestDate smalldatetime
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Update Work Order Dates
	update dbo.tbWorkOrder set DesiredCompletionDate = @DesiredCompletionDate, CompletionDate = @CompletionDate, 
		RequestDate = @RequestDate, UpdateDateTime = current_timestamp
	where WorkOrderID = @WorkOrderID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upUpdateWODates - work order dates were NOT successfully updated', 16, 1)

END
GO
grant exec on dbo.upUpdateWODates to system