USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateWOComment') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateWOComment
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100506
-- ============================================================
CREATE PROCEDURE dbo.upUpdateWOComment
	@WOCommentID int
	,@WOComment varchar(3000)
	,@UserAccountID int
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Update record
  update dbo.tbWOComment set WOComment = @WOComment, UserAccountID = @UserAccountID, 
		UpdateDateTime = current_timestamp
  where WOCommentID = @WOCommentID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upUpdateWOComment - Comment was NOT successfully Updated.', 16, 1)

END
GO
grant exec on dbo.upUpdateWOComment to system