USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'dbo.vwTaskStatus'))
DROP VIEW dbo.vwTaskStatus
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091218
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091218
-- ============================================================
CREATE VIEW dbo.vwTaskStatus
AS
	select 'P' 'StatusCode', 'Pending' 'Status'
	union
	select 'R', 'Running'
	union
	select 'S', 'Completed Successfully'
	union
	select 'F', 'Failed'
	union
	select 'X', 'Completed with Errors'