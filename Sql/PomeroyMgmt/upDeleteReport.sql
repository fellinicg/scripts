USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upDeleteReport') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upDeleteReport
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100104
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100104
-- ============================================================
CREATE PROCEDURE dbo.upDeleteReport
	@ReportID int output
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Delete record
  update dbo.tbReport set IsDeleted = 1
  where ReportID = @ReportID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upDeleteReport - Report was NOT successfully deleted.', 16, 1)

END
GO
grant exec on dbo.upDeleteReport to system