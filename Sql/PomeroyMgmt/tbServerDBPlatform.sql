USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbServerDBPlatform') AND type in (N'U'))
DROP TABLE dbo.tbServerDBPlatform
GO
CREATE TABLE dbo.tbServerDBPlatform(
	ServerDBPlatformID int identity(1,1) not null,
	ServerID int not null,
	DBPlatformID tinyint not null,
CONSTRAINT PK_tbServerDBPlatform PRIMARY KEY CLUSTERED 
(
	ServerDBPlatformID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbServerDBPlatform with check add constraint fktbServerDBPlatformServerID
foreign key(ServerID) references dbo.tbServer (ServerID)
GO
alter table dbo.tbServerDBPlatform with check add constraint fktbServerDBPlatformSQLInstanceID
foreign key(SQLInstanceID) references dbo.tbSQLInstance (SQLInstanceID)
GO
alter table dbo.tbServerDBPlatform with check add constraint fktbServerDBPlatformDBPlatformID
foreign key(DBPlatformID) references dbo.tbDBPlatform (DBPlatformID)