USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'dbo.vwServerType'))
DROP VIEW dbo.vwServerType
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100113
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100113
-- ============================================================
CREATE VIEW dbo.vwServerType
AS
	select 'P' 'ServerType', 'Physical' 'Description'
	union
	select 'V', 'Virtual'