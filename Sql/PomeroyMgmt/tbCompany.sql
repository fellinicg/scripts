USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbCompany') AND type in (N'U'))
DROP TABLE dbo.tbCompany
GO
CREATE TABLE dbo.tbCompany(
	CompanyID tinyint identity(1,1) not null,
	CompanyName varchar(50) not null,
	IsDeleted bit constraint dftbCompanyIsDeleted default 0,
 CONSTRAINT PK_tbCompany PRIMARY KEY CLUSTERED 
(
	CompanyID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
