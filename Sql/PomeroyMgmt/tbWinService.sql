USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbWinService') AND type in (N'U'))
DROP TABLE dbo.tbWinService
GO
CREATE TABLE dbo.tbWinService(
	WinServiceID tinyint identity(1,1) not null,
	DisplayName varchar(100) not null,
	Exe varchar(50) not null,
 CONSTRAINT PK_tbWinService PRIMARY KEY CLUSTERED 
(
	WinServiceID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF