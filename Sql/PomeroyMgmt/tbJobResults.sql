USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbJobResults') AND type in (N'U'))
DROP TABLE dbo.tbJobResults
GO
CREATE TABLE dbo.tbJobResults(
	JobResultsID int identity(1,1) not null,
	ServerID int not null,
	SQLInstanceID int null,
	Job nvarchar(128) not null,
	Message nvarchar(1024) null,
	Status int not null,
	RunDateTime datetime not null,
 CONSTRAINT PK_tbJobResults PRIMARY KEY CLUSTERED 
(
	JobResultsID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbJobResults with check add constraint fktbJobResultsServerID foreign key(ServerID)
references dbo.tbServer (ServerID)
