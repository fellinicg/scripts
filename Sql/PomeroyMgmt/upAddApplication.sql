USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddApplication') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddApplication
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091229
-- ============================================================
CREATE PROCEDURE dbo.upAddApplication
	@Description varchar(100)
	,@ApplicationID int output
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	select @ApplicationID = ApplicationID from dbo.tbApplication where Description = @Description 

	if (@ApplicationID is not null)
		-- If exists, undelete
		update dbo.tbApplication set IsDeleted = 0 where ApplicationID = @ApplicationID
	else
		-- Add record
		insert into dbo.tbApplication (Description)
		values (@Description)

	-- Verify success
	if (@@rowcount = 1)
	begin
		set @Result = 'S'
		if (@ApplicationID is null) set @ApplicationID = @@identity
	end
	else
		raiserror('upAddApplication - Application was NOT successfully added.', 16, 1)

END
GO
grant exec on dbo.upAddApplication to system