USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddDBPlatforms') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddDBPlatforms
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091103
-- ============================================================
CREATE PROCEDURE dbo.upAddDBPlatforms
--	@Result char(1) output
AS
BEGIN

	-- Declarations
	declare @Servers table (ServerID int, InstanceID tinyint, ServerName varchar(50))
	declare @DBPlatforms table (dbplatid int, pver varchar(20), plvl varchar(5), ed varchar(40))
	declare
		@ServerId int,
		@InstanceID int,
		@ServerName varchar(50),
		@sql varchar(1000),
		@rc char(1),
		@id int

	-- Initialize settings
	set nocount on

	-- Initialize variables
--	set @Result = 'F'
	set @sql = 'select convert(varchar, serverproperty(''''ProductVersion'''')) ver,
		convert(varchar, serverproperty(''''ProductLevel'''')) lvl, 
		convert(varchar, serverproperty(''''Edition'''')) ed'

	-- Insert a list of servers into table var
	insert into @Servers
	select ServerID, SQLInstanceID, SQLInstanceName
	from dbo.vwSrvDbList

	-- Loop over servers
	while exists(select 1 from @Servers)
		begin
			delete from @DBPlatforms
			select top 1 @ServerID = ServerID, @ServerName = ServerName, @InstanceID = InstanceID from @Servers
			-- Check if the server is a linked server, if not, create it
			if (select dbo.sfLinkedSrvExists(@ServerName)) is null
				exec dbo.upAddLinkedServer @ServerName, @rc output
			else set @rc = 'S'
			if @rc = 'S'
				begin
					-- Test connection to remote sql server
					if @@servername <> @ServerName
						exec dbo.upLinkedSrvAvail @ServerName, @rc output
					if @rc = 'S'
						begin
							if @@servername <> @ServerName
								insert into @DBPlatforms
								exec ('select DBPlatformID, ver, lvl, ed from openquery([' + @ServerName + '], ''' + @sql + ''')
									left join dbo.tbDBPlatform on ver = ProductVer and ed = Edition')
							else
								insert into @DBPlatforms
								select DBPlatformID, ver, lvl, ed 
								from (select convert(varchar, serverproperty('ProductVersion')) ver,
												convert(varchar, serverproperty('ProductLevel')) lvl, 
												convert(varchar, serverproperty('Edition')) ed) a
								left join dbo.tbDBPlatform on ver = ProductVer and ed = Edition
							if (select dbplatid from @DBPlatforms) is null
							begin
								insert into dbo.tbDBPlatform (Description, ProductVer, ProductLevel, Edition)
								select case left(pver, 1) 
										when '8' then 'SQL Server 2000' when '9' then 'SQL Server 2005'
										when '1' then 'SQL Server 2008'	else 'Unknown' end, 
									pver, plvl, ed
								from @DBPlatforms
								set @id = @@identity
							end
							else select @id = dbplatid from @DBPlatforms
							if not exists(select 1 from dbo.tbServerDBPlatform where ServerID = @ServerId and isnull(SQLInstanceID, 0) = isnull(@InstanceID, 0))
								insert into dbo.tbServerDBPlatform (ServerID, SQLInstanceID, DBPlatformID) 
								select @ServerID, @InstanceID, @id
							else
								update dbo.tbServerDBPlatform set DBPlatformID = @id
								where ServerID = @ServerID and isnull(SQLInstanceID, 0) = isnull(@InstanceID, 0)
						end
				end
			delete from @Servers where ServerName = @ServerName
		end

	-- Verify success
--	if @@rowcount > 0
--		set @Result = 'S'

END
GO
grant exec on dbo.upAddDBPlatforms to system