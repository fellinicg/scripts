USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetLatestWOComment') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetLatestWOComment
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100510
-- ============================================================
CREATE PROCEDURE dbo.upGetLatestWOComment
	@WorkOrderID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select WOComment, Author, convert(varchar, CreateDateTime, 101) 'CreateDateTime'
	from dbo.vwLatestWOComments
	where WorkOrderID = @WorkOrderID

END
GO
grant exec on dbo.upGetLatestWOComment to system