USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbDBPlatform') AND type in (N'U'))
DROP TABLE dbo.tbDBPlatform
GO
CREATE TABLE dbo.tbDBPlatform(
	DBPlatformID tinyint identity(1,1) not null,
	Description varchar(50) null,
	ProductVer varchar(20) null,
	ProductLevel varchar(5) null,
	Edition varchar(40) null,
 CONSTRAINT PK_tbDBPlatform PRIMARY KEY CLUSTERED 
(
	DBPlatformID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbDBPlatform add constraint ucProductVerEd unique (ProductVer, Edition)
