USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'dbo.vwWorkOrderStatus'))
DROP VIEW dbo.vwWorkOrderStatus
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100121
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100121
-- ============================================================
CREATE VIEW dbo.vwWorkOrderStatus
AS
	select a.WorkOrderStatusID, a.WorkOrderID, c.WOStatusCode, c.Description, a.CreateDateTime
	from dbo.tbWorkOrderStatus a
	inner join (select WorkOrderID, max(WorkOrderStatusID) 'WorkOrderStatusID'
							from dbo.tbWorkOrderStatus group by WorkOrderID) b on a.WorkOrderStatusID = b.WorkOrderStatusID
	left join dbo.tbWOStatusCode c on a.WOStatusCode = c.WOStatusCode