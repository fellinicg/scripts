USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.sfServerType') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.sfServerType
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100104
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100104
-- ============================================================
CREATE FUNCTION dbo.sfServerType (@ServerType char(1))
	RETURNS varchar(20) AS
BEGIN

	return (select case @ServerType when 'P' then 'Physical' 
		when 'V' then 'Virtual' else @ServerType end)

END
