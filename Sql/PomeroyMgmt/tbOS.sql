USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbOS') AND type in (N'U'))
DROP TABLE dbo.tbOS
GO
CREATE TABLE dbo.tbOS(
	OSID tinyint identity(1,1) not null,
	ShortDesc varchar(10) not null,
	Description varchar(50) not null,
	IsDeleted bit constraint dftbOSIsDeleted default 0,
 CONSTRAINT PK_tbOS PRIMARY KEY CLUSTERED 
(
	OSID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
set identity_insert dbo.tbOS ON
GO
insert into dbo.tbOS (OSID, ShortDesc, Description, IsDeleted) values('1','2000Adv','Windows 2000 Advanced Server','0')
insert into dbo.tbOS (OSID, ShortDesc, Description, IsDeleted) values('2','2003Std','Windows 2003 Standard Edition','0')
insert into dbo.tbOS (OSID, ShortDesc, Description, IsDeleted) values('3','2003Ent','Windows 2003 Enterprise Edition','0')
GO
set identity_insert dbo.tbOS OFF