USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbReport') AND type in (N'U'))
DROP TABLE dbo.tbReport
GO
CREATE TABLE dbo.tbReport(
	ReportID smallint identity(1,1) not null,
	ReportName varchar(50) not null,
	ReportType char(1) not null constraint dftbReportReportType default 'S',
	Page varchar(20) not null,
	IsDeleted bit not null constraint dftbReportIsDeleted default 0,
	CreateDateTime datetime not null constraint dftbReportCreateDateTime default (getdate()),
 CONSTRAINT PK_tbReport PRIMARY KEY CLUSTERED 
(
	ReportID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbReport with check add constraint tcReportType check (ReportType='S')
