USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddNote') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddNote
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091117
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091117
-- ============================================================
CREATE PROCEDURE dbo.upAddNote
	@SourceID int
	,@NoteType char(1)
	,@Note varchar(1000)
	,@UserAccountID int
	,@Result char(1) output
AS
BEGIN

	-- Declarations

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Add record
  insert into dbo.tbNote (SourceID, NoteType, Note, UserAccountID)
  values (@SourceID, upper(@NoteType), @Note, @UserAccountID)

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upAddNote - Note was NOT successfully added.', 16, 1)

END
GO
grant exec on dbo.upAddNote to system