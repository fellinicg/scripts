USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddServerRole') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetRoles
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091203
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091203
-- ============================================================
CREATE PROCEDURE dbo.upAddServerRole
	@ServerID int
	,@RoleID int
	,@Result char(1) output	
AS
BEGIN

	-- Declarations

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Delete user role
	insert into dbo.tbServerRole (ServerID, RoleID)
	values (@ServerID, @RoleID)

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'

END
GO
grant exec on dbo.upAddServerRole to system