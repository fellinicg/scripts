USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetWOComments') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetWOComments
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100510
-- ============================================================
CREATE PROCEDURE dbo.upGetWOComments
	@WorkOrderID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return workorder comments
	select WOCommentID, WOComment, 
		case UserAccountID when 0 then 'System' else dbo.sfFullName(UserAccountID) end 'Author', 
		convert(varchar, CreateDateTime, 100) AddedOn
	from dbo.tbWOComment
	where WorkOrderID = @WorkOrderID and IsDeleted = 0
	order by CreateDateTime desc

END
GO
grant exec on dbo.upGetWOComments to system