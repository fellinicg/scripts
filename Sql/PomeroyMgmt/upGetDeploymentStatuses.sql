USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetDeploymentStatuses') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetDeploymentStatuses
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091202
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091202
-- ============================================================
CREATE PROCEDURE dbo.upGetDeploymentStatuses
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select DeploymentStatusID, Description
	from dbo.tbDeploymentStatus
	where IsDeleted = 0
	order by Description

END
GO
grant exec on dbo.upGetDeploymentStatuses to system