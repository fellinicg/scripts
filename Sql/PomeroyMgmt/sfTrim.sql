USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.sfTrim') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.sfTrim
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091111
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091111
-- ============================================================
GO
CREATE FUNCTION dbo.sfTrim (@Input varchar(1000))
RETURNS varchar(1000)
AS
BEGIN
  
  return(ltrim(rtrim(@Input)))

END
