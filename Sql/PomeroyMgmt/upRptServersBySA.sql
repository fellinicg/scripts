USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upRptServersBySA') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upRptServersBySA
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100113
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100113
-- ============================================================
CREATE PROCEDURE dbo.upRptServersBySA
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select count(isnull(convert(varchar, sa.UserAccountID), s.SupportType)) 'ServerCt', 
	case isnull(convert(varchar, sa.UserAccountID), s.SupportType) when 'H' then '*** Hardware Only' else dbo.sfFullName(sa.UserAccountID) end 'SA'
	from dbo.tbServer s
		left join dbo.tbUARoleServer sa on s.ServerID = sa.ServerID and sa.UARoleID = 2 and sa.IsPrimary = 1
		left join dbo.vwSrvDeploymentStatus ds on s.ServerID = ds.ServerID
	where s.SupportType <> 'D' and ds.DeploymentStatusID <> 3
	group by sa.UserAccountID, isnull(convert(varchar, sa.UserAccountID), s.SupportType)
	order by ServerCt
		
END
GO
grant exec on dbo.upRptServersBySA to system
