USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upDeleteEmailAddr') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upDeleteEmailAddr
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091209
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091209
-- ============================================================
CREATE PROCEDURE dbo.upDeleteEmailAddr
	@EmailAddressID int
	,@UserAccountID int
	,@Result char(1) output
AS
BEGIN

	-- Declarations

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Update DepartmentID
	update dbo.tbEmailAddress set IsDeleted = 1, UpdateDateTime = current_timestamp
	where EmailAddressID = @EmailAddressID and UserAccountID = @UserAccountID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'

END
GO
grant exec on dbo.upDeleteEmailAddr to system