USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetApplications') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetApplications
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091207
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091207
-- ============================================================
CREATE PROCEDURE dbo.upGetApplications
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select ApplicationID, Description
	from dbo.tbApplication
	where IsDeleted = 0
	order by Description

END
GO
grant exec on dbo.upGetApplications to system