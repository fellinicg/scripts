USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upMaintainWODoc') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upMaintainWODoc
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100128
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100128
-- ============================================================
CREATE PROCEDURE dbo.upMaintainWODoc
	@WorkOrderID int
	,@WODocIDs varchar(1000)
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Begin transaction
	begin tran
	
	begin try
		-- Undelete
		update dbo.tbWorkOrderDoc set IsDeleted = 0
		from dbo.tbWorkOrderDoc
		left join dbo.tfCommaListToTable(@WODocIDs) on WorkOrderDocID = ListItem
		where WorkOrderID = @WorkOrderID and IsDeleted = 1
	
		-- Delete
		update dbo.tbWorkOrderDoc set IsDeleted = 1
		from dbo.tbWorkOrderDoc
		left join dbo.tfCommaListToTable(@WODocIDs) on WorkOrderDocID = ListItem
		where WorkOrderID = @WorkOrderID and IsDeleted = 0 and ListItem is null
	
		commit tran
		set @Result = 'S'
	end try
	begin catch
		rollback tran
		raiserror('upMaintainWODoc - work order documents were NOT successfully maintained', 16, 1)
	end catch
	
END
GO
grant exec on dbo.upMaintainWODoc to system