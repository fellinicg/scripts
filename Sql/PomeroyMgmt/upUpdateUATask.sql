USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateUATask') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateUATask
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091217
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091217
-- ============================================================
CREATE PROCEDURE dbo.upUpdateUATask
	@UATaskID int
	,@TaskStatus char(1)
	,@Result char(1) output	
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Update record
	update dbo.tbUATask set TaskStatus = @TaskStatus
	where UATaskID = @UATaskID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upUpdateUATask - user task was NOT successfully updated', 16, 1)

END
GO
grant exec on dbo.upUpdateUATask to system