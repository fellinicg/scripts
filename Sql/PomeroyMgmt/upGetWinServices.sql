USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetWinServices') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetWinServices
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091130
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091130
-- ============================================================
CREATE PROCEDURE dbo.upGetWinServices
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select DisplayName, Exe
	from dbo.tbWinService
	order by DisplayName

END
GO
grant exec on dbo.upGetWinServices to system