USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbUARole') AND type in (N'U'))
DROP TABLE dbo.tbUARole
GO
CREATE TABLE dbo.tbUARole(
	UARoleID int identity(1,1) not null,
	ShortDesc varchar(10) not null,
	Description varchar(50) not null,
	IsDeleted bit constraint dftbUARoleIsDeleted default 0,
 CONSTRAINT PK_tbUARole PRIMARY KEY CLUSTERED 
(
	UARoleID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
go
set identity_insert dbo.tbUARole on
go
insert into dbo.tbUARole (UARoleID, ShortDesc, Description, IsDeleted) values('1','DBA','Database Administrator','0')
insert into dbo.tbUARole (UARoleID, ShortDesc, Description, IsDeleted) values('2','SA','System Administrator','0')
go
set identity_insert dbo.tbUARole off
