USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbWorkOrderRequestor') AND type in (N'U'))
DROP TABLE dbo.tbWorkOrderRequestor
GO
CREATE TABLE dbo.tbWorkOrderRequestor(
	WorkOrderRequestorID int identity(1,1) not null,
	WorkOrderID int not null,
	ClientContactID int not null,
	IsDeleted bit not null constraint dftbWorkOrderRequestorIsDeleted default (0),
	CreateDateTime datetime not null constraint dftbWorkOrderRequestorCreateDateTime default (getdate()),
 CONSTRAINT PK_tbWorkOrderRequestor PRIMARY KEY CLUSTERED 
(
	WorkOrderRequestorID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE dbo.tbWorkOrderRequestor  WITH CHECK ADD CONSTRAINT fktbWorkOrderRequestorClientContactID FOREIGN KEY(ClientContactID)
REFERENCES dbo.tbClientContact (ClientContactID)
GO
ALTER TABLE dbo.tbWorkOrderRequestor  WITH CHECK ADD CONSTRAINT fktbWorkOrderRequestorWorkOrderID FOREIGN KEY(WorkOrderID)
REFERENCES dbo.tbWorkOrder (WorkOrderID)