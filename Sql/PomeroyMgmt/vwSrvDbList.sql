USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'dbo.vwSrvDbList'))
DROP VIEW dbo.vwSrvDbList
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20110801
-- ============================================================
CREATE VIEW dbo.vwSrvDbList
AS
	select a.ServerID, b.SQLInstanceID, isnull(b.SQLInstanceName, a.ServerName) 'SQLInstanceName', b.Cluster, isnull(b.IsOnline, a.IsOnline) 'IsOnline'
	from dbo.tbServer a 
	left join dbo.vwSrvDeploymentStatus d on a.ServerID = d.ServerID
	left join dbo.tbSQLInstance b on a.ServerID = b.ServerID
	where a.SupportType in ('F', 'D') and a.OnPawanet = 1 and a.IsOnline = 1
		 and d.DeploymentStatusID in (1,2,5)