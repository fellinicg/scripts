USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddContactNum') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddContactNum
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091209
-- ============================================================
CREATE PROCEDURE dbo.upAddContactNum
	@UserAccountID int
	,@ContactNumType char(2)
	,@ContactNum	varchar(20)
	,@ContactNumID int output
	,@Result char(1) output	
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	select @ContactNumID = ContactNumID from dbo.tbContactNum where UserAccountID = @UserAccountID 
		and ContactNumType = @ContactNumType and ContactNum = @ContactNum

	if (@ContactNumID is not null)
		-- If exists, undelete
		update dbo.tbContactNum set IsDeleted = 0, UpdateDateTime = current_timestamp
		where ContactNumID = @ContactNumID
	else
		-- Insert new record into tbContactNum
		insert into dbo.tbContactNum (UserAccountID, ContactNumType, ContactNum, 
			CreateDateTime, UpdateDateTime)
		values (@UserAccountID, upper(@ContactNumType), @ContactNum, 
			current_timestamp, current_timestamp)

	-- Verify success
	if (@@rowcount = 1)
	begin
		set @Result = 'S'
		if (@ContactNumID is null) set @ContactNumID = @@identity
	end
	else
		raiserror('upAddContactNum - contact number was NOT successfully added', 16, 1)

END
GO
grant exec on dbo.upAddContactNum to system