USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upMaintainWORequestor') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upMaintainWORequestor
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100128
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100128
-- ============================================================
CREATE PROCEDURE dbo.upMaintainWORequestor
	@WorkOrderID int
	,@ClientContacts varchar(1000)
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Begin transaction
	begin tran
	
	begin try
		-- Undelete
		update dbo.tbWorkOrderRequestor set IsDeleted = 0
		from dbo.tbWorkOrderRequestor
		inner join dbo.tfCommaListToTable(@ClientContacts) on ClientContactID = ListItem
		where WorkOrderID = @WorkOrderID and IsDeleted = 1
	
		-- Delete
		update dbo.tbWorkOrderRequestor set IsDeleted = 1
		from dbo.tbWorkOrderRequestor
		left join dbo.tfCommaListToTable(@ClientContacts) on ClientContactID = ListItem
		where WorkOrderID = @WorkOrderID and IsDeleted = 0 and ListItem is null
	
		-- Add
		insert into dbo.tbWorkOrderRequestor (WorkOrderID, ClientContactID)
		select @WorkOrderID, ListItem
		from dbo.tfCommaListToTable(@ClientContacts)
		left join dbo.tbWorkOrderRequestor on ListItem = ClientContactID and WorkOrderID = @WorkOrderID
		where WorkOrderRequestorID is null
	
		commit tran
		set @Result = 'S'
	end try
	begin catch
		rollback tran
		raiserror('upMaintainWORequestor - work order requestor was NOT successfully maintained', 16, 1)
	end catch
	
END
GO
grant exec on dbo.upMaintainWORequestor to system