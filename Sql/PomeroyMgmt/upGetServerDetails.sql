USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetServerDetails') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetServerDetails
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091124
-- ============================================================
CREATE PROCEDURE dbo.upGetServerDetails
	@ServerID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select s.ServerName, s.SerialNumber, s.Manufacturer, s.Model, s.ProcessorInfo,
		s.Processors, s.MemoryBytes, s.OS, s.OSVer, s.OSServicePack, i.IPAddress, s.OSProductID,
		s.OSType, s.MajorApps, s.IsOnline, s.OnPawanet, dbo.sfServerType(s.ServerType) 'ServerType'
	from dbo.tbServer s
	left join tbServerIP i on s.ServerID = i.ServerID
	where s.ServerID = @ServerID

	-- Return IP address list
	--select i.IPAddress
	--from dbo.tbServer s
	--left join tbServerIP i on s.ServerID = i.ServerID
	--where s.ServerID = @ServerID

END
GO
grant exec on dbo.upGetServerDetails to system