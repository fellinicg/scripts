USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetReportInfo') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetReportInfo
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100114
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100114
-- ============================================================
CREATE PROCEDURE dbo.upGetReportInfo
	@ReportID smallint
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
  select ReportName, ReportType, Page, StoredProc, Sort, Filter
	from dbo.tbReport 
	where ReportID = @ReportID

END
GO
grant exec on dbo.upGetReportInfo to system