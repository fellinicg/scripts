USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetModelList') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetModelList
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100108
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100108
-- ============================================================
CREATE PROCEDURE dbo.upGetModelList
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select distinct Model 'Description'
	from dbo.tbServer
	where dbo.sfTrim(isnull(Model, '')) <> ''
	order by Model

END
GO
grant exec on dbo.upGetModelList to system