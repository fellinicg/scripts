USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddEmailAddress') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddEmailAddress
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091119
-- ============================================================
CREATE PROCEDURE dbo.upAddEmailAddress
	@UserAccountID int
	,@EmailAddressType char(1)
	,@EmailAddress	varchar(120)
	,@EmailAddressID int output
	,@Result char(1) output	
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	select @EmailAddressID = EmailAddressID from dbo.tbEmailAddress where UserAccountID = @UserAccountID 
		and EmailAddressType = @EmailAddressType and EmailAddress = @EmailAddress

	if (@EmailAddressID is not null)
		-- If exists, undelete
		update dbo.tbEmailAddress set IsDeleted = 0, UpdateDateTime = current_timestamp
		where EmailAddressID = @EmailAddressID
	else
		-- Insert new record into tbEmailAddress
		insert into dbo.tbEmailAddress (UserAccountID, EMailAddressType, EmailAddress, 
			CreateDateTime, UpdateDateTime)
		values (@UserAccountID, upper(@EMailAddressType), @EmailAddress,
			current_timestamp, current_timestamp)

	-- Verify success
	if (@@rowcount = 1)
	begin
		set @Result = 'S'
		if (@EmailAddressID is null) set @EmailAddressID = @@identity
	end
	else
		raiserror('upAddEmailAddress - email address was NOT successfully added', 16, 1)

END
GO
grant exec on dbo.upAddEmailAddress to system