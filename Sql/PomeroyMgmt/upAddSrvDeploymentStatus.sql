USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddSrvDeploymentStatus') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddSrvDeploymentStatus
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091227
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091227
-- ============================================================
CREATE PROCEDURE dbo.upAddSrvDeploymentStatus
	@ServerID int
	,@DeploymentStatusID int
	,@Result char(1) output	
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialixe variables
	set @Result = 'F'

	-- Record deployment status
	insert into dbo.tbSrvDeploymentStatus (ServerID, DeploymentStatusID)
	values (@ServerID, @DeploymentStatusID)

	-- Verify success
	if (@@rowcount = 1 and @@identity > 0)
		set @Result = 'S'
	else
		raiserror('upAddSrvDeploymentStatus - Server deployment status was NOT successfully added.', 16, 1)

END
GO
grant exec on dbo.upAddSrvDeploymentStatus to system