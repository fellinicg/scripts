USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetUsers') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetUsers
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091127
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091127
-- ============================================================
CREATE PROCEDURE dbo.upGetUsers
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select UserAccountID, dbo.sfFullName(UserAccountID) 'FullName'
	from dbo.tbUserAccount
	where AccountStatus = 'A'
	order by FirstName, LastName

END
GO
grant exec on dbo.upGetUsers to system