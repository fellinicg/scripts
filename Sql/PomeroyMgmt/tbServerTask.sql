USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbServerTask') AND type in (N'U'))
DROP TABLE dbo.tbServerTask
GO
CREATE TABLE dbo.tbServerTask(
	ServerTaskID int identity(1,1) not null,
	ServerID int not null,
	TaskID int not null,
CONSTRAINT PK_tbServerTask PRIMARY KEY CLUSTERED 
(
	ServerTaskID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbServerTask with check add constraint fktbServerTaskServerID
foreign key(ServerID) references dbo.tbServer (ServerID)
GO
alter table dbo.tbServerTask with check add constraint fktbServerTaskTaskID
foreign key(TaskID) references dbo.tbTask (TaskID)