USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upMaintainServerOrder') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upMaintainServerOrder
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091209
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091209
-- ============================================================
CREATE PROCEDURE dbo.upMaintainServerOrder
	@ServerID int
	,@OrderDate varchar(20)
	,@OrderNumber varchar(20)
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	if (len(@OrderDate) = 0) set @OrderDate = null

	-- Add/update data
	if not exists(select 1 from dbo.tbServerOrder where ServerID = @ServerID)
		-- Add record
		insert into dbo.tbServerOrder (ServerID, OrderDate, OrderNumber, CreateDateTime, UpdateDateTime)
		values (@ServerID, @OrderDate, @OrderNumber, current_timestamp, current_timestamp)
	else
		-- Update record
		update dbo.tbServerOrder set OrderDate = @OrderDate, OrderNumber = @OrderNumber, 
			UpdateDateTime = current_timestamp
		where ServerID = @ServerID

	-- Verify success
	if (@@error = 0)
		set @Result = 'S'
	else
		raiserror('upMaintainServerOrder - server order was NOT successfully maintained', 16, 1)

END
GO
grant exec on dbo.upMaintainServerOrder to system