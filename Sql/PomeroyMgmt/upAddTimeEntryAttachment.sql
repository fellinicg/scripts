use PomeroyMgmt
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upAddTimeEntryAttachment') and type in (N'P', N'PC'))
drop procedure dbo.upAddTimeEntryAttachment
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20121205
-- ============================================================
create procedure dbo.upAddTimeEntryAttachment
	@TimeEntryID int
	,@FileName varchar(500)
	,@BinaryContent varbinary(max)
	,@Result char(1) output	
as
begin

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Insert data
	insert into dbo.tbTimeEntryAttachment (TimeEntryID, FileName, BinaryContent)
	values (@TimeEntryID, @FileName, @BinaryContent)
		
	-- Verify success
	if (@@rowcount = 1 and @@identity > 0)
		set @Result = 'S'
	else
		raiserror('upAddTimeEntryAttachment - Time entry attachment was NOT successfully added.', 16, 1)

end
go
grant exec on dbo.upAddTimeEntryAttachment to system