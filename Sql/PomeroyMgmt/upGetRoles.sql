USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetRoles') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetRoles
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091203
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091203
-- ============================================================
CREATE PROCEDURE dbo.upGetRoles
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select RoleID, Description
	from dbo.tbRole
	where IsDeleted = 0
	order by Description

END
GO
grant exec on dbo.upGetRoles to system