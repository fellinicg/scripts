use PomeroyMgmt
go
set ansi_nulls on
go
set quoted_identifier on
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.sfNumAttachments') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfNumAttachments
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20121212
-- ============================================================
create function dbo.sfNumAttachments (@TimeEntryID int)
	returns int as
begin
	
	-- Return data
	return(select count(1) 
	from dbo.tbTimeEntryAttachment 
	where TimeEntryID = @TimeEntryID and IsDeleted = 0)

end
