USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbUATask') AND type in (N'U'))
DROP TABLE dbo.tbUATask
GO
CREATE TABLE dbo.tbUATask(
	UATaskID int identity(1,1) not null,
	UserAccountID int not null,
	TaskID int not null,
	LogFile varchar(500) null,
	TaskStatus char(1) not null constraint dftbUATaskTaskStatus default 'P',
	IsDeleted bit not null constraint dftbUATaskIsDeleted default 0,
	CreateDateTime datetime not null constraint dftbUATaskCreateDateTime default (getdate()),
CONSTRAINT PK_tbUATask PRIMARY KEY CLUSTERED 
(
	UATaskID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbUATask with check add constraint fktbUATaskUserAccountID
foreign key(UserAccountID) references dbo.tbUserAccount (UserAccountID)
GO
alter table dbo.tbUATask with check add constraint fktbUATaskTaskID
foreign key(TaskID) references dbo.tbTask (TaskID)