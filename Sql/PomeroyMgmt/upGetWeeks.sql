USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetWeeks') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetWeeks
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20121114
-- ============================================================
CREATE PROCEDURE dbo.upGetWeeks
AS
BEGIN

	-- Declarations
	declare @currwk int
	
	-- Initialize settings
	set nocount on

	-- Get current week id
	select @currwk = WeekID
	from dbo.tbWeek
	where current_timestamp between BeginDate and EndDate
		
	-- Return data
	select WeekID, convert(varchar, BeginDate, 101) + ' - ' + convert(varchar, EndDate, 101) 'Week', BeginDate, EndDate,
		case when WeekID = @currwk then 1 else 0 end 'CurrWk'
	from dbo.tbWeek
	order by BeginDate

END
GO
grant exec on dbo.upGetWeeks to system