USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upRptServerDrive') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upRptServerDrive
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100708
-- ============================================================
CREATE PROCEDURE dbo.upRptServerDrive
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		s.ServerID
		,s.ServerName
		,d.Drive
		,d.FreeMB
		,convert(varchar, d.PollDateTime, 100) 'PollDateTime'
		,SrvRowSpan
	from dbo.tbServerDrive d
		left join dbo.tbServer s on d.ServerID = s.ServerID
		left join (select ServerID, count(ServerID) 'SrvRowSpan' 
				from dbo.tbServerDrive group by ServerID) sr on s.ServerID = sr.ServerID
	order by s.ServerName, d.Drive

END
GO
grant exec on dbo.upRptServerDrive to system