USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbWorkOrderStatus') AND type in (N'U'))
DROP TABLE dbo.tbWorkOrderStatus
GO
CREATE TABLE dbo.tbWorkOrderStatus(
	WorkOrderStatusID int identity(1,1) not null,
	WorkOrderID int not null,
	WOStatusCode char(1) not null,
	UserAccountID int not null,
	CreateDateTime datetime not null constraint dftbWorkOrderStatusCreateDateTime default (getdate()),
 CONSTRAINT PK_tbWorkOrderStatus PRIMARY KEY CLUSTERED 
(
	WorkOrderStatusID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE dbo.tbWorkOrderStatus  WITH CHECK ADD CONSTRAINT fktbWorkOrderStatusCode FOREIGN KEY(WOStatusCode)
REFERENCES dbo.tbWOStatusCode (WOStatusCode)
GO
ALTER TABLE dbo.tbWorkOrderStatus WITH CHECK ADD CONSTRAINT fktbWorkOrderStatusUserAccountID FOREIGN KEY(UserAccountID)
REFERENCES dbo.tbUserAccount (UserAccountID)
GO
ALTER TABLE dbo.tbWorkOrderStatus  WITH CHECK ADD CONSTRAINT fktbWorkOrderStatusWorkOrderID FOREIGN KEY(WorkOrderID)
REFERENCES dbo.tbWorkOrder (WorkOrderID)