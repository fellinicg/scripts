USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbDeploymentStatus') AND type in (N'U'))
DROP TABLE dbo.tbDeploymentStatus
GO
CREATE TABLE dbo.tbDeploymentStatus(
	DeploymentStatusID tinyint identity(1,1) not null,
	Description varchar(50) not null,
	IsDeleted bit constraint not null dftbDeploymentStatusIsDeleted default 0,
 CONSTRAINT PK_tbDeploymentStatus PRIMARY KEY CLUSTERED 
(
	DeploymentStatusID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF