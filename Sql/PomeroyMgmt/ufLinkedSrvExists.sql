USE Tools
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.ufLinkedSrvExists') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.ufLinkedSrvExists
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091106
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091106
-- ============================================================
CREATE FUNCTION dbo.ufLinkedSrvExists (@Server varchar(50))
RETURNS TABLE 
AS
RETURN 
(
	select data_source from master.sys.servers where data_source = @Server
)