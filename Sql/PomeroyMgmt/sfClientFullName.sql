USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.sfClientFullName') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.sfClientFullName
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100119
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100119
-- ============================================================
CREATE FUNCTION dbo.sfClientFullName (@ClientContactID int)
	RETURNS varchar(200) AS
BEGIN

	-- Get data
	return(select FirstName + ' ' + case when isnull(MiddleInitial, '') <> '' then MiddleInitial + '. ' else '' end + LastName
	from dbo.tbClientContact
	where ClientContactID = @ClientContactID)

END
