USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetWOStatusCodes') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetWOStatusCodes
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100122
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100122
-- ============================================================
CREATE PROCEDURE dbo.upGetWOStatusCodes
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select WOStatusCode, Description
	from dbo.tbWOStatusCode
	where IsDeleted = 0
	order by Description

END
GO
grant exec on dbo.upGetWOStatusCodes to system