USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'dbo.vwSrvDbSupported'))
DROP VIEW dbo.vwSrvDbSupported
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091202
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091202
-- ============================================================
CREATE VIEW dbo.vwSrvDbSupported
AS
	select distinct s.ServerID, IsSupported
	from dbo.tbServer s
	inner join dbo.tbDatabase d on s.ServerID = d.ServerID and IsSupported = 1
