USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddWorkOrder') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddWorkOrder
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100128
-- ============================================================
CREATE PROCEDURE dbo.upAddWorkOrder
	@Title varchar(300)
	,@Description text
	,@DepartmentID int
	,@ReceivedDate smalldatetime
	,@IsBillable bit
	,@WOStatusCode char(1)
	,@ClientContacts varchar(1000)
	,@UserAccountID int
	,@ServiceOrder varchar(200)
	,@ClientProject varchar(50)
	,@DesiredCompletionDate smalldatetime
	,@RequestDate smalldatetime
	,@EstCost money
	,@FixedCost money
	,@Comment varchar(3000)
	,@AssignedTos varchar(1000)
	,@WorkOrderID int output
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'X'
	set @Comment = dbo.sfEmptyToNull(@Comment)
	set @AssignedTos = dbo.sfEmptyToNull(@AssignedTos)

	-- Check for duplicate
	if exists(select 1 from tbWorkOrder where dbo.sfTrim(Title) = dbo.sfTrim(@Title))
		return

	set @Result = 'F'
		
	begin tran
	
	begin try
		-- Add record
		insert into dbo.tbWorkOrder (WorkOrderID, Title, Description, DepartmentID, ServiceOrder, ClientProject,
			ReceivedDate, DesiredCompletionDate, RequestDate, IsBillable, EstCost, 
			FixedCost, CreateDateTime, UpdateDateTime)
		values (dbo.sfGetNextWorkOrderID(), @Title, @Description, @DepartmentID, dbo.sfEmptyToNull(@ServiceOrder),
			dbo.sfEmptyToNull(@ClientProject), @ReceivedDate, dbo.sfEmptyToNull(@DesiredCompletionDate),
			dbo.sfEmptyToNull(@RequestDate), @IsBillable, isnull(dbo.sfEmptyToNull(@EstCost), 0), 
			isnull(dbo.sfEmptyToNull(@FixedCost), 0), current_timestamp, current_timestamp)

		-- Verify success
		if (@@rowcount = 1 and @@identity > 0)
			select @WorkOrderID = WorkOrderID from dbo.tbWorkOrder where Idx = @@identity 
		else
		begin
			rollback tran
			raiserror('upAddWorkOrder - Work order was NOT successfully added', 16, 1)
			return
		end

		-- Add status
		exec dbo.upAddWorkOrderStatus @WorkOrderID, @WOStatusCode, @UserAccountID, @Result output
		
		-- Verify success
		if (@Result = 'S')
			set @Result = 'F'
		else
		begin
			rollback tran
			raiserror('upAddWorkOrder - Work order status was NOT successfully added', 16, 1)
			return
		end
		
		-- Add requestor(s)
		exec dbo.upMaintainWORequestor @WorkOrderID, @ClientContacts, @Result output
		
		-- Verify success
		if (@Result <> 'S')
		begin
			rollback tran
			raiserror('upAddWorkOrder - Work order requestor was NOT successfully added', 16, 1)
			return
		end
		
		-- Add comment, if necessary
		if (@Comment is not null)
		begin
			exec dbo.upAddWOComment @WorkOrderID, @Comment, @UserAccountID, @Result output
		
			-- Verify success
			if (@Result <> 'S')
			begin
				rollback tran
				raiserror('upAddWorkOrder - Work order note was NOT successfully added', 16, 1)
				return
			end
		end
		
		-- Add assigned to, if necessary
		if (@AssignedTos is not null)
		begin
			exec dbo.upMaintainWOAssignedTo @WorkOrderID, @AssignedTos, @Result output
		
			-- Verify success
			if (@Result <> 'S')
			begin
				rollback tran
				raiserror('upAddWorkOrder - Work order assigned to was NOT successfully added', 16, 1)
				return
			end
		end
	end try
	begin catch
		declare @Errormsg nvarchar(2048) 
		set @Result = 'F'
		set @WorkOrderID = null
		rollback tran
		select @Errormsg = ERROR_MESSAGE()
		raiserror(@Errormsg, 16, 1)
		return
	end catch

	commit tran
END
GO
grant exec on dbo.upAddWorkOrder to system