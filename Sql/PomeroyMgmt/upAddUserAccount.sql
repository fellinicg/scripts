USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddUserAccount') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddUserAccount
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091112
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091112
-- ============================================================
CREATE PROCEDURE dbo.upAddUserAccount
	@WindowsUser varchar(50)
	,@FirstName varchar(50)
	,@LastName varchar(80)
	,@AccountStatus char(1)
	,@UserAccountID int output
	,@Result char(1) output
AS
BEGIN

	-- Declarations

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @UserAccountID = 0
	set @Result = 'F'

	-- Add user
	insert into dbo.tbUserAccount (WindowsUser, FirstName, LastName, AccountStatus)
	values (@WindowsUser, @FirstName, @LastName, @AccountStatus)

	-- Verify success
	if @@rowcount > 0 and @@identity > 0
		begin
			set @UserAccountID = @@identity
			set @Result = 'S'
		end

END
GO
grant exec on dbo.upAddUserAccount to system