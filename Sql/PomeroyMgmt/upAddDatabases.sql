USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddDatabases') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddDatabases
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091104
-- ============================================================
CREATE PROCEDURE dbo.upAddDatabases
--	@Result char(1) output
AS
BEGIN

	-- Declarations
	declare @Servers table (ServerID int, InstanceID tinyint, ServerName varchar(50), Cluster varchar(50))
	declare @Temp table (ServerName varchar(50))
	declare @DBs table (dbname varchar(100), DBID int, status varchar(30))
	declare 
		@ServerId int,
		@InstanceID int,
		@ServerName varchar(50),
		@Cluster varchar(50),
		@DBname varchar(64),
		@DBID int,
		@Status varchar(40),
		@rc char(1),
		@local bit,
		@cmd varchar(max)

	-- Initialize settings
	set nocount on

	-- Initialize variables
	--	set @Result = 'F'

	-- Insert a list of servers into table var
	insert into @Servers
	select ServerID, SQLInstanceID, SQLInstanceName, Cluster
	from dbo.vwSrvDbList
	order by Cluster, IsOnline desc
			 
	-- Truncate tbDBFiles
	truncate table dbo.tbDBFiles

	-- Loop over servers
	while exists(select 1 from @Servers)
		begin
			select top 1 @ServerID = ServerID, @ServerName = ServerName, @InstanceID = InstanceID, @Cluster = Cluster from @Servers
			-- Check if server is local
			if @@servername = @ServerName
				set @local = 1
			else
				set @local = 0
			-- Check if the server is a linked server, if not, create it
			if (select dbo.sfLinkedSrvExists(@ServerName)) is null
				exec dbo.upAddLinkedServer @ServerName, @rc output
			else set @rc = 'S'
			if @rc = 'S'
				begin
					-- Test connection to remote sql server
					if @local = 0 
						exec dbo.upLinkedSrvAvail @ServerName, @rc output
					if @rc = 'F' and @Cluster is not null 
						begin
							-- Check if the server is a linked server, if not, create it
							if (select dbo.sfLinkedSrvExists(@Cluster)) is null
								exec dbo.upAddLinkedServer @Cluster, @rc output
							-- Test connection to remote sql server
							exec dbo.upLinkedSrvAvail @Cluster, @rc output
							if @rc <> 'F'
							  begin
									-- Clear temporary table
									delete from @Temp
									-- Populate temporary table
									insert into @Temp
									exec ('select * from openquery([' + @Cluster + '], 
												''select convert(varchar, serverproperty(''''servername''''))'')')
									if exists (select 1 from @Temp where ServerName = @ServerName)
										begin
											set @rc = 'S'
											set @ServerName = @Cluster
										end
									else
										set @rc = 'F'
								end
						end
					if @rc = 'F'
						begin
							-- Set databases to offline
							update dbo.tbDatabase set IsOnline = 0 
								where ServerID = @ServerID and isnull(SQLInstanceID, 0) = isnull(@InstanceID, 0)
							-- Set instance to offline
							update dbo.tbSQLInstance set IsOnline = 0
								where ServerID = @ServerID and isnull(SQLInstanceID, 0) = isnull(@InstanceID, 0)
						end 
					else
						begin
							-- Set instance to online
							update dbo.tbSQLInstance set IsOnline = 1
								where ServerID = @ServerID and isnull(SQLInstanceID, 0) = isnull(@InstanceID, 0)
							-- Clear table variable
							delete from @DBs
							-- Get list of user databases
							if @local = 0
								insert into @DBs
								exec ('select * from openquery([' + @ServerName + '], 
											''select name, 0, convert(varchar, databasepropertyex(name, ''''Status'''')) from master.dbo.sysdatabases
											where name not in (''''master'''',''''tempdb'''',''''model'''',''''msdb'''',''''distribution'''')
											order by name'')')
							else
								insert into @DBs
								select name, 0, convert(varchar, databasepropertyex(name, 'Status'))
									from master.dbo.sysdatabases
									where name not in ('master','tempdb','model','msdb','distribution')
									order by name

							-- Update DBID in table variable
							update @DBs set DBID = DatabaseID
							from @DBs s
							inner join dbo.tbDatabase d on s.DBName = d.DBName and @ServerID = d.ServerID
								and isnull(@InstanceID, 0) = isnull(d.SQLInstanceID, 0)

							-- Insert databases that do not already exist
							insert into tbDatabase (DBName, ServerID, SQLInstanceID, CreateDateTime)
							select dbname, @ServerID, @InstanceID, current_timestamp from @DBs
							where DBID = 0

							-- Update tbDatabase
							update dbo.tbDatabase set IsOnline = case status when 'ONLINE' then 1 else 0 end,
								IsDeleted = case isnull(status, '1') when '1' then 1 else 0 end,
								UpdateDateTime = current_timestamp
							from dbo.tbDatabase
							left join @DBs on DatabaseID = DBID
							where ServerID = @ServerID and isnull(SQLInstanceID, 0) = isnull(@InstanceID, 0)

							-- Populate tbDBFiles
							set @cmd = 'use [master]
							if (object_id(''''PM_DBInfo'''') is not null)
							drop table PM_DBInfo
							create table PM_DBInfo (ServerName varchar(100),DatabaseName varchar(100),FileSizeMB varchar(20),
							LogicalFileName varchar(500),PhysicalFileName varchar(1000),Status varchar(100),
							Updateability varchar(100),RecoveryMode varchar(100),FreeSpaceMB varchar(20),FreeSpacePct varchar(7),
							MaxSize varchar(50),Growth varchar(50),PollDate datetime)
							declare @command varchar(5000)  
							set @command = ''''Use [' + '?' + ']
							select  
							@@servername
							,' + '''''' + '''''?''''' + '''''' + '
							,convert(varchar, cast(round(sysfiles.size/128.00, 2) as decimal(10,2))) + ' + '''''''''' + ' MB' + '''''''''' + '
							,sysfiles.name
							,sysfiles.filename
							,convert(varchar,DatabasePropertyEx(''''''''?'''''''',''''''''Status''''''''))
							,convert(varchar,DatabasePropertyEx(''''''''?'''''''',''''''''Updateability''''''''))
							,convert(varchar,DatabasePropertyEx(''''''''?'''''''',''''''''Recovery''''''''))
							,convert(varchar, cast(round(sysfiles.size/128.00 - fileproperty(sysfiles.name, ' + '''''''''' +  'SpaceUsed' + '''''''''' + ' )/128.00, 2) as decimal(10,2))) + ' + '''''''''' + ' MB' + '''''''''' + '
							,cast(100 * (cast(((sysfiles.size/128.00 - fileproperty(sysfiles.name, ' + '''''''''' + 'SpaceUsed' + '''''''''' + ' )/128.00)/(sysfiles.size/128.00)) AS decimal(4,2))) AS varchar(8)) + ' + '''''''''' + '%' + '''''''''' + '
							,case maxsize when -1 then ''''''''Unlimited'''''''' else convert(varchar, (convert(bigint, maxsize) * 8) / 1024) + ' + '''''''''' + ' MB' + '''''''''' + ' end
							,case status & 0x100000 when 0 then convert(varchar, (growth * 8) / 1024) + ' + '''''''''' + ' MB' + '''''''''' + ' else convert(varchar, growth) + ' + '''''''''' + '%' + '''''''''' + ' end
							,getdate()
							from dbo.sysfiles
							''''
							insert into PM_DBInfo
							exec sp_MSForEachDB @command'

							if @Local = 0
							begin
								exec('exec [' + @ServerName + '].master.dbo.sp_sqlexec ''' + @cmd + '''')

								insert into dbo.tbDBFiles (DatabaseID, LogicalFileName, PhysicalFileName, Status, FileSizeMB, Updateability, RecoveryMode, FreeSpaceMB, FreeSpacePct, MaxSize, Growth, PollDateTime)
								exec ('select DatabaseID, LogicalFileName, PhysicalFileName, Status, FileSizeMB, Updateability, RecoveryMode,
											FreeSpaceMB, FreeSpacePct, MaxSize, Growth, PollDate from openquery([' + @ServerName + '], 
											''select * from master.dbo.PM_DBInfo'')
											inner join dbo.tbDatabase on DatabaseName = DBName and ServerID = ' + @ServerID)
							end
							else
							begin
								set @cmd = replace(@cmd, '''''', '''')
								exec master.dbo.sp_sqlexec @cmd

								insert into dbo.tbDBFiles (DatabaseID, LogicalFileName, PhysicalFileName, Status, FileSizeMB, Updateability, RecoveryMode, FreeSpaceMB, FreeSpacePct, MaxSize, Growth, PollDateTime)
								exec ('select DatabaseID, LogicalFileName, PhysicalFileName, Status, FileSizeMB, Updateability, RecoveryMode,
											FreeSpaceMB, FreeSpacePct, MaxSize, Growth, PollDate from master.dbo.PM_DBInfo
											inner join dbo.tbDatabase on DatabaseName = DBName and ServerID = ' + @ServerID)
							end
						end
				end
			delete from @Servers where ServerID = @ServerID and isnull(InstanceID, 0) = isnull(@InstanceID, 0)
		end

	-- Verify success
	--	set @Result = 'S'

END
GO
grant exec on dbo.upAddDatabases to system