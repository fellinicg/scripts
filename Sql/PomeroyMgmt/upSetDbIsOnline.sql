USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upSetDbIsOnline') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upSetDbIsOnline
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091208
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091208
-- ============================================================
CREATE PROCEDURE dbo.upSetDbIsOnline
	@DatabaseID int
	,@IsOnline bit
	,@Result char(1) output
AS
BEGIN

	-- Declarations

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Update DepartmentID
	update dbo.tbDatabase set IsOnline = @IsOnline
	where DatabaseID = @DatabaseID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'

END
