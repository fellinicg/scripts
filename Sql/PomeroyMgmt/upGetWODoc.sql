USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetWODoc') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetWODoc
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100128
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100128
-- ============================================================
CREATE PROCEDURE dbo.upGetWODoc
	@WorkOrderID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select WorkOrderDocID, WorkOrderDoc
	from dbo.tbWorkOrderDoc
	where WorkOrderID = @WorkOrderID
	order by WorkOrderDoc

END
GO
grant exec on dbo.upGetWODoc to system