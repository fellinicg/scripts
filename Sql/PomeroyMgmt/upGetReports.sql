USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetReports') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetReports
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100104
-- ============================================================
CREATE PROCEDURE dbo.upGetReports
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
  select ReportID, ReportName, ReportType, Page, HasOptions
	from dbo.tbReport 
	where IsDeleted = 0
	order by ReportName

END
GO
grant exec on dbo.upGetReports to system