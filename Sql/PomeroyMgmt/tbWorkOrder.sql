USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.fktbWorkOrderAssignedToWorkOrderID') AND parent_object_id = OBJECT_ID(N'dbo.tbWorkOrderAssignedTo'))
ALTER TABLE dbo.tbWorkOrderAssignedTo DROP CONSTRAINT fktbWorkOrderAssignedToWorkOrderID
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.fktbWorkOrderDocWorkOrderID') AND parent_object_id = OBJECT_ID(N'dbo.tbWorkOrderDoc'))
ALTER TABLE dbo.tbWorkOrderDoc DROP CONSTRAINT fktbWorkOrderDocWorkOrderID
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.fktbWorkOrderRequestorWorkOrderID') AND parent_object_id = OBJECT_ID(N'dbo.tbWorkOrderRequestor'))
ALTER TABLE dbo.tbWorkOrderRequestor DROP CONSTRAINT fktbWorkOrderRequestorWorkOrderID
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.fktbWorkOrderStatusWorkOrderID') AND parent_object_id = OBJECT_ID(N'dbo.tbWorkOrderStatus'))
ALTER TABLE dbo.tbWorkOrderStatus DROP CONSTRAINT fktbWorkOrderStatusWorkOrderID
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.fktbtbWOCommentWorkOrderID') AND parent_object_id = OBJECT_ID(N'dbo.tbWorkOrderStatus'))
ALTER TABLE dbo.tbWOComment DROP CONSTRAINT fktbtbWOCommentWorkOrderID
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbWorkOrder') AND type in (N'U'))
DROP TABLE dbo.tbWorkOrder
GO
CREATE TABLE dbo.tbWorkOrder(
	WorkOrderID int not null,
	Title varchar(300) not null,
	Description varchar(2000) not null,
	DepartmentID int not null,
	ServiceOrder varchar(200) null,
	ClientProject varchar(50) null,
	ReceivedDate smalldatetime not null,
	DesiredCompletionDate smalldatetime null,
	CompletionDate smalldatetime null,
	RequestDate smalldatetime null,
	IsBillable bit not null,
	EstCost money not null constraint dftbWorkOrderEstCost default (0),
	FixedCost money not null constraint dftbWorkOrderFixedCost default (0),
	Idx int identity(1,1) not null,
	CreateDateTime datetime not null constraint dftbWorkOrderCreateDateTime default (getdate()),
	UpdateDateTime datetime not null constraint dftbWorkOrderUpdateDateTime default (getdate()),
 CONSTRAINT PK_tbWorkOrder PRIMARY KEY CLUSTERED 
(
	WorkOrderID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE dbo.tbWorkOrderAssignedTo  WITH CHECK ADD  CONSTRAINT fktbWorkOrderAssignedToWorkOrderID FOREIGN KEY(WorkOrderID)
REFERENCES dbo.tbWorkOrder (WorkOrderID)
GO
ALTER TABLE dbo.tbWorkOrderDoc  WITH CHECK ADD  CONSTRAINT fktbWorkOrderDocWorkOrderID FOREIGN KEY(WorkOrderID)
REFERENCES dbo.tbWorkOrder (WorkOrderID)
GO
ALTER TABLE dbo.tbWorkOrderRequestor  WITH CHECK ADD CONSTRAINT fktbWorkOrderRequestorWorkOrderID FOREIGN KEY(WorkOrderID)
REFERENCES dbo.tbWorkOrder (WorkOrderID)
GO
ALTER TABLE dbo.tbWorkOrderStatus  WITH CHECK ADD CONSTRAINT fktbWorkOrderStatusWorkOrderID FOREIGN KEY(WorkOrderID)
REFERENCES dbo.tbWorkOrder (WorkOrderID)
GO
ALTER TABLE dbo.tbWOComment  WITH CHECK ADD CONSTRAINT fktbtbWOCommentWorkOrderID FOREIGN KEY(WorkOrderID)
REFERENCES dbo.tbWorkOrder (WorkOrderID)