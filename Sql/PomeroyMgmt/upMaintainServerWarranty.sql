USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upMaintainServerWarranty') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upMaintainServerWarranty
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091209
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091209
-- ============================================================
CREATE PROCEDURE dbo.upMaintainServerWarranty
	@ServerID int
	,@ExpirationDate varchar(20)
	,@Provider varchar(20)
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	if (len(@ExpirationDate) = 0) set @ExpirationDate = null

	-- Add/update data
	if not exists(select 1 from dbo.tbServerWarranty where ServerID = @ServerID)
		-- Add record
		insert into dbo.tbServerWarranty (ServerID, ExpirationDate, Provider, CreateDateTime, UpdateDateTime)
		values (@ServerID, @ExpirationDate, @Provider, current_timestamp, current_timestamp)
	else
		-- Update record
		update dbo.tbServerWarranty set ExpirationDate = @ExpirationDate, Provider = @Provider, 
			UpdateDateTime = current_timestamp
		where ServerID = @ServerID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upMaintainServerWarranty - server warranty was NOT successfully maintained', 16, 1)

END
GO
grant exec on dbo.upMaintainServerWarranty to system