USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbServerDrive') AND type in (N'U'))
DROP TABLE dbo.tbServerDrive
GO
CREATE TABLE dbo.tbServerDrive(
	ServerDriveID int identity(1,1) not null,
	ServerID int not null,
	Drive char(1) not null,
	FreeMB bigint not null,
	PollDateTime datetime not null,
 CONSTRAINT PK_tbServerDrive PRIMARY KEY CLUSTERED 
(
	ServerDriveID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
