use PomeroyMgmt
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetAttachment') and type in (N'P', N'PC'))
drop procedure dbo.upGetAttachment
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20121220
-- ============================================================
create procedure dbo.upGetAttachment
	@TimeEntryAttachmentID int
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select FileName, BinaryContent
	from dbo.tbTimeEntryAttachment
	where TimeEntryAttachmentID = @TimeEntryAttachmentID and IsDeleted = 0

end
go
grant exec on dbo.upGetAttachment to system