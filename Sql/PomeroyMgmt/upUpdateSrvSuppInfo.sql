USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateSrvSuppInfo') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateSrvSuppInfo
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091110
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091110
-- ============================================================
CREATE PROCEDURE dbo.upUpdateSrvSuppInfo
	@ServerID int
	,@SupportType char(1)
	,@SupportBeginDate varchar(20)
	,@SupportEndDate varchar(20)
	,@Result char(1) = 'F' output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialiaze variables
	if (@SupportEndDate = '') set @SupportEndDate = '12/31/9999'

	-- Update server
	update tbServer set 
		SupportType = @SupportType
		,SupportBeginDate = @SupportBeginDate
		,SupportEndDate = @SupportEndDate
		,UpdateDateTime = current_timestamp
	where ServerID = @ServerID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upUpdateSrvSuppInfo - server support info was NOT successfully updated', 16, 1)

END
GO
grant exec on dbo.upUpdateSrvSuppInfo to system