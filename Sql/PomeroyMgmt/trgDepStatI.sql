USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trgDepStatI]'))
DROP TRIGGER [dbo].[trgDepStatI]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20101020
-- ============================================================
CREATE TRIGGER dbo.trgDepStatI
ON dbo.tbSrvDeploymentStatus
FOR INSERT
AS 
BEGIN
	-- Declarations
	declare @CurrID int

	set nocount on

  select @CurrID = MIN(SrvDeploymentStatusID) FROM inserted
  while (isnull(@CurrID, 0) <> 0)
	begin
		-- Test
		if exists(select 1 from inserted where DeploymentStatusID = 4 and SrvDeploymentStatusID = @CurrID)
			update dbo.tbServer set OS = null, OSType = 'None'
			from dbo.tbServer s
			inner join inserted i on s.ServerID = i.ServerID
			where i.SrvDeploymentStatusID = @CurrID
			
		-- Get next id
		select @CurrID = min(SrvDeploymentStatusID) from inserted where SrvDeploymentStatusID > @CurrID
	    
	end


END
GO
