USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbWOStatusCode') AND type in (N'U'))
DROP TABLE dbo.tbWOStatusCode
GO
CREATE TABLE dbo.tbWOStatusCode(
	WOStatusCode char(1) not null,
	Description varchar(50) not null,
	IsDeleted bit constraint dftbWOStatusCodeIsDeleted default 0,
 CONSTRAINT PK_tbWOStatusCode PRIMARY KEY CLUSTERED 
(
	WOStatusCode ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF