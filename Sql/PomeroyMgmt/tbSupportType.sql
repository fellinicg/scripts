USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbSupportType') AND type in (N'U'))
DROP TABLE dbo.tbSupportType
GO
CREATE TABLE dbo.tbSupportType(
	SupportType char(1) not null,
	Description varchar(50) not null,
	IsDeleted bit constraint dftbSupportTypeIsDeleted default 0,
 CONSTRAINT PK_tbSupportType PRIMARY KEY CLUSTERED 
(
	SupportType ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF