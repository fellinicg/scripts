USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetSAList') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetSAList
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100115
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100115
-- ============================================================
CREATE PROCEDURE dbo.upGetSAList
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select distinct UserAccountID, dbo.sfFullName(UserAccountID) 'SA'
	from dbo.tbUARoleServer
	where UARoleID = 2 and IsPrimary = 1
	order by SA
		
END
GO
grant exec on dbo.upGetSAList to system
