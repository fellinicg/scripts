USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbDatabase') AND type in (N'U'))
DROP TABLE dbo.tbDatabase
GO
CREATE TABLE dbo.tbDatabase(
	DatabaseID int identity(1,1) not null,
	DBName varchar(100) not null,
	ServerID int not null,
	SQLInstanceID tinyint null,
	DepartmentID smallint null,
	ClientContactID int null,
	IsOnline bit not null constraint dftbDatabaseIsOnline default 1,
	IsDeleted bit not null constraint dftbDatabaseIsDeleted default 0,
	IsSupported bit not null constraint dftbDatabaseIsSupported default 0,
	CreateDateTime datetime not null constraint dftbDatabaseCreateDateTime default (getdate()),
	UpdateDateTime datetime not null constraint dftbDatabaseUpdateDateTime default (getdate()),
CONSTRAINT PK_tbDatabase PRIMARY KEY CLUSTERED 
(
	DatabaseID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO 
alter table dbo.tbDatabase with check add constraint ucDBName unique (DBName, ServerID, SQLInstanceID)
GO
alter table dbo.tbDatabase with check add constraint fktbDatabaseServerID foreign key(ServerID)
references dbo.tbServer (ServerID)
GO
alter table dbo.tbDatabase with check add constraint fktbDatabaseSQLInstanceID foreign key(SQLInstanceID)
references dbo.tbSQLInstance (SQLInstanceID)
GO
alter table dbo.tbDatabase with check add constraint fktbDatabaseDepartmentID foreign key(DepartmentID)
references dbo.tbDepartment (DepartmentID)
GO
alter table dbo.tbDatabase with check add constraint fktbDatabaseClientContactID foreign key(ClientContactID)
references dbo.tbClientContact (ClientContactID)