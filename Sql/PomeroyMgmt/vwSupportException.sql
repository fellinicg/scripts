USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'dbo.vwSupportException'))
DROP VIEW dbo.vwSupportException
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091202
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091202
-- ============================================================
CREATE VIEW dbo.vwSupportException
AS
	select ServerID, SupportType, SupportBeginDate, SupportEndDate, 1 'IsSupported'
	from dbo.tbSupportException
	where current_timestamp between SupportBeginDate and SupportEndDate
