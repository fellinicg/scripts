USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbClientContact') AND type in (N'U'))
DROP TABLE dbo.tbClientContact
GO
CREATE TABLE dbo.tbClientContact(
	ClientContactID int identity(1,1) not null,
	CompanyID tinyint not null constraint dftbClientContactCompanyID]  default 1,
	FirstName varchar(50) not null,
	MiddleInitial char(1) null,
	LastName varchar(80) not null,
	Phone varchar(20) null,
	Email varchar(20) null,
	IsDeleted bit constraint dftbClientContactIsDeleted default 0,
	CreateDateTime datetime not null constraint dftbClientContactCreateDateTime default (getdate()),	
 CONSTRAINT PK_tbClientContact PRIMARY KEY CLUSTERED 
(
	ClientContactID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE dbo.tbClientContact  WITH CHECK ADD  CONSTRAINT fkCompanyID FOREIGN KEY(CompanyID)
REFERENCES dbo.tbCompany (CompanyID)
