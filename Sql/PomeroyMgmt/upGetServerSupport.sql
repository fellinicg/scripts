USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetServerSupport') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetServerSupport
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091227
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091227
-- ============================================================
CREATE PROCEDURE dbo.upGetServerSupport
	@ServerID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select s.SupportType, convert(varchar, s.SupportBeginDate, 101) 'SupportBeginDate',
	case year(s.SupportEndDate) when '9999' then '' else convert(varchar, s.SupportEndDate, 101) end 'SupportEndDate', t.Description
	from dbo.tbServer s
	left join dbo.tbSupportType t on s.SupportType = t.SupportType
	where s.ServerID = @ServerID

END
GO
grant exec on dbo.upGetServerSupport to system