USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetWOAssignedTo') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetWOAssignedTo
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100125
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100125
-- ============================================================
CREATE PROCEDURE dbo.upGetWOAssignedTo
	@WorkOrderID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select WorkOrderAssignedToID, UserAccountID, dbo.sfFullName(UserAccountID) 'AssignedTo'
	from dbo.tbWorkOrderAssignedTo
	where WorkOrderID = @WorkOrderID and IsDeleted = 0
	order by AssignedTo

END
GO
grant exec on dbo.upGetWOAssignedTo to system