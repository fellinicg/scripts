USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetPrimarySA') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetPrimarySA
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091204
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091204
-- ============================================================
CREATE PROCEDURE dbo.upGetPrimarySA
	@ServerID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select u.UserAccountID, dbo.sfFullName(u.UserAccountID) 'SA', n.ContactNum, e.EmailAddress
	from dbo.tbUARoleServer r
		inner join dbo.tbUserAccount u on r.UserAccountID = u.UserAccountID
		left join dbo.tbContactNum n on u.UserAccountID = n.UserAccountID and n.ContactNumType = 'WP'
		left join dbo.tbEmailAddress e on u.UserAccountID = e.UserAccountID and e.EmailAddressType = 'W'
	where r.ServerID = @ServerID and r.UARoleID = 2 and r.IsPrimary = 1

END
GO
grant exec on dbo.upGetPrimarySA to system