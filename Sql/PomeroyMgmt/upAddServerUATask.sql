USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddServerUATask') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddServerUATask
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091221
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 2009111221
-- ============================================================
CREATE PROCEDURE dbo.upAddServerUATask
	@ServerIDList	varchar(4000)
	,@UATaskID int
	,@Result char(1) = 'F' output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Insert new record into tbServerTask
	insert into dbo.tbServerUATask (ServerID, UATaskID)
	select ListItem, @UATaskID
	from dbo.tfCommaListToTable(@ServerIDList)

	-- Verify success
	if (@@error = 0)
		set @Result = 'S'
	else
		raiserror('upAddServerUATask - ServerTask was NOT successfully added', 16, 1)

END
GO
grant exec on dbo.upAddServerUATask to system