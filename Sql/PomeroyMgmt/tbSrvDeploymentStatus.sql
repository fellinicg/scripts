USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbSrvDeploymentStatus') AND type in (N'U'))
DROP TABLE dbo.tbSrvDeploymentStatus
GO
CREATE TABLE dbo.tbSrvDeploymentStatus(
	SrvDeploymentStatusID int identity(1,1) not null,
	ServerID int not null,
	DeploymentStatusID tinyint not null,
 	CreateDateTime datetime not null constraint dftbSDSCreateDateTime default (getdate()),
CONSTRAINT PK_tbSrvDeploymentStatus PRIMARY KEY CLUSTERED 
(
	SrvDeploymentStatusID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbSrvDeploymentStatus with check add constraint fktbSrvDeploymentStatusServerID
foreign key(ServerID) references dbo.tbServer (ServerID)
GO
alter table dbo.tbSrvDeploymentStatus with check add constraint fktbSrvDeploymentStatusDeploymentStatusID
foreign key(DeploymentStatusID) references dbo.tbDeploymentStatus (DeploymentStatusID)