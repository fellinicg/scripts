USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tfDelimListToTable') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.tfDelimListToTable
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100208
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100208
-- ============================================================
CREATE FUNCTION dbo.tfDelimListToTable (@DelimitedList varchar(8000), @Delimiter varchar(5))
RETURNS @ResultTable table (ListItem varchar(1000) NOT NULL)
AS
BEGIN

	-- Declarations
	declare
		@TokenLen int,
		@Token varchar(200),
		@Remainder varchar(8000)

	-- Initialize variables
	set @Remainder = @DelimitedList

	while len(@Remainder) > 0
	begin
		set @TokenLen = case 
										when patindex('%' + @Delimiter + '%', @Remainder) > 0 then patindex('%' + @Delimiter + '%', @Remainder) - 1
										else len(@Remainder) end
		set @Token = left(@Remainder, @TokenLen)
		insert @ResultTable(ListItem) values(@Token)
		set @Remainder = substring(@Remainder, @TokenLen + len(@Delimiter) + 1, 8000)
	end

	return

END