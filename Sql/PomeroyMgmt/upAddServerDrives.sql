USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddServerDrives') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddServerDrives
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100708
-- ============================================================
CREATE PROCEDURE dbo.upAddServerDrives
AS
BEGIN

	-- Declarations
	declare @Servers table (ServerID int, ServerName varchar(50))
	declare 
		@ServerId int,
		@ServerName varchar(50),
		@rc char(1),
		@local bit,
		@cmd varchar(max)

	-- Initialize settings
	set nocount on

	-- Initialize variables

	-- Insert a list of servers into table var
	insert into @Servers
	select a.ServerID, a.ServerName
	from dbo.tbServer a 
	left join dbo.vwSrvDeploymentStatus d on a.ServerID = d.ServerID
	where a.SupportType in ('F', 'D') and a.OnPawanet = 1 and a.IsOnline = 1
		 and d.DeploymentStatusID in (1,2,5)
		 
	-- Truncate tbDBFiles
	truncate table dbo.tbServerDrive

	-- Loop over servers
	while exists(select 1 from @Servers)
		begin
			select top 1 @ServerID = ServerID, @ServerName = ServerName from @Servers
			-- Check if server is local
			if @@servername = @ServerName
				set @local = 1
			else
				set @local = 0
			-- Check if the server is a linked server, if not, create it
			if (select dbo.sfLinkedSrvExists(@ServerName)) is null
				exec dbo.upAddLinkedServer @ServerName, @rc output
			else set @rc = 'S'
			if @rc = 'S'
				begin
					-- Test connection to remote sql server
					if @local = 0 
						exec dbo.upLinkedSrvAvail @ServerName, @rc output
					if @rc = 'S'
						begin
							-- Populate tbServerDrive
							set @cmd = 'use [master]
							if (object_id(''''PM_DriveInfo'''') is not null)
							drop table PM_DriveInfo
							create table PM_DriveInfo (Drive char(1),MBFree bigint,PollDate datetime default (getdate()))
							insert into PM_DriveInfo (Drive, MBFree)
							exec xp_fixeddrives'

							if @Local = 0
							begin
								exec('exec [' + @ServerName + '].master.dbo.sp_sqlexec ''' + @cmd + '''')

								insert into dbo.tbServerDrive (ServerID, Drive, FreeMB, PollDateTime)
								exec ('select ' + @ServerID + ', Drive, MBFree, PollDate from openquery([' + @ServerName + '], 
											''select * from master.dbo.PM_DriveInfo'')')
							end
							else
							begin
								set @cmd = replace(@cmd, '''''', '''')
								exec master.dbo.sp_sqlexec @cmd

								insert into dbo.tbServerDrive (ServerID, Drive, FreeMB, PollDateTime)
								exec ('select ' + @ServerID + ', Drive, MBFree, PollDate from master.dbo.PM_DriveInfo')
							end
						end
				end
			delete from @Servers where ServerID = @ServerId
		end

END
GO
grant exec on dbo.upAddServerDrives to system