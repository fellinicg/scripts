USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upMaintainSAInfo') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upMaintainSAInfo
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091209
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091209
-- ============================================================
CREATE PROCEDURE dbo.upMaintainSAInfo
	@UserAccountID int
	,@ContactNum varchar(20)
	,@EmailAddress varchar(120)
	,@Result char(1) output
AS
BEGIN

	-- Declarations
	declare @id int

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Add/update contact number
	if not exists(select 1 from dbo.tbContactNum where UserAccountID = @UserAccountID and ContactNumType = 'WP')
	begin
		-- Add record
		execute dbo.upAddContactNum @UserAccountID, 'WP', @ContactNum, @id OUTPUT
		if (@id is null) goto err
	end
	else 
		-- Update record
		update dbo.tbContactNum set ContactNum = @ContactNum, UpdateDateTime = current_timestamp
		where UserAccountID = @UserAccountID and ContactNumType = 'WP'

	-- Add/update email address
	if not exists(select 1 from dbo.tbEmailAddress where UserAccountID = @UserAccountID and EmailAddressType = 'W')
	begin
		-- Add record
		execute dbo.upAddEmailAddress @UserAccountID, 'W', @EmailAddress, @id OUTPUT
		if (@id is null) goto err
	end
	else
		-- Update record
		update dbo.tbEmailAddress set EmailAddress = @EmailAddress,	UpdateDateTime = current_timestamp
		where UserAccountID = @UserAccountID and EmailAddressType = 'W'

	-- Verify success
	if (@@error = 0)
	begin
		set @Result = 'S'
		return
	end

err: raiserror('upMaintainSAInfo - SA info was NOT successfully maintained', 16, 1)

END
GO
grant exec on dbo.upMaintainSAInfo to system