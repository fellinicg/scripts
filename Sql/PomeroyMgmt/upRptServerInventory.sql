USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upRptServerInventory') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upRptServerInventory
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100113
-- ============================================================
CREATE PROCEDURE dbo.upRptServerInventory
	@ServerType char(1)
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		ServerName
		,ds.Description 'DeploymentStatus'
		,s.OSType
		,s.MajorApps
		,s.Manufacturer
		,s.Model
		,s.SerialNumber
		,case when isnull(w.ExpirationDate, '1/1/2000') < current_timestamp then 'No' else 'Yes' end 'Warranty'
		,convert(varchar, w.ExpirationDate, 101) 'ExpirationDate'
	from dbo.tbServer s
		left join dbo.vwSrvDeploymentStatus ds on s.ServerID = ds.ServerID
		left join dbo.tbServerWarranty w on s.ServerID = w.ServerID
	where s.IsDeleted = 0 and s.SupportType <> 'D' 
		and ds.DeploymentStatusID <> 3 and s.ServerType like @ServerType
	order by s.ServerName

END
GO
grant exec on dbo.upRptServerInventory to system