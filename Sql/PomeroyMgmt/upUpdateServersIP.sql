USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateServersIP') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateServersIP
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091109
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091109
-- ============================================================
CREATE PROCEDURE dbo.upUpdateServersIP
AS
BEGIN

	-- Declarations
	declare @Servers table (ServerID int, ServerName varchar(50))
	declare 
		@ServerId int,
		@ServerName varchar(50)

	-- Initialize settings
	set nocount on

	-- Insert a list of servers into table var
	insert into @Servers
	select a.ServerID, a.ServerName
	from dbo.tbServer a
	where OnPawanet = 1 and IsDeleted = 0

	-- Loop over servers
	while exists(select 1 from @Servers)
		begin
			select top 1 @ServerID = ServerID, @ServerName = ServerName from @Servers
			update tbServer set IPAddress = dbo.ufGetServerIP(@ServerName) where ServerID = @ServerID
			delete from @Servers where ServerId = @ServerId
		end

END
