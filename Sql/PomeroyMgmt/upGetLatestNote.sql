USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetLatestNote') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetLatestNote
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100201
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100201
-- ============================================================
CREATE PROCEDURE dbo.upGetLatestNote
	@SourceID int
	,@NoteType char(1)
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select Note, Author, convert(varchar, CreateDateTime, 101) 'NoteDateStamp'
	from dbo.vwLatestNotes
	where SourceID = @SourceID and NoteType = @NoteType

END
GO
grant exec on dbo.upGetLatestNote to system