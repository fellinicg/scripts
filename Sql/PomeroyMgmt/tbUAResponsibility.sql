USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbUAResponsibility') AND type in (N'U'))
DROP TABLE dbo.tbUAResponsibility
GO
CREATE TABLE dbo.tbUAResponsibility(
	UserAccountID int not null,
	Tasks varchar(max) null,
	Projects varchar(max) null,
	CreateDateTime datetime not null constraint dftbUAResponsibilityCreateDateTime default (getdate()),
	UpdateDateTime datetime not null constraint dftbUAResponsibilityUpdateDateTime default (getdate()),
CONSTRAINT PK_tbUAResponsibility PRIMARY KEY CLUSTERED 
(
	UserAccountID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbUAResponsibility with check add constraint fktbUAResponsibilityUserAccountID
foreign key(UserAccountID) references dbo.tbUserAccount (UserAccountID)
