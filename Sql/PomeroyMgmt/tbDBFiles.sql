USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbDBFiles') AND type in (N'U'))
DROP TABLE dbo.tbDBFiles
GO
CREATE TABLE dbo.tbDBFiles(
	DBFilesID int identity(1,1) not null,
	DatabaseID int not null,
	LogicalFileName varchar(500) not null,
	PhysicalFileName varchar(1000) not null,
	Status varchar(100) not null,
	FileSizeMB varchar(20) not null,
	Updateability varchar(100) not null,
	RecoveryMode varchar(100) not null,
	FreeSpaceMB varchar(20) not null,
	FreeSpacePct varchar(7) not null,
	MaxSize varchar(50) not null,
	Growth varchar(50) not null,
	PollDateTime datetime not null,
 CONSTRAINT PK_tbDBFiles PRIMARY KEY CLUSTERED 
(
	DBFilesID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbDBFiles with check add constraint fktbDBFilesDatabaseID foreign key(DatabaseID)
references dbo.tbDatabase (DatabaseID)
