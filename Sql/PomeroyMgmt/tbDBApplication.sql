USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbDBApplication') AND type in (N'U'))
DROP TABLE dbo.tbDBApplication
GO
CREATE TABLE dbo.tbDBApplication(
	DBApplicationID smallint identity(1,1) not null,
	DatabaseID int not null,
	ApplicationID smallint not null,
	IsDeleted bit not null constraint dftbDBApplicationIsDeleted default 0,
	CreateDateTime datetime not null constraint dftbDBApplicationCreateDateTime default (getdate()),	
	UpdateDateTime datetime not null constraint dftbDBApplicationUpdateDateTime default (getdate()),
 CONSTRAINT PK_tbDBApplication PRIMARY KEY CLUSTERED 
(
	DBApplicationID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbDBApplication add constraint ucDBApplication unique (DatabaseID, ApplicationID)
GO
alter table dbo.tbDBApplication with check add constraint fktbDBApplicationDatabaseID foreign key(DatabaseID)
references dbo.tbDatabase (DatabaseID)
GO
alter table dbo.tbDBApplication with check add constraint fktbDBApplicationApplicationID foreign key(ApplicationID)
references dbo.tbApplication (ApplicationID)
