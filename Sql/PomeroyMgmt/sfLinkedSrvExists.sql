USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091106
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091106
-- ============================================================
CREATE FUNCTION dbo.sfLinkedSrvExists (@Server varchar(50))
RETURNS int 
AS
BEGIN

	RETURN (select 1 from master.sys.servers where data_source = @Server)

END