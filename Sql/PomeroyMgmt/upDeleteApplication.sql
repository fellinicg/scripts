USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upDeleteApplication') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upDeleteApplication
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091229
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091229
-- ============================================================
CREATE PROCEDURE dbo.upDeleteApplication
	@ApplicationID int
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Delete record
  update dbo.tbApplication set IsDeleted = 1
  where ApplicationID = @ApplicationID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upDeleteApplication - Application was NOT successfully Deleted.', 16, 1)

END
GO
grant exec on dbo.upDeleteApplication to system