USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.sfWODoc') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.sfWODoc
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100514
-- ============================================================
CREATE FUNCTION dbo.sfWODoc (@WorkOrderID int, @DelimChar char(1))
	RETURNS varchar(max) AS
BEGIN

	-- Declarations
	declare @Doc varchar(max), @idx int
	declare @tbl table (idx int identity(1,1), doc varchar(200))
	
	-- Initialize variables
	set @Doc = ''
	if (len(dbo.sfTrim(@DelimChar))) = 0 set @DelimChar = ','
	
	-- Get data
	insert into @tbl (doc)
	select WorkOrderDoc
	from dbo.tbWorkOrderDoc
	where WorkOrderID = @WorkOrderID

	-- Loop over table and comma delimit
	while exists(select 1 from @tbl)
	begin
		select top 1 @idx = idx, @Doc = @Doc + doc + @DelimChar from @tbl
		delete from @tbl where idx = @idx
	end
	
	return case when len(@Doc) = 0 then '' else left(@Doc, len(@Doc)-1) end
	
END
