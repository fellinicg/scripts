USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'dbo.vwServerInfo'))
DROP VIEW dbo.vwServerInfo
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100114
-- ============================================================
CREATE VIEW dbo.vwServerInfo
AS
	select
		s.ServerID
		,s.ServerName
		,s.ServerType
		,dbo.sfServerType(s.ServerType) 'ServerTypeDesc'
		,s.SerialNumber
		,s.Manufacturer
		,s.Model
		,s.ProcessorInfo
		,s.Processors
		,s.MemoryBytes
		,s.OS
		,s.OSVer
		,s.OSServicePack
		,s.OSType
		,s.OSProductID
		,s.MajorApps
		,s.SupportType
		,st.Description 'SupportTypeDesc'
		,s.OnPawanet
		,s.IsOnline
		,s.SupportBeginDate
		,s.SupportEndDate
		,sa.UserAccountID 'SA_UAID'
		,dbo.sfFirstName(sa.UserAccountID) 'SA_FirstName'
		,dbo.sfLastName(sa.UserAccountID) 'SA_LastName'
		,dbo.sfFullName(sa.UserAccountID) 'SA'
		,dba.UserAccountID 'DBA_UAID'
		,dbo.sfFirstName(dba.UserAccountID) 'DBA_FirstName'
		,dbo.sfLastName(dba.UserAccountID) 'DBA_LastName'
		,dbo.sfFullName(dba.UserAccountID) 'DBA'
		,ds.DeploymentStatusID 'DeploymentStatusID'
		,ds.Description 'DeploymentStatus'
		,w.ExpirationDate
		,w.Provider
		,o.OrderDate
		,o.OrderNumber
		,s.IsDeleted
	from dbo.tbServer s
		left join dbo.tbUARoleServer sa on s.ServerID = sa.ServerID and sa.UARoleID = 2 and sa.IsPrimary = 1
		left join dbo.tbUARoleServer dba on s.ServerID = dba.ServerID and dba.UARoleID = 1 and dba.IsPrimary = 1
		left join dbo.tbSupportType st on s.SupportType = st.SupportType
		left join dbo.vwSrvDeploymentStatus ds on s.ServerID = ds.ServerID
		left join dbo.tbServerWarranty w on s.ServerID = w.ServerID
		left join dbo.tbServerOrder o on s.ServerID = o.ServerID
