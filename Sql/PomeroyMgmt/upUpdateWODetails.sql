USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateWODetails') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateWODetails
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100127
-- ============================================================
CREATE PROCEDURE dbo.upUpdateWODetails
	@WorkOrderID int
	,@Title varchar(300)
	,@Description text
	,@DepartmentID int
	,@ClientProject varchar(50)
	,@ServiceOrder varchar(200)
	,@WOStatusCode char(1)
	,@UserAccountID int
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Add work order status
	exec upAddWorkOrderStatus @WorkOrderID, @WOStatusCode, @UserAccountID, @Result output
	if (@@error <> 0)
		return

	-- Update Work Order
	update dbo.tbWorkOrder set Title = @Title, Description = @Description, DepartmentID = @DepartmentID,
		ClientProject = @ClientProject, ServiceOrder = @ServiceOrder, UpdateDateTime = current_timestamp
	where WorkOrderID = @WorkOrderID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upUpdateWODetails - work order details were NOT successfully updated', 16, 1)

END
GO
grant exec on dbo.upUpdateWODetails to system