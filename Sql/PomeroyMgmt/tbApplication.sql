USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbApplication') AND type in (N'U'))
DROP TABLE dbo.tbApplication
GO
CREATE TABLE dbo.tbApplication(
	ApplicationID smallint identity(1,1) not null,
	Description varchar(100) not null,
	IsDeleted bit constraint dftbApplicationIsDeleted default 0,
 CONSTRAINT PK_tbApplication PRIMARY KEY CLUSTERED 
(
	ApplicationID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
