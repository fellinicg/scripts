USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateContactNum') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateContactNum
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091209
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091209
-- ============================================================
CREATE PROCEDURE dbo.upUpdateContactNum
	@ContactNumID int
	,@UserAccountID int
	,@ContactNumType char(2)
	,@ContactNum varchar(20)
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Update DepartmentID
	update dbo.tbContactNum set ContactNumType = @ContactNumType,
		ContactNum = @ContactNum, UpdateDateTime = current_timestamp
	where ContactNumID = @ContactNumID and UserAccountID = @UserAccountID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upUpdateContactNum - contact number was NOT successfully updated', 16, 1)

END
GO
grant exec on dbo.upUpdateContactNum to system