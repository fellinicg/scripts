USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upRptServerList') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upRptServerList
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100104
-- ============================================================
CREATE PROCEDURE dbo.upRptServerList
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		s.ServerName
		,s.SA
		,s.ServerTypeDesc 'ServerType'
		,s.SerialNumber
		,s.Manufacturer
		,s.Model
		,s.ProcessorInfo
		,s.Processors
		,s.MemoryBytes
		,s.OS
		,s.OSVer
		,s.OSServicePack
		,s.OSType
		,case when s.OSType <> 'Windows' then 'N/A' else s.OSProductID end 'OSProductID'
		,s.SupportType
		,convert(varchar, s.SupportBeginDate, 101) 'SupportBeginDate'
		,case s.OnPawanet when 1 then 'Yes' else 'No' end 'OnPawanet'
		,case s.IsOnline when 1 then 'Yes' else 'No' end 'IsOnline'
		,s.DeploymentStatus
		,s.MajorApps
		,case when isnull(s.ExpirationDate, '1/1/2000') < current_timestamp then 'No' else 'Yes' end 'Warranty'
		,convert(varchar, s.ExpirationDate, 101) 'ExpirationDate'
		,s.Provider
		,convert(varchar, s.OrderDate, 101) 'OrderDate'
		,s.OrderNumber
	from dbo.vwServerInfo s
	where s.IsDeleted = 0 and s.SupportType <> 'D' and s.DeploymentStatusID <> 3
	order by s.ServerName

END
GO
grant exec on dbo.upRptServerList to system