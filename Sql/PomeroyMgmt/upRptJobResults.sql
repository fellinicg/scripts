use PomeroyMgmt
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upRptJobResults') and type in (N'P', N'PC'))
drop procedure dbo.upRptJobResults
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20110712
-- ============================================================
create procedure dbo.upRptJobResults
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		j.ServerID
		,isnull(si.SQLInstanceName, s.ServerName) 'ServerName'
		,j.Job
		,j.Message
		,case j.Status 
			when 0 then 'Failed'
			when 1 then 'Succeeded'
			when 2 then 'Retry'
			when 3 then 'Canceled'
		 end 'Status'
		,convert(varchar, j.RunDateTime, 100) 'RunDateTime'
	from dbo.tbJobResults j
		left join tbServer s on j.ServerID = s.ServerID
		left join dbo.tbSQLInstance si on j.ServerID = si.ServerID and j.SQLInstanceID = si.SQLInstanceID
	where j.RunDateTime > dateadd(d, -5, current_timestamp)
	order by s.ServerName, j.Job, j.RunDateTime desc
	
end
go
grant exec on dbo.upRptJobResults to system