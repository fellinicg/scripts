USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddTask') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddTask
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091119
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091119
-- ============================================================
CREATE PROCEDURE dbo.upAddTask
	@Description	varchar(50)
	,@TaskID int output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Insert new record into tbTask
	insert into dbo.tbTask (
		Description,
		CreateDateTime, 
		UpdateDateTime)
	values (
		@Description,
		current_timestamp,
		current_timestamp)

	-- Verify success
	if (@@rowcount = 1 and @@identity > 0)
		set @TaskID = @@identity
	else
		raiserror('upAddTask - task was NOT successfully added', 16, 1)

END
GO
grant exec on dbo.upAddTask to system