USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetServerList') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetServerList
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091110
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091110
-- ============================================================
CREATE PROCEDURE dbo.upGetServerList
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return a list of servers
	select ServerID, ServerName
	from dbo.tbServer 
	where OnPawanet = 1 and IsDeleted = 0

END
GO
grant exec on dbo.upGetServerList to system