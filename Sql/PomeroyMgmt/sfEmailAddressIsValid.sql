USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.sfEmailAddressIsValid') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.sfEmailAddressIsValid
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091111
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091111
-- ============================================================
GO
CREATE FUNCTION dbo.sfEmailAddressIsValid (@EmailAddress varchar(120))
returnS bit
AS
BEGIN
	declare
		@IsValid bit
  
  if (
     -- No embedded spaces
     charindex(' ',ltrim(rtrim(@EmailAddress))) = 0
     -- '@' can't be the first character of an email address
     AND LEFT(ltrim(@EmailAddress),1) <> '@'
     -- '.' can't be the last character of an email address
     AND RIGHT(rtrim(@EmailAddress),1) <> '.'
     -- There must be a '.' after '@'
     AND charindex('.',@EmailAddress,charindex('@',@EmailAddress)) - charindex('@',@EmailAddress) > 1
     -- Only one '@' sign is allowed
     AND LEN(ltrim(rtrim(@EmailAddress))) - LEN(REPLACE(ltrim(rtrim(@EmailAddress)),'@','')) = 1
     -- Domain name should end with at least 2 character extension
     AND charindex('.',REVERSE(ltrim(rtrim(@EmailAddress)))) >= 3
     -- can't have patterns like '.@' and '..'
     AND (charindex('.@',@EmailAddress) = 0 AND charindex('..',@EmailAddress) = 0)
     )
    set @IsValid = 1
  else
    set @IsValid = 0

  return(@IsValid)

END
