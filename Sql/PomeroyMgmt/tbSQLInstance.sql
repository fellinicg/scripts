USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbSQLInstance') AND type in (N'U'))
DROP TABLE dbo.tbSQLInstance
GO
CREATE TABLE dbo.tbSQLInstance(
	SQLInstanceID tinyint identity(1,1) not null,
	ServerID int not null,
	SQLInstanceName varchar(50) not null,
 CONSTRAINT PK_tbSQLInstance PRIMARY KEY CLUSTERED 
(
	SQLInstanceID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbSQLInstance with check add constraint fktbSQLInstanceServerID
	foreign key(ServerID) references dbo.tbServer (ServerID)
GO
alter table dbo.tbSQLInstance with check add constraint ucSQLInstanceName unique (SQLInstanceName)
