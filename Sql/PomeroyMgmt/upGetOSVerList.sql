USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetOSVerList') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetOSVerList
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100108
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100108
-- ============================================================
CREATE PROCEDURE dbo.upGetOSVerList
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select distinct OSVer 'Description'
	from dbo.tbServer
	where dbo.sfTrim(isnull(OSVer, '')) <> ''
	order by OSVer

END
GO
grant exec on dbo.upGetOSVerList to system