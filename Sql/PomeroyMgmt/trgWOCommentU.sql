USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trgWOCommentU]'))
DROP TRIGGER [dbo].[trgWOCommentU]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100506
-- ============================================================
CREATE TRIGGER dbo.trgWOCommentU
ON dbo.tbWOComment
AFTER UPDATE
AS 
BEGIN
	-- Declarations
	declare @CurrID int

	set nocount on

  select @CurrID = MIN(WOCommentID) FROM inserted
  while (isnull(@CurrID, 0) <> 0)
	begin
		-- Audit
		insert into dbo.tbWOCommentAudit(WOCommentID, WorkOrderID, WOComment, OrigAuthor, 
			UpdatedBy)
		select del.WOCommentID, del.WorkOrderID, del.WOComment, del.UserAccountID, ins.UserAccountID
		from deleted del, inserted ins
		where del.WOCommentID = @CurrID and ins.WOCommentID = @CurrID
			
		-- Get next id
		select @CurrID = min(WOCommentID) from inserted where WOCommentID > @CurrID
	    
	end


END
GO
