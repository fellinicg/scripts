USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetEmailAddresses') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetEmailAddresses
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091214
-- ============================================================
CREATE PROCEDURE dbo.upGetEmailAddresses
	@UserAccountID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select e.EmailAddressID, e.EmailAddress, t.Description
	from dbo.tbEmailAddress e
	left join dbo.tfEmailAddressType() t on e.EmailAddressType = t.EmailAddressType
	where e.UserAccountID = @UserAccountID and IsDeleted = 0
	order by e.EmailAddress

END
GO
grant exec on dbo.upGetEmailAddresses to system