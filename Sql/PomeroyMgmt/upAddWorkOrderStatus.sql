USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddWorkOrderStatus') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddWorkOrderStatus
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 2010127
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 2010127
-- ============================================================
CREATE PROCEDURE dbo.upAddWorkOrderStatus
	@WorkOrderID int
	,@WOStatusCode char(1)
	,@UserAccountID int
	,@Result char(1) output	
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Insert data
	if not exists (select 1 from dbo.vwWorkOrderStatus where WorkOrderID = @WorkOrderID and WOStatusCode = @WOStatusCode)
	begin
		insert into dbo.tbWorkOrderStatus (WorkOrderID, WOStatusCode, UserAccountID)
		values (@WorkOrderID, @WOStatusCode, @UserAccountID)

		-- Verify success
		if (@@rowcount = 1 and @@identity > 0)
			set @Result = 'S'
		else
			raiserror('upAddWorkOrderStatus - Work Order status was NOT successfully added.', 16, 1)
	end
	else set @Result = 'S'

END
GO
grant exec on dbo.upAddWorkOrderStatus to system