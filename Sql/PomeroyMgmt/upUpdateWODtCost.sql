USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateWODtCost') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateWODtCost
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100201
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100201
-- ============================================================
CREATE PROCEDURE dbo.upUpdateWODtCost
	@WorkOrderID int
	,@DesiredCompletionDate smalldatetime
	,@CompletionDate smalldatetime
	,@RequestDate smalldatetime
	,@IsBillable bit
	,@EstCost money
	,@FixedCost money
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Update Work Order Dates
	update dbo.tbWorkOrder set DesiredCompletionDate = @DesiredCompletionDate, CompletionDate = @CompletionDate, 
		RequestDate = @RequestDate, IsBillable = @IsBillable, EstCost = @EstCost, 
		FixedCost = @FixedCost, UpdateDateTime = current_timestamp
	where WorkOrderID = @WorkOrderID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upUpdateWODtCost - work order date/costs were NOT successfully updated', 16, 1)

END
GO
grant exec on dbo.upUpdateWODtCost to system