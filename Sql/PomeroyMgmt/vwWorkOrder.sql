USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'dbo.vwWorkOrder'))
DROP VIEW dbo.vwWorkOrder
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100514
-- ============================================================
CREATE VIEW dbo.vwWorkOrder
AS
	select
		w.WorkOrderID
		,'WOR' + convert(varchar, datepart(yy, w.ReceivedDate)) + '-' + dbo.sfZeroPad(w.WorkOrderID, 3) 'WorkOrder'
		,w.Title
		,w.Description
		,d.Description 'Department'
		,w.ServiceOrder
		,w.ClientProject
		,convert(varchar, w.ReceivedDate, 101) 'ReceivedDate'
		,convert(varchar, w.DesiredCompletionDate, 101) 'DesiredCompletionDate'
		,convert(varchar, w.CompletionDate, 101) 'CompletionDate'
		,convert(varchar, w.RequestDate, 101) 'RequestDate'
		,w.IsBillable
		,w.EstCost
		,w.FixedCost
		,dbo.sfWOAssignedTo(w.WorkOrderID, ',') 'AssignedTo'
		,dbo.sfWORequestor(w.WorkOrderID, ',') 'Requestor'
		,dbo.sfWODoc(w.WorkOrderID, '|') 'Doc'
		,c.WOComment
		,c.Author
		,convert(varchar, c.CreateDateTime, 101) 'CommentDateStamp'
		,s.Description 'WoStatus'
		,s.WOStatusCode
	from dbo.tbWorkOrder w
		left join dbo.vwLatestWOComments c on w.WorkOrderID = c.WorkOrderID
		left join dbo.vwWorkOrderStatus s on w.WorkOrderID = s.WorkOrderID
		left join dbo.tbDepartment d on w.DepartmentID = d.DepartmentID
