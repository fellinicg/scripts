USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddWODoc') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddWODoc
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100128
-- ============================================================
CREATE PROCEDURE dbo.upAddWODoc
	@WorkOrderID int
	,@WorkOrderDoc varchar(200)
	,@WorkOrderDocID int output
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Add record
	insert into dbo.tbWorkOrderDoc (WorkOrderID, WorkOrderDoc)
	values (@WorkOrderID, @WorkOrderDoc)
	
	-- Verify success
	if (@@rowcount = 1 and @@identity > 0)
	begin
		set @Result = 'S'
		set @WorkOrderDocID = @@identity
	end
	else
		raiserror('upAddWODoc - work order document was NOT successfully added', 16, 1)
	
END
GO
grant exec on dbo.upAddWODoc to system