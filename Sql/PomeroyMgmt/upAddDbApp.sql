USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddDbApp') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddDbApp
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091207
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091207
-- ============================================================
CREATE PROCEDURE dbo.upAddDbApp
	@DatabaseID int
	,@ApplicationID int
	,@Result char(1) output	
AS
BEGIN

	-- Declarations

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Set all other apps for DB to deleted
	update dbo.tbDBApplication set IsDeleted = 1
	where DatabaseID = @DatabaseID
	
	-- Insert data
	if (@ApplicationID > 0)
		insert into dbo.tbDBApplication (DatabaseID, ApplicationID)
		values (@DatabaseID, @ApplicationID)
		
	-- Verify success
	if (@@error = 0)
		set @Result = 'S'

END
GO
grant exec on dbo.upAddDbApp to system