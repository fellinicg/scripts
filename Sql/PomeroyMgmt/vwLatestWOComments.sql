USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'dbo.vwLatestWOComments'))
DROP VIEW dbo.vwLatestWOComments
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100510
-- ============================================================
CREATE VIEW dbo.vwLatestWOComments
AS
	select a.WOCommentID, a.WorkOrderID, a.WOComment, isnull(dbo.sfFullName(a.UserAccountID), 'System') 'Author',
		a.CreateDateTime
	from dbo.tbWOComment a
		inner join (select distinct WorkOrderID, max(CreateDateTime) 'CreateDateTime' 
								from dbo.tbWOComment where IsDeleted = 0 group by WorkOrderID) b 
			on a.WorkOrderID = b.WorkOrderID and a.CreateDateTime = b.CreateDateTime and a.IsDeleted = 0