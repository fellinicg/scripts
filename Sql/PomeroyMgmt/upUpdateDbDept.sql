USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateDbDept') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateDbDept
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091208
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091208
-- ============================================================
CREATE PROCEDURE dbo.upUpdateDbDept
	@DatabaseID int
	,@DepartmentID smallint
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Update DepartmentID
	update dbo.tbDatabase set DepartmentID = @DepartmentID
	where DatabaseID = @DatabaseID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upUpdateDbDept - database department was NOT successfully updated', 16, 1)

END
GO
grant exec on dbo.upUpdateDbDept to system