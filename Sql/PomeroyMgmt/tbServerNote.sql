USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbServerNote') AND type in (N'U'))
DROP TABLE dbo.tbServerNote
GO
CREATE TABLE dbo.tbServerNote(
	ServerNoteID int identity(1,1) not null,
	ServerID int not null,
	Note varchar(200) not null,
	Author varchar(20) not null,
	IsDeleted bit not null constraint dftbServerNoteIsDeleted default 0,
	CreateDateTime datetime not null constraint dftbServerNoteCreateDateTime default (getdate()),
 CONSTRAINT PK_tbServerNote PRIMARY KEY CLUSTERED 
(
	ServerNoteID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF