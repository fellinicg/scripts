USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetWORequestor') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetWORequestor
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100125
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100125
-- ============================================================
CREATE PROCEDURE dbo.upGetWORequestor
	@WorkOrderID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select WorkOrderRequestorID, ClientContactID, dbo.sfClientFullName(ClientContactID) 'Requestor'
	from dbo.tbWorkOrderRequestor
	where WorkOrderID = @WorkOrderID and IsDeleted = 0
	order by Requestor
	
END
GO
grant exec on dbo.upGetWORequestor to system