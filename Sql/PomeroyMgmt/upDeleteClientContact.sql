USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upDeleteClientContact') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upDeleteClientContact
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091228
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091228
-- ============================================================
CREATE PROCEDURE dbo.upDeleteClientContact
	@ClientContactID int
	,@Result char(1) output	
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Update record
	update dbo.tbClientContact set IsDeleted = 1
	where ClientContactID = @ClientContactID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upDeleteClientContact - client contact was NOT successfully Deleted', 16, 1)

END
GO
grant exec on dbo.upDeleteClientContact to system