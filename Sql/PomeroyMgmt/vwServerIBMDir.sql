USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'dbo.vwSrvDeploymentStatus'))
DROP VIEW dbo.vwServerIBMDir
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091231
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091231
-- ============================================================
CREATE VIEW dbo.vwServerIBMDir
AS
	select a.Idx, a.ServerID, a.IBMDirectorID, a.UpdFromIBMDir, a.IBMDirDateCreated
	from dbo.tbServerIBMDir a
	inner join (select ServerID, max(IBMDirectorID) 'IBMDirectorID'
							from dbo.tbServerIBMDir
							group by ServerID) b on a.ServerID = b.ServerID and a.IBMDirectorID = b.IBMDirectorID
