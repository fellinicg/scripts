USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbUserAccount') AND type in (N'U'))
DROP TABLE dbo.tbUserAccount
GO
CREATE TABLE dbo.tbUserAccount(
	UserAccountID int identity(1,1) not null,
	WindowsUser varchar(50) not null,
	FirstName varchar(50) null,
	LastName varchar(80) null,
	AccountStatus char(1) not null,
	CreateDateTime datetime not null constraint dftbUserAccountCreateDateTime default (getdate()),	
 CONSTRAINT PK_tbUserAccount PRIMARY KEY CLUSTERED 
(
	UserAccountID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
ALTER TABLE dbo.tbUserAccount ADD CONSTRAINT ucWinUser UNIQUE (WindowsUser)
