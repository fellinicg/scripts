USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tfEmailAddressType') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.tfEmailAddressType
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091214
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091214
-- ============================================================
CREATE FUNCTION dbo.tfEmailAddressType ()
RETURNS @ResultTable table (EmailAddressType char(2),
														Description varchar(20))
AS
BEGIN

	insert into @ResultTable
	select 'H', 'Home'
	union
	select 'W', 'Work'

	return

END