USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddIP') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddIP
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091130
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091130
-- ============================================================
CREATE PROCEDURE dbo.upAddIP
	@ServerID int
	,@IPAddress varchar(64)
	,@Result char(1) output
AS
BEGIN

	-- Declarations

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Add record
  insert into dbo.tbServerIP (ServerID, IPAddress)
	values (@ServerID, @IPAddress)

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'

END
GO
grant exec on dbo.upAddIP to system