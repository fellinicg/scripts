USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.sfWOAssignedTo') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.sfWOAssignedTo
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100122
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100122
-- ============================================================
CREATE FUNCTION dbo.sfWOAssignedTo (@WorkOrderID int, @DelimChar char(1))
	RETURNS varchar(2000) AS
BEGIN

	-- Declarations
	declare @AssignedTo varchar(2000), @idx int
	declare @tbl table (idx int identity(1,1), uaid int)
	
	-- Initialize variables
	set @AssignedTo = ''
	if (len(dbo.sfTrim(@DelimChar))) = 0 set @DelimChar = ','
	
	-- Get data
	insert into @tbl (uaid)
	select UserAccountID
	from dbo.tbWorkOrderAssignedTo
	where WorkOrderID = @WorkOrderID and IsDeleted = 0

	-- Loop over table and comma delimit
	while exists(select 1 from @tbl)
	begin
		select top 1 @idx = idx, @AssignedTo = @AssignedTo + dbo.sfFullName(uaid) + @DelimChar from @tbl
		delete from @tbl where idx = @idx
	end
	
	return case when len(@AssignedTo) = 0 then '' else left(@AssignedTo, len(@AssignedTo)-1) end
	
END
