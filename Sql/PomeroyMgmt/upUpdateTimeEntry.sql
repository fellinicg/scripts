USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateTimeEntry') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateTimeEntry
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20121116
-- ============================================================
CREATE PROCEDURE dbo.upUpdateTimeEntry
	@TimeEntryID int
	,@TimeTypeID tinyint
	,@Info varchar(500)
	,@Hours float
	,@StartTime varchar(10)
	,@EndTime varchar(10)
	,@IsDeleted bit
	,@Result char(1) output	
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Update data
	if @IsDeleted = 1
		update dbo.tbTimeEntry set IsDeleted = @IsDeleted, UpdateDateTime = current_timestamp
		where TimeEntryID = @TimeEntryID
	else
		update dbo.tbTimeEntry set TimeTypeID = @TimeTypeID, Info = @Info, Hours = @Hours, StartTime = @StartTime,
			EndTime = @EndTime, UpdateDateTime = current_timestamp
		where TimeEntryID = @TimeEntryID	

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upUpdateTimeEntry - Time entry was NOT successfully updated.', 16, 1)

END
GO
grant exec on dbo.upUpdateTimeEntry to system