USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddWOComment') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddWOComment
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100506
-- ============================================================
CREATE PROCEDURE dbo.upAddWOComment
	@WorkOrderID int
	,@WOComment varchar(3000)
	,@UserAccountID int
	,@CreateDateTime smalldatetime = null
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	set @CreateDateTime = isnull(@CreateDateTime, current_timestamp)

	-- Add record
  insert into dbo.tbWOComment (WorkOrderID, WOComment, UserAccountID, CreateDateTime, UpdateDateTime)
  values (@WorkOrderID, @WOComment, @UserAccountID, @CreateDateTime, @CreateDateTime)

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upAddWOComment - Comment was NOT successfully added.', 16, 1)

END
GO
grant exec on dbo.upAddWOComment to system