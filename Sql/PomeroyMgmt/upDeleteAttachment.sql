use PomeroyMgmt
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upDeleteAttachment') and type in (N'P', N'PC'))
drop procedure dbo.upDeleteAttachment
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20121219
-- ============================================================
create procedure dbo.upDeleteAttachment
	@TimeEntryAttachmentID int
	,@Result char(1) output
as
begin

	-- Initialize settings
	set nocount on

	-- Delete record
  update dbo.tbTimeEntryAttachment set IsDeleted = 1, UpdateDateTime = current_timestamp
  where TimeEntryAttachmentID = @TimeEntryAttachmentID	

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upDeleteAttachment - Attachment was NOT successfully deleted.', 16, 1)

end
go
grant exec on dbo.upDeleteAttachment to system