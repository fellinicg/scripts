USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.sfWORequestor') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.sfWORequestor
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100122
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100122
-- ============================================================
CREATE FUNCTION dbo.sfWORequestor (@WorkOrderID int, @DelimChar char(1))
	RETURNS varchar(2000) AS
BEGIN

	-- Declarations
	declare @Requestor varchar(2000), @idx int
	declare @tbl table (idx int identity(1,1), ccid int)
	
	-- Initialize variables
	set @Requestor = ''
	if (len(dbo.sfTrim(@DelimChar))) = 0 set @DelimChar = ','
	
	-- Get data
	insert into @tbl (ccid)
	select ClientContactID
	from dbo.tbWorkOrderRequestor
	where WorkOrderID = @WorkOrderID and IsDeleted = 0

	-- Loop over table and comma delimit
	while exists(select 1 from @tbl)
	begin
		select top 1 @idx = idx, @Requestor = @Requestor + dbo.sfClientFullName(ccid) + @DelimChar from @tbl
		delete from @tbl where idx = @idx
	end
	
	return  case when len(@Requestor) = 0 then '' else left(@Requestor, len(@Requestor)-1) end
	
END
