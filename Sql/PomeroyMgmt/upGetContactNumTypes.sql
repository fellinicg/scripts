USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetContactNumTypes') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetContactNums
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100423
-- ============================================================
CREATE PROCEDURE dbo.upGetContactNumTypes
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select ContactNumType, Description
	from dbo.tfContactNumType()
	order by Description

END
GO
grant exec on dbo.upGetContactNumTypes to system