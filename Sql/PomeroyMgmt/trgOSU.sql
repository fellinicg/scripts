USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trgOSU]'))
DROP TRIGGER [dbo].[trgOSU]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20101020
-- ============================================================
CREATE TRIGGER dbo.trgOSU
ON dbo.tbServer
AFTER UPDATE
AS 
BEGIN
	-- Declarations
	declare @CurrID int

	set nocount on

  select @CurrID = MIN(ServerID) FROM inserted
  while (isnull(@CurrID, 0) <> 0)
	begin
		-- Update
		update dbo.tbServer set OSType = case when charindex('microsoft', os) > 0 then 'Windows'
																				when charindex('novell', os) > 0 then 'Novell'
																				when charindex('dos', os) > 0 then 'DOS'
																				when charindex('esx', os) > 0 then 'ESX'
																				else OSType 
																		 end
		where ServerID = @CurrID
			
		-- Get next id
		select @CurrID = min(ServerID) from inserted where ServerID > @CurrID
	    
	end


END
GO
