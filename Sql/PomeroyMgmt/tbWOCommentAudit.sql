USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbWOCommentAudit') AND type in (N'U'))
DROP TABLE dbo.tbWOCommentAudit
GO
CREATE TABLE dbo.tbWOCommentAudit(
	WOCommentAuditID int identity(1,1) not null,
	WOCommentID int not null,
	WorkOrderID int not null,
	WOComment varchar(3000) not null,
	OrigAuthor int not null,
	UpdatedBy int not null,
	CreateDateTime datetime not null constraint dftbWOCommentAuditCreateDateTime default (getdate()),
 CONSTRAINT PK_tbWOCommentAudit PRIMARY KEY CLUSTERED 
(
	WOCommentAuditID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbWOCommentAudit with check add constraint fktbtbWOCommentAuditWOCommentID
foreign key(WOCommentID) references dbo.tbWOComment (WOCommentID)
GO
alter table dbo.tbWOCommentAudit with check add constraint fktbtbWOCommentAuditWorkOrderID
foreign key(WorkOrderID) references dbo.tbWorkOrder (WorkOrderID)
