USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upMaintainLSL') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upMaintainLSL
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100519
-- ============================================================
CREATE PROCEDURE dbo.upMaintainLSL 
	@Action bit = 0
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @srvs table (sname varchar (100))
	declare @srv varchar(100)

	-- Store list of linked servers
	insert into @srvs
	select name from master.sys.servers
	
	-- Loop of list
	while exists(select 1 from @srvs)
	begin
		select @srv = sname from @srvs
		if (@Action = 1)
			exec sp_addlinkedsrvlogin @rmtsrvname = @srv, @useself = 'true', @locallogin = 'panynj\tfellini'
		else
			exec sp_droplinkedsrvlogin @rmtsrvname = @srv, @locallogin = 'panynj\tfellini'
		delete from @srvs where sname = @srv
	end

END
