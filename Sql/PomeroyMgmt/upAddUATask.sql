USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddUATask') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddUATask
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091217
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091217
-- ============================================================
CREATE PROCEDURE dbo.upAddUATask
	@UserAccountID int
	,@TaskID int
	,@LogPath	varchar(500)
	,@UATaskID int output
	,@Result char(1) output	
AS
BEGIN

	-- Declarations
	declare @Log varchar(20)

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	set @Log = '-' + convert(varchar, datepart(hh, current_timestamp)) +
		convert(varchar, datepart(mi, current_timestamp)) +
		convert(varchar, datepart(ss, current_timestamp)) + '.log'
	if not (right(@LogPath, 1) = '\') set @LogPath = @LogPath + '\'

	-- Begin transaction
	begin tran

	-- Insert new record into tbUATask
	insert into dbo.tbUATask (UserAccountID, TaskID, CreateDateTime)
	values (@UserAccountID, @TaskID, current_timestamp)

	-- Verify success
	if (@@rowcount = 1 and @@identity > 0)
		set @UATaskID = @@identity
	else
	begin
		rollback tran
		raiserror('upAddUATask - user task was NOT successfully added', 16, 1)
		return
	end

	-- Assign logfile
	update dbo.tbUATask set LogFile = @LogPath + convert(varchar, @UATaskID) + @Log
	where UATaskID = @UATaskID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
	begin
		set @Result = 'S'
		commit tran
	end
	else
	begin
		rollback tran
		raiserror('upAddUATask - user task was NOT successfully added', 16, 1)
	end

END
GO
grant exec on dbo.upAddUATask to system