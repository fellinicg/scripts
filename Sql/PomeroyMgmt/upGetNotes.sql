USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetNotes') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetNotes
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091117
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091117
-- ============================================================
CREATE PROCEDURE dbo.upGetNotes
	@SourceID int
	,@NoteType char(1)
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return a list of servers
	select SourceID, NoteType, Note, case UserAccountID when 0 then 'System' else dbo.sfFullName(UserAccountID) end 'Author', 
	convert(varchar, CreateDateTime, 100) AddedOn
	from dbo.tbNote
	where SourceID = @SourceID and NoteType = @NoteType
	order by CreateDateTime desc

END
GO
grant exec on dbo.upGetNotes to system