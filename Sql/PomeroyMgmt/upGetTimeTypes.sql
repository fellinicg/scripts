USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetTimeTypes') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetTimeTypes
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20121115
-- ============================================================
CREATE PROCEDURE dbo.upGetTimeTypes
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select TimeTypeID, Description, HasInfo, HasTime
	from dbo.tbTimeType
	where IsDeleted = 0
	order by Description

END
GO
grant exec on dbo.upGetTimeTypes to system