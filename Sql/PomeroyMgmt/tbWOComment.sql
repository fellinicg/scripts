USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fktbtbWOCommentAuditWOCommentID]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbWOCommentAudit]'))
ALTER TABLE [dbo].[tbWOCommentAudit] DROP CONSTRAINT [fktbtbWOCommentAuditWOCommentID]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbWOComment') AND type in (N'U'))
DROP TABLE dbo.tbWOComment
GO
CREATE TABLE dbo.tbWOComment(
	WOCommentID int identity(1,1) not null,
	WorkOrderID int not null,
	WOComment varchar(3000) not null,
	UserAccountID int not null,
	IsDeleted bit not null constraint dftbWOCommentIsDeleted default (0),
	CreateDateTime datetime not null constraint dftbWOCommentCreateDateTime default (getdate()),
	UpdateDateTime datetime not null constraint dftbWOCommentUpdateDateTime default (getdate()),
 CONSTRAINT PK_tbWOComment PRIMARY KEY CLUSTERED 
(
	WOCommentID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbWOComment with check add constraint fktbtbWOCommentWorkOrderID
foreign key(WorkOrderID) references dbo.tbWorkOrder (WorkOrderID)
GO
alter table dbo.tbWOCommentAudit with check add constraint fktbtbWOCommentAuditWOCommentID 
foreign key(WOCommentID) references dbo.tbWOComment (WOCommentID)