USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upDeleteUATask') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upDeleteUATask
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091217
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091217
-- ============================================================
CREATE PROCEDURE dbo.upDeleteUATask
	@UATaskID int
	,@Result char(1) output	
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Update record
	update dbo.tbUATask set IsDeleted = 1
	where UATaskID = @UATaskID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upDeleteUATask - user task was NOT successfully deleted', 16, 1)

END
GO
grant exec on dbo.upDeleteUATask to system