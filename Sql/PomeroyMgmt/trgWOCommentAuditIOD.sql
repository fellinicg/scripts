USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trgWOCommentAuditIOD]'))
DROP TRIGGER [dbo].[trgWOCommentAuditIOD]
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100506
-- ============================================================
CREATE TRIGGER dbo.trgWOCommentAuditIOD
ON dbo.tbWOCommentAudit
INSTEAD OF DELETE
AS 
BEGIN
	set nocount on
	raiserror ('Deletions not allowed from this table.', 16, 1)
END
GO
