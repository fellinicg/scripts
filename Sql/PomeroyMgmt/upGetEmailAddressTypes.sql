USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetEmailAddressTypes') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetEmailAddresses
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100423
-- ============================================================
CREATE PROCEDURE dbo.upGetEmailAddressTypes
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select EmailAddressType, Description
	from dbo.tfEmailAddressType()
	order by Description

END
GO
grant exec on dbo.upGetEmailAddressTypes to system