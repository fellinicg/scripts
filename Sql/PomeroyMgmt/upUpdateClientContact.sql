USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateClientContact') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateClientContact
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091228
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091228
-- ============================================================
CREATE PROCEDURE dbo.upUpdateClientContact
	@ClientContactID int
	,@FirstName varchar(50)
	,@MiddleInitial char(1)
	,@LastName varchar(80)
	,@Phone	varchar(20)
	,@Email	varchar(20)
	,@Result char(1) output	
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	if (len(dbo.sfTrim(@MiddleInitial)) = 0) set @MiddleInitial = null

	-- Insert new record into tbClientContact
	update dbo.tbClientContact set FirstName = @FirstName, MiddleInitial = upper(@MiddleInitial),
		LastName = @LastName,	Phone = @Phone, Email = @Email
	where ClientContactID = @ClientContactID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upUpdateClientContact - client contact was NOT successfully updated', 16, 1)

END
GO
grant exec on dbo.upUpdateClientContact to system