USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateServerDBPlatform') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateServerDBPlatform
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091105
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091105
-- ============================================================
CREATE PROCEDURE dbo.upUpdateServerDBPlatform
	@ServerID int
	,@Result char(1) output
AS
BEGIN

	-- Declarations
	declare @Server table (ServerID int, ProdVer varchar(20))
	declare @SQLInstanceName varchar(50)

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	select @SQLInstanceName = isnull(b.SQLInstanceName, a.ServerName)
	from dbo.tbServer a
	left join dbo.tbSQLInstance b on a.ServerID = b.ServerID
	where a.ServerID = @ServerID

	-- Retrieve servers SQL Server Product Version
	begin try
		insert into @Server (ServerID, ProdVer)
		exec ('select ' + @ServerID + ', convert(varchar, ver) from openquery([' + @SQLInstanceName + '], ''select serverproperty(''''ProductVersion'''') ver'')')
	end try
	begin catch
		declare @ErrorMessage nvarchar(4000);
		declare @ErrorSeverity int;
		declare @ErrorState int;

		select @ErrorMessage = ERROR_MESSAGE(),
					 @ErrorSeverity = ERROR_SEVERITY(),
					 @ErrorState = ERROR_STATE();

		if error_number() <> 2627
			begin
				raiserror (@ErrorMessage, 
									 @ErrorSeverity, 
									 @ErrorState -- 
									 )
				return
			end
	end catch

	-- Update tbServer
	update dbo.tbServer set DBPlatformID = a.DBPlatformID, UpdateDateTime = current_timestamp
	from dbo.tbDBPlatform a
	inner join @Server b on a.ProductVer = b.ProdVer
	inner join dbo.tbServer c on c.ServerID = b.ServerID

	-- Verify success
	if @@rowcount > 0
		set @Result = 'S'
	else
		raiserror('upUpdateServerDBPlatform - server was NOT successfully updated', 16, 1)

END
GO
grant exec on dbo.upUpdateServerDBPlatform to system