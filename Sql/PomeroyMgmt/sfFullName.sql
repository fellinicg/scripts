USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.sfFullName') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.sfFullName
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091117
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091117
-- ============================================================
CREATE FUNCTION dbo.sfFullName (@UserAccountID int)
	RETURNS varchar(200) AS
BEGIN

	-- Get data
	return(select isnull(FirstName + ' ' + LastName, WindowsUser)
	from dbo.tbUserAccount
	where UserAccountID = @UserAccountID)

END
