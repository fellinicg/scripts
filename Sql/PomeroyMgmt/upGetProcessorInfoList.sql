USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetProcessorInfoList') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetProcessorInfoList
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100108
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100108
-- ============================================================
CREATE PROCEDURE dbo.upGetProcessorInfoList
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select distinct ProcessorInfo 'Description'
	from dbo.tbServer
	where dbo.sfTrim(isnull(ProcessorInfo, '')) <> ''
	order by ProcessorInfo

END
GO
grant exec on dbo.upGetProcessorInfoList to system