USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetUATasks') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetUATasks
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091217
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091217
-- ============================================================
CREATE PROCEDURE dbo.upGetUATasks
	@UserAccountID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select ut.UATaskID, ut.UserAccountID, ut.TaskID, ut.LogFile, ut.TaskStatus, s.Status,
		convert(varchar, ut.CreateDateTime, 101) 'CreateDateTime', t.Description, t.StoredProc, t.JSFunc, t.Type
	from dbo.tbUATask ut
	left join dbo.tbTask t on ut.TaskID = t.TaskID
	left join dbo.vwTaskStatus s on ut.TaskStatus = s.StatusCode
	where ut.UserAccountID = @UserAccountID and ut.IsDeleted = 0
	order by ut.CreateDateTime desc

END
GO
grant exec on dbo.upGetUATasks to system