USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetClientContacts') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetClientContacts
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091207
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091207
-- ============================================================
CREATE PROCEDURE dbo.upGetClientContacts
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select ClientContactID, dbo.sfClientFullName(ClientContactID) 'FullName', FirstName, MiddleInitial, 
	LastName, Phone, Email
	from dbo.tbClientContact
	where IsDeleted = 0
	order by LastName

END
GO
grant exec on dbo.upGetClientContacts to system