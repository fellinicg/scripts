USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetUAResponsibilities') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetUAResponsibilities
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100418
-- ============================================================
CREATE PROCEDURE dbo.upGetUAResponsibilities
	@UserAccountID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select Tasks, Projects
	from dbo.tbUAResponsibility
	where UserAccountID = @UserAccountID

END
GO
grant exec on dbo.upGetUAResponsibilities to system