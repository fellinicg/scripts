USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddReport') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddReport
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100104
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100104
-- ============================================================
CREATE PROCEDURE dbo.upAddReport
	@ReportName varchar(50)
	,@ReportType char(1)
	,@Page varchar(20)
	,@ReportID int output
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Add record
  insert into dbo.tbReport (ReportName, ReportType, Page)
  values (@ReportName, @ReportType, @Page)

	-- Verify success
	if (@@rowcount = 1 and @@identity > 0)
	begin
		set @ReportID = @@identity
		set @Result = 'S'
	end
	else
		raiserror('upAddReport - Report was NOT successfully added.', 16, 1)

END
GO
grant exec on dbo.upAddReport to system