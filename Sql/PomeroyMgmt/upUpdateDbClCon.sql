USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateDbClCon') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateDbClCon
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091208
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091208
-- ============================================================
CREATE PROCEDURE dbo.upUpdateDbClCon
	@DatabaseID int
	,@ClientContactID int
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Update DepartmentID
	update dbo.tbDatabase set ClientContactID = @ClientContactID
	where DatabaseID = @DatabaseID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upUpdateDbClCon - Database client contact was NOT successfully updated', 16, 1)

END
GO
grant exec on dbo.upUpdateDbClCon to system