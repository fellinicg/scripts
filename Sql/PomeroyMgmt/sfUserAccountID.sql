USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.sfUserAccountID') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.sfUserAccountID
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091112
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091112
-- ============================================================
CREATE FUNCTION dbo.sfUserAccountID (@WindowsUser varchar(100))
RETURNS int
AS
BEGIN
	-- Declarations
	declare @UserAccountID int
	
	-- Initialize variables
	set @UserAccountID = 0
  
	-- Retrieve UserAccountID
	select @UserAccountID = UserAccountID from dbo.tbUserAccount where WindowsUser = @WindowsUser

	-- Return UserAccountID if WindowsUser exists, else return 0
	return(@UserAccountID)

END
