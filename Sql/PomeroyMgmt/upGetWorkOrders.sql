USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetWorkOrders') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetWorkOrders
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100122
-- ============================================================
CREATE PROCEDURE dbo.upGetWorkOrders
	@WOStatusCode char(1)
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	if len(dbo.sfTrim(@WOStatusCode)) = 0 set @WOStatusCode = '%'

	-- Return a list of WorkOrders
	select
		WorkOrderID
		,WorkOrder
		,Title
		,Description
		,ReceivedDate
		,DesiredCompletionDate
		,IsBillable
		,WOComment
		,Author
		,CommentDateStamp
		,AssignedTo
		,Requestor
		,WoStatus
	from dbo.vwWorkOrder
	where WOStatusCode like (@WOStatusCode)
	order by WorkOrderID desc

END
GO
grant exec on dbo.upGetWorkOrders to system