USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbDatabaseStatusHistory') AND type in (N'U'))
DROP TABLE dbo.tbDatabaseStatusHistory
GO
CREATE TABLE dbo.tbDatabaseStatusHistory(
	DatabaseStatusHistoryID int identity(1,1) not null,
	DatabaseID int not null,
	StatusDateTime datetime not null constraint dftbDatabaseStatusHistoryStatusDateTime default (getdate()),
	Status varchar(128) null
CONSTRAINT PK_tbDatabaseStatusHistory PRIMARY KEY CLUSTERED 
(
	DatabaseStatusHistoryID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO 
alter table dbo.tbDatabaseStatusHistory with check add constraint fktbDatabaseStatusHistoryDatabaseID foreign key(DatabaseID)
references dbo.tbDatabase (DatabaseID)
