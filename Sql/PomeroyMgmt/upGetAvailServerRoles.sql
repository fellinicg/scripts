USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetAvailServerRoles') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetAvailServerRoles
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100422
-- ============================================================
CREATE PROCEDURE dbo.upGetAvailServerRoles
	@ServerID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select r.RoleID, r.Description
	from dbo.tbRole r
	left join dbo.tbServerRole sr on r.RoleID = sr.RoleID and ServerID = @ServerID
	where sr.RoleID is null
	order by r.Description

END
GO
grant exec on dbo.upGetAvailServerRoles to system