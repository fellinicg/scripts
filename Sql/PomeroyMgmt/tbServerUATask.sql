USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbServerUATask') AND type in (N'U'))
DROP TABLE dbo.tbServerUATask
GO
CREATE TABLE dbo.tbServerUATask(
	ServerUATaskID int identity(1,1) not null,
	ServerID int not null,
	UATaskID int not null,
	Success bit not null constraint dftbServerUATaskSuccess default 0,
CONSTRAINT PK_tbServerUATask PRIMARY KEY CLUSTERED 
(
	ServerUATaskID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbServerUATask with check add constraint fktbServerUATaskServerID
foreign key(ServerID) references dbo.tbServer (ServerID)
GO
alter table dbo.tbServerUATask with check add constraint fktbServerUATaskUATaskID
foreign key(UATaskID) references dbo.tbUATask (UATaskID)