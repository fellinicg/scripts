USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetContactNums') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetContactNums
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091214
-- ============================================================
CREATE PROCEDURE dbo.upGetContactNums
	@UserAccountID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select ContactNumID, ContactNum, t.Description
	from dbo.tbContactNum c
	left join dbo.tfContactNumType() t on c.ContactNumType = t.ContactNumType
	where UserAccountID = @UserAccountID and IsDeleted = 0
	order by ContactNum

END
GO
grant exec on dbo.upGetContactNums to system