USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upSetSrvTaskSuccess') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upSetSrvTaskSuccess
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100115
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100115
-- ============================================================
CREATE PROCEDURE dbo.upSetSrvTaskSuccess
	@ServerUATaskID int
	,@Success bit
	,@Result char(1) output	
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Delete user role
	update dbo.tbServerUATask set Success = @Success
	where ServerUATaskID = @ServerUATaskID
	
	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upSetSrvTaskSuccess - Success flag was NOT successfully updated', 16, 1)

END
GO
grant exec on dbo.upSetSrvTaskSuccess to system