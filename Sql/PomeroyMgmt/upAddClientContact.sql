USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddClientContact') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddClientContact
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091228
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091228
-- ============================================================
CREATE PROCEDURE dbo.upAddClientContact
	@FirstName varchar(50)
	,@MiddleInitial char(1)
	,@LastName varchar(80)
	,@Phone	varchar(20)
	,@Email	varchar(20)
	,@ClientContactID int output
	,@Result char(1) output	
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'X'

	-- Verify a contact doesn't already exist with that name
	if exists(select 1 from dbo.tbClientContact where FirstName = dbo.sfTrim(@FirstName) 
		and MiddleInitial = @MiddleInitial and LastName = dbo.sfTrim(@LastName)) return
	
	-- Reinitialize variables
	set @Result = 'F'

	-- Insert new record into tbClientContact
	insert into dbo.tbClientContact (FirstName, MiddleInitial, LastName, Phone, Email, CreateDateTime)
	values (@FirstName, upper(@MiddleInitial), @LastName, @Phone, @Email, current_timestamp)

	-- Verify success
	if (@@rowcount = 1 and @@identity > 0)
	begin
		set @Result = 'S'
		set @ClientContactID = @@identity
	end
	else
		raiserror('upAddClientContact - client contact was NOT successfully added', 16, 1)

END
GO
grant exec on dbo.upAddClientContact to system