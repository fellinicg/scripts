USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateServer') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateServer
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091110
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091110
-- ============================================================
CREATE PROCEDURE dbo.upUpdateServer
	@ServerID int
	,@Manufacturer varchar(50)
	,@Model varchar(50)
	,@ProcessorInfo varchar(50)
	,@Processors tinyint
	,@MemoryBytes	bigint
	,@OS varchar(200)
	,@OSVer varchar(20)
	,@OSServicePack varchar(5)
	,@Result char(1) = 'F' output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Update server
	update tbServer set 
		Manufacturer = @Manufacturer
		,Model = @Model
		,ProcessorInfo = @ProcessorInfo
		,Processors = @Processors
		,MemoryBytes = @MemoryBytes
		,OS = @OS
		,OSVer = @OSVer
		,OSServicePack = @OSServicePack
		,UpdateDateTime = current_timestamp
	where ServerID = @ServerID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upUpdateServer - server was NOT successfully updated', 16, 1)

END
GO
grant exec on dbo.upUpdateServer to system