USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upSetDbIsSupported') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upSetDbIsSupported
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091208
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091208
-- ============================================================
CREATE PROCEDURE dbo.upSetDbIsSupported
	@DatabaseID int
	,@IsSupported bit
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Update DepartmentID
	update dbo.tbDatabase set IsSupported = @IsSupported
	where DatabaseID = @DatabaseID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'

END
GO
grant exec on dbo.upSetDbIsSupported to system