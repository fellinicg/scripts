use PomeroyMgmt
go
set ansi_nulls on
go
set quoted_identifier on
go
set ansi_padding on
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbTimeEntryAttachment') and type in (N'U'))
drop table dbo.tbTimeEntryAttachment
go
create table dbo.tbTimeEntryAttachment(
	TimeEntryAttachmentID int identity(1,1) not null,
	TimeEntryID int,
	FileName varchar (500) not null,
	BinaryContent varbinary(max) not null,
	IsDeleted bit constraint dftbTimeEntryAttachmentIsDeleted default 0,
	CreateDateTime datetime not null constraint dftbTimeEntryAttachmentCreateDateTime default (getdate()),
	UpdateDateTime datetime not null constraint dftbTimeEntryAttachmentUpdateDateTime default (getdate()),
 constraint PK_tbTimeEntryAttachment primary key clustered 
(
	TimeEntryAttachmentID asc
)with (pad_index  = off, statistics_norecompute  = off, ignore_dup_key = off, allow_row_locks  = on, allow_page_locks  = on) on [primary]
) on [primary]

go
set ansi_padding off
go
alter table dbo.tbTimeEntryAttachment with check add constraint fktbTimeEntryAttachmentTimeEntryID
foreign key(TimeEntryID) references dbo.tbTimeEntry (TimeEntryID)