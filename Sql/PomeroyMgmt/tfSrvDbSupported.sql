USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tfSrvDbSupported') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.tfSrvDbSupported
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091202
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091202
-- ============================================================
CREATE FUNCTION dbo.tfSrvDbSupported ()
RETURNS @ResultTable table (ServerID int,
														DBSupported int)
AS
BEGIN

	insert into @ResultTable
	select distinct s.ServerID, IsSupported
	from dbo.tbServer s
	inner join dbo.tbDatabase d on s.ServerID = d.ServerID and IsSupported = 1

	return

END