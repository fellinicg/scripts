USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetManufacturerList') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetManufacturerList
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100108
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100108
-- ============================================================
CREATE PROCEDURE dbo.upGetManufacturerList
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select distinct Manufacturer 'Description'
	from dbo.tbServer
	where dbo.sfTrim(isnull(Manufacturer, '')) <> ''
	order by Manufacturer

END
GO
grant exec on dbo.upGetManufacturerList to system