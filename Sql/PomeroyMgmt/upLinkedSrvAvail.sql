USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upLinkedSrvAvail') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upLinkedSrvAvail
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100111
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100111
-- ============================================================
CREATE PROCEDURE dbo.upLinkedSrvAvail 
	@Server nvarchar(50)
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on
	--exec sp_serveroption @Server, 'connect timeout', 3

	-- Initialize variables
	set @Result = 'S'

	begin try 
			exec sp_testlinkedserver @Server
	end try 
	begin catch 
			set @Result = 'F'
	end catch 

	-- Reset timeout to default
	--exec sp_serveroption @Server, 'connect timeout', 30

END