USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tfSrvDeploymentStatus') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.tfSrvDeploymentStatus
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091111
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091111
-- ============================================================
CREATE FUNCTION dbo.tfSrvDeploymentStatus ()
RETURNS @ResultTable table (SrvDeploymentStatusID int, ServerID int,
														DeploymentStatusID int, Description varchar(50),
														CreateDateTime datetime)
AS
BEGIN

	insert into @ResultTable
	select a.SrvDeploymentStatusID, a.ServerID, c.DeploymentStatusID, c.Description, a.CreateDateTime
	from dbo.tbSrvDeploymentStatus a
	inner join (select ds.ServerID, max(ds.SrvDeploymentStatusID) 'SrvDeploymentStatusID'
							from dbo.tbSrvDeploymentStatus ds
							group by ds.ServerID) b on a.SrvDeploymentStatusID = b.SrvDeploymentStatusID
	left join dbo.tbDeploymentStatus c on a.DeploymentStatusID = c.DeploymentStatusID

	return

END