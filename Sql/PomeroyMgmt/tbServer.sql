USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbServer') AND type in (N'U'))
DROP TABLE dbo.tbServer
GO
CREATE TABLE dbo.tbServer(
	ServerID int identity(1,1) not null,
	ServerName varchar(50) not null,
	ServerType char(1) not null,
	SerialNumber varchar(60) null,
	Manufacturer varchar(50) null,
	ProcessorInfo varchar(50) null,
	Processors tinyint null,
	MemoryBytes bigint null,
	OS varchar(200) null,
	OSVer varchar(20) null,
	OSServicePack varchar(20) null,
	OSType varchar(20) null,
	MajorApps varchar(100) null,
	SupportType char(1) not null constraint dftbServerSupportType default 'S'
	OnPawanet bit not null constraint dftbServerOnPawanet default 0,
	IsOnline bit not null constraint dftbServerIsOnline default 1,
	SupportBeginDate datetime not null,
	SupportEndDate datetime not null constraint dftbServerSupportEndDate default ('12/31/9999'),
	CreateDateTime datetime not null constraint dftbServerCreateDateTime default (getdate()),
	UpdateDateTime datetime not null constraint dftbServerUpdateDateTime default (getdate()),
 CONSTRAINT PK_tbServer PRIMARY KEY CLUSTERED 
(
	ServerID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbServer with check add constraint tcServerType check ((ServerType='P' OR ServerType='V'))
GO
alter table dbo.tbServer with check add constraint ucServerName unique (ServerName)
