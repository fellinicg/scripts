USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetAvailWOAssignedTo') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetAvailWOAssignedTo
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100127
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100127
-- ============================================================
CREATE PROCEDURE dbo.upGetAvailWOAssignedTo
	@WorkOrderID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select u.UserAccountID, dbo.sfFullName(u.UserAccountID) 'Available'
	from dbo.tbUserAccount u
	left join dbo.tbWorkOrderAssignedTo a on u.UserAccountID = a.UserAccountID 
		and a.WorkOrderID = @WorkOrderID and a.IsDeleted = 0
	where u.AccountStatus = 'A' and a.WorkOrderAssignedToID is null
	order by Available

END
GO
grant exec on dbo.upGetAvailWOAssignedTo to system