USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbDatabaseNote') AND type in (N'U'))
DROP TABLE dbo.tbDatabaseNote
GO
CREATE TABLE dbo.tbDatabaseNote(
	DatabaseNoteID int identity(1,1) not null,
	DatabaseID int not null,
	Note varchar(200) not null,
	Author varchar(20) not null,
	IsDeleted bit not null constraint dftbDatabaseNoteIsDeleted default 0,
	CreateDateTime datetime not null constraint dftbDatabaseNoteCreateDateTime default (getdate()),
 CONSTRAINT PK_tbDatabaseNote PRIMARY KEY CLUSTERED 
(
	DatabaseNoteID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF