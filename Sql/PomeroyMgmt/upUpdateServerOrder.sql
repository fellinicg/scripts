USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateServerOrder') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateServerOrder
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091204
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091204
-- ============================================================
CREATE PROCEDURE dbo.upUpdateServerOrder
	@ServerID int
	,@OrderDate datetime
	,@OrderNumber varchar(20)
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Return data
	update dbo.tbServerOrder set OrderDate = @OrderDate
		,OrderNumber = @OrderNumber
	where ServerID = @ServerID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upUpdateServerOrder - server order was NOT successfully updated', 16, 1)

END
GO
grant exec on dbo.upUpdateServerOrder to system