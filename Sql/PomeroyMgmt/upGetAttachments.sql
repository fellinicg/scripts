use PomeroyMgmt
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = object_id(N'dbo.upGetAttachments') and type in (N'P', N'PC'))
drop procedure dbo.upGetAttachments
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20121212
-- ============================================================
create procedure dbo.upGetAttachments
	@TimeEntryID int
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select TimeEntryAttachmentID, FileName
	from dbo.tbTimeEntryAttachment
	where TimeEntryID = @TimeEntryID and IsDeleted = 0
	order by CreateDateTime	

end
go
grant exec on dbo.upGetAttachments to system