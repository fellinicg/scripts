USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetServers') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetServers
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091116
-- ============================================================
CREATE PROCEDURE dbo.upGetServers
	@Return char(1)
	,@UserAccountID int = null
	,@Search varchar(50) = null
	,@DeploymentStatusID tinyint = 0
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	if len(dbo.sfTrim(@Return)) = 0 
		set @Return = 'A'

	-- Return a list of servers
	if (@Return = 'A')
		select s.ServerID, s.ServerName, s.OnPawanet, s.IsOnline, 
			dbo.sfNumNotes(s.ServerID, 'S') 'NumSrvNotes', dbo.sfFullName(sa.UserAccountID) 'SA',
			dbo.sfFullName(dba.UserAccountID) 'DBA', sa.UserAccountID 'sa_id', dba.UserAccountID 'dba_id',
			ds.DeploymentStatusID, ds.Description 'DeploymentStatus', s.SupportType, s.OSType, s.MajorApps,
			case SupportType when 'F' then 1 when 'D' then 1 else 0 end 'DBSupported'
		from dbo.tbServer s
		left join dbo.tbUARoleServer sa on s.ServerID = sa.ServerID and sa.UARoleID = 2 and sa.IsPrimary = 1
		left join dbo.tbUARoleServer dba on s.ServerID = dba.ServerID and dba.UARoleID = 1 and dba.IsPrimary = 1
		left join dbo.vwSrvDeploymentStatus ds on s.ServerID = ds.ServerID
		where ds.DeploymentStatusID <> @DeploymentStatusID
			and s.IsDeleted = 0
		order by s.ServerName

	if (@Return = 'U')
		select distinct s.ServerID, s.ServerName, s.OnPawanet, s.IsOnline, 
			dbo.sfNumNotes(s.ServerID, 'S') 'NumSrvNotes', dbo.sfFullName(sa.UserAccountID) 'SA',
			dbo.sfFullName(dba.UserAccountID) 'DBA', sa.UserAccountID 'sa_id', dba.UserAccountID 'dba_id',
			ds.DeploymentStatusID, ds.Description 'DeploymentStatus', s.SupportType, s.OSType, s.MajorApps,
			case SupportType when 'F' then 1 when 'D' then 1 else 0 end 'DBSupported'
		from dbo.tbServer s
		inner join dbo.tbUARoleServer ur on ur.UserAccountID = @UserAccountID and s.ServerID = ur.ServerID and ur.IsPrimary = 1
		left join dbo.tbUARoleServer sa on s.ServerID = sa.ServerID and sa.UARoleID = 2 and sa.IsPrimary = 1
		left join dbo.tbUARoleServer dba on s.ServerID = dba.ServerID and dba.UARoleID = 1 and dba.IsPrimary = 1
		left join dbo.vwSrvDeploymentStatus ds on s.ServerID = ds.ServerID
		where ds.DeploymentStatusID <> @DeploymentStatusID
			and s.IsDeleted = 0
		order by s.ServerName

	if (@Return = 'S')
		select s.ServerID, s.ServerName, s.OnPawanet, s.IsOnline, 
			dbo.sfNumNotes(s.ServerID, 'S') 'NumSrvNotes', dbo.sfFullName(sa.UserAccountID) 'SA',
			dbo.sfFullName(dba.UserAccountID) 'DBA', sa.UserAccountID 'sa_id', dba.UserAccountID 'dba_id',
			ds.DeploymentStatusID, ds.Description 'DeploymentStatus', s.SupportType, s.OSType, s.MajorApps,
			case SupportType when 'F' then 1 when 'D' then 1 else 0 end 'DBSupported'
		from dbo.tbServer s
		left join dbo.tbUARoleServer sa on s.ServerID = sa.ServerID and sa.UARoleID = 2 and sa.IsPrimary = 1
		left join dbo.tbUARoleServer dba on s.ServerID = dba.ServerID and dba.UARoleID = 1 and dba.IsPrimary = 1
		left join dbo.vwSrvDeploymentStatus ds on s.ServerID = ds.ServerID
		where s.ServerName like @Search and ds.DeploymentStatusID <> @DeploymentStatusID
			and s.IsDeleted = 0
		order by s.ServerName

END
GO
grant exec on dbo.upGetServers to system