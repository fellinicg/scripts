insert into tbHardwareServer
select src.ServerID, dst.HardwareID
from PomeroyMgmt.dbo.vwServerInfo src
left join dbo.vwServer dst on src.ServerName = dst.Name
where DeploymentStatus = 'Production' and HardwareID is not null