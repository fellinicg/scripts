USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbDepartment') AND type in (N'U'))
DROP TABLE dbo.tbDepartment
GO
CREATE TABLE dbo.tbDepartment(
	DepartmentID smallint identity(1,1) not null,
	Description varchar(50) not null,
	CompanyID tinyint not null dftbDepartmentCompanyID default 1,
	IsDeleted bit constraint not null dftbDepartmentIsDeleted default 0,
 CONSTRAINT PK_tbDepartment PRIMARY KEY CLUSTERED 
(
	DepartmentID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbDepartment with check add constraint fkttbDepartmentCompanyID 
foreign key(CompanyID) references dbo.tbCompany (CompanyID)

