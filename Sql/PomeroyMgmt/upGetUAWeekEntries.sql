USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetUAWeekEntries') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetUAWeekEntries
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20121114
-- ============================================================
CREATE PROCEDURE dbo.upGetUAWeekEntries
	@UserAccountID int
	,@WeekID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select t.TimeEntryID, tt.TimeTypeID, tt.Description, t.Info, t.Hours, t.StartTime, t.EndTime, 
		tt.HasInfo, tt.HasTime, dbo.sfNumAttachments(t.TimeEntryID) 'NumAttachments'
	from dbo.tbTimeEntry t inner join dbo.tbTimeType tt on t.TimeTypeID = tt.TimeTypeID
	where t.UserAccountID = @UserAccountID and t.WeekID = @WeekID and t.IsDeleted = 0
	order by t.CreateDateTime	

END
GO
grant exec on dbo.upGetUAWeekEntries to system