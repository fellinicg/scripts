USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateServerWarranty') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateServerWarranty
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091204
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091204
-- ============================================================
CREATE PROCEDURE dbo.upUpdateServerWarranty
	@ServerID int
	,@ExpirationDate datetime
	,@Provider varchar(20)
	,@Result char(1) output
AS
BEGIN

	-- Declarations

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Return data
	update dbo.tbServerWarranty set ExpirationDate = @ExpirationDate
		,Provider = @Provider
	where ServerID = @ServerID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upUpdateServerWarranty - server warranty was NOT successfully updated', 16, 1)

END
GO
grant exec on dbo.upUpdateServerWarranty to system