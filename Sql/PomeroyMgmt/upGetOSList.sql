USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetOSList') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetOSList
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100108
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100108
-- ============================================================
CREATE PROCEDURE dbo.upGetOSList
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select distinct OS 'Description'
	from dbo.tbServer
	where dbo.sfTrim(isnull(OS, '')) <> ''
	order by OS

END
GO
grant exec on dbo.upGetOSList to system