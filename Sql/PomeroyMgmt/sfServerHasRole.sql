USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.sfServerHasRole') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.sfServerHasRole
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100104
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100104
-- ============================================================
CREATE FUNCTION dbo.sfServerHasRole (@ServerID int, @RoleID int)
	RETURNS varchar(3) AS
BEGIN

	-- Declarations
	declare @Ret varchar(3)

	if exists (select 1 from dbo.tbServerRole where ServerID = @ServerID and RoleID = @RoleID)
		set @Ret = 'Yes'
	else
		set @Ret =  'No'

	return @Ret

END
