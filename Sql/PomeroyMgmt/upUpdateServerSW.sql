USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upUpdateServerSW') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upUpdateServerSW
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091209
-- ============================================================
CREATE PROCEDURE dbo.upUpdateServerSW
	@ServerID int
	,@OS varchar(200)
	,@OSProductID varchar(32)
	,@OSVer varchar(20)
	,@OSServicePack varchar(20)
	,@MajorApps varchar(100)
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Update server
	update tbServer set OS = @OS, OSProductID = @OSProductID, OSVer = @OSVer, OSServicePack = @OSServicePack
		,MajorApps = isnull(@MajorApps, MajorApps), UpdateDateTime = current_timestamp
	where ServerID = @ServerID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upUpdateServerSW - server software was NOT successfully updated', 16, 1)

END
GO
grant exec on dbo.upUpdateServerSW to system