USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbLocation') AND type in (N'U'))
DROP TABLE dbo.tbLocation
GO
CREATE TABLE dbo.tbLocation(
	LocationID tinyint identity(1,1) not null,
	ShortDesc varchar(10) not null,
	Description varchar(50) not null,
	IsDeleted bit constraint dftbLocationIsDeleted default 0,
 CONSTRAINT PK_tbLocation PRIMARY KEY CLUSTERED 
(
	LocationID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF