USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddMajAppsFromIBMDir') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddMajAppsFromIBMDir
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100106
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100106
-- ============================================================
CREATE PROCEDURE dbo.upAddMajAppsFromIBMDir
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Update
	update dbo.tbServer set MajorApps = dbo.sfTrim(i.MajorApps)
	from dbo.tbServer s
	inner join dbo.vwServerIBMDir si on s.ServerID = si.ServerID
	inner join openquery(teleas22, 'select MANAGED_OBJ_ID, case when charindex(''('', label) > 0 
		then substring(label, charindex(''('', label) + 1, (charindex('')'', label) - 
		charindex(''('', label))-1) end as MajorApps from IBMDirector.dbo.TWG_MANAGED_OBJECT') i on si.IBMDirectorID = i.MANAGED_OBJ_ID
	where i.MajorApps is not null

END
GO
grant exec on dbo.upAddMajAppsFromIBMDir to system