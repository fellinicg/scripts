USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetServerWarranty') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetServerWarranty
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091204
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091204
-- ============================================================
CREATE PROCEDURE dbo.upGetServerWarranty
	@ServerID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select s.ServerName, convert(varchar, w.ExpirationDate, 101) 'ExpirationDate', w.Provider, 
		dbo.sfNumNotes(@ServerID, 'W') 'NumNotes'
	from dbo.tbServer s
		left join dbo.tbServerWarranty w on s.ServerID = w.ServerID
	where s.ServerID = @ServerID

END
GO
grant exec on dbo.upGetServerWarranty to system