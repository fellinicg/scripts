USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'dbo.vwSrvDeploymentStatus'))
DROP VIEW dbo.vwSrvDeploymentStatus
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091202
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091202
-- ============================================================
CREATE VIEW dbo.vwSrvDeploymentStatus
AS
	select a.SrvDeploymentStatusID, a.ServerID, c.DeploymentStatusID, c.Description, a.CreateDateTime
	from dbo.tbSrvDeploymentStatus a
	inner join (select ds.ServerID, max(ds.SrvDeploymentStatusID) 'SrvDeploymentStatusID'
							from dbo.tbSrvDeploymentStatus ds
							group by ds.ServerID) b on a.SrvDeploymentStatusID = b.SrvDeploymentStatusID
	left join dbo.tbDeploymentStatus c on a.DeploymentStatusID = c.DeploymentStatusID