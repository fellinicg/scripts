USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetServerUATask') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetServerUATask
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091222
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091222
-- ============================================================
CREATE PROCEDURE dbo.upGetServerUATask
	@UATaskID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select s.ServerID, s.ServerName, ut.LogFile, uat.ServerUATaskID
	from dbo.tbUATask ut
	left join dbo.tbServerUATask uat on ut.UATaskID = uat.UATaskID
	left join dbo.tbServer s on uat.ServerID = s.ServerID
	where ut.UATaskID = @UATaskID
	order by s.ServerName

END
GO
grant exec on dbo.upGetServerUATask to system