USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetCurrWeek') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetCurrWeek
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20121114
-- ============================================================
CREATE PROCEDURE dbo.upGetCurrWeek
	@Result int output	
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 0

	-- Get current week id
	select @Result = WeekID
	from dbo.tbWeek
	where current_timestamp between BeginDate and EndDate
		
END
GO
grant exec on dbo.upGetCurrWeek to system