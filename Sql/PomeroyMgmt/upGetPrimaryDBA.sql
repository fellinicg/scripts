USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetPrimaryDBA') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetPrimaryDBA
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20120503
-- ============================================================
CREATE PROCEDURE dbo.upGetPrimaryDBA
	@ServerID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select u.UserAccountID, dbo.sfFullName(u.UserAccountID) 'DBA', n.ContactNum, e.EmailAddress
	from dbo.tbUARoleServer r
		inner join dbo.tbUserAccount u on r.UserAccountID = u.UserAccountID
		left join dbo.tbContactNum n on u.UserAccountID = n.UserAccountID and n.ContactNumType = 'WP'
		left join dbo.tbEmailAddress e on u.UserAccountID = e.UserAccountID and e.EmailAddressType = 'W'
	where r.ServerID = @ServerID and r.UARoleID = 1 and r.IsPrimary = 1

END
GO
grant exec on dbo.upGetPrimaryDBA to system