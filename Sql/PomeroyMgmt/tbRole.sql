USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbRole') AND type in (N'U'))
DROP TABLE dbo.tbRole
GO
CREATE TABLE dbo.tbRole(
	RoleID int identity(1,1) not null,
	ShortDesc varchar(10) not null,
	Description varchar(50) not null,
	IsDeleted bit not null constraint dftbRoleIsDeleted default 0,
 CONSTRAINT PK_tbRole PRIMARY KEY CLUSTERED 
(
	RoleID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
go
set identity_insert dbo.tbRole on
go
insert into dbo.tbRole (RoleID, ShortDesc, Description, IsDeleted) values('1','DB','Database Server','0')
insert into dbo.tbRole (RoleID, ShortDesc, Description, IsDeleted) values('2','APP','Application Server','0')
insert into dbo.tbRole (RoleID, ShortDesc, Description, IsDeleted) values('3','WEB','Web Server','0')
insert into dbo.tbRole (RoleID, ShortDesc, Description, IsDeleted) values('4','FILE/PRN','File/Print Server','0')
insert into dbo.tbRole (RoleID, ShortDesc, Description, IsDeleted) values('5','INET','Internet Server','0')
go
set identity_insert dbo.tbRole off
