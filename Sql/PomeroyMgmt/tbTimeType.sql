USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbTimeType') AND type in (N'U'))
DROP TABLE dbo.tbTimeType
GO
CREATE TABLE dbo.tbTimeType(
	TimeTypeID tinyint identity(1,1) not null,
	Description varchar(50) not null,
	HasInfo bit constraint dftbTimeTypeHasInfo default 0 not null,
	HasTime bit constraint dftbTimeTypeHasTime default 0 not null,
	IsDeleted bit constraint dftbTimeTypeIsDeleted default 0 not null,
CONSTRAINT PK_tbTimeType PRIMARY KEY CLUSTERED 
(
	TimeTypeID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
