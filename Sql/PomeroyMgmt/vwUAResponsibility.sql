USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'dbo.vwUAResponsibility'))
DROP VIEW dbo.vwUAResponsibility
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100503
-- ============================================================
CREATE VIEW dbo.vwUAResponsibility
AS
	select 
		dbo.sfFullName(UserAccountID) 'User', 
		Tasks, 
		Projects, 
		convert(varchar, CreateDateTime, 100) CreateDateTime, 
		convert(varchar, UpdateDateTime, 100) UpdateDateTime
	from dbo.tbUAResponsibility
