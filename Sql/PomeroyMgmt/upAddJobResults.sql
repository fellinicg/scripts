USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddJobResults') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddJobResults
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20110712
-- ============================================================
CREATE PROCEDURE dbo.upAddJobResults
AS
BEGIN

	-- Declarations
	declare @Servers table (ServerID int, InstanceID tinyint, ServerName varchar(50))
	declare 
		@ServerId int,
		@InstanceID varchar(5),
		@ServerName varchar(50),
		@Status varchar(40),
		@rc char(1),
		@local bit

	-- Initialize settings
	set nocount on

	-- Initialize variables
	--	set @Result = 'F'

	-- Insert a list of servers into table var
	insert into @Servers
	select ServerID, SQLInstanceID, SQLInstanceName
	from dbo.vwSrvDbList
		 
	-- Truncate tbJobResults
	truncate table dbo.tbJobResults

	-- Loop over servers
	while exists(select 1 from @Servers)
		begin
			select top 1 @ServerID = ServerID, @ServerName = ServerName, @InstanceID = InstanceID from @Servers
			-- Check if server is local
			if @@servername = @ServerName
				set @local = 1
			else
				set @local = 0
			-- Check if the server is a linked server, if not, create it
			if (select dbo.sfLinkedSrvExists(@ServerName)) is null
				exec dbo.upAddLinkedServer @ServerName, @rc output
			else set @rc = 'S'
			if @rc = 'S'
				begin
					-- Test connection to remote sql server
					if @local = 0 
						exec dbo.upLinkedSrvAvail @ServerName, @rc output
					if @rc <> 'F'
						begin
							-- Populate tbJobResults
							if @local = 0
								insert into dbo.tbJobResults
								exec('select ' + @ServerId + ',''' + @InstanceID + ''', * from openquery([' + @ServerName + '],
								''select j.name, h.message, h.run_status, convert(datetime, rtrim(run_date)) + (run_time * 9 + run_time % 10000 * 6 + run_time % 100 * 10)/216e4
								from msdb.dbo.sysjobs j
								inner join master.dbo.syslogins l on j.owner_sid = l.sid
								inner join msdb.dbo.sysjobhistory h on j.job_id = h.job_id
								inner join (
									select job_id, max(run_date) ''''latest''''
									from msdb.dbo.sysjobhistory
									group by job_id
								) r on h.job_id = r.job_id and h.run_date = r.latest and h.step_id = 0
								where j.enabled = 1
								and l.name like ''''%tfellini%''''
								order by j.name'')')							
							else
								insert into dbo.tbJobResults
								select @ServerId, @InstanceID, j.name, h.message, h.run_status, convert(datetime, rtrim(run_date)) + (run_time * 9 + run_time % 10000 * 6 + run_time % 100 * 10)/216e4
								from msdb.dbo.sysjobs j
								inner join master.dbo.syslogins l on j.owner_sid = l.sid
								inner join msdb.dbo.sysjobhistory h on j.job_id = h.job_id
								inner join (
									select job_id, max(run_date) 'latest'
									from msdb.dbo.sysjobhistory
									group by job_id
								) r on h.job_id = r.job_id and h.run_date = r.latest and h.step_id = 0
								where j.enabled = 1
								and l.name like '%tfellini%'
								order by j.name
						end						
				end
			delete from @Servers where ServerName = @ServerName
		end

	-- Verify success
	--	set @Result = 'S'

END
GO
grant exec on dbo.upAddJobResults to system