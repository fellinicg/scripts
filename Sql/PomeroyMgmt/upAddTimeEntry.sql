USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddTimeEntry') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddTimeEntry
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20121115
-- ============================================================
CREATE PROCEDURE dbo.upAddTimeEntry
	@UserAccountID int
	,@WeekID smallint
	,@TimeTypeID tinyint
	,@Info varchar(500)
	,@Hours float
	,@StartTime varchar(10)
	,@EndTime varchar(10)
	,@Result char(1) output	
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Insert data
	insert into dbo.tbTimeEntry (UserAccountID, WeekID, TimeTypeID, Info, Hours, StartTime, EndTime)
	values (@UserAccountID, @WeekID, @TimeTypeID, @Info, @Hours, @StartTime, @EndTime)

	-- Verify success
	if (@@rowcount = 1 and @@identity > 0)
		set @Result = 'S'
	else
		raiserror('upAddTimeEntry - Time entry was NOT successfully added.', 16, 1)

END
GO
grant exec on dbo.upAddTimeEntry to system