USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.sfWOComment') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.sfWOComment
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100514
-- ============================================================
CREATE FUNCTION dbo.sfWOComment (@WorkOrderID int, @DelimChar char(1))
	RETURNS varchar(max) AS
BEGIN

	-- Declarations
	declare @Comment varchar(2000), @idx int
	declare @tbl table (idx int identity(1,1), comment varchar(max))
	
	-- Initialize variables
	set @Comment = ''
	if (len(dbo.sfTrim(@DelimChar))) = 0 set @DelimChar = ','
	
	-- Get data
	insert into @tbl (comment)
	select convert(varchar, CreateDateTime, 101) + ' - ' + dbo.sfTrim(WOComment)
	from dbo.tbWOComment
	where WorkOrderID = @WorkOrderID and IsDeleted = 0

	-- Loop over table and comma delimit
	while exists(select 1 from @tbl)
	begin
		select top 1 @idx = idx, @Comment = @Comment + comment + @DelimChar from @tbl
		delete from @tbl where idx = @idx
	end
	
	return case when len(@Comment) = 0 then '' else left(@Comment, len(@Comment)-1) end
	
END
