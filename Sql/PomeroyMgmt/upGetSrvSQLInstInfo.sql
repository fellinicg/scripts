USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetSrvSQLInstInfo') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetSrvSQLInstInfo
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100803
-- ============================================================
CREATE PROCEDURE dbo.upGetSrvSQLInstInfo
	@ServerID int
	,@SQLInstanceID int = null
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select dp.Description + ' ' + isnull(dp.Edition, '') + ' ' + isnull(dp.ProductLevel, '') 'SqlVer',
		dbo.sfFullName(urs.UserAccountID) 'DBA'
	from dbo.tbServer s
		left join dbo.tbServerDBPlatform sd on s.ServerID = sd.ServerID and isnull(@SQLInstanceID, 0) = isnull(sd.SQLInstanceID, 0)
		left join dbo.tbDBPlatform dp on sd.DBPlatformID = dp.DBPlatformID
		left join dbo.tbUARoleServer urs on s.ServerID = urs.ServerID and urs.UARoleID = 1 and urs.IsPrimary = 1
	where s.ServerID = @ServerID

END
GO
grant exec on dbo.upGetSrvSQLInstInfo to system
