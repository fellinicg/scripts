USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upMaintainServerIP') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upMaintainServerIP
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091209
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091209
-- ============================================================
CREATE PROCEDURE dbo.upMaintainServerIP
	@ServerID int
	,@IPAddress varchar(64)
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Add/update data
	if not exists(select 1 from dbo.tbServerIP where ServerID = @ServerID)
		-- Add record
		insert into dbo.tbServerIP (ServerID, IPAddress)
		values (@ServerID, @IPAddress)
	else
		-- Update record
		update dbo.tbServerIP set IPAddress = @IPAddress
		where ServerID = @ServerID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upMaintainServerIP - server IP was NOT successfully maintained', 16, 1)

END
GO
grant exec on dbo.upMaintainServerIP to system