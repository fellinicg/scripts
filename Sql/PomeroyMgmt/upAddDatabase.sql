USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddDatabase') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddDatabase
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100106
-- ============================================================
CREATE PROCEDURE dbo.upAddDatabase
	@DBName	varchar(50)
	,@ServerID int
	,@SQLInstanceID int = null
	,@DepartmentID smallint
	,@ClientContactID int
	,@IsOnline bit
	,@IsSupported bit
	,@DatabaseID int output
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Check that database name doesn't already exist on the server
	if exists(select 1 from dbo.tbDatabase where DBName = @DBName and ServerID = @ServerID
		and isnull(SQLInstanceID, 0) = isnull(@SQLInstanceID, 0))
	begin
		set @Result = 'X'
		return
	end

	-- Insert new record into tbDatabase
	insert into dbo.tbDatabase (DBName, ServerID, SQLInstanceID, DepartmentID, ClientContactID, 
		IsOnline, IsSupported, CreateDateTime, UpdateDateTime)
	values (@DBName, @ServerID, @SQLInstanceID, @DepartmentID, @ClientContactID, 
		@IsOnline, @IsSupported, current_timestamp, current_timestamp)

	-- Verify success
	if (@@rowcount = 1 and @@identity > 0)
	begin
		set @Result = 'S'
		set @DatabaseID = @@identity
	end
	else
		raiserror('upAddDatabase - Database was NOT successfully added', 16, 1)

END
GO
grant exec on dbo.upAddDatabase to system