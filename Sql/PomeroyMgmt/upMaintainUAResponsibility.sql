USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upMaintainUAResponsibility') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upMaintainUAResponsibility
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100418
-- ============================================================
CREATE PROCEDURE dbo.upMaintainUAResponsibility
	@UserAccountID int
	,@Tasks varchar(max)
	,@Projects varchar(max)
	,@Result char(1) output
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	set @Tasks = dbo.sfEmptyToNull(@Tasks)
	set @Projects = dbo.sfEmptyToNull(@Projects)
	
	-- Add/update data
	if not exists(select 1 from dbo.tbUAResponsibility where UserAccountID = @UserAccountID)
		-- Add record
		insert into dbo.tbUAResponsibility (UserAccountID, Tasks, Projects, CreateDateTime, UpdateDateTime)
		values (@UserAccountID, @Tasks, @Projects, current_timestamp, current_timestamp)
	else
		-- Update record
		update dbo.tbUAResponsibility set Tasks = @Tasks, Projects = @Projects, 
			UpdateDateTime = current_timestamp
		where UserAccountID = @UserAccountID

	-- Verify success
	if (@@rowcount = 1 and @@error = 0)
		set @Result = 'S'
	else
		raiserror('upMaintainUAResponsibility - responsibilites were NOT successfully maintained', 16, 1)

END
GO
grant exec on dbo.upMaintainUAResponsibility to system