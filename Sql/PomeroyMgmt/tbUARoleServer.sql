USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbUARoleServer') AND type in (N'U'))
DROP TABLE dbo.tbUARoleServer
GO
CREATE TABLE dbo.tbUARoleServer(
	UARoleServerID int identity(1,1) not null,
	UARoleID int not null,
	UserAccountID int not null,
	ServerID int not null,
	IsPrimary bit not null constraint dftbUARoleServerIsPrimary default 0,
CONSTRAINT PK_tbUARoleServer PRIMARY KEY CLUSTERED 
(
	UARoleServerID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO 
alter table dbo.tbUARoleServer with check add constraint ucUserRoleSrv unique (UARoleID,UserAccountID,ServerID)
GO
alter table dbo.tbUARoleServer with check add constraint fktbUARoleServerUARoleID
foreign key(UARoleID) references dbo.tbUARole (UARoleID)
GO
alter table dbo.tbUARoleServer with check add constraint fktbUARoleServerUserAccountID
foreign key(UserAccountID) references dbo.tbUserAccount (UserAccountID)
GO
alter table dbo.tbUARoleServer with check add constraint fktbUARoleServerServerID
foreign key(ServerID) references dbo.tbServer (ServerID)