USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddServer') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddServer
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091209
-- ============================================================
CREATE PROCEDURE dbo.upAddServer
	@ServerName	varchar(50)
	,@ServerType char(1)
	,@OnPawanet bit
	,@IsOnline bit
	,@SupportBeginDate datetime
	,@DeploymentStatusID tinyint
	,@UserAccountID int
	,@OSType varchar(20)
	,@SupportType char(1)
	,@ServerID int output
	,@Result char(1) output	
AS
BEGIN

	-- Declarations
	declare @UARoleID int

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'
	select @UARoleID = UARoleID from dbo.tbUARole where ShortDesc = 'DBA'

	-- Check that server name doesn't already exist
	if exists(select 1 from dbo.tbServer where ServerName = @ServerName)
	begin
		set @Result = 'X'
		return
	end

	-- Open transaction
	begin tran

	-- Insert new record into tbServer
	insert into dbo.tbServer (ServerName, ServerType, OnPawanet, IsOnline,
		SupportBeginDate, OSType, SupportType, CreateDateTime, UpdateDateTime)
	values (@ServerName, @ServerType, @OnPawanet, @IsOnline,
		@SupportBeginDate, @OSType, @SupportType, current_timestamp, current_timestamp)

	-- Verify success
	if (@@rowcount = 1 and @@identity > 0)
		set @ServerID = @@identity
	else
	begin
		rollback tran
		raiserror('upAddServer - Server was NOT successfully added', 16, 1)
		return
	end

	if @UserAccountID <> -1 and (@SupportType = 'D' or @SupportType = 'F')
	begin
		-- Record DBA
		exec dbo.upAddUARoleServer @UARoleID, @UserAccountID, @ServerID, 1, @Result output

		-- Verify success
		if (@Result <> 'S')
		begin
			rollback tran
			raiserror('upAddServer - Server was NOT successfully added.  Primary DBA failed.', 16, 1)
			return
		end
	end
	
	-- Record deployment status
	insert into dbo.tbSrvDeploymentStatus (ServerID, DeploymentStatusID)
	values (@ServerID, @DeploymentStatusID)

	-- Verify success
	if (@@rowcount = 1 and @@identity > 0)
	begin
		commit tran
		set @Result = 'S'
	end
	else
	begin
		set @Result = 'F'
		rollback tran
		raiserror('upAddServer - Server was NOT successfully added.  Deployment status failed.', 16, 1)
	end

END
GO
grant exec on dbo.upAddServer to system