USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upGetAvailWORequestor') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upGetAvailWORequestor
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100127
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100127
-- ============================================================
CREATE PROCEDURE dbo.upGetAvailWORequestor
	@WorkOrderID int
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return Data
	select c.ClientContactID, dbo.sfClientFullName(c.ClientContactID) 'Available'
	from dbo.tbClientContact c
	left join dbo.tbWorkOrderRequestor a on c.ClientContactID = a.ClientContactID
		and a.WorkOrderID = @WorkOrderID and a.IsDeleted = 0
	where c.IsDeleted = 0 and a.WorkOrderRequestorID is null
	order by Available
END
GO
grant exec on dbo.upGetAvailWORequestor to system