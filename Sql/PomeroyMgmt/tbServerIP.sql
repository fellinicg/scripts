USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbServerIP') AND type in (N'U'))
DROP TABLE dbo.tbServerIP
GO
CREATE TABLE dbo.tbServerIP(
	ServerIPID int identity(1,1) not null,
	ServerID int not null,
	IPAddress varchar(64) not null,
CONSTRAINT PK_tbServerIP PRIMARY KEY CLUSTERED 
(
	ServerIPID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbServerIP with check add constraint ucServerIDIP unique (ServerID, IPAddress)
GO
alter table dbo.tbServerIP with check add constraint fktbServerIPServerID
foreign key(ServerID) references dbo.tbServer (ServerID)
