USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbServerOrder') AND type in (N'U'))
DROP TABLE dbo.tbServerOrder
GO
CREATE TABLE dbo.tbServerOrder(
	ServerID int not null,
	OrderDate datetime null,
	OrderNumber varchar(20) null,
	CreateDateTime datetime not null constraint dftbServerOrderCreateDateTime default (getdate()),
	UpdateDateTime datetime not null constraint dftbServerOrderUpdateDateTime default (getdate()),
 CONSTRAINT PK_tbServerOrder PRIMARY KEY CLUSTERED 
(
	ServerID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF