USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.tbEmailAddress') AND type in (N'U'))
DROP TABLE dbo.tbEmailAddress
GO
CREATE TABLE dbo.tbEmailAddress(
	EmailAddressID int identity(1,1) not null,
	UserAccountID int not null,
	EmailAddressType char(1) not null,
	EmailAddress varchar(120) not null,
	IsValidFormat as (dbo.sfEmailAddressIsValid([EmailAddress])),
	IsDeleted bit not null constraint dftbEmailAddressIsDeleted default 0,
	CreateDateTime datetime not null constraint dftbEmailAddressCreateDateTime default (getdate()),
	UpdateDateTime datetime not null constraint dftbEmailAddressUpdateDateTime default (getdate()),
 CONSTRAINT PK_tbEmailAddress PRIMARY KEY CLUSTERED 
(
	EmailAddressID ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
alter table dbo.tbEmailAddress with check add constraint tcEmailAddressType 
	check ((EmailAddressType='H' OR EmailAddressType='W'))
GO
alter table dbo.tbEmailAddress with check add constraint fktbEmailAddressUserAccountID
foreign key(UserAccountID) references dbo.tbUserAccount (UserAccountID)
