USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upRptDBFileInfo') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upRptDBFileInfo
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100105
-- ============================================================
CREATE PROCEDURE dbo.upRptDBFileInfo
AS
BEGIN

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		isnull(si.SQLInstanceName, s.ServerName) 'ServerName'
		,si.Cluster
		,d.DBName
		,df.LogicalFileName
		,df.PhysicalFileName
		,df.FileSizeMB
		,df.FreeSpaceMB
		,df.FreeSpacePct
		,df.MaxSize
		,df.Growth
		,df.RecoveryMode
		,s.ServerID
		,isnull(d.SQLInstanceID, 0) 'SQLInstanceID'
		,d.DatabaseID
		,isnull(dr.DBRowSpan, 1) 'DBRowSpan'
		,d.IsOnline
		,d.IsDeleted
		,d.IsSupported
		,r.UserAccountID
		,convert(varchar, df.PollDateTime, 100) 'PollDateTime'
	from dbo.tbDBFiles df
		right join dbo.tbDatabase d on df.DatabaseID = d.DatabaseID
		left join dbo.tbServer s on d.ServerID = s.ServerID
		left join dbo.tbSQLInstance si on d.ServerID = si.ServerID and d.SQLInstanceID = si.SQLInstanceID
		left join dbo.vwSrvDeploymentStatus sd on s.ServerID = sd.ServerID
		left join dbo.tbUARoleServer r on r.ServerID = s.ServerID and r.UARoleID = 1
		left join (select df.DatabaseID,count(df.DatabaseID) 'DBRowSpan'
							from dbo.tbDBFiles df group by df.DatabaseID) dr on d.DatabaseID = dr.DatabaseID
	where s.OnPawanet = 1 and sd.DeploymentStatusID in (1,2,5) and s.SupportType in ('F', 'D')
	order by ServerName, d.DBName
	
END
GO
grant exec on dbo.upRptDBFileInfo to system