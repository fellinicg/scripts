USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.sfGetNextWorkOrderID') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION dbo.sfGetNextWorkOrderID
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20100121
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20100121
-- ============================================================
CREATE FUNCTION dbo.sfGetNextWorkOrderID ()
	RETURNS int AS
BEGIN
	return(select isnull(max(WorkOrderID), 0) + 1 from dbo.tbWorkOrder)
END