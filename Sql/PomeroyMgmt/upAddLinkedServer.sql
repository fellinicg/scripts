USE PomeroyMgmt
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.upAddLinkedServer') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.upAddLinkedServer
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20091106
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20091106
-- ============================================================
CREATE PROCEDURE dbo.upAddLinkedServer
	@Server nvarchar(50)
	,@Result char(1) output
AS
BEGIN

	-- Declarations

	-- Initialize settings
	set nocount on

	-- Initialize variables
	set @Result = 'F'

	-- Add linked server
	EXEC master.dbo.sp_addlinkedserver @server = @Server, @srvproduct=N'SQL Server'
	EXEC master.dbo.sp_serveroption @server=@Server, @optname=N'collation compatible', @optvalue=N'true'
	EXEC master.dbo.sp_serveroption @server=@Server, @optname=N'data access', @optvalue=N'true'
	EXEC master.dbo.sp_serveroption @server=@Server, @optname=N'dist', @optvalue=N'false'
	EXEC master.dbo.sp_serveroption @server=@Server, @optname=N'pub', @optvalue=N'false'
	EXEC master.dbo.sp_serveroption @server=@Server, @optname=N'rpc', @optvalue=N'true'
	EXEC master.dbo.sp_serveroption @server=@Server, @optname=N'rpc out', @optvalue=N'true'
	EXEC master.dbo.sp_serveroption @server=@Server, @optname=N'sub', @optvalue=N'false'
	EXEC master.dbo.sp_serveroption @server=@Server, @optname=N'connect timeout', @optvalue=N'0'
	EXEC master.dbo.sp_serveroption @server=@Server, @optname=N'collation name', @optvalue=null
	EXEC master.dbo.sp_serveroption @server=@Server, @optname=N'lazy schema validation', @optvalue=N'false'
	EXEC master.dbo.sp_serveroption @server=@Server, @optname=N'query timeout', @optvalue=N'0'
	EXEC master.dbo.sp_serveroption @server=@Server, @optname=N'use remote collation', @optvalue=N'true'

	-- Verify success
	if @@rowcount > 0
		set @Result = 'S'

END
