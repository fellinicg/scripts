use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAddress') and type in (N'U'))
drop table dbo.tbAddress

create table dbo.tbAddress(
	AddressID int identity(1,1) not null,
	Line1 varchar(80) null,
	Line2 varchar(80) null,
	Line3 varchar(80) null,
	Line4 varchar(80) null,
	CityTown varchar(40) null,
	StateProvince varchar(40) null,
	PostalCode varchar(20) null,
	Country varchar(80) null,
	IsDeleted bit not null constraint dfAddressIsDeleted default ((0)),
	CreatedTS datetime not null constraint dfAddressCreatedTS default (getdate()),
	UpdatedTS datetime not null constraint dfAddressUpdatedTS default (getdate()),
 constraint pkAddressID primary key clustered 
(
	AddressID asc
)
)

set ansi_padding off