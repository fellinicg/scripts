use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbIndustry') and type in (N'U'))
drop table dbo.tbIndustry

create table dbo.tbIndustry(
	IndustryID int identity(1,1) not null,
	Description varchar(40) not null,
 constraint pkIndustry primary key clustered 
(
	IndustryID asc
)
)

set ansi_padding off