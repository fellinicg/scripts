use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbVendorType') and type in (N'U'))
drop table dbo.tbVendorType

create table dbo.tbVendorType(
	VendorType char(1) not null,
	Description varchar(40) not null,
 constraint pkVendorType primary key clustered 
(
	VendorType asc
)
)

set ansi_padding off