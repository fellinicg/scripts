use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAccountPhone') and type in (N'U'))
drop table dbo.tbAccountPhone

create table dbo.tbAccountPhone(
	AccountPhoneID int identity(1,1) not null,
	AccountID int not null,
	PhoneID int not null,
	PhoneType char(1) not null,
	IsDeleted bit not null constraint dfAccountPhoneIsDeleted default ((0)),
	CreatedTS datetime not null constraint dfAccountPhoneCreatedTS default (getdate()),
 constraint pkAccountPhone primary key clustered 
(
	AccountPhoneID asc
)
)

alter table dbo.tbAccountPhone add constraint ucAccountPhone unique (AccountID, PhoneID, PhoneType)

set ansi_padding off