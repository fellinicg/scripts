use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbRfp') and type in (N'U'))
drop table dbo.tbRfp

create table dbo.tbRfp(
	RfpID int identity(1,1) not null,
	RequestID int not null,
	LineItem varchar(1000) not null,
	IsDeleted bit not null constraint dfRfpIsDeleted default ((0)),
	CreatedTS datetime not null constraint dfRfpCreatedTS default (getdate()),
 constraint pkRfp primary key clustered 
(
	RfpID asc
)
)

set ansi_padding off