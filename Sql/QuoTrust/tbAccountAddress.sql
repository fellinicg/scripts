use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAccountAddress') and type in (N'U'))
drop table dbo.tbAccountAddress

create table dbo.tbAccountAddress(
	AccountAddressID int identity(1,1) not null,
	AccountID int not null,
	AddressID int not null,
	AddressType char(1) not null,
	IsDeleted bit not null constraint dfAccountAddressIsDeleted default ((0)),
	CreatedTS datetime not null constraint dfAccountAddressCreatedTS default (getdate()),
 constraint pkAccountAddress primary key clustered 
(
	AccountAddressID asc
)
)

alter table dbo.tbAccountAddress add constraint ucAccountAddress unique (AccountID, AddressID, AddressType)

set ansi_padding off