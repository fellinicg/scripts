use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbEmailType') and type in (N'U'))
drop table dbo.tbEmailType

create table dbo.tbEmailType(
	EmailType char(1) not null,
	Description varchar(40) not null,
 constraint pkEmailType primary key clustered 
(
	EmailType asc
)
)

set ansi_padding off