use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbRequestIndustry') and type in (N'U'))
drop table dbo.tbRequestIndustry

create table dbo.tbRequestIndustry(
	RequestID int not null,
	IndustryID int not null,
	CreatedTS datetime not null constraint dfRequestIndustryCreatedTS default (getdate()),
 constraint pkRequestIndustry primary key clustered 
(
	RequestID, IndustryID asc
)
)

set ansi_padding off