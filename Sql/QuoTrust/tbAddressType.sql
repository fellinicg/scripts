use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAddressType') and type in (N'U'))
drop table dbo.tbAddressType

create table dbo.tbAddressType(
	AddressType char(1) not null,
	Description varchar(40) not null,
 constraint pkAddressType primary key clustered 
(
	AddressType asc
)
)

set ansi_padding off