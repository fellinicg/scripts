--dbo.tbAccount
if  exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountUserID'))
alter table dbo.tbAccount drop fkAccountUserID

if  exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountAccountType'))
alter table dbo.tbAccount drop fkAccountAccountType

if  exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountStatus'))
alter table dbo.tbAccount drop fkAccountStatus

--dbo.tbAccountAddress
if  exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountAddressAccountID'))
alter table dbo.tbAccountAddress drop fkAccountAddressAccountID

if  exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountAddressAddressID'))
alter table dbo.tbAccountAddress drop fkAccountAddressAddressID

if  exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountAddressAddressType'))
alter table dbo.tbAccountAddress drop fkAccountAddressAddressType

--dbo.tbAccountEmail
if  exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountEmailAccountID'))
alter table dbo.tbAccountEmail drop fkAccountEmailAccountID

if  exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountEmailEmailID'))
alter table dbo.tbAccountEmail drop fkAccountEmailEmailID

if  exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountEmailEmailType'))
alter table dbo.tbAccountEmail drop fkAccountEmailEmailType

--dbo.tbAccountPhone
if  exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountPhoneAccountID'))
alter table dbo.tbAccountPhone drop fkAccountPhoneAccountID

if  exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountPhonePhoneID'))
alter table dbo.tbAccountPhone drop fkAccountPhonePhoneID

if  exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountPhonePhoneType'))
alter table dbo.tbAccountPhone drop fkAccountPhonePhoneType

--dbo.tbUser
if  exists (select * from sys.foreign_keys where object_id = object_id(N'fkUserStatus'))
alter table dbo.tbUser drop fkUserStatus