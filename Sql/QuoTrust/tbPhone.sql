use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPhone') and type in (N'U'))
drop table dbo.tbPhone

create table dbo.tbPhone(
	PhoneID int identity(1,1) not null,
	CityAreaCode varchar(10) null,
	Number varchar(20) not null,
	Extension varchar(10) null,
	TxtMsgCapable bit not null constraint dfPhoneTextMsgCapable  default (0),
	IsDeleted bit not null constraint dfPhoneIsDeleted default ((0)),
	CreatedTS datetime not null constraint dfPhoneCreatedTS default (getdate()),
	UpdatedTS datetime not null constraint dfPhoneUpdatedTS default (getdate()),
 constraint pkPhone primary key clustered
(
	PhoneID asc
)
)

set ansi_padding off