use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbStatus') and type in (N'U'))
drop table dbo.tbStatus

create table dbo.tbStatus(
	Status char(1) not null,
	Description varchar(40) not null,
 constraint pkStatus primary key clustered 
(
	Status asc
)
)

set ansi_padding off