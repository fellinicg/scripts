use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbRfpResponse') and type in (N'U'))
drop table dbo.tbRfpResponse

create table dbo.tbRfpResponse(
	RfpResponseID int identity(1,1) not null,
	RfpID int not null,
	AccountID int not null,
	Response varchar(3000) not null,
	IsDeleted bit not null constraint dfRfpResponseIsDeleted default ((0)),
	CreatedTS datetime not null constraint dfRfpResponseCreatedTS default (getdate()),
	UpdatedTS datetime not null constraint dfRfpResponseUpdatedTS default (getdate()),
 constraint pkRfpResponse primary key clustered 
(
	RfpResponseID asc
)
)

set ansi_padding off