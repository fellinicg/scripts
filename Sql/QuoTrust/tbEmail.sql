use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbEmail') and type in (N'U'))
drop table dbo.tbEmail

create table dbo.tbEmail(
	EmailID int identity(1,1) not null,
	Email varchar(120) not null,
	IsValidFormat AS (dbo.ufEmailAddressIsValid(Email)),
	IsDeleted bit not null constraint dfEmailIsDeleted default ((0)),
	CreatedTS datetime not null constraint dfEmailCreatedTS default (getdate()),
	UpdatedTS datetime not null constraint dfEmailUpdatedTS default (getdate()),
 constraint pkEmail primary key clustered
(
	EmailID asc
)
)

alter table dbo.tbEmail add constraint ucEmail unique (Email) 

set ansi_padding off