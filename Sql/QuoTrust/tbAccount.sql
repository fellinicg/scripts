use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAccount') and type in (N'U'))
drop table dbo.tbAccount

create table dbo.tbAccount(
	AccountID int identity(1,1) not null,
	UserID int not null,
	AccountType char(1) not null,
	FirstName varchar(40) null,
	MiddleName varchar(40) null,
	LastName varchar(60) null,
	FullName varchar(200) null,
	CompanyName varchar(200) null,
	Status char(1) not null,
	CreatedTS datetime not null constraint dfAccountCreatedTS default (getdate()),
 constraint pkAccount primary key clustered 
(
	AccountID asc
)
)

set ansi_padding off