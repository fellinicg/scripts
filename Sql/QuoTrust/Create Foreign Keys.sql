--dbo.tbAccount
if  not exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountUserID'))
alter table dbo.tbAccount with check add constraint fkAccountUserID foreign key(UserID)
references dbo.tbUser (UserID)

if  not exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountAccountType'))
alter table dbo.tbAccount with check add constraint fkAccountAccountType foreign key(AccountType)
references dbo.tbAccountType (AccountType)

if  not exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountStatus'))
alter table dbo.tbAccount with check add constraint fkAccountStatus foreign key(Status)
references dbo.tbStatus (Status)

--dbo.tbAccountAddress
if  not exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountAddressAccountID'))
alter table dbo.tbAccountAddress with check add constraint fkAccountAddressAccountID foreign key(AccountID)
references dbo.tbAccount (AccountID)

if  not exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountAddressAddressID'))
alter table dbo.tbAccountAddress with check add constraint fkAccountAddressAddressID foreign key(AddressID)
references dbo.tbAddress (AddressID)

if  not exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountAddressAddressType'))
alter table dbo.tbAccountAddress with check add constraint fkAccountAddressAddressType foreign key(AddressType)
references dbo.tbAddressType (AddressType)

--dbo.tbAccountEmail
if  not exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountEmailAccountID'))
alter table dbo.tbAccountEmail with check add constraint fkAccountEmailAccountID foreign key(AccountID)
references dbo.tbAccount (AccountID)

if  not exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountEmailEmailID'))
alter table dbo.tbAccountEmail with check add constraint fkAccountEmailEmailID foreign key(EmailID)
references dbo.tbEmail (EmailID)

if  not exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountEmailEmailType'))
alter table dbo.tbAccountEmail with check add constraint fkAccountEmailEmailType foreign key(EmailType)
references dbo.tbEmailType (EmailType)

--dbo.tbAccountPhone
if  not exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountPhoneAccountID'))
alter table dbo.tbAccountPhone with check add constraint fkAccountPhoneAccountID foreign key(AccountID)
references dbo.tbAccount (AccountID)

if  not exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountPhonePhoneID'))
alter table dbo.tbAccountPhone with check add constraint fkAccountPhonePhoneID foreign key(PhoneID)
references dbo.tbPhone (PhoneID)

if  not exists (select * from sys.foreign_keys where object_id = object_id(N'fkAccountPhonePhoneType'))
alter table dbo.tbAccountPhone with check add constraint fkAccountPhonePhoneType foreign key(PhoneType)
references dbo.tbPhoneType (PhoneType)

--dbo.tbUser
if  not exists (select * from sys.foreign_keys where object_id = object_id(N'fkUserStatus'))
alter table dbo.tbUser with check add constraint fkUserStatus foreign key(Status)
references dbo.tbStatus (Status)

