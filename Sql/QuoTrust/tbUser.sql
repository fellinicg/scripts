use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbUser') and type in (N'U'))
drop table dbo.tbUser

create table dbo.tbUser(
	UserID int identity(1,1) not null,
	UserName varchar(20) not null,
	PrivateKey varchar(200) not null,
	Status char(1) not null,
	CreatedTS datetime not null constraint dfUserCreatedTS default (getdate()),
	UpdatedTS datetime not null constraint dfUserUpdatedTS default (getdate()),
 constraint pkUserID primary key clustered 
(
	UserID asc
)
)

alter table dbo.tbUser add constraint ucUserName unique (UserName) 

set ansi_padding off