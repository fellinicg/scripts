use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbRequestResponseRating') and type in (N'U'))
drop table dbo.tbRequestResponseRating

create table dbo.tbRequestResponseRating(
	RequestResponseRatingID int identity(1,1) not null,
	RequestID int not null,
	AccountID int not null,
	Rating char(1) not null,
	CreatedTS datetime not null constraint dfRequestResponseRatingCreatedTS default (getdate()),
 constraint pkRequestResponseRating primary key clustered 
(
	RequestResponseRatingID asc
)
)

set ansi_padding off

alter table dbo.tbRequestResponseRating with check add constraint [tcRating] check 
(Rating = 'A' or Rating = 'B' or Rating = 'C' or Rating = 'D' or Rating = 'F')
