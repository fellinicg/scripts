use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAccountType') and type in (N'U'))
drop table dbo.tbAccountType

create table dbo.tbAccountType(
	AccountType char(1) not null,
	Description varchar(40) not null,
 constraint pkAccountType primary key clustered 
(
	AccountType asc
)
)

set ansi_padding off