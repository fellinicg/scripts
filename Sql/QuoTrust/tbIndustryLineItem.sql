use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbIndustryLineItem') and type in (N'U'))
drop table dbo.tbIndustryLineItem

create table dbo.tbIndustryLineItem(
	IndustryLineItemID int identity(1,1) not null,
	IndustryID int not null,
	LineItem varchar(1000) not null,
	IsDeleted bit not null constraint dfIndustryLineItemIsDeleted default ((0)),
	CreatedTS datetime not null constraint dfIndustryLineItemCreatedTS default (getdate()),
	UpdatedTS datetime not null constraint dfIndustryLineItemUpdatedTS default (getdate()),
 constraint pkIndustryLineItem primary key clustered 
(
	IndustryLineItemID asc
)
)

set ansi_padding off