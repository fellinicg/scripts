use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbRequest') and type in (N'U'))
drop table dbo.tbRequest

create table dbo.tbRequest(
	RequestID int identity(1,1) not null,
	AccountID int not null,
	Name varchar(100) not null,
	Description varchar(2000) not null,
	IsDeleted bit not null constraint dfRequestIsDeleted default ((0)),
	CreatedTS datetime not null constraint dfRequestCreatedTS default (getdate()),
 constraint pkRequest primary key clustered 
(
	RequestID asc
)
)

set ansi_padding off