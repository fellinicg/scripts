use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbRfpVendorQuestion') and type in (N'U'))
drop table dbo.tbRfpVendorQuestion

create table dbo.tbRfpVendorQuestion(
	RfpVendorQuestionID int identity(1,1) not null,
	RfpResponseID int not null,
	Question varchar(200) not null,
	Answer varchar(2000) null,
	CreatedTS datetime not null constraint dfRfpVendorQuestionCreatedTS default (getdate()),
 constraint pkRfpVendorQuestion primary key clustered 
(
	RfpVendorQuestionID asc
)
)

set ansi_padding off