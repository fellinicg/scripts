--declare @fks table(id int identity(1,1), fk varchar(200))
--declare @fks table(fk varchar(200), tbl varchar(200))
declare @drop table(id int identity(1,1), cmd varchar(2000))
declare @cmd varchar(2000)

insert into @drop
select 'alter table ' + o.name + ' drop ' + f.name
from sys.foreign_keys f
inner join sys.objects o on f.parent_object_id = o.object_id

select * from @drop

while exists(select 1 from @drop)
begin
	select top 1 @cmd = cmd from @drop order by id
	exec(@cmd)
	delete from @drop where id = (select top 1 id from @drop order by id)
end

select * from sys.foreign_keys