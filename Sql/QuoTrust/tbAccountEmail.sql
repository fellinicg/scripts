use QuoTrust

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAccountEmail') and type in (N'U'))
drop table dbo.tbAccountEmail

create table dbo.tbAccountEmail(
	AccountEmailID int identity(1,1) not null,
	AccountID int not null,
	EmailID int not null,
	EmailType char(1) not null,
	IsDeleted bit not null constraint dfAccountEmailIsDeleted default ((0)),
	CreatedTS datetime not null constraint dfAccountEmailCreatedTS default (getdate()),
 constraint pkAccountEmail primary key clustered 
(
	AccountEmailID asc
)
)

alter table dbo.tbAccountEmail add constraint ucAccountEmail unique (AccountID, EmailID, EmailType)

set ansi_padding off