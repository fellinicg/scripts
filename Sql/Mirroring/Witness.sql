-- Witness Server

use master

drop endpoint Mirroring
go

create endpoint Mirroring state = started as tcp (listener_port = 5023) 
	for database_mirroring (role=witness);
go

if not exists(select name from master.dbo.syslogins where name = 'PANYNJ\SQLServer_SVC')
create login [PANYNJ\SQLServer_SVC] from windows;
go

grant connect on endpoint::Mirroring to [PANYNJ\SQLServer_SVC]

--alter database PomeroyMgmt set witness off
--alter database PomeroyMgmt set witness = 'TCP://PATCAV73:5023'
