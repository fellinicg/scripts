-- Principal/Mirror Server

use master

drop endpoint Mirroring
go

create endpoint Mirroring state = started as tcp (listener_port = 5022) 
	for database_mirroring (role=partner);
go

if not exists(select name from master.dbo.syslogins where name = 'PANYNJ\SQLServer_SVC')
create login [PANYNJ\SQLServer_SVC] from windows;
go

grant connect on endpoint::Mirroring to [PANYNJ\SQLServer_SVC]

-- On Mirror
alter database xxxx set partner = N'TCP://PRINCIPLE:5022'

-- On Principle
alter database xxxx set partner = N'TCP://MIRROR:5022'

-- High Performance Async
alter database xxxx set safety off
