-- Break mirror

-- On mirror
restore database mediaspan with recovery

-- Delete database on mirror

-- Restore database on mirror with recovery
alter database mediaspan set partner off = 'tcp://patcdb12:7023'


select * from sys.database_mirroring
select * from sys.database_mirroring_witnesses
select * from sys.database_mirroring_endpoints
