USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbAccountType]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbAccountType]
GO
CREATE TABLE [dbo].[tbAccountType](
	[AccountType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AccountTypeDesc] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [pkAccountType] PRIMARY KEY CLUSTERED 
(
	[AccountType] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF