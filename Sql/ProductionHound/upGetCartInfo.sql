USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071010
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071010
-- ============================================================
ALTER PROCEDURE dbo.upGetCartInfo
(
	@UserAccountID int
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Return records
	SELECT 
		ISNULL(SUM(c.Quantity), 0) NumItems
		,ISNULL(SUM((i.Price * c.Quantity) * CASE OrderType WHEN 'R' THEN dbo.sfNumWeeks(RentalStartDate, RentalEndDate) ELSE 1 END), 0) SubTotal
	FROM dbo.tbCartItem c
		INNER JOIN dbo.tbItem i ON c.ItemID = i.ItemID
	WHERE c.UserAccountID = @UserAccountID
		AND i.IsDeleted = 0

END
