USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbCartItem](
	[CartItemID] [int] IDENTITY(1,1) NOT NULL,
	[UserAccountID] [int] NOT NULL,
	[ItemID] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[OrderType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[RentalStartDate] [datetime] NULL,
	[RentalEndDate] [datetime] NULL,
	[CreateDate] [datetime] NOT NULL CONSTRAINT [DF_tbCartItem_CreateDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbCartItem] PRIMARY KEY CLUSTERED 
(
	[CartItemID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF