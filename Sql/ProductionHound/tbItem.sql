USE [phound_dev]
GO
/****** Object:  Table [dbo].[tbItem]    Script Date: 05/04/2007 13:47:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbItem]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.tbItem
GO
CREATE TABLE dbo.tbItem(
	[ItemID] [int] IDENTITY(1,1) NOT NULL,
	[Item] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorID] [int] NOT NULL,
	[VendorItemID] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Price] [smallmoney] NOT NULL,
  [Quantity] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbItem_IsDeleted]  DEFAULT (0),
 CONSTRAINT [PK_tbItem] PRIMARY KEY CLUSTERED 
(
	[ItemID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF