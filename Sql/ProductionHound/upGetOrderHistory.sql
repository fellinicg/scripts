USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071030
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071030
-- ============================================================
ALTER PROCEDURE dbo.upGetOrderHistory
(
	@UserAccountID int
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Return records
	SELECT OrderId, CONVERT(varchar, CreateDate, 100) CreateDate
	FROM dbo.tbOrder
	WHERE UserAccountID = @UserAccountID
		AND Status <> 'V'
	ORDER BY CreateDate DESC

END
