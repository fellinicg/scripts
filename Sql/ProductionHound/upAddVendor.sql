SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20070919
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20070919
-- ============================================================
ALTER PROCEDURE dbo.upAddVendor
(
	@Company varchar(100),
	@Address1 varchar(55), 
	@Address2 varchar(55) = null, 
	@City varchar(30), 
	@State char(2), 
	@Zip char(5), 
	@Phone varchar(20),
	@Status char(1) = 'P',
	@UserAccountID int = null,
	@VendorID int output,
	@Result char(1) output
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @VendorID = 0
	SET @Result = 'F'

	-- Insert record
	INSERT INTO dbo.tbVendor (Company, Address1, Address2, City, State, Zip, Phone, Status)
	VALUES (@Company, @Address1, @Address2, @City, @State, @Zip, @Phone, @Status)

	-- Check for success
	IF (@@ERROR = 0) AND (@@ROWCOUNT > 0)
		BEGIN
			SET @VendorID = @@IDENTITY
			SET @Result = 'S'
		END
	ELSE RETURN

	-- Add user account if provided and is of type vendor
	IF (@UserAccountID IS NOT NULL)
		INSERT INTO dbo.tbVendorUserAcount (VendorID, UserAccountID)
		VALUES (@VendorID, @UserAccountID)

END
