USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071010
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071010
-- ============================================================
CREATE PROCEDURE dbo.upClearUsersCart
(
	@UserAccountID int
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Delete records from tbCartItem
	DELETE FROM dbo.tbCartItem WHERE UserAccountID = @UserAccountID

END
