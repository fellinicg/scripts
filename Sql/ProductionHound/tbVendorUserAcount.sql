USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbVendorUserAcount](
	[VendorID] [int] NOT NULL,
	[UserAccountID] [int] NOT NULL,
 CONSTRAINT [PK_tbVendorUserAcount] PRIMARY KEY CLUSTERED 
(
	[VendorID] ASC,
	[UserAccountID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[tbVendorUserAcount]  WITH CHECK ADD  CONSTRAINT [CK_UserIsVendor] CHECK  (([dbo].[sfUserIsVendor]([UserAccountID]) = 1))