USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbKeywordItemWorking](
	[KeywordID] [int] NOT NULL,
	[ItemID] [int] NOT NULL,
	[Position] [int] NOT NULL,
	[IDX] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
