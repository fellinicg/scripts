USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071030
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071030
-- ============================================================
ALTER PROCEDURE dbo.upGetOrderInfo
(
	@OrderID int
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Return records
	SELECT 
		o.OrderID
		,ISNULL(o.FirstName, '') + ' ' + ISNULL(o.LastName, '') OrderedBy
		,ISNULL(o.Address1, '') Address1
		,ISNULL(o.Address2, '') Address2
		,ISNULL(o.City, '') City
		,ISNULL(o.State, '') State
		,ISNULL(o.Zip, '') Zip
		,ol.TotalCost
		,ol.Quantity
	FROM dbo.tbOrder o
		INNER JOIN (SELECT OrderID, SUM(TotalCost) TotalCost, SUM(Quantity) Quantity
								FROM dbo.tbOrderLine
								WHERE OrderID = @OrderID AND Status <> 'V'
								GROUP BY OrderID) ol ON o.OrderID = ol.OrderID
	WHERE o.OrderID = @OrderID
		AND o.Status <> 'V'

END
