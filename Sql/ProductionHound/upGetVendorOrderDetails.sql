USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071116
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071116
-- ============================================================
ALTER PROCEDURE dbo.upGetVendorOrderDetails
(
	@OrderID int,
	@VendorID int
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Return records
	SELECT 
		ol.OrderLineID
		,i.VendorItemID
		,i.Item
		,ol.Price
		,ol.Quantity
		,ol.TotalCost
		,CONVERT(varchar, ol.RentalStartDate, 101) RentalStartDate
		,CONVERT(varchar, ol.RentalEndDate, 101) RentalEndDate
		,ol.Status
	FROM dbo.tbOrderLine ol 
	INNER JOIN dbo.tbItem i ON ol.ItemID = i.ItemID
	WHERE ol.OrderID = @OrderID
		AND ol.VendorID = @VendorID
	ORDER BY OrderLineID

END
