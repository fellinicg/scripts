USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20070913
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20070913
-- ============================================================
ALTER PROCEDURE dbo.upItemDetail(@ItemID int)
AS
BEGIN

  -- Initialize settings
	SET NOCOUNT ON

  -- Return results
  SELECT i.ItemID, i.Item, i.Price, i.Description, ISNULL(ii.ImageFile, 'notavail.jpg') ImageFile, 
		ISNULL(ii.Width, '160px') Width, ISNULL(ii.Height, '160px') Height, ISNULL(v.Company, '') Vendor, c.Category, 'Yes' IsAvailable
  FROM dbo.tbItem i
	LEFT JOIN dbo.tbVendor v ON i.VendorID = v.VendorID
	INNER JOIN dbo.tbCategory c ON i.CategoryID = c.CategoryID
	LEFT JOIN dbo.tbItemImage ii ON i.ItemID = ii.ItemID AND IsDefault = 1 AND Size = 'medium'
	WHERE i.ItemID = @ItemID

END
