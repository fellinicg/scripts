USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ManhMed_Import]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.ManhMed_Import
GO
CREATE TABLE [dbo].[ManhMed_Import](
	[ItemID] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Item] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Category] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Keywords] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description] [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Price] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Quantity] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF