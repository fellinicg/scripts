USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20070924
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20070924
-- ============================================================
CREATE PROCEDURE dbo.upCheckLoginName
(
	@LoginName varchar(20), 
	@Result char(1) output
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'A'

	-- Check for existing login name
	SELECT @Result = CASE 1 WHEN 1 THEN 'U' ELSE 'A' END
	FROM dbo.tbUserAccount 
		WHERE LoginName = @LoginName

END
