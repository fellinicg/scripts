USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20070923
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20070923
-- ============================================================
ALTER PROCEDURE dbo.upAuthenticateUser
(
	@LoginName varchar(20), 
	@PasswordCrypted binary,
	@FirstName varchar(30) output,
	@LastName varchar(50) output,
	@AccountType char(1) output,
	@UserAccountID int output,
	@VendorID int output,
	@Result char(1) output
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'
	SET @AccountType = 'X'

	-- Get record
	SELECT @FirstName = a.FirstName, @LastName = a.LastName, @AccountType = a.AccountType, 
		@UserAccountID = a.UserAccountID, @VendorID = ISNULL(v.VendorID, 0)
	FROM dbo.tbUserAccount a 
	LEFT JOIN dbo.tbVendorUserAcount v ON a.UserAccountID = v.UserAccountID
	WHERE a.LoginName = @LoginName
		AND a.PasswordCrypted = @PasswordCrypted

	-- Check for success
	IF (@@ERROR = 0) AND (@@ROWCOUNT > 0)
		SET @Result = 'S'

END
