USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20070914
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20070919
-- ============================================================
ALTER PROCEDURE dbo.upLogin
(
	@Username varchar(20),
	@Password varchar(20),
	@LoginID int output
)
AS
BEGIN

  -- Initialize settings
	SET NOCOUNT ON

  -- Return results
	SELECT @LoginID = LoginID
	FROM dbo.tbLogin
	WHERE Username = @Username
		AND Password = @Password
		AND IsActive = 1

END
