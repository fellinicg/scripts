USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION dbo.sfUserIsVendor (@UserAccountID int)
	RETURNS int AS
BEGIN
	DECLARE @Ret int

	SET @Ret = 0

	SELECT @Ret = 1 FROM dbo.tbUserAccount
	WHERE UserAccountID = @UserAccountID
		AND AccountType = 'V'

	RETURN(@Ret)
END
