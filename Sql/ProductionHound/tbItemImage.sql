USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbItemImage](
	[ItemImageID] [int] IDENTITY(1,1) NOT NULL,
	[ItemID] [int] NOT NULL,
	[ImageFile] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Size] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Width] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Height] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsDefault] [bit] NOT NULL CONSTRAINT [DF_tbItemImage_IsDefault]  DEFAULT (0),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbItemImage_IsDeleted]  DEFAULT (0),
 CONSTRAINT [PK_tbItemImage] PRIMARY KEY CLUSTERED 
(
	[ItemImageID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF