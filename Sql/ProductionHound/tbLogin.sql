USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbLogin](
	[LoginID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Password] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF