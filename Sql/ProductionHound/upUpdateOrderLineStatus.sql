USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071119
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071119
-- ============================================================
ALTER PROCEDURE dbo.upUpdateOrderLineStatus
(
	@OrderLineID int,
	@Status char(1),
	@Result char(1) output

)
AS
BEGIN

	-- Declarations
	DECLARE @OrderID int
	DECLARE @UpdateOrder bit

	-- Initialize settings
	SET NOCOUNT ON
	SET @Result = 'S'
	SET @UpdateOrder = 0

	-- Update record
	UPDATE dbo.tbOrderLine SET Status = @Status
	WHERE OrderLineID = @OrderLineID

	-- Get order info
	SELECT 
		@UpdateOrder = MIN(CASE ord.Status WHEN 'P' THEN 0 ELSE 1 END),
		@Status = CASE 
			WHEN COUNT(ord.Status) = SUM(CASE ord.Status WHEN 'V' THEN 1 ELSE 0 END) THEN 'V' 
			WHEN COUNT(ord.Status) = SUM(CASE ord.Status WHEN 'C' THEN 1 ELSE 0 END) THEN 'C' 
			ELSE 'O' END,
		@OrderID = MAX(ord.OrderID)
	FROM dbo.tbOrderLine ol
		INNER JOIN dbo.tbOrderLine ord ON ol.OrderID = ord.OrderID 
	WHERE ol.OrderLineID = @OrderLineID

	-- Update status of order if necessary
	IF (@UpdateOrder = 1)
		UPDATE dbo.tbOrder SET Status = @Status
		WHERE OrderID = @OrderID

END
