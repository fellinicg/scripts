SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20070928
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20070928
-- ============================================================
CREATE PROCEDURE dbo.upAddCartItem
(
	@UserAccountID int,
	@ItemID int,
	@Quantity int,
	@OrderType char(1),
	@RentalStartDate datetime,
	@RentalEndDate datetime,
	@CartItemID int output,
	@Result char(1) output
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @CartItemID = 0
	SET @Result = 'F'

	-- Insert record
	INSERT INTO dbo.tbCartItem (UserAccountID, ItemID, Quantity, OrderType, RentalStartDate, RentalEndDate)
	VALUES (@UserAccountID, @ItemID, @Quantity, @OrderType, @RentalStartDate, @RentalEndDate)

	-- Check for success
	IF (@@ERROR = 0) AND (@@ROWCOUNT > 0)
		BEGIN
			SET @CartItemID = @@IDENTITY
			SET @Result = 'S'
		END

END
