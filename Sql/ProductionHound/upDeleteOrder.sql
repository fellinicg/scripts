USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071010
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071010
-- ============================================================
CREATE PROCEDURE dbo.upDeleteOrder
(
	@OrderID int
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Delete record from tbOrderLine
	DELETE FROM dbo.tbOrderLine WHERE OrderID = @OrderID

	-- Delete record from tbOrder
	DELETE FROM dbo.tbOrder WHERE OrderID = @OrderID

END
