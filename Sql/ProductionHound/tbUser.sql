USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbUser]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbUser]
GO
CREATE TABLE [dbo].[tbUser](
	[UserID] [int] NOT NULL,
	[FirstName] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LastName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Company] [varchar](55) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Address1] [varchar](55) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Address2] [varchar](55) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[City] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[State] [char](6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Zip] [char](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Email] [varchar](120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Phone] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsDeleted] [bit] NOT NULL DEFAULT (0),
	[CreateDateTime] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [pkEmployee] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF