USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071030
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071030
-- ============================================================
ALTER PROCEDURE dbo.upGetOrderDetails
(
	@OrderID int
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Return records
	SELECT 
		i.Item
		,ol.Price
		,ol.Quantity
		,ol.TotalCost
		,CONVERT(varchar, ol.RentalStartDate, 101) RentalStartDate
		,CONVERT(varchar, ol.RentalEndDate, 101) RentalEndDate
	FROM dbo.tbOrderLine ol 
	INNER JOIN dbo.tbItem i ON ol.ItemID = i.ItemID
	WHERE ol.OrderID = @OrderID
		AND ol.STatus <> 'V'
	ORDER BY OrderLineID

END
