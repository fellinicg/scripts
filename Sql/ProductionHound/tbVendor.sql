USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbVendor](
	[VendorID] [int] IDENTITY(1,1) NOT NULL,
	[Company] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Address1] [varchar](55) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Address2] [varchar](55) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[City] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[State] [char](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Zip] [char](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Phone] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbVendor_IsDeleted]  DEFAULT (0),
	[CreateDate] [datetime] NOT NULL CONSTRAINT [DF_tbVendor_CreateDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbVendor] PRIMARY KEY CLUSTERED 
(
	[VendorID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF