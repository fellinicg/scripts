USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20070504
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
ALTER PROCEDURE dbo.upGetManhMedData
AS
BEGIN

	SET NOCOUNT ON

  TRUNCATE TABLE dbo.ManhMed_Import

  INSERT INTO dbo.ManhMed_Import (ItemID, Item, Category, Keywords, Description, Price, Quantity)
  SELECT a.REFNUM, a.ITEM, a.Category, a.Keywords, a.[DESC], a.PRICE, a.Qty
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0', 
     'Excel 8.0;Database=D:\Corporations\Production Hound\data\manmed.xls', 'SELECT * FROM [subset$]') 
     AS a

  TRUNCATE TABLE dbo.tbKeyword

  INSERT INTO dbo.tbKeyword (Keyword)
  SELECT DISTINCT Keyword
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0', 
     'Excel 8.0;Database=D:\Corporations\Production Hound\data\manmed.xls', 'SELECT * FROM [searchwords$]')
  WHERE Keyword IS NOT NULL
  ORDER BY Keyword


  TRUNCATE TABLE dbo.tbItem

  INSERT INTO dbo.tbItem (Item, Description, VendorID, VendorItemID, Price, Quantity)
  SELECT Item, Description, 1, ItemID, convert(smallmoney, Price), Quantity
  FROM dbo.ManhMed_Import

END