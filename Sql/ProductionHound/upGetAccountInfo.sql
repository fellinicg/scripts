USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071002
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071002
-- ============================================================
CREATE PROCEDURE dbo.upGetAccountInfo
(
	@UserAccountID int
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Return records
	SELECT 
		FirstName
		,LastName
		,ISNULL(Company, '') Company
		,ISNULL(Address1, '') Address1
		,ISNULL(Address2, '') Address2
		,ISNULL(City, '') City
		,ISNULL(State, '') State
		,ISNULL(Zip, '') Zip
		,ISNULL(Email, '') Email
		,ISNULL(Phone, '') Phone
		,ISNULL(SecurityQuestion, '') SecurityQuestion
		,ISNULL(SecurityAnswer, '') SecurityAnswer
	FROM dbo.tbUserAccount
	WHERE UserAccountID = @UserAccountID

END
