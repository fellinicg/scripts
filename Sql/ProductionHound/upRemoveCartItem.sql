USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071002
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071002
-- ============================================================
ALTER PROCEDURE dbo.upRemoveCartItem
(
	@CartItemID int
	,@SubTotal money output
	,@ItemsLeftInCart int output
	,@Result char(1) output
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'F'
	SET @ItemsLeftInCart = 0

	-- Get number of items in users cart, excluding the item sent iin
	SELECT @ItemsLeftInCart = ISNULL(SUM(b.Quantity), 0),
		@SubTotal = ISNULL(SUM((i.Price * b.Quantity) * 
								CASE b.OrderType WHEN 'R' THEN dbo.sfNumWeeks(b.RentalStartDate, b.RentalEndDate) ELSE 1 END), 0) 
	FROM dbo.tbCartItem a
	INNER JOIN dbo.tbCartItem b ON a.UserAccountID = b.UserAccountID
	INNER JOIN dbo.tbItem i ON b.ItemID = i.ItemID
	WHERE a.CartItemID = @CartItemID
		AND b.CartItemID <> @CartItemID
		AND i.IsDeleted = 0

	-- Delete record
	DELETE FROM dbo.tbCartItem
	WHERE CartItemID = @CartItemID

	-- Check for success
	IF (@@ERROR = 0) AND (@@ROWCOUNT > 0)
		SET @Result = 'S'

END
