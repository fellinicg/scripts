USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbOrderLine](
	[OrderLineID] [int] IDENTITY(1,1) NOT NULL,
	[OrderID] [int] NOT NULL,
	[ItemID] [int] NOT NULL,
	[VendorID] [int] NOT NULL,
	[Price] [money] NOT NULL,
	[Quantity] [int] NOT NULL,
	[OrderType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[TotalCost] [money] NOT NULL,
	[RentalStartDate] [datetime] NULL,
	[RentalEndDate] [datetime] NULL,
	[Status] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_tbOrderLine_Status]  DEFAULT ('P'),
	[CreateDate] [datetime] NOT NULL CONSTRAINT [DF_tbOrderLine_CreateDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbOrderLine] PRIMARY KEY CLUSTERED 
(
	[OrderLineID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF