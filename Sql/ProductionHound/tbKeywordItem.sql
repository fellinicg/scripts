USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbKeywordItem](
	[KeywordID] [int] NOT NULL,
	[ItemID] [int] NOT NULL,
	[Hits] [int] NOT NULL,
 CONSTRAINT [PK_tbKeywordItem] PRIMARY KEY CLUSTERED 
(
	[KeywordID] ASC,
	[ItemID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
