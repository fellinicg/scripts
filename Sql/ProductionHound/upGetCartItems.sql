USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071001
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071001
-- ============================================================
ALTER PROCEDURE dbo.upGetCartItems
(
	@UserAccountID int
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Return records
	SELECT 
		c.CartItemID
		,c.ItemID
		,i.Item
		,i.Price
		,c.Quantity
		,CASE OrderType WHEN 'R' THEN dbo.sfNumWeeks(RentalStartDate, RentalEndDate) ELSE 1 END NumWeeks
		,ISNULL(ii.ImageFile, 'notavail.jpg') ImageFile
		,ISNULL(ii.Width, '75px') Width
		,ISNULL(ii.Height, '75px') Height
		,v.Company
		,CONVERT(varchar, c.RentalStartDate, 101) RentalStartDate
		,CONVERT(varchar, c.RentalEndDate, 101) RentalEndDate
	FROM dbo.tbCartItem c
		INNER JOIN dbo.tbItem i ON c.ItemID = i.ItemID
		INNER JOIN dbo.tbVendor v ON i.VendorID = v.VendorID
		LEFT JOIN dbo.tbItemImage ii ON i.ItemID = ii.ItemID AND ii.IsDefault = 1 AND ii.Size = 'small'
	WHERE c.UserAccountID = @UserAccountID
		AND i.IsDeleted = 0

END
