USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20070928
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20070928
-- ============================================================
CREATE PROCEDURE dbo.upCheckItemAvailability
(
	@ItemID int,
	@RentalStartDate datetime,
	@RentalEndDate datetime,
	@Result char(1) output
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @Result = 'A'

	-- Check data
	SELECT @Result = 'U'
	FROM dbo.tbItem i 
	INNER JOIN (SELECT ol.ItemID, SUM(ol.Quantity) TotalQty
							FROM dbo.tbOrderLine ol
							WHERE ol.ItemID = @ItemID
								AND (@RentalStartDate BETWEEN ol.RentalStartDate AND ol.RentalEndDate 
											OR @RentalEndDate BETWEEN ol.RentalStartDate AND ol.RentalEndDate 
											OR ol.RentalStartDate BETWEEN @RentalStartDate AND @RentalEndDate 
											OR ol.RentalEndDate BETWEEN @RentalStartDate AND @RentalEndDate)
							GROUP BY ol.ItemID) ol ON ol.ItemID = i.ItemID
	WHERE TotalQty >= i.Quantity

END
