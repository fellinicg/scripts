USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20070601
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 200700910
-- ============================================================
ALTER PROCEDURE dbo.upItemSearch(@SearchStr varchar(3000))
AS
BEGIN

  -- Initialize settings
	SET NOCOUNT ON

  -- Replace single qoutes
  SET @SearchStr = replace(@SearchStr, char(34), char(34)+char(34))

  -- Return results
  SELECT i.ItemID, i.Item, i.Price, i.Description, ISNULL(ii.ImageFile, 'notavail.jpg') ImageFile, 
		ISNULL(ii.Width, '75px') Width, ISNULL(ii.Height, '75px') Height, ISNULL(v.Company, '') Vendor, c.Category, 'Yes' IsAvailable
  FROM dbo.tbItem i
  INNER JOIN (SELECT ki.ItemID, SUM(ki.Hits) Hits
              FROM dbo.tfSplit(@SearchStr, ' ') s
              INNER JOIN dbo.tbKeyword k ON s.Items = k.Keyword
              INNER JOIN dbo.tbKeywordItem ki ON k.KeywordID = ki.KeywordID
              GROUP BY ki.ItemID) sr ON i.ItemID = sr.ItemID
	LEFT JOIN dbo.tbVendor v ON i.VendorID = v.VendorID
	INNER JOIN dbo.tbCategory c ON i.CategoryID = c.CategoryID
	LEFT JOIN dbo.tbItemImage ii ON i.ItemID = ii.ItemID AND IsDefault = 1 AND Size = 'small'
  WHERE i.IsDeleted = 0
  ORDER BY sr.Hits DESC
	
END