USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20070530
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upPopulateCategories
AS
BEGIN

	SET NOCOUNT ON

  INSERT INTO dbo.tbCategory (Category)
  SELECT DISTINCT Category
  FROM dbo.ManhMed_Import
  ORDER BY Category

END