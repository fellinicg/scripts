SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20070919
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20070919
-- ============================================================
ALTER PROCEDURE dbo.upAddUser
(
	@FirstName varchar(30), 
	@LastName varchar(50), 
	@Company varchar(55) = null, 
	@Address1 varchar(55) = null, 
	@Address2 varchar(55) = null, 
	@City varchar(30) = null, 
	@State char(2) = null, 
	@Zip char(5) = null, 
	@Email varchar(120), 
	@Phone varchar(20) = null, 
	@LoginName varchar(20), 
	@PasswordCrypted binary, 
	@SecurityQuestion varchar(40) = null, 
	@SecurityAnswer varchar(40) = null, 
	@AccountType char(1),
	@Status char(1),
	@UserAccountID int output,
	@Result char(1) output
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @UserAccountID = 0
	SET @Result = 'F'

	-- Insert record
	INSERT INTO dbo.tbUserAccount (FirstName, LastName, Company, Address1, Address2, 
		City, State, Zip, Email, Phone, LoginName, PasswordCrypted, SecurityQuestion, SecurityAnswer, 
		AccountType, Status)
	VALUES (@FirstName, @LastName, @Company, @Address1, @Address2, 
		@City, @State, @Zip, @Email, @Phone, @LoginName, @PasswordCrypted, @SecurityQuestion, @SecurityAnswer, 
		@AccountType, @Status)

	-- Check for success
	IF (@@ERROR = 0) AND (@@ROWCOUNT > 0)
		BEGIN
			SET @UserAccountID = @@IDENTITY
			SET @Result = 'S'
		END

END
