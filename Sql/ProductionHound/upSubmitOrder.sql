USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071010
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071010
-- ============================================================
ALTER PROCEDURE dbo.upSubmitOrder
(
	@UserAccountID int,
	@FirstName varchar(30), 
	@LastName varchar(50), 
	@Company varchar(55) = null, 
	@Address1 varchar(55) = null, 
	@Address2 varchar(55) = null, 
	@City varchar(30) = null, 
	@State char(2) = null, 
	@Zip char(5) = null, 
	@OrderID int output,
	@Result char(1) output
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	SET @OrderID = 0
	SET @Result = 'F'

	-- Insert record into tbOrder
	INSERT INTO dbo.tbOrder (UserAccountID, FirstName, LastName, Address1, Address2, 
		City, State, Zip)
	VALUES (@UserAccountID, @FirstName, @LastName, @Address1, @Address2,
		@City, @State, @Zip)

	-- Check for success
	IF (@@ERROR = 0) AND (@@ROWCOUNT > 0)
		SET @OrderID = @@IDENTITY
	ELSE
		RETURN

	-- Insert record into tbOrderLine
	INSERT INTO dbo.tbOrderLine (OrderID, ItemID, VendorID, Price, Quantity, OrderType, 
		TotalCost, RentalStartDate, RentalEndDate)
	SELECT @OrderID, c.ItemID, v.VendorID, i.Price, c.Quantity, OrderType,
		(i.Price * c.Quantity) * CASE OrderType WHEN 'R' THEN dbo.sfNumWeeks(RentalStartDate, RentalEndDate) ELSE 1 END
		,c.RentalStartDate, c.RentalEndDate
	FROM dbo.tbCartItem c
		INNER JOIN dbo.tbItem i ON c.ItemID = i.ItemID
		INNER JOIN dbo.tbVendor v ON i.VendorID = v.VendorID
	WHERE c.UserAccountID = @UserAccountID
		AND i.IsDeleted = 0

	-- Check for success
	IF (@@ERROR = 0) AND (@@ROWCOUNT > 0)
		SET @Result = 'S'
	ELSE
		BEGIN
			EXEC dbo.upDeleteOrder @OrderID
			SET @OrderID = 0
		END

	-- Clear users cart
	EXEC dbo.upClearUsersCart @UserAccountID

END
