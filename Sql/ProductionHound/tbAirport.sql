USE [SabreConnect]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbAirport](
	[Code] [char](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[City] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Province] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Airport] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Country] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_tbAirport] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF