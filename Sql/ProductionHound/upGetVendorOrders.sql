USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20071101
-- LastEditedByFullName: Todd Fellini
-- LastEditDate: 20071101
-- ============================================================
ALTER PROCEDURE dbo.upGetVendorOrders
(
	@VendorID int
	,@Status char(1)
)
AS
BEGIN

	-- Initialize settings
	SET NOCOUNT ON

	-- Return records
	SELECT o.OrderID, os.Description, o.FirstName, o.LastName, SUM(ol.TotalCost) TotalCost, CONVERT(varchar, o.CreateDate, 100) DateSubmitted
	FROM dbo.tbOrderLine ol
		INNER JOIN dbo.tbOrder o ON ol.OrderID = o.OrderID
		INNER JOIN dbo.tbOrderStatus os ON o.Status = os.OrderStatus
	WHERE ol.VendorID = @VendorID
		AND o.Status = @Status
	GROUP BY o.OrderID, os.Description, o.FirstName, o.LastName, o.CreateDate
	ORDER BY o.CreateDate DESC

END
