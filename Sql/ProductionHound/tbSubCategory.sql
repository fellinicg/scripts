USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbSubCategory](
	[SubCategoryID] [int] NOT NULL,
	[SubCategory] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CategoryID] [int] NOT NULL,
 CONSTRAINT [PK_tbSubCategory] PRIMARY KEY CLUSTERED 
(
	[SubCategoryID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF