USE [phound_dev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION dbo.sfNumWeeks (@StartDate datetime, @EndDate datetime)
	RETURNS int AS
BEGIN
	DECLARE @Ret int

	SET @Ret = 0

	SET @Ret = (DATEPART(wk, @EndDate) - DATEPART(wk, @StartDate)) + 1

	RETURN(@Ret)
END
