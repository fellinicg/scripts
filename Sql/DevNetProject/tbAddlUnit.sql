USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbAddlUnit_tbAddlUnitType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbAddlUnit]'))
ALTER TABLE [dbo].[tbAddlUnit] DROP CONSTRAINT [FK_tbAddlUnit_tbAddlUnitType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbAddlUnit_tbProject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbAddlUnit]'))
ALTER TABLE [dbo].[tbAddlUnit] DROP CONSTRAINT [FK_tbAddlUnit_tbProject]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbAddlUnitAttribute_tbAddlUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbAddlUnitAttribute]'))
ALTER TABLE [dbo].[tbAddlUnitAttribute] DROP CONSTRAINT [FK_tbAddlUnitAttribute_tbAddlUnit]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbAddlUnitCharge_tbAddlUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbAddlUnitCharge]'))
ALTER TABLE [dbo].[tbAddlUnitCharge] DROP CONSTRAINT [FK_tbAddlUnitCharge_tbAddlUnit]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractAddlUnit_tbAddlUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractAddlUnit]'))
ALTER TABLE [dbo].[tbContractAddlUnit] DROP CONSTRAINT [FK_tbContractAddlUnit_tbAddlUnit]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbAddlUnit]') AND type in (N'U'))
DROP TABLE [dbo].[tbAddlUnit]
GO
CREATE TABLE [dbo].[tbAddlUnit](
	[AddlUnitID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [int] NOT NULL,
	[AddlUnitTypeID] [int] NOT NULL,
	[Code] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SquareFeet] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbAddlUnit_IsDeleted]  DEFAULT ((0)),
	[LastModifiedBy] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbAddlUnit_CreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbAddlUnit_UpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbAddlUnit] PRIMARY KEY CLUSTERED 
(
	[AddlUnitID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbAddlUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbAddlUnit_tbAddlUnitType] FOREIGN KEY([AddlUnitTypeID])
REFERENCES [dbo].[tbAddlUnitType] ([AddlUnitTypeID])
GO
ALTER TABLE [dbo].[tbAddlUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbAddlUnit_tbProject] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[tbProject] ([ProjectID])
GO
ALTER TABLE [dbo].[tbAddlUnitAttribute]  WITH CHECK ADD  CONSTRAINT [FK_tbAddlUnitAttribute_tbAddlUnit] FOREIGN KEY([AddlUnitID])
REFERENCES [dbo].[tbAddlUnit] ([AddlUnitID])
GO
ALTER TABLE [dbo].[tbAddlUnitCharge]  WITH CHECK ADD  CONSTRAINT [FK_tbAddlUnitCharge_tbAddlUnit] FOREIGN KEY([AddlUnitID])
REFERENCES [dbo].[tbAddlUnit] ([AddlUnitID])
GO
ALTER TABLE [dbo].[tbContractAddlUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbContractAddlUnit_tbAddlUnit] FOREIGN KEY([AddlUnitID])
REFERENCES [dbo].[tbAddlUnit] ([AddlUnitID])