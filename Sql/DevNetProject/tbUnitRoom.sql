USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbUnitRoom_tbUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUnitRoom]'))
ALTER TABLE [dbo].[tbUnitRoom] DROP CONSTRAINT [FK_tbUnitRoom_tbUnit]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbUnitRoom_tbRoom]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUnitRoom]'))
ALTER TABLE [dbo].[tbUnitRoom] DROP CONSTRAINT [FK_tbUnitRoom_tbRoom]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbUnitRoom]') AND type in (N'U'))
DROP TABLE [dbo].[tbUnitRoom]
GO
CREATE TABLE [dbo].[tbUnitRoom](
	[UnitRoomID] [int] IDENTITY(1,1) NOT NULL,
	[UnitID] [int] NOT NULL,
	[RoomID] [tinyint] NOT NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbUnitRoom_IsDeleted]  DEFAULT ((0)),
	[LastModifiedBy] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbUnitRoom_CreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbUnitRoom_UpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbUnitRoom] PRIMARY KEY CLUSTERED 
(
	[UnitRoomID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbUnitRoom]  WITH CHECK ADD  CONSTRAINT [FK_tbUnitRoom_tbRoom] FOREIGN KEY([RoomID])
REFERENCES [dbo].[tbRoom] ([RoomID])
GO
ALTER TABLE [dbo].[tbUnitRoom]  WITH CHECK ADD  CONSTRAINT [FK_tbUnitRoom_tbUnit] FOREIGN KEY([UnitID])
REFERENCES [dbo].[tbUnit] ([UnitID])