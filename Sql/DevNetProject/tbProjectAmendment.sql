USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbProjectAmendment_tbProject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbProjectAmendment]'))
ALTER TABLE [dbo].[tbProjectAmendment] DROP CONSTRAINT [FK_tbProjectAmendment_tbProject]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbProjectAmendment]') AND type in (N'U'))
DROP TABLE [dbo].[tbProjectAmendment]
GO
CREATE TABLE [dbo].[tbProjectAmendment](
	[ProjectAmendmentID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [int] NOT NULL,
	[Note] [varchar](200) NOT NULL,
	[MostRecent] [bit] NOT NULL CONSTRAINT [DF_tbProjectAmendment_MostRecent]  DEFAULT ((1)),
 	[LastModifiedBy] [int] NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [dfPACreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [dfPAUpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbProjectAmendment] PRIMARY KEY CLUSTERED 
(
	[ProjectAmendmentID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[tbProjectAmendment]  WITH CHECK ADD  CONSTRAINT [FK_tbProjectAmendment_tbProject] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[tbProject] ([ProjectID])