USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractUnitDepSched_tbContractUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractUnitDepSched]'))
ALTER TABLE [dbo].[tbContractUnitDepSched] DROP CONSTRAINT [FK_tbContractUnitDepSched_tbContractUnit]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbContractUnitDepSched]') AND type in (N'U'))
DROP TABLE [dbo].[tbContractUnitDepSched]
GO
CREATE TABLE [dbo].[tbContractUnitDepSched](
	[ContractUnitDepSchedID] [int] IDENTITY(1,1) NOT NULL,
	[ContractUnitID] [int] NOT NULL,
	[Percent] [float] NULL,
	[Amount] [money] NOT NULL,
	[DueDate] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbContractUnitDepSched_IsDeleted]  DEFAULT ((0)),
	[LastModifiedBy] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_ttbContractUnitDepSched_CreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbContractUnitDepSched_UpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbContractUnitDepSched] PRIMARY KEY CLUSTERED 
(
	[ContractUnitDepSchedID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbContractUnitDepSched]  WITH CHECK ADD  CONSTRAINT [FK_tbContractUnitDepSched_tbContractUnit] FOREIGN KEY([ContractUnitID])
REFERENCES [dbo].[tbContractUnit] ([ContractUnitID])