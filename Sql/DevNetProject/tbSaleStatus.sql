USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbUnit_tbSaleStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUnit]'))
ALTER TABLE [dbo].[tbUnit] DROP CONSTRAINT [FK_tbUnit_tbSaleStatus]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbSaleStatus]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbSaleStatus]
GO
CREATE TABLE [dbo].[tbSaleStatus](
	[SaleStatus] [char](2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_tbSaleStatus] PRIMARY KEY CLUSTERED 
(
	[SaleStatus] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbUnit_tbSaleStatus] FOREIGN KEY([SaleStatus])
REFERENCES [dbo].[tbSaleStatus] ([SaleStatus])