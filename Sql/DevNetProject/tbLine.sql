USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_tbUnit_tbLine]') AND type = 'F')
ALTER TABLE [dbo].[tbUnit] DROP CONSTRAINT [FK_tbUnit_tbLine]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_tbLine_tbProject]') AND type = 'F')
ALTER TABLE [dbo].[tbLine] DROP CONSTRAINT [FK_tbLine_tbProject]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbLine]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbLine]
GO
CREATE TABLE [dbo].[tbLine](
	[LineID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [int] NOT NULL,
	[Description] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbLine_IsDeleted]  DEFAULT (0),
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbLine_CreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbLine_UpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbLine] PRIMARY KEY CLUSTERED 
(
	[LineID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbLine]  WITH CHECK ADD  CONSTRAINT [FK_tbLine_tbProject] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[tbProject] ([ProjectID])
GO
ALTER TABLE [dbo].[tbUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbUnit_tbLine] FOREIGN KEY([LineID])
REFERENCES [dbo].[tbLine] ([LineID])