USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbAudit]') AND type in (N'U'))
DROP TABLE [dbo].[tbAudit]
GO
CREATE TABLE [dbo].[tbAudit](
	[AuditID] [int] IDENTITY(1,1) NOT NULL,
	[UserAccountID] [int] NOT NULL,
	[Table] [varchar](50) NOT NULL,
	[RecordID] [int] NOT NULL,
	[FieldValues] [varchar] (200) NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbAudit_CreateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbAudit] PRIMARY KEY CLUSTERED 
(
	[AuditID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF