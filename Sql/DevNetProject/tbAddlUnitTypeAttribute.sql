USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbAddlUnitTypeAttribute_tbAddlUnitType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbAddlUnitTypeAttribute]'))
ALTER TABLE [dbo].[tbAddlUnitTypeAttribute] DROP CONSTRAINT [FK_tbAddlUnitTypeAttribute_tbAddlUnitType]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbAddlUnitTypeAttribute]') AND type in (N'U'))
DROP TABLE [dbo].[tbAddlUnitTypeAttribute]
GO
CREATE TABLE [dbo].[tbAddlUnitTypeAttribute](
	[AddlUnitTypeAttributeID] [int] IDENTITY(1,1) NOT NULL,
	[AddlUnitTypeID] [int] NOT NULL,
	[Attribute] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbAddlUnitTypeAttribute_IsDeleted]  DEFAULT ((0)),
	[LastModifiedBy] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbAddlUnitTypeAttribute_CreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbAddlUnitTypeAttribute_CreateDateTime1]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbAddlUnitTypeAttribute] PRIMARY KEY CLUSTERED 
(
	[AddlUnitTypeAttributeID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbAddlUnitTypeAttribute]  WITH CHECK ADD  CONSTRAINT [FK_tbAddlUnitTypeAttribute_tbAddlUnitType] FOREIGN KEY([AddlUnitTypeID])
REFERENCES [dbo].[tbAddlUnitType] ([AddlUnitTypeID])