USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbUnitCharge_tbUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUnitCharge]'))
ALTER TABLE [dbo].[tbUnitCharge] DROP CONSTRAINT [FK_tbUnitCharge_tbUnit]
GO
ALTER TABLE [dbo].[tbUnitCharge] DROP CONSTRAINT [CK_tbUnitCharge_Type]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbUnitCharge]') AND type in (N'U'))
DROP TABLE [dbo].[tbUnitCharge]
GO
CREATE TABLE [dbo].[tbUnitCharge](
	[UnitChargeID] [int] IDENTITY(1,1) NOT NULL,
	[UnitID] [int] NOT NULL,
	[Type] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Amount] [money] NOT NULL,
	[MostRecent] [bit] NOT NULL CONSTRAINT [DF_tbUnitCharge_MostRecent]  DEFAULT ((1)),
	[CreatedBy] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbUnitCharge_CreateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbUnitCharge] PRIMARY KEY CLUSTERED 
(
	[UnitChargeID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbUnitCharge]  WITH CHECK ADD  CONSTRAINT [FK_tbUnitCharge_tbUnit] FOREIGN KEY([UnitID])
REFERENCES [dbo].[tbUnit] ([UnitID])
GO
ALTER TABLE [dbo].[tbUnitCharge]  WITH CHECK ADD  CONSTRAINT [CK_tbUnitCharge_Type] CHECK  (([Type]='C' OR [Type]='T'))