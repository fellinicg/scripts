USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbUnitExposure_tbExposure]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUnitExposure]'))
ALTER TABLE [dbo].[tbUnitExposure] DROP CONSTRAINT [FK_tbUnitExposure_tbExposure]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbExposure]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbExposure]
GO
CREATE TABLE [dbo].[tbExposure](
	[ExposureID] [tinyint] IDENTITY(1,1) NOT NULL,
	[ShortDescription] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbExposure_IsDeleted]  DEFAULT (0),
 CONSTRAINT [PK_tbExposure] PRIMARY KEY CLUSTERED 
(
	[ExposureID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbUnitExposure]  WITH CHECK ADD  CONSTRAINT [FK_tbUnitExposure_tbExposure] FOREIGN KEY([ExposureID])
REFERENCES [dbo].[tbExposure] ([ExposureID])
