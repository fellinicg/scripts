USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbUnit_tbOutdoorType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUnit]'))
ALTER TABLE [dbo].[tbUnit] DROP CONSTRAINT [FK_tbUnit_tbOutdoorType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbUnit_tbSaleStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUnit]'))
ALTER TABLE [dbo].[tbUnit] DROP CONSTRAINT [FK_tbUnit_tbSaleStatus]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbUnit_tbLine]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUnit]'))
ALTER TABLE [dbo].[tbUnit] DROP CONSTRAINT [FK_tbUnit_tbLine]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbUnit_tbModel]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUnit]'))
ALTER TABLE [dbo].[tbUnit] DROP CONSTRAINT [FK_tbUnit_tbModel]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbUnit_tbProject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUnit]'))
ALTER TABLE [dbo].[tbUnit] DROP CONSTRAINT [FK_tbUnit_tbProject]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbAskingPrice_tbUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbAskingPrice]'))
ALTER TABLE [dbo].[tbAskingPrice] DROP CONSTRAINT [FK_tbAskingPrice_tbUnit]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractUnit_tbUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractUnit]'))
ALTER TABLE [dbo].[tbContractUnit] DROP CONSTRAINT [FK_tbContractUnit_tbUnit]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbPriceAmendment_tbUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbPriceAmendment]'))
ALTER TABLE [dbo].[tbPriceAmendment] DROP CONSTRAINT [FK_tbPriceAmendment_tbUnit]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbReleaseRound_tbUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbReleaseRound]'))
ALTER TABLE [dbo].[tbReleaseRound] DROP CONSTRAINT [FK_tbReleaseRound_tbUnit]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbUnitCharge_tbUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUnitCharge]'))
ALTER TABLE [dbo].[tbUnitCharge] DROP CONSTRAINT [FK_tbUnitCharge_tbUnit]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbUnitExposure_tbUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUnitExposure]'))
ALTER TABLE [dbo].[tbUnitExposure] DROP CONSTRAINT [FK_tbUnitExposure_tbUnit]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbUnitRoom_tbUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUnitRoom]'))
ALTER TABLE [dbo].[tbUnitRoom] DROP CONSTRAINT [FK_tbUnitRoom_tbUnit]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbUnit]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbUnit]
GO
CREATE TABLE [dbo].[tbUnit](
	[UnitID] [int] NOT NULL,
	[ProjectID] [int] NOT NULL,
	[SaleStatus] [char](2) NOT NULL,
	[Floor] [tinyint] NOT NULL,
	[ModelID] [int] NOT NULL,
	[LineID] [int] NOT NULL,
	[Unit] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[InsideSF] [int] NOT NULL,
	[OutsideSF] [int] NOT NULL,
	[OutdoorTypeID] [tinyint] NOT NULL,
	[ListingNumber] [int] NULL,
	[PurchasePrice] [money] NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_tbUnit_IsActive]  DEFAULT (0),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbUnit_IsDeleted]  DEFAULT (0),
	[LastModifiedBy] [int] NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbUnit_CreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbUnit_UpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbUnit] PRIMARY KEY CLUSTERED 
(
	[UnitID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbUnit_tbSaleStatus] FOREIGN KEY([SaleStatus])
REFERENCES [dbo].[tbSaleStatus] ([SaleStatus])
GO
ALTER TABLE [dbo].[tbUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbUnit_tbLine] FOREIGN KEY([LineID])
REFERENCES [dbo].[tbLine] ([LineID])
GO
ALTER TABLE [dbo].[tbUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbUnit_tbModel] FOREIGN KEY([ModelID])
REFERENCES [dbo].[tbModel] ([ModelID])
GO
ALTER TABLE [dbo].[tbUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbUnit_tbProject] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[tbProject] ([ProjectID])
GO
ALTER TABLE [dbo].[tbUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbUnit_tbOutdoorType] FOREIGN KEY([OutdoorTypeID])
REFERENCES [dbo].[tbOutdoorType] ([OutdoorTypeID])
GO
ALTER TABLE [dbo].[tbAskingPrice]  WITH CHECK ADD  CONSTRAINT [FK_tbAskingPrice_tbUnit] FOREIGN KEY([UnitID])
REFERENCES [dbo].[tbUnit] ([UnitID])
GO
ALTER TABLE [dbo].[tbContractUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbContractUnit_tbUnit] FOREIGN KEY([UnitID])
REFERENCES [dbo].[tbUnit] ([UnitID])
GO
ALTER TABLE [dbo].[tbReleaseRound]  WITH CHECK ADD  CONSTRAINT [FK_tbReleaseRound_tbUnit] FOREIGN KEY([UnitID])
REFERENCES [dbo].[tbUnit] ([UnitID])
GO
ALTER TABLE [dbo].[tbPriceAmendment]  WITH CHECK ADD  CONSTRAINT [FK_tbPriceAmendment_tbUnit] FOREIGN KEY([UnitID])
REFERENCES [dbo].[tbUnit] ([UnitID])
GO
ALTER TABLE [dbo].[tbUnitCharge]  WITH CHECK ADD  CONSTRAINT [FK_tbUnitCharge_tbUnit] FOREIGN KEY([UnitID])
REFERENCES [dbo].[tbUnit] ([UnitID])
USE [DevNetProject]
GO
ALTER TABLE [dbo].[tbUnitExposure]  WITH CHECK ADD  CONSTRAINT [FK_tbUnitExposure_tbUnit] FOREIGN KEY([UnitID])
REFERENCES [dbo].[tbUnit] ([UnitID])
GO
ALTER TABLE [dbo].[tbUnitRoom]  WITH CHECK ADD  CONSTRAINT [FK_tbUnitRoom_tbUnit] FOREIGN KEY([UnitID])
REFERENCES [dbo].[tbUnit] ([UnitID])