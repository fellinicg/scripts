USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbUnit_tbOutdoorType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUnit]'))
ALTER TABLE [dbo].[tbUnit] DROP CONSTRAINT [FK_tbUnit_tbOutdoorType]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbOutdoorType]') AND type in (N'U'))
DROP TABLE [dbo].[tbOutdoorType]
GO
CREATE TABLE [dbo].[tbOutdoorType](
	[OutdoorTypeID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Description] [varbinary](50) NOT NULL,
 CONSTRAINT [PK_tbOutdoorType] PRIMARY KEY CLUSTERED 
(
	[OutdoorTypeID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbUnit_tbOutdoorType] FOREIGN KEY([OutdoorTypeID])
REFERENCES [dbo].[tbOutdoorType] ([OutdoorTypeID])