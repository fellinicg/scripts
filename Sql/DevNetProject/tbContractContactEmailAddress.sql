USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fkContractContactEmailAddressAccount]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractContactPhoneNum]'))
ALTER TABLE [dbo].[tbContractContactEmailAddress] DROP CONSTRAINT [fkContractContactEmailAddressAccount]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tcContractContactEmailAddressType]') AND type in (N'C'))
ALTER TABLE [dbo].[tbContractContactEmailAddress] DROP CONSTRAINT [tcContractContactEmailAddressType]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbContractContactEmailAddress]') AND type in (N'U'))
DROP TABLE [dbo].[tbContractContactEmailAddress]
GO
CREATE TABLE [dbo].[tbContractContactEmailAddress](
	[ContractContactEmailAddressID] [int] IDENTITY(1,1) NOT NULL,
	[ContractContactID] [int] NOT NULL,
	[EmailAddressType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[EmailAddress] [varchar](120) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsValidFormat]  AS ([dbo].[ufEmailAddressIsValid]([EmailAddress])),
	[IsPreferred] [bit] NOT NULL,
	[LastModifiedBy] [int] NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [dfContractContactEmailAddressCreateDate]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [dfContractContactEmailAddressUpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [pkContractContactEmailAddress] PRIMARY KEY CLUSTERED 
(
	[ContractContactEmailAddressID] ASC
) ON [PRIMARY],
 CONSTRAINT [akContractContactEmailAddress] UNIQUE NONCLUSTERED 
(
	[ContractContactID] ASC,
	[EmailAddress] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbContractContactEmailAddress]  WITH CHECK ADD  CONSTRAINT [fkContractContactEmailAddressAccount] FOREIGN KEY([ContractContactID])
REFERENCES [dbo].[tbContractContact] ([ContractContactID])
GO
ALTER TABLE [dbo].[tbContractContactEmailAddress]  WITH CHECK ADD  CONSTRAINT [tcContractContactEmailAddressType] CHECK  (([EmailAddressType] = 'H' or ([EmailAddressType] = 'O' or ([EmailAddressType] = 'M' or [EmailAddressType] = 'W'))))
