USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbAddlUnitAttribute]') AND type in (N'U'))
DROP TABLE [dbo].[tbAddlUnitAttribute]
GO
CREATE TABLE [dbo].[tbAddlUnitAttribute](
	[AddlUnitAttributeID] [int] IDENTITY(1,1) NOT NULL,
	[AddlUnitID] [int] NOT NULL,
	[AddlUnitTypeAttributeID] [int] NOT NULL,
	[Value] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbAddlUnitAttribute_IsDeleted]  DEFAULT ((0)),
	[LastModifiedBy] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbAddlUnitAttribute_CreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbAddlUnitAttribute_CreateDateTime1]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbAddlUnitAttribute] PRIMARY KEY CLUSTERED 
(
	[AddlUnitAttributeID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
