USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbProjectTeam_tbTeamRole]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbProjectTeam]'))
ALTER TABLE [dbo].[tbProjectTeam] DROP CONSTRAINT [FK_tbProjectTeam_tbTeamRole]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbTeamRole]') AND type in (N'U'))
DROP TABLE [dbo].[tbTeamRole]
GO
CREATE TABLE [dbo].[tbTeamRole](
	[TeamRoleID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbTeamRole_IsDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_tbTeamRole] PRIMARY KEY CLUSTERED 
(
	[TeamRoleID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbProjectTeam]  WITH CHECK ADD  CONSTRAINT [FK_tbProjectTeam_tbTeamRole] FOREIGN KEY([TeamRoleID])
REFERENCES [dbo].[tbTeamRole] ([TeamRoleID])