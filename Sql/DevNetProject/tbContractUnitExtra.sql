USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractUnitExtra_tbContractUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractUnitExtra]'))
ALTER TABLE [dbo].[tbContractUnitExtra] DROP CONSTRAINT [FK_tbContractUnitExtra_tbContractUnit]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbContractUnitExtra]') AND type in (N'U'))
DROP TABLE [dbo].[tbContractUnitExtra]
GO
CREATE TABLE [dbo].[tbContractUnitExtra](
	[ContractUnitExtraID] [int] IDENTITY(1,1) NOT NULL,
	[ContractUnitID] [int] NOT NULL,
	[Amount] [money] NOT NULL,
	[Description] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbContractUnitExtra_IsDeleted]  DEFAULT ((0)),
	[LastModifiedBy] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbContractUnitExtra_CreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbContractUnitExtra_UpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbContractUnitExtra] PRIMARY KEY CLUSTERED 
(
	[ContractUnitExtraID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbContractUnitExtra]  WITH CHECK ADD  CONSTRAINT [FK_tbContractUnitExtra_tbContractUnit] FOREIGN KEY([ContractUnitID])
REFERENCES [dbo].[tbContractUnit] ([ContractUnitID])