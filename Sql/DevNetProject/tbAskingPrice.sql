USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbAskingPrice_tbUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbAskingPrice]'))
ALTER TABLE [dbo].[tbAskingPrice] DROP CONSTRAINT [FK_tbAskingPrice_tbUnit]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbAskingPrice]') AND type in (N'U'))
DROP TABLE [dbo].[tbAskingPrice]
GO
CREATE TABLE [dbo].[tbAskingPrice](
	[AskingPriceID] [int] IDENTITY(1,1) NOT NULL,
	[UnitID] [int] NOT NULL,
	[AskingPrice] [money] NOT NULL,
	[MostRecent] [bit] NOT NULL CONSTRAINT [DF_tbAskingPrice_MostRecent]  DEFAULT ((1)),
	[CreatedBy] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbAskingPrice_CreateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbAskingPrice] PRIMARY KEY CLUSTERED 
(
	[AskingPriceID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
USE [DevNetProject]
GO
ALTER TABLE [dbo].[tbAskingPrice]  WITH CHECK ADD  CONSTRAINT [FK_tbAskingPrice_tbUnit] FOREIGN KEY([UnitID])
REFERENCES [dbo].[tbUnit] ([UnitID])