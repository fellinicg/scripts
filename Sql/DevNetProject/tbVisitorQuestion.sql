USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbVisitorQuestion]') AND type in (N'U'))
DROP TABLE [dbo].[tbVisitorQuestion]
GO
CREATE TABLE [dbo].[tbVisitorQuestion](
	[VisitorQuestionID] [int] IDENTITY(1,1) NOT NULL,
	[Question] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AnswerType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LastModifiedBy] [int] NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [dfVisitorQuestionCreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [dfVisitorQuestionUpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [pkVisitorQuestionID] PRIMARY KEY CLUSTERED 
(
	[VisitorQuestionID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbVisitorQuestion]  WITH CHECK ADD  CONSTRAINT [ckVisitorQuestionAnswerType] 
CHECK  (([AnswerType]='C' OR [AnswerType]='T'))