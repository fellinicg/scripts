USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractAddlUnitDepSched_tbContractAddlUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractAddlUnitDepSched]'))
ALTER TABLE [dbo].[tbContractAddlUnitDepSched] DROP CONSTRAINT [FK_tbContractAddlUnitDepSched_tbContractAddlUnit]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbContractAddlUnitDepSched]') AND type in (N'U'))
DROP TABLE [dbo].[tbContractAddlUnitDepSched]
GO
CREATE TABLE [dbo].[tbContractAddlUnitDepSched](
	[ContractAddlUnitDepSchedID] [int] IDENTITY(1,1) NOT NULL,
	[ContractAddlUnitID] [int] NOT NULL,
	[Percent] [float] NULL,
	[Amount] [money] NOT NULL,
	[DueDate] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbContractAddlUnitDepSched_IsDeleted]  DEFAULT ((0)),
	[LastModifiedBy] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_ttbContractAddlUnitDepSched_CreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbContractAddlUnitDepSched_UpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbContractAddlUnitDepSched] PRIMARY KEY CLUSTERED 
(
	[ContractAddlUnitDepSchedID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbContractAddlUnitDepSched]  WITH CHECK ADD  CONSTRAINT [FK_tbContractAddlUnitDepSched_tbContractAddlUnit] FOREIGN KEY([ContractAddlUnitID])
REFERENCES [dbo].[tbContractAddlUnit] ([ContractAddlUnitID])