USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbVisitorProfile]') AND type in (N'U'))
DROP TABLE [dbo].[tbVisitorProfile]
GO
CREATE TABLE [dbo].[tbVisitorProfile](
	[VisitorProfileID] [int] IDENTITY(1,1) NOT NULL,
	[VisitType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[VisitorUserAccountID] [int] NOT NULL,
	[BrokerUserAccountID] [int] NULL,
	[Comments] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [dfVisitorProfileCreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [dfVisitorProfileUpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [pkVisitorProfileID] PRIMARY KEY CLUSTERED 
(
	[VisitorProfileID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbVisitorProfile]  WITH CHECK ADD  CONSTRAINT [ckVisitorProfileVisitType] 
CHECK  (([VisitType]='A' OR [VisitType]='O' OR [VisitType]='P' OR [VisitType]='W'))