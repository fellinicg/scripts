USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fkContactNumAccount]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContactNum]'))
ALTER TABLE [dbo].[tbContactNum] DROP CONSTRAINT [fkContactNumAccount]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fkEmailAddressAccount]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbEmailAddress]'))
ALTER TABLE [dbo].[tbEmailAddress] DROP CONSTRAINT [fkEmailAddressAccount]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fkMailAddressAccount]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbMailAddress]'))
ALTER TABLE [dbo].[tbMailAddress] DROP CONSTRAINT [fkMailAddressAccount]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fkAccountStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUserAccount]'))
ALTER TABLE [dbo].[tbUserAccount] DROP CONSTRAINT [fkAccountStatus]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fkAccountType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUserAccount]'))
ALTER TABLE [dbo].[tbUserAccount] DROP CONSTRAINT [fkAccountType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fkGovtIDType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUserAccount]'))
ALTER TABLE [dbo].[tbUserAccount] DROP CONSTRAINT [fkGovtIDType]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbUserAccount]') AND type in (N'U'))
DROP TABLE [dbo].[tbUserAccount]
GO
CREATE TABLE [dbo].[tbUserAccount](
	[UserAccountID] [int] IDENTITY(100000,1) NOT NULL,
	[AccountType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AccountName] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AccountKey] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[NamePrefix] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FirstGivenName] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SecondGivenName] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FamilyName] [varchar](60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[NameSuffix] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FullName] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[GovtIDType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[GovtIDCode] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AccountStatus] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsRegistered] [bit] NOT NULL CONSTRAINT [dfUserAccountIsRegistered]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [dfUserAccountIsDeleted]  DEFAULT ((0)),
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [dfUserAccountCreateDate]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [dfUserAccountUpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [pkUserAccount] PRIMARY KEY CLUSTERED 
(
	[UserAccountID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbUserAccount]  WITH CHECK ADD  CONSTRAINT [fkAccountStatus] FOREIGN KEY([AccountStatus])
REFERENCES [dbo].[tbAccountStatus] ([AccountStatus])
GO
ALTER TABLE [dbo].[tbUserAccount]  WITH CHECK ADD  CONSTRAINT [fkAccountType] FOREIGN KEY([AccountType])
REFERENCES [dbo].[tbAccountType] ([AccountType])
GO
ALTER TABLE [dbo].[tbUserAccount]  WITH CHECK ADD  CONSTRAINT [fkGovtIDType] FOREIGN KEY([GovtIDType])
REFERENCES [dbo].[tbGovtIDType] ([GovtIDType])
GO
ALTER TABLE [dbo].[tbEmailAddress]  WITH CHECK ADD  CONSTRAINT [fkEmailAddressAccount] FOREIGN KEY([UserAccountID])
REFERENCES [dbo].[tbUserAccount] ([UserAccountID])
GO
ALTER TABLE [dbo].[tbMailAddress]  WITH CHECK ADD  CONSTRAINT [fkMailAddressAccount] FOREIGN KEY([UserAccountID])
REFERENCES [dbo].[tbUserAccount] ([UserAccountID])
GO
ALTER TABLE [dbo].[tbContactNum]  WITH CHECK ADD  CONSTRAINT [fkContactNumAccount] FOREIGN KEY([UserAccountID])
REFERENCES [dbo].[tbUserAccount] ([UserAccountID])