USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbUnitRoom_tbRoom]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUnitRoom]'))
ALTER TABLE [dbo].[tbUnitRoom] DROP CONSTRAINT [FK_tbUnitRoom_tbRoom]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbRoom]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbRoom]
GO
CREATE TABLE [dbo].[tbRoom](
	[RoomID] [tinyint] IDENTITY(1,1) NOT NULL,
	[ShortDescription] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbRoom_IsDeleted]  DEFAULT (0),
 CONSTRAINT [PK_tbRoom] PRIMARY KEY CLUSTERED 
(
	[RoomID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbUnitRoom]  WITH CHECK ADD  CONSTRAINT [FK_tbUnitRoom_tbRoom] FOREIGN KEY([RoomID])
REFERENCES [dbo].[tbRoom] ([RoomID])