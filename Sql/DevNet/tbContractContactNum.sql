USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fkContractContactNumAccount]') 
	AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractContactNum]'))
ALTER TABLE [dbo].[tbContractContactNum] DROP CONSTRAINT [fkContractContactNumAccount]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ckContractContactNumCityAreaCode]') AND type in (N'C'))
ALTER TABLE [dbo].[tbContractContactNum] DROP CONSTRAINT [ckContractContactNumCityAreaCode]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ckContractContactNumExtension]') AND type in (N'C'))
ALTER TABLE [dbo].[tbContractContactNum] DROP CONSTRAINT [ckContractContactNumExtension]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ckContractContactNumContractContactNum]') AND type in (N'C'))
ALTER TABLE [dbo].[tbContractContactNum] DROP CONSTRAINT [ckContractContactNumContractContactNum]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tcContractContactNumType]') AND type in (N'C'))
ALTER TABLE [dbo].[tbContractContactNum] DROP CONSTRAINT [tcContractContactNumType]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbContractContactNum]') AND type in (N'U'))
DROP TABLE [dbo].[tbContractContactNum]
GO
CREATE TABLE [dbo].[tbContractContactNum](
	[ContractContactNumID] [int] IDENTITY(1,1) NOT NULL,
	[ContractContactID] [int] NOT NULL,
	[ContactNumType] [char](2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CityAreaCode] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ContactNum] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Extension] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsPreferred] [bit] NOT NULL,
	[LastModifiedBy] [int] NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [dfContractContactNumCreateDate]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [dfContractContactNumUpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [pkContractContactNum] PRIMARY KEY CLUSTERED 
(
	[ContractContactNumID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbContractContactNum]  WITH CHECK ADD  CONSTRAINT [fkContractContactNumAccount] FOREIGN KEY([ContractContactID])
REFERENCES [dbo].[tbContractContact] ([ContractContactID])
GO
ALTER TABLE [dbo].[tbContractContactNum]  WITH CHECK ADD  CONSTRAINT [ckContractContactNumContractContactNum] CHECK  ((NOT [ContactNum] like '[ContactNum][^a-z0-9][ContactNum]'))
GO
ALTER TABLE [dbo].[tbContractContactNum]  WITH CHECK ADD  CONSTRAINT [ckContractContactNumCityAreaCode] CHECK  ((NOT [CityAreaCode] like '[CityAreaCode][^a-z0-9][CityAreaCode]'))
GO
ALTER TABLE [dbo].[tbContractContactNum]  WITH CHECK ADD  CONSTRAINT [ckContractContactNumExtension] CHECK  ((NOT [Extension] like '[Extension][^a-z0-9][Extension]'))
GO
ALTER TABLE [dbo].[tbContractContactNum]  WITH CHECK ADD  CONSTRAINT [tcContractContactNumType] CHECK  (([ContactNumType]='HP' OR [ContactNumType]='OP' OR [ContactNumType]='MP' OR [ContactNumType]='HF' OR [ContactNumType]='OF' OR [ContactNumType]='WF'))