USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbPriceAmendment_tbUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbPriceAmendment]'))
ALTER TABLE [dbo].[tbPriceAmendment] DROP CONSTRAINT [FK_tbPriceAmendment_tbUnit]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbPriceAmendment]') AND type in (N'U'))
DROP TABLE [dbo].[tbPriceAmendment]
GO
CREATE TABLE [dbo].[tbPriceAmendment](
	[PriceAmendmentID] [int] IDENTITY(1,1) NOT NULL,
	[UnitID] [int] NOT NULL,
	[AmendedPrice] [money] NOT NULL,
	[MostRecent] [bit] NOT NULL CONSTRAINT [DF_tbPriceAmendment_MostRecent]  DEFAULT ((1)),
	[CreatedBy] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbPriceAmendment_CreateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbPriceAmendment] PRIMARY KEY CLUSTERED 
(
	[PriceAmendmentID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
USE [DevNetProject]
GO
ALTER TABLE [dbo].[tbPriceAmendment]  WITH CHECK ADD  CONSTRAINT [FK_tbPriceAmendment_tbUnit] FOREIGN KEY([UnitID])
REFERENCES [dbo].[tbUnit] ([UnitID])