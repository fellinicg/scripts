USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fkProjectTeamEmailAddressAccount]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbProjectTeamPhoneNum]'))
ALTER TABLE [dbo].[tbProjectTeamEmailAddress] DROP CONSTRAINT [fkProjectTeamEmailAddressAccount]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tcProjectTeamEmailAddressType]') AND type in (N'C'))
ALTER TABLE [dbo].[tbProjectTeamEmailAddress] DROP CONSTRAINT [tcProjectTeamEmailAddressType]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbProjectTeamEmailAddress]') AND type in (N'U'))
DROP TABLE [dbo].[tbProjectTeamEmailAddress]
GO
CREATE TABLE [dbo].[tbProjectTeamEmailAddress](
	[ProjectTeamEmailAddressID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectTeamID] [int] NOT NULL,
	[EmailAddressType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[EmailAddress] [varchar](120) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsValidFormat]  AS ([dbo].[ufEmailAddressIsValid]([EmailAddress])),
	[IsPreferred] [bit] NOT NULL,
	[LastModifiedBy] [int] NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [dfProjectTeamEmailAddressCreateDate]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [dfProjectTeamEmailAddressUpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [pkProjectTeamEmailAddress] PRIMARY KEY CLUSTERED 
(
	[ProjectTeamEmailAddressID] ASC
) ON [PRIMARY],
 CONSTRAINT [akProjectTeamEmailAddress] UNIQUE NONCLUSTERED 
(
	[ProjectTeamID] ASC,
	[EmailAddress] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbProjectTeamEmailAddress]  WITH CHECK ADD  CONSTRAINT [fkProjectTeamEmailAddressAccount] FOREIGN KEY([ProjectTeamID])
REFERENCES [dbo].[tbProjectTeam] ([ProjectTeamID])
GO
ALTER TABLE [dbo].[tbProjectTeamEmailAddress]  WITH CHECK ADD  CONSTRAINT [tcProjectTeamEmailAddressType] CHECK  (([EmailAddressType] = 'H' or ([EmailAddressType] = 'O' or ([EmailAddressType] = 'M' or [EmailAddressType] = 'W'))))
