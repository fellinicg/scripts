USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractAddlUnit_tbContract]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractAddlUnit]'))
ALTER TABLE [dbo].[tbContractAddlUnit] DROP CONSTRAINT [FK_tbContractAddlUnit_tbContract]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractContact_tbContract]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractContact]'))
ALTER TABLE [dbo].[tbContractContact] DROP CONSTRAINT [FK_tbContractContact_tbContract]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractNote_tbContract]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractNote]'))
ALTER TABLE [dbo].[tbContractNote] DROP CONSTRAINT [FK_tbContractNote_tbContract]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractUnit_tbContract]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractUnit]'))
ALTER TABLE [dbo].[tbContractUnit] DROP CONSTRAINT [FK_tbContractUnit_tbContract]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbContract]') AND type in (N'U'))
DROP TABLE [dbo].[tbContract]
GO
CREATE TABLE [dbo].[tbContract](
	[ContractID] [int] IDENTITY(1,1) NOT NULL,
	[DealType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CoBrokeCompany] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CoBrokeAgentName] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CoBrokeAgentPhone] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FinancedAmount] [money] NOT NULL,
	[Price] [money] NOT NULL,
	[Bank] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BankAddress1] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BankAddress2] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BankCityTown] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BankStateProvince] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BankPostalCode] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_tbContract_IsActive]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbContract_IsDeleted]  DEFAULT ((0)),
	[LastModifiedBy] [int] NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbContract_CreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbContract_UpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbContract] PRIMARY KEY CLUSTERED 
(
	[ContractID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbContractAddlUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbContractAddlUnit_tbContract] FOREIGN KEY([ContractID])
REFERENCES [dbo].[tbContract] ([ContractID])
GO
ALTER TABLE [dbo].[tbContractContact]  WITH CHECK ADD  CONSTRAINT [FK_tbContractContact_tbContract] FOREIGN KEY([ContractID])
REFERENCES [dbo].[tbContract] ([ContractID])
GO
ALTER TABLE [dbo].[tbContractNote]  WITH CHECK ADD  CONSTRAINT [FK_tbContractNote_tbContract] FOREIGN KEY([ContractID])
REFERENCES [dbo].[tbContract] ([ContractID])
GO
ALTER TABLE [dbo].[tbContractUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbContractUnit_tbContract] FOREIGN KEY([ContractID])
REFERENCES [dbo].[tbContract] ([ContractID])