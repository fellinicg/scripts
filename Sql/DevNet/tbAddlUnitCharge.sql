USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbAddlUnitCharge_tbAddlUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbAddlUnitCharge]'))
ALTER TABLE [dbo].[tbAddlUnitCharge] DROP CONSTRAINT [FK_tbAddlUnitCharge_tbAddlUnit]
GO
ALTER TABLE [dbo].[tbAddlUnitCharge] DROP CONSTRAINT [CK_tbAddlUnitCharge_Type]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbAddlUnitCharge]') AND type in (N'U'))
DROP TABLE [dbo].[tbAddlUnitCharge]
GO
CREATE TABLE [dbo].[tbAddlUnitCharge](
	[AddlUnitChargeID] [int] IDENTITY(1,1) NOT NULL,
	[AddlUnitID] [int] NOT NULL,
	[Type] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Amount] [money] NOT NULL,
	[MostRecent] [bit] NOT NULL CONSTRAINT [DF_tbAddlUnitCharge_MostRecent]  DEFAULT ((1)),
	[CreatedBy] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbAddlUnitCharge_CreateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbAddlUnitCharge] PRIMARY KEY CLUSTERED 
(
	[AddlUnitChargeID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbAddlUnitCharge]  WITH CHECK ADD  CONSTRAINT [FK_tbAddlUnitCharge_tbAddlUnit] FOREIGN KEY([AddlUnitID])
REFERENCES [dbo].[tbAddlUnit] ([AddlUnitID])
GO
ALTER TABLE [dbo].[tbAddlUnitCharge]  WITH CHECK ADD  CONSTRAINT [CK_tbAddlUnitCharge_Type] CHECK  (([Type]='C' OR [Type]='T'))