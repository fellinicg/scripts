USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbProjectProjection_tbProject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbProjectProjection]'))
ALTER TABLE [dbo].[tbProjectProjection] DROP CONSTRAINT [FK_tbProjectProjection_tbProject]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbProjectProjection]') AND type in (N'U'))
DROP TABLE [dbo].[tbProjectProjection]
GO
CREATE TABLE [dbo].[tbProjectProjection](
	[ProjectProjectionID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [int] NOT NULL,
	[NumFloors] [tinyint] NULL,
	[NumUnits] [smallint] NULL,
	[SquareFootage] [int] NULL,
	[Sellout] [money] NULL,
	[NumElevators] [tinyint] NULL,
	[YearBuilt] [smallint] NULL,
	[OPSubDate] [smalldatetime] NULL,
	[OPAppDate] [smalldatetime] NULL,
	[SalesOffOpenDate] [smalldatetime] NULL,
	[UnitCloseStartDate] [smalldatetime] NULL,
	[UnitCloseEndDate] [smalldatetime] NULL,
 	[LastModifiedBy] [int] NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [dfPPCreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [dfPPUpdateDateTime]  DEFAULT (getdate()),
CONSTRAINT [PK_tbProjectProjection] PRIMARY KEY CLUSTERED 
(
	[ProjectProjectionID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbProjectProjection]  WITH CHECK ADD  CONSTRAINT [FK_tbProjectProjection_tbProject] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[tbProject] ([ProjectID])