USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[fkMailAddressAccount]') AND type = 'F')
ALTER TABLE [dbo].[tbMailAddress] DROP CONSTRAINT [fkMailAddressAccount]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbMailAddress]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbMailAddress]
GO
CREATE TABLE [dbo].[tbMailAddress](
	[MailAddressID] [int] IDENTITY(1,1) NOT NULL,
	[UserAccountID] [int] NOT NULL,
	[MailAddressType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Line1] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Line2] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Line3] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Line4] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CityTown] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StateProvince] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PostalCode] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Country] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsPreferred] [bit] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [dfMailAddressCreateDate]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [dfMailAddressUpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [pkMailAddress] PRIMARY KEY CLUSTERED 
(
	[MailAddressID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbMailAddress]  WITH CHECK ADD  CONSTRAINT [fkMailAddressAccount] FOREIGN KEY([UserAccountID])
REFERENCES [dbo].[tbUserAccount] ([UserAccountID])
GO
ALTER TABLE [dbo].[tbMailAddress]  WITH CHECK ADD CONSTRAINT [tcMailAddressType] CHECK  ([MailAddressType] = 'H' or [MailAddressType] = 'O' or [MailAddressType] = 'W')
