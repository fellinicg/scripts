USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fkContractContactNumAccount]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractContactNum]'))
ALTER TABLE [dbo].[tbContractContactNum] DROP CONSTRAINT [fkContractContactNumAccount]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fkContractContactEmailAddressAccount]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractContactEmailAddress]'))
ALTER TABLE [dbo].[tbContractContactEmailAddress] DROP CONSTRAINT [fkContractContactEmailAddressAccount]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fkContractContactMailAddressAccount]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractContactMailAddress]'))
ALTER TABLE [dbo].[tbContractContactMailAddress] DROP CONSTRAINT [fkContractContactMailAddressAccount]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbContractContact]') AND type in (N'U'))
DROP TABLE [dbo].[tbContractContact]
GO
CREATE TABLE [dbo].[tbContractContact](
	[ContractContactID] [int] IDENTITY(1,1) NOT NULL,
	[ContractID] [int] NOT NULL,
	[UserAccountID] [int] NULL,
	[Type] [char](1) NOT NULL,
	[NamePrefix] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FirstGivenName] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SecondGivenName] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FamilyName] [varchar](60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[NameSuffix] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[GovtIDCode] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsPrimary] [bit] NOT NULL CONSTRAINT [DF_tbContractContact_IsPrimary]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbContractContact_IsDeleted]  DEFAULT ((0)),
	[LastModifiedBy] [int] NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [dfCCCreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [dfCCUpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbContractContact] PRIMARY KEY CLUSTERED 
(
	[ContractContactID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbContractContact]  WITH CHECK ADD  CONSTRAINT [FK_tbContractContact_tbContract] FOREIGN KEY([ContractID])
REFERENCES [dbo].[tbContract] ([ContractID])
GO
ALTER TABLE [dbo].[tbContractContact]  WITH CHECK ADD  CONSTRAINT [CK_tbContractContact] CHECK  (([Type]='A' OR [Type]='B'))
GO
ALTER TABLE [dbo].[tbContractContactEmailAddress]  WITH CHECK ADD  CONSTRAINT [fkContractContactEmailAddressAccount] FOREIGN KEY([ContractContactID])
REFERENCES [dbo].[tbContractContact] ([ContractContactID])
GO
ALTER TABLE [dbo].[tbContractContactNum]  WITH CHECK ADD  CONSTRAINT [fkContractContactNumAccount] FOREIGN KEY([ContractContactID])
REFERENCES [dbo].[tbContractContact] ([ContractContactID])
GO
ALTER TABLE [dbo].[tbContractContactMailAddress]  WITH CHECK ADD  CONSTRAINT [fkContractContactMailAddressAccount] FOREIGN KEY([ContractContactID])
REFERENCES [dbo].[tbContractContact] ([ContractContactID])
