USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fkAccountStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUserAccount]'))
ALTER TABLE [dbo].[tbUserAccount] DROP CONSTRAINT [fkAccountStatus]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbAccountStatus]') AND type in (N'U'))
DROP TABLE [dbo].[tbAccountStatus]
GO
CREATE TABLE [dbo].[tbAccountStatus](
	[AccountStatus] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [pkAccountStatus] PRIMARY KEY CLUSTERED 
(
	[AccountStatus] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbUserAccount]  WITH CHECK ADD  CONSTRAINT [fkAccountStatus] FOREIGN KEY([AccountStatus])
REFERENCES [dbo].[tbAccountStatus] ([AccountStatus])