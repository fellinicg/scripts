USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbReleaseRound_tbUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbReleaseRound]'))
ALTER TABLE [dbo].[tbReleaseRound] DROP CONSTRAINT [FK_tbReleaseRound_tbUnit]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbReleaseRound]') AND type in (N'U'))
DROP TABLE [dbo].[tbReleaseRound]
GO
CREATE TABLE [dbo].[tbReleaseRound](
	[ReleaseRoundID] [int] IDENTITY(1,1) NOT NULL,
	[UnitID] [int] NOT NULL,
	[ReleaseRound] [tinyint] NOT NULL,
	[MostRecent] [bit] NOT NULL CONSTRAINT [DF_tbReleaseRound_MostRecent]  DEFAULT ((1)),
	[CreatedBy] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbReleaseRound_CreateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbReleaseRound] PRIMARY KEY CLUSTERED 
(
	[ReleaseRoundID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[tbReleaseRound]  WITH CHECK ADD  CONSTRAINT [FK_tbReleaseRound_tbUnit] FOREIGN KEY([UnitID])
REFERENCES [dbo].[tbUnit] ([UnitID])