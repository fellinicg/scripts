USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbVisitorResponse]') AND type in (N'U'))
DROP TABLE [dbo].[tbVisitorResponse]
GO
CREATE TABLE [dbo].[tbVisitorResponse](
	[VisitorResponseID] [int] IDENTITY(1,1) NOT NULL,
	[VisitorQuestionID] [int] NOT NULL,
	[Response] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LastModifiedBy] [int] NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [dfVisitorResponseCreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [dfVisitorResponseUpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [pkVisitorResponseID] PRIMARY KEY CLUSTERED 
(
	[VisitorResponseID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF