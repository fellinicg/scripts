USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbAddlUnit_tbAddlUnitType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbAddlUnit]'))
ALTER TABLE [dbo].[tbAddlUnit] DROP CONSTRAINT [FK_tbAddlUnit_tbAddlUnitType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbAddlUnitTypeAttribute_tbAddlUnitType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbAddlUnitTypeAttribute]'))
ALTER TABLE [dbo].[tbAddlUnitTypeAttribute] DROP CONSTRAINT [FK_tbAddlUnitTypeAttribute_tbAddlUnitType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbAddlUnitType_tbProject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbAddlUnitType]'))
ALTER TABLE [dbo].[tbAddlUnitType] DROP CONSTRAINT [FK_tbAddlUnitType_tbProject]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbAddlUnitType]') AND type in (N'U'))
DROP TABLE [dbo].[tbAddlUnitType]
GO
CREATE TABLE [dbo].[tbAddlUnitType](
	[AddlUnitTypeID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [int] NOT NULL,
	[Code] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbAddlUnitType_IsDeleted]  DEFAULT ((0)),
	[LastModifiedBy] [int] NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbAddlUnitType_CreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbAddlUnitType_UpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbAddlUnitType] PRIMARY KEY CLUSTERED 
(
	[AddlUnitTypeID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbAddlUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbAddlUnit_tbAddlUnitType] FOREIGN KEY([AddlUnitTypeID])
REFERENCES [dbo].[tbAddlUnitType] ([AddlUnitTypeID])
GO
ALTER TABLE [dbo].[tbAddlUnitTypeAttribute]  WITH CHECK ADD  CONSTRAINT [FK_tbAddlUnitTypeAttribute_tbAddlUnitType] FOREIGN KEY([AddlUnitTypeID])
REFERENCES [dbo].[tbAddlUnitType] ([AddlUnitTypeID])
GO
ALTER TABLE [dbo].[tbAddlUnitType]  WITH CHECK ADD  CONSTRAINT [FK_tbAddlUnitType_tbProject] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[tbProject] ([ProjectID])