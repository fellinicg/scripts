USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[fkContractContactMailAddressAccount]') AND type = 'F')
ALTER TABLE [dbo].[tbContractContactMailAddress] DROP CONSTRAINT [fkContractContactMailAddressAccount]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbContractContactMailAddress]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbContractContactMailAddress]
GO
CREATE TABLE [dbo].[tbContractContactMailAddress](
	[ContractContactMailAddressID] [int] IDENTITY(1,1) NOT NULL,
	[ContractContactID] [int] NOT NULL,
	[MailAddressType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Line1] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Line2] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Line3] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Line4] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CityTown] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StateProvince] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PostalCode] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Country] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsPreferred] [bit] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [dfContractContactMailAddressCreateDate]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [dfContractContactMailAddressUpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [pkContractContactMailAddress] PRIMARY KEY CLUSTERED 
(
	[ContractContactMailAddressID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbContractContactMailAddress]  WITH CHECK ADD  CONSTRAINT [fkContractContactMailAddressAccount] FOREIGN KEY([ContractContactID])
REFERENCES [dbo].[tbContractContact] ([ContractContactID])
GO
ALTER TABLE [dbo].[tbContractContactMailAddress]  WITH CHECK ADD CONSTRAINT [tcContractContactMailAddressType] CHECK  ([MailAddressType] = 'H' or [MailAddressType] = 'O' or [MailAddressType] = 'W')
