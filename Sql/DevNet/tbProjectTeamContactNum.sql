USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fkProjectTeamContactNumAccount]') 
	AND parent_object_id = OBJECT_ID(N'[dbo].[tbProjectTeamContactNum]'))
ALTER TABLE [dbo].[tbProjectTeamContactNum] DROP CONSTRAINT [fkProjectTeamContactNumAccount]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ckProjectTeamContactNumCityAreaCode]') AND type in (N'C'))
ALTER TABLE [dbo].[tbProjectTeamContactNum] DROP CONSTRAINT [ckProjectTeamContactNumCityAreaCode]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ckProjectTeamContactNumExtension]') AND type in (N'C'))
ALTER TABLE [dbo].[tbProjectTeamContactNum] DROP CONSTRAINT [ckProjectTeamContactNumExtension]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ckProjectTeamContactNumProjectTeamContactNum]') AND type in (N'C'))
ALTER TABLE [dbo].[tbProjectTeamContactNum] DROP CONSTRAINT [ckProjectTeamContactNumProjectTeamContactNum]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tcProjectTeamContactNumType]') AND type in (N'C'))
ALTER TABLE [dbo].[tbProjectTeamContactNum] DROP CONSTRAINT [tcProjectTeamContactNumType]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbProjectTeamContactNum]') AND type in (N'U'))
DROP TABLE [dbo].[tbProjectTeamContactNum]
GO
CREATE TABLE [dbo].[tbProjectTeamContactNum](
	[ProjectTeamContactNumID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectTeamID] [int] NOT NULL,
	[ContactNumType] [char](2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CityAreaCode] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ContactNum] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Extension] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsPreferred] [bit] NOT NULL,
	[LastModifiedBy] [int] NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [dfProjectTeamContactNumCreateDate]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [dfProjectTeamContactNumUpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [pkProjectTeamContactNum] PRIMARY KEY CLUSTERED 
(
	[ProjectTeamContactNumID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbProjectTeamContactNum]  WITH CHECK ADD  CONSTRAINT [fkProjectTeamContactNumAccount] FOREIGN KEY([ProjectTeamID])
REFERENCES [dbo].[tbProjectTeam] ([ProjectTeamID])
GO
ALTER TABLE [dbo].[tbProjectTeamContactNum]  WITH CHECK ADD  CONSTRAINT [ckProjectTeamContactNumProjectTeamContactNum] CHECK  ((NOT [ContactNum] like '[ContactNum][^a-z0-9][ContactNum]'))
GO
ALTER TABLE [dbo].[tbProjectTeamContactNum]  WITH CHECK ADD  CONSTRAINT [ckProjectTeamContactNumCityAreaCode] CHECK  ((NOT [CityAreaCode] like '[CityAreaCode][^a-z0-9][CityAreaCode]'))
GO
ALTER TABLE [dbo].[tbProjectTeamContactNum]  WITH CHECK ADD  CONSTRAINT [ckProjectTeamContactNumExtension] CHECK  ((NOT [Extension] like '[Extension][^a-z0-9][Extension]'))
GO
ALTER TABLE [dbo].[tbProjectTeamContactNum]  WITH CHECK ADD  CONSTRAINT [tcProjectTeamContactNumType] CHECK  (([ContactNumType]='HP' OR [ContactNumType]='OP' OR [ContactNumType]='MP' OR [ContactNumType]='HF' OR [ContactNumType]='OF' OR [ContactNumType]='WF'))