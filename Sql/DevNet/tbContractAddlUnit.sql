
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractAddlUnit_tbAddlUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractAddlUnit]'))
ALTER TABLE [dbo].[tbContractAddlUnit] DROP CONSTRAINT [FK_tbContractAddlUnit_tbAddlUnit]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractAddlUnit_tbContract]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractAddlUnit]'))
ALTER TABLE [dbo].[tbContractAddlUnit] DROP CONSTRAINT [FK_tbContractAddlUnit_tbContract]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractAddlUnitDepSched_tbContractAddlUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractAddlUnitDepSched]'))
ALTER TABLE [dbo].[tbContractAddlUnitDepSched] DROP CONSTRAINT [FK_tbContractAddlUnitDepSched_tbContractAddlUnit]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractAddlUnitExtra_tbContractAddlUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractAddlUnitExtra]'))
ALTER TABLE [dbo].[tbContractAddlUnitExtra] DROP CONSTRAINT [FK_tbContractAddlUnitExtra_tbContractAddlUnit]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractAddlUnitNote_tbContractAddlUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractAddlUnitNote]'))
ALTER TABLE [dbo].[tbContractAddlUnitNote] DROP CONSTRAINT [FK_tbContractAddlUnitNote_tbContractAddlUnit]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbContractAddlUnit]') AND type in (N'U'))
DROP TABLE [dbo].[tbContractAddlUnit]
GO
CREATE TABLE [dbo].[tbContractAddlUnit](
	[ContractAddlUnitID] [int] IDENTITY(1,1) NOT NULL,
	[ContractID] [int] NOT NULL,
	[AddlUnitID] [int] NOT NULL,
	[Date1] [datetime] NULL,
	[Date2] [datetime] NULL,
	[Date3] [datetime] NULL,
	[Date4] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbContractAddlUnit_IsDeleted]  DEFAULT ((0)),
	[LastModifiedBy] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_ttbContractAddlUnit_CreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbContractAddlUnit_UpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbContractAddlUnit] PRIMARY KEY CLUSTERED 
(
	[ContractAddlUnitID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbContractAddlUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbContractAddlUnit_tbAddlUnit] FOREIGN KEY([AddlUnitID])
REFERENCES [dbo].[tbAddlUnit] ([AddlUnitID])
GO
ALTER TABLE [dbo].[tbContractAddlUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbContractAddlUnit_tbContract] FOREIGN KEY([ContractID])
REFERENCES [dbo].[tbContract] ([ContractID])
GO
ALTER TABLE [dbo].[tbContractAddlUnitDepSched]  WITH CHECK ADD  CONSTRAINT [FK_tbContractAddlUnitDepSched_tbContractAddlUnit] FOREIGN KEY([ContractAddlUnitID])
REFERENCES [dbo].[tbContractAddlUnit] ([ContractAddlUnitID])
GO
ALTER TABLE [dbo].[tbContractAddlUnitExtra]  WITH CHECK ADD  CONSTRAINT [FK_tbContractAddlUnitExtra_tbContractAddlUnit] FOREIGN KEY([ContractAddlUnitID])
REFERENCES [dbo].[tbContractAddlUnit] ([ContractAddlUnitID])
GO
ALTER TABLE [dbo].[tbContractAddlUnitNote]  WITH CHECK ADD  CONSTRAINT [FK_tbContractAddlUnitNote_tbContractAddlUnit] FOREIGN KEY([ContractAddlUnitID])
REFERENCES [dbo].[tbContractAddlUnit] ([ContractAddlUnitID])