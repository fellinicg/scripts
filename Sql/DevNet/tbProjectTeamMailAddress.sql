USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[fkProjectTeamMailAddressAccount]') AND type = 'F')
ALTER TABLE [dbo].[tbProjectTeamMailAddress] DROP CONSTRAINT [fkProjectTeamMailAddressAccount]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbProjectTeamMailAddress]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbProjectTeamMailAddress]
GO
CREATE TABLE [dbo].[tbProjectTeamMailAddress](
	[ProjectTeamMailAddressID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectTeamID] [int] NOT NULL,
	[MailAddressType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Line1] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Line2] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Line3] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Line4] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CityTown] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StateProvince] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PostalCode] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Country] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsPreferred] [bit] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [dfProjectTeamMailAddressCreateDate]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [dfProjectTeamMailAddressUpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [pkProjectTeamMailAddress] PRIMARY KEY CLUSTERED 
(
	[ProjectTeamMailAddressID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbProjectTeamMailAddress]  WITH CHECK ADD  CONSTRAINT [fkProjectTeamMailAddressAccount] FOREIGN KEY([ProjectTeamID])
REFERENCES [dbo].[tbProjectTeam] ([ProjectTeamID])
GO
ALTER TABLE [dbo].[tbProjectTeamMailAddress]  WITH CHECK ADD CONSTRAINT [tcProjectTeamMailAddressType] CHECK  ([MailAddressType] = 'H' or [MailAddressType] = 'O' or [MailAddressType] = 'W')
