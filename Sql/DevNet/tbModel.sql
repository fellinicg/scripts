USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_tbUnit_tbModel]') AND type = 'F')
ALTER TABLE [dbo].[tbUnit] DROP CONSTRAINT [FK_tbUnit_tbModel]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_tbModel_tbProject]') AND type = 'F')
ALTER TABLE [dbo].[tbModel] DROP CONSTRAINT [FK_tbModel_tbProject]
GO
USE [DevNetProject]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbModel]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbModel]
GO
CREATE TABLE [dbo].[tbModel](
	[ModelID] [int] NOT NULL,
	[ProjectID] [int] NOT NULL,
	[Description] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbModel_IsDeleted]  DEFAULT (0),
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbModel_CreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbModel_UpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbModel] PRIMARY KEY CLUSTERED 
(
	[ModelID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbModel]  WITH CHECK ADD  CONSTRAINT [FK_tbModel_tbProject] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[tbProject] ([ProjectID])
GO
ALTER TABLE [dbo].[tbUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbUnit_tbModel] FOREIGN KEY([ModelID])
REFERENCES [dbo].[tbModel] ([ModelID])
