USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbVisitorQA]') AND type in (N'U'))
DROP TABLE [dbo].[tbVisitorQA]
GO
CREATE TABLE [dbo].[tbVisitorQA](
	[VisitorQAID] [int] IDENTITY(1,1) NOT NULL,
	[VisitorQuestionID] [int] NOT NULL,
	[Answer] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LastModifiedBy] [int] NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [dfVisitorQACreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [dfVisitorQAUpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [pktbVisitorQAID] PRIMARY KEY CLUSTERED 
(
	[VisitorQAID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF