USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbUnit_tbProject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUnit]'))
ALTER TABLE [dbo].[tbUnit] DROP CONSTRAINT [FK_tbUnit_tbProject]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbModel_tbProject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbModel]'))
ALTER TABLE [dbo].[tbModel] DROP CONSTRAINT [FK_tbModel_tbProject]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbLine_tbProject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbLine]'))
ALTER TABLE [dbo].[tbLine] DROP CONSTRAINT [FK_tbLine_tbProject]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_tbProject_tbPropertyType_New]') AND type = 'F')
ALTER TABLE [dbo].[tbProject] DROP CONSTRAINT [FK_tbProject_tbPropertyType_New]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_tbProject_tbPropertyType_Org]') AND type = 'F')
ALTER TABLE [dbo].[tbProject] DROP CONSTRAINT [FK_tbProject_tbPropertyType_Org]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbProject_tbNeighborhood]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbProject]'))
ALTER TABLE [dbo].[tbProject] DROP CONSTRAINT [FK_tbProject_tbNeighborhood]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbAddlUnitType_tbProject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbAddlUnitType]'))
ALTER TABLE [dbo].[tbAddlUnitType] DROP CONSTRAINT [FK_tbAddlUnitType_tbProject]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbAddlUnit_tbProject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbAddlUnit]'))
ALTER TABLE [dbo].[tbAddlUnit] DROP CONSTRAINT [FK_tbAddlUnit_tbProject]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbProjectAmendment_tbProject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbProjectAmendment]'))
ALTER TABLE [dbo].[tbProjectAmendment] DROP CONSTRAINT [FK_tbProjectAmendment_tbProject]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbProjectProjection_tbProject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbProjectProjection]'))
ALTER TABLE [dbo].[tbProjectProjection] DROP CONSTRAINT [FK_tbProjectProjection_tbProject]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbProjectTeam_tbProject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbProjectTeam]'))
ALTER TABLE [dbo].[tbProjectTeam] DROP CONSTRAINT [FK_tbProjectTeam_tbProject]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbProject]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbProject]
GO
CREATE TABLE [dbo].[tbProject](
	[ProjectID] [int] IDENTITY(1,1) NOT NULL,
	[PropertyID] [int] NULL,
	[Address] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CityTown] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[StateProvince] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[PostalCode] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[NeighborhoodID] [smallint] NULL,
	[CrossStreets] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OrgPropertyType] [smallint] NOT NULL,
	[NewPropertyType] [smallint] NOT NULL,
	[ProjectStatusCode] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ManagementType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[NumFloors] [tinyint] NULL,
	[NumUnits] [smallint] NULL,
	[SquareFootage] [int] NULL,
	[Sellout] [money] NULL,
	[NumElevators] [tinyint] NULL,
	[YearBuilt] [smallint] NULL,
	[OPSubDate] [smalldatetime] NULL,
	[OPAppDate] [smalldatetime] NULL,
	[OPSalesOffOpenDate] [smalldatetime] NULL,
	[OPUnitClStartDateDate] [smalldatetime] NULL,
	[OPUnitClEndDateDate] [smalldatetime] NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_tbProject_IsActive]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbProject_IsDeleted]  DEFAULT ((0)),
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbProject_CreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbProject_UpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbProject] PRIMARY KEY CLUSTERED 
(
	[ProjectID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbProject]  WITH CHECK ADD  CONSTRAINT [FK_tbProject_tbPropertyType_Org] FOREIGN KEY([OrgPropertyType])
REFERENCES [dbo].[tbPropertyType] ([PropertyTypeID])
GO
ALTER TABLE [dbo].[tbProject]  WITH CHECK ADD  CONSTRAINT [FK_tbProject_tbPropertyType_New] FOREIGN KEY([NewPropertyType])
REFERENCES [dbo].[tbPropertyType] ([PropertyTypeID])
GO
ALTER TABLE [dbo].[tbProject]  WITH CHECK ADD  CONSTRAINT [FK_tbProject_tbNeighborhood] FOREIGN KEY([NeighborhoodID])
REFERENCES [dbo].[tbNeighborhood] ([NeighborhoodID])
GO
ALTER TABLE [dbo].[tbLine]  WITH CHECK ADD  CONSTRAINT [FK_tbLine_tbProject] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[tbProject] ([ProjectID])
GO
ALTER TABLE [dbo].[tbModel]  WITH CHECK ADD  CONSTRAINT [FK_tbModel_tbProject] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[tbProject] ([ProjectID])
GO
ALTER TABLE [dbo].[tbUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbUnit_tbProject] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[tbProject] ([ProjectID])
GO
ALTER TABLE [dbo].[tbAddlUnitType]  WITH CHECK ADD  CONSTRAINT [FK_tbAddlUnitType_tbProject] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[tbProject] ([ProjectID])
GO
ALTER TABLE [dbo].[tbAddlUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbAddlUnit_tbProject] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[tbProject] ([ProjectID])
GO
ALTER TABLE [dbo].[tbProjectAmendment]  WITH CHECK ADD  CONSTRAINT [FK_tbProjectAmendment_tbProject] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[tbProject] ([ProjectID])
GO
ALTER TABLE [dbo].[tbProjectProjection]  WITH CHECK ADD  CONSTRAINT [FK_tbProjectProjection_tbProject] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[tbProject] ([ProjectID])
GO
ALTER TABLE [dbo].[tbProjectTeam]  WITH CHECK ADD  CONSTRAINT [FK_tbProjectTeam_tbProject] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[tbProject] ([ProjectID])