USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractAddlUnitNote_tbContractAddlUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractAddlUnitNote]'))
ALTER TABLE [dbo].[tbContractAddlUnitNote] DROP CONSTRAINT [FK_tbContractAddlUnitNote_tbContractAddlUnit]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbContractAddlUnitNote]') AND type in (N'U'))
DROP TABLE [dbo].[tbContractAddlUnitNote]
GO
CREATE TABLE [dbo].[tbContractAddlUnitNote](
	[ContractAddlUnitNoteID] [int] IDENTITY(1,1) NOT NULL,
	[ContractAddlUnitID] [int] NOT NULL,
	[NoteType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Note] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbContractAddlUnitNote_IsDeleted]  DEFAULT ((0)),
	[UserAccountID] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbContractAddlUnitNote_CreateDateTime]  DEFAULT (getdate()),
CONSTRAINT [PK_tbContractAddlUnitNote] PRIMARY KEY CLUSTERED 
(
	[ContractAddlUnitNoteID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbContractAddlUnitNote]  WITH CHECK ADD  CONSTRAINT [CK_tbContractAddlUnitNoteType] CHECK  (([NoteType]='E' OR [NoteType]='I'))
GO
ALTER TABLE [dbo].[tbContractAddlUnitNote]  WITH CHECK ADD  CONSTRAINT [FK_tbContractAddlUnitNote_tbContractAddlUnit] FOREIGN KEY([ContractAddlUnitID])
REFERENCES [dbo].[tbContractAddlUnit] ([ContractAddlUnitID])