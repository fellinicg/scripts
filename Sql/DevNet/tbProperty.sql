USE [DevNetProperty]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tbProperty]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tbProperty]
GO
CREATE TABLE [dbo].[tbProperty](
	[PropertyID] [int] IDENTITY(1,1) NOT NULL,
	[Block] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Lot] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Section] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Address] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CityTown] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StateProvince] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PostalCode] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Floor] [smallint] NULL,
	[Unit] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Latitude] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Longitude] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OwnerName] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OwnerAddress1] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OwnerAddress2] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OwnerCityTown] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OwnerStateProvince] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OwnerPostalCode] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OwnerPhoneNumber] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbProperty_CreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbProperty_UpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbProperty] PRIMARY KEY CLUSTERED 
(
	[PropertyID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF