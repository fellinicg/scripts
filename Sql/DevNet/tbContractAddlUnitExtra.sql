USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractAddlUnitExtra_tbContractAddlUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractAddlUnitExtra]'))
ALTER TABLE [dbo].[tbContractAddlUnitExtra] DROP CONSTRAINT [FK_tbContractAddlUnitExtra_tbContractAddlUnit]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbContractAddlUnitExtra]') AND type in (N'U'))
DROP TABLE [dbo].[tbContractAddlUnitExtra]
GO
CREATE TABLE [dbo].[tbContractAddlUnitExtra](
	[ContractAddlUnitExtraID] [int] IDENTITY(1,1) NOT NULL,
	[ContractAddlUnitID] [int] NOT NULL,
	[Amount] [money] NOT NULL,
	[Description] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbContractAddlUnitExtra_IsDeleted]  DEFAULT ((0)),
	[LastModifiedBy] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbContractAddlUnitExtra_CreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbContractAddlUnitExtra_UpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbContractAddlUnitExtra] PRIMARY KEY CLUSTERED 
(
	[ContractAddlUnitExtraID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbContractAddlUnitExtra]  WITH CHECK ADD  CONSTRAINT [FK_tbContractAddlUnitExtra_tbContractAddlUnit] FOREIGN KEY([ContractAddlUnitID])
REFERENCES [dbo].[tbContractAddlUnit] ([ContractAddlUnitID])