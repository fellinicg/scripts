USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbUnitExposure_tbExposure]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUnitExposure]'))
ALTER TABLE [dbo].[tbUnitExposure] DROP CONSTRAINT [FK_tbUnitExposure_tbExposure]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbUnitExposure_tbUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbUnitExposure]'))
ALTER TABLE [dbo].[tbUnitExposure] DROP CONSTRAINT [FK_tbUnitExposure_tbUnit]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbUnitExposure]') AND type in (N'U'))
DROP TABLE [dbo].[tbUnitExposure]
GO
CREATE TABLE [dbo].[tbUnitExposure](
	[UnitID] [int] NOT NULL,
	[ExposureID] [tinyint] NOT NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbUnitExposure_IsDeleted]  DEFAULT ((0)),
	[LastModifiedBy] [int] NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbUnitExposure_CreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbUnitExposure_UpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbUnitExposure] PRIMARY KEY CLUSTERED 
(
	[UnitID] ASC,
	[ExposureID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbUnitExposure]  WITH CHECK ADD  CONSTRAINT [FK_tbUnitExposure_tbExposure] FOREIGN KEY([ExposureID])
REFERENCES [dbo].[tbExposure] ([ExposureID])
GO
ALTER TABLE [dbo].[tbUnitExposure]  WITH CHECK ADD  CONSTRAINT [FK_tbUnitExposure_tbUnit] FOREIGN KEY([UnitID])
REFERENCES [dbo].[tbUnit] ([UnitID])