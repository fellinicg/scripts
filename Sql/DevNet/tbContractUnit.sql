USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractUnit_tbContract]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractUnit]'))
ALTER TABLE [dbo].[tbContractUnit] DROP CONSTRAINT [FK_tbContractUnit_tbContract]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractUnit_tbUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractUnit]'))
ALTER TABLE [dbo].[tbContractUnit] DROP CONSTRAINT [FK_tbContractUnit_tbUnit]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractUnitDepSched_tbContractUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractUnitDepSched]'))
ALTER TABLE [dbo].[tbContractUnitDepSched] DROP CONSTRAINT [FK_tbContractUnitDepSched_tbContractUnit]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractUnitExtra_tbContractUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractUnitExtra]'))
ALTER TABLE [dbo].[tbContractUnitExtra] DROP CONSTRAINT [FK_tbContractUnitExtra_tbContractUnit]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractUnitNote_tbContractUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractUnitNote]'))
ALTER TABLE [dbo].[tbContractUnitNote] DROP CONSTRAINT [FK_tbContractUnitNote_tbContractUnit]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbContractUnit]') AND type in (N'U'))
DROP TABLE [dbo].[tbContractUnit]
GO
CREATE TABLE [dbo].[tbContractUnit](
	[ContractUnitID] [int] IDENTITY(1,1) NOT NULL,
	[ContractID] [int] NOT NULL,
	[UnitID] [int] NOT NULL,
	[Date1] [datetime] NULL,
	[Date2] [datetime] NULL,
	[Date3] [datetime] NULL,
	[Date4] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbContractUnit_IsDeleted]  DEFAULT ((0)),
	[LastModifiedBy] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_ttbContractUnit_CreateDateTime]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbContractUnit_UpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbContractUnit] PRIMARY KEY CLUSTERED 
(
	[ContractUnitID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbContractUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbContractUnit_tbContract] FOREIGN KEY([ContractID])
REFERENCES [dbo].[tbContract] ([ContractID])
GO
ALTER TABLE [dbo].[tbContractUnit]  WITH CHECK ADD  CONSTRAINT [FK_tbContractUnit_tbUnit] FOREIGN KEY([UnitID])
REFERENCES [dbo].[tbUnit] ([UnitID])
GO
ALTER TABLE [dbo].[tbContractUnitDepSched]  WITH CHECK ADD  CONSTRAINT [FK_tbContractUnitDepSched_tbContractUnit] FOREIGN KEY([ContractUnitID])
REFERENCES [dbo].[tbContractUnit] ([ContractUnitID])
GO
ALTER TABLE [dbo].[tbContractUnitExtra]  WITH CHECK ADD  CONSTRAINT [FK_tbContractUnitExtra_tbContractUnit] FOREIGN KEY([ContractUnitID])
REFERENCES [dbo].[tbContractUnit] ([ContractUnitID])
GO
ALTER TABLE [dbo].[tbContractUnitNote]  WITH CHECK ADD  CONSTRAINT [FK_tbContractUnitNote_tbContractUnit] FOREIGN KEY([ContractUnitID])
REFERENCES [dbo].[tbContractUnit] ([ContractUnitID])