USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbProjectStatus]') AND type in (N'U'))
DROP TABLE [dbo].[tbProjectStatus]
GO
CREATE TABLE [dbo].[tbProjectStatus](
	[ProjectStatusCode] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_tbProjectStatus] PRIMARY KEY CLUSTERED 
(
	[ProjectStatusCode] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF