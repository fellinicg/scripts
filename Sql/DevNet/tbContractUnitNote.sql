USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractUnitNote_tbContractUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractUnitNote]'))
ALTER TABLE [dbo].[tbContractUnitNote] DROP CONSTRAINT [FK_tbContractUnitNote_tbContractUnit]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbContractUnitNote]') AND type in (N'U'))
DROP TABLE [dbo].[tbContractUnitNote]
GO
CREATE TABLE [dbo].[tbContractUnitNote](
	[ContractUnitNoteID] [int] IDENTITY(1,1) NOT NULL,
	[ContractUnitID] [int] NOT NULL,
	[NoteType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Note] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbContractUnitNote_IsDeleted]  DEFAULT ((0)),
	[UserAccountID] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbContractUnitNote_CreateDateTime]  DEFAULT (getdate()),
CONSTRAINT [PK_tbContractUnitNote] PRIMARY KEY CLUSTERED 
(
	[ContractUnitNoteID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbContractUnitNote]  WITH CHECK ADD  CONSTRAINT [CK_tbContractUnitNoteType] CHECK  (([NoteType]='E' OR [NoteType]='I'))
GO
ALTER TABLE [dbo].[tbContractUnitNote]  WITH CHECK ADD  CONSTRAINT [FK_tbContractUnitNote_tbContractUnit] FOREIGN KEY([ContractUnitID])
REFERENCES [dbo].[tbContractUnit] ([ContractUnitID])