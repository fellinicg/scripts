USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbProject_tbNeighborhood]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbProject]'))
ALTER TABLE [dbo].[tbProject] DROP CONSTRAINT [FK_tbProject_tbNeighborhood]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbNeighborhood]') AND type in (N'U'))
DROP TABLE [dbo].[tbNeighborhood]
GO
CREATE TABLE [dbo].[tbNeighborhood](
	[NeighborhoodID] [smallint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbNeighborhood_CreateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbNeighborhood] PRIMARY KEY CLUSTERED 
(
	[NeighborhoodID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbProject]  WITH CHECK ADD  CONSTRAINT [FK_tbProject_tbNeighborhood] FOREIGN KEY([NeighborhoodID])
REFERENCES [dbo].[tbNeighborhood] ([NeighborhoodID])