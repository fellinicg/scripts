USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fkContactNumAccount]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContactNum]'))
ALTER TABLE [dbo].[tbContactNum] DROP CONSTRAINT [fkContactNumAccount]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbContactNum]') AND type in (N'U'))
DROP TABLE [dbo].[tbContactNum]
GO
CREATE TABLE [dbo].[tbContactNum](
	[ContactNumID] [int] IDENTITY(1,1) NOT NULL,
	[UserAccountID] [int] NOT NULL,
	[ContactNumType] [char](2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CityAreaCode] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ContactNum] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Extension] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TxtMsgCapable] [bit] NOT NULL CONSTRAINT [dfContactNumTextMsgCapable]  DEFAULT (0),
	[IsPreferred] [bit] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [dfContactNumCreateDate]  DEFAULT (getdate()),
	[UpdateDateTime] [datetime] NOT NULL CONSTRAINT [dfContactNumUpdateDateTime]  DEFAULT (getdate()),
 CONSTRAINT [pkContactNumID] PRIMARY KEY CLUSTERED 
(
	[ContactNumID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbContactNum]  WITH CHECK ADD  CONSTRAINT [fkContactNumAccount] FOREIGN KEY([UserAccountID])
REFERENCES [dbo].[tbUserAccount] ([UserAccountID])
GO
ALTER TABLE [dbo].[tbContactNum]  WITH CHECK ADD  CONSTRAINT [ckContactNumCityAreaCode] CHECK  ((not([CityAreaCode] like '[CityAreaCode][^a-z0-9][CityAreaCode]')))
GO
ALTER TABLE [dbo].[tbContactNum]  WITH CHECK ADD  CONSTRAINT [ckContactNumExtension] CHECK  ((not([Extension] like '[Extension][^a-z0-9][Extension]')))
GO
ALTER TABLE [dbo].[tbContactNum]  WITH CHECK ADD  CONSTRAINT [ckContactNumPhoneNum] CHECK  ((not([ContactNum] like '[ContactNum][^a-z0-9][ContactNum]')))
GO
ALTER TABLE [dbo].[tbContactNum]  WITH CHECK ADD  CONSTRAINT [tcContactNumType] CHECK  (([ContactNumType]='HP' OR [ContactNumType]='OP' OR [ContactNumType]='MP' OR [ContactNumType]='WP' OR [ContactNumType]='HF' OR [ContactNumType]='OF' OR [ContactNumType]='WF'))
