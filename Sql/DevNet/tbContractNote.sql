USE [DevNetProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbContractNote_tbContract]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbContractNote]'))
ALTER TABLE [dbo].[tbContractNote] DROP CONSTRAINT [FK_tbContractNote_tbContract]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbContractNote]') AND type in (N'U'))
DROP TABLE [dbo].[tbContractNote]
GO
CREATE TABLE [dbo].[tbContractNote](
	[ContractNoteID] [int] IDENTITY(1,1) NOT NULL,
	[ContractID] [int] NOT NULL,
	[NoteType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Note] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tbContractNote_IsDeleted]  DEFAULT ((0)),
	[UserAccountID] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL CONSTRAINT [DF_tbContractNote_CreateDateTime]  DEFAULT (getdate()),
CONSTRAINT [PK_tbContractNote] PRIMARY KEY CLUSTERED 
(
	[ContractNoteID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbContractNote]  WITH CHECK ADD  CONSTRAINT [CK_tbContractNoteType] CHECK  (([NoteType]='E' OR [NoteType]='I'))
GO
ALTER TABLE [dbo].[tbContractNote]  WITH CHECK ADD  CONSTRAINT [FK_tbContractNote_tbContract] FOREIGN KEY([ContractID])
REFERENCES [dbo].[tbContract] ([ContractID])