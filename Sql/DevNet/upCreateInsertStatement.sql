USE [DevNetUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20090323
-- LastEditedByFullName: 
-- LastEditDate: 
-- ============================================================
CREATE PROCEDURE dbo.upCreateInsertStatement
	@tbl varchar(100)
AS
BEGIN

	-- Local variable declarations
	declare @tmp int
	declare @output varchar(1000)
	declare @q varchar(15)

	-- Initialize settings
	SET NOCOUNT ON

	-- Initialize variables
	set @q = ''''''''
	set @output = 'SELECT ''INSERT INTO dbo.' + @tbl + ' VALUES ('

	select c.colid, c.name
	into #temp
	from dbo.sysobjects o
	inner join dbo.syscolumns c on o.id = c.id
	where o.name = @tbl

	while exists (select 1 from #temp)
		begin
			select top 1 @tmp = colid from #temp order by colid asc
			select @output = @output + @q + ' + ' + name + ' + ' + @q + ',' from #temp where colid = @tmp
			delete from #temp where colid = @tmp		
		end

	select left(@output, len(@output)-1) + ')'' FROM dbo.' + @tbl

	drop table #temp

END
GO
