select *
from
	(select PropertyID, a.AttributeName, v.ValueName-- stuff((select ',' + convert(varchar(max), v.ValueName)
	from dbo.tbPropertyAttribute pa
		inner join dbo.tbAttributeValue av on pa.AttributeValueID = av.AttributeValueID and av.Isdeleted = 0
		inner join dbo.tbAttribute a on av.AttributeID = a.AttributeID and a.IsDeleted = 0
		inner join dbo.tbValue v on av.ValueID = v.ValueID and v.Isdeleted = 0
		--inner join dbo.tbAttribute a on pan.AttributeID = a.AttributeID
	--where PropertyID = 2
	) as pan
	pivot
	(max(ValueName) for AttributeName in (ApartmentType,Area,BuildingAllow,BuildingAmenity,BuildingEra,BuildingNotAllow,BuildingPersonnel,BuildingType,Exposure,IsNewConstruction,Neighborhood,OutdoorSpace,OwnershipType,[View])) as pvt
	order by PropertyID