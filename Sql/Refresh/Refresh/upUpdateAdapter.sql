use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateAdapter') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateAdapter
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150714
-- ============================================================
create procedure dbo.upUpdateAdapter
	@IpAddressID int
	,@Adapter varchar (50)
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @OldValue varchar(500)
	declare @Proc varchar(100)

	-- Proc
	set @Proc = object_name(@@procid)

	-- Old Value
	select @OldValue = Adapter from dbo.tbIpAddress where IpAddressID = @IpAddressID

	-- Update data
	update dbo.tbIpAddress
	set Adapter = @Adapter
	where IpAddressID = @IpAddressID

	-- Audit
	exec dbo.upAddAudit @Proc, 'tbIpAddress', @IpAddressID, 'Adapter', @OldValue, @Adapter
	
end
go
grant exec on dbo.upUpdateAdapter to manager