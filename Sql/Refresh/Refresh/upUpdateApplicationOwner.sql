use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateApplicationOwner') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateApplicationOwner
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150602
-- ============================================================
create procedure dbo.upUpdateApplicationOwner
	@ServerID int
	,@ApplicationOwner varchar (50)
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @OldValue varchar(50)
	declare @Proc varchar(100)

	-- Proc
	set @Proc = object_name(@@procid)

	-- Old Value
	select @OldValue = ApplicationOwner from dbo.tbServer where ServerID = @ServerID

	-- Update data
	update dbo.tbServer
	set ApplicationOwner = @ApplicationOwner
	where ServerID = @ServerID

	-- Audit
	exec dbo.upAddAudit @Proc, 'tbServer', @ServerID, 'ApplicationOwner', @OldValue, @ApplicationOwner

end
go
grant exec on dbo.upUpdateApplicationOwner to manager