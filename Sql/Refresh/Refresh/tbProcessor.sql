use Refresh
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbProcessor') and type in (N'U'))
drop table dbo.tbProcessor
go

create table dbo.tbProcessor(
	ProcessorID int identity(1,1) not null,
	QualityAssuranceID int not null,
	Availability tinyint not null,
	Caption varchar(64) not null,
	CpuStatus tinyint not null,
	CurrentClockSpeed int not null,
	CurrentVoltage smallint not null,
	DeviceID varchar(10) not null,
	Family int not null,
	L3CacheSize int not null,
	L3CacheSpeed int not null,
	Level int not null,
	Manufacturer varchar(30) not null,
	MaxClockSpeed int not null,
	Name varchar(100) not null,
	NumberOfCores smallint not null,
	NumberOfLogicalProcessors smallint not null,
	PowerManagementSupported bit not null,
	ProcessorType tinyint not null,
	Revision int not null,
	SocketDesignation varchar(20) not null,
	Status varchar(10) not null,
	StatusInfo tinyint not null,
 constraint PK_tbProcessor primary key clustered 
(
	ProcessorID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off