use Refresh
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbComputer') and type in (N'U'))
drop table dbo.tbComputer
go

create table dbo.tbComputer(
	ComputerID int identity(1,1) not null,
	QualityAssuranceID int not null,
	CurrentTimeZone smallint not null,
	EnableDaylightSavingsTime bit not null,
	DaylightInEffect bit not null,
	Manufacturer varchar(200) null,
	Model varchar(200) null,
	NumberOfLogicalProcessors tinyint not null,
	NumberOfProcessors tinyint not null,
	TotalPhysicalMemory bigint not null,
 constraint PK_tbComputer primary key clustered 
(
	ComputerID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off




