use Refresh
select 'Assigned - Built'
select 
	s.NewName Server
	, h.SerialNumber
	, h.Model
	, h.Description
	, s.Location
	, s.Application
	, dbo.sfStatus(s.Status) Status
	, isnull(dbo.sfServerNotes(s.ServerID, '1/1/1900', '12/31/2020'), '') Note
	,  h.WarrantyExpiration
from dbo.tbHardware h 
	left join dbo.tbServer s on SerialNumber = NewSerialNumber
where Type = 'Server'
	and s.NewSerialNumber is not null
	and s.Status not in ('U')
order by h.Model, s.Status, s.NewName

select
	s.Name SAN
	, h.SerialNumber
	, h.Model
	, h.Description
	, s.Location
	, s.Application
	, dbo.sfStatus(s.Status) Status
	,  h.WarrantyExpiration
from dbo.tbHardware h 
	left join dbo.tbSan s on h.SerialNumber = s.SerialNumber
where Type = 'SAN'

select 'Assigned - Not Built'
select 
	s.NewName Server
	, h.SerialNumber
	, h.Model
	, h.Description
	, s.Location
	, s.Application
	, dbo.sfStatus(s.Status) Status
	, isnull(dbo.sfServerNotes(s.ServerID, '1/1/1900', '12/31/2020'), '') Note
	,  h.WarrantyExpiration
from dbo.tbHardware h 
	left join dbo.tbServer s on SerialNumber = NewSerialNumber
where Type = 'Server'
	and s.NewSerialNumber is not null
	and s.Status = 'U'
order by h.Model

select 'Not Assigned'
select 
	h.SerialNumber
	, h.Model
	, h.Description
	, h.WarrantyExpiration
from dbo.tbHardware h 
	left join dbo.tbServer s on SerialNumber = NewSerialNumber
where Type = 'Server'
	and s.NewSerialNumber is null
order by h.Model
