use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.tfStatus') and type in (N'TF'))
drop function dbo.tfStatus
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150827
-- ============================================================
create function dbo.tfStatus ()
returns @ResultTable table (StatusID char(1), Status varchar(15), Sort tinyint) as

begin

	insert @ResultTable(StatusID, Status, Sort) values('U', 'Unbuilt', 1)
	insert @ResultTable(StatusID, Status, Sort) values('B', 'Built', 2)
	insert @ResultTable(StatusID, Status, Sort) values('Q', 'QA' + char(39) + 'd', 3) 
	insert @ResultTable(StatusID, Status, Sort) values('R', 'Racked', 4)
	insert @ResultTable(StatusID, Status, Sort) values('D', 'Delivered', 5)
	insert @ResultTable(StatusID, Status, Sort) values('E', 'Excluded', 6)

	return
end

go
