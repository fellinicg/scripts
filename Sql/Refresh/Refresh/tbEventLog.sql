use Refresh
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbEventLog') and type in (N'U'))
drop table dbo.tbEventLog
go

create table dbo.tbEventLog(
	EventLogID int identity(1,1) not null,
	QualityAssuranceID int not null,
	ApplicationLogSize bigint not null,
	ApplicationLogRetention int not null,
	SecurityLogSize bigint not null,
	SecurityLogRetention int not null,
	SetupLogSize bigint not null,
	SetupLogRetention int not null,
	SystemLogSize bigint not null,
	SystemLogRetention int not null,
	ForwardedLogSize bigint not null,
	ForwardedLogRetention int not null,
 constraint PK_tbEventLog primary key clustered 
(
	EventLogID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off




