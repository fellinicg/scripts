use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwWmiOSRecoveryQA'))
drop view dbo.vwWmiOSRecoveryQA
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150915
-- ============================================================
create view dbo.vwWmiOSRecoveryQA
as
	select 
		coalesce(ServerID, '') 'ServerID'
		, max(coalesce(NewName, '')) 'ServerName'
		, max(coalesce(case when Field = 'autoreboot' then Value end, '')) 'autoreboot'
		, max(coalesce(case when Field = 'debugfilepath' then Value end, '')) 'debugfilepath'
		, max(coalesce(case when Field = 'debuginfotype' then Value end, '')) 'debuginfotype'
		, max(coalesce(case when Field = 'kerneldumponly' then Value end, '')) 'kerneldumponly'
		, max(coalesce(case when Field = 'minidumpdirectory' then Value end, '')) 'minidumpdirectory'
		, max(coalesce(case when Field = 'OverwriteExistingDebugFile' then Value end, '')) 'OverwriteExistingDebugFile'
		, max(coalesce(case when Field = 'SendAdminAlert' then Value end, '')) 'SendAdminAlert'
		, max(coalesce(case when Field = 'WriteDebugInfo' then Value end, '')) 'WriteDebugInfo'
		, max(coalesce(case when Field = 'WriteToSystemLog' then Value end, '')) 'WriteToSystemLog'
	from (
		select
			a.ServerID
			, a.NewName
			, b.Field
			, b.Value
		from dbo.vwLatestQualityAssurance a
		inner join dbo.tbFieldValue b on a.QualityAssuranceID = b.QualityAssuranceID
		where 
			b.Category = 'WMI'
			and b.Subcategory in ('OSRecovery')
	) a
	group by ServerID
go


