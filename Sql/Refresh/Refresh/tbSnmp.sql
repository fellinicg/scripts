use Refresh
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbSnmp') and type in (N'U'))
drop table dbo.tbSnmp
go

create table dbo.tbSnmp(
	SnmpID int identity(1,1) not null,
	QualityAssuranceID int not null,
	Type varchar(30) not null,
	Value varchar(100) not null,
 constraint PK_tbSnmp primary key clustered 
(
	SnmpID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off




