use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwPhysicalIMMQA'))
drop view dbo.vwPhysicalIMMQA
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20151204
-- ============================================================
create view dbo.vwPhysicalIMMQA
as
	select 
		coalesce(ServerID, '') 'ServerID'
		, max(coalesce(NewName, '')) 'ServerName'
		, max(coalesce(case when Field = 'IMM.HostName1' then Value end, '')) 'IMM.HostName1'
		, max(coalesce(case when Field = 'IMM.HostIPAddress1' then Value end, '')) 'IMM.HostIPAddress1'
		, max(coalesce(case when Field = 'IMM.MACAddress1' then Value end, '')) 'IMM.MACAddress1'
		, max(coalesce(case when Field = 'IMM.BurnedInMacAddress' then Value end, '')) 'IMM.BurnedInMacAddress'
	from (
		select
			a.ServerID
			, a.NewName
			, b.Field
			, b.Value
		from dbo.vwLatestQualityAssurance a
		inner join dbo.tbFieldValue b on a.QualityAssuranceID = b.QualityAssuranceID
		where 
			b.Category = 'Physical'
			and b.Subcategory in ('IMM')
	) a
	group by ServerID
go


