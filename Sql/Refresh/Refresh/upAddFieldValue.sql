use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddFieldValue') and type in (N'P', N'PC'))
drop procedure dbo.upAddFieldValue
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150610
-- ============================================================
create procedure dbo.upAddFieldValue
	@QualityAssuranceID int
	, @Category varchar(20)
	, @Subcategory varchar(40)
	, @Section varchar(50) = null
	, @Field varchar(200)
	, @Value varchar(500)
	, @Subvalue varchar(500) = null
	, @Sort smallint = 0
	, @Success bit output
as
begin

	-- Initialize settings
	set nocount on

	-- Add FieldValue
	insert into dbo.tbFieldValue (QualityAssuranceID, Category, Subcategory, Section, Field, Value, Subvalue, Sort)
	values (@QualityAssuranceID, @Category, @Subcategory, @Section, @Field, @Value, @Subvalue, @Sort)
	
	-- Check for success
	if (@@error = 0) and (@@rowcount > 0) set @Success = 1
	else set @Success = 0

end
go
grant exec on dbo.upAddFieldValue to qa