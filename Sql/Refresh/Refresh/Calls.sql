-- STEP #1
--
DECLARE	@QualityAssuranceID int

EXEC	dbo.upAddQualityAssurance
		@ServerName = N'1MADAV41',
		@QualityAssuranceID = @QualityAssuranceID OUTPUT

SELECT	@QualityAssuranceID as N'@QualityAssuranceID'

-----------------------------------------------------------
-----------------------------------------------------------

-- Computer
--
DECLARE	@Success bit

EXEC	dbo.upAddComputer
		@QualityAssuranceID = 1,
		@CurrentTimeZone = -240,
		@EnableDaylightSavingsTime = 1,
		@DaylightInEffect = 1,
		@Manufacturer = N'VMware, Inc.',
		@Model = N'VMware Virtual Platform',
		@NumberOfLogicalProcessors = 2,
		@NumberOfProcessors = 2,
		@TotalPhysicalMemory = 4294496256,
		@Success = @Success OUTPUT

SELECT	@Success as N'@Success'

-- EventLog
--
DECLARE	@Success bit

EXEC	dbo.upAddEventLog
		@QualityAssuranceID = 1,
		@ApplicationLogSize = 20971520,
		@ApplicationLogRetention = 0,
		@SecurityLogSize = 20971520,
		@SecurityLogRetention = 0,
		@SetupLogSize = 20971520,
		@SetupLogRetention = 0,
		@SystemLogSize = 20971520,
		@SystemLogRetention = 0,
		@ForwardedLogSize = 20971520,
		@ForwardedLogRetention = 0,
		@Success = @Success OUTPUT

SELECT	@Success as N'@Success'

-- NetworkAdapter
--
DECLARE	@Success bit

EXEC	dbo.upAddNetworkAdapter
		@QualityAssuranceID = 1,
		@ActiveMaximumTransmissionUnit = 1500,
		@AdminStatus = 'Up',
		@ComponentID = 'pci\ven_15ad&dev_07b0',
		@DeviceWakeUpEnable = 1,
		@DriverDate = '8/28/2012',
		@DriverFileName = 'vmxnet3n61x64.sys',
		@DriverVersion = '1.3.11.0',
		@FullDuplex = 1,
		@ifAlias = 'LCL - Primary PAWANET',
		@ifDesc = 'vmxnet3 Ethernet Adapter',
		@ifIndex = 12,
		@ifName = 'Ethernet_10',
		@InstanceID = '{83CAE6FF-6328-4267-B493-86C2B7C202CE}',
		@MacAddress = '00-50-56-9C-73-9B',
		@MediaConnectionState = 'Connected',
		@NdisVersion = '6.2',
		@PhysicalMediaType = '802.3',
		@Speed = 10000000000,
		@Status = 'Up',
		@Success = @Success OUTPUT

SELECT	@Success as N'@Success'

-- Processor
--
DECLARE	@Success bit

EXEC	dbo.upAddProcessor
		@QualityAssuranceID = 1,
		@Availability = 3,
		@Caption =' Intel64 Family 6 Model 45 Stepping 7',
		@CpuStatus = 1,
		@CurrentClockSpeed = 2200,
		@CurrentVoltage = 33,
		@DeviceID =' CPU1',
		@Family = 2,
		@L3CacheSize = 0,
		@L3CacheSpeed = 0,
		@Level = 6,
		@Manufacturer =' GenuineIntel',
		@MaxClockSpeed = 2200,
		@Name =' Intel(R) Xeon(R) CPU E5-2660 0 @ 2.20GHz',
		@NumberOfCores = 1,
		@NumberOfLogicalProcessors = 1,
		@PowerManagementSupported = 0,
		@ProcessorType = 3,
		@Revision = 11527,
		@SocketDesignation =' CPU socket #1',
		@Status =' OK',
		@StatusInfo = 3,
		@Success = @Success OUTPUT

SELECT	@Success as N'@Success'

-- WinUser
--
DECLARE	@Success bit

EXEC	dbo.upAddWinUser
		@QualityAssuranceID = 1,
		@Caption =' PATCAV258\Guest',
		@Description =' Built-in account for guest access to the computer/domain',
		@Disabled = 1,
		@Lockout = 0,
		@Name =' Guest',
		@PasswordChangeable = 0,
		@PasswordExpires = 0,
		@PasswordRequired = 0,
		@SID =' S-1-5-21-3175090649-3826490762-1071927352-501',
		@Status =' Degraded',
 		@Success = @Success OUTPUT

SELECT	@Success as N'@Success'

-- FieldValue
--
DECLARE	@Success bit

EXEC	dbo.upAddFieldValue
		@QualityAssuranceID = 1,
		@Category = 'SNMP',
		@Subcategory = 'ValidCommunities',
		@Field = 'Community',
		@Value = 'papublic',
		@Subvalue = 'readonly',
 		@Success = @Success OUTPUT

SELECT	@Success as N'@Success'
