use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwWmiPageFileQA'))
drop view dbo.vwWmiPageFileQA
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150915
-- ============================================================
create view dbo.vwWmiPageFileQA
as
	select 
		coalesce(ServerID, '') 'ServerID'
		, max(coalesce(NewName, '')) 'ServerName'
		, max(coalesce(case when Field = 'name' then Value end, '')) 'name'
		, max(coalesce(case when Field = 'allocatedbasesize' then Value end, '')) 'allocatedbasesize'
		, max(coalesce(case when Field = 'caption' then Value end, '')) 'caption'
	from (
		select
			a.ServerID
			, a.NewName
			, b.Field
			, b.Value
		from dbo.vwLatestQualityAssurance a
		inner join dbo.tbFieldValue b on a.QualityAssuranceID = b.QualityAssuranceID
		where 
			b.Category = 'WMI'
			and b.Subcategory in ('PageFile')
	) a
	group by ServerID
go


