use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddQualityAssurance') and type in (N'P', N'PC'))
drop procedure dbo.upAddQualityAssurance
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150609
-- ============================================================
create procedure dbo.upAddQualityAssurance
	@ServerName varchar(20)
	, @QualityAssuranceID int output
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @ServerID int

	-- Retrieve ServerID
	select distinct @ServerID = ServerID
	from dbo.tbServer
	where OriginalName = @ServerName 
		or NewName = @ServerName

	-- Check that we only retrieved one ServerID
	if @@rowcount > 1 return null

	-- Add QualityAssurance
	insert into dbo.tbQualityAssurance (ServerID)
	values (@ServerID)
	
	set @QualityAssuranceID = @@identity

end
go
grant exec on dbo.upAddQualityAssurance to qa