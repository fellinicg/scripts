use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwWmiProcessorsQA'))
drop view dbo.vwWmiProcessorsQA
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20151103
-- ============================================================
create view dbo.vwWmiProcessorsQA
as
	select 
		coalesce(ServerID, '') 'ServerID'
		, max(coalesce(NewName, '')) 'ServerName'
		, max(coalesce(case when Field = 'deviceid' then Value end, '')) 'deviceid'
		, max(coalesce(case when Field = 'manufacturer' then Value end, '')) 'manufacturer'
		, max(coalesce(case when Field = 'name' then Value end, '')) 'name'
		, max(coalesce(case when Field = 'socketdesignation' then Value end, '')) 'socketdesignation'
		, max(coalesce(case when Field = 'caption' then Value end, '')) 'caption'
		, max(coalesce(case when Field = 'status' then Value end, '')) 'status'
		, max(coalesce(case when Field = 'availability' then Value end, '')) 'availability'
		, max(coalesce(case when Field = 'cpustatus' then Value end, '')) 'cpustatus'
		, max(coalesce(case when Field = 'MacAddress' then Value end, '')) 'currentclockspeed'
		, max(coalesce(case when Field = 'currentvoltage' then Value end, '')) 'currentvoltage'
		, max(coalesce(case when Field = 'family' then Value end, '')) 'family'
		, max(coalesce(case when Field = 'l3cachesize' then Value end, '')) 'l3cachesize'
		, max(coalesce(case when Field = 'l3cachespeed' then Value end, '')) 'l3cachespeed'
		, max(coalesce(case when Field = 'level' then Value end, '')) 'level'
		, max(coalesce(case when Field = 'maxclockspeed' then Value end, '')) 'maxclockspeed'
		, max(coalesce(case when Field = 'numberofcores' then Value end, '')) 'numberofcores'
		, max(coalesce(case when Field = 'numberoflogicalprocessors' then Value end, '')) 'numberoflogicalprocessors'
		, max(coalesce(case when Field = 'powermanagementsupported' then Value end, '')) 'powermanagementsupported'
		, max(coalesce(case when Field = 'processortype' then Value end, '')) 'processortype'
		, max(coalesce(case when Field = 'revision' then Value end, '')) 'revision'
		, max(coalesce(case when Field = 'statusinfo' then Value end, '')) 'statusinfo'
	from (
		select
			a.ServerID
			, a.NewName
			, b.Section
			, b.Field
			, b.Value
		from dbo.vwLatestQualityAssurance a
		inner join dbo.tbFieldValue b on a.QualityAssuranceID = b.QualityAssuranceID
		where 
			b.Category = 'WMI'
			and b.Subcategory in ('Processors')
	) a
	group by ServerID, Section
go


