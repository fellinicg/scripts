use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAuthenticate') and type in (N'P', N'PC'))
drop procedure dbo.upAuthenticate
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150603
-- ============================================================
create procedure dbo.upAuthenticate
	@Rights char(1) output 
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	if (is_member('manager') = 1 or is_member('dbo') = 1) set @Rights = 'M'
	else if (is_member('reviewer') = 1) set @Rights = 'R'
	else set @Rights =  'X'

	return

end
go
grant exec on dbo.upAuthenticate to manager, reviewer