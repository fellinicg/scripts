-- QA STUFF
select distinct Category, Subcategory
from dbo.tbFieldValue
where QualityAssuranceID = 460
order by Category, Subcategory

select Name
from sys.views
where right(name, 2) = 'QA'
order by Name

		select
			a.ServerID
			, a.NewName
			, b.Section
			, b.Field
			, b.Value
			, b.Subvalue
			, char(9) + char(9) + ', max(coalesce(case when Field = ' + char(39) + b.Field + char(39) + ' then Value end, ' + char(39) + char(39) + ')) ' + char(39) + b.Field + char(39)
		from dbo.vwLatestQualityAssurance a
		inner join dbo.tbFieldValue b on a.QualityAssuranceID = b.QualityAssuranceID
		where 
			b.Category = 'Reg'
			and b.Subcategory in ('SNMP')

-- ******************************************************************************************************************************************

select a.*, b.ApplicationOwner 'Current'
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Feeds\win2003-dmf.xlsx', 
	'select * from [Servers$]') a
left join dbo.tbServer b on a.ServerID = b.ServerID
where a.ApplicationOwner <> isnull(b.ApplicationOwner, '')
	--and b.ApplicationOwner is null
order by a.ApplicationOwner, a.Name

--union
--select a.*
--from openrowset('Microsoft.ACE.OLEDB.12.0',
--	'Excel 12.0;Database=C:\Feeds\win2003-christina.xlsx', 
--	'select * from [Servers$]') a
--left join dbo.tbServer b on a.ServerID = b.ServerID
--where a.ApplicationOwner <> isnull(b.ApplicationOwner, '')


declare @ServerID int
declare @ApplicationOwner varchar(50)

drop table #temp

select a.ServerID, a.ApplicationOwner
into #temp
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Feeds\win2003-dmf.xlsx', 
	'select * from [Servers$]') a
left join dbo.tbServer b on a.ServerID = b.ServerID
where a.ApplicationOwner <> isnull(b.ApplicationOwner, '')
	and b.ApplicationOwner is null
order by a.ServerID

while (select count(1) from #temp) > 0
begin
 select @ServerID = ServerID, @ApplicationOwner = ltrim(rtrim(ApplicationOwner)) from #temp
 exec dbo.upUpdateApplicationOwner @ServerID, @ApplicationOwner
 delete from #temp where ServerID = @ServerID
end


select 
	Name 'Server'
	, isnull(replace(replace(convert(varchar, UDF_MajorApplications), char(10), ''), char(13), ''), '') 'MajorApplications'
	, isnull(UDF_Owning_Department, '') 'Department'
	, isnull(ApplicationOwner, '') 'ApplicationOwner'
	, isnull(UDF_SiteName, '') 'SiteName'
	, isnull(UDF_Subnet, '') 'Subnet'
	, '' 'Verified'
from AN6.dbo.Computers
left join dbo.tbServer on Name = OriginalName
where Type_ID = '629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3'
	and charindex('Decomm', Description) = 0
order by Name

declare @tbl table (ServerID int, NewName varchar(500))
declare @sid int
declare @nn varchar(500)

-- Update NewName
insert into @tbl (ServerID, NewName)
select s.ServerID, a.NewName
from dbo.tbServer s
left join openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Feeds\Win2003Refresh.xlsx', 
	'select * from [Servers$]') a on s.ServerID = a.ServerID
where s.NewName is null and a.NewName is not null and a.NewName <> 'XXXXXXXX'

while exists(select 1 from @tbl)
	begin
		select top 1 @sid = ServerID, @nn = NewName from @tbl
		exec dbo.upUpdateNewName @sid, @nn
		delete from @tbl where ServerID = @sid
	end

declare @tbl table (ServerID int, Location varchar(500))
declare @sid int
declare @l varchar(500)

-- Update Location
insert into @tbl (ServerID, Location)
select s.ServerID, a.Location
from dbo.tbServer s
left join openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Feeds\Win2003Refresh.xlsx', 
	'select * from [Servers$]') a on s.ServerID = a.ServerID and s.Location <> a.Location
where a.Location is not null

while exists(select 1 from @tbl)
	begin
		select top 1 @sid = ServerID, @l = Location from @tbl
--		select @sid, @l
		exec dbo.upUpdateLocation @sid, @l
		delete from @tbl where ServerID = @sid
	end