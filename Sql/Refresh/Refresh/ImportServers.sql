-- Virtual Servers
--
insert into dbo.tbServer (OriginalName, OriginalIP, Application, OriginalRack)
select [Computer Name], [Primary IP Address], MajorApplications, [Server Rack]
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Feeds\Windows2003.xlsx', 
	'select * from [DataFromAlloyNavigator$]') a
left join dbo.tbServer b on a.[Computer Name] = b.OriginalName
where charindex('vmware', Manufacturer) > 0
	and b.OriginalName is null

-- Physical Servers
--
insert into dbo.tbServer (OriginalName, OriginalIP, Application, OriginalRack, IsVirtual)
select distinct [Computer Name], [Primary IP Address], MajorApplications, [Server Rack], 0
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Feeds\Windows2003.xlsx', 
	'select * from [DataFromAlloyNavigator$]')a
left join dbo.tbServer b on a.[Computer Name] = b.OriginalName
where charindex('vmware', Manufacturer) = 0
	and b.OriginalName is null



insert into dbo.tbIpAddress (ServerID, OriginalIP)
select b.ServerID, a.OriginalIP
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Feeds\CopyWin2003.xlsx', 
	'select * from [IPs$]') a
left join dbo.tbServer b on a.OriginalName = b.OriginalName
left join dbo.tbIpAddress c on b.ServerID = c.ServerID and a.OriginalIP = c.OriginalIP
where a.OriginalIP is not null
	and c.IpAddressID is null