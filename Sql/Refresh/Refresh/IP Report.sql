use Refresh

select distinct
	ServerName
	,InterfaceName
	,DeviceName
	,IPAddress
	,MacAddress
from dbo.vwServerNetworkInterface
where len(IPAddress) > 0 
	and charindex('169.', IPAddress) = 0
union
select distinct 
	ServerName
	,[IMM.HostName1]
	,'RSA' DeviceName
	,[IMM.HostIPAddress1]
	,[IMM.BurnedInMacAddress]
from dbo.vwPhysicalIMMQA
where len([IMM.HostIPAddress1]) > 0 
	and charindex('169.', [IMM.HostIPAddress1]) = 0
	and charindex('192.', [IMM.HostIPAddress1]) = 0
order by ServerName