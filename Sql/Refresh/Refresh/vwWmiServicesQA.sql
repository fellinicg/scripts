use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwWmiServicesQA'))
drop view dbo.vwWmiServicesQA
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20151103
-- ============================================================
create view dbo.vwWmiServicesQA
as
	select
		a.ServerID
		, a.NewName 'ServerName'
		, b.Field 'Service'
		, b.Value 'State'
	from dbo.vwLatestQualityAssurance a
	inner join dbo.tbFieldValue b on a.QualityAssuranceID = b.QualityAssuranceID
	where 
		b.Category = 'WMI'
		and b.Subcategory in ('Services')
go


