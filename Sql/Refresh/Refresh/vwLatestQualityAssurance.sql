use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwLatestQualityAssurance'))
drop view dbo.vwLatestQualityAssurance
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150911
-- ============================================================
create view dbo.vwLatestQualityAssurance
as
	select 
		a.QualityAssuranceID
		, a.ServerID
		, c.OriginalName
		, c.NewName
		, a.CreateDT
	from dbo.tbQualityAssurance a
	inner join (
		select ServerID, max(QualityAssuranceID) 'QualityAssuranceID'
		from dbo.tbQualityAssurance
		group by ServerID) b on a.QualityAssuranceID = b.QualityAssuranceID
	left join dbo.tbServer c on a.ServerID = c.ServerID
go


