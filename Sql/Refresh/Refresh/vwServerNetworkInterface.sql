use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwServerNetworkInterface'))
drop view dbo.vwServerNetworkInterface
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150911
-- ============================================================
create view dbo.vwServerNetworkInterface
as
	select 
		coalesce(ServerID, '') 'ServerID'
		, max(coalesce(NewName, '')) 'ServerName'
		, coalesce(Section, '') 'Section'
		, max(coalesce(case when Field = 'ifAlias' then Value end, '')) 'InterfaceName'
		, max(coalesce(case when Field = 'ifDesc' then Value end, '')) 'DeviceName'
		, max(coalesce(case when Field = 'ipv4address' then Value end, '')) 'IPAddress'
		, max(coalesce(case when Field = 'MacAddress' then Value end, '')) 'MacAddress'
	from (
		select
			a.ServerID
			, a.NewName
			, b.Section
			, b.Field
			, b.Value
		from dbo.vwLatestQualityAssurance a
		inner join dbo.tbFieldValue b on a.QualityAssuranceID = b.QualityAssuranceID
		where b.Subcategory in ('NetAdapters')
			and b.Field in ('ifAlias', 'ifDesc', 'ipv4address', 'MacAddress')
	) a
	group by ServerID, Section
go