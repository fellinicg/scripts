use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateOriginalRack') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateOriginalRack
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150602
-- ============================================================
create procedure dbo.upUpdateOriginalRack
	@ServerID int
	,@OriginalRack varchar (20)
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @OldValue varchar(20)
	declare @Proc varchar(100)

	-- Proc
	set @Proc = object_name(@@procid)

	-- Old Value
	select @OldValue = OriginalRack from dbo.tbServer where ServerID = @ServerID

	-- Update data
	update dbo.tbServer
	set OriginalRack = @OriginalRack
	where ServerID = @ServerID

	-- Audit
	exec dbo.upAddAudit @Proc, 'tbServer', @ServerID, 'OriginalRack', @OldValue, @OriginalRack

end
go
grant exec on dbo.upUpdateOriginalRack to manager