use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetServerQA') and type in (N'P', N'PC'))
drop procedure dbo.upGetServerQA
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150916
-- ============================================================
create procedure dbo.upGetServerQA (@ServerID int)
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		a.NewName 'ServerName'
		, a.CreateDT
		, b.Category
		, isnull(b.Subcategory, '') 'Subcategory'
		, isnull(b.Section, '') 'Section'
		, isnull(b.Field, '') 'Field'
		, isnull(b.Value, '') 'Value'
		, isnull(b.Subvalue, '') 'Subvalue'
	from
		dbo.vwLatestQualityAssurance a 
		inner join dbo.tbFieldValue b on a.QualityAssuranceID = b.QualityAssuranceID
	where 
		ServerID = @ServerID
	order by
		b.Category, b.Subcategory, b.Section, b.Field, b.Value, b.Subvalue

end
go
grant exec on dbo.upGetServerQA to manager, reviewer