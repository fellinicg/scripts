use Omni
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwServer'))
drop view dbo.vwServer
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20140221
-- ============================================================
create view dbo.vwServer
as
select h.HardwareID, c.Name, c.Description, o.Type, s.Status, c.Serial_Num, c.Computer_Name, c.CPU, 
	c.CPU_Count, c.CPU_Speed, c.MAC, b.Brand, c.Model, c.OS_Name, c.OS_ServicePack, c.OS_Version, c.Product, c.RAM,
	a.Warranty_Exp
from dbo.vwServerAlloy h
	inner join AN6.dbo.Computers c on h.AlloyID = c.ID
	left join AN6.dbo.Object_Type o on c.Type_ID = o.id
	left join AN6.dbo.Status s on c.Status_ID = s.ID
	left join AN6.dbo.Brands b on c.Manufacturer_ID = b.ID
	left join AN6.dbo.Assets a on c.ID = a.Associated_CI_ID
go
grant select on dbo.vwServer to omni