use Refresh

select s.name 'StoredProcedure', p.name 'Parameter', t.name 'DataType', p.max_length 'Size', case p.is_output when 0 then 'Input' else 'Output' end 'ParameterDirection'
from sys.procedures s
	inner join sys.parameters p on s.object_id = p.object_id
	inner join sys.types t on p.system_type_id = t.system_type_id
where charindex('upAdd', s.name) = 1
order by s.name, p.parameter_id