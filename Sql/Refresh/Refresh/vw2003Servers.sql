use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vw2003Servers'))
drop view dbo.vw2003Servers
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150910
-- ============================================================
create view dbo.vw2003Servers
as
	select
		ServerID
		, OriginalName
		, OriginalRack
		, Location
		, Application
		, ApplicationOwner
		, NewName
		, NewRack
		, NewModel
		, NewSerialNumber
		, IsVirtual
		, Status
		, AlloyID
	from dbo.tbServer
	where OriginalName not in ('N/A')
go
grant select on dbo.vw2003Servers to manager, reviewer