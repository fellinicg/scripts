use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddEventLog') and type in (N'P', N'PC'))
drop procedure dbo.upAddEventLog
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150609
-- ============================================================
create procedure dbo.upAddEventLog
	@QualityAssuranceID int
	, @ApplicationLogSize bigint
	, @ApplicationLogRetention int
	, @SecurityLogSize bigint
	, @SecurityLogRetention int
	, @SetupLogSize bigint
	, @SetupLogRetention int
	, @SystemLogSize bigint
	, @SystemLogRetention int
	, @ForwardedLogSize bigint
	, @ForwardedLogRetention int
	, @Success bit output
as
begin

	-- Initialize settings
	set nocount on

	-- Add EventLog
	insert into dbo.tbEventLog (QualityAssuranceID, ApplicationLogSize, ApplicationLogRetention, SecurityLogSize, SecurityLogRetention, SetupLogSize, SetupLogRetention, SystemLogSize, SystemLogRetention, ForwardedLogSize, ForwardedLogRetention)
	values (@QualityAssuranceID, @ApplicationLogSize, @ApplicationLogRetention, @SecurityLogSize, @SecurityLogRetention, @SetupLogSize, @SetupLogRetention, @SystemLogSize, @SystemLogRetention, @ForwardedLogSize, @ForwardedLogRetention)
	
	-- Check for success
	if (@@error = 0) and (@@rowcount > 0) set @Success = 1
	else set @Success = 0

end
go
grant exec on dbo.upAddEventLog to qa