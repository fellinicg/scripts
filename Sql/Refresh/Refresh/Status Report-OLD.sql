use Refresh

set nocount on

declare @WkEnding datetime
declare @WkBeginning datetime

set @WkEnding = '9/11/2015'

set @WkEnding = dateadd(s, 86399, @WkEnding)
set @WkBeginning = dateadd(d, -6, convert(varchar, @WkEnding, 101))

-- Week Ending
--
select convert(varchar, @WkEnding, 101) 'Week Ending'

select '' 'Current Statistics' where 1 = 2

select 
	[ ]
	, Unbuilt 'To Be Built'
	, Built
	, [QA'd]
	, Racked
	, Delivered
	, Excluded
	, Unbuilt + Built + [QA'd] + Racked + Delivered + Excluded 'Total'
from
(
	select 
		a.MachineType ' '
		, coalesce(max(case when Status = 'U' then Ct end), '0') 'Unbuilt'
		, coalesce(max(case when Status = 'B' then Ct end), '0') 'Built'
		, coalesce(max(case when Status = 'Q' then Ct end), '0') 'QA'
		, coalesce(max(case when Status = 'R' then Ct end), '0') 'Racked'
		, coalesce(max(case when Status = 'D' then Ct end), '0') 'Delivered'
		, coalesce(max(case when Status = 'E' then Ct end), '0') 'Excluded'

	from
	(
		select Status, case IsVirtual when 0 then 'Physical' else 'Virtual' end MachineType, count(1) 'Ct'
		from dbo.vw2003Servers
		group by Status, IsVirtual
	) a
	group by MachineType
) b

--select isnull(b.Status, a.Status) Status, [# of Servers]
--from (
--	select case grouping(Status) when 1 then 'Total' else isnull(Status, 'Unknown') end Status, count(1) '# of Servers'
--	from dbo.vw2003Servers
--	group by Status
--	with rollup) a
--left join dbo.tfstatus() b on a.Status = b.StatusID
--order by isnull(b.Sort, 99)

select '' 'Last Week''s Progress' where 1 = 2

select 
	a.MachineType ' '
	, coalesce(max(case when Status = 'E' then Ct end), '0') 'Excluded'
	, coalesce(max(case when Status = 'B' then Ct end), '0') 'Built'
	, coalesce(max(case when Status = 'Q' then Ct end), '0') 'QA'
	, coalesce(max(case when Status = 'R' then Ct end), '0') 'Racked'
	, coalesce(max(case when Status = 'D' then Ct end), '0') 'Delivered'
from
(
	select a.NewValue Status, case b.IsVirtual when 0 then 'Physical' else 'Virtual' end MachineType, count(1) 'Ct'
	from dbo.tbAudit a
	inner join dbo.vw2003Servers b on a.PrimaryKey = b.ServerID
	where a.TableName = 'tbServer'
		and a.ColumnName = 'Status'
		and a.CreateDt between @WkBeginning and @WkEnding
	group by a.NewValue, b.IsVirtual
	) a
group by MachineType

--select 
--	a.MachineType ' '
--	, coalesce(max(case when Status = 'E' then Ct end), '0') '# Servers Removed From Project'
--	, coalesce(max(case when Status = 'B' then Ct end), '0') '# Built'
--	, coalesce(max(case when Status = 'Q' then Ct end), '0') '# QA''d'
--	, coalesce(max(case when Status = 'R' then Ct end), '0') '# Racked Awaiting Networking/Power'
--	, coalesce(max(case when Status = 'D' then Ct end), '0') '# Turned Over to App Owner'
--from 
--(
--	select a.NewValue Status, case b.IsVirtual when 0 then 'Physical' else 'Virtual' end MachineType, count(1) 'Ct'
--	from dbo.tbAudit a
--	inner join dbo.vw2003Servers b on a.PrimaryKey = b.ServerID
--	where a.TableName = 'tbServer'
--		and a.ColumnName = 'Status'
--		and a.CreateDt between @WkBeginning and @WkEnding
--		and b.IsVirtual = 0
--	group by a.NewValue, b.IsVirtual
--) a
--group by MachineType
--union
--select 
--	a.MachineType ' '
--	, coalesce(max(case when Status = 'E' then Ct end), '0') '# Servers Removed From Project'
--	, coalesce(max(case when Status = 'B' then Ct end), '0') '# Built'
--	, coalesce(max(case when Status = 'Q' then Ct end), '0') '# QA''d'
--	, coalesce(max(case when Status = 'R' then Ct end), '0') '# Racked Awaiting Networking/Power'
--	, coalesce(max(case when Status = 'D' then Ct end), '0') '# Turned Over to App Owner'
--from 
--(
--	select a.NewValue Status, case b.IsVirtual when 0 then 'Physical' else 'Virtual' end MachineType, count(1) 'Ct'
--	from dbo.tbAudit a
--	inner join dbo.vw2003Servers b on a.PrimaryKey = b.ServerID
--	where a.TableName = 'tbServer'
--		and a.ColumnName = 'Status'
--		and a.CreateDt between @WkBeginning and @WkEnding
--		and b.IsVirtual = 1
--	group by a.NewValue, b.IsVirtual
--) a
--group by MachineType

select '' 'RK Scorecard' where 1 = 2

select 
	[ ]
	, Unbuilt + Built + [QA'd] + Racked + Delivered + Excluded 'Number of Servers'
	, Excluded '# Servers Removed From Project'
	, Built + [QA'd] + Racked + Delivered  '# Built'
	, [QA'd] + Racked + Delivered '# Built and QA''d'
	, Racked '# Racked Awaiting Networking/Power'
	, Delivered '# Turned Over to App Owner'
	, Unbuilt + Built + [QA'd] + Racked 'Remaining # of Servers to be Turned Over to App Owner'
	 
from
(
	select 
		a.MachineType ' '
		, coalesce(max(case when Status = 'U' then Ct end), '0') 'Unbuilt'
		, coalesce(max(case when Status = 'B' then Ct end), '0') 'Built'
		, coalesce(max(case when Status = 'Q' then Ct end), '0') 'QA''d'
		, coalesce(max(case when Status = 'R' then Ct end), '0') 'Racked'
		, coalesce(max(case when Status = 'D' then Ct end), '0') 'Delivered'
		, coalesce(max(case when Status = 'E' then Ct end), '0') 'Excluded'

	from
	(
		select Status, case IsVirtual when 0 then 'Physical' else 'Virtual' end MachineType, count(1) 'Ct'
		from dbo.vw2003Servers
		group by Status, IsVirtual
	) a
	group by MachineType
) b

--select 
--	a.MachineType ' '
--	, coalesce(max(case when Status = 'U' then Ct end), '0')
--		+ coalesce(max(case when Status = 'E' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'B' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'Q' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'R' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'D' then Ct end), '0') 'Number of Servers'
--	, coalesce(max(case when Status = 'E' then Ct end), '0') '# Servers Removed From Project'
--	, coalesce(max(case when Status = 'B' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'Q' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'R' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'D' then Ct end), '0') '# Built'
--	, coalesce(max(case when Status = 'Q' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'R' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'D' then Ct end), '0') '# Built and QA''d'
--	, coalesce(max(case when Status = 'R' then Ct end), '0') '# Racked Awaiting Networking/Power'
--	, coalesce(max(case when Status = 'D' then Ct end), '0') '# Turned Over to App Owner'
--	, coalesce(max(case when Status = 'U' then Ct end), '0')
--		+ coalesce(max(case when Status = 'B' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'Q' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'R' then Ct end), '0') 'Remaining # of Servers to be Turned Over to App Owner'
--from 
--(
--	select distinct case grouping(Status) when 1 then 'Total' else isnull(Status, 'Unknown') end Status, case IsVirtual when 1 then 'Virtual' else 'Physical' end 'MachineType', count(1) 'Ct'
--	from dbo.vw2003Servers
--	where IsVirtual = 0
--	group by Status, IsVirtual
--) a
--group by MachineType
--union
--select 
--	a.MachineType
--	, coalesce(max(case when Status = 'U' then Ct end), '0')
--		+ coalesce(max(case when Status = 'E' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'B' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'Q' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'R' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'D' then Ct end), '0') 'Number of Servers'
--	, coalesce(max(case when Status = 'E' then Ct end), '0') '# Servers Removed From Project'
--	, coalesce(max(case when Status = 'B' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'Q' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'R' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'D' then Ct end), '0') '# Built'
--	, coalesce(max(case when Status = 'Q' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'R' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'D' then Ct end), '0') '# Built and QA''d'
--	, coalesce(max(case when Status = 'R' then Ct end), '0') '# Racked Awaiting Networking/Power'
--	, coalesce(max(case when Status = 'D' then Ct end), '0') '# Turned Over to App Owner'
--	, coalesce(max(case when Status = 'U' then Ct end), '0')
--		+ coalesce(max(case when Status = 'B' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'Q' then Ct end), '0') 
--		+ coalesce(max(case when Status = 'R' then Ct end), '0') 'Remaining # of Servers to be Turned Over to App Owner'
--from 
--(
--	select distinct case grouping(Status) when 1 then 'Total' else isnull(Status, 'Unknown') end Status, case IsVirtual when 1 then 'Virtual' else 'Physical' end 'MachineType', count(1) 'Ct'
--	from dbo.vw2003Servers
--	where IsVirtual = 1
--	group by Status, IsVirtual
--) a
--group by MachineType

select '' 'Notes' where 1 = 2

select distinct 
	s.OriginalName 'Server'
	, isnull(s.NewName, '') 'NewName'
	, dbo.sfStatus(Status) 'Status'
	, dbo.sfServerNotes(s.ServerID, @WkBeginning, @WkEnding) 'Notes'
from dbo.vw2003Servers s
inner join dbo.tbNote n on s.ServerID = n.ServerID
where n.CreateDt between @WkBeginning and @WkEnding
order by Status, s.OriginalName
