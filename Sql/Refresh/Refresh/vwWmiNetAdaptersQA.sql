use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwWmiNetAdaptersQA'))
drop view dbo.vwWmiNetAdaptersQA
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20151103
-- ============================================================
create view dbo.vwWmiNetAdaptersQA
as
	select 
		coalesce(ServerID, '') 'ServerID'
		, max(coalesce(NewName, '')) 'ServerName'
		, max(coalesce(case when Field = 'AddressFamily' then Value end, '')) 'AddressFamily'
		, max(coalesce(case when Field = 'ipv4address' then Value end, '')) 'ipv4address'
		, max(coalesce(case when Field = 'ipv6address' then Value end, '')) 'ipv6address'
		, max(coalesce(case when Field = 'prefixlength' then Value end, '')) 'prefixlength'
		, max(coalesce(case when Field = 'AddressState' then Value end, '')) 'AddressState'
		, max(coalesce(case when Field = 'Link-Layer Topology Discovery Responder' then Value end, '')) 'Link-Layer Topology Discovery Responder'
		, max(coalesce(case when Field = 'Link-Layer Topology Discovery Mapper I/O Driver' then Value end, '')) 'Link-Layer Topology Discovery Mapper I/O Driver'
		, max(coalesce(case when Field = 'Microsoft Network Adapter Multiplexor Protocol' then Value end, '')) 'Microsoft Network Adapter Multiplexor Protocol'
		, max(coalesce(case when Field = 'Client for Microsoft Networks' then Value end, '')) 'Client for Microsoft Networks'
		, max(coalesce(case when Field = 'QoS Packet Scheduler' then Value end, '')) 'QoS Packet Scheduler'
		, max(coalesce(case when Field = 'File and Printer Sharing for Microsoft Networks' then Value end, '')) 'File and Printer Sharing for Microsoft Networks'
		, max(coalesce(case when Field = 'Internet Protocol Version 6 (TCP/IPv6)' then Value end, '')) 'Internet Protocol Version 6 (TCP/IPv6)'
		, max(coalesce(case when Field = 'Internet Protocol Version 4 (TCP/IPv4)' then Value end, '')) 'Internet Protocol Version 4 (TCP/IPv4)'
		, max(coalesce(case when Field = 'DHCPLeaseExpires' then Value end, '')) 'DHCPLeaseExpires'
		, max(coalesce(case when Field = 'DHCPEnabled' then Value end, '')) 'DHCPEnabled'
		, max(coalesce(case when Field = 'DHCPLeaseObtained' then Value end, '')) 'DHCPLeaseObtained'
		, max(coalesce(case when Field = 'DHCPServer' then Value end, '')) 'DHCPServer'
		, max(coalesce(case when Field = 'DNSDomain' then Value end, '')) 'DNSDomain'
		, max(coalesce(case when Field = 'DNSDomainSuffixSearchOrder' then Value end, '')) 'DNSDomainSuffixSearchOrder'
		, max(coalesce(case when Field = 'DNSEnabledForWINSResolution' then Value end, '')) 'DNSEnabledForWINSResolution'
		, max(coalesce(case when Field = 'DNSHostName' then Value end, '')) 'DNSHostName'
		, max(coalesce(case when Field = 'DNSServerSearchOrder' then Value end, '')) 'DNSServerSearchOrder'
		, max(coalesce(case when Field = 'DomainDNSRegistrationEnabled' then Value end, '')) 'DomainDNSRegistrationEnabled'
		, max(coalesce(case when Field = 'FullDNSRegistrationEnabled' then Value end, '')) 'FullDNSRegistrationEnabled'
		, max(coalesce(case when Field = 'IPAddress' then Value end, '')) 'IPAddress'
		, max(coalesce(case when Field = 'IPConnectionMetric' then Value end, '')) 'IPConnectionMetric'
		, max(coalesce(case when Field = 'IPEnabled' then Value end, '')) 'IPEnabled'
		, max(coalesce(case when Field = 'IPFilterSecurityEnabled' then Value end, '')) 'IPFilterSecurityEnabled'
		, max(coalesce(case when Field = 'WINSEnableLMHostsLookup' then Value end, '')) 'WINSEnableLMHostsLookup'
		, max(coalesce(case when Field = 'DefaultIPGateway' then Value end, '')) 'DefaultIPGateway'
		, max(coalesce(case when Field = 'TcpipNetbiosOptions' then Value end, '')) 'TcpipNetbiosOptions'
		, max(coalesce(case when Field = 'ifIndex' then Value end, '')) 'ifIndex'
		, max(coalesce(case when Field = 'ifAlias' then Value end, '')) 'ifAlias'
		, max(coalesce(case when Field = 'ifDesc' then Value end, '')) 'ifDesc'
		, max(coalesce(case when Field = 'ActiveMaximumTransmissionUnit' then Value end, '')) 'ActiveMaximumTransmissionUnit'
		, max(coalesce(case when Field = 'AdminStatus' then Value end, '')) 'AdminStatus'
		, max(coalesce(case when Field = 'componentid' then Value end, '')) 'componentid'
		, max(coalesce(case when Field = 'devicewakeupenable' then Value end, '')) 'devicewakeupenable'
		, max(coalesce(case when Field = 'driverdate' then Value end, '')) 'driverdate'
		, max(coalesce(case when Field = 'DriverVersion' then Value end, '')) 'DriverVersion'
		, max(coalesce(case when Field = 'DriverFileName' then Value end, '')) 'DriverFileName'
		, max(coalesce(case when Field = 'fullduplex' then Value end, '')) 'fullduplex'
		, max(coalesce(case when Field = 'ifName' then Value end, '')) 'ifName'
		, max(coalesce(case when Field = 'instanceid' then Value end, '')) 'instanceid'
		, max(coalesce(case when Field = 'MacAddress' then Value end, '')) 'MacAddress'
		, max(coalesce(case when Field = 'MediaConnectionState' then Value end, '')) 'MediaConnectionState'
		, max(coalesce(case when Field = 'NdisVersion' then Value end, '')) 'NdisVersion'
		, max(coalesce(case when Field = 'PhysicalMediaType' then Value end, '')) 'PhysicalMediaType'
		, max(coalesce(case when Field = 'speed' then Value end, '')) 'speed'
		, max(coalesce(case when Field = 'Status' then Value end, '')) 'Status'
	from (
		select
			a.ServerID
			, a.NewName
			, b.Section
			, b.Field
			, b.Value
		from dbo.vwLatestQualityAssurance a
		inner join dbo.tbFieldValue b on a.QualityAssuranceID = b.QualityAssuranceID
		where 
			b.Category = 'WMI'
			and b.Subcategory in ('NetAdapters')
	) a
	group by ServerID, Section
go