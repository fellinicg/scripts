use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwOsUpdatesQA'))
drop view dbo.vwOsUpdatesQA
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150916
-- ============================================================
create view dbo.vwOsUpdatesQA
as
	select 
		ServerID
		, max(NewName) 'ServerName'
		, Section
		, max(case when Field = 'InstalledOn' then Value end) 'InstalledOn'
		, max(case when Field = 'Description' then Value end) 'Description'
	from (
		select
			a.ServerID
			, a.NewName
			, b.Section
			, b.Field
			, b.Value
		from dbo.vwLatestQualityAssurance a
		inner join dbo.tbFieldValue b on a.QualityAssuranceID = b.QualityAssuranceID
		where 
			b.Category = 'OS'
			and b.Subcategory in ('Updates')
	) a
	group by ServerID, Section
go


