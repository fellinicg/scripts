use Refresh
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbFieldValue') and type in (N'U'))
drop table dbo.tbFieldValue
go

create table dbo.tbFieldValue(
	FieldValueID int identity(1,1) not null,
	QualityAssuranceID int not null,
	Category varchar(20) not null,
	Subcategory varchar(40) null,
	Section varchar(50) null,
	Field varchar(200) not null,
	Value varchar(500) not null,
	Subvalue varchar(500) null,
	Sort smallint not null constraint DF_tbFieldValue_Sort default (0),
constraint PK_tbFieldValue primary key clustered 
(
	FieldValueID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off
