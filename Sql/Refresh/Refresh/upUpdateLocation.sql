use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateLocation') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateLocation
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150602
-- ============================================================
create procedure dbo.upUpdateLocation
	@ServerID int
	,@Location varchar (50)
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @OldValue varchar(50)
	declare @Proc varchar(100)

	-- Proc
	set @Proc = object_name(@@procid)

	-- Old Value
	select @OldValue = Location from dbo.tbServer where ServerID = @ServerID

	-- Update data
	update dbo.tbServer
	set Location = @Location
	where ServerID = @ServerID

	-- Audit
	exec dbo.upAddAudit @Proc, 'tbServer', @ServerID, 'Location', @OldValue, @Location
	
end
go
grant exec on dbo.upUpdateLocation to manager