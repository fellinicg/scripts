use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddProcessor') and type in (N'P', N'PC'))
drop procedure dbo.upAddProcessor
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150609
-- ============================================================
create procedure dbo.upAddProcessor
	@QualityAssuranceID int
	, @Availability tinyint
	, @Caption varchar(64)
	, @CpuStatus tinyint
	, @CurrentClockSpeed int
	, @CurrentVoltage smallint
	, @DeviceID varchar(10)
	, @Family int
	, @L3CacheSize int
	, @L3CacheSpeed int
	, @Level int
	, @Manufacturer varchar(30)
	, @MaxClockSpeed int
	, @Name varchar(100)
	, @NumberOfCores smallint
	, @NumberOfLogicalProcessors smallint
	, @PowerManagementSupported bit
	, @ProcessorType tinyint
	, @Revision int
	, @SocketDesignation varchar(20)
	, @Status varchar(10)
	, @StatusInfo tinyint
	, @Success bit output
as
begin

	-- Initialize settings
	set nocount on

	-- Add Processor
	insert into dbo.tbProcessor (QualityAssuranceID, Availability, Caption, CpuStatus, CurrentClockSpeed, CurrentVoltage, DeviceID, Family, L3CacheSize, L3CacheSpeed, Level, Manufacturer, MaxClockSpeed, Name, NumberOfCores, NumberOfLogicalProcessors, PowerManagementSupported, ProcessorType, Revision, SocketDesignation, Status, StatusInfo)
	values (@QualityAssuranceID, @Availability, @Caption, @CpuStatus, @CurrentClockSpeed, @CurrentVoltage, @DeviceID, @Family, @L3CacheSize, @L3CacheSpeed, @Level, @Manufacturer, @MaxClockSpeed, @Name, @NumberOfCores, @NumberOfLogicalProcessors, @PowerManagementSupported, @ProcessorType, @Revision, @SocketDesignation, @Status, @StatusInfo)
	
	-- Check for success
	if (@@error = 0) and (@@rowcount > 0) set @Success = 1
	else set @Success = 0

end
go
grant exec on dbo.upAddProcessor to qa