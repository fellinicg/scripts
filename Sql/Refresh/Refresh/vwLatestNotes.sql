use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwLatestNotes'))
drop view dbo.vwLatestNotes
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150603
-- ============================================================
create view dbo.vwLatestNotes
as
	select 
		a.ServerID
		, a.Note
		, a.Author
		, a.CreateDT
	from dbo.tbNote a
		inner join (
			select ServerID, max(NoteID) 'NoteID'
			from dbo.tbNote
			where Hide = 0
			group by ServerID
		) b on a.NoteID = b.NoteID
go


