use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwServerAudit'))
drop view dbo.vwServerAudit
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150826
-- ============================================================
create view dbo.vwServerAudit
as
	select top 100000
		s.ServerID
		, s.OriginalName
		, s.NewName
		, a.ColumnName + ' was changed to ' + isnull(a.NewValue, 'NULL') + ' from ' + isnull(a.OldValue, 'NULL') + ' by ' + a.UpdatedBy 'Desc'
		, a.CreateDT
		, a.ColumnName
		, a.OldValue
		, a.NewValue
		, a.UpdatedBy
	from dbo.vw2003Servers s
	left join dbo.tbAudit a on s.ServerID = a.PrimaryKey
	where a.TableName = 'tbServer'
	order by s.ServerID, a.ColumnName, a.CreateDT desc
go


