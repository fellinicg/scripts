use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddComputer') and type in (N'P', N'PC'))
drop procedure dbo.upAddComputer
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150609
-- ============================================================
create procedure dbo.upAddComputer
	@QualityAssuranceID int
	, @CurrentTimeZone smallint
	, @EnableDaylightSavingsTime bit
	, @DaylightInEffect bit
	, @Manufacturer varchar(200) = null
	, @Model varchar(200) = null
	, @NumberOfLogicalProcessors tinyint
	, @NumberOfProcessors tinyint
	, @TotalPhysicalMemory bigint
	, @Success bit output
as
begin

	-- Initialize settings
	set nocount on

	-- Add Computer
	insert into dbo.tbComputer (QualityAssuranceID, CurrentTimeZone, EnableDaylightSavingsTime, DaylightInEffect, Manufacturer, Model, NumberOfLogicalProcessors, NumberOfProcessors, TotalPhysicalMemory)
	values (@QualityAssuranceID, @CurrentTimeZone, @EnableDaylightSavingsTime, @DaylightInEffect, @Manufacturer, @Model, @NumberOfLogicalProcessors, @NumberOfProcessors, @TotalPhysicalMemory)
	
	-- Check for success
	if (@@error = 0) and (@@rowcount > 0) set @Success = 1
	else set @Success = 0

end
go
grant exec on dbo.upAddComputer to qa