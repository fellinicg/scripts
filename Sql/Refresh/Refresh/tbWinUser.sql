use Refresh
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbWinUser') and type in (N'U'))
drop table dbo.tbWinUser
go

create table dbo.tbWinUser(
	WinUserID int identity(1,1) not null,
	QualityAssuranceID int not null,
	Caption varchar(64) not null,
	Description varchar(100) not null,
	Disabled bit not null,
	Lockout bit not null,
	Name varchar(50) not null,
	PasswordChangeable bit not null,
	PasswordExpires bit not null,
	PasswordRequired bit not null,
	SID varchar(100) not null,
	Status varchar(10) not null,
constraint PK_tbWinUser primary key clustered 
(
	WinUserID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off
