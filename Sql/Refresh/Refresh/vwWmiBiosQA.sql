use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwWmiBiosQA'))
drop view dbo.vwWmiBiosQA
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20151103
-- ============================================================
create view dbo.vwWmiBiosQA
as
	select 
		coalesce(ServerID, '') 'ServerID'
		, max(coalesce(NewName, '')) 'ServerName'
		, max(coalesce(case when Field = 'biosversion' then Value end, '')) 'biosversion'
		, max(coalesce(case when Field = 'currentlanguage' then Value end, '')) 'currentlanguage'
		, max(coalesce(case when Field = 'description' then Value end, '')) 'description'
		, max(coalesce(case when Field = 'installablelanguages' then Value end, '')) 'installablelanguages'
		, max(coalesce(case when Field = 'manufacturer' then Value end, '')) 'manufacturer'
		, max(coalesce(case when Field = 'serialnumber' then Value end, '')) 'serialnumber'
		, max(coalesce(case when Field = 'SMBIOSBIOSVersion' then Value end, '')) 'SMBIOSBIOSVersion'
	from (
		select
			a.ServerID
			, a.NewName
			, b.Field
			, b.Value
		from dbo.vwLatestQualityAssurance a
		inner join dbo.tbFieldValue b on a.QualityAssuranceID = b.QualityAssuranceID
		where 
			b.Category = 'WMI'
			and b.Subcategory in ('Bios')
	) a
	group by ServerID
go