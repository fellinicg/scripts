use Refresh
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbQualityAssurance') and type in (N'U'))
drop table dbo.tbQualityAssurance
go

create table dbo.tbQualityAssurance(
	QualityAssuranceID int identity(1,1) not null,
	ServerID int not null,
	CreateDT datetime not null constraint DF_tbQualityAssurance_CreateDT  default (getdate()),
 constraint PK_tbQualityAssurance primary key clustered 
(
	QualityAssuranceID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

