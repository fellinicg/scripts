use Refresh

set nocount on

declare @counts as table (MType varchar(20), Unbuilt int, Built int, QA int, Racked int, Delivered int, Excluded int)
declare @WkEnding datetime
declare @WkBeginning datetime

set @WkEnding = '12/11/2015'

set @WkEnding = dateadd(s, 86399, @WkEnding)
set @WkBeginning = dateadd(d, -6, convert(varchar, @WkEnding, 101))

insert into @counts
select 
	a.MachineType
	, coalesce(max(case when Status = 'U' then Ct end), '0') 'Unbuilt'
	, coalesce(max(case when Status = 'B' then Ct end), '0') 'Built'
	, coalesce(max(case when Status = 'Q' then Ct end), '0') 'QA'
	, coalesce(max(case when Status = 'R' then Ct end), '0') 'Racked'
	, coalesce(max(case when Status = 'D' then Ct end), '0') 'Delivered'
	, coalesce(max(case when Status = 'E' then Ct end), '0') 'Excluded'
from
(
	select Status, case IsVirtual when 0 then 'Physical' else 'Virtual' end MachineType, count(1) 'Ct'
	from dbo.vw2003Servers
	group by Status, IsVirtual
) a
group by MachineType

select
	Mtype 'Project Overview'
	, Unbuilt + Built + QA + Racked + Delivered 'Included in Project'
	, Racked + Delivered
from @counts

select
	Mtype 'Project Totals'
	, Unbuilt + Built + QA + Racked + Delivered + Excluded 'Total'
	, Excluded
	, Unbuilt + Built + QA + Racked + Delivered 'Included in Project'
from @counts

select 
	MType 'Project Statistics'
	, Unbuilt
	, Built
	, QA
	, Racked
	, Delivered
	, Excluded
from @counts

select 
	a.MachineType 'Weeks Statistics'
	, coalesce(sum(case when Status = 'E' then 1 end), '0') 'Excluded'
	, coalesce(sum(case when Status = 'B' then 1 end), '0') 'Built'
	, coalesce(sum(case when Status = 'Q' then 1 end), '0') 'QA'
	, coalesce(sum(case when Status = 'R' then 1 end), '0') 'Racked'
	, coalesce(sum(case when Status = 'D' then 1 end), '0') 'Delivered'
from
(
	select a.NewValue Status, case b.IsVirtual when 0 then 'Physical' else 'Virtual' end MachineType
	from dbo.tbAudit a
	inner join dbo.vw2003Servers b on a.PrimaryKey = b.ServerID
	where 
	a.TableName = 'tbServer'
		and a.ColumnName = 'Status'
		and a.CreateDt between @WkBeginning and @WkEnding
	group by b.ServerID, a.NewValue, b.IsVirtual
	) a
group by MachineType

select
	Mtype 'Project Scorecard'
	, Excluded
	, Built + QA + Racked + Delivered  'Built'
	, QA + Racked + Delivered 'Built and QA''d'
	, Racked 'Racked Awaiting Networking'
	, Delivered 'Delivered To Client'
	, Unbuilt + Built + QA + Racked 'Remaining Deliverables'
from @counts

select distinct 
	s.OriginalName 'Original Name'
	, isnull(s.NewName, '') 'New Name'
	, dbo.sfStatus(Status) 'Status'
	, dbo.sfServerNotes(s.ServerID, @WkBeginning, @WkEnding) 'Notes'
from dbo.vw2003Servers s
inner join dbo.tbNote n on s.ServerID = n.ServerID
where n.CreateDt between @WkBeginning and @WkEnding
order by Status, s.OriginalName
