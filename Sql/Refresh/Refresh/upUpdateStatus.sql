use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateStatus') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateStatus
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150604
-- ============================================================
create procedure dbo.upUpdateStatus
	@ServerID int
	,@Status char (1)
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @OldValue char(1)
	declare @Proc varchar(100)

	-- Proc
	set @Proc = object_name(@@procid)

	-- Old Value
	select @OldValue = Status from dbo.tbServer where ServerID = @ServerID

	-- Update data
	update dbo.tbServer
	set Status = upper(@Status)
	where ServerID = @ServerID

	-- Audit
	exec dbo.upAddAudit @Proc, 'tbServer', @ServerID, 'Status', @OldValue, @Status

end
go
grant exec on dbo.upUpdateStatus to manager