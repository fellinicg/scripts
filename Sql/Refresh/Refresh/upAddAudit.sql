use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddAudit') and type in (N'P', N'PC'))
drop procedure dbo.upAddAudit
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150603
-- ============================================================
create procedure dbo.upAddAudit
	@SourceProcedure varchar(100)
	, @TableName varchar(20)
	, @PrimaryKey int
	, @ColumnName varchar(50)
	, @OldValue varchar(500)
	, @NewValue varchar(500)
as
begin

	-- Initialize settings
	set nocount on

	-- Add audit data
	insert into dbo.tbAudit (SourceProcedure, TableName, PrimaryKey, ColumnName, OldValue, NewValue, UpdatedBy)
	values (@SourceProcedure, @TableName, @PrimaryKey, @ColumnName, @OldValue, @NewValue, system_user)

end
go
grant exec on dbo.upAddAudit to manager