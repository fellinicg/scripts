use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetServers') and type in (N'P', N'PC'))
drop procedure dbo.upGetServers
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150602
-- ============================================================
create procedure dbo.upGetServers
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		s.ServerID
		, s.OriginalName
		, case s.IsVirtual when 1 then 'Virtual' when 0 then 'Physical' end 'Type'
		, s.OriginalRack
		, s.Location
		, s.Application
		, s.ApplicationOwner
		, s.NewName
		, s.NewRack
		, s.NewModel
		, s.NewSerialNumber
		, s.Status
		, n.Note 
		, n.CreateDT 
		, n.Author
	from dbo.vw2003Servers s
		left join dbo.vwLatestNotes n on s.ServerID = n.ServerID
	order by OriginalName

end
go
grant exec on dbo.upGetServers to manager, reviewer