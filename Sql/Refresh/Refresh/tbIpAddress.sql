use Refresh
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbIpAddress') and type in (N'U'))
drop table dbo.tbIpAddress
go

create table dbo.tbIpAddress(
	IpAddressID int identity(1,1) not null,
	ServerID int not null,
	OriginalIP varchar(15) null,
	NewIP varchar(15) null,
	Adapter varchar(50) null,
 constraint PK_tbComputer primary key clustered 
(
	IpAddressID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off




