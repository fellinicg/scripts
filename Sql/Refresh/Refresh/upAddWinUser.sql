use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddWinUser') and type in (N'P', N'PC'))
drop procedure dbo.upAddWinUser
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150609
-- ============================================================
create procedure dbo.upAddWinUser
	@QualityAssuranceID int
	, @Caption varchar(64)
	, @Description varchar(100)
	, @Disabled bit
	, @Lockout bit
	, @Name varchar(50)
	, @PasswordChangeable bit
	, @PasswordExpires bit
	, @PasswordRequired bit
	, @SID varchar(100)
	, @Status varchar(10)
	, @Success bit output
as
begin

	-- Initialize settings
	set nocount on

	-- Add WinUser
	insert into dbo.tbWinUser (QualityAssuranceID, Caption, Description, Disabled, Lockout, Name, PasswordChangeable, PasswordExpires, PasswordRequired, SID, Status)
	values (@QualityAssuranceID, @Caption, @Description, @Disabled, @Lockout, @Name, @PasswordChangeable, @PasswordExpires, @PasswordRequired, @SID, @Status)
	
	-- Check for success
	if (@@error = 0) and (@@rowcount > 0) set @Success = 1
	else set @Success = 0

end
go
grant exec on dbo.upAddWinUser to qa