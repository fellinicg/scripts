use Win2003Phase2

set nocount on

declare @counts as table (StatusCode char(1), StatusDesc varchar(50), MType varchar(20), Ct int, Stage tinyint, Substage tinyint)
declare @WkEnding datetime
declare @WkBeginning datetime

set @WkEnding = '3/8/2017'

set @WkEnding = dateadd(s, 86399, @WkEnding)
set @WkBeginning = dateadd(d, -6, convert(varchar, @WkEnding, 101))

insert into @counts
select 
	StatusCode
	, StatusDesc
	, case IsVirtual when 0 then 'Physical' else 'Virtual' end MachineType
	, case IsVirtual when 0 then count(1) when 1 then count(1) else 0 end Ct 
	, Stage
	, Substage
from dbo.vwPhase2Servers
group by StatusCode, StatusDesc, IsVirtual, Stage, Substage

select 
	a.Mtype
	, TotalServers
	, TotalAssessed
from 
	(select Mtype, sum(ct) 'TotalServers' from @counts group by Mtype) a
	join 
	(select Mtype, sum(ct) 'TotalAssessed' from @counts where Stage = 3 group by Mtype, Stage) b on a.MType = b.MType

select
	MachineType
	, isnull(R,0) [Racked]
	, isnull(D,0) [Delivered]
	, isnull(I,0) [In-progress]
	, isnull(O,0) [Exception Request]
	--, isnull(A,0) [To Be Migrated]
	, isnull(A,0) [To Be Migrated By Owner]
	, isnull(T,0) [To Be Migrated By TEC]
	, isnull(M,0) [Migrated By Owner]
	, isnull(X,0) [Application Retired]
	, isnull(C,0) [Controlled Shutdown]
from 
(
	select 
		StatusCode
		, MType MachineType
		, Ct 
	from @counts
) a
pivot
(
	max(Ct)
	for StatusCode in (D,R,I,O,A,T,M,X,C)
	--for StatusDesc in ([Racked],[Delivered],[In-progress],[Omission Request],[Testing],[Assessed],[Floater],[Migrated],[Upgraded],[Production],[Decommissioned])
) p

select 
	MachineType
	, isnull(R,0) [Racked]
	, isnull(D,0) [Delivered]
	, isnull(I,0) [In-progress]
	, isnull(O,0) [Exception Request]
	, isnull(A,0) [To Be Migrated]
	, isnull(M,0) [Migrated By Owner]
	, isnull(X,0) [Application Retired]
	, isnull(C,0) [Controlled Shutdown]
from 
(	
	select 
		isnull(a.NewValue, s.StatusCode) StatusCode
		, case s.IsVirtual when 0 then 'Physical' else 'Virtual' end MachineType
		, case IsVirtual when 0 then count(1) when 1 then count(1) else 0 end Ct 
	from dbo.vwPhase2Servers s
	left outer join dbo.tbAudit a on s.ServerID = isnull(a.PrimaryKey, s.ServerID)
	where isnull(a.TableName, 'tbServer') = 'tbServer'
		and isnull(a.ColumnName, 'Status') = 'Status'
		and isnull(a.CreateDt, @WkBeginning) between @WkBeginning and @WkEnding
	group by s.StatusCode, a.NewValue, s.IsVirtual
) a
pivot
(
	max(Ct)
	for StatusCode in (D,R,I,O,A,M,X,C)
--	for StatusDesc in ([Racked],[Delivered],[In-progress],[Omission Request],[Testing],[Assessed],[Floater],[Migrated],[Upgraded],[Production],[Decommissioned])
) p

select distinct
	--distinct char(39) +  Status + char(39) + ','
	OriginalName 'Windows 2003'
	, NewName 'Windows 2012'
	, StatusDesc
	, Application
	, ApplicationOwner
	, dbo.sfServerNotes(s.ServerID, @WkBeginning, @WkEnding) Notes
from dbo.vwPhase2Servers s
	inner join dbo.tbNote n on s.ServerID = n.ServerID
where n.CreateDt between @WkBeginning and @WkEnding
order by s.StatusDesc, s.OriginalName

select convert(varchar, count(distinct ServerID)) + ' assessment documents completed through ' + convert(varchar, @WkEnding, 101) + '.' 
from dbo.tbNote 
where CreateDt <= @WkEnding
	and Note = 'Assessment document completed.'

select 
	OriginalName 'Windows 2003'
	, DecommWOR 'Decommission WOR'
from dbo.tbServer
where DecommWOR is not null
order by OriginalName