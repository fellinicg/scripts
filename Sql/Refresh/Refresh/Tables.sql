select t.name, c.name, d.name, c.max_length
from sys.tables t
	inner join sys.columns c on t.object_id = c.object_id
	inner join sys.types d on c.system_type_id = d.system_type_id
where t.type = 'U'
	and c.is_identity = 0
order by t.name, c.name