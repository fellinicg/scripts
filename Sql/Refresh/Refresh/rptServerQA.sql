use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.rptServerQA') and type in (N'P', N'PC'))
drop procedure dbo.rptServerQA
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20151004
-- ============================================================
create procedure dbo.rptServerQA
	@ServerName varchar(100)
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @ServerID int
	declare @CreateDT datetime

	-- Get ServerID
	select @ServerID = s.ServerID, @CreateDT = qa.CreateDT
	from dbo.tbServer s
	inner join dbo.vwLatestQualityAssurance qa on s.ServerID = qa.ServerID
	where 
		s.OriginalName = @ServerName 
		or s.NewName = @ServerName

	-- Report Info
	select @CreateDT 'CreateDT'

	-- Computer Info
	select 
		ServerName
		, Domain
		, Manufacturer
		, Model
		, NumberOfProcessors
		, NumberOfLogicalProcessors
		, TotalPhysicalMemory
		, currenttimezone
		, EnableDaylightSavingsTime
		, DaylightInEffect
	from dbo.vwWmiComputerQA
	where ServerID = @ServerID

	-- Processor Info
	select
		ServerName
		, deviceid
		, socketdesignation
		, manufacturer
		, name
		, caption
		, numberofcores
		, numberoflogicalprocessors
		, maxclockspeed
		, status
		, availability
		, cpustatus
		, currentvoltage
		, family
		, l3cachesize
		, l3cachespeed
		, level
		, powermanagementsupported
		, processortype
		, revision
	from dbo.vwWmiProcessorsQA
	where ServerID = @ServerID
	order by deviceid

	-- Bios Info
	select
		ServerName
		, serialnumber
		, biosversion
		, description
		, manufacturer
		, SMBIOSBIOSVersion
	from dbo.vwWmiBiosQA
	where ServerID = @ServerID

	-- NetAdapter Info
	select
		ServerName
		, AddressFamily
		, IPAddress
		, ipv4address
		, ipv6address
		, DNSDomain
		, DNSDomainSuffixSearchOrder
		, DNSEnabledForWINSResolution
		, DNSServerSearchOrder
		, DomainDNSRegistrationEnabled
		, DefaultIPGateway
		, WINSEnableLMHostsLookup
		, ifAlias
		, ifDesc
		, MacAddress
		, ifName
		, AdminStatus
		, MediaConnectionState
		, fullduplex
		, speed
		, ActiveMaximumTransmissionUnit
		, PhysicalMediaType
		, devicewakeupenable
		, driverdate
		, DriverVersion
		, DriverFileName
		, NdisVersion
	from dbo.vwWmiNetAdaptersQA
	where ServerID = @ServerID
	order by ifIndex

	-- Drive Info
	select
		ServerName
		, DeviceID
		, Description
		, Size
		, FreeSpace
		, VolumeSerialNumber
	from dbo.vwWmiDisksQA
	where ServerID = @ServerID
	order by deviceid

	-- EventLog Info
	select
		ServerName
		, ApplicationSize
		, ApplicationRetention
		, SecuritySize
		, SecurityRetention
		, SystemSize
		, SystemRetention
		, SetupSize
		, SetupRetention
		, ForwardedEventsSize
		, ForwardedEventsRetention
	from dbo.vwWmiEventlogQA
	where ServerID = @ServerID

	-- Pagefile Info
	select
		ServerName
		, name
		, allocatedbasesize
	from dbo.vwWmiPageFileQA
	where ServerID = @ServerID

	-- OS Recovery Info
	select
		ServerName
		, autoreboot
		, debugfilepath
		, debuginfotype
		, kerneldumponly
		, minidumpdirectory
		, OverwriteExistingDebugFile
		, SendAdminAlert
		, WriteDebugInfo
		, WriteToSystemLog
	from dbo.vwWmiOSRecoveryQA
	where ServerID = @ServerID

	-- User Info
	select 
		ServerName
		, name 'UserName'
		, domain
		, status
		, disabled
		, passwordexpires
		, description
		, lockout
		, passwordchangeable
		, passwordrequired
	from dbo.vwWmiUsersQA
	where ServerID = @ServerID

	-- Services
	select
		ServerName
		, Service
		, State
	from dbo.vwWmiServicesQA
	where ServerID = @ServerID

	-- OS Updates
	select
		ServerName
		, Section 'KB'
		, InstalledOn
		, Description
	from dbo.vwOsUpdatesQA
	where ServerID = @ServerID

end
go
grant exec on dbo.rptServerQA to manager