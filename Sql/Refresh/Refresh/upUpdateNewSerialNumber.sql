use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateNewSerialNumber') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateNewSerialNumber
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150817
-- ============================================================
create procedure dbo.upUpdateNewSerialNumber
	@ServerID int
	,@NewSerialNumber varchar (15)
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @OldValue varchar(15)
	declare @Proc varchar(100)

	-- Proc
	set @Proc = object_name(@@procid)

	-- Old Value
	select @OldValue = NewSerialNumber from dbo.tbServer where ServerID = @ServerID

	-- Update data
	update dbo.tbServer
	set NewSerialNumber = @NewSerialNumber
	where ServerID = @ServerID

	-- Audit
	exec dbo.upAddAudit @Proc, 'tbServer', @ServerID, 'NewSerialNumber', @OldValue, @NewSerialNumber
	
end
go
grant exec on dbo.upUpdateNewSerialNumber to manager