use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateNewIP') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateNewIP
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150604
-- ============================================================
create procedure dbo.upUpdateNewIP
	@IpAddressID int
	,@NewIP varchar (15)
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @OldValue varchar(500)
	declare @Proc varchar(100)

	-- Proc
	set @Proc = object_name(@@procid)

	-- Old Value
	select @OldValue = NewIP from dbo.tbIpAddress where IpAddressID = @IpAddressID

	-- Update data
	update dbo.tbIpAddress
	set NewIP = @NewIP
	where IpAddressID = @IpAddressID

	-- Audit
	exec dbo.upAddAudit @Proc, 'tbIpAddress', @IpAddressID, 'NewIP', @OldValue, @NewIP

end
go
grant exec on dbo.upUpdateNewIP to manager