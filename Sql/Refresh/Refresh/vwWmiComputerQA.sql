use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwWmiComputerQA'))
drop view dbo.vwWmiComputerQA
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150915
-- ============================================================
create view dbo.vwWmiComputerQA
as
	select 
		coalesce(ServerID, '') 'ServerID'
		, max(coalesce(NewName, '')) 'ServerName'
		, max(coalesce(case when Field = 'DNSHostName' then Value end, '')) 'DNSHostName'
		, max(coalesce(case when Field = 'Domain' then Value end, '')) 'Domain'
		, max(coalesce(case when Field = 'PartOfDomain' then Value end, '')) 'PartOfDomain'
		, max(coalesce(case when Field = 'Manufacturer' then Value end, '')) 'Manufacturer'
		, max(coalesce(case when Field = 'Model' then Value end, '')) 'Model'
		, max(coalesce(case when Field = 'NumberOfLogicalProcessors' then Value end, '')) 'NumberOfLogicalProcessors'
		, max(coalesce(case when Field = 'NumberOfProcessors' then Value end, '')) 'NumberOfProcessors'
		, max(coalesce(case when Field = 'TotalPhysicalMemory' then Value end, '')) 'TotalPhysicalMemory'
		, max(coalesce(case when Field = 'currenttimezone' then Value end, '')) 'currenttimezone'
		, max(coalesce(case when Field = 'EnableDaylightSavingsTime' then Value end, '')) 'EnableDaylightSavingsTime'
		, max(coalesce(case when Field = 'DaylightInEffect' then Value end, '')) 'DaylightInEffect'
	from (
		select
			a.ServerID
			, a.NewName
			, b.Field
			, b.Value
		from dbo.vwLatestQualityAssurance a
		inner join dbo.tbFieldValue b on a.QualityAssuranceID = b.QualityAssuranceID
		where 
			b.Category = 'WMI'
			and b.Subcategory in ('Computer')
	) a
	group by ServerID
go


