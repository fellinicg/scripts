use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddNetworkAdapter') and type in (N'P', N'PC'))
drop procedure dbo.upAddNetworkAdapter
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150609
-- ============================================================
create procedure dbo.upAddNetworkAdapter
	@QualityAssuranceID int
	, @ActiveMaximumTransmissionUnit smallint
	, @AdminStatus varchar(5)
	, @ComponentID varchar(50)
	, @DeviceWakeUpEnable bit
	, @DriverDate smalldatetime
	, @DriverFileName varchar(50)
	, @DriverVersion varchar(20)
	, @FullDuplex bit
	, @ifAlias varchar(50)
	, @ifDesc varchar(50)
	, @ifIndex smallint
	, @ifName varchar(50)
	, @InstanceID varchar(50)
	, @MacAddress varchar(20)
	, @MediaConnectionState varchar(20)
	, @NdisVersion varchar(10)
	, @PhysicalMediaType varchar(10)
	, @Speed bigint
	, @Status varchar(10)
	, @Success bit output
as
begin

	-- Initialize settings
	set nocount on

	-- Add NetworkAdapter
	insert into dbo.tbNetworkAdapter (QualityAssuranceID, ActiveMaximumTransmissionUnit, AdminStatus, ComponentID, DeviceWakeUpEnable, DriverDate, DriverFileName, DriverVersion, FullDuplex, ifAlias, ifDesc, ifIndex, ifName, InstanceID, MacAddress, MediaConnectionState, NdisVersion, PhysicalMediaType, Speed, Status)
	values (@QualityAssuranceID, @ActiveMaximumTransmissionUnit, @AdminStatus, @ComponentID, @DeviceWakeUpEnable, @DriverDate, @DriverFileName, @DriverVersion, @FullDuplex, @ifAlias, @ifDesc, @ifIndex, @ifName, @InstanceID, @MacAddress, @MediaConnectionState, @NdisVersion, @PhysicalMediaType, @Speed, @Status)
	
	-- Check for success
	if (@@error = 0) and (@@rowcount > 0) set @Success = 1
	else set @Success = 0

end
go
grant exec on dbo.upAddNetworkAdapter to qa