use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddNote') and type in (N'P', N'PC'))
drop procedure dbo.upAddNote
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150603
-- ============================================================
create procedure dbo.upAddNote
	@ServerID int
	,@Note varchar (1000)
as
begin

	-- Initialize settings
	set nocount on

	-- Add note
	insert into dbo.tbNote (ServerID, Note, Author)
	values (@ServerID, @Note, system_user)

end
go
grant exec on dbo.upAddNote to manager