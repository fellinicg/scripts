use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.sfServerNotes') and type in (N'FN'))
drop function dbo.sfServerNotes
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150807
-- ============================================================
create function dbo.sfServerNotes (
	@ServerID int
	, @BeginDate datetime = '1/1/1900'
	, @EndDate datetime = '12/31/2099'
)
returns varchar(max) as

begin

declare @ret varchar(max);

select @ret = coalesce(@ret + char(10), '') + replace(Note, char(34), char(39))
from dbo.tbNote
where ServerID = @ServerID
	and CreateDt between @BeginDate and @EndDate
	and Hide = 0
order by CreateDt desc;

return (char(34) + @ret + char(34))
end

go
