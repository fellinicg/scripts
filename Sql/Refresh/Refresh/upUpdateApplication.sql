use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upUpdateApplication') and type in (N'P', N'PC'))
drop procedure dbo.upUpdateApplication
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150602
-- ============================================================
create procedure dbo.upUpdateApplication
	@ServerID int
	,@Application varchar (100)
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @OldValue varchar(100)
	declare @Proc varchar(100)

	-- Proc
	set @Proc = object_name(@@procid)

	-- Old Value
	select @OldValue = Application from dbo.tbServer where ServerID = @ServerID

	-- Update data
	update dbo.tbServer
	set Application = @Application
	where ServerID = @ServerID

	-- Audit
	exec dbo.upAddAudit @Proc, 'tbServer', @ServerID, 'Application', @OldValue, @Application
	
end
go
grant exec on dbo.upUpdateApplication to manager