use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddSnmp') and type in (N'P', N'PC'))
drop procedure dbo.upAddSnmp
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150609
-- ============================================================
create procedure dbo.upAddSnmp
	@QualityAssuranceID int
	, @Type varchar(30)
	, @Value varchar(100)
	, @Success bit output
as
begin

	-- Initialize settings
	set nocount on

	-- Add Snmp
	insert into dbo.tbSnmp (QualityAssuranceID, Type, Value)
	values (@QualityAssuranceID, @Type, @Value)
	
	-- Check for success
	if (@@error = 0) and (@@rowcount > 0) set @Success = 1
	else set @Success = 0

end
go
grant exec on dbo.upAddSnmp to qa