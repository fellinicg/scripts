select distinct a.NewName, b.Value
from dbo.vwLatestQualityAssurance a
inner join dbo.tbFieldValue b on a.QualityAssuranceID = b.QualityAssuranceID
where (b.Subcategory = 'Computer'
	and b.Field in ('DNSHostName'))
	and a.NewName <> b.Value