use Omni
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbAudit') and type in (N'U'))
drop table dbo.tbAudit
go

create table dbo.tbAudit(
	AuditID int identity(1,1) not null,
	SourceProcedure varchar(100) not null,
	TableName varchar(20) not null,
	PrimaryKey int null,
	ColumnName varchar(50) not null,
	OldValue varchar(500) null,
	NewValue varchar(500) null,
	UpdatedBy varchar(50) not null,
	CreateDT datetime not null constraint DF_tbAudit_CreateDT  default (getdate()),
 constraint PK_tbAudit primary key clustered 
(
	AuditID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off

