use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.sfStatus') and type in (N'FN'))
drop function dbo.sfStatus
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150701
-- ============================================================
create function dbo.sfStatus (
	@Status char(1)
)
returns varchar(10) as

begin

return (
	select 
		case @Status 
			when 'U' then 'Unbuilt' 
			when 'B' then 'Built' 
			when 'Q' then 'QA' + char(39) + 'd' 
			when 'R' then 'Racked' 
			when 'D' then 'Delivered'
			when 'E' then 'Excluded'
			else @Status 
		end
	)
end

go
