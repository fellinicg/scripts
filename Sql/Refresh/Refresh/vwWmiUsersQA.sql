use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwWmiUsersQA'))
drop view dbo.vwWmiUsersQA
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20151103
-- ============================================================
create view dbo.vwWmiUsersQA
as
	select 
		coalesce(ServerID, '') 'ServerID'
		, max(coalesce(NewName, '')) 'ServerName'
		, max(coalesce(case when Field = 'name' then Value end, '')) 'name'
		, max(coalesce(case when Field = 'caption' then Value end, '')) 'caption'
		, max(coalesce(case when Field = 'status' then Value end, '')) 'status'
		, max(coalesce(case when Field = 'disabled' then Value end, '')) 'disabled'
		, max(coalesce(case when Field = 'passwordexpires' then Value end, '')) 'passwordexpires'
		, max(coalesce(case when Field = 'accounttype' then Value end, '')) 'accounttype'
		, max(coalesce(case when Field = 'description' then Value end, '')) 'description'
		, max(coalesce(case when Field = 'domain' then Value end, '')) 'domain'
		, max(coalesce(case when Field = 'fullname' then Value end, '')) 'fullname'
		, max(coalesce(case when Field = 'lockout' then Value end, '')) 'lockout'
		, max(coalesce(case when Field = 'passwordchangeable' then Value end, '')) 'passwordchangeable'
		, max(coalesce(case when Field = 'passwordrequired' then Value end, '')) 'passwordrequired'
		, max(coalesce(case when Field = 'sid' then Value end, '')) 'sid'
		, max(coalesce(case when Field = 'sidtype' then Value end, '')) 'sidtype'
	from (
		select
			a.ServerID
			, a.NewName
			, b.Section
			, b.Field
			, b.Value
		from dbo.vwLatestQualityAssurance a
		inner join dbo.tbFieldValue b on a.QualityAssuranceID = b.QualityAssuranceID
		where 
			b.Category = 'WMI'
			and b.Subcategory in ('Users')
	) a
	group by ServerID, Section
go


