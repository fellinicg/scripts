use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwWmiEventlogQA'))
drop view dbo.vwWmiEventlogQA
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150915
-- ============================================================
create view dbo.vwWmiEventlogQA
as
	select
		ServerID
		, max(ServerName) 'ServerName'
		, max(case when Section = 'Application' then Size end) 'ApplicationSize'
		, max(case when Section = 'Application' then Retention end) 'ApplicationRetention'
		, max(case when Section = 'Security' then Size end) 'SecuritySize'
		, max(case when Section = 'Security' then Retention end) 'SecurityRetention'
		, max(case when Section = 'System' then Size end) 'SystemSize'
		, max(case when Section = 'System' then Retention end) 'SystemRetention'
		, max(case when Section = 'Setup' then Size end) 'SetupSize'
		, max(case when Section = 'Setup' then Retention end) 'SetupRetention'
		, max(case when Section = 'ForwardedEvents' then Size end) 'ForwardedEventsSize'
		, max(case when Section = 'ForwardedEvents' then Retention end) 'ForwardedEventsRetention'
	from (
		select 
			ServerID
			, max(NewName) 'ServerName'
			, Section
			, max(case when Field = 'Size' then Value end) 'Size'
			, max(case when Field = 'Retention' then Value end) 'Retention'
		from (
			select
				a.ServerID
				, a.NewName
				, b.Section
				, b.Field
				, b.Value
			from dbo.vwLatestQualityAssurance a
			inner join dbo.tbFieldValue b on a.QualityAssuranceID = b.QualityAssuranceID
			where 
				b.Category = 'WMI'
				and b.Subcategory in ('EventLog')
		) a
		group by ServerID, Section
	) b
	group by ServerID
go


