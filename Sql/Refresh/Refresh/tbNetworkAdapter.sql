use Refresh
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbNetworkAdapter') and type in (N'U'))
drop table dbo.tbNetworkAdapter
go

create table dbo.tbNetworkAdapter(
	NetworkAdapterID int identity(1,1) not null,
	QualityAssuranceID int not null,
	ActiveMaximumTransmissionUnit smallint not null,
	AdminStatus varchar(5) not null,
	ComponentID varchar(50) not null,
	DeviceWakeUpEnable bit not null,
	DriverDate smalldatetime not null,
	DriverFileName varchar(50) not null,
	DriverVersion varchar(20) not null,
	FullDuplex bit not null,
	ifAlias varchar(50) not null,
	ifDesc varchar(50) not null,
	ifIndex smallint not null,
	ifName varchar(50) not null,
	InstanceID varchar(50) not null,
	MacAddress varchar(20) not null,
	MediaConnectionState varchar(20) not null,
	NdisVersion varchar(10) not null,
	PhysicalMediaType varchar(10) not null,
	Speed bigint not null,
	Status varchar(10) not null,
 constraint PK_tbNetworkAdapter primary key clustered 
(
	NetworkAdapterID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off