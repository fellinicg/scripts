use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwWmiDisksQA'))
drop view dbo.vwWmiDisksQA
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150915
-- ============================================================
create view dbo.vwWmiDisksQA
as
	select 
		ServerID
		, max(NewName) 'ServerName'
		, Section
		, max(case when Field = 'DeviceID' then Value end) 'DeviceID'
		, max(case when Field = 'Description' then Value end) 'Description'
		, max(case when Field = 'Size' then Value end) 'Size'
		, max(case when Field = 'FreeSpace' then Value end) 'FreeSpace'
		, max(case when Field = 'VolumeSerialNumber' then Value end) 'VolumeSerialNumber'
	from (
		select
			a.ServerID
			, a.NewName
			, b.Section
			, b.Field
			, b.Value
		from dbo.vwLatestQualityAssurance a
		inner join dbo.tbFieldValue b on a.QualityAssuranceID = b.QualityAssuranceID
		where 
			b.Category = 'WMI'
			and b.Subcategory in ('LogDisks')
	) a
	group by ServerID, Section
go


