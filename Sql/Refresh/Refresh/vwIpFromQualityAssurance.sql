use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwIpFromQualityAssurance'))
drop view dbo.vwIpFromQualityAssurance
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150911
-- ============================================================
create view dbo.vwIpFromQualityAssurance
as
	select 
		a.QualityAssuranceID
		, a.ServerID
		, a.NewName 'ServerName'
		, b.Value 'IPAddress'
	from dbo.vwLatestQualityAssurance a
	left join dbo.tbFieldValue b on a.QualityAssuranceID = b.QualityAssuranceID
	where 
		b.Subcategory in ('NetAdapters')
		and b.Field in ('ipv4address')
go


