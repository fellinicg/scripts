use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetServersRO') and type in (N'P', N'PC'))
drop procedure dbo.upGetServersRO
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150701
-- ============================================================
create procedure dbo.upGetServersRO
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		s.OriginalName
		, s.NewName
		, case s.IsVirtual when 1 then 'Virtual' when 0 then 'Physical' end 'Type'
		, s.Location
		, s.Application
		, s.ApplicationOwner
		, dbo.sfStatus(s.Status) 'Status'
	from dbo.vw2003Servers s
	order by OriginalName

end
go
grant exec on dbo.upGetServersRO to public