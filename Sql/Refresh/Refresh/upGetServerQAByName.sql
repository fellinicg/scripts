use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetServerQAByName') and type in (N'P', N'PC'))
drop procedure dbo.upGetServerQAByName
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150724
-- ============================================================
create procedure dbo.upGetServerQAByName
	@ServerName varchar(100)
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select 
		qa.QualityAssuranceID
		, qa.CreateDT
		, fv.Category
		, isnull(fv.Subcategory, '') 'Subcategory'
		, isnull(fv.Section, '') 'Section'
		, isnull(fv.Field, '') 'Field'
		, isnull(fv.Value, '') 'Value'
		, isnull(fv.Subvalue, '') 'Subvalue'
	from dbo.tbServer s
		inner join dbo.vwLatestQualityAssurance qa on s.ServerID = qa.ServerID
		inner join dbo.tbFieldValue fv on qa.QualityAssuranceID = fv.QualityAssuranceID
	where 
		s.OriginalName = @ServerName 
		or s.NewName = @ServerName
	order by fv.QualityAssuranceID, fv.Category, fv.Subcategory, fv.Section, fv.Field

end
go
grant exec on dbo.upGetServerQAByName to manager