use Refresh
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetServerIPs') and type in (N'P', N'PC'))
drop procedure dbo.upGetServerIPs
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20150602
-- ============================================================
create procedure dbo.upGetServerIPs
as
begin

	-- Initialize settings
	set nocount on

	-- Return data
	select
		i.IpAddressID
		, s.OriginalName
		, i.OriginalIP
		, i.NewIP
		, i.Adapter
		, s.NewName
		, s.Status
	from dbo.vw2003Servers s
		left join dbo.tbIpAddress i on s.ServerID = i.ServerID
	order by OriginalName

end
go
grant exec on dbo.upGetServerIPs to manager, reviewer