use NetOps
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbJob') and type in (N'U'))
drop table dbo.tbJob
go

create table dbo.tbJob(
	JobID int identity(1,1) not null,
	JobName varchar(20) not null, 
	JobDT datetime not null,
 constraint PK_tbJob primary key clustered 
(
	JobID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off