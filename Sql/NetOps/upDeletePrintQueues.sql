use NetOps
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upDeletePrintQueues') and type in (N'P', N'PC'))
drop procedure dbo.upDeletePrintQueues
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20171025
-- ============================================================
create procedure dbo.upDeletePrintQueues
as
begin

	-- Initialize settings
	set nocount on

	-- Set IsDeleted flag 
	update p
	set
		p.IsDeleted = 1
		, p.UpdateDT = getdate()
	from 
		dbo.tbPrintQueue p
	left join
		openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [PrintQueue-Information.csv]') a on a.Name = p.PrintQueue and a.PSComputerName = p.PrintServer
	where
		a.Name is null

	-- Output for log
	print convert(varchar, @@rowcount) + ' deleted.'

end