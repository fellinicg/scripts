use NetOps
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddDhcpScopes') and type in (N'P', N'PC'))
drop procedure dbo.upAddDhcpScopes
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20171011
-- ============================================================
create procedure dbo.upAddDhcpScopes
as
begin

	-- Initialize settings
	set nocount on

	-- Add data
	insert into dbo.tbDhcpScope (
		ObjectOID
		, ScopeName
		, StartAddress
		, EndAddress
		, PolicyID)
	select
		a.objectOid
		, a.name
		, replace(a.Start, ' ', ',')
		, replace(a.[End], ' ', ',')
		, p.PolicyID
	from 
		openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [Registrar-Scopes.csv]') a
	left join 
		dbo.tbDhcpScope s on a.objectOid = s.ObjectOID
	left join 
		dbo.tbDhcpPolicy p on a.policy = p.PolicyName
	where 
		s.ScopeID is null

	-- Output for log
	print convert(varchar, @@rowcount) + ' added.'

end
