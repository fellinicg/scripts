use NetOps
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddDhcpPolicies') and type in (N'P', N'PC'))
drop procedure dbo.upAddDhcpPolicies
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20171011
-- ============================================================
create procedure dbo.upAddDhcpPolicies
as
begin

	-- Initialize settings
	set nocount on

	-- Add data
	insert into dbo.tbDhcpPolicy (
		ObjectOID
		, PolicyName
		, LeaseTime)
	select
		a.objectOid
		, a.name
		, a.LeaseTime
	from 
		openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [Registrar-Policies.csv]') a
	left join 
		dbo.tbDhcpPolicy p on a.objectOid = p.ObjectOID
	where 
		p.PolicyID is null

	-- Output for log
	print convert(varchar, @@rowcount) + ' added.'

end
