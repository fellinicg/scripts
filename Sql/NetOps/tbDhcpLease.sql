use NetOps
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbDhcpLease') and type in (N'U'))
drop table dbo.tbDhcpLease
go

create table dbo.tbDhcpLease(
	JobID int not null,
	IpAddress varchar(15) not null,
	ClientHostName varchar(100) null,
	ClientMacAddress varchar(50) null,
	ClientLastTransactionDT datetime null,
	Flags varchar(100) null,
	LeaseState varchar(20) null,
	IsReserved bit not null,
	ScopeID int null
 constraint PK_tbDhcpLease primary key clustered 
(
	JobID, IpAddress asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off