use NetOps
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddJob') and type in (N'P', N'PC'))
drop procedure dbo.upAddJob
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20171011
-- ============================================================
create procedure dbo.upAddJob
	@JobName varchar(20)
	, @JobDT datetime
	, @JobID int output
as
begin

	-- Initialize settings
	set nocount on

	-- Retrieve JobID, if it exists
	select @JobID = JobID from tbjob where JobName = @JobName and JobDt = @JobDT

	-- Insert data if it does not exist
	if (@JobID is null) 
	begin
		insert into dbo.tbJob (
			JobName
			, JobDT)
		values (
			@JobName
			, @JobDT)

		-- Verify success
		if (@@rowcount = 1)
			set @JobID = @@identity
	end
end