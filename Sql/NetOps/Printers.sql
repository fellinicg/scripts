select
	p.PrintQueueID
	, p.PrintServer
	, p.PrintQueue
	, p.IPAddress
	, a.DisplayName
	, a.DeviceStatusSeverity
	, isnull(p.SnmpChanged, '') SnmpChanged
	, isnull(p.PasswordSet, '') PasswordSet
from
	dbo.tbPrintQueue p
inner join 	 
	openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [JetadminDevices.csv]') a on p.IPAddress = a.IPv4Address
order by PrintServer, DisplayName