use NetOps
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddDhcpLeases') and type in (N'P', N'PC'))
drop procedure dbo.upAddDhcpLeases
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20171011
-- ============================================================
create procedure dbo.upAddDhcpLeases
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @JobDT datetime
	declare @JobID int

	-- Retrieve job timestamp
	select @JobDT = FeedDt from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select top 1 FeedDT from [Registrar-Leases.csv]') a

	-- Add job
	exec dbo.upAddJob 'DhcpLeases', @JobDT, @JobID output

	-- Add data
	insert into dbo.tbDhcpLease (
		JobID
		, IpAddress
		, ClientHostName
		, ClientMacAddress
		, ClientLastTransactionDT
		, Flags
		, LeaseState
		, IsReserved
		, ScopeID)
	select 
		@JobID
		, a.address
		, a.clientHostName
		, a.clientMacAddr
		, case when a.clientLastTransactionTime = 'none' then null else convert(datetime, substring(a.clientLastTransactionTime, 5, 20)) end
		, a.flags
		, a.state
		, case when a.reservationLookupKeyType <> null then 1 else 0 end
		, s.ScopeID
	from 
		openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [Registrar-Leases.csv]') a
	left join 
		dbo.tbDhcpLease l on l.JobID = @JobID and a.address = l.IpAddress
	left join
		dbo.tbDhcpScope s on a.scopeName = s.ScopeName
	where
		l.IpAddress is null

	-- Output for log
	print convert(varchar, @@rowcount) + ' added.'

end
