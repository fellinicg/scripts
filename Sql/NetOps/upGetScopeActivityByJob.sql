use NetOps
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetScopeActivityByJob') and type in (N'P', N'PC'))
drop procedure dbo.upGetScopeActivityByJob
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20171023
-- ============================================================
create procedure dbo.upGetScopeActivityByJob
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	declare @sql varchar(max) = 'select * from (select ''I'' Idx, JobID, ScopeName, StartAddress, EndAddress from dbo.vwScopeUtilization where ScopeState = ''inactive'') a pivot (max(Idx) for JobID in ($JOBIDS$)) as p order by ScopeName'
	declare @jobids varchar(max)

	-- Convert JobID list to csv
	select @jobids = stuff((select (',' + ('[' + convert(varchar, JobID) + ']')) from dbo.vwInactiveScopeCount order by JobID for xml path('')),1,1,'')

	-- Replace token with JobID csv list
	set @sql = replace(@sql, '$JOBIDS$', @jobids)

	-- Execute SQL and return data
	exec(@sql)

end