use NetOps
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwInactiveScopeCount'))
drop view dbo.vwInactiveScopeCount
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20171023
-- ============================================================
create view dbo.vwInactiveScopeCount
as
	select
		s.JobID
		, convert(varchar, max(JobDT), 101) JobDT
		, count(1) TotalInactiveScopes
	from 
		dbo.vwScopeUtilization s
	inner join 
		dbo.tbJob j on s.JobID = j.JobID
	where 
		s.ScopeState = 'Inactive'
	group by 
		s.JobID
go