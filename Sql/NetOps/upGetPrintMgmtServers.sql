use NetOps
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upGetPrintMgmtServers') and type in (N'P', N'PC'))
drop procedure dbo.upGetPrintMgmtServers
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20171025
-- ============================================================
create procedure dbo.upGetPrintMgmtServers
as
begin

	-- Initialize settings
	set nocount on

	-- Declarations
	select
		c.ID
		, c.Computer_Name
	from 
		AN7.dbo.Object_Relations r
	inner join
		AN7.dbo.Computers c on r.Left_Object_ID = c.id
	where 
		r.Right_Object_ID = '23EB72D3-09FA-4580-BAA9-AFA0AE286C36'	--MS Print Management
		and
		r.Type_ID = '6FCF5FE4-0DAC-41AE-93B7-3D0388ADE8CA'	--Consist of
		and
		c.Status_ID <> '{3417FB44-7DF7-4C9F-9A25-AAB4CC4C43E6}'

end