use NetOps
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwScopeUtilization'))
drop view dbo.vwScopeUtilization
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20171011
-- ============================================================
create view dbo.vwScopeUtilization
as
	select 
		l.JobID
		, s.ScopeName
		, s.StartAddress
		, s.EndAddress
		, count(1) as LeaseCount
		, sum(case when charindex('reserved', l.Flags) = 0 then 1 else 0 end) DynamicCount
		, sum(case when charindex('reserved', l.Flags) > 0 then 1 else 0 end) ReservedCount
		, sum(case when charindex('available', l.LeaseState) > 0 then 1 else 0 end) AvailableCount
		, sum(case when l.LeaseState = 'available' and charindex('reserved', l.Flags) = 0 then 1 else 0 end) DynamicAvailable
		, sum(case when l.LeaseState = 'other-available' then 1 else 0 end) [DynamicOther-Available]
		, sum(case when charindex('reserved', flags) > 0 and l.LeaseState = 'leased' then 1 else 0 end) ReservedActive
		, sum(case when charindex('reserved', flags) > 0 and l.LeaseState = 'available' then 1 else 0 end) ReservedInactive
		, sum(case when l.LeaseState = 'leased' then 1 else 0 end) Leased
		, sum(case when l.LeaseState = 'unavailable' then 1 else 0 end) Unavailable
		, case when count(1) = sum(case when charindex('available', l.LeaseState) > 0 then 1 else 0 end) then 'Inactive' else 'Active' end ScopeState
	from 
		dbo.tbDhcpLease l
	inner join 
		dbo.tbDhcpScope s on l.ScopeID = s.ScopeID
	group by 
		l.JobID, s.ScopeName, s.StartAddress, s.EndAddress
go