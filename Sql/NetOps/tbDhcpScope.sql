use NetOps
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbDhcpScope') and type in (N'U'))
drop table dbo.tbDhcpScope
go

create table dbo.tbDhcpScope(
	ScopeID int identity(1,1) not null,
	ScopeName varchar(100) not null,
	StartAddress varchar(100) null,
	EndAddress varchar(100) null,
	PolicyID int null,
	ObjectOID varchar(50) not null
 constraint PK_tbDhcpScope primary key clustered 
(
	ScopeID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off