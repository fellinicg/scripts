use NetOps
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.upAddPrintQueues') and type in (N'P', N'PC'))
drop procedure dbo.upAddPrintQueues
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20171025
-- ============================================================
create procedure dbo.upAddPrintQueues
as
begin

	-- Initialize settings
	set nocount on

	-- Insert data 
	insert into dbo.tbPrintQueue (
		PrintQueue
		, IPAddress
		, PrintServer
		, PrintServerAlloyID
	)
	select
		a.Name
		, a.IpAddress
		, a.PSComputerName
		, a.AlloyID
	from 
		openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [PrintQueue-Information.csv]') a
	left join
		dbo.tbPrintQueue p on a.Name = p.PrintQueue and a.PSComputerName = p.PrintServer
	where
		p.PrintQueueID is null

	-- Output for log
	print convert(varchar, @@rowcount) + ' added.'

	-- Undelete printers
	update p
	set
		p.IsDeleted = 0
		, p.UpdateDT = getdate()
	from 
		dbo.tbPrintQueue p
	inner join
		openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [PrintQueue-Information.csv]') a on a.Name = p.PrintQueue and a.PSComputerName = p.PrintServer
	where
		p.IsDeleted = 1

	-- Output for log
	print convert(varchar, @@rowcount) + ' undeleted.'

end