use NetOps
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbPrintQueue') and type in (N'U'))
drop table dbo.tbPrintQueue
go

create table dbo.tbPrintQueue(
	PrintQueueID int identity(1,1) not null,
	PrintQueue varchar(200) not null,
	IPAddress varchar(30) null,
	PrintServer varchar(50) null,
	PrintServerAlloyID uniqueidentifier null,
	IsDeleted bit default 0,
	SnmpChanged char(1) null,
	PasswordSet char(1) null,
	CreateDT datetime default getdate(),
	UpdateDT datetime default getdate(),
 constraint PK_tbPrintQueue primary key clustered 
(
	PrintQueueID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off