use NetOps
go

set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.tbDhcpPolicy') and type in (N'U'))
drop table dbo.tbDhcpPolicy
go

create table dbo.tbDhcpPolicy(
	PolicyID int identity(1,1) not null,
	PolicyName varchar(100) not null,
	LeaseTime varchar(10) null,
	ObjectOID varchar(50) not null
 constraint PK_tbDhcpPolicy primary key clustered 
(
	PolicyID asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [PRIMARY]
) on [PRIMARY]

go

set ansi_padding off