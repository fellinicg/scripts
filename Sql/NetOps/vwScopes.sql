use NetOps
go
set ansi_nulls on
go
set quoted_identifier off
go
if  exists (select * from sys.views where object_id = OBJECT_ID(N'dbo.vwScopes'))
drop view dbo.vwScopes
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20171011
-- ============================================================
create view dbo.vwScopes
as
	select 
		s.ScopeID
		, s.ScopeName
		, p.PolicyName
		, p.LeaseTime
	from 
		dbo.tbDhcpScope s
	left join
		dbo.tbDhcpPolicy p on s.PolicyID = p.PolicyID
go