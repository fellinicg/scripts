declare @DataFile varchar(40) = '20150519-Qualys.xlsx'
declare @ScanDate smalldatetime = left(@DataFile, charindex('-', @DataFile) - 1)
declare @QualysScanID int

-- Drop ##temp
if object_id('tempdb..##temp') is not null
    drop table ##temp

-- Store data in ##temp
exec ('select * into ##temp from openrowset(''Microsoft.ACE.OLEDB.12.0'',''Excel 12.0;Database=C:\Feeds\' + @DataFile + ''',''select * from [Sheet1$]'')')

-- Add Qualys Scan
--
insert into dbo.tbQualysScan(ScanDate, DataFile)
select @ScanDate, @DataFile

-- Get new ID
--
set @QualysScanID = @@identity

-- Hosts
--
insert into dbo.tbQualysScanHost (QualysScanID, IpAddress, Application, DnsName, OS)
select distinct @QualysScanID, a.IP, a.DNS, a.Application, a.OS
from ##temp a
left join dbo.tbQualysScanHost b on a.IP = b.IpAddress and b.QualysScanID = @QualysScanID
where b.IpAddress is null

-- Results
--
insert into dbo.tbQualysScanResult (QualysScanID, IpAddress, QID, Port)
select distinct @QualysScanID, IP, QID, Port
from ##temp a



-------------------------------------------------------------------------------------

-- select 
--	src.QualysScanID
--	, src.IpAddress
--	, src.QID
--	, src.Port
--	, src.[Action Needed]
--*
update dbo.tbQualysScanResult set Actions = src.[Action Needed]
from openrowset('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=C:\Feeds\Qualys.xlsx','select * from [Windows Details$]') src
inner join dbo.tbQualysScanResult dest on src.QualysScanID = dest.QualysScanID and 
											src.IpAddress = dest.IpAddress and 
											src.QID = dest.QID and 
											isnull(src.Port, '0') = isnull(dest.Port, 0)

--select 
--	src.QualysScanID
--	, src.IpAddress
--	, src.QID
--	, src.Port
--	, src.[Pomeroy Response:]
--	, src.[Action:]
update dbo.tbQualysScanResult set Notes = src.[Pomeroy Response:], Actions = src.[Action:]
from openrowset('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=C:\Feeds\Qualys.xlsx','select * from [Unix Details$]') src
inner join dbo.tbQualysScanResult dest on src.QualysScanID = dest.QualysScanID and 
											src.IpAddress = dest.IpAddress and 
											src.QID = dest.QID and 
											src.Port = dest.Port