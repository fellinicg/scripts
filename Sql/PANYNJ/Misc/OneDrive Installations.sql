select
	c.Computer_Name
	, c.Serial_Num
	, c.Owner
	, p.Product_Name
	, p.Version
from AN7.dbo.Soft_Products p
inner join AN7.dbo.Detected_Software_Products d on p.ID = d.Soft_Product_ID
inner join AN7.dbo.Computer_List c on d.Object_ID = c.ID
where p.Product_Name like 'Microsoft OneDrive%'
order by c.Computer_Name