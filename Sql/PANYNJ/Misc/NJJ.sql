-- Virtual Servers
--
select distinct [Computer Name]
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Feeds\Windows2003.xlsx', 
	'select * from [DataFromAlloyNavigator$]')
where charindex('vmware', Manufacturer) > 0

-- Physical Servers
--
select distinct [Computer Name]
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Feeds\Windows2003.xlsx', 
	'select * from [DataFromAlloyNavigator$]')
where charindex('vmware', Manufacturer) = 0

-- Nate
select *
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Feeds\Windows2003.xlsx', 
	'select * from [NJJ-Physical$]')

-- Difference
select *
from 
	(
		select distinct [Computer Name] TMF
		from openrowset('Microsoft.ACE.OLEDB.12.0',
			'Excel 12.0;Database=C:\Feeds\Windows2003.xlsx', 
			'select * from [DataFromAlloyNavigator$]')
		where charindex('vmware', Manufacturer) = 0
	 ) a
left join 
	(
		select distinct [Computer Name] NJJ
		from openrowset('Microsoft.ACE.OLEDB.12.0',
			'Excel 12.0;Database=C:\Feeds\Windows2003.xlsx', 
			'select * from [NJJ-Physical$]')
	) b on a.TMF = b.NJJ
where NJJ is null