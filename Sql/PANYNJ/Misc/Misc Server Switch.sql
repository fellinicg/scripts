use omni

select Description, count(Description)
from dbo.vwServer
--where Description is not null
where left(Description, 4) = 'PMRY'
group by Description

use omni

select Description, count(Description)
from dbo.vwServer s
inner join (
	select isnull([MAC Address], MAC) 'MACAdress', isnull([IP Address], [Host IP]) 'IPAddress', isnull([Switch], [Host Switch IP]) 'SwitchIP', isnull([Port], [Host Switch Port]) 'SwitchPort'
	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20140602-endhosts.xlsx', 'select * from [Sheet1$] where [MAC Address] is not null and [IP Address] is not null') a
	full outer join openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20140602-riverbedexport.xlsx', 'select * from [Sheet1$] where MAC is not null and [Host Switch IP] is not null and [Host IP] is not null') b on a.[MAC Address] = replace(b.Mac, ':', '-')
	) sw on s.PrimaryIP = sw.IPAddress
where left(s.Description, 4) = 'PMRY'
group by Description

select distinct s.Name, s.Description, s.PrimaryIP
from dbo.vwServer s
left join (
	select isnull([MAC Address], MAC) 'MACAdress', isnull([IP Address], [Host IP]) 'IPAddress', isnull([Switch], [Host Switch IP]) 'SwitchIP', isnull([Port], [Host Switch Port]) 'SwitchPort'
	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20140602-endhosts.xlsx', 'select * from [Sheet1$] where [MAC Address] is not null and [IP Address] is not null') a
	full outer join openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20140602-riverbedexport.xlsx', 'select * from [Sheet1$] where MAC is not null and [Host Switch IP] is not null and [Host IP] is not null') b on a.[MAC Address] = replace(b.Mac, ':', '-')
	) sw on s.PrimaryIP = sw.IPAddress
where left(s.Description, 4) = 'PMRY'
and sw.IPAddress is null
order by s.Description, s.Name

======================

select count(distinct s.Name)
from dbo.vwServer s
where left(s.Description, 4) = 'PMRY'

select count(distinct s.Name)--, s.Description--, sw.*
from dbo.vwServer s
left join (
	select isnull([MAC Address], MAC) 'MACAdress', isnull([IP Address], [Host IP]) 'IPAddress', isnull([Switch], [Host Switch IP]) 'SwitchIP', isnull([Port], [Host Switch Port]) 'SwitchPort'
	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20140602-endhosts.xlsx', 'select * from [Sheet1$] where [MAC Address] is not null and [IP Address] is not null') a
	full outer join openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20140602-riverbedexport.xlsx', 'select * from [Sheet1$] where MAC is not null and [Host Switch IP] is not null and [Host IP] is not null') b on a.[MAC Address] = replace(b.Mac, ':', '-')
	) sw on s.PrimaryIP = sw.IPAddress
where left(s.Description, 4) = 'PMRY'
and sw.IPAddress is null
--group by s.Name, s.Description
--having count(1) > 1
--order by s.Description, s.Name

select count(distinct s.Name)--, s.Description, sw.*
from dbo.vwServer s
inner join (
	select isnull([MAC Address], MAC) 'MACAdress', isnull([IP Address], [Host IP]) 'IPAddress', isnull([Switch], [Host Switch IP]) 'SwitchIP', isnull([Port], [Host Switch Port]) 'SwitchPort'
	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20140602-endhosts.xlsx', 'select * from [Sheet1$] where [MAC Address] is not null and [IP Address] is not null') a
	full outer join openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20140602-riverbedexport.xlsx', 'select * from [Sheet1$] where MAC is not null and [Host Switch IP] is not null and [Host IP] is not null') b on a.[MAC Address] = replace(b.Mac, ':', '-')
	) sw on s.PrimaryIP = sw.IPAddress
where left(s.Description, 4) = 'PMRY'