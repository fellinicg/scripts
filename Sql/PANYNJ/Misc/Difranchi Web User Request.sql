--select *
--from  (
select distinct a.UserName, b.FirstName, b.LastName, b.Office, b.PeoplesoftID, b.PeoplesoftStatus, case b.IsEnabled when 1 then 'Yes' when 0 then 'No' else 'Unknown' end 'EnabledInAD'
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Feeds\webusers.xlsx', 
	'select * from [Sheet2$]') a
left join (select * from dbo.tbADUser where charindex('@', Email) > 0) b on rtrim(ltrim(a.UserName)) = isnull(left(isnull(b.Email, ''), isnull(charindex('@', b.Email), 1)-1), '')
where b.PeoplesoftID is not null
union
select distinct a.UserName, b.FirstName, b.LastName, b.Office, b.PeoplesoftID, b.PeoplesoftStatus, case b.IsEnabled when 1 then 'Yes' when 0 then 'No' else 'Unknown' end 'EnabledInAD'
from (select [Operator ID] UserName
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Feeds\webusers.xlsx', 
	'select * from [Sheet3$]') a
where charindex('NO ADDITIONAL INFO FOUND', [Employee ID]) > 0) a
left join (select * from dbo.tbADUser where charindex('@', Email) > 0) b on rtrim(ltrim(replace(a.UserName, ' c', ''))) = isnull(left(isnull(b.Email, ''), isnull(charindex('@', b.Email), 1)-1), '')
where b.PeoplesoftID is not null
union
select distinct a.UserName, a.FirstName, a.LastName, a.Office, p.EMPLID, p.Status, case a.IsEnabled when 1 then 'Yes' when 0 then 'No' else 'Unknown' end 'EnabledInAD'
from (
		select a.UserName, b.FirstName, b.LastName, b.Office, b.IsEnabled
		from (select [Operator ID] UserName
		from openrowset('Microsoft.ACE.OLEDB.12.0',
			'Excel 12.0;Database=C:\Feeds\webusers.xlsx', 
			'select * from [Sheet3$]') a
		) a
		left join (select * from dbo.tbADUser where charindex('@', Email) > 0) b on rtrim(ltrim(replace(a.UserName, ' c', ''))) = isnull(left(isnull(b.Email, ''), isnull(charindex('@', b.Email), 1)-1), '')
		where PeopleSoftID is null) a
left join openrowset('Microsoft.ACE.OLEDB.12.0', 
		'Text;Database=C:\Feeds', 
		'select * from [AD_LISTING2.TXT]') p on a.FirstName = p.[First Name] and a.LastName = p.[Last Name]
where p.EMPLID is not null
union
select distinct UserName, FirstName, LastName, Office, PeoplesoftID, PeoplesoftStatus, case IsEnabled when 1 then 'Yes' when 0 then 'No' else 'Unknown' end 'EnabledInAD'
from ##temp
where PeoplesoftID is not null
union
select distinct UserName, FirstName, LastName, Office, PeoplesoftID, PeoplesoftStatus, case IsEnabled when 1 then 'Yes' when 0 then 'No' else 'Unknown' end 'EnabledInAD'
from ##temp
where PeoplesoftID is null
order by Username
--) a
--where UserName in (
--'LMARTINEZ'
--,'RWILLIAM'
--,'MPALERMO'
--)
--group by Username
--having count(1) > 1

--select right(a.UserName, len(rtrim(ltrim(a.UserName)))), a.UserName, len(a.UserName), b.FirstName, b.LastName, b.Office
--from openrowset('Microsoft.ACE.OLEDB.12.0',
--	'Excel 12.0;Database=C:\Feeds\webusers.xlsx', 
--	'select * from [Sheet1$]') a
--left join dbo.tbADUser b on rtrim(ltrim(a.UserName)) = b.UserName
--where FirstName is null

--select a.UserName, b.FirstName, b.LastName, b.Office
--from openrowset('Microsoft.ACE.OLEDB.12.0',
--	'Excel 12.0;Database=C:\Feeds\webusers.xlsx', 
--	'select * from [Sheet1$]') a
--left join dbo.tbADUser b on a.UserName = b.UserName
--where FirstName is not null