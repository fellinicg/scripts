-- Jason
select a.*, isnull(b.Machine, '') Machine
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Feeds\TC-4507-GIG  Connections-v3.xlsx', 
	'select * from [TC-4507-GIG1 Connections$]') a
left join (
	select Machine, MACAddress from dbo.vwAlloyMAC
	union 
	select Machine, MACAddress from dbo.vwSccmMAC
	union 
	select Machine, MACAddress from dbo.vwEpoMAC
	union 
	select Machine, MACAddress from dbo.vwVirtualMachineMAC) b on replace(a.[MAC Address], ':', '') = b.MACAddress

--Solarwinds
select
	a.IPAddress
	, b.*
from (select * from [teledb48].[SolarWindsOrion].[dbo].[DiscoveredNodes] where ProfileID = 117 and SnmpVersion = 0) a
left join (select * from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20170508-Devices.xlsx', 'select * from [Sheet1$]')) b on a.IPAddress = b.[IP Address]

select distinct deviceName
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20170324-NetworkDevices.xlsx', 'select * from [Sheet1$]') a
where deviceName is not null

select *
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20170324-NetworkDevices.xlsx', 'select * from [Sheet1$]') a
where a.deviceName in (
	select distinct deviceName
	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20170324-NetworkDevices.xlsx', 'select * from [Sheet1$]') a
	where deviceName is not null
	group by a.deviceName
	having count(1) > 1)
order by a.DeviceName, a.ipAddress

select 
	a.deviceName
	--, b.Caption
	--, a.ipAddress
	--, b.IP_Address
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20170324-NetworkDevices.xlsx', 'select * from [Sheet1$]') a
left join TELEDB48.SolarWindsOrion.dbo.Nodes b on a.deviceName = b.Caption
where a.deviceName is not null
group by a.deviceName
having count(1) > 1

select 
	count(1)
	,count (distinct caption)
from TELEDB48.SolarWindsOrion.dbo.Nodes
where Pomeroy_Support = 'Networking'

select 
	caption
from TELEDB48.SolarWindsOrion.dbo.Nodes
where Pomeroy_Support = 'Networking'
group by caption
having count(1) > 1

select * from TELEDB48.SolarWindsOrion.dbo.Nodes
where caption = 'JFK111-3850-1'

select 
	*
from TELEDB48.SolarWindsOrion.dbo.Nodes
where Pomeroy_Support = 'Networking'
and caption is null


select 
	a.deviceName
	, b.Caption
	, a.ipAddress
	, b.IP_Address
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20170324-NetworkDevices.xlsx', 'select * from [Sheet1$]') a
inner join TELEDB48.SolarWindsOrion.dbo.Nodes b on a.ipAddress = b.IP_Address

select
	a.deviceName
	, b.Caption
	, a.ipAddress
	, b.IP_Address
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20170324-NetworkDevices.xlsx', 'select * from [Sheet1$]') a
inner join TELEDB48.SolarWindsOrion.dbo.Nodes b on a.deviceName = b.Caption

select
	a.deviceName
	, b.Caption
	, a.ipAddress
	, b.IP_Address
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20170324-NetworkDevices.xlsx', 'select * from [Sheet1$]') a
inner join TELEDB48.SolarWindsOrion.dbo.Nodes b on a.deviceName = b.Caption and a.ipAddress = b.IP_Address

select 
	a.deviceName
	, b.Caption
	, a.ipAddress
	, b.IP_Address
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20170324-NetworkDevices.xlsx', 'select * from [Sheet1$]') a
inner join TELEDB48.SolarWindsOrion.dbo.Nodes b on a.ipAddress = b.IP_Address and isnull(a.deviceName, b.Caption) <> b.Caption

select
	a.deviceName
	, b.Caption
	, a.ipAddress
	, b.IP_Address
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20170324-NetworkDevices.xlsx', 'select * from [Sheet1$]') a
inner join TELEDB48.SolarWindsOrion.dbo.Nodes b on a.deviceName = b.Caption and a.ipAddress <> b.IP_Address

------------------
--insert into openrowset ('Microsoft.ACE.OLEDB.12.0', 
--	'Excel 12.0;Database=c:\feeds\test.xlsx;',
--	'select * from [Sheet1$]')
--Select * from [dbo].[tbAdminReport]

--Citrix
select distinct APPNAME
from [dbo].[LU_APPNAME]
where APPNAME <> ''
order by APPNAME


select distinct USERNAME, APPNAME
from dbo.SDB_SESSION s
left join [dbo].[LU_USER] u on s.FK_USERID = u.PK_USERID
left join [dbo].[LU_APPNAME] a on s.FK_APPNAMEID = a.PK_APPNAMEID
where APPNAME <> ''
--and FK_NETDOMAINID = 1
order by USERNAME

dbo.upGetADInfo 'cn,samaccountname,name,objectsid,objectCategory', 'where objectClass="group" or objectClass="user" and name="admin*"'

declare @res varbinary(100)
set @res = encryptbypassphrase('SQL SERVER', '100000')
print len(@res)
print @res
print convert(varchar(200), decryptbypassphrase('SQL SERVER', @res))

select distinct NodeName
from dbo.vwServerOS
where charindex('2003', OSType) > 0
order by NodeName

select distinct Source, 'TCC_20140905_outbound.txt'
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\feeds\',
	'select * from TCC_20140905_outbound.txt')
union
select distinct Source, 'PATC_20140905_outbound.txt'
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Text;Database=C:\feeds\',
	'select * from PATC_20140905_outbound.txt')
order by source

select a.Name, case a.Description when 'PMRY-VM' then 'VM' when 'PMRY -P' then 'Physical' when 'PMRY-P' then 'Physical' when 'PMRY-Unx' then 'Unix' else a.Description end 'Description', a.Serial_Num, b.IPAddress, a.Domain
from dbo.vwServer a
left join dbo.vwServerIP b on a.HardwareID = b.HardwareID
where a.Description like 'PMRY-Unx%'
order by convert(varchar(max), a.Description), a.Name, b.IPAddress

select a.Name, a.Description, replace(replace(convert(varchar(max), a.UDF_MajorApplications), char(10), ''), char(13), '') 'UDF_MajorApplications', b.Category, b.ServerName,
	replace(replace(convert(varchar(max), b.MajorApplication), char(10), ''), char(13), '') 'MajorApplication', b.Dept, 
	replace(replace(convert(varchar(max), b.Contact), char(10), ''), char(13), '') 'Contact', b.IPAdress, b.Cabinet, b.OS, b.Model, b.Serial, b.ResourcePool
from dbo.vwServer a
left join (
select F1 'Category', F2 'ServerName', Server 'MajorApplication', Dept, [PA Dept CTM] 'Contact', Server1 'IPAdress', Cabinet, Server2 'OS', Model, Serial, F3 'ResourcePool'
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\ServerNameMASTER.xls', 'select * from [TELE$]')
where Model = 'virtual'
union
select F1 'Category', F2 'ServerName', Server 'MajorApplication', Dept, [PA Dept CTM] 'Contact', Server1 'IPAdress', Cabinet, Server2 'OS', Model, Serial, F3 'ResourcePool'
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\ServerNameMASTER.xls', 'select * from [PATC$]')
where Model = 'virtual'
) b on a.Name = b.ServerName
where charindex('VMWare', a.Brand) > 0  --charindex('-VM', Description) > 0 --charindex('VMware', Serial_Num) > 0

select b.*, a.Name
from (
select F1 'Category', F2 'ServerName', Server 'MajorApplication', Dept, [PA Dept CTM] 'Contact', Server1 'IPAdress', Cabinet, Server2 'OS', Model, Serial, F3 'ResourcePool'
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\ServerNameMASTER.xls', 'select * from [TELE$]')
where Model = 'virtual'
union
select F1 'Category', F2 'ServerName', Server 'MajorApplication', Dept, [PA Dept CTM] 'Contact', Server1 'IPAdress', Cabinet, Server2 'OS', Model, Serial, F3 'ResourcePool'
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\ServerNameMASTER.xls', 'select * from [PATC$]')
where Model = 'virtual'
) b
left join dbo.vwServer a on b.ServerName = a.Name
where a.Name is null

select PrintServer, Queue, IPAddress, h.Name, h.Primary_IP_Address, Online
from (
	select *
	from openrowset('Microsoft.ACE.OLEDB.12.0',
		'Text;Database=\\teledsrv1\data\TSD\HOME\tfellini\Printers\',
		'select * from PATCIV05_allprintqueues.csv')
	union
	select *
	from openrowset('Microsoft.ACE.OLEDB.12.0',
		'Text;Database=\\teledsrv1\data\TSD\HOME\tfellini\Printers\',
		'select * from PATCIV06_allprintqueues.csv')
	union
	select *
	from openrowset('Microsoft.ACE.OLEDB.12.0',
		'Text;Database=\\teledsrv1\data\TSD\HOME\tfellini\Printers\',
		'select * from TELEIV07_allprintqueues.csv')
	union
	select *
	from openrowset('Microsoft.ACE.OLEDB.12.0',
		'Text;Database=\\teledsrv1\data\TSD\HOME\tfellini\Printers\',
		'select * from TELEIV08_allprintqueues.csv')
) printers
left join AN6.dbo.Hardware h on printers.IPAddress = h.Primary_IP_Address
--where Category_ID = 'F9DAC2AC-EE52-4BE8-9390-7AC23707E5FA'
--where [Duplex Capable] = 'Yes'
--	and Duplex = 'TwoSidedLongEdge'
--	--and Driver = 'PCL6 Driver for Universal Print'
--	--and PrintServer = 'TELEIV08'
--order by Queue


select srv.Name, s.[IP Address], sw.Name 'Switch', srv.HardwareID, sw.HardwareID, srv.*, sw.*
from dbo.vwServerIP srv
inner join openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\endhosts.xlsx', 'select switch, port, [mac address], [host name], [ip address], vlan from [Sheet1$]') s on srv.IPAddress = s.[ip address]
left join dbo.vwSwitchIP sw on s.switch = sw.IPAddress
where srv.Name = 'PATCDB19'
order by srv.Name


insert into dbo.tbServerSwitchPort(HardwareID, SwitchPortID, MacAddress, IPAddress)
select distinct ServerID, SwitchPortID, MacAddress, IPAddress
from dbo.tbSwitchPort sp
inner join (
select srv.Name, s.[IP Address] IPAddress, sw.Name 'Switch', s.port, srv.HardwareID 'ServerID', sw.HardwareID 'SwitchID', s.[mac address] 'MacAddress'
from dbo.vwServerIP srv
inner join openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\endhosts.xlsx', 'select * from [Sheet1$]') s on srv.IPAddress = s.[ip address]
left join dbo.vwSwitchIP sw on s.switch = sw.IPAddress
union
select c.Name, s.[IP Address] IPAddress, sw.Name 'Switch', s.port, a.HardwareID, sw.HardwareID, s.[mac address] 'MacAddress'
from dbo.vwServerAlloy a
inner join AN6.dbo.Computers c on a.AlloyID = c.ID
inner join openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\endhosts.xlsx', 'select * from [Sheet1$]') s on c.MAC = replace([mac address], '-', ':')
left join dbo.vwSwitchIP sw on s.switch = sw.IPAddress
) a on sp.HardwareID = a.SwitchID and sp.Port = a.port


select *
from dbo.Soft_Product_List
where Product_Name like 'System Center%'
or Product_Name like '%SMS%' --'Sccm%'
or Product_Name like 'Microsoft System Center%'
order by Product_Name

--Product_Name like '%upstream client %'
--and Version <> '3.7.3100'

select c.Name, p.Product_Name
from dbo.Detected_Software_Products sp
inner join dbo.Soft_Product_List p on sp.Soft_Product_ID = p.id
inner join dbo.Computers c on sp.Object_ID = c.ID
where 
Product_Name like '%upstream client %'
and Version <> '3.7.3100'
order by c.Name

select p.Product_Name
from dbo.Computers c
inner join dbo.Detected_Software_Products sp on c.ID = sp.Object_ID
inner join dbo.Soft_Product_List p on sp.Soft_Product_ID = p.ID
where Name = 'MONTIS07'
order by p.Product_Name

--select switch, port, [mac address], case when left(isnull([host name], 1), 1) = '1' then '' when charindex('.wks.', [host name]) > 0 then '' else [host name] end, [ip address], vlan

select srv.Name, srv.IPAddress, sw.Name 'Switch', s.*
from dbo.vwServerIP srv
inner join openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\endhosts.xlsx', 'select switch, port, [mac address], [host name], [ip address], vlan from [Sheet1$]') s on srv.IPAddress = s.[ip address]
left join dbo.vwSwitchIP sw on s.switch = sw.IPAddress
--left join AN6.dbo.IP_Address_Link_List i on s.[ip address] = i.IP_Address
--left join AN6.dbo.Computers c on i.Object_ID = c.ID
--where c.Name is not null


SELECT TOP 1000 [ID]
      ,[Class_ID]
      ,[Status]
      ,[Rank]
      ,[System]
      ,[Active]
      ,[Tags]
      ,[Image_ID]
  FROM [AN6].[dbo].[Status]
  where class_id = 'C73482C8-A913-48BF-9898-88AE449C9F19'

select IPAddress, [Printer Queue], Dept
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Feeds\Lanier.xlsx', 'select * from [Lanier$]')
where steps in (19,20,24,'C')
	and [Printer Queue] is not null
	--and [ping test] = 'Y'
order by Dept, [Printer Queue]

insert into dbo.tbSwitchPort(HardwareID, Port)
select distinct SwitchID, Port--, MacAddress, IPAddress
from (
select srv.Name, s.[IP Address] IPAddress, sw.Name 'Switch', s.port, srv.HardwareID 'ServerID', sw.HardwareID 'SwitchID', s.[mac address] 'MacAddress'
from dbo.vwServerIP srv
inner join openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\endhosts.xlsx', 'select * from [Sheet1$]') s on srv.IPAddress = s.[ip address]
left join dbo.vwSwitchIP sw on s.switch = sw.IPAddress
union
select c.Name, s.[IP Address] IPAddress, sw.Name 'Switch', s.port, a.HardwareID, sw.HardwareID, s.[mac address] 'MacAddress'
from dbo.vwServerAlloy a
inner join AN6.dbo.Computers c on a.AlloyID = c.ID
inner join openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\endhosts.xlsx', 'select * from [Sheet1$]') s on c.MAC = replace([mac address], '-', ':')
left join dbo.vwSwitchIP sw on s.switch = sw.IPAddress
) a
order by switchID, Port

--select switch, port, [mac address], case when left(isnull([host name], 1), 1) = '1' then '' when charindex('.wks.', [host name]) > 0 then '' else [host name] end, [ip address], vlan

select srv.Name, srv.IPAddress, sw.Name 'Switch', s.*
from dbo.vwServerIP srv
inner join openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\endhosts.xlsx', 'select switch, port, [mac address], [host name], [ip address], vlan from [Sheet1$]') s on srv.IPAddress = s.[ip address]
left join dbo.vwSwitchIP sw on s.switch = sw.IPAddress
--left join AN6.dbo.IP_Address_Link_List i on s.[ip address] = i.IP_Address
--left join AN6.dbo.Computers c on i.Object_ID = c.ID
--where c.Name is not null
order by srv.Name

select *
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\endhosts.xlsx', 'select * from [Sheet1$]')
where replace([mac Address], '-', ':') in ('00:0D:56:71:5C:F4', '00:0D:56:71:5C:F6', '00:02:B3:EF:18:80', '90:E2:BA:00:09:BC','90:E2:BA:00:09:A6')

select top 10 * from openquery( ADSI, 'select 
badPasswordTime, badPwdCount
from ''LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov'' where objectClass =  ''User''')

select *
from 
openrowset('ADSDSOObject', 'adsdatasource',
'select  *
from ''LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov'' where objectClass =  ''User'' and Name = ''tfellini''' )