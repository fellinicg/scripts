select 
	Contained
	, [Returned to TSD]
	, [Type]
	, [UDF_SiteCode] 'Site Code'
	, [UDF_SiteDescription] 'Site Name'
--	, [UDF_AMRSMachineBuildingManual] 'AMRS Machine Building'
	, isnull(Computer_Name, shost) 'Computer Name'
	, Serial_Num 'Serial Number'
	, [UDF_RAW_AMRSUsernameDetected] 'Owner'
	, [UDF_ScannedUser] 'Scanned User'
	, [UDF_AMRSUserDivision] 'AMRS User Division'
	, [UDF_AMRSUserFirstName] 'AMRS User First Name'
	, [UDF_AMRSUserLastName] 'AMRS User Last Name'
	, OS_Name + ' ' + OS_ServicePack 'OS Ver'
	, convert(varchar, [Build Date], 101) 'Build Date'
	, convert(varchar, [Deliver Date], 101) 'Deliver Date'
	, convert(varchar, [Install Date], 101) 'Install Date'
from openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Feeds\20140605-FireEye7Day.xlsx', 'select Contained, [Returned to TSD], Type, src, shost, [Build Date], [Deliver Date], [Install Date] from [Sheet1$]') a
left join dbo.Computers on replace(replace(shost, '.wks.pacorp.panynj.gov', ''), 'l-', '')  = convert(varchar(max), Name)