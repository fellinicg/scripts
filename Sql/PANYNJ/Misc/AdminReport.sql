select d.Name, d.Serial_Num, ug.Name, u.PeoplesoftDept Department, a.s, u.UserName, u.IsEnabled -- *
from AD6.dbo.Computers d
	left join AN6.dbo.Computers n on d.Node_ID = n.Import_ID
	inner join AN6.dbo.Status s on s.ID = n.Status_ID
	inner join AD6.dbo.Inv_User_Groups ug on d.Node_ID = ug.Node_ID
	cross apply AD6.dbo.fnSplitString(Accounts, ',') a
	left join dbo.tbADUser u on replace(a.s, '"', '') = 'PANYNJ\' + u.UserName
where n.ID is not null
	and n.Status_ID not in ('3417FB44-7DF7-4C9F-9A25-AAB4CC4C43E6', '89F5D1A2-4B43-4C40-9E71-1A5BE2EB1C3A')
	and ug.Name in ('Administrators', 'Power Users')
	and datediff(day, d.Audit_Date, SYSDATETIME()) < 90
	and len(a.s) > 0
	--and u.IsEnabled = 1