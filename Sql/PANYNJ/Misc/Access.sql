use REAL_ESTATEACCESSCONTROL

-- Employee search
select top 20
	emp.FIRSTNAME
	,emp.LASTNAME
	,case evts.EVENTID when 0 then 'Access Granted' else evt.EVDESCR end
	,rdr.READERDESC
	,convert(varchar, dateadd(second, datediff(second, getdate(), getutcdate()) * -1, evts.EVENT_TIME_UTC), 100)
from dbo.EMP emp
inner join dbo.EVENTS evts on emp.ID = evts.EMPID
left join dbo.EVENT evt on evts.EVENTID = evt.EVENTID
left join dbo.READER rdr on evts.DEVID = rdr.READERID and evts.MACHINE = rdr.PANELID
where emp.LASTNAME in ('jackson', 'collier', 'fellini', 'matchancikal', 'radoncic', 'fitterman', 'stine', 'munday', 'harrison', 'pagano', 'schiff')
 and convert(date, evts.EVENT_TIME_UTC) = convert(date, current_timestamp)
order by evts.EVENT_TIME_UTC desc

return;

-- Current day
select 
	emp.FIRSTNAME
	,emp.LASTNAME
	,case evts.EVENTID when 0 then 'Access Granted' else evt.EVDESCR end
	,rdr.READERDESC
	,convert(varchar, dateadd(hh, -4, evts.EVENT_TIME_UTC), 100)
from dbo.EMP emp
inner join dbo.EVENTS evts on emp.ID = evts.EMPID
left join dbo.EVENT evt on evts.EVENTID = evt.EVENTID
left join dbo.READER rdr on evts.DEVID = rdr.READERID and evts.MACHINE = rdr.PANELID
where convert(date, evts.EVENT_TIME_UTC) = convert(date, current_timestamp)
	--and READERDESC like '5th%'
order by evts.EVENT_TIME_UTC desc