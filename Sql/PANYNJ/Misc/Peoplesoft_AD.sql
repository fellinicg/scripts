use Omni
-- Update from Peoplesoft file
update dbo.tbADUser set PeoplesoftID = p.EMPLID, Office = p.[ORG UNIT DESCR], PeoplesoftDept = p.[DEPT DESCR], PeoplesoftStatus = p.[STATUS]
from dbo.tbADUser m
	left join openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [AD_LISTING2.TXT]') p on m.Username = p.USERID
where p.USERID is not null

-- Matches
select m.UserName, m.FirstName, p.[FIRST NAME], m.LastName, p.[LAST NAME], m.Office, p.[ORG UNIT DESCR], p.[DEPT DESCR], p.EMPLID, [IsEnabled], p.[STATUS]
from dbo.tbADUser m
	left join openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [AD_LISTING2.TXT]') p on m.Username = p.USERID
where p.USERID is not null

-- Not in Peoplesoft
select m.UserName, m.FirstName, m.LastName, m.Office
from dbo.tbADUser m
	left join openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [AD_LISTING2.TXT]') p on m.Username = p.USERID
where p.USERID is null


-- Not in AD
select p.EMPLID, p.[FIRST NAME], p.[LAST NAME], p.[ORG UNIT DESCR], p.[DEPT DESCR], p.[STATUS]
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [AD_LISTING2.TXT]') p
	left join dbo.tbADUser m on m.Username = p.USERID
where m.Username is null


--Peoplesoft Departments
select distinct p.[DEPT DESCR]
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [AD_LISTING2.TXT]') p
where p.[DEPT DESCR] is not null
	and left(p.[DEPT DESCR], 3) <> 'Ret'
order by p.[DEPT DESCR]

select count(distinct p.USERID)
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [AD_LISTING2.TXT]') p

select count(p.USERID)
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [AD_LISTING2.TXT]') p

--Fitterman
select distinct PeoplesoftDept, isnull(PeoplesoftLocation, '') as PeoplesoftLocation, isnull(PeoplesoftLocDescription, '') as PeoplesoftLocDescription, '' as DN
from dbo.tbADUser
where PeoplesoftDept is not null
order by PeoplesoftDept

select distinct substring(DN, charindex('.', DN, 2), 100) 'Novell DN'
from dbo.tbADUser
where DN is not null

select distinct reverse(substring(reverse(OU), charindex('/', reverse(OU)), 1000)) 'OU', substring(DN, charindex('.', DN, 2), 100) 'Novell DN'
from dbo.tbADUser
where DN is not null

select distinct PeoplesoftLocation, PeoplesoftLocDescription
from dbo.tbADUser
where PeoplesoftLocation is not null
order by PeoplesoftLocDescription

--select top 10 OU, reverse(OU), reverse(substring(reverse(OU), charindex('/', OU), 100))
--from dbo.tbADUser

--select charindex('.', DN, 2), DN, UserName, replace(DN, '.' + UserName, ''), substring(DN, charindex('.', DN, 2), 100)
--from dbo.tbADUser
--where DN is not null

