truncate table dbo.tbServerACl

insert into dbo.tbServerACL
select 
	'TELEFS20' ServerName
	, [Folder Path]
	, IdentityReference
	, AccessControlType
	, FileSystemRights
	, case IsInherited when 'True' then 1 when 'False' then 0 else null end IsInherited
	, InheritanceFlags
	, PropagationFlags
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [TELEFS20 - ACLs.csv]')