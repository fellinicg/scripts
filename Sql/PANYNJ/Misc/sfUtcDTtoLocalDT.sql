set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

if  exists (select * from sys.objects where object_id = object_id(N'dbo.sfUtcDTtoLocalDT') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
drop function dbo.sfUtcDTtoLocalDT
go
-- ============================================================
-- OrigAuthorFullName: Todd Fellini
-- OrigCreateDate: 20171004
-- ============================================================
go
create function dbo.sfUtcDTtoLocalDT(@UtcDT datetime)
returns datetime
as
begin
	declare @Offset int = -5
	declare @SpringAheadDT datetime
	declare @FallBackDT datetime

	if year(@UtcDT) <= 2006
	begin 
 		set @SpringAheadDT = dateadd(hh, 2, '4/' + convert(varchar, ((2 + 6 * year(@UtcDT) - (floor(year(@UtcDT) / 4))) % 7 + 1)) + '/' + convert(varchar, year(@UtcDT)))
		set @FallBackDT = dateadd(s, 3599, '10/' + convert(varchar, (31 - ((year(@UtcDT) * 5 / 4) + 1) % 7)) + '/' + convert(varchar, year(@UtcDT)))
	end
	else
	begin 
 		set @SpringAheadDT = dateadd(hh, 2, '3/' + convert(varchar, (14 - (floor (1 + year(@UtcDT) * 5 / 4) % 7))) + '/' + convert(varchar, year(@UtcDT)))
		set @FallBackDT = dateadd(s, 3599, '11/' + convert(varchar, (7 - (floor (1 + year(@UtcDT) * 5 / 4) % 7))) + '/' + convert(varchar, year(@UtcDT)))
	end

	return (select case when dateadd(hh, @Offset, @UtcDT) between @SpringAheadDT and @FallBackDT then dateadd(hh, (@Offset + 1), @UtcDT) else dateadd(hh, @Offset, @UtcDT) end)
end