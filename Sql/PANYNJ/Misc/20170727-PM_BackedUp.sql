-- NO
select 
	Server
	, c.UDF_BackedUp
--update AN7.dbo.Computers set UDF_BackedUp = 'No'
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\combined.xlsx', 'select * from [No$]') x
inner join AN7.dbo.Computers c on x.Server = c.Computer_Name
union
select 
	Server
	, c.UDF_BackedUp
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\combined.xlsx', 'select * from [No$]') x
left join AN7.dbo.Computers c on x.Server = c.Computer_Name
where c.ID is null


select distinct server from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\combined.xlsx', 'select * from [No$]') x

-- YES
select 
	Server
	, c.UDF_BackedUp
update AN7.dbo.Computers set UDF_BackedUp = 'Yes'
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\combined.xlsx', 'select * from [Yes$]') x
inner join AN7.dbo.Computers c on x.Server = c.Computer_Name
union
select 
	Server
	, c.UDF_BackedUp
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\combined.xlsx', 'select * from [Yes$]') x
left join AN7.dbo.Computers c on x.Server = c.Computer_Name
where c.ID is null


select distinct server from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\combined.xlsx', 'select * from [Yes$]') x