select distinct a.appname, count(distinct u.username) '# of Users'
from dbo.sdb_session s
inner join dbo.lu_user u on s.fk_userid = u.pk_userid
inner join dbo.lu_appname a on s.fk_appnameid = a.pk_appnameid
where len(a.appname) > 0
group by a.appname
order by a.appname

select distinct a.appname, u.username
from dbo.sdb_session s
inner join dbo.lu_user u on s.fk_userid = u.pk_userid
inner join dbo.lu_appname a on s.fk_appnameid = a.pk_appnameid
where len(a.appname) > 0
order by a.appname, u.username
