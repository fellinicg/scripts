select distinct Name, isnull(isnull(PrimaryIP, b.IP), c.IP) 'IP'--, ID, Status --, 'nslookup ' + Name + '.pacorp.panynj.gov >> c:\ips3.txt'
from dbo.vwServer a
left join (select replace(Server, '.PANYNJ.GOV', '') as Server, IP from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [IPS.CSV]')) b on a.Name = b.Server
left join (select replace(Server, '.PACORP.PANYNJ.GOV', '') as Server, IP from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [IPS.CSV]')) c on a.Name = c.Server
where charindex('PMRY-', Description) = 1 and charindex('-HWO', Description) = 0
-- and PrimaryIP is null and IP is null
order by IP, Name