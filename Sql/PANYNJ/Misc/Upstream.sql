use Omni

select c.Name, p.Product_Name, c.OS_Name, c.OS_ServicePack, c.OS_Version, c.Description
from dbo.vwServerAlloy
inner join AN6.dbo.Computers c on AlloyID = c.ID
inner join AN6.dbo.Detected_Software_Products sp on c.ID = sp.Object_ID
inner join AN6.dbo.Soft_Product_List p on sp.Soft_Product_ID = p.id
where Product_Name like '%upstream client %'
	and Version <> '3.7.3200'
	and (charindex('PMRY-', c.Description) = 1 and charindex('-HWO', c.Description) = 0)
order by c.Name