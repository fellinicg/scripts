use omni

select facility, replace(location, char(10), ' ') location
from (
	select distinct facility, location
	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\SystemsDump.xlsx', 'select * from [Sheet1$]') a
	union
	select distinct facility_name, (physical_location + ' ' + equipment_location) location
	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\DevicesDump.xlsx', 'select * from [Sheet1$]') a
) a
where 
	facility is not null
	and location <> 'building 1'
	and replace(location, ' ', '') <> 'Building1Building1'
	and location <> 'Virtual Virtual'
order by facility, location


--select replace('Building 1  Building 1 ', ' ', '')