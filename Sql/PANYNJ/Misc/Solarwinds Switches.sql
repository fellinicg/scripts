select *
from 
(
select distinct
	i.InterfaceIndex
	, i.FullName SwitchPort
	, i.InterfaceSpeed
	, i.PhysicalAddress SwitchPortMAC
	, replace(i.StatusLED, '.gif', '') PortStatus
	, nc.MACAddress ConnectedMAC
	, n.Caption	ConnectedDevice
from 
	dbo.Interfaces i
	inner join dbo.NodePortInterfaceMap ni on i.NodeID = ni.NodeID and i.InterfaceIndex = ni.IfIndex
	inner join dbo.NodeL2Connections nc on ni.NodeID = nc.NodeID and ni.PortID = nc.PortID and ni.VlanId = nc.VlanId
	left join dbo.Interfaces ci on nc.MACAddress = ci.PhysicalAddress
	left join dbo.Nodes n on ci.NodeID = n.NodeID
where 
	i.NodeID in (--1027, 
				1426, 1427, 1428, 1429, 1430, 1431
				--, 1432, 1433
				)
	and i.IfName not in ('Po1', 'Po2')
	--and charindex('uplink', i.Caption) = 0
--order by 
--	i.FullName
--	,i.InterfaceIndex
) a
--where charindex('teledb23', a.ConnectedDevice) > 0
order by 
	SwitchPort
	, InterfaceIndex