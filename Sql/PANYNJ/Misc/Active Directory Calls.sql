--select *
--from openquery (
--	ADSI, 
--	'<LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov>;
--	(&(objectCategory=person)(objectClass=user)(userAccountControl:1.2.840.113556.1.4.803:=65536));
--	cn;subtree')

select *
from openquery (
	ADSI, 
	'<LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov>;
	(memberOf:1.2.840.113556.1.4.1941:=CN=DBA_Admins,OU=Groups,OU=Network Resources,DC=PACORP,DC=PANYNJ,DC=GOV);
	cn,samaccounttype;subtree')

select *
from openquery (
	ADSI, 
	'<LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov>;
	(member:1.2.840.113556.1.4.1941:=CN=tfellini,OU=Mbox On Office365,OU=TEC,OU=2 Montgomery St,OU=PANYNJ Users,DC=PACORP,DC=PANYNJ,DC=GOV);
	cn,samaccounttype,primaryGroupID;subtree')

select *
from openquery (
	ADSI, 
	'<LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov>;
	(&(objectCategory=person)(objectClass=user)(primaryGroupID=513));
	cn;subtree')

--select top 1500 * from openquery( ADSI, 'select 
--distinguishedname
--from ''LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov'' where objectClass =  ''Group'' and name = ''DnsAdmins''')
----where charindex('admin', name) > 0


--select * 
--from openquery( 
--	ADSI, 
--	'select distinguishedname 
--	from ''LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov'' 
--	where objectClass =  ''User'' 
--	and sAMAccountName = ''tfellini''
--	'
--)

--groups
select 
	*
from openquery (
	ADSI, '<LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov>;(&(objectClass=group)(member:1.2.840.113556.1.4.1941:=CN=AYramos,OU=Disabled.Users,OU=PANYNJ.Disabled,OU=PANYNJ.Resources,DC=PACORP,DC=PANYNJ,DC=GOV));objectsid;subtree')

select 
	* 
from openquery (
	ADSI, '<LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov>;(&(objectClass=group)(member:1.2.840.113556.1.4.1941:=CN=svc_LGA_PAPD_137,OU=SVC Accounts,OU=Network Resources,DC=PACORP,DC=PANYNJ,DC=GOV));objectsid,samaccounttype;subtree')


		select b.SID, dbo.sfHexSidToString(a.objectsid), passwordlastset --cast((convert(bigint, pwdlastset)/ 864000000000.0 - 109207) as datetime)
		from openquery (
			ADSI, 
			'<LDAP://teleis29/dc=pacorp,dc=panynj,dc=gov>;
			(memberOf:1.2.840.113556.1.4.1941:=CN=Domain Admins,CN=Users,DC=PACORP,DC=PANYNJ,DC=GOV);
			objectsid,samaccounttype,passwordLastSet;subtree') a
		join dbo.tbADGroup b on GroupName = 'Domain Admins'
		where samaccounttype = 805306368