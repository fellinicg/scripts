insert into dbo.tbServerAppContact(HardwareID, DeploymentStatus, DepartmentName, FailoverServerName, ApplicationName, TSDContact, CustomerContact, HoursOfOperation, ExpectedRecoveryTime)
select distinct
	HardwareID 
	--, a.Name
	, b.Status
	, b.Dept# 'Dept'
	, [Failover Server] 'FailoverServer'
	, b.Application
	, b.[TSD Contact] 'TsdContact'
	, b.[Customer Contact] 'CustomerContact'
	, b.[Hours of Operation] 'HoursOfOperation'
	, b.[User's Expected Recovery Time] 'ExpectedRecoveryTime'
from dbo.vwServer a
inner join openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\ServerAppContacts.xls', 'select * from [Sheet1$]') b on a.Name = b.[Server Name]



--truncate table [dbo].[tbServerAppContact]