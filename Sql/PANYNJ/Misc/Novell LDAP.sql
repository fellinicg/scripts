select m.*, c.OS_Name, c.UDF_MajorApplications, c.Description
from 
	(select distinct Server, replace(replace(Client, '.pacorp', ''), '.wks', '') 'Client', [Client IP]
	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\novell.xlsx', 'select Server, Client, [Client IP] from [Novell$] where Client is not null')
	where Client <> [Client IP]) m
left join AN6.dbo.Computers c on m.Client = c.Name
union all
select m.Server, c.Name 'Client', m.[Client IP], c.OS_Name, c.UDF_MajorApplications, c.Description
from 
	(select distinct Server, replace(replace(Client, '.pacorp', ''), '.wks', '') 'Client', [Client IP]
	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\novell.xlsx', 'select Server, Client, [Client IP] from [Novell$] where Client is not null')
	where Client = [Client IP]) m
left join AN6.dbo.Computers c on m.[Client IP] = c.Primary_IP_Address
order by c.OS_Name, Client
