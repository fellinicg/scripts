select
	count(*) [Total Clients]
	, li.title
	--,coll.name,
	SUM(CASE WHEN ucs.status=3 or ucs.status=1  then 1 ELSE 0 END ) as 'Installed / Not Applicable',
	sum( case When ucs.status=2 Then 1 ELSE 0 END ) as 'Required',
	sum( case When ucs.status=0 Then 1 ELSE 0 END ) as 'Unknown',
	round((CAST(SUM (CASE WHEN ucs.status=3 or ucs.status=1 THEN 1 ELSE 0 END) as float)/count(*) )*100,2) as 'Success %',
	round((CAST(count(case when ucs.status not in('3','1') THEN '*' end) as float)/count(*))*100,2) as 'Not Success%'
From v_Update_ComplianceStatusAll UCS
inner join v_r_system sys on ucs.resourceid=sys.resourceid
inner join v_FullCollectionMembership fcm on ucs.resourceid=fcm.resourceid
inner join v_collection coll on coll.collectionid=fcm.collectionid
inner join v_AuthListInfo LI on ucs.ci_id=li.ci_id
where coll.CollectionID='0020003F'
--and LI.CI_UniqueID in (@AuthListID)
group by li.title,coll.name
order by 1

SELECT [dbo].[v_R_System].Name0 as MachineName
	FROM [dbo].[v_Update_ComplianceStatusAll]
JOIN [dbo].[v_R_System]
	ON [dbo].[v_Update_ComplianceStatusAll].ResourceID=[dbo].[v_R_System].ResourceID
WHERE [dbo].[v_Update_ComplianceStatusAll].CI_ID='16844373'
	AND [dbo].[v_Update_ComplianceStatusAll].Status=3
	AND [dbo].[v_Update_ComplianceStatusAll].ResourceID
		IN (SELECT [dbo].[v_ClientCollectionMembers].ResourceID
			FROM [dbo].[v_ClientCollectionMembers]
			WHERE [dbo].[v_ClientCollectionMembers].CollectionID='0020003F')