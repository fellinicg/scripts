use omni

-- Clear Table
--
truncate table tbPeopleSoft

-- Import data into temp table
--
insert into tbPeopleSoft
select *
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\peoplesoft.xlsx', 'select * from [all_employees$]')

-- PeopleSoft Totals
--
select Status + ' (' + EmployeeStatus + ')' Status, count(EmployeeStatus)
from dbo.tbPeopleSoft
inner join dbo.vwEmployeeStatus on EmployeeStatus = StatusCode
group by Status, EmployeeStatus
with rollup
order by Status

-- Active Directory Totals
--
select case IsEnabled when 0 then 'Disabled' when 1 then 'Enabled' else 'Total' end 'IsEnabled', count(IsEnabled)
from dbo.tbADUser
group by IsEnabled
with rollup
order by IsEnabled
	
-- Almost perfect matches
--
--select count(1)
select m.UserName 'AD Username', m.FirstName 'AD FirstName', p.FirstName ' PS FirstName', m.MiddleInitials 'AD Middle', p.MiddleInitial 'PS Middle', 
	m.LastName 'AD LastName', p.LastName 'PS LastName', case m.IsEnabled when 1 then 'Enabled' else 'Disabled' end 'AD Status', s.Status 'PS Status',
	m.OfficePhone 'AD Ofc Phone', m.MobilePhone 'AD Mobile Phone', p.Phone 'PS Phone', m.Email 'AD Email', p.Email 'PS Email'
from dbo.tbADUser m
	inner join dbo.tbPeopleSoft p on m.Username = p.LoginID
	left join dbo.vwEmployeeStatus s on p.EmployeeStatus = s.StatusCode
where 
	p.FirstName = m.FirstName
	and p.LastName = m.LastName
order by m.IsEnabled desc, s.Status, m.UserName

-- Username/login id matches
--
--select count(1)
select m.UserName 'AD Username', m.FirstName 'AD FirstName', p.FirstName ' PS FirstName', m.MiddleInitials 'AD Middle', p.MiddleInitial 'PS Middle', 
	m.LastName 'AD LastName', p.LastName 'PS LastName', case m.IsEnabled when 1 then 'Enabled' else 'Disabled' end 'AD Status', s.Status 'PS Status',
	m.OfficePhone 'AD Ofc Phone', m.MobilePhone 'AD Mobile Phone', p.Phone 'PS Phone', m.Email 'AD Email', p.Email 'PS Email'
from dbo.tbADUser m
	inner join dbo.tbPeopleSoft p on m.Username = p.LoginID
	left join dbo.vwEmployeeStatus s on p.EmployeeStatus = s.StatusCode
where 
	isnull(p.FirstName, '') <> isnull(m.FirstName, '')
	or isnull(p.LastName, '') <> isnull(m.LastName, '')
order by m.IsEnabled desc, s.Status, m.UserName

-- First name mismatch, last name match
--
--select count(1)
select m.UserName 'AD Username', m.FirstName 'AD FirstName', p.FirstName ' PS FirstName', m.MiddleInitials 'AD Middle', p.MiddleInitial 'PS Middle', 
	m.LastName 'AD LastName', p.LastName 'PS LastName', case m.IsEnabled when 1 then 'Enabled' else 'Disabled' end 'AD Status', s.Status 'PS Status',
	m.OfficePhone 'AD Ofc Phone', m.MobilePhone 'AD Mobile Phone', p.Phone 'PS Phone', m.Email 'AD Email', p.Email 'PS Email'
from dbo.tbADUser m
	inner join dbo.tbPeopleSoft p on m.Username = p.LoginID
	left join dbo.vwEmployeeStatus s on p.EmployeeStatus = s.StatusCode
where 
	isnull(p.FirstName, '') <> isnull(m.FirstName, '')
	and isnull(p.LastName, '') = isnull(m.LastName, '')
order by m.IsEnabled desc, s.Status, m.UserName

-- Last name mismatch, first name match
--select count(1)
select m.UserName 'AD Username', m.FirstName 'AD FirstName', p.FirstName ' PS FirstName', m.MiddleInitials 'AD Middle', p.MiddleInitial 'PS Middle', 
	m.LastName 'AD LastName', p.LastName 'PS LastName', case m.IsEnabled when 1 then 'Enabled' else 'Disabled' end 'AD Status', s.Status 'PS Status',
	m.OfficePhone 'AD Ofc Phone', m.MobilePhone 'AD Mobile Phone', p.Phone 'PS Phone', m.Email 'AD Email', p.Email 'PS Email'
from dbo.tbADUser m
	inner join dbo.tbPeopleSoft p on m.Username = p.LoginID
	left join dbo.vwEmployeeStatus s on p.EmployeeStatus = s.StatusCode
where 
	isnull(p.FirstName, '') = isnull(m.FirstName, '')
	and isnull(p.LastName, '') <> isnull(m.LastName, '')
order by m.IsEnabled desc, s.Status, m.UserName

-- First and last name mismatch
--
--select count(1)
select m.UserName 'AD Username', m.FirstName 'AD FirstName', p.FirstName ' PS FirstName', m.MiddleInitials 'AD Middle', p.MiddleInitial 'PS Middle', 
	m.LastName 'AD LastName', p.LastName 'PS LastName', case m.IsEnabled when 1 then 'Enabled' else 'Disabled' end 'AD Status', s.Status 'PS Status',
	m.OfficePhone 'AD Ofc Phone', m.MobilePhone 'AD Mobile Phone', p.Phone 'PS Phone', m.Email 'AD Email', p.Email 'PS Email'
from dbo.tbADUser m
	inner join dbo.tbPeopleSoft p on m.Username = p.LoginID
	left join dbo.vwEmployeeStatus s on p.EmployeeStatus = s.StatusCode
where 
	isnull(p.FirstName, '') <> isnull(m.FirstName, '')
	and isnull(p.LastName, '') <> isnull(m.LastName, '')
order by m.IsEnabled desc, s.Status, m.UserName

-- First name is null
--
--select count(1)
select m.UserName 'AD Username', m.FirstName 'AD FirstName', p.FirstName ' PS FirstName', m.MiddleInitials 'AD Middle', p.MiddleInitial 'PS Middle', 
	m.LastName 'AD LastName', p.LastName 'PS LastName', case m.IsEnabled when 1 then 'Enabled' else 'Disabled' end 'AD Status', s.Status 'PS Status',
	m.OfficePhone 'AD Ofc Phone', m.MobilePhone 'AD Mobile Phone', p.Phone 'PS Phone', m.Email 'AD Email', p.Email 'PS Email'
from dbo.tbADUser m
	inner join dbo.tbPeopleSoft p on m.Username = p.LoginID
	left join dbo.vwEmployeeStatus s on p.EmployeeStatus = s.StatusCode
where 
	m.FirstName is null

-- No Username match
--
select m.IsEnabled, count(distinct m.ADUserID)
from dbo.tbADUser m
left join dbo.tbPeopleSoft p on m.UserName = p.LoginID
where p.LoginID is null
group by IsEnabled
with rollup

select m.UserName 'AD Username', m.FirstName 'AD FirstName', m.MiddleInitials 'AD Middle', m.LastName 'AD LastName', 
	case m.IsEnabled when 1 then 'Enabled' else 'Disabled' end 'AD Status', m.OfficePhone 'AD Ofc Phone', m.MobilePhone 'AD Mobile Phone', m.Email 'AD Email'
from dbo.tbADUser m
	left join dbo.tbPeopleSoft p on m.Username = p.LoginID
where 
	p.LoginID is null
	and not (charindex('SVC', OU) > 0
		or charindex('PANYNJ Service', OU) > 0)
order by m.IsEnabled desc, m.UserName

-- No login ID match, first name, last name match
--
if object_id('tempdb..#temp') is not null
	drop table #temp

select m.ADUserID, m.UserName 'AD Username', m.FirstName 'AD FirstName', p.FirstName 'PS FirstName', m.MiddleInitials 'AD Middle', p.MiddleInitial 'PS Middle', 
m.LastName 'AD LastName', p.LastName 'PS LastName', case m.IsEnabled when 1 then 'Enabled' else 'Disabled' end 'AD Status', s.Status 'PS Status',
m.OfficePhone 'AD Ofc Phone', m.MobilePhone 'AD Mobile Phone', p.Phone 'PS Phone', m.Email 'AD Email', p.Email 'PS Email'
into #temp
from (
		select m.ADUserID, m.UserName, m.FirstName, m.MiddleInitials, m.LastName, m.IsEnabled, m.OfficePhone, m.MobilePhone, m.Email
		from dbo.tbADUser m
		left join dbo.tbPeopleSoft p on m.UserName = p.LoginID
		where p.LoginID is null
	) m
	inner join dbo.tbPeopleSoft p on m.FirstName = p.FirstName and m.LastName = p.LastName
	left join dbo.vwEmployeeStatus s on p.EmployeeStatus = s.StatusCode

select distinct isnull([AD Status], 'Total') [AD Status], count(distinct ADUserID) --distinct ADUserID, m.IsEnabled
from #temp
group by [AD Status]
with rollup
order by [AD Status] desc

select [AD Username], [AD FirstName], [PS FirstName], [AD Middle], [PS Middle], [AD LastName], [PS LastName], [AD Status], [PS Status], [AD Ofc Phone], 
	[AD Mobile Phone], [PS Phone], [AD Email], [PS Email]
from #temp
where [AD Status] = 'Enabled'
order by [AD Status] desc, [PS Status], [AD Username]

select [AD Username], [AD FirstName], [PS FirstName], [AD Middle], [PS Middle], [AD LastName], [PS LastName], [AD Status], [PS Status], [AD Ofc Phone], 
	[AD Mobile Phone], [PS Phone], [AD Email], [PS Email]
from #temp
where [AD Status] = 'Disabled'
order by [AD Status] desc, [PS Status], [AD Username]

-- No Match at all
--
if object_id('tempdb..#temp') is not null
	drop table #temp

select m.ADUserID, m.UserName 'AD Username', m.FirstName 'AD FirstName', p.FirstName 'PS FirstName', m.MiddleInitials 'AD Middle', p.MiddleInitial 'PS Middle', 
m.LastName 'AD LastName', p.LastName 'PS LastName', case m.IsEnabled when 1 then 'Enabled' else 'Disabled' end 'AD Status', s.Status 'PS Status',
m.OfficePhone 'AD Ofc Phone', m.MobilePhone 'AD Mobile Phone', p.Phone 'PS Phone', m.Email 'AD Email', p.Email 'PS Email'
into #temp
from (
		select m.ADUserID, m.UserName, m.FirstName, m.MiddleInitials, m.LastName, m.IsEnabled, m.OfficePhone, m.MobilePhone, m.Email
		from dbo.tbADUser m
		left join dbo.tbPeopleSoft p on m.UserName = p.LoginID
		where p.LoginID is null
	) m
	inner join dbo.tbPeopleSoft p on m.FirstName = p.FirstName and m.LastName = p.LastName
	left join dbo.vwEmployeeStatus s on p.EmployeeStatus = s.StatusCode

select distinct isnull([AD Status], 'Total') [AD Status], count(distinct ADUserID) --distinct ADUserID, m.IsEnabled
from #temp
group by [AD Status]
with rollup
order by [AD Status] desc

-- Sheet7
--
select [AD Username], [AD FirstName], [PS FirstName], [AD Middle], [PS Middle], [AD LastName], [PS LastName], [AD Status], [PS Status], [AD Ofc Phone], 
	[AD Mobile Phone], [PS Phone], [AD Email], [PS Email]
from #temp
where [AD Status] = 'Enabled'
order by [AD Status] desc, [PS Status], [AD Username]

-- Sheet8
--
select [AD Username], [AD FirstName], [PS FirstName], [AD Middle], [PS Middle], [AD LastName], [PS LastName], [AD Status], [PS Status], [AD Ofc Phone], 
	[AD Mobile Phone], [PS Phone], [AD Email], [PS Email]
from #temp
where [AD Status] = 'Disabled'
order by [AD Status] desc, [PS Status], [AD Username]

select distinct case IsEnabled when 0 then 'Disabled' when 1 then 'Enabled' else 'Total' end 'IsEnabled', count(distinct m.ADUserID)
from dbo.tbADUser m
	left join #temp t on m.ADUserID = t.ADUserID
	left join dbo.tbPeopleSoft p on m.UserName = p.LoginID
where t.ADUserID is null and p.LoginID is null
group by IsEnabled
with rollup
order by IsEnabled desc

select m.UserName, m.FirstName, m.MiddleInitials, m.LastName, m.OfficePhone, m.MobilePhone, m.Email, case m.IsEnabled when 1 then 'Enabled' else 'Disabled' end IsEnabled
from dbo.tbADUser m
	left join #temp t on m.ADUserID = t.ADUserID
	left join dbo.tbPeopleSoft p on m.UserName = p.LoginID
where t.ADUserID is null and p.LoginID is null
order by m.IsEnabled desc, m.UserName

select m.UserName 'AD Username', m.FirstName 'AD FirstName', x.FirstName 'PS FirstName', m.MiddleInitials 'AD Middle', x.MiddleInitial 'PS Middle', 
m.LastName 'AD LastName', x.LastName 'PS LastName', case m.IsEnabled when 1 then 'Enabled' else 'Disabled' end 'AD Status', s.Status 'PS Status',
m.OfficePhone 'AD Ofc Phone', m.MobilePhone 'AD Mobile Phone', x.Phone 'PS Phone', m.Email 'AD Email', x.Email 'PS Email'
from dbo.tbADUser m
	left join #temp t on m.ADUserID = t.ADUserID
	left join dbo.tbPeopleSoft p on m.UserName = p.LoginID
	inner join dbo.tbPeopleSoft x on m.LastName = x.LastName
	left join dbo.vwEmployeeStatus s on x.EmployeeStatus = s.StatusCode
where t.ADUserID is null 
	and p.LoginID is null
--	and m.IsEnabled = 1
order by m.IsEnabled desc, x.EmployeeStatus, m.UserName, x.FirstName


-- Service Account
--
--select UserName, FirstName, LastName, IsEnabled, OU
select case IsEnabled when 0 then 'Disabled' when 1 then 'Enabled' else 'Total' end 'IsEnabled', count(IsEnabled)
from dbo.tbADUser m
left join dbo.tbPeopleSoft p on m.UserName = p.LoginID
where p.LoginID is null
	and (charindex('SVC', OU) > 0
		or charindex('PANYNJ Service', OU) > 0)
group by IsEnabled
with rollup
order by IsEnabled

-- All other
--select UserName, FirstName, LastName, IsEnabled, OU
select case IsEnabled when 0 then 'Disabled' when 1 then 'Enabled' else 'Total' end 'IsEnabled', count(IsEnabled)
from dbo.tbADUser m
left join dbo.tbPeopleSoft p on m.UserName = p.LoginID
where p.LoginID is null
--	and IsEnabled = 1
	and not (charindex('SVC', OU) > 0
		or charindex('PANYNJ Service', OU) > 0)
group by IsEnabled
with rollup
order by IsEnabled


select ct, count(ct)
from (
		select m.ADUserID, count(m.ADUserID) 'ct' --, m.UserName, m.FirstName, m.LastName, p.*
		from dbo.tbADUser m
		--left join dbo.tbPeopleSoft p on m.UserName = p.LoginID
		, dbo.tbPeopleSoft p
		where m.UserName <> p.LoginID
			and m.FirstName = p.FirstName
			and m.LastName = p.LastName
		group by m.ADUserID
		having count(m.ADUserID) > 1
	) a
group by ct
order by ct


select m.ADUserID, m.UserName, m.FirstName, m.LastName, m.MiddleInitials, m.IsEnabled, m.PeoplesoftID, p.*
from dbo.tbADUser m
--left join dbo.tbPeopleSoft p on m.UserName = p.LoginID
, dbo.tbPeopleSoft p
where m.UserName <> p.LoginID
	and m.FirstName = p.FirstName
	and m.LastName = p.LastName


select case a.IsEnabled when 0 then 'Disabled' when 1 then 'Enabled' else 'Total' end 'IsEnabled', count(distinct a.ADUserID)
from 
	(
		select distinct m.ADUserID, m.IsEnabled
		from dbo.tbADUser m
		left join dbo.tbPeopleSoft p on m.UserName = p.LoginID
		where p.LoginID is null
	) a
left join 
	(
		select distinct m.ADUserID
		from dbo.tbADUser m
		, dbo.tbPeopleSoft p 
		where 
			m.PeoplesoftID is null
			and m.FirstName = p.FirstName
			and m.LastName = p.LastName
	) b on a.ADUserID = b.ADUserID
where b.ADUserID is null
group by a.IsEnabled
with rollup
order by a.IsEnabled