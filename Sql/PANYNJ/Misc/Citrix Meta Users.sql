declare @DaysBack int 

set @DaysBack = -7 --365

--select distinct u.USERNAME, @DaysBack 'DaysBack'
--from dbo.SDB_SESSION s
--inner join dbo.LU_USER u on s.FK_USERID = u.PK_USERID
--where s.SESSIONSTART >= convert(varchar, dateadd(d, @DaysBack, getdate()), 101)

--select USERNAME, max(SESSIONSTART) 'LastLogin', @DaysBack 'DaysBack'
--from dbo.SDB_SESSION s
--inner join dbo.LU_USER u on s.FK_USERID = u.PK_USERID
--where s.SESSIONSTART >= convert(varchar, dateadd(d, @DaysBack, getdate()), 101)
--group by USERNAME

select 
	x.USERNAME
	, a.FirstName
	, a.LastName
	, a.Email
	, a.Office
	, isnull(a.PeoplesoftLocDescription, '') Location
	, isnull(a.OfficePhone, '') OfficePhone
	, isnull(a.MobilePhone, '') MobilePhone
	, convert(varchar, x.LastLogin, 100) LastLogin
	, x.DaysBack
from (
select u.USERNAME, max(SESSIONSTART) 'LastLogin', @DaysBack 'DaysBack'
from dbo.SDB_SESSION s
inner join dbo.LU_USER u on s.FK_USERID = u.PK_USERID
where s.SESSIONSTART >= convert(varchar, dateadd(d, @DaysBack, getdate()), 101)
group by u.USERNAME) x
left join [PATCDB21\ALLOY_1].Omni.dbo.tbADUser a on x.USERNAME = a.UserName
where isnull(a.IsEnabled, 1) = 1
order by Office

--select a.*, c.Computer_Name, c.OS_Name, c.OS_Version, c.Product, replace(replace(convert(varchar, c.Notes), char(13), ''), char(10), '') Notes, d.Status
--from openrowset('Microsoft.ACE.OLEDB.12.0',
--	'Excel 12.0;Database=C:\Feeds\20150804-LDAP.xlsx', 
--	'select * from [LDAP$]') a
--inner join AN6.dbo.Computers c on replace(replace(a.Client, '.pacorp', ''), '.wks', '') = c.Computer_Name
--inner join AN6.dbo.Status d on c.Status_ID = d.ID

--select a.*, c.Computer_Name, c.OS_Name, c.OS_Version, c.Product, replace(replace(convert(varchar, c.Notes), char(13), ''), char(10), '') Notes, d.Status
--from openrowset('Microsoft.ACE.OLEDB.12.0',
--	'Excel 12.0;Database=C:\Feeds\20150804-LDAP.xlsx', 
--	'select * from [LDAP$]') a
--inner join AN6.dbo.Computers c on replace(replace(a.Client, '.pacorp', ''), '.wks', '') = c.Primary_IP_Address
--inner join AN6.dbo.Status d on c.Status_ID = d.ID
