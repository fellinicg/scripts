Use Omni

-- Delete temporary table if it exists
--
if object_id('tempdb..#Results') is not null
    drop table #Results

-- Insert data into temporary table
--
--select 
--	d.Name 'MachineName', 
--	d.Serial_Num, 
--	u.PeoplesoftDept Department,
--	d.OS_Name, 
--	d.OS_Version, 
--	replace(a.s, '"', '') 'UserName', 
--	s.Status, 
--	ug.Accounts, 
--	d.Audit_Date,
--	u.IsEnabled
--into #Results
--from AD6.dbo.Computers d
--	inner join AN6.dbo.Computers n on d.Node_ID = n.Import_ID
--	inner join AN6.dbo.Status s on s.ID = n.Status_ID
--	inner join AD6.dbo.Inv_User_Groups ug on d.Node_ID = ug.Node_ID
--	cross apply AD6.dbo.fnSplitString(ug.Accounts, ',') a
--	left join dbo.tbADUser u on replace(a.s, '"', '') = 'PANYNJ\' + u.UserName
--where n.Type_ID <> '629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3' -- Servers
--	and n.Status_ID not in ('3417FB44-7DF7-4C9F-9A25-AAB4CC4C43E6', '89F5D1A2-4B43-4C40-9E71-1A5BE2EB1C3A') -- Retired and Discovery Delete
--	and ug.Name in ('Administrators') -- Local Admin group
--	and charindex('PANYNJ\', a.s) > 0 -- Domain Users only
--	and datediff(day, d.Audit_Date, SYSDATETIME()) < 30 -- Audited in the last 90 days
select 
	d.Name 'MachineName'
	, d.Serial_Num
	, n.Primary_IP_Address
	, u.PeoplesoftDept Department
	, n.Location
	, d.OS_Name
	, d.OS_Version
	, replace(a.s, '"', '') 'UserName'
	, n.Status
	, ug.Accounts
	, d.Audit_Date
	, u.IsEnabled
into #Results
from AD7.dbo.Computers d
	inner join AN7.dbo.Computer_List n on d.Node_ID = n.Import_ID
	inner join AD7.dbo.Inv_User_Groups ug on d.Node_ID = ug.Node_ID
	cross apply AD7.dbo.fnSplitString(ug.Accounts, ',') a
	left join Omni.dbo.tbADUser u on replace(a.s, '"', '') = 'PANYNJ\' + u.UserName
where n.Type_ID <> '629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3' -- Servers
	and n.Status_ID not in ('3417FB44-7DF7-4C9F-9A25-AAB4CC4C43E6', '89F5D1A2-4B43-4C40-9E71-1A5BE2EB1C3A') -- Retired and Discovery Delete
	and ug.Name in ('Administrators') -- Local Admin group
	and charindex('PANYNJ\', a.s) > 0 -- Domain Users only
	and datediff(day, d.Audit_Date, SYSDATETIME()) < 30 -- Audited in the last 30 days

-- Update IsEnabled using OrgUserName
--
update #Results set IsEnabled = u.IsEnabled
from #Results r
inner join dbo.tbADUser u on r.UserName = 'PANYNJ\' + u.OrgUserName
where r.IsEnabled is null

-- Engineering
--
select
	r.MachineName
	, r.Primary_IP_Address
	, r.UserName
	, case r.IsEnabled when 1 then 'Yes' else 'No' end IsEnabled
	, '' 'AD Group'
	, r.Department
	, r.Location
	, r.Serial_Num
	, r.OS_Name
	, r.OS_Version
	, r.Status
	, r.Audit_Date
	, r.Accounts
from #Results r
where charindex('Engineering', r.Department) > 0
union
select 
	r.MachineName
	, r.Primary_IP_Address
	, 'PANYNJ\' + u.UserName 
	, case u.IsEnabled when 1 then 'Yes' else 'No' end IsEnabled
	, r.UserName 'Group'
	, isnull(r.Department, u.PeoplesoftDept) Department
	, r.Location
	, r.Serial_Num
	, r.OS_Name
	, r.OS_Version
	, r.Status
	, r.Audit_Date
	, r.Accounts
from #Results r
inner join (
	select u.UserName, g.GroupName, u.PeoplesoftDept, u.IsEnabled
	from dbo.tbADUser u
	inner join dbo.tbADGroupMember gm on u.SID = gm.MemberSID
	inner join dbo.tbADGroup g on gm.GroupSID = g.SID
	where charindex('Engineering', u.PeoplesoftDept) > 0
) u on r.UserName = 'PANYNJ\' + u.GroupName
where
	r.IsEnabled is null
