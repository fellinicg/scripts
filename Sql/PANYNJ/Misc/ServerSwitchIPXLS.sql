use omni

select distinct s.Name, s.Description, sw.*
from dbo.vwServer s
inner join (
	select isnull([MAC Address], MAC) 'MACAdress', isnull([IP Address], [Host IP]) 'IPAddress', isnull([Switch], [Host Switch IP]) 'SwitchIP', isnull([Port], [Host Switch Port]) 'SwitchPort'
	from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20140602-endhosts.xlsx', 'select * from [Sheet1$] where [MAC Address] is not null and [IP Address] is not null') a
	full outer join openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20140602-riverbedexport.xlsx', 'select * from [Sheet1$] where MAC is not null and [Host Switch IP] is not null and [Host IP] is not null') b on a.[MAC Address] = replace(b.Mac, ':', '-')
	) sw on s.PrimaryIP = sw.IPAddress
where left(s.Description, 4) = 'PMRY'
--group by s.Name, s.Description
--having count(1) > 1
order by s.Description, s.Name


select distinct s.Name, s.Description, x.*
from dbo.vwServer s
left join (
	select distinct s.Name
	from dbo.vwServer s
	inner join (
		select isnull([MAC Address], MAC) 'MACAdress', isnull([IP Address], [Host IP]) 'IPAddress', isnull([Switch], [Host Switch IP]) 'SwitchIP', isnull([Port], [Host Switch Port]) 'SwitchPort'
		from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20140602-endhosts.xlsx', 'select * from [Sheet1$] where [MAC Address] is not null and [IP Address] is not null') a
		full outer join openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\20140602-riverbedexport.xlsx', 'select * from [Sheet1$] where MAC is not null and [Host Switch IP] is not null and [Host IP] is not null') b on a.[MAC Address] = replace(b.Mac, ':', '-')
		) sw on s.PrimaryIP = sw.IPAddress
	where left(s.Description, 4) = 'PMRY') x on s.Name = x.Name
where left(s.Description, 4) = 'PMRY'
	and x.name is null