use Omni

insert into dbo.tbServerAce (Path, Account, AccountSID, AceApplyTo, AceType, AceInheritance, AcePermissions)
select 
--max(len([Path])),max(len([Ace Account])),max(len([Ace Account SID])),max(len([Ace Apply To])),max(len([Ace Type])),max(len([Ace Inheritance])),max(len([Ace Permissions Text]))
*
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Text;Database=C:\Feeds', 'select * from [TELEAS79-ACL.TXT]')

SELECT [Path]
      ,[Account]
      ,[AccountSID]
      ,[AceApplyTo]
      ,[AceType]
      ,[AceInheritance]
      ,[AcePermissions]
      ,[CreateDT]
  FROM [Omni].[dbo].[tbServerAce]
  where 
	(charindex('Everyone', [Account]) > 0
	or 
	charindex('Authenticated Users', [Account]) > 0)
	and
	charindex('ACD', [Path]) > 0