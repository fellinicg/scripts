use REAL_ESTATEACCESSCONTROL

-- Employee search
select --top 20
	emp.FIRSTNAME
	,emp.LASTNAME
	,evt.EVDESCR
	,rdr.READERDESC
	,convert(varchar, dateadd(second, datediff(second, getdate(), getutcdate()) * -1, evts.EVENT_TIME_UTC), 100) 'TimeStamp'
from dbo.EMP emp
inner join dbo.EVENTS evts on emp.ID = evts.EMPID
inner join dbo.EVENT evt on evts.EVENTID = evt.EVENTID
inner join dbo.READER rdr on evts.DEVID = rdr.READERID
where emp.LASTNAME in ('Parrotta', 'Minto', 'Matchanickal', 'Alequin') and emp.FIRSTNAME <> 'RAMON'
	and evts.EVENT_TIME_UTC between '5/1/2013' and '8/1/2013'
order by emp.LASTNAME, evts.EVENT_TIME_UTC