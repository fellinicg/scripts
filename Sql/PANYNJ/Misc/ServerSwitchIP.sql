use omni

select distinct s.Name, s.PrimaryIP, sw.[Host Switch IP], sw.[Host Switch Port], sw.[MAC], ss.[Switch IP Address], ss.[Switch Port]
from dbo.vwServer s
inner join openrowset('Microsoft.ACE.OLEDB.12.0',
	'Excel 12.0;Database=C:\Feeds\20140602-RiverbedExport.xlsx', 'select * from [Sheet1$] where [Host Switch IP] is not null') sw on s.PrimaryIP = sw.[Host IP]
left join dbo.vwServerSwitch ss on s.PrimaryIP = ss.[Server IP Address]
order by PrimaryIP, Name