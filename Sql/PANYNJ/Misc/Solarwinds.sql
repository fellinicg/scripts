/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [NodeID]
      ,[ObjectSubType]
      ,[IP_Address]
      ,[DynamicIP]
      ,[UnManaged]
      ,[UnManageFrom]
      ,[UnManageUntil]
      ,[Caption]
      ,[DNS]
      ,[Community]
      ,[RWCommunity]
      ,[SysName]
      ,[Vendor]
      ,[LastBoot]
      ,[SystemUpTime]
      ,[LastSystemUpTimePollUtc]
      ,[SysObjectID]
      ,[Description]
      ,[Location]
      ,[Contact]
      ,[RediscoveryInterval]
      ,[PollInterval]
      ,[VendorIcon]
      ,[IOSImage]
      ,[IOSVersion]
      ,[GroupStatus]
      ,[StatusDescription]
      ,[Status]
      ,[StatusLED]
      ,[ChildStatus]
      ,[ResponseTime]
      ,[PercentLoss]
      ,[AvgResponseTime]
      ,[MinResponseTime]
      ,[MaxResponseTime]
      ,[NextPoll]
      ,[EngineID]
      ,[LastSync]
      ,[MachineType]
      ,[Severity]
      ,[NextRediscovery]
      ,[StatCollection]
      ,[Allow64BitCounters]
      ,[SNMPV2Only]
      ,[AgentPort]
      ,[SNMPVersion]
      ,[SNMPV3Username]
      ,[SNMPV3Context]
      ,[SNMPV3PrivMethod]
      ,[SNMPV3PrivKey]
      ,[SNMPV3PrivKeyIsPwd]
      ,[SNMPV3AuthMethod]
      ,[SNMPV3AuthKey]
      ,[SNMPV3AuthKeyIsPwd]
      ,[RWSNMPV3Username]
      ,[RWSNMPV3Context]
      ,[RWSNMPV3PrivMethod]
      ,[RWSNMPV3PrivKey]
      ,[RWSNMPV3PrivKeyIsPwd]
      ,[RWSNMPV3AuthMethod]
      ,[RWSNMPV3AuthKey]
      ,[RWSNMPV3AuthKeyIsPwd]
      ,[CPULoad]
      ,[TotalMemory]
      ,[MemoryUsed]
      ,[PercentMemoryUsed]
      ,[BufferNoMemThisHour]
      ,[BufferNoMemToday]
      ,[BufferSmMissThisHour]
      ,[BufferSmMissToday]
      ,[BufferMdMissThisHour]
      ,[BufferMdMissToday]
      ,[BufferBgMissThisHour]
      ,[BufferBgMissToday]
      ,[BufferLgMissThisHour]
      ,[BufferLgMissToday]
      ,[BufferHgMissThisHour]
      ,[BufferHgMissToday]
      ,[External]
      ,[EntityType]
      ,[CMTS]
      ,[BlockUntil]
      ,[CustomPollerLastStatisticsPoll]
      ,[CustomPollerLastStatisticsPollSuccess]
      ,[City]
      ,[Department]
      ,[Comments]
      ,[IP_Address_Type]
      ,[IPAddressGUID]
      ,[Application_Description]
      ,[Application_Name]
  FROM [SolarWindsOrion].[dbo].[Nodes]