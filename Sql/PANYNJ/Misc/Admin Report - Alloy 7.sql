-- Alloy 7

use Omni

-- Delete temporary table if it exists

if object_id('tempdb..#Results') is not null
    drop table #Results

-- Insert data into temporary table

select d.Name 'MachineName', d.Serial_Num, isnull(replace(UDF_RAW_AMRSUserDepartment, '<Unable to open registry key>', u.PeoplesoftDept), '') Department, 
d.OS_Name, d.OS_Version, a.s 'UserName', s.Status, ug.Accounts, d.Audit_Date
into #Results
from AD7.dbo.Computers d
	inner join AN7.dbo.Computers n on d.Node_ID = n.Import_ID
	inner join AN7.dbo.Status s on s.ID = n.Status_ID
	inner join AD7.dbo.Inv_User_Groups ug on d.Node_ID = ug.Node_ID
	cross apply AD7.dbo.fnSplitString(ug.Accounts, ',') a
	left join dbo.tbADUser u on replace(a.s, '"', '') = 'PANYNJ\' + u.UserName
where n.Type_ID <> '629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3' -- Servers
	and n.Status_ID not in ('3417FB44-7DF7-4C9F-9A25-AAB4CC4C43E6', '89F5D1A2-4B43-4C40-9E71-1A5BE2EB1C3A') -- Retired and Discovery Delete
--	and ug.Name in ('Administrators', 'Power Users')
	and ug.Name in ('Administrators')
	and charindex('PANYNJ\', a.s) > 0 -- Domain Users only
	and datediff(day, d.Audit_Date, SYSDATETIME()) < 90 -- Audited in the last 90 days
	and u.IsEnabled = 1 -- AD enabled users only

-- Windows 7

select MachineName, Serial_Num 'Serial Number', Department, UserName, Status, convert(varchar, Audit_Date, 100) 'AuditDate'
from #Results
where left(OS_Version, 1) = '6'
order by MachineName

-- Windows XP

select MachineName, Serial_Num 'Serial Number', Department, UserName, Status, convert(varchar, Audit_Date, 100) 'AuditDate'
from #Results
where left(OS_Version, 1) = '5'
order by MachineName

-- Computers

select distinct d.Name 'MachineName', ug.Accounts, d.OS_Name 'Operating System', s.Status
from AD7.dbo.Computers d
	inner join AN7.dbo.Computers n on d.Node_ID = n.Import_ID
	inner join AN7.dbo.Status s on s.ID = n.Status_ID
	inner join AD7.dbo.Inv_User_Groups ug on d.Node_ID = ug.Node_ID
where n.Type_ID <> '629F0C1B-0C7B-4E10-AFD9-8D0D5DEA5BA3' -- Servers
	and n.Status_ID not in ('3417FB44-7DF7-4C9F-9A25-AAB4CC4C43E6', '89F5D1A2-4B43-4C40-9E71-1A5BE2EB1C3A') -- Retired and Discovery Delete
	and ug.Name in ('Administrators')
	and datediff(day, d.Audit_Date, SYSDATETIME()) < 90 -- Audited in the last 90 days
order by d.OS_Name, MachineName

--select MachineName, Serial_Num 'Serial Number', Department, UserName, Status, convert(varchar, Audit_Date, 100) 'AuditDate'
--from #Results
--where charindex('eng', Department) > 0