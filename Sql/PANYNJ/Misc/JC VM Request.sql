select HOST, [HOST IP], VM, GUEST_FULL_NAME
from openquery([PATCDV14], 'select i.ID, h.DNS_NAME as HOST, h.IP_ADDRESS as [HOST IP], v.DNS_NAME as [VM DNS NAME], i.NAME as VM, v.IP_ADDRESS as [VM IP], i.GUEST_FULL_NAME from vmwarevcdb.dbo.VPX_VM_CONFIG_INFO i inner join vmwarevcdb.dbo.VPX_VM v on i.ID = v.ID inner join vmwarevcdb.dbo.VPX_HOST h on v.HOST_ID = h.ID where v.POWER_STATE = 1')
union all
select HOST, [HOST IP], VM, GUEST_FULL_NAME
from openquery([TELEDV12], 'select i.ID, h.DNS_NAME as HOST, h.IP_ADDRESS as [HOST IP], v.DNS_NAME as [VM DNS NAME], i.NAME as VM, v.IP_ADDRESS as [VM IP], i.GUEST_FULL_NAME from vmwarevcdb.dbo.VPX_VM_CONFIG_INFO i inner join vmwarevcdb.dbo.VPX_VM v on i.ID = v.ID inner join vmwarevcdb.dbo.VPX_HOST h on v.HOST_ID = h.ID where v.POWER_STATE = 1')
order by HOST, VM