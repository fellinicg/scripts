insert into dbo.tbOracle (Request, ProblemSummary, ItemDescription, SerialNumber, Customer, Alias, Account, [Status], [Date], Severity, RequestType, Urgency, [Contract], Line, ExpectedResolution, [Owner], [Group], CustCountry, Channel, CustomerType, CustomerNumber)
select 
	a.[Request]
	, a.[Problem Summary]
	, a.[Item Description]
	, a.[Serial Number]
	, a.[Customer]
	, a.[Alias]
	, a.[Account]
	, a.[Status]
	, a.[Date]
	, a.[Severity]
	, a.[Request Type]
	, a.[Urgency]
	, a.[Contract]
	, a.[Line]
	, a.[Expected Resolution]
	, a.[Owner]
	, a.[Group]
	, a.[Cust Country]
	, a.[Channel]
	, a.[Customer Type]
	, a.[Customer Number]
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\Oracle.xlsx', 'select * from [Sheet1$]') a
	left join dbo.tbOracle o on a.Request = o.Request
where o.Request is null


