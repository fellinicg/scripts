insert into tbServiceRequest (SRNumber, BusinessUnit, ClientName, ClientNumber, ContractNumber, MultipleWarPartsOrdered, CallType, SRCreationDate, IncidentDate, ItemNumber, ItemDescription, ProblemDescription, StatusCode, SRContact, Age, EquipmentModelNumber, EquipmentSerialNumber, ErrorInfo, CustomerIntRefNumber, ContactDate, ContactTime, NotifyDate, NotifyTime, NotifyToRestore, RepairDesc, TaskNumber, TaskName, TaskAssignee, Region, TaskOwner, TaskStatus)
select 
	a.[SR Number]
	, a.[Business unit]
	, a.[Client Name]
	, a.[Client Number]
	, a.[Contract Number]
	, a.[Multiple War Parts Ordered]
	, a.[Call Type]
	, a.[SR Creation Date]
	, a.[Incident Date]
	, a.[Item Number]
	, a.[Item Description]
	, a.[Problem Description]
	, a.[Status Code]
	, a.[SR Contact]
	, a.[Age]
	, a.[Equipment Model Number]
	, a.[Equipment Serial Number]
	, a.[Error Info]
	, a.[Customer Int Ref Number]
	, a.[Contact Date]
	, a.[Contact Time]
	, a.[Notify Date]
	, a.[Notify Time]
	, a.[Notify To Restore]
	, a.[Repair Desc]
	, a.[Task Number]
	, a.[Task Name]
	, a.[Task Assignee]
	, a.[Region]
	, a.[Task Owner]
	, a.[Task Status]
from openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=C:\Feeds\sr.xlsx', 'select * from [ALL OPEN SR''S$] where [Client Number] = 1672') a
	left join dbo.tbServiceRequest sr on a.[SR Number] = sr.SRNumber and a.[Task Number] = sr.TaskNumber
where sr.SRNumber is null


