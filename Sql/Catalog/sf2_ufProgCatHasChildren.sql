SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: September 20, 2005
-- Description:	Returns whether the program category has children
-- =============================================
CREATE FUNCTION dbo.sf2_ufProgCatHasChildren 
(
	@pct_p_ID INT,
	@pct_Parent INT
)
RETURNS INT
AS
BEGIN

	DECLARE @Result INT

	SELECT @Result = CASE COUNT(pct_ID) WHEN 0 THEN 0 ELSE 1 END
	FROM ProgramCategories
	WHERE pct_p_ID = @pct_p_ID
	AND pct_Parent = @pct_Parent
	AND pct_rs_RecStatus = 0

	RETURN @Result

END
GO

