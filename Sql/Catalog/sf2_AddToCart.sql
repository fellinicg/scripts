USE [Fulfilment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  PROCEDURE sf2_AddToCart
@p_ID INT,
@u_ID VARCHAR(100),
@pc_ID INT,
@Qty INT,
@Options VARCHAR(500)

AS

DECLARE @ProgCode VARCHAR(100)
DECLARE @CartID INT

/* Get program code from catalog database. */
SELECT @ProgCode = p_Code FROM Catalog.dbo.Programs WHERE p_ID = @p_ID

/* Insert info into shopping cart table. */
INSERT INTO dbo.ShoppingCart (ProgramID, UserID, AwardID, Quantity, ProgramCode)
VALUES (@p_ID, @u_ID, @pc_ID, @Qty, @ProgCode)

SET @CartID = @@IDENTITY

/*If there are options, insert into shopping cart options table. */
INSERT INTO dbo.ShoppingCartOptions (CartID, OptionValue)
SELECT @CartID, * FROM dbo.Split(@Options, ',')
