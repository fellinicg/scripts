USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE dbo.sf2_GetItemDetail
(
	@pc_ID VARCHAR(15), 
	@p_ID INT
)
AS

	DECLARE @RoundFunction VARCHAR(50)
	DECLARE @MerchCalc VARCHAR(500)
	DECLARE @CardCalc VARCHAR(500)
	DECLARE @TravelCalc VARCHAR(500)
	DECLARE @OptCost VARCHAR(5000)
	DECLARE @MerchCalcOpt VARCHAR(500)
	DECLARE @TravelCalcOpt VARCHAR(500)
	DECLARE @CardCalcOpt VARCHAR(500)
	DECLARE @today DATETIME

	-- Set date variable
	SELECT @today = GETDATE()

	-- Set variables for which price we should markup on.
	SELECT
	@RoundFunction = RoundFunction,
	@MerchCalc = Merch,
	@TravelCalc = Travel,
	@CardCalc = GiftCard,
	@MerchCalcOpt = MerchOpt,
	@TravelCalcOpt = TravelOpt,
	@CardCalcOpt = GiftCardOpt
	FROM dbo.sf2_ufPriceCalcWith2Decimal(@p_ID, @today)

	-- First result set -- Item information.
	EXEC('SELECT pc_ID, mc_ID, mc_SDesc,  mc_Desc, cd_ImageURL + mc_Image AS ImagePath, cd_ThumbnailURL + mc_Image AS ThumdnailPath,
	CASE mc_it_ID WHEN 3 THEN ' + @RoundFunction + @MerchCalc + '
	WHEN 4 THEN ' + @RoundFunction + @TravelCalc + '
	ELSE ' + @RoundFunction + @CardCalc +' END AS Price
	FROM Programs
	INNER JOIN ProgramCatalogs ON p_ID = pc_p_ID
	INNER JOIN MasterCatalog ON pc_mc_ID = mc_ID
	INNER JOIN ItemPricing ON mc_ip_ID = ip_ID
	LEFT JOIN ItemShippingInfo ON mc_isi_ID = isi_ID AND p_IncludeShipping = 1
	LEFT JOIN SalesTax ON GETDATE() BETWEEN slt_StartDate AND slt_EndDate AND p_IncludeTax = 1
	LEFT JOIN ProgramMarkups ON p_ID = pm_p_ID AND GETDATE() BETWEEN pm_StartDate AND pm_EndDate
	LEFT JOIN ProgramItemMarkup ON pc_ID = pim_pc_ID AND GETDATE() BETWEEN pim_StartDate AND pim_EndDate
	LEFT JOIN CatalogDefaults ON cd_Type = ''P''
	WHERE pc_ID = ' + @pc_ID + '
	AND p_ID = ' + @p_ID + '
	AND GETDATE() BETWEEN pc_StartDate AND pc_EndDate
	AND mc_rs_RecStatus IN (0,1)')

	-- Second result set -- Option Description information.
	SELECT DISTINCT io_ot_ID, 
	CASE io_ot_ID WHEN 53 THEN o_Desc ELSE ot_Desc END 'ot_Desc'
	FROM dbo.ProgramCatalogs
	INNER JOIN ItemOptions ON pc_mc_ID = io_mc_ID
	INNER JOIN OptionTypes ON io_ot_ID = ot_ID
	INNER JOIN Options ON io_o_ID = o_ID
	WHERE pc_ID = @pc_ID
	AND io_rs_RecStatus = 0

	-- Third result set -- Item Option information.
	IF EXISTS (SELECT 1 FROM MasterCatalog
		INNER JOIN ProgramCatalogs ON mc_ID = pc_mc_ID  
		INNER JOIN ItemOptions ON mc_ID = io_mc_ID AND io_rs_Recstatus = 0
		INNER JOIN ItemOptionCost ON io_ID = ioc_io_ID AND ioc_StartDate < GETDATE() AND ioc_EndDate > GETDATE()  
		WHERE pc_ID = @pc_ID)
	 BEGIN
		SET @OptCost = 'CASE mc_it_ID WHEN 3 THEN ' + @RoundFunction + @MerchCalcOpt + '
			WHEN 4 THEN ' + @RoundFunction + @TravelCalcOpt + '
			ELSE ' + @RoundFunction + @CardCalcOpt +' END AS Price'
	 END	
	ELSE SET @OptCost = 'ioc_Cost AS Price'

	EXEC('SELECT io_ID, ot_ID, ot_Desc, o_ID, o_Desc, io_Model, ' + 
	@OptCost + '
	FROM Programs
	INNER JOIN ProgramCatalogs ON p_ID = pc_p_ID
	INNER JOIN MasterCatalog ON pc_mc_ID = mc_ID
	LEFT JOIN ItemShippingInfo ON mc_isi_ID = isi_ID AND p_IncludeShipping = 1
	LEFT JOIN SalesTax ON GETDATE() BETWEEN slt_StartDate AND slt_EndDate AND p_IncludeTax = 1
	LEFT JOIN ProgramMarkups ON p_ID = pm_p_ID AND GETDATE() BETWEEN pm_StartDate AND pm_EndDate
	LEFT JOIN ProgramItemMarkup ON pc_ID = pim_pc_ID AND GETDATE() BETWEEN pim_StartDate AND pim_EndDate
	LEFT JOIN ItemOptions ON mc_ID = io_mc_ID AND io_rs_Recstatus = 0
	LEFT JOIN ItemOptionCost ON io_ID = ioc_io_ID AND ioc_StartDate < GETDATE() AND ioc_EndDate > GETDATE() 
	LEFT JOIN Options ON io_o_ID = o_ID
	LEFT JOIN dbo.OptionTypes ON io_ot_ID = ot_ID
	WHERE pc_ID = ' + @pc_ID + '
	AND io_ID IS NOT NULL
	ORDER BY ot_Desc, Price, o_Desc')

	-- Fourth result set -- Option Type information.
	SELECT DISTINCT io_ot_ID, ot_Desc
	FROM dbo.ProgramCatalogs
	INNER JOIN ItemOptions ON pc_mc_ID = io_mc_ID
	INNER JOIN OptionTypes ON io_ot_ID = ot_ID
	WHERE pc_ID = @pc_ID
	AND io_rs_RecStatus = 0

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF