USE Catalog
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: November 22, 2005
-- Description:	Retrieve project manager's email address for the specified program
-- =============================================
ALTER PROCEDURE dbo.sf2_GetPMEmail 
	@ProgramID INT,
  @ProgDesc VARCHAR(100) OUTPUT,
  @Email VARCHAR(50) OUTPUT

AS
BEGIN

	SET NOCOUNT ON

	-- Retrieve work email address, use Larry Silverstein as a default.
	--
	SELECT @ProgDesc = ISNULL(p_SDesc, 'UNKNOWN(' + CONVERT(VARCHAR, p_ID) + ')'), @Email = em_Address
	FROM dbo.Clients
	LEFT JOIN dbo.Programs ON p_ID = @ProgramID
	LEFT JOIN GlobalUser.dbo.Email ON ISNULL(p_gu_ID, 54) = em_gu_ID AND em_ct_ID = 1
	WHERE cl_ID = 5
		
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
