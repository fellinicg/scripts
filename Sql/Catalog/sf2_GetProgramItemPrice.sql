USE [Catalog]
GO
/****** Object:  StoredProcedure [dbo].[sf2_GetProgramItemPrice]    Script Date: 08/24/2005 13:10:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Todd Fellini
-- Create date: August 22, 2005
-- Description:	Returns price of items for a given program
-- =============================================
ALTER PROCEDURE [dbo].[sf2_GetProgramItemPrice] 
	@pc_ID INT, 
	@p_ID INT

AS
DECLARE @RoundFunction VARCHAR(50)
DECLARE @MerchCalc VARCHAR(500)
DECLARE @CardCalc VARCHAR(500)
DECLARE @TravelCalc VARCHAR(500)

SET NOCOUNT ON

/* Set variable for which price we should markup on. */
EXEC dbo.sf2_GetProgramPricingInfo 
	@pm_p_ID = @p_ID, 
	@RoundFunction = @RoundFunction OUTPUT, 
	@MerchCalc = @MerchCalc OUTPUT, 
	@CardCalc = @CardCalc OUTPUT, 
	@TravelCalc = @TravelCalc OUTPUT

EXEC('SELECT 
CASE mc_it_ID WHEN 3 THEN ' + @RoundFunction + @MerchCalc + '
WHEN 4 THEN ' + @RoundFunction + @TravelCalc + '
ELSE ' + @RoundFunction + @CardCalc +' END AS Price
FROM Programs
INNER JOIN ProgramCatalogs ON p_ID = pc_p_ID
INNER JOIN MasterCatalog ON pc_mc_ID = mc_ID
INNER JOIN ItemPricing ON mc_ip_ID = ip_ID
LEFT JOIN ItemShippingInfo ON mc_isi_ID = isi_ID AND p_IncludeShipping = 1
LEFT JOIN SalesTax ON GETDATE() BETWEEN slt_StartDate AND slt_EndDate AND p_IncludeTax = 1
LEFT JOIN ProgramMarkups ON p_ID = pm_p_ID AND GETDATE() BETWEEN pm_StartDate AND pm_EndDate
LEFT JOIN ProgramItemMarkup ON pc_ID = pim_pc_ID AND GETDATE() BETWEEN pim_StartDate AND pim_EndDate
WHERE pc_ID = ' + @pc_ID + '
AND p_ID = ' + @p_ID)

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF