USE [Catalog]
GO
/****** Object:  StoredProcedure [dbo].[sf2_GetProgramPricingInfo]    Script Date: 08/24/2005 13:11:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Todd Fellini
-- Create date: August 24, 2005
-- Description:	Returns pricing info for a specific program catalog
-- =============================================
CREATE PROCEDURE [dbo].[sf2_GetProgramPricingInfo] 
	@pm_p_ID INT, 
	@RoundFunction VARCHAR(50) OUT,
	@MerchCalc VARCHAR(500) OUT,
	@CardCalc VARCHAR(500) OUT,
	@TravelCalc VARCHAR(500) OUT
AS
BEGIN

	DECLARE @MarkUpOn CHAR(1)
	DECLARE @Price VARCHAR(20)
	DECLARE @p_GC_markup_flag bit

	SET NOCOUNT ON;

	SELECT @MarkUpOn = ISNULL(pm_MarkUpOn, 'C'), @RoundFunction = ISNULL( pm_AddlFunction, '')
	FROM ProgramMarkups
	WHERE pm_p_ID = @pm_p_ID
	AND (GETDATE() BETWEEN pm_StartDate AND pm_EndDate OR pm_StartDate IS NULL)

	IF ISNULL(@MarkUpOn, 'C') = 'C' SET @Price = 'ip_Cost'  ELSE SET @Price = 'ip_MSRP'

	SET @p_GC_markup_flag = (SELECT p_GC_markup_flag 
					FROM dbo.Programs
					WHERE p_ID = @pm_p_ID)

	EXEC MerchCalcWith2Decimal @Price, 'M', @CalcText = @MerchCalc OUTPUT
	EXEC MerchCalcWith2Decimal @Price, 'G', @p_GC_markup_flag, @CalcText = @CardCalc OUTPUT
	EXEC MerchCalcWith2Decimal @Price, 'T', @CalcText = @TravelCalc OUTPUT

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF