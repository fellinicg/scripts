USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Todd Fellini
-- Create date:	August 31, 2005
-- Description:	Returns all items in a persons shopping cart for a given program
-- =============================================
ALTER PROCEDURE dbo.sf2_GetCartItems
(
	@pc_p_ID	varchar(100),
	@u_ID     varchar(50)
)
AS

	DECLARE @RoundFunction VARCHAR(50)
	DECLARE @Where VARCHAR(5000)
	DECLARE @MerchCalc VARCHAR(500)
	DECLARE @TravelCalc VARCHAR(500)
	DECLARE @CardCalc VARCHAR(500)
	DECLARE @MerchCalcOpt VARCHAR(500)
	DECLARE @TravelCalcOpt VARCHAR(500)
	DECLARE @CardCalcOpt VARCHAR(500)
	DECLARE @today DATETIME


	SET NOCOUNT ON

	-- Initialize variables.
	SET @Where = ''
	SELECT @today = GETDATE()

	-- Set variables for which price we should markup on.
	SELECT
	@RoundFunction = RoundFunction,
	@MerchCalc = Merch,
	@TravelCalc = Travel,
	@CardCalc = GiftCard,
	@MerchCalcOpt = MerchOpt,
	@TravelCalcOpt = TravelOpt,
	@CardCalcOpt = GiftCardOpt
	FROM dbo.sf2_ufPriceCalcWith2Decimal(@pc_p_ID, @today)

	-- Execute first query for number of options per cart item.
	SELECT DISTINCT CartID, HasOptions
	FROM dbo.sf2_ufGetCart(@pc_p_ID, @u_ID)
	INNER JOIN dbo.ProgramCatalogs ON ProgramID = pc_p_ID AND AwardID = pc_ID
	WHERE GETDATE() BETWEEN pc_StartDate AND pc_EndDate
	ORDER BY CartID

	-- Execute query to return cart info.
	EXEC ('SELECT CartID, pc_ID, mc_ID, mc_SDesc,  mc_Desc,
	ISNULL(
	CASE mc_it_ID WHEN 3 THEN ' + @RoundFunction + @MerchCalcOpt + '
	WHEN 4 THEN ' + @RoundFunction + @TravelCalcOpt + '
	ELSE ' + @RoundFunction + @CardCalcOpt +' END,
	CASE mc_it_ID WHEN 3 THEN ' + @RoundFunction + @MerchCalc + '
	WHEN 4 THEN ' + @RoundFunction + @TravelCalc + '
	ELSE ' + @RoundFunction + @CardCalc +' END
	) AS Price,
	Quantity,
	p_AllowShopping,
	cd_ThumbNailURL + mc_Image AS "ThumbnailImage",
	cd_ImageURL + mc_Image AS "Image",
	p_MinPoint,
	p_MaxPoint,
	CASE ot_ID WHEN 53 THEN OptionText ELSE o_Desc END o_Desc,
	CASE ot_ID WHEN 53 THEN o_Desc ELSE ot_Desc END ot_Desc,
	io_ID,
	io_ot_ID,
	mc_it_ID,
	mc_v_number
	FROM dbo.sf2_ufGetCart(' + @pc_p_ID + ','  + @u_ID + ')
	INNER JOIN dbo.Programs ON ProgramID = p_ID
	INNER JOIN dbo.ProgramCatalogs ON p_ID = pc_p_ID AND AwardID = pc_ID
	INNER JOIN dbo.MasterCatalog ON pc_mc_ID = mc_ID
	INNER JOIN dbo.ItemPricing ON mc_ip_ID = ip_ID
	LEFT JOIN dbo.ItemShippingInfo ON mc_isi_ID = isi_ID AND p_IncludeShipping = 1
	LEFT JOIN dbo.SalesTax ON GETDATE() BETWEEN slt_StartDate AND slt_EndDate AND p_IncludeTax = 1
	LEFT JOIN dbo.ProgramMarkups ON p_ID = pm_p_ID AND GETDATE() BETWEEN pm_StartDate AND pm_EndDate
	LEFT JOIN dbo.CatalogDefaults ON cd_Type = ''P''
	LEFT JOIN dbo.ProgramItemMarkup ON pc_ID = pim_pc_ID AND GETDATE() BETWEEN pim_StartDate AND pim_EndDate
	LEFT JOIN dbo.ItemOptions ON io_ID = OptionValue
	LEFT JOIN dbo.ItemOptionCost ON io_ID = ioc_io_ID
	LEFT JOIN dbo.Options ON io_o_ID = o_ID
	LEFT JOIN dbo.OptionTypes ON io_ot_ID = ot_ID
	WHERE GETDATE() BETWEEN pc_StartDate AND pc_EndDate
	AND ((' + @RoundFunction + @MerchCalc + ' BETWEEN ISNULL(p_MinPoint, 0) AND ISNULL(p_MaxPoint, 1000000) AND mc_it_ID = 3) OR
			(' + @RoundFunction + @TravelCalc + ' BETWEEN ISNULL(p_MinPoint, 0) AND ISNULL(p_MaxPoint, 1000000) AND mc_it_ID = 4) OR
			(' + @RoundFunction + @CardCalc + ' BETWEEN ISNULL(p_MinPoint, 0) AND ISNULL(p_MaxPoint, 1000000) AND mc_it_ID IN (1,2)) OR
			(' + @RoundFunction + @MerchCalcOpt + ' BETWEEN ISNULL(p_MinPoint, 0) AND ISNULL(p_MaxPoint, 1000000) AND mc_it_ID = 3) OR
			(' + @RoundFunction + @TravelCalcOpt + ' BETWEEN ISNULL(p_MinPoint, 0) AND ISNULL(p_MaxPoint, 1000000) AND mc_it_ID = 4) OR
			(' + @RoundFunction + @CardCalcOpt + ' BETWEEN ISNULL(p_MinPoint, 0) AND ISNULL(p_MaxPoint, 1000000) AND mc_it_ID IN (1,2)))
	AND mc_rs_RecStatus IN (0)
	AND ISNULL(io_rs_RecStatus, 0) IN (0) ' + @Where + ' ORDER BY CartID')

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF