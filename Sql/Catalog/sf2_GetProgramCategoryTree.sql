SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: September, 21, 2005
-- Description:	Return program category tree
-- =============================================
ALTER PROCEDURE dbo.sf2_GetProgramCategoryTree
	@pct_p_ID INT
AS
BEGIN

	DECLARE @count int
	DECLARE @max int

	SET NOCOUNT ON

	-- Create temporary table
	CREATE TABLE #Temp(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[PctID] [int] NOT NULL,
		[Description] [varchar](50) NOT NULL,
		[Type] [char](1) NOT NULL,
		[c_ID] [int] NOT NULL,
		[Parent] [int] NOT NULL,
		[SortOrd] [int] NULL,
		[ParentPctID] [int] NOT NULL DEFAULT (0),
		[Level] [int] NULL,
		[HasChildren] [bit] NOT NULL DEFAULT (1))

	-- Populate temporary table
	INSERT INTO #Temp (PctID, Description, Type, c_ID, Parent, SortOrd)
	SELECT pct_ID, c_SDesc, c_Type, pct_c_ID, pct_Parent, pct_SortOrd
	FROM ProgramCategories
	INNER JOIN Categories ON pct_c_ID = c_ID
	WHERE pct_p_ID = @pct_p_ID
	AND pct_rs_RecStatus = 0
	ORDER BY c_Type DESC, pct_SortOrd

	-- Populate local variables for loop
	SELECT @count = MIN(ID) FROM #Temp
	SELECT @max = MAX(ID) FROM #Temp

	-- Loop over temporary table and populate info
	WHILE @count <= @max 
	BEGIN
			-- Set Level to 1 where Parent field is 0
			IF (SELECT Parent FROM #Temp WHERE ID = @count) = 0
				UPDATE #Temp SET Level = 1 WHERE ID = @count

			-- Set ParentPctID for every one else
			ELSE
				UPDATE #Temp SET ParentPctID = (SELECT MAX(b.PctID)
																				FROM #Temp 
																				INNER JOIN #Temp b ON #Temp.Parent = b.c_ID AND b.PctId < #Temp.PctID
																				WHERE #Temp.ID = @count)
				WHERE #Temp.ID = @count
			SET @count = @count + 1
	END

	-- Loop over temporary table and populate Level until there are no more Nulls in Level field
	WHILE (EXISTS (SELECT 1 FROM #Temp WHERE Level IS NULL))
		BEGIN
			UPDATE #Temp SET Level = parent.Level + 1
			FROM #Temp
			INNER JOIN #Temp parent ON (#Temp.ParentPctID = parent.PctID AND parent.Level IS NOT NULL)
			WHERE #Temp.PctID > parent.PctID
			AND #Temp.Level IS NULL
		END

	-- Set Has Children field to 0 for the lowest level in each branch
	UPDATE #Temp SET HasChildren = 0
	FROM #Temp
	LEFT JOIN #Temp a ON #Temp.PctID = a.ParentPctID
	WHERE a.ID IS NULL

	-- Return result set
	SELECT *
	FROM #Temp

	-- Clean up
	DROP TABLE #Temp

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
