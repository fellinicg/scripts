USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Todd Fellini
-- Create date:	October 3, 2006
-- Description:	Returns a persons current AMEX point balance for a given program
-- =============================================
ALTER PROCEDURE dbo.sf2_GetAMEXBalance
(
	@ProgramID int,
	@UserID varchar(50),
	@AMEXBalance int OUTPUT
)

AS

	DECLARE @Year char(4)
	DECLARE @IsSuccess bit

	SET NOCOUNT ON

	-- Initialize variables
	SET @Year = YEAR(CURRENT_TIMESTAMP)
	SET @AMEXBalance = 0

	-- PfizerACE 
	IF (@ProgramID = 182)
		BEGIN
			EXECUTE SQL2KPRODZEBRA.pfizer.dbo.upGetAMEXBalanceByYear
				 @UserID
				,@Year
				,@AMEXBalance OUTPUT
				,@IsSuccess OUTPUT

			-- Prevent further execution
			RETURN
		END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
