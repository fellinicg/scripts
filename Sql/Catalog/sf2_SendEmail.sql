CREATE PROCEDURE dbo.sf2_SendEmail
  @MessageFrom varchar(500),
  @MessageTo varchar(500),
  @MessageSubject varchar(120),
  @MessageBody varchar(7000),
  @SendStatus char(1) OUTPUT
AS
DECLARE
  @IsError bit,
  @SMTPErrorStatus int

  SET NOCOUNT ON
  
  SET @SendStatus = 'F'  -- failure
  
  -- send message
  EXEC @SMTPErrorStatus = [MAIL.MADISONPG.COM].master.dbo.xp_smtp_sendmail
    @FROM = @MessageFrom,
    @FROM_NAME = '',
    @replyto = '',
    @TO = @MessageTo,
    @BCC = '',
    @priority = 'HIGH',
    @subject = @MessageSubject,
    @type = 'text/plain',
    @message = @MessageBody,
    @server = 'mail.madisonpg.com'
  IF @SMTPErrorStatus = 0
    SET @SendStatus = 'S'
  
  SET NOCOUNT OFF

GO
