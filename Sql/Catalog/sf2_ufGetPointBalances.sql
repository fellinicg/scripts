USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION dbo.sf2_ufGetPointBalances 
(	
	@Userid VARCHAR(20)
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT p_UserID, p_ProgramID, p_ClientID, p_DK, SUM(p_Points) 'TotalEarnedPoints'
	FROM Moonraker.Points.dbo.Points
	WHERE p_UserID = @Userid
	AND p_Status = 0
	AND ISNULL(p_Description, '') NOT LIKE '%CASH%'
	GROUP BY p_UserID, p_ProgramID, p_ClientID, p_DK
)

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF