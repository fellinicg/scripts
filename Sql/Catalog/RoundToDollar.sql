SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: September 8, 2005
-- Description:	Round to up to dollar
-- =============================================
CREATE FUNCTION dbo.RoundToDollar 
(
	@Value INT
)
RETURNS int
AS
BEGIN
	DECLARE @Result int

	SELECT @Result = CEILING(@Value)

	RETURN @Result

END
GO

