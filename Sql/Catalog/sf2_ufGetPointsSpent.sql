USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION dbo.sf2_ufGetPointsSpent 
(	
	@Userid VARCHAR(20)
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT UserID, ProjectNumber, SUM(TotalSpentPoints) 'TotalSpentPoints'
	FROM (
				SELECT oh.UserID, oh.ProjectNumber, SUM(TotalPoints) 'TotalSpentPoints'
				FROM CURLY.Fulfilment.dbo.OrderHeader oh
				INNER JOIN CURLY.Fulfilment.dbo.AwardLine al ON oh.OrderID = al.OrderID
				WHERE oh.UserID = @Userid
				AND al.Void = 0
				GROUP BY oh.UserID, oh.ProjectNumber
				UNION
				SELECT pp.UserID, p.p_DKNo, SUM(pp.PurchasedPoints * -1)
				FROM CURLY.Fulfilment.dbo.tbPurchasePointOrders pp
				INNER JOIN dbo.Programs p ON pp.ProgCode = p.p_Code
				WHERE pp.UserID = @Userid
				GROUP BY pp.UserID, p.p_DKNo
				) t
	GROUP BY UserID, ProjectNumber
)
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF