USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Todd Fellini
-- Create date:	October 12, 2006
-- Description:	Records AMEX transaction
-- =============================================
ALTER PROCEDURE dbo.sf2_RecordAMEXTran
(
	@ProgramID int,
	@UserID varchar(50),
	@AMEXAmount int,
	@TranID int OUTPUT
)

AS

	DECLARE @Year char(4)
	DECLARE @IsSuccess bit
	DECLARE @TranDateTime datetime

	SET NOCOUNT ON

	-- Initialize variables
	SET @Year = YEAR(CURRENT_TIMESTAMP)
	SET @TranID = 0
	SET @TranDateTime = CURRENT_TIMESTAMP

	-- PfizerACE 
	IF (@ProgramID = 182)
		BEGIN
			EXECUTE SQL2KPRODZEBRA.pfizer.dbo.upInsertEmployeeAMEXTran
				 @UserID
				,@AMEXAmount
				,@Year
				,@TranDateTime
				,@TranID OUTPUT
				,@IsSuccess OUTPUT

			-- Prevent further execution
			RETURN
		END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
