USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Todd Fellini
-- Create date:	September 6, 2005
-- Description:	Returns all items in a persons shopping cart for a given program for checkout
-- =============================================
ALTER PROCEDURE dbo.sf2_GetCartItemsForCheckout
(
	@pc_p_ID	varchar(100),
	@u_ID     varchar(50),
	@token		varchar(50) OUTPUT
)
AS

	DECLARE @RoundFunction VARCHAR(50)
	DECLARE @Where VARCHAR(5000)
	DECLARE @MerchCalc VARCHAR(500)
	DECLARE @TravelCalc VARCHAR(500)
	DECLARE @CardCalc VARCHAR(500)
	DECLARE @MerchCalcOpt VARCHAR(500)
	DECLARE @TravelCalcOpt VARCHAR(500)
	DECLARE @CardCalcOpt VARCHAR(500)
	DECLARE @today DATETIME
	DECLARE @rc TINYINT

	SET NOCOUNT ON

	-- Initialize variables
	SET @Where = ''
	SET @today = GETDATE()
	SET @token = NEWID()

	-- Get program specific functions
	SELECT
	@RoundFunction = RoundFunction,
	@MerchCalc = Merch,
	@TravelCalc = Travel,
	@CardCalc = GiftCard,
	@MerchCalcOpt = MerchOpt,
	@TravelCalcOpt = TravelOpt,
	@CardCalcOpt = GiftCardOpt
	FROM dbo.sf2_ufPriceCalcWith2Decimal(@pc_p_ID, @today)

	-- Get any items that might be packages
	SELECT CartID 'Cart_ID', COUNT(pk_ID) 'ItemsInPkg'
	INTO #TEMP
	FROM dbo.sf2_ufGetCart(@pc_p_ID, @u_ID)
	INNER JOIN ProgramCatalogs ON AwardID = pc_ID
	INNER JOIN Packages ON  pc_mc_ID = pk_mc_ID
	WHERE GETDATE() BETWEEN ISNULL(pk_StartDate, '1/1/1990') AND ISNULL(pk_EndDate, '1/1/2070')
	GROUP BY CartID

	-- Insert information into table in fulfilment for further processing
	EXEC ('INSERT INTO CURLY.Fulfilment.dbo.ShoppingCartCheckout
	(CartID, ProgramItemId, MasterCatalogId, ItemDesc, ItemType, 
	VendorNum, Model, Price, Cost, Shipping, TaxRate, ShipFlag, 
	ItemsInPkg, Token)
	SELECT DISTINCT CartID, pc_ID, mc_ID, mc_SDesc, it_SDesc,
	mc_v_Number, mc_Model,
	ISNULL(CASE mc_it_ID WHEN 3 THEN ' + @RoundFunction + @MerchCalcOpt + '
	WHEN 4 THEN ' + @RoundFunction + @TravelCalcOpt + '
	ELSE ' + @RoundFunction + @CardCalcOpt +' END,
	CASE mc_it_ID WHEN 3 THEN ' + @RoundFunction + @MerchCalc + '
	WHEN 4 THEN ' + @RoundFunction + @TravelCalc + '
	ELSE ' + @RoundFunction + @CardCalc +' END) AS Price,
	ISNULL(ioc_Cost, ip_Cost) AS Cost, 
	sh.ShipCost, 
	st.Rate, 
	sh.ShipFlag,
	ISNULL(ItemsInPkg, 0), ''' + @token + '''
	FROM dbo.sf2_ufGetCart(' + @pc_p_ID + ','  + @u_ID + ')
	INNER JOIN dbo.Programs ON ProgramID = p_ID
	INNER JOIN dbo.ProgramCatalogs ON p_ID = pc_p_ID AND AwardID = pc_ID
	INNER JOIN dbo.MasterCatalog ON pc_mc_ID = mc_ID
	INNER JOIN dbo.ItemPricing ON mc_ip_ID = ip_ID
	INNER JOIN dbo.ItemTypes ON mc_it_ID = it_ID
	LEFT JOIN dbo.ItemShippingInfo ON mc_isi_ID = isi_ID AND p_IncludeShipping = 1
	LEFT JOIN (SELECT isi_ID AS isiID, isi_ShipCost AS ShipCost, isi_ShipFlag AS ShipFlag FROM dbo.ItemShippingInfo) sh ON mc_isi_ID = isiID
	LEFT JOIN dbo.SalesTax ON GETDATE() BETWEEN slt_StartDate AND slt_EndDate AND p_IncludeTax = 1
	LEFT JOIN (SELECT slt_Rate AS Rate FROM dbo.SalesTax WHERE GETDATE() BETWEEN slt_StartDate AND slt_EndDate) st ON 1 = 1
	LEFT JOIN dbo.ProgramMarkups ON p_ID = pm_p_ID AND GETDATE() BETWEEN pm_StartDate AND pm_EndDate
	LEFT JOIN dbo.CatalogDefaults ON cd_Type = ''P''
	LEFT JOIN dbo.ProgramItemMarkup ON pc_ID = pim_pc_ID AND GETDATE() BETWEEN pim_StartDate AND pim_EndDate
	LEFT JOIN dbo.ItemOptions ON io_ID = OptionValue
	LEFT JOIN dbo.ItemOptionCost ON io_ID = ioc_io_ID
	LEFT JOIN dbo.Options ON io_o_ID = o_ID
	LEFT JOIN dbo.OptionTypes ON io_ot_ID = ot_ID
	LEFT JOIN #TEMP ON CartID = Cart_ID
	WHERE GETDATE() BETWEEN pc_StartDate AND pc_EndDate
	AND ((' + @RoundFunction + @MerchCalc + ' BETWEEN ISNULL(p_MinPoint, 0) AND ISNULL(p_MaxPoint, 1000000) AND mc_it_ID = 3) OR
			(' + @RoundFunction + @TravelCalc + ' BETWEEN ISNULL(p_MinPoint, 0) AND ISNULL(p_MaxPoint, 1000000) AND mc_it_ID = 4) OR
			(' + @RoundFunction + @CardCalc + ' BETWEEN ISNULL(p_MinPoint, 0) AND ISNULL(p_MaxPoint, 1000000) AND mc_it_ID IN (1,2)) OR
			(' + @RoundFunction + @MerchCalcOpt + ' BETWEEN ISNULL(p_MinPoint, 0) AND ISNULL(p_MaxPoint, 1000000) AND mc_it_ID = 3) OR
			(' + @RoundFunction + @TravelCalcOpt + ' BETWEEN ISNULL(p_MinPoint, 0) AND ISNULL(p_MaxPoint, 1000000) AND mc_it_ID = 4) OR
			(' + @RoundFunction + @CardCalcOpt + ' BETWEEN ISNULL(p_MinPoint, 0) AND ISNULL(p_MaxPoint, 1000000) AND mc_it_ID IN (1,2)))
	AND mc_rs_RecStatus IN (0)
	AND ISNULL(io_rs_RecStatus, 0) IN (0) ' + @Where + ' ORDER BY CartID')

	IF @@ROWCOUNT = 0 OR @@ERROR <> 0
		BEGIN
			SET @rc = 0
			SET @token = 0
		END
	ELSE SET @rc = 1

	DROP TABLE #TEMP

	RETURN @rc

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF