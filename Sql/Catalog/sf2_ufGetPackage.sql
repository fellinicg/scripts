SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: September 7, 2005
-- Description:	Returns package items
-- =============================================
CREATE FUNCTION dbo.sf2_ufGetPackage
(
	@timestamp DATETIME
)
RETURNS 
@Results TABLE 
(
	pc_mc_ID INT, 
	it_SDesc CHAR(1), 
	mc_ID INT, 
	mc_v_Number NVARCHAR(6), 
	mc_SDesc NVARCHAR(100), 
	mc_Model NVARCHAR(100), 
	ip_Cost SMALLMONEY,
	isi_ShipCost SMALLMONEY, 
	isi_ShipFlag CHAR(1)
)
AS
BEGIN

	INSERT INTO @Results
	SELECT pk_mc_ID, it_SDesc, mc_ID, mc_v_Number, mc_SDesc, mc_Model, ip_Cost, isi_ShipCost, isi_ShipFlag
	FROM dbo.Packages
	INNER JOIN dbo.MasterCatalog ON pk_Item_ID = mc_ID
	INNER JOIN dbo.ItemPricing ON mc_ip_ID = ip_ID
	INNER JOIN dbo.ItemTypes ON mc_it_ID = it_ID
	LEFT JOIN dbo.ItemShippingInfo ON mc_isi_ID = isi_ID
	WHERE @timestamp BETWEEN pk_StartDate AND pk_EndDate

	RETURN

END
GO