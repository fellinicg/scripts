USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Todd Fellini
-- Create date:	May 16, 2006
-- Description:	Returns a persons current available point balance for a given program
-- =============================================
ALTER PROCEDURE dbo.sf2_GetAvailableBalance
(
	@ProgramID integer,
	@UserID varchar(50),
	@PointsTable varchar(40)
)

AS

	DECLARE @ClientID int
	DECLARE @UseClientID bit

	SET NOCOUNT ON

	-- Initialize variables
	SET @PointsTable = UPPER(@PointsTable)

	-- Test whether to sum points by client or program
	SELECT @UseClientID = ISNULL(p_UseClientID, 0), @ClientID = ISNULL(p_cl_ID, 0)
	FROM dbo.Programs
	WHERE p_ID = @ProgramID

	-- Points
	IF (@PointsTable = 'POINTS')
		BEGIN
			IF (@UseClientID = 1)
				SELECT 
					p_UserID, 
					'N/A' p_programID, 
					p_ClientID,
					'N/A' p_DK, 
					SUM(ISNULL(TotalEarnedPoints, 0)) 'TotalEarnedPoints', 
					SUM(ISNULL(TotalSpentPoints, 0)) 'TotalSpentPoints', 
					SUM((ISNULL(TotalEarnedPoints, 0) - ISNULL(TotalSpentPoints, 0))) 'TotalAvailableBalanace'
				FROM dbo.sf2_ufGetPointBalances(@UserID)
				LEFT JOIN dbo.sf2_ufGetPointsSpent(@UserID) ON p_DK = ProjectNumber
				WHERE p_ClientID = @ClientID
				GROUP BY p_UserID, p_ClientID
			ELSE
				SELECT 
					p_UserID, 
					p_programID, 
					p_ClientID,
					p_DK, 
					ISNULL(TotalEarnedPoints, 0) 'TotalEarnedPoints', 
					ISNULL(TotalSpentPoints, 0) 'TotalSpentPoints', 
					(ISNULL(TotalEarnedPoints, 0) - ISNULL(TotalSpentPoints, 0)) 'TotalAvailableBalanace'
				FROM dbo.sf2_ufGetPointBalances(@UserID)
				LEFT JOIN dbo.sf2_ufGetPointsSpent(@UserID) ON p_DK = ProjectNumber
				WHERE p_ProgramID = @ProgramID

			RETURN
		END

	-- 78
	IF (@PointsTable = '78')
		BEGIN
			SELECT
				e.p_UserID, 
				e.p_programID, 
				e.p_ClientID,
				e.p_DK, 
				ISNULL(TotalEarnedPoints, 0) 'TotalEarnedPoints', 
				ISNULL(TotalSpentPoints, 0) 'TotalSpentPoints', 
				(ISNULL(TotalEarnedPoints, 0) - ISNULL(TotalSpentPoints, 0)) 'TotalAvailableBalanace'
			FROM (SELECT p_UserID, p_ProgramID, 8 p_ClientID, p_DK, SUM(p_Points) 'TotalEarnedPoints'
						FROM MOONRAKER.Points.dbo.[78]
						WHERE p_UserID = @Userid
						AND ISNULL(p_Description, '') NOT LIKE '%CASH%'
						GROUP BY p_UserID, p_ProgramID, p_DK) e
			LEFT JOIN (SELECT oh.UserID, oh.ProjectNumber, SUM(TotalPoints) 'TotalSpentPoints'
								FROM CURLY.Fulfilment.dbo.OrderHeader oh
								INNER JOIN CURLY.Fulfilment.dbo.AwardLine al ON oh.OrderID = al.OrderID
								WHERE oh.UserID = CONVERT(varchar, @Userid)
								AND al.Void = 0
								GROUP BY oh.UserID, oh.ProjectNumber) s ON e.p_DK = s.ProjectNumber
			WHERE e.p_ProgramID = @ProgramID

			RETURN
		END

	-- 94
	IF (@PointsTable = '94')
		BEGIN
			SELECT
				e.p_UserID, 
				e.p_programID, 
				e.p_ClientID,
				e.p_DK, 
				ISNULL(TotalEarnedPoints, 0) 'TotalEarnedPoints', 
				ISNULL(TotalSpentPoints, 0) 'TotalSpentPoints', 
				(ISNULL(TotalEarnedPoints, 0) - ISNULL(TotalSpentPoints, 0) - ISNULL(TotalCashPoints, 0)) 'TotalAvailableBalanace'
			FROM (SELECT p_UserID, p_ProgramID, 8 p_ClientID, p_DK, SUM(p_Points) 'TotalEarnedPoints'
						FROM MOONRAKER.Points.dbo.[94]
						WHERE p_UserID = @Userid
						AND ISNULL(p_Description, '') NOT LIKE '%CASH%'
						GROUP BY p_UserID, p_ProgramID, p_DK) e
			LEFT JOIN (SELECT p_UserID, p_ProgramID, 8 p_ClientID, p_DK, SUM(p_Points) 'TotalCashPoints'
						FROM MOONRAKER.Points.dbo.[94]
						WHERE p_UserID = @Userid
						AND ISNULL(p_Description, '') = 'CASH'
						GROUP BY p_UserID, p_ProgramID, p_DK) c ON e.p_DK = c.p_DK
			LEFT JOIN (SELECT oh.UserID, oh.ProjectNumber, SUM(TotalPoints) 'TotalSpentPoints'
								FROM CURLY.Fulfilment.dbo.OrderHeader oh
								INNER JOIN CURLY.Fulfilment.dbo.AwardLine al ON oh.OrderID = al.OrderID
								WHERE oh.UserID = CONVERT(varchar, @Userid)
								AND al.Void = 0
								GROUP BY oh.UserID, oh.ProjectNumber) s ON e.p_DK = s.ProjectNumber
			WHERE e.p_ProgramID = @ProgramID

			RETURN
		END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
