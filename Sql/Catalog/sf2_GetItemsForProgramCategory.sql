USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Todd Fellini
-- Create date: August 22, 2005
-- Description:	Returns all items in the specified category for a given program
-- =============================================
ALTER PROCEDURE dbo.sf2_GetItemsForProgramCategory
(
	@pct_ID	int,
	@p_ID	int,
	@char_mc_SDesc     varchar(255) ='%',
	@sngMinimum		varchar(20) ='0',
	@sngMaximum		varchar(20) ='0',
	@strCostOrMSRP	varchar(50) =''
)
AS

	DECLARE @RoundFunction VARCHAR(50)
	DECLARE @Where VARCHAR(5000)
	DECLARE @CatType varchar(5)
	DECLARE @Min int
	DECLARE @Max int
	DECLARE @MerchCalc VARCHAR(500)
	DECLARE @CardCalc VARCHAR(500)
	DECLARE @TravelCalc VARCHAR(500)
	DECLARE @today DATETIME


	SET NOCOUNT ON

	-- Determine category type and take appropriate actions.
	SELECT @CatType = c_Type, @Min = ISNULL(c_LowRange, 0), @Max = ISNULL(c_HighRange, 0), @pct_ID = pct_ID
	FROM ProgramCategories
	INNER JOIN Categories ON pct_c_ID = c_ID
	WHERE pct_c_ID = @pct_ID
	AND pct_p_ID = @p_ID

	-- Call SP for category type RANGE.
	IF @CatType = 'R'
	 BEGIN
		EXEC sf2_GetItemsForProgramCategoryRange @pct_ID, @p_ID, '%', @Min, @Max
		RETURN
	 END

	-- Initialize variables.
	SET @Where = ''
	SELECT @today = GETDATE()

	-- Set variables for which price we should markup on.
	SELECT
	@RoundFunction = RoundFunction,
	@MerchCalc = Merch,
	@TravelCalc = Travel,
	@CardCalc = GiftCard
	FROM dbo.sf2_ufPriceCalcWith2Decimal(@p_ID, @today)

	-- Check to see if we are filtering on anything and set appropriately.
	IF @strCostOrMSRP = 'Cost' SET @Where = 'AND ip_Cost BETWEEN ' + @sngMinimum + ' AND ' + @sngMaximum
	IF @strCostOrMSRP = 'MSRP' SET @Where = 'AND ip_MSRP BETWEEN ' + @sngMinimum + ' AND ' + @sngMaximum
	IF @strCostOrMSRP = 'Points' SET @Where = 'AND ((' + @RoundFunction + @MerchCalc + ' BETWEEN ' + @sngMinimum + ' AND ' + @sngMaximum + ' AND mc_it_ID = 3) OR
																																						 (' + @RoundFunction + @TravelCalc + ' BETWEEN ' + @sngMinimum + ' AND ' + @sngMaximum + ' AND mc_it_ID = 4) OR 
										(' + @RoundFunction + @CardCalc + ' BETWEEN ' + @sngMinimum + ' AND ' + @sngMaximum + ' AND mc_it_ID = 2) OR 
										(' + @RoundFunction + @CardCalc + ' BETWEEN ' + @sngMinimum + ' AND ' + @sngMaximum + ' AND mc_it_ID = 1))'


	-- Store option count by item in temp table.
	SELECT mc_ID AS 'ItemId', COUNT(io_ID) as 'Options'
	INTO #Temp
	FROM ProgramCategories
	INNER JOIN ItemCategories ON pct_c_ID = ic_c_ID
	INNER JOIN ProgramCatalogs ON ic_mc_ID = pc_mc_ID AND pct_p_ID = pc_p_ID 
	INNER JOIN MasterCatalog ON pc_mc_ID = mc_ID
	LEFT JOIN ItemOptions ON mc_ID = io_mc_ID
	WHERE pct_ID = @pct_ID
	GROUP BY mc_ID

	-- Execute query.
	EXEC ('SELECT pc_ID, mc_ID, mc_SDesc,  mc_Desc,
	CASE mc_it_ID WHEN 3 THEN ' + @RoundFunction + @MerchCalc + '
	WHEN 4 THEN ' + @RoundFunction + @TravelCalc + '
	ELSE ' + @RoundFunction + @CardCalc +' END AS Price,
	p_AllowShopping,
	cd_ThumbNailURL + mc_Image AS "ThumbnailImage",
	cd_ImageURL + mc_Image AS "Image",
	Options
	FROM ProgramCategories
	INNER JOIN ItemCategories ON pct_c_ID = ic_c_ID
	INNER JOIN ProgramCatalogs ON ic_mc_ID = pc_mc_ID AND pct_p_ID = pc_p_ID 
	INNER JOIN MasterCatalog ON pc_mc_ID = mc_ID
	INNER JOIN Programs ON pct_p_ID = p_ID
	INNER JOIN ItemPricing ON mc_ip_ID = ip_ID
	LEFT JOIN ItemShippingInfo ON mc_isi_ID = isi_ID AND p_IncludeShipping = 1
	LEFT JOIN SalesTax ON GETDATE() BETWEEN slt_StartDate AND slt_EndDate AND p_IncludeTax = 1
	LEFT JOIN ProgramMarkups ON p_ID = pm_p_ID AND GETDATE() BETWEEN pm_StartDate AND pm_EndDate
	LEFT JOIN CatalogDefaults ON cd_Type = ''P''
	LEFT JOIN #Temp ON mc_ID = ItemID
	LEFT JOIN ProgramItemMarkup ON pc_ID = pim_pc_ID AND GETDATE() BETWEEN pim_StartDate AND pim_EndDate
	WHERE pct_ID = ' + @pct_ID + '
	AND GETDATE() BETWEEN pc_StartDate AND pc_EndDate
	AND ((' + @RoundFunction + @MerchCalc + ' BETWEEN ISNULL(p_MinPoint, 0) AND ISNULL(p_MaxPoint, 1000000) AND mc_it_ID = 3) OR
																																						 (' + @RoundFunction + @TravelCalc + ' BETWEEN ISNULL(p_MinPoint, 0) AND ISNULL(p_MaxPoint, 1000000) AND mc_it_ID = 4) OR
																																						 (' + @RoundFunction + @CardCalc + ' BETWEEN ISNULL(p_MinPoint, 0) AND ISNULL(p_MaxPoint, 1000000) AND mc_it_ID = 2) OR
																																						 (' + @RoundFunction + @CardCalc + ' BETWEEN ISNULL(p_MinPoint, 0) AND ISNULL(p_MaxPoint, 1000000) AND mc_it_ID = 1))
	AND mc_rs_RecStatus IN (0)
	AND pct_rs_RecStatus = 0
	AND ic_rs_RecStatus = 0
	AND mc_SDesc LIKE ''' + @char_mc_SDesc + ''' ' + @Where + ' ORDER BY Price, mc_SDesc')

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF