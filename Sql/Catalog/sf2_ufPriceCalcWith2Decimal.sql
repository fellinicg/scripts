SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: August 31, 2005
-- Description:	Returns pricing formulas for a given program
-- =============================================
CREATE FUNCTION dbo.sf2_ufPriceCalcWith2Decimal
(
	@pm_p_ID INT,
	@today DATETIME
)
RETURNS 
@Results TABLE 
(
	RoundFunction nvarchar(50), 
	Merch nvarchar(500), 
	GiftCard nvarchar(500), 
	Travel nvarchar(500), 
	MerchOpt nvarchar(500), 
	GiftCardOpt nvarchar(500), 
	TravelOpt nvarchar(500)
)
AS
BEGIN

	DECLARE @RoundFunction VARCHAR(50)
	DECLARE @MarkUpOn CHAR(1)
	DECLARE @Price VARCHAR(20)
	DECLARE @p_GC_markup_flag bit
	DECLARE @Merch NVARCHAR(500)
	DECLARE @GiftCard NVARCHAR(500)
	DECLARE @Travel NVARCHAR(500)
	DECLARE @MerchOpt NVARCHAR(500)
	DECLARE @GiftCardOpt NVARCHAR(500)
	DECLARE @TravelOpt NVARCHAR(500)

	-- Retrieve the what to markup and round function to use for the program
	--
	SELECT @MarkUpOn = ISNULL(pm_MarkUpOn, 'C'), @RoundFunction = ISNULL( pm_AddlFunction, '')
	FROM ProgramMarkups
	WHERE pm_p_ID = @pm_p_ID
	AND (@today BETWEEN pm_StartDate AND pm_EndDate OR pm_StartDate IS NULL)

	-- Set which column to use depending on markup
	--
	IF ISNULL(@MarkUpOn, 'C') = 'C' 
		SET @Price = 'ip_Cost'  
	ELSE 
		SET @Price = 'ip_MSRP'

	-- Set gift card markup flag for program
	--
	SELECT @p_GC_markup_flag = p_GC_markup_flag 
	FROM dbo.Programs
	WHERE p_ID = @pm_p_ID

	-- Set return values for merch, travel and cards cost
	--
	SET @Merch = '(ROUND(((' + @Price + ' * CASE ISNULL(pim_MarkUp, -1) WHEN -1 THEN ISNULL(pm_MarkUp, 1) ELSE pim_MarkUp END) + (' + @Price + ' * ISNULL(slt_Rate, 0))) + ISNULL(isi_ShipCost, 0), 2))'
	SET @Travel = '(ROUND(((' + @Price + ') + (' + @Price + ' * ISNULL(slt_Rate, 0))) + ISNULL(isi_ShipCost, 0), 2))'
	IF @p_GC_markup_flag = 1 SET @GiftCard = '(ROUND(' + @Price + ' * CASE ISNULL(pim_MarkUp, -1) WHEN -1 THEN ISNULL(pm_MarkUp, 1) ELSE pim_MarkUp END, 2))'
	IF @p_GC_markup_flag = 0 SET @GiftCard = '(ROUND(' + @Price + ', 2))'

	-- Reset local variable for option costs
	--
	SET @Price = 'ioc_Cost'

	-- Set return values for merch, travel and gift card option cost
	--
	SET @MerchOpt = '(ROUND(((' + @Price + ' * CASE ISNULL(pim_MarkUp, -1) WHEN -1 THEN ISNULL(pm_MarkUp, 1) ELSE pim_MarkUp END) + (' + @Price + ' * ISNULL(slt_Rate, 0))) + ISNULL(isi_ShipCost, 0), 2))'
	SET @TravelOpt = '(ROUND(((' + @Price + ') + (' + @Price + ' * ISNULL(slt_Rate, 0))) + ISNULL(isi_ShipCost, 0), 2))'
	IF @p_GC_markup_flag = 1 SET @GiftCardOpt = '(ROUND(' + @Price + ' * CASE ISNULL(pim_MarkUp, -1) WHEN -1 THEN ISNULL(pm_MarkUp, 1) ELSE pim_MarkUp END, 2))'
	IF @p_GC_markup_flag = 0 SET @GiftCardOpt = '(ROUND(' + @Price + ', 2))'

	-- Insert everything into table to return
	--
	INSERT INTO @Results
	SELECT @RoundFunction, @Merch, @GiftCard, @Travel, @MerchOpt, @GiftCardOpt, @TravelOpt

	RETURN

END
GO