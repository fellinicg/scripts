USE [Catalog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION dbo.sf2_ufGetWishList (@ProgramID INT, @u_ID VARCHAR(50))  
RETURNS TABLE AS
RETURN
(
	SELECT 
	sco.SCOID, 
	sc.CartID, 
	ProgramID, 
	AwardID, 
	OptionValue,
	OptionText,
	Quantity, 
	CASE ISNULL(OptionValue, -1) WHEN -1 THEN 0 ELSE 1 END AS HasOptions
	FROM CURLY.Fulfilment.dbo.ShoppingCart sc
	LEFT JOIN CURLY.Fulfilment.dbo.ShoppingCartOptions sco ON sc.CartId = sco.CartId
	WHERE ProgramID = @ProgramID
	AND UserID = @u_ID
	AND WishList = 1
	AND SpecialOrder = 0
)
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

