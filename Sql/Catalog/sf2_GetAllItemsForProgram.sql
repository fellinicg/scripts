USE [Catalog]
GO
/****** Object:  StoredProcedure [dbo].[sf2_GetAllItemsForProgram]    Script Date: 08/24/2005 13:09:38 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- =============================================
-- Author:		Todd Fellini
-- Create date: August 24, 2005
-- Description:	Returns all items for given program from the Master Catalog
-- =============================================
CREATE  PROCEDURE sf2_GetAllItemsForProgram
(
@pc_p_ID	varchar(100),
@char_mc_SDesc     varchar(255) ='',
@char_mc_Desc     varchar(255) ='',
@char_mc_Keyword     varchar(255) ='',
@sngMinimum		varchar(20) ='0',
@sngMaximum		varchar(20) ='0',
@strCostOrMSRP	varchar(50) ='',
@mf_ID			int =0
)

AS

DECLARE @RoundFunction VARCHAR(50)
DECLARE @Where VARCHAR(5000)
DECLARE @MerchCalc VARCHAR(500)
DECLARE @TravelCalc VARCHAR(500)
DECLARE @CardCalc VARCHAR(500)
DECLARE @today DATETIME

SET NOCOUNT ON

-- Initialize variables.
SET @Where = ''
SELECT @today = GETDATE()

-- Set variables for which price we should markup on.
SELECT
@RoundFunction = RoundFunction,
@MerchCalc = Merch,
@TravelCalc = Travel,
@CardCalc = GiftCard
FROM dbo.sf2_ufPriceCalcWith2Decimal(@pc_p_ID, @today)
 
-- Set WHERE clause for cost ranges.
IF @strCostOrMSRP = 'Cost' SET @Where = 'AND ip_Cost BETWEEN ' + @sngMinimum + ' AND ' + @sngMaximum
IF @strCostOrMSRP = 'MSRP' SET @Where = 'AND ip_MSRP BETWEEN ' + @sngMinimum + ' AND ' + @sngMaximum
IF @strCostOrMSRP = 'Points' SET @Where = 'AND ((' + @RoundFunction + @MerchCalc + ' BETWEEN ' + @sngMinimum + ' AND ' + @sngMaximum + ' AND mc_it_ID = 3) OR
												(' + @RoundFunction + @TravelCalc + ' BETWEEN ' + @sngMinimum + ' AND ' + @sngMaximum + ' AND mc_it_ID = 4) OR 
												(' + @RoundFunction + @CardCalc + ' BETWEEN ' + @sngMinimum + ' AND ' + @sngMaximum + ' AND mc_it_ID = 2) OR 
												(' + @RoundFunction + @CardCalc + ' BETWEEN ' + @sngMinimum + ' AND ' + @sngMaximum + ' AND mc_it_ID = 1))'


-- If necessary, set WHERE clause for manufacturer.
IF @mf_ID > 0 SET @Where = @Where + ' AND mc_mf_ID = ' + CONVERT(VARCHAR, @mf_ID)

-- If necessary, set WHERE clause for search.
IF @char_mc_SDesc <> '' OR @char_mc_Desc <> '' OR @char_mc_Keyword <> ''
 BEGIN
   SET @Where = @Where + ' AND ('
   IF @char_mc_SDesc <> '' 
    BEGIN
     SET @Where = @Where + 'mc_SDesc LIKE ''%' + @char_mc_SDesc + '%'''
    END
   IF @char_mc_Desc <> '' 
    BEGIN
     IF RIGHT(@Where, 2) = '%''' SET @Where = @Where + ' OR'
     SET @Where = @Where + ' mc_Desc LIKE ''%' + @char_mc_Desc + '%'''
    END
   IF @char_mc_Keyword <> '' 
    BEGIN
     IF RIGHT(@Where, 2) = '%''' SET @Where = @Where + ' OR'
     SET @Where = @Where + ' mc_Keywords LIKE ''%' + @char_mc_Keyword + '%'''
    END
   SET @Where = @Where + ')'
 END

-- Execute query.
EXEC ('SELECT pc_ID, mc_ID, mc_SDesc,  mc_Desc,
CASE mc_it_ID WHEN 3 THEN ' + @RoundFunction + @MerchCalc + '
WHEN 4 THEN ' + @RoundFunction + @TravelCalc + '
ELSE ' + @RoundFunction + @CardCalc +' END AS Price,
p_AllowShopping,
cd_ThumbNailURL + mc_Image AS "ThumbnailImage",
cd_ImageURL + mc_Image AS "Image",
p_MinPoint,
p_MaxPoint
FROM Programs
INNER JOIN ProgramCatalogs ON p_ID = pc_p_ID
INNER JOIN MasterCatalog ON pc_mc_ID = mc_ID
INNER JOIN ItemPricing ON mc_ip_ID = ip_ID
LEFT JOIN ItemShippingInfo ON mc_isi_ID = isi_ID AND p_IncludeShipping = 1
LEFT JOIN SalesTax ON GETDATE() BETWEEN slt_StartDate AND slt_EndDate AND p_IncludeTax = 1
LEFT JOIN ProgramMarkups ON p_ID = pm_p_ID AND GETDATE() BETWEEN pm_StartDate AND pm_EndDate
LEFT JOIN CatalogDefaults ON cd_Type = "P"
LEFT JOIN ProgramItemMarkup ON pc_ID = pim_pc_ID AND GETDATE() BETWEEN pim_StartDate AND pim_EndDate
WHERE p_ID = ' + @pc_p_ID + '
AND GETDATE() BETWEEN pc_StartDate AND pc_EndDate
AND ((' + @RoundFunction + @MerchCalc + ' BETWEEN ISNULL(p_MinPoint, 0) AND ISNULL(p_MaxPoint, 1000000) AND mc_it_ID = 3) OR
    (' + @RoundFunction + @TravelCalc + ' BETWEEN ISNULL(p_MinPoint, 0) AND ISNULL(p_MaxPoint, 1000000) AND mc_it_ID = 4) OR
    (' + @RoundFunction + @CardCalc + ' BETWEEN ISNULL(p_MinPoint, 0) AND ISNULL(p_MaxPoint, 1000000) AND mc_it_ID = 2) OR
    (' + @RoundFunction + @CardCalc + ' BETWEEN ISNULL(p_MinPoint, 0) AND ISNULL(p_MaxPoint, 1000000) AND mc_it_ID = 1))
AND mc_rs_RecStatus IN (0) ' + @Where + ' ORDER BY Price, mc_SDesc')
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF