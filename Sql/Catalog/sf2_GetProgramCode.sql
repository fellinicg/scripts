USE Catalog
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Todd Fellini
-- Create date: October 7, 2006
-- Description:	Return program code
-- =============================================
ALTER PROCEDURE dbo.sf2_GetProgramCode
	@ProgramID int, 
	@ProgramCode varchar(20) OUTPUT
AS
BEGIN

	SET NOCOUNT ON

	-- Initialize variables
	SET @ProgramCode = 'XXXX'

	-- Return program code
	SELECT @ProgramCode = ISNULL(p_Code, 'XXXX')
	FROM dbo.Programs
  WHERE p_ID = @ProgramID

END
GO
