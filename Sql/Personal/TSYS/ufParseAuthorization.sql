USE [TSYS]
GO
/****** Object:  UserDefinedFunction [dbo].[ufParseAuthorization]    Script Date: 03/24/2006 10:02:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Todd Fellini
-- Create date: March 22, 2006
-- Description:	Parse TSYS Authorization
-- =============================================
CREATE FUNCTION dbo.ufParseAuthorization
(	
	@Input varchar (8000)
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT

		convert(bigint, substring(@Input, 1, 20)) InternalKey,
		convert(money, substring(@Input, 21, 20)) / 100 LedgBal,
		convert(money, substring(@Input, 41, 20)) / 100 AvailBal,
		convert(money, substring(@Input, 61, 20)) / 100 TranAmount,
		substring(@Input, 81, 100) MerchDescr,
		convert(bigint, substring(@Input, 181, 20)) TranID,
		DateAdd(s, convert(bigint, substring(@Input, 201, 20)) / 1000, '1/1/1970') AuthTime,
		convert(int, substring(@Input, 221, 2)) ResponseCode,
		convert(int, substring(@Input, 223, 6)) AuthCode,
		convert(int, substring(@Input, 229, 4)) MCC
)


GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF