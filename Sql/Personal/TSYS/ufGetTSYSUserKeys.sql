USE [TSYS]
GO
/****** Object:  UserDefinedFunction [dbo].[ufGetTSYSUserKeys]    Script Date: 03/24/2006 10:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Todd Fellini
-- Create date: March 20, 2006
-- Description:	Return TSYS IDs for a user
-- =============================================
CREATE FUNCTION ufGetTSYSUserKeys 
(	
	@UserID int
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT u_ExternalKey, ac_InternalKey, u_MPGProgUserID
	FROM SQL2KPROD.ClarityCards.dbo.Users
	INNER JOIN SQL2KPROD.ClarityCards.dbo.ProgramUser ON u_externalkey = pu_ExternalKey
	INNER JOIN SQL2KPROD.ClarityCards.dbo.Accounts ON pu_PUID = ac_PUID
	WHERE u_mpgproguserid = @UserID
)



GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF