USE [TSYS]
GO
/****** Object:  StoredProcedure [dbo].[upDownloadRYPTransactions]    Script Date: 03/24/2006 09:59:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:			Todd Fellini
-- Create date: March 16, 2006
-- Description:	Download RYP transactions
-- =============================================
ALTER PROCEDURE dbo.upDownloadRYPTransactions 
@BeginDate DATETIME,
@EndDate DATETIME
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @Object int
	DECLARE @Hresult int
	DECLARE @Error VARCHAR(8000)
	DECLARE @ErrorSource varchar (255)
	DECLARE @ErrorDesc varchar (255)
	DECLARE @SystemID varchar (50)
	DECLARE @SecretKey varchar (50)
	DECLARE @UserID bigint
	DECLARE @InterfaceVersion bigint
	DECLARE @APIUrl varchar (50)
	DECLARE @ProgramID bigint
	DECLARE @Result varchar (8000)
	DECLARE @NumberOfTrans INT
	DECLARE @Counter INT

	--Get parameters
	SELECT 
		@SystemID = p_SystemID, 
		@SecretKey = p_SecretKey, 
		@UserID = p_UserID, 
		@InterfaceVersion = p_API_IterfaceVers, 
		@APIUrl = p_API_Url, 
		@ProgramID = p_ProgID
	FROM SQL2KPROD.ClarityCards.dbo.Programs

	--Create the object
	EXEC @Hresult = sp_OACreate 'TSYS.cInterface', @Object OUT 

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upDownloadRYPTransactions - error occured while creating object: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set SystemID Property
	EXEC @Hresult = sp_OASetProperty @Object, 'SystemID', @SystemID

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upDownloadRYPTransactions - error occured while setting SystemID: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set SecretKey Property
	EXEC @Hresult = sp_OASetProperty @Object, 'SecretKey', @SecretKey

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upDownloadRYPTransactions - error occured while setting SecretKey: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set UserID Property
	EXEC @Hresult = sp_OASetProperty @Object, 'UserID', @UserID

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upDownloadRYPTransactions - error occured while setting UserID: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set InterfaceVersion Property
	EXEC @Hresult = sp_OASetProperty @Object, 'InterfaceVersion', @InterfaceVersion

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upDownloadRYPTransactions - error occured while setting InterfaceVersion: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set APIUrl Property
	EXEC @Hresult = sp_OASetProperty @Object, 'APIUrl', @APIUrl

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upDownloadRYPTransactions - error occured while setting APIUrl: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set ProgramID Property
	EXEC @Hresult = sp_OASetProperty @Object, 'ProgramID', @ProgramID

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upDownloadRYPTransactions - error occured while setting ProgramID: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Download transactions
	EXEC @Hresult = sp_OAMethod @Object, 'GetAllTransactions', @Result OUT, @BeginDate, @EndDate

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upDownloadRYPTransactions - error occured while downloading transactions: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	IF @Result = 'SUCCESS'
		BEGIN

			--Get number of transactions
			EXEC @Hresult = sp_OAGetProperty @Object, 'ReturnInfoCount', @NumberOfTrans OUT

			--Error check
			IF @Hresult <> 0
			BEGIN
				EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
				SET @Error = 'upDownloadRYPTransactions - error occured while downloading number of transactions: ' + @ErrorSource + ' ' + @ErrorDesc
				RAISERROR(@Error, 16, 1)
				GOTO CleanUp
			END

			--Create temporary table for transactions
			CREATE TABLE #transactions (
				tranID BIGINT,
				tranType BIGINT,
				InternalKey BIGINT,
				tranDate DATETIME,
				tranAmount MONEY,
				tranDescr VARCHAR(100),
				LedgBal MONEY,
				AvailBal MONEY)

			--Loop and get all transactions
			SET @Counter = 1
			WHILE @Counter <= @NumberOfTrans
				BEGIN

					--Get transaction
					EXEC @Hresult = sp_OAGetProperty @Object, 'GetReturnInfo', @Result OUT, @Counter

					--Error check
					IF @Hresult <> 0
					BEGIN
						EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
						SET @Error = 'upDownloadRYPTransactions - error occured while downloading a transaction: ' + @ErrorSource + ' ' + @ErrorDesc
						RAISERROR(@Error, 16, 1)
						GOTO CleanUp
					END

					--Put transaction into temporary table
					INSERT INTO #transactions
					SELECT * FROM dbo.ufParseTransaction(@Result)

					--Increment counter
					SET @Counter = @Counter + 1
	
				END

			--Insert transactions into Statements table
			INSERT INTO SQL2KPROD.ClarityCards.dbo.Statements (s_tranID, s_tranType, s_InternalKey, s_tranDate, s_tranAmount, s_tranDescr, s_LedgBal, s_AvailBal)
			SELECT tranID, tranType, InternalKey, tranDate, tranAmount, tranDescr, LedgBal, AvailBal
			FROM #transactions 
			LEFT JOIN SQL2KPROD.ClarityCards.dbo.Statements ON tranID = s_tranID AND InternalKey = s_InternalKey
			WHERE s_TranID IS NULL

			--Drop temporary table
			DROP TABLE #transactions

		END

CleanUp:

	--Destroy the object 
	EXEC @Hresult = sp_OADestroy @Object

END


GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF