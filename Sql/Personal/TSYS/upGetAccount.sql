USE [TSYS]
GO
/****** Object:  StoredProcedure [dbo].[upGetAccount]    Script Date: 03/24/2006 10:00:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:			Todd Fellini
-- Create date: March 20, 2006
-- Description:	return account info for a user
-- =============================================
CREATE PROCEDURE dbo.upGetAccount
	@ExternalKey BIGINT,				-- Externalkey field
	@SystemID VARCHAR (50),			-- SystemID for TSYS
	@SecretKey VARCHAR (50),		-- SecretKey for TSYS
	@UserID BIGINT,							-- UserId for TSYS
	@InterfaceVersion BIGINT,		-- InterfaceVersion for TSYS
	@APIUrl VARCHAR (50),				-- APIUrl for TSYS
	@ProgramID BIGINT					-- ProgramID for TSYS
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @Object INT
	DECLARE @Hresult INT
	DECLARE @Error VARCHAR(8000)
	DECLARE @ErrorSource VARCHAR (255)
	DECLARE @ErrorDesc VARCHAR (255)
	DECLARE @Result VARCHAR (8000)
	DECLARE @ResponseCode VARCHAR (200)
	DECLARE @RowIdx INT
	DECLARE @Sql VARCHAR(8000)

	--Create the object
	EXEC @Hresult = sp_OACreate 'TSYS.cInterface', @Object OUT 

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upGetAccount - error occured while creating object: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set SystemID Property
	EXEC @Hresult = sp_OASetProperty @Object, 'SystemID', @SystemID

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upGetAccount - error occured while setting SystemID: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set SecretKey Property
	EXEC @Hresult = sp_OASetProperty @Object, 'SecretKey', @SecretKey

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upGetAccount - error occured while setting SecretKey: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set UserID Property
	EXEC @Hresult = sp_OASetProperty @Object, 'UserID', @UserID

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upGetAccount - error occured while setting UserID: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set InterfaceVersion Property
	EXEC @Hresult = sp_OASetProperty @Object, 'InterfaceVersion', @InterfaceVersion

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upGetAccount - error occured while setting InterfaceVersion: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set APIUrl Property
	EXEC @Hresult = sp_OASetProperty @Object, 'APIUrl', @APIUrl

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upGetAccount - error occured while setting APIUrl: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Set ProgramID Property
	EXEC @Hresult = sp_OASetProperty @Object, 'ProgramID', @ProgramID

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upGetAccount - error occured while setting ProgramID: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Adjust balance
	EXEC @Hresult = sp_OAMethod @Object, 'GetAccount', @Result OUT, @ExternalKey

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upGetAccount - error occured while retrieving account: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Get Response Code
	EXEC @Hresult = sp_OAGetProperty @Object, 'ResponseCode', @ResponseCode OUT

	--Error check
	IF @Hresult <> 0
	BEGIN
		EXEC sp_OAGetErrorInfo @Object, @ErrorSource OUT, @ErrorDesc OUT
		SET @Error = 'upGetAccount - error occured while getting ResponseCode: ' + @ErrorSource + ' ' + @ErrorDesc
		RAISERROR(@Error, 16, 1)
		GOTO CleanUp
	END

	--Check ResponseCode for success
	IF @ResponseCode = 'SUCCESS'
		BEGIN
			-- Create temp table
			CREATE TABLE #Account (
				InternalKey BIGINT,
				ExternalKey VARCHAR(50),
				FirstName VARCHAR(50),
				LastName VARCHAR(50),
				CardNum VARCHAR(16),
				MonthExpr CHAR(2),
				YearExpr CHAR(4),
				CVV2 INT,
				Street1 VARCHAR(50),
				Street2 VARCHAR(50),
				City VARCHAR(50),
				State VARCHAR(50),
				Zip VARCHAR(50),
				Country VARCHAR(50),
				Email VARCHAR(50),
				AccountStatus VARCHAR(50),
				LedgerBalance BIGINT,
				AvailableBalance BIGINT)

			--Insert return data into temporary table
			SELECT *
			INTO #temp
			FROM dbo.Split(@Result, '||')

			--Initialize SQL statement
			SET @Sql = 'INSERT INTO #Account (InternalKey, ExternalKey, FirstName, LastName, CardNum, MonthExpr, '
			SET @Sql = @Sql + 'YearExpr, CVV2, Street1, Street2, City, State, Zip, Country, Email, AccountStatus, '
			SET @Sql = @Sql + 'LedgerBalance, AvailableBalance) VALUES('

			--Loop over temporary table and put in #Account table
			WHILE EXISTS(SELECT 1 FROM #temp)
				BEGIN
					--Get row index
					SELECT TOP 1 @RowIdx = RowIdx FROM #temp ORDER BY RowIdx
					--Append to SQL statement
					SET @Sql = @Sql + (SELECT '''' + Items + '''' FROM #temp WHERE RowIdx = @RowIdx) +  ','
					--Remove row that we just worked on
					DELETE FROM #temp WHERE RowIdx = @RowIdx
				END

			--Remove last comma and add parens
			SET @Sql = LEFT(@Sql, LEN(@Sql) - 1) + ')'

			--Execute SQL to insert data into #Account table
			EXEC (@Sql)

			--Display results
			SELECT * FROM #Account

			--Drop temporary tables if they exist
			DROP TABLE #temp
			DROP TABLE #Account
	
		END

CleanUp:

	--Destroy the object 
	EXEC @Hresult = sp_OADestroy @Object

END


GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF