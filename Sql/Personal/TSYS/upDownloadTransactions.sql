USE [TSYS]
GO
/****** Object:  StoredProcedure [dbo].[upDownloadTransactions]    Script Date: 03/24/2006 10:00:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:			Todd Fellini
-- Create date: March 22, 2006
-- Description:	Download Debit Card transactions
-- =============================================
CREATE PROCEDURE dbo.upDownloadTransactions 
	@BeginDate DATETIME,			  -- Beginning of date range
	@EndDate DATETIME,					-- End of date range
	@ShowResults BIT = 0				-- Show report of results?
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @SystemID varchar (50)
	DECLARE @SecretKey varchar (50)
	DECLARE @UserID bigint
	DECLARE @InterfaceVersion bigint
	DECLARE @APIUrl varchar (50)
	DECLARE @ProgramID bigint

	--Insert Programs into temporary table for loop
	SELECT *
	INTO #temp
	FROM GOLDFINGER.ClarityCards.dbo.Programs

	--Loop over programs
	WHILE EXISTS (SELECT 1 FROM #temp)
		BEGIN

			--Store programid
			SELECT TOP 1 @ProgramID = p_ProgID FROM #temp ORDER BY p_ProgID

			--Get parameters
			SELECT 
				@SystemID = p_SystemID, 
				@SecretKey = p_SecretKey, 
				@UserID = p_UserID,		
				@InterfaceVersion = p_API_IterfaceVers, 
				@APIUrl = p_API_Url
			FROM #temp
			WHERE p_ProgID = @ProgramID

			--Download transactions
			EXECUTE dbo.upGetAllTransactions
				@BeginDate
				,@EndDate
				,@SystemID
				,@SecretKey
				,@UserID
				,@InterfaceVersion
				,@APIUrl
				,@ProgramID
				,@ShowResults

			--Download authorizations
			EXECUTE dbo.upGetAuthorizations
				@BeginDate
				,@EndDate
				,@SystemID
				,@SecretKey
				,@UserID
				,@InterfaceVersion
				,@APIUrl
				,@ProgramID
				,@ShowResults

			--Delete processed program 
			DELETE FROM #temp WHERE p_ProgID = @ProgramID
		END

	--Drop temp tables
	DROP TABLE #temp

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF