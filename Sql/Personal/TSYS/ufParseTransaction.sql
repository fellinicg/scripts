USE [TSYS]
GO
/****** Object:  UserDefinedFunction [dbo].[ufParseTransaction]    Script Date: 03/24/2006 10:02:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Todd Fellini
-- Create date: March 16, 2006
-- Description:	Parse TSYS Transaction
-- =============================================
CREATE FUNCTION dbo.ufParseTransaction 
(	
	@Trans varchar (8000)
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT
		convert(bigint, substring(@Trans, 1, 20)) tranID,
		convert(bigint, substring(@Trans, 21, 20)) tranType,
		convert(bigint, substring(@Trans, 41, 20)) InternalKey,
		DateAdd(s, convert(bigint, substring(@Trans, 61, 20)) / 1000, '1/1/1970') tranDate,
		convert(money, substring(@Trans, 81, 20)) / 100 tranAmount,
		substring(@Trans, 101, 100) tranDescr,
		convert(money, substring(@Trans, 201, 20)) / 100 LedgBal,
		convert(money, substring(@Trans, 221, 20)) / 100 AvailBal
)

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF