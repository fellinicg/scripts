USE [Ace]
GO
/****** Object:  StoredProcedure [dbo].[SendEmail]    Script Date: 03/24/2006 10:07:52 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE dbo.SendEmail 
@recip VARCHAR(500),
@subj VARCHAR(8000),
@body VARCHAR(8000)
AS

declare @rc int
exec @rc = [MAIL.MADISONPG.COM].master.dbo.xp_smtp_sendmail
	@FROM = N'aceproject@madisonpg.com',
	@FROM_NAME = N'AceProject Notification',
	@TO = @recip,
	@replyto = N'',
	@CC = N'',
	@BCC = N'aceproject@madisonpg.com',
	@priority = N'HIGH',
	@subject = @subj,
	@message = @body,
	@messagefile = N'',
	@type = N'text/html',
	@attachment = N'',
	@attachments = N'',
	@codepage = 0,
	@server = N'mail.madisonpg.com'
select RC = @rc
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF