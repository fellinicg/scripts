USE [Ace]
GO
/****** Object:  UserDefinedFunction [dbo].[DelimitAssigned]    Script Date: 03/24/2006 10:08:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION dbo.DelimitAssigned  (@Value INT)
RETURNS VARCHAR(5000)
AS
BEGIN
   DECLARE @RetVal VARCHAR(5000)
   DECLARE @Tmp VARCHAR(5000)
   DECLARE @Rowct INT
   DECLARE @i INT
   DECLARE @Temp TABLE (Assigned VARCHAR(500)) 

   INSERT INTO @Temp
   SELECT LEFT(FIRST_NAME, 1) + '. ' + LTRIM(LAST_NAME)
   FROM [s2.aceproject.com].madisonpg.dbo.TASK_USER tu
   INNER JOIN [s2.aceproject.com].madisonpg.dbo.USERS u ON tu.USER_ID = u.USER_ID
   WHERE tu.TASK_ID = @Value
   AND tu.ASSIGNATION IN (0,2)

   SET @Rowct = @@ROWCOUNT

   SET @i = 1
   SET @Retval = ''
   WHILE @i <= @Rowct
     BEGIN
       SELECT TOP 1 @Tmp = Assigned FROM @Temp ORDER BY Assigned
       SET @Retval = @Retval + @Tmp + ','
       DELETE FROM @Temp
       WHERE Assigned = (SELECT TOP 1 Assigned FROM @Temp ORDER BY Assigned)
       SET @i = @i + 1
     END

   SET @i = LEN(@Retval)
   IF @i > 0 SET @Retval = LEFT(@RetVal, @i-1)

   RETURN(@RetVal)

END


GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF