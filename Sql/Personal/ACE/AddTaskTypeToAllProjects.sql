USE [Ace]
GO
/****** Object:  StoredProcedure [dbo].[AddTaskTypeToAllProjects]    Script Date: 03/24/2006 10:06:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE dbo.AddTaskTypeToAllProjects @TT VARCHAR(500) AS

INSERT INTO [s2.aceproject.com].madisonpg.dbo.TASK_TYPE (PROJECT_ID, TASK_TYPE_NAME, TASK_TYPE_DESC)
SELECT PROJECT_ID, @TT, @TT
FROM dbo.GetProjWithTaskType(@TT),
[s2.aceproject.com].madisonpg.dbo.PROJECT
WHERE PROJECT_ID <> pid
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF