USE [Ace]
GO
/****** Object:  UserDefinedFunction [dbo].[GetProjWithTaskType]    Script Date: 03/24/2006 10:08:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION dbo.GetProjWithTaskType(@TT nvarchar(500))
RETURNS TABLE
AS

RETURN (SELECT PROJECT_ID PID
 FROM [s2.aceproject.com].madisonpg.dbo.TASK_TYPE
 WHERE TASK_TYPE_NAME = @TT)



GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF