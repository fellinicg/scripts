USE [Ace]
GO
/****** Object:  StoredProcedure [dbo].[SendEmailFile]    Script Date: 03/24/2006 10:08:02 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE dbo.SendEmailFile
@recip VARCHAR(500),
@subj VARCHAR(8000),
@file VARCHAR(1000)
AS

declare @rc int
exec @rc = master.dbo.xp_smtp_sendmail
	@FROM = N'aceproject@madisonpg.com',
	@FROM_NAME = N'AceProject Notification',
	@TO = @recip,
	@replyto = N'',
	@CC = N'',
	@BCC = N'',
	@priority = N'HIGH',
	@subject = @subj,
	@message = N'',
	@messagefile = @file,
	@type = N'text/html',
	@attachment = N'',
	@attachments = N'',
	@codepage = 0,
	@server = N'mail.madisonpg.com',
	@dumpmsg     = N'D:\TEMP\dumpmsg.log'
select RC = @rc
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF