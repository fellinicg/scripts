USE [Ace]
GO
/****** Object:  StoredProcedure [dbo].[AddStatusTypeToAllProjects]    Script Date: 03/24/2006 10:05:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE dbo.AddStatusTypeToAllProjects @TT VARCHAR(500) AS

INSERT INTO [s2.aceproject.com].madisonpg.dbo.STATUS_TYPE (PROJECT_ID, STATUS_TYPE_NAME, STATUS_TYPE_DESC)
SELECT PROJECT_ID, @TT, @TT
FROM dbo.GetProjWithStatusType(@TT),
[s2.aceproject.com].madisonpg.dbo.PROJECT
WHERE PROJECT_ID <> pid

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF