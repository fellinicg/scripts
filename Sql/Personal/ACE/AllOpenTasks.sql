USE [Ace]
GO
/****** Object:  StoredProcedure [dbo].[AllOpenTasks]    Script Date: 03/24/2006 10:06:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE dbo.AllOpenTasks AS

DECLARE @emailbody VARCHAR(8000)
DECLARE @RowCt INT
DECLARE @i INT
DECLARE @SendTo VARCHAR(100)

SET NOCOUNT ON
SET ANSI_NULLS ON
SET ANSI_WARNINGS ON

PRINT '<HTML><STYLE TYPE=text/css>TABLE{font-family:arial; font-size:8pt}TH{text-align: left}</STYLE><BODY>The following tasks are OPEN:<BR><BR><TABLE><TR><TH>Assigned To</TH><TH>Priority</TH><TH>Task</TH><TH>Project</TH><TH>Added</TH><TH>Due</TH><TH>Status</TH></TR>'

SELECT 
 '<TR>' +
 dbo.HTML_TD(u.FIRST_NAME + ' ' + u.LAST_NAME) +
 dbo.HTML_TD(pt.PRIORITY_TYPE_NAME) + 
 dbo.HTML_TD(CONVERT(VARCHAR, CONVERT(INT, t.TASK_NUMBER)) + ' - ' + t.TASK_RESUME) +
 dbo.HTML_TD(pj.PROJECT_NAME) +
 dbo.HTML_TD(CONVERT(VARCHAR, t.DATE_EXPECTED_START_TASK, 101)) +
 dbo.HTML_TD(CASE ISNULL(CONVERT(VARCHAR, DATE_EXPECTED_END_TASK), '') WHEN '' THEN '&nbsp;' ELSE CONVERT(VARCHAR, DATE_EXPECTED_END_TASK, 101) END) +
 dbo.HTML_TD(st.STATUS_TYPE_NAME) +
 '</TR>'
FROM [s2.aceproject.com].madisonpg.dbo.TASK_USER tu
INNER JOIN [s2.aceproject.com].madisonpg.dbo.TASK t ON tu.TASK_ID = t.TASK_ID
INNER JOIN [s2.aceproject.com].madisonpg.dbo.STATUS_TYPE st ON t.STATUS_TYPE_ID = st.STATUS_TYPE_ID
INNER JOIN [s2.aceproject.com].madisonpg.dbo.USERS u ON tu.USER_ID = u.USER_ID
INNER JOIN [s2.aceproject.com].madisonpg.dbo.PRIORITY_TYPE pt ON t.PRIORITY_TYPE_ID = pt.PRIORITY_TYPE_ID
INNER JOIN [s2.aceproject.com].madisonpg.dbo.PROJECT pj ON t.PROJECT_ID = pj.PROJECT_ID
WHERE COMPLETED_STATUS = 2
AND tu.ASSIGNATION in (0,2)
ORDER BY CONVERT(VARCHAR, PRIORITY_TYPE_DESC), t.DATE_EXPECTED_END_TASK, t.DATE_EXPECTED_START_TASK, t.TASK_NUMBER

PRINT '</TABLE></BODY></HTML>'
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF