USE [Ace]
GO
/****** Object:  UserDefinedFunction [dbo].[GetProjWithStatusType]    Script Date: 03/24/2006 10:08:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION dbo.GetProjWithStatusType(@ST nvarchar(500))
RETURNS TABLE
AS

RETURN (
SELECT PROJECT_ID PID
 FROM [s2.aceproject.com].madisonpg.dbo.STATUS_TYPE
 WHERE STATUS_TYPE_NAME = @ST)

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF