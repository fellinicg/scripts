USE [Ace]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE dbo.DueTodayAlert AS

DECLARE @emailbody VARCHAR(8000)
DECLARE @RowCt INT
DECLARE @i INT
DECLARE @SendTo VARCHAR(100)

SET NOCOUNT ON

SELECT 
 u.FIRST_NAME, 
 EMAIL_ALERT, 
 pt.PRIORITY_TYPE_NAME, 
 tu.ASSIGNATION, 
 t.TASK_RESUME, 
 t.DATE_EXPECTED_END_TASK, 
 st.STATUS_TYPE_NAME, 
 tu.USER_ID,
 CONVERT(VARCHAR, PRIORITY_TYPE_DESC) 'PRIORITY'
INTO #TEMP
FROM [s2.aceproject.com].madisonpg.dbo.TASK_USER tu
INNER JOIN [s2.aceproject.com].madisonpg.dbo.TASK t ON tu.TASK_ID = t.TASK_ID
INNER JOIN [s2.aceproject.com].madisonpg.dbo.STATUS_TYPE st ON t.STATUS_TYPE_ID = st.STATUS_TYPE_ID
INNER JOIN [s2.aceproject.com].madisonpg.dbo.USERS u ON tu.USER_ID = u.USER_ID
INNER JOIN [s2.aceproject.com].madisonpg.dbo.PRIORITY_TYPE pt ON t.PRIORITY_TYPE_ID = pt.PRIORITY_TYPE_ID
WHERE DATEDIFF(dd, t.DATE_EXPECTED_END_TASK, GETDATE()) = 0
AND (COMPLETED_STATUS = 2 AND STATUS_TYPE_NAME <> 'Review')
AND tu.ASSIGNATION in (0,2)

SELECT DISTINCT
'<HTML><STYLE TYPE=text/css>TH{text-align: left}</STYLE><BODY>Dear ' + FIRST_NAME + ',<BR><BR>' + CHAR(13) + CHAR(10) +
'The following tasks are DUE TODAY:<BR>'  + CHAR(13) + CHAR(10) +
'<BR><TABLE WIDTH=600><TR><TH>Assigned Task(s)</TH><TH>Due Date</TH><TH>Priority</TH><TH>Status</TH></TR>'  + CHAR(13) + CHAR(10) AS 'Greeting',
USER_ID AS 'USER_ID2',
EMAIL_ALERT
INTO #TEMP2
FROM #TEMP
ORDER BY USER_ID

SET @RowCt = @@ROWCOUNT
SET @i = 1

WHILE @i <= @RowCt
 BEGIN
  SELECT TOP 1 @SendTo = EMAIL_ALERT, @emailbody = Greeting FROM #TEMP2 ORDER BY USER_ID2
  SELECT  @emailbody = @emailbody + '<TR><TD>' + TASK_RESUME + '</TD><TD>' + CONVERT(VARCHAR, DATE_EXPECTED_END_TASK, 101) + '</TD><TD>' + PRIORITY_TYPE_NAME + '</TD><TD>' + STATUS_TYPE_NAME + '</TD></TR>' + char(13) + char(10)
  FROM #TEMP
  WHERE USER_ID = (SELECT TOP 1 USER_ID2 FROM #TEMP2 ORDER BY USER_ID2)
  ORDER BY PRIORITY
  SET @emailbody = @emailbody + '</TABLE></BODY></HTML>'
  EXEC dbo.sendemail @sendto, 'AceProject - TASK(S) ALERT', @emailbody
  DELETE FROM #TEMP2 WHERE USER_ID2 = (SELECT TOP 1 USER_ID2 FROM #TEMP2 ORDER BY USER_ID2)
  SET @i = @i + 1
 END

DROP TABLE #TEMP
DROP TABLE #TEMP2
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF