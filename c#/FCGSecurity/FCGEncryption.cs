﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace FCGSecurity
{
    public class FCGEncryption
    {
        public static string EncryptString(string Text, string Key)
        {
            return Encrypt(Text, Key);
        }

        public static string DecryptString(string Text, string Key)
        {
            return Decrypt(Text, Key);
        }

        private static string Encrypt(string Text, string Key)
        {
            byte[] clearBytes = Encoding.Unicode.GetBytes(Text);
            string encrypted = null;

            try
            {
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(Key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        encrypted = Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
            catch { }

            return encrypted;
        }

        private static string Decrypt(string Text, string Key)
        {
            byte[] cipherBytes = Convert.FromBase64String(Text);
            string decrypted = null;

            try
            {
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(Key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        decrypted = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
            }
            catch {}

            return decrypted;
        }
    }
}
