﻿using FCGSecurity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EncryptionTool
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }
        private void buttonEncrypt_Click(object sender, EventArgs e)
        {
            // Declarations
            FCGEncryption FCGEncrypt = new FCGEncryption();

            this.textBoxOutput.Text = FCGEncryption.EncryptString(this.textBoxInput.Text,
                                                                    this.textBoxKey.Text);
        }

        private void buttonDecrypt_Click(object sender, EventArgs e)
        {
            // Declarations
            FCGEncryption FCGEncrypt = new FCGEncryption();

            this.textBoxOutput.Text = FCGEncryption.DecryptString(this.textBoxInput.Text,
                                                                    this.textBoxKey.Text);

        }

        private void linkLabelCreate_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[12];

            rng.GetBytes(buff);

            this.textBoxKey.Text = BytesToHexString(buff);
        }

        private String BytesToHexString(byte[] bytes)
        {
            StringBuilder hexString = new StringBuilder(64);

            for (int counter = 0; counter < bytes.Length; counter++)
            {
                hexString.Append(String.Format("{0:X2}", bytes[counter]));
            }
            return hexString.ToString();
        }
    }
}
