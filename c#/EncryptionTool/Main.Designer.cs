﻿namespace EncryptionTool
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.textBoxInput = new System.Windows.Forms.TextBox();
			this.groupBoxInput = new System.Windows.Forms.GroupBox();
			this.groupBoxKey = new System.Windows.Forms.GroupBox();
			this.linkLabelCreate = new System.Windows.Forms.LinkLabel();
			this.checkBoxHash = new System.Windows.Forms.CheckBox();
			this.textBoxKey = new System.Windows.Forms.TextBox();
			this.groupBoxAction = new System.Windows.Forms.GroupBox();
			this.buttonDecrypt = new System.Windows.Forms.Button();
			this.buttonEncrypt = new System.Windows.Forms.Button();
			this.groupBoxOutput = new System.Windows.Forms.GroupBox();
			this.textBoxOutput = new System.Windows.Forms.TextBox();
			this.groupBoxInput.SuspendLayout();
			this.groupBoxKey.SuspendLayout();
			this.groupBoxAction.SuspendLayout();
			this.groupBoxOutput.SuspendLayout();
			this.SuspendLayout();
			// 
			// textBoxInput
			// 
			this.textBoxInput.Location = new System.Drawing.Point(15, 19);
			this.textBoxInput.Multiline = true;
			this.textBoxInput.Name = "textBoxInput";
			this.textBoxInput.Size = new System.Drawing.Size(563, 74);
			this.textBoxInput.TabIndex = 0;
			// 
			// groupBoxInput
			// 
			this.groupBoxInput.Controls.Add(this.textBoxInput);
			this.groupBoxInput.Location = new System.Drawing.Point(12, 12);
			this.groupBoxInput.Name = "groupBoxInput";
			this.groupBoxInput.Size = new System.Drawing.Size(598, 106);
			this.groupBoxInput.TabIndex = 1;
			this.groupBoxInput.TabStop = false;
			this.groupBoxInput.Text = "Input";
			// 
			// groupBoxKey
			// 
			this.groupBoxKey.Controls.Add(this.linkLabelCreate);
			this.groupBoxKey.Controls.Add(this.checkBoxHash);
			this.groupBoxKey.Controls.Add(this.textBoxKey);
			this.groupBoxKey.Location = new System.Drawing.Point(12, 137);
			this.groupBoxKey.Name = "groupBoxKey";
			this.groupBoxKey.Size = new System.Drawing.Size(387, 50);
			this.groupBoxKey.TabIndex = 2;
			this.groupBoxKey.TabStop = false;
			this.groupBoxKey.Text = "Key";
			// 
			// linkLabelCreate
			// 
			this.linkLabelCreate.AutoSize = true;
			this.linkLabelCreate.Location = new System.Drawing.Point(282, 22);
			this.linkLabelCreate.Name = "linkLabelCreate";
			this.linkLabelCreate.Size = new System.Drawing.Size(38, 13);
			this.linkLabelCreate.TabIndex = 2;
			this.linkLabelCreate.TabStop = true;
			this.linkLabelCreate.Text = "Create";
			this.linkLabelCreate.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelCreate_LinkClicked);
			// 
			// checkBoxHash
			// 
			this.checkBoxHash.AutoSize = true;
			this.checkBoxHash.Location = new System.Drawing.Point(326, 21);
			this.checkBoxHash.Name = "checkBoxHash";
			this.checkBoxHash.Size = new System.Drawing.Size(57, 17);
			this.checkBoxHash.TabIndex = 1;
			this.checkBoxHash.Text = "Hash?";
			this.checkBoxHash.UseVisualStyleBackColor = true;
			// 
			// textBoxKey
			// 
			this.textBoxKey.Location = new System.Drawing.Point(15, 19);
			this.textBoxKey.MaxLength = 50;
			this.textBoxKey.Name = "textBoxKey";
			this.textBoxKey.Size = new System.Drawing.Size(261, 20);
			this.textBoxKey.TabIndex = 0;
			// 
			// groupBoxAction
			// 
			this.groupBoxAction.Controls.Add(this.buttonDecrypt);
			this.groupBoxAction.Controls.Add(this.buttonEncrypt);
			this.groupBoxAction.Location = new System.Drawing.Point(405, 137);
			this.groupBoxAction.Name = "groupBoxAction";
			this.groupBoxAction.Size = new System.Drawing.Size(205, 50);
			this.groupBoxAction.TabIndex = 3;
			this.groupBoxAction.TabStop = false;
			this.groupBoxAction.Text = "Action";
			// 
			// buttonDecrypt
			// 
			this.buttonDecrypt.BackColor = System.Drawing.SystemColors.Desktop;
			this.buttonDecrypt.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.buttonDecrypt.Location = new System.Drawing.Point(111, 15);
			this.buttonDecrypt.Name = "buttonDecrypt";
			this.buttonDecrypt.Size = new System.Drawing.Size(75, 23);
			this.buttonDecrypt.TabIndex = 1;
			this.buttonDecrypt.Text = "&Decrypt";
			this.buttonDecrypt.UseVisualStyleBackColor = false;
			this.buttonDecrypt.Click += new System.EventHandler(this.buttonDecrypt_Click);
			// 
			// buttonEncrypt
			// 
			this.buttonEncrypt.BackColor = System.Drawing.SystemColors.Desktop;
			this.buttonEncrypt.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.buttonEncrypt.Location = new System.Drawing.Point(22, 15);
			this.buttonEncrypt.Name = "buttonEncrypt";
			this.buttonEncrypt.Size = new System.Drawing.Size(75, 23);
			this.buttonEncrypt.TabIndex = 0;
			this.buttonEncrypt.Text = "&Encrypt";
			this.buttonEncrypt.UseVisualStyleBackColor = false;
			this.buttonEncrypt.Click += new System.EventHandler(this.buttonEncrypt_Click);
			// 
			// groupBoxOutput
			// 
			this.groupBoxOutput.Controls.Add(this.textBoxOutput);
			this.groupBoxOutput.Location = new System.Drawing.Point(12, 210);
			this.groupBoxOutput.Name = "groupBoxOutput";
			this.groupBoxOutput.Size = new System.Drawing.Size(598, 115);
			this.groupBoxOutput.TabIndex = 4;
			this.groupBoxOutput.TabStop = false;
			this.groupBoxOutput.Text = "Output";
			// 
			// textBoxOutput
			// 
			this.textBoxOutput.Location = new System.Drawing.Point(15, 19);
			this.textBoxOutput.Multiline = true;
			this.textBoxOutput.Name = "textBoxOutput";
			this.textBoxOutput.Size = new System.Drawing.Size(563, 80);
			this.textBoxOutput.TabIndex = 0;
			// 
			// Main
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(622, 339);
			this.Controls.Add(this.groupBoxOutput);
			this.Controls.Add(this.groupBoxAction);
			this.Controls.Add(this.groupBoxKey);
			this.Controls.Add(this.groupBoxInput);
			this.Name = "Main";
			this.Text = "Encryption Tool";
			this.groupBoxInput.ResumeLayout(false);
			this.groupBoxInput.PerformLayout();
			this.groupBoxKey.ResumeLayout(false);
			this.groupBoxKey.PerformLayout();
			this.groupBoxAction.ResumeLayout(false);
			this.groupBoxOutput.ResumeLayout(false);
			this.groupBoxOutput.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxInput;
        private System.Windows.Forms.GroupBox groupBoxInput;
        private System.Windows.Forms.GroupBox groupBoxKey;
        private System.Windows.Forms.CheckBox checkBoxHash;
        private System.Windows.Forms.TextBox textBoxKey;
        private System.Windows.Forms.GroupBox groupBoxAction;
        private System.Windows.Forms.Button buttonEncrypt;
        private System.Windows.Forms.Button buttonDecrypt;
        private System.Windows.Forms.GroupBox groupBoxOutput;
        private System.Windows.Forms.TextBox textBoxOutput;
        private System.Windows.Forms.LinkLabel linkLabelCreate;
    }
}

