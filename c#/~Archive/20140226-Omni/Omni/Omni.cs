﻿using FCGLogger;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Web;

namespace Omni
{
    public class Omni
    {

	#region Declarations

		private bool IsDevelopmentPlatform = false;
		private string ConnectionString = string.Empty;
		private string XmlDataPath = string.Empty;
		private string ErrorLog = string.Empty;
        private string Log = string.Empty;

	#endregion
        
	#region Public Functions

        public string GetServerInfo(string ServerID)
        {
            // Declarations
            string rs = string.Empty;

            try
            {
                // Declarations
                SqlCommand comm = new SqlCommand();

                // Add parameters
                comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;

                // Get data
                rs = SqlToJSON(comm.Parameters, "upGetServerInfo", System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ExceptionErr)
            {
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, ErrorLog);
            }

            // Return result
            return rs;
        }

        public string GetServers()
        {
            // Declarations
            string rs = string.Empty;

            try
            {
                // Declarations
                SqlCommand comm = new SqlCommand();

                // Get data
                rs = SqlToJSON(comm.Parameters, "upGetServers", System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ExceptionErr)
            {
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, ErrorLog);
            }

            // Return result
            return rs;
        }

        public Omni()
        {
            try
            {
                // Check whether its a web or console app
                if (System.Web.HttpContext.Current == null)
                {
                    InitializeClassVariables();
                }
                else
                {
                    // Attempt to get path to xmldata
                    try { XmlDataPath = HttpContext.Current.Request.MapPath("xmldata", "/", false); }
                    catch { }

                    // Check session variables
                    if (HttpContext.Current.Session["IsDevelopmentPlatform"] == null ||
                        HttpContext.Current.Session["ConnectionStr"] == null ||
                        HttpContext.Current.Session["ErrorLog"] == null ||
                        HttpContext.Current.Session["Log"] == null)
                    {
                        InitializeSession();
                    }

                    // Retrieve platform
                    try
                    {
                        IsDevelopmentPlatform = Convert.ToBoolean(HttpContext.Current.Session["IsDevelopmentPlatform"].ToString().ToLower());
                    }
                    catch { }

                    // Retrieve database connection and SMTP information
                    ConnectionString = HttpContext.Current.Session["ConnectionStr"].ToString();

                    // Retrieve ErrorLog
                    ErrorLog = HttpContext.Current.Session["ErrorLog"].ToString();
                }
            }

            catch (Exception ExceptionErr)
            {
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, ErrorLog);
            }
        }

    #endregion

    #region Private Functions

        private object EmptyToNull(string Input)
        {
            return (Input.Trim().Length == 0 ? (object)DBNull.Value : Input.Trim());
        }

        private void InitializeClassVariables()
        {
            // Declarations
            AppSettingsReader AppSetReader = new AppSettingsReader();

            // Attempt to get path to xmldata
            try { XmlDataPath = System.Reflection.Assembly.GetEntryAssembly().Location.Substring(0, System.Reflection.Assembly.GetEntryAssembly().Location.LastIndexOf("\\") + 1) + "xmldata"; }
            catch { }

            // Retrieve platform
            try
            {
                IsDevelopmentPlatform = (bool)AppSetReader.GetValue("Development", typeof(bool));
            }
            catch { }

            // Retrieve database connection and SMTP information
            if (IsDevelopmentPlatform)
            {
                ConnectionString = (string)AppSetReader.GetValue("DevDB", typeof(string));
            }
            else
            {
                ConnectionString = (string)AppSetReader.GetValue("DB", typeof(string));
            }

            // Retrieve ErrorLog
            try { ErrorLog = (string)AppSetReader.GetValue("ErrorLog", typeof(string)); }
            catch
            {
                try { ErrorLog = HttpContext.Current.Request.MapPath("logfiles", "/", false) + "\\ErrorLog.xml"; }
                catch
                {
                    try { ErrorLog = System.Reflection.Assembly.GetEntryAssembly().Location.Substring(0, System.Reflection.Assembly.GetEntryAssembly().Location.LastIndexOf("\\") + 1) + "ErrorLog.xml"; }
                    catch
                    { ErrorLog = string.Empty; }
                }
            }

            // Retrieve Log
            try { Log = (string)AppSetReader.GetValue("Log", typeof(string)); }
            catch
            {
                try { Log = HttpContext.Current.Request.MapPath("logfiles", "/", false) + "\\Log.xml"; }
                catch
                {
                    try { Log = System.Reflection.Assembly.GetEntryAssembly().Location.Substring(0, System.Reflection.Assembly.GetEntryAssembly().Location.LastIndexOf("\\") + 1) + "Log.xml"; }
                    catch
                    { Log = string.Empty; }
                }
            }

        }

        private void InitializeSession()
        {
            // Declarations
            AppSettingsReader AppSetReader = new AppSettingsReader();

            // Retrieve platform
            try
            {
                HttpContext.Current.Session["IsDevelopmentPlatform"] = (bool)AppSetReader.GetValue("Development", typeof(bool));
            }
            catch { }

            // Retrieve database connection
            if (Convert.ToBoolean(HttpContext.Current.Session["IsDevelopmentPlatform"].ToString().ToLower()))
            {
                HttpContext.Current.Session["ConnectionStr"] =
                    (string)AppSetReader.GetValue("DevDB", typeof(string));
            }
            else
            {
                HttpContext.Current.Session["ConnectionStr"] =
                    (string)AppSetReader.GetValue("DB", typeof(string));
            }

            // Verify user
            //if (!VerifyUser())
            //{
            //    HttpContext.Current.Session.Abandon();
            //}

            // Retrieve ErrorLog
            try { HttpContext.Current.Session["ErrorLog"] = (string)AppSetReader.GetValue("ErrorLog", typeof(string)); }
            catch
            {
                try { HttpContext.Current.Session["ErrorLog"] = HttpContext.Current.Request.MapPath("logfiles", "/", false) + "\\ErrorLog.xml"; }
                catch
                {
                    try { HttpContext.Current.Session["ErrorLog"] = System.Reflection.Assembly.GetEntryAssembly().Location.Substring(0, System.Reflection.Assembly.GetEntryAssembly().Location.LastIndexOf("\\") + 1); }
                    catch
                    { HttpContext.Current.Session["ErrorLog"] = string.Empty; }
                }
            }
            // Retrieve Log
            try { Log = (string)AppSetReader.GetValue("Log", typeof(string)); }
            catch
            {
                try { Log = HttpContext.Current.Request.MapPath("logfiles", "/", false) + "\\Log.xml"; }
                catch
                {
                    try { Log = System.Reflection.Assembly.GetEntryAssembly().Location.Substring(0, System.Reflection.Assembly.GetEntryAssembly().Location.LastIndexOf("\\") + 1) + "Log.xml"; }
                    catch
                    { Log = string.Empty; }
                }
            }
        }

        private bool Ping(string Server)
        {
            try
            {
                // Declarations
                Ping pingSender = new Ping();
                PingOptions options = new PingOptions();

                // Set options
                options.DontFragment = true;

                // Create a buffer of 32 bytes of data to be transmitted.
                string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                byte[] buffer = Encoding.ASCII.GetBytes(data);
                int timeout = 120;
                PingReply reply = pingSender.Send(Server, timeout, buffer, options);
                return (reply.Status == IPStatus.Success ? true : false);

            }
            catch { return false; }
        }

        private string ReaderToJSON(SqlDataReader dr, int EmptyRecords)
        {
            // Declarations
            StringBuilder sbJSON = new StringBuilder();
            int FieldCount = 0;
            bool AddComma = false;
            string varID = "$";

            try
            {
                // Get number of fields(columns) in the DataReader
                FieldCount = dr.FieldCount;

                // Initiate with our standard data object
                sbJSON.Append("{\"data\":[");

                // Loop of Datareader
                while (dr.Read())
                {
                    // Initiate row
                    sbJSON.Append((AddComma ? "," : "") + "{");

                    // Loop over fields
                    for (int i = 0; i < FieldCount; i++)
                    {
                        // Pair string will be field name in lowercase
                        sbJSON.Append("\"" + dr.GetName(i).ToString().Trim().ToLower() + "\":");
                        // Pair value will be field data cleansed so we have well-formed JSON
                        sbJSON.Append("\"" + dr[i].ToString().Trim().Replace("\"", "\\\"").Replace("\\", "\\\\").Replace("\b", "\\b").Replace("\b", "\\b").Replace("\t", "\\t").Replace("\n", "\\n").Replace("\f", "\\f").Replace("\r", "\\r")
                            + "\"" + (i < FieldCount - 1 ? "," : ""));
                    }

                    // Close object
                    sbJSON.Append("}");

                    // Reset flag
                    AddComma = true;
                }

                // Add empty records
                for (int y = 0; y < EmptyRecords; y++)
                {
                    // Initiate member object
                    sbJSON.Append((AddComma ? "," : "") + "{");

                    // Loop over fields
                    for (int i = 0; i < FieldCount; i++)
                    {
                        // Pair string will be field name in lowercase
                        sbJSON.Append("\"" + dr.GetName(i).ToString().Trim().ToLower() + "\":");
                        // Pair value will be field name plus counter enclosed in varID
                        sbJSON.Append("\"" + varID + dr.GetName(i).ToString().Trim().ToLower() + y.ToString() + varID + "\"" +
                            (i < FieldCount - 1 ? "," : ""));
                    }

                    // Close object
                    sbJSON.Append("}");

                    // Reset flag
                    AddComma = true;
                }

                // Close Array and objects
                sbJSON.Append("]}");

                // Close Datareader
                dr.Close();
            }
            catch (Exception ExceptionErr)
            {
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, ErrorLog);
                if (!dr.IsClosed) dr.Close();
                sbJSON.Remove(0, sbJSON.Length);
                sbJSON.Append("{\"data\":{\"resultcode\":false}}");
            }

            // Return XML
            return sbJSON.ToString();
        }

        private SqlParameterCollection ReturnSqlParams(SqlParameterCollection SqlParams, string StoredProcedure, string CallingFunc)
        {
            // Declarations
            SqlConnection conn = new SqlConnection(ConnectionString);
            SqlCommand comm = new SqlCommand(StoredProcedure, conn);

            try
            {
                // Set command type
                comm.CommandType = CommandType.StoredProcedure;

                // Add parameters
                for (int i = 0; i < SqlParams.Count; i++)
                    if (SqlParams[i].Direction == ParameterDirection.Output)
                        comm.Parameters.Add(SqlParams[i].ParameterName.ToString(), SqlParams[i].SqlDbType, SqlParams[i].Size).Direction = ParameterDirection.Output;
                    else
                        comm.Parameters.Add(SqlParams[i].ParameterName.ToString(), SqlParams[i].SqlDbType, SqlParams[i].Size).Value = SqlParams[i].Value;

                // Open Connection and execute
                comm.Connection.Open();
                comm.ExecuteNonQuery();

                // Close connection
                conn.Close();
            }
            catch (Exception ExceptionErr)
            {
                comm.Parameters["@Result"].Value = (comm.Parameters["@Result"].Value == null ? "F" : comm.Parameters["@Result"].Value);
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, ErrorLog, "Called from: " + CallingFunc);
                if (conn.State != ConnectionState.Closed) conn.Close();
            }

            // Return
            return comm.Parameters;
        }

        private DataTable SqlToDT(SqlParameterCollection SqlParams, string StoredProcedure, string CallingFunc)
        {
            // Declarations
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConnectionString);

            try
            {
                // Declarations
                SqlCommand comm = new SqlCommand(StoredProcedure, conn);
                SqlDataAdapter da = new SqlDataAdapter();

                // Set command type
                comm.CommandType = CommandType.StoredProcedure;

                // Add parameters
                for (int i = 0; i < SqlParams.Count; i++)
                    comm.Parameters.Add(SqlParams[i].ParameterName.ToString(), SqlParams[i].SqlDbType, SqlParams[i].Size).Value = SqlParams[i].Value;

                // Assign SQL command to Adapter
                da.SelectCommand = comm;

                // Open Connection
                comm.Connection.Open();

                // Fill DataSet
                da.Fill(ds);

                // Close connection
                conn.Close();
            }
            catch (Exception ExceptionErr)
            {
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, ErrorLog, "Called from: " + CallingFunc);
                if (conn.State != ConnectionState.Closed) conn.Close();
            }

            // Return result
            return ds.Tables[0];
        }

        private string SqlToJSON(SqlParameterCollection SqlParams, string StoredProcedure, string CallingFunc)
        {
            return SqlToJSON(SqlParams, StoredProcedure, CallingFunc, 0);
        }

        private string SqlToJSON(SqlParameterCollection SqlParams, string StoredProcedure, string CallingFunc, int EmptyRecords)
        {
            // Declarations
            string JSON = string.Empty;
            SqlConnection conn = new SqlConnection(ConnectionString);

            try
            {
                // Declarations
                SqlCommand comm = new SqlCommand(StoredProcedure, conn);

                // Set command type
                comm.CommandType = CommandType.StoredProcedure;

                // Add parameters
                for (int i = 0; i < SqlParams.Count; i++)
                    comm.Parameters.Add(SqlParams[i].ParameterName.ToString(), SqlParams[i].SqlDbType, SqlParams[i].Size).Value = SqlParams[i].Value;

                // Open Connection
                comm.Connection.Open();

                // Assign to datareader
                JSON = ReaderToJSON(comm.ExecuteReader(), EmptyRecords);

                // Close connection
                conn.Close();
            }
            catch (Exception ExceptionErr)
            {
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, ErrorLog, "Called from: " + CallingFunc);
                if (conn.State != ConnectionState.Closed) conn.Close();
            }

            // Return result
            return JSON;
        }

        private string ToTitleCase(string Text)
        {
            return System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Text);
        }

        private Boolean VerifyUser()
        {
            // Declarations
            Boolean Result = false;

            try
            {
                // Declarations
                DirectoryEntry de = new DirectoryEntry("WinNT://" + HttpContext.Current.User.Identity.Name.Replace("\\", "/").ToString());
                SqlCommand comm = new SqlCommand();
                SqlParameterCollection retParams = null;
                String FirstName, LastName;

                // Initialize class variable
                ConnectionString = HttpContext.Current.Session["ConnectionStr"].ToString();

                // Parse First and Last Name
                if (de.Properties["FullName"].Value.ToString().IndexOf(",") > 0)
                {
                    FirstName = de.Properties["FullName"].Value.ToString().Split(",".ToCharArray())[1].Trim();
                    LastName = de.Properties["FullName"].Value.ToString().Split(",".ToCharArray())[0].Trim();
                }
                else
                {
                    FirstName = de.Properties["FullName"].Value.ToString().Split(" ".ToCharArray())[1].Trim();
                    LastName = de.Properties["FullName"].Value.ToString().Split(" ".ToCharArray())[0].Trim();
                }

                // Get user's group to assign access
                for (int i = 0; i < System.Web.HttpContext.Current.Request.LogonUserIdentity.Groups.Count; i++)
                {
                    if (System.Web.HttpContext.Current.Request.LogonUserIdentity.Groups[i].Translate(typeof(System.Security.Principal.NTAccount)).ToString().ToUpper().Contains("PM_FULL"))
                    {
                        HttpContext.Current.Session["View"] = "FULL";
                        break;
                    }
                    if (System.Web.HttpContext.Current.Request.LogonUserIdentity.Groups[i].Translate(typeof(System.Security.Principal.NTAccount)).ToString().ToUpper().Contains("PM_WO"))
                    {
                        HttpContext.Current.Session["View"] = "WO";
                    }
                }

                // Add parameters
                comm.Parameters.Add("@WindowsUser", SqlDbType.VarChar, 50).Value = HttpContext.Current.User.Identity.Name.ToString();
                comm.Parameters.Add("@FirstName", SqlDbType.VarChar, 50).Value = FirstName;
                comm.Parameters.Add("@LastName", SqlDbType.VarChar, 50).Value = LastName;
                comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
                comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

                // Execute sql
                retParams = ReturnSqlParams(comm.Parameters, "upVerifyUser", System.Reflection.MethodBase.GetCurrentMethod().Name);

                // If user was verified, assign return and session variables
                if (retParams["@Result"].Value.ToString().ToUpper() == "S")
                {
                    Result = true;
                    HttpContext.Current.Session["UserAccountID"] =
                        retParams["@UserAccountID"].Value.ToString();
                    HttpContext.Current.Session["FullName"] = FirstName + " " + LastName;
                }
            }
            catch (Exception ExceptionErr)
            {
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, ErrorLog);
            }

            // Return result
            return Result;
        }

    #endregion

    }
}