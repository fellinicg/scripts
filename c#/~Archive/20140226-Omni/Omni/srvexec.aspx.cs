﻿using FCGLogger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Omni
{
    public partial class srvexec : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Declarations
                string action = (HttpContext.Current.Request.Params["a"] == null ? "" : HttpContext.Current.Request.Params["a"].ToString());

                // Set all responses to text
                HttpContext.Current.Response.ContentType = "text/plain";

                // Execute the action
                HttpContext.Current.Response.Write(ProcessRequest(action));
//                HttpContext.Current.Response.End();
            }
            catch (Exception ExceptionErr)
            {
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, HttpContext.Current.Session["ErrorLog"].ToString());
            }
        }
        #region Private Functions

        private string ProcessRequest(string func)
        {
            // Declarations
            Omni omni = new Omni();
            string Resp = string.Empty;
            //string CurrUA = HttpContext.Current.Session["UserAccountID"].ToString();

            try
            {
                switch (func)
                {
                    #region A
                    #endregion

                    #region D
                    #endregion

                    #region G
                    //case "getapps":
                    //    Resp = omni.GetApplications(Convert.ToBoolean(Context.Request.Params["o"].ToString()));
                    //    break;

                    //case "getattachments":
                    //    Resp = omni.GetAttachments(Context.Request.Params["t"].ToString());
                    //    break;

                    case "getsrvinfo":
                        Resp = omni.GetServerInfo(Context.Request.Params["s"].ToString());
                        break;

                    //case "getclcons":
                    //    Resp = omni.GetClientContacts();
                    //    break;

                    //case "getcnumtypes":
                    //    Resp = omni.GetContactNumTypes();
                    //    break;

                    //case "getcnums":
                    //    Resp = omni.GetContactNums(
                    //        (string.IsNullOrEmpty(Context.Request.Params["u"]) ?
                    //            CurrUA : Context.Request.Params["u"].ToString()),
                    //            (string.IsNullOrEmpty(Context.Request.Params["o"]) ? false :
                    //            Convert.ToBoolean(Context.Request.Params["o"].ToString())));
                    //    break;

                    case "getsrvs":
                        Resp = omni.GetServers();
                        break;

                    #endregion

                    #region H
                    #endregion

                    #region M
                    #endregion

                    #region R
                    #endregion

                    #region S
                    #endregion

                    #region U
                    #endregion

                    default:
                        break;
                }

            }
            catch (Exception ExceptionErr)
            {
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, HttpContext.Current.Session["ErrorLog"].ToString());
            }

            // Return data
            return Resp;
        }

        #endregion
    
    }
}