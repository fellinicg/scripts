/// <reference path="x.js" />
/// <reference path="common.js" />
var exec_url = location.href.replace(location.pathname, "/") + "srvexec.aspx";
// GETSRVS
function getsrvs() {
    var oTable = $('#servers').dataTable({
        "sDom": 'Rlfrtip',
        //"iDisplayLength": 50,
        "bProcessing": true,
        //"bServerSide": true,
        "sAjaxSource": "srvexec.aspx?a=getsrvs",
        "sAjaxDataProp": "data",
        "aoColumns": [
            { "mData": "name" },
            { "mData": "type" },
            { "mData": "serialnum" },
            { "mData": "status" }
        ]
    });
    getsrvs2();
    return void (0);
}
function getsrvs2() {

    function fnFormatDetails(oTable, nTr, id) {
        var sOut = "";
        $.ajax({
            url: "srvexec.aspx?a=getsrvinfo&s=" + id,
            async: false,
            dataType: 'json',
            success: function (json) {
                sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
                sOut += '<tr><td>Department Name:</td><td>' + json.data[0].departmentname + '</td></tr>';
                sOut += '<tr><td>Alloy ID:</td><td>' + json.data[0].alloyid + '</td></tr>';
                sOut += '<tr><td>Even more info:</td><td>And any further details here</td></tr>';
                sOut += '</table>';
            }
        });
        return sOut;
    }

    var oTable = $('#servers2').dataTable({
        "sDom": 'Rlfrtip',
        "aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
        "iDisplayLength": 5,
        "bProcessing": true,
        "sPaginationType": "full_numbers",
        //"bServerSide": true,
        "sAjaxSource": "srvexec.aspx?a=getsrvs",
        "sAjaxDataProp": "data",
        "aoColumns": [
            {
                "mData": null, "bSortable": false, "mRender": function (data, type, full) {
                    return '<a href=javascript:void(0)><img id="' + full.serverid + '" src="../images/details_open.png"></a>';
                }
            },
            { "mData": "name" },
            { "mData": "type" },
            { "mData": "serialnum" },
            { "mData": "status" }
        ],
        "aaSorting": [[1, 'asc']]
    });

    $('#servers2 tbody td img').live('click', function () {
        alert(this.id);
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr)) {
            /* This row is already open - close it */
            this.src = "../images/details_open.png";
            oTable.fnClose(nTr);
        }
        else {
            /* Open this row */
            this.src = "../images/details_close.png";
            oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr, this.id), 'details');
        }
    });

    return void (0);

}

function getsrvs3() {
    /*
     * Insert a 'details' column to the table
     */
    //var nCloneTh = document.createElement('th');
    //var nCloneTd = document.createElement('td');
    //nCloneTd.innerHTML = '<img src="../images/details_open.png">';
    //nCloneTd.className = "center";

    //$('#servers2 thead tr').each(function () {
    //    this.insertBefore(nCloneTh, this.childNodes[0]);
    //});

    //$('#servers2 tbody tr').each(function () {
    //    this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
    //});

    function fnFormatDetails(oTable, nTr) {
        var aData = oTable.fnGetData(nTr);
        var sOut = ""
        var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
        sOut += '<tr><td>Rendering engine:</td><td>' + aData.serverid + '</td></tr>';
        sOut += '<tr><td>Link to source:</td><td>Could provide a link here</td></tr>';
        sOut += '<tr><td>Extra info:</td><td>And any further details here (images etc)</td></tr>';
        sOut += '</table>';

        return sOut;
    }

    //$('#servers2 tbody tr').click(function () {
    //    alert("hi");
    //    if (oTable.fnIsOpen(this)) {
    //        oTable.fnClose(this);
    //    } else {
    //        oTable.fnOpen(this, "Temporary row opened", "info_row");
    //    }
    //});

    var oTable = $('#servers2').dataTable({
        "sDom": 'Rlfrtip',
        //        "iDisplayLength": 25,
        "iDisplayLength": 3,
        "bProcessing": true,
        //"bServerSide": true,
        "sAjaxSource": "srvexec.aspx?a=getsrvs",
        "sAjaxDataProp": "data",
        "aoColumns": [
            {
                "mData": null, "bSortable": false, "mRender": function (data, type, full) {
                    //                    return '<a href="main.php?do=labor&action=edit&sid=' + full.serverid + '"><img src="../images/details_open.png"></a>';
                    return '<img src="../images/details_open.png">';
                }
            },
            { "mData": "name" },
            { "mData": "type" },
            { "mData": "serialnum" },
            { "mData": "status" }
        ],
        //        "aoColumnDefs": [
        //           { "bSortable": false, "aTargets": [ 0 ] }
        //        ],
        "aaSorting": [[1, 'asc']]
    });

    //$('#servers2 tbody td img').live('click', function () {
    //    alert('crap');
    //});

    //$('#servers2').dataTable({
    //    fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
    //        // Row click
    //        $(nRow).on('click', function () {
    //            console.log('Row Clicked. Look I have access to all params, thank You closures.', this, aData, iDisplayIndex, iDisplayIndexFull);
    //        });

    //        // Cell click
    //        $('td', nRow).on('click', function () {
    //            console.log('Col Clicked.', this, aData, iDisplayIndex, iDisplayIndexFull);
    //        });
    //    }
    //});

    $('#servers2 tbody td img').live('click', function () {
        //alert('crap');
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr)) {
            /* This row is already open - close it */
            this.src = "../images/details_open.png";
            oTable.fnClose(nTr);
        }
        else {
            /* Open this row */
            this.src = "../images/details_close.png";
            oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
    });

    return void (0);

}

