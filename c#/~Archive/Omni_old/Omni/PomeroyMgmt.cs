﻿using FCGErrorLogger;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace Pomeroy
{
	public class PomeroyMgmt
	{
	#region Declarations

		private bool cIsDevelopmentPlatform = false;
		private string cConnectionStr = string.Empty;
		private string cXmlDataPath = string.Empty;
		private string cErrorLog = string.Empty;

	#endregion

	#region Public Functions

		public string AddApplication(string Description)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@Description", SqlDbType.VarChar, 100).Value = Description.Trim();
				comm.Parameters.Add("@ApplicationID", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upAddApplication", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string AddContactNum(string UserAccountID, string ContactNumType, string ContactNum)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserAccountID;
				comm.Parameters.Add("@ContactNumType", SqlDbType.Char, 2).Value = ContactNumType;
				comm.Parameters.Add("@ContactNum", SqlDbType.VarChar, 20).Value = ContactNum.Trim();
				comm.Parameters.Add("@ContactNumID", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upAddContactNum", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{	FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog); }

			// Return result
			return Result;
		}

		public string AddClientContact(string FirstName, string MiddleInitial, string LastName, string Phone, string Email)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@FirstName", SqlDbType.VarChar, 50).Value = FirstName.Trim();
				comm.Parameters.Add("@MiddleInitial", SqlDbType.Char, 1).Value = (MiddleInitial.Trim().Length == 0 ? (object)DBNull.Value : MiddleInitial.Trim());
				comm.Parameters.Add("@LastName", SqlDbType.VarChar, 80).Value = LastName.Trim();
				comm.Parameters.Add("@Phone", SqlDbType.VarChar, 20).Value = (Phone.Trim().Length == 0 ? (object)DBNull.Value : Phone.Trim());
				comm.Parameters.Add("@Email", SqlDbType.VarChar, 20).Value = (Email.Trim().Length == 0 ? (object)DBNull.Value : Email.Trim());
				comm.Parameters.Add("@ClientContactID", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upAddClientContact", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{ FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog); }

			// Return result
			return Result;
		}

		public string AddDatabase(string DBName, string ServerID, string SQLInstanceID, string IsSupported, string IsOnline)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@DBName", SqlDbType.VarChar, 50).Value = DBName.Trim();
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;
				comm.Parameters.Add("@SQLInstanceID", SqlDbType.Int, 4).Value = EmptyToNull(SQLInstanceID);
				comm.Parameters.Add("@DepartmentID", SqlDbType.SmallInt, 2).Value = DBNull.Value;
				comm.Parameters.Add("@ClientContactID", SqlDbType.Int, 4).Value = DBNull.Value;
				comm.Parameters.Add("@IsOnline", SqlDbType.Bit, 1).Value = Convert.ToBoolean(IsOnline);
				comm.Parameters.Add("@IsSupported", SqlDbType.Bit, 1).Value = Convert.ToBoolean(IsSupported);
				comm.Parameters.Add("@DatabaseID", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upAddDatabase", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string AddDbApp(string DatabaseID, string ApplicationID)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@DatabaseID", SqlDbType.Int, 4).Value = DatabaseID;
				comm.Parameters.Add("@ApplicationID", SqlDbType.Int, 4).Value = ApplicationID;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upAddDbApp", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string AddEmailAddress(string UserAccountID, string EmailAddressType, string EmailAddress)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserAccountID;
				comm.Parameters.Add("@EmailAddressType", SqlDbType.Char, 1).Value = EmailAddressType.Trim();
				comm.Parameters.Add("@EmailAddress", SqlDbType.VarChar, 120).Value = EmailAddress.Trim();
				comm.Parameters.Add("@EmailAddressID", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upAddEmailAddress", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{ FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog); }

			// Return result
			return Result;
		}

		public string AddNote(string SourceID, string NoteType, string Note, string UserAccountID)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@SourceID", SqlDbType.Int).Value = SourceID;
				comm.Parameters.Add("@NoteType", SqlDbType.Char, 1).Value = NoteType.Trim();
				comm.Parameters.Add("@Note", SqlDbType.VarChar, 200).Value = Note.Trim();
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int).Value = UserAccountID;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upAddNote", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string AddServer(string ServerName, string ServerType, string OnPawanet, string IsOnline,
			string SupportBeginDate, string DeploymentStatusID, string UserAccountID, string SupportType, string OSType)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@ServerName", SqlDbType.VarChar, 50).Value = ServerName.Trim();
				comm.Parameters.Add("@ServerType", SqlDbType.Char, 1).Value = ServerType.Trim();
				comm.Parameters.Add("@OnPawanet", SqlDbType.Bit, 1).Value = Convert.ToBoolean(OnPawanet);
				comm.Parameters.Add("@IsOnline", SqlDbType.Bit, 1).Value = Convert.ToBoolean(IsOnline);
				comm.Parameters.Add("@SupportBeginDate", SqlDbType.DateTime, 8).Value = SupportBeginDate;
				comm.Parameters.Add("@DeploymentStatusID", SqlDbType.Int, 4).Value = DeploymentStatusID;
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserAccountID;
				comm.Parameters.Add("@OSType", SqlDbType.VarChar, 20).Value = OSType.Trim();
				comm.Parameters.Add("@SupportType", SqlDbType.Char, 1).Value = SupportType.Trim();
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upAddServer", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string AddServerRole(string ServerID, string RoleID)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;
				comm.Parameters.Add("@RoleID", SqlDbType.Int, 4).Value = RoleID;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upAddServerRole", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string AddSrvDeploymentStatus(string ServerID, string DeploymentStatusID)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;
				comm.Parameters.Add("@DeploymentStatusID", SqlDbType.Int, 4).Value = DeploymentStatusID;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upAddSrvDeploymentStatus", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string AddServerUATask(string ServerIDList, string UATaskID)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@ServerIDList", SqlDbType.VarChar, 4000).Value = ServerIDList.Trim();
				comm.Parameters.Add("@UATaskID", SqlDbType.Int, 4).Value = UATaskID;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upAddServerUATask", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string AddTimeEntry(string UserAccountID, string WeekID, string TimeTypeID, string Info, string Hours, string StartTime, string EndTime)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserAccountID;
				comm.Parameters.Add("@WeekID", SqlDbType.SmallInt, 2).Value = WeekID;
				comm.Parameters.Add("@TimeTypeID", SqlDbType.TinyInt, 1).Value = TimeTypeID;
				comm.Parameters.Add("@Info", SqlDbType.VarChar, 500).Value = Info;
				comm.Parameters.Add("@Hours", SqlDbType.Float).Value = Hours;
				comm.Parameters.Add("@StartTime", SqlDbType.VarChar, 10).Value = StartTime;
				comm.Parameters.Add("@EndTime", SqlDbType.VarChar, 10).Value = EndTime;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upAddTimeEntry", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{ FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog); }

			// Return result
			return Result;
		}

		public string AddTimeEntryAttachment(string TimeEntryID, string FileName, byte[] FileContent)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@TimeEntryID", SqlDbType.Int, 4).Value = TimeEntryID;
				comm.Parameters.Add("@FileName", SqlDbType.VarChar, 500).Value = FileName;
				comm.Parameters.Add("@BinaryContent", SqlDbType.VarBinary).Value = FileContent;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upAddTimeEntryAttachment", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string AddUARoleServer(string UARoleID, string UserAccountID, string ServerID, string IsPrimary)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@UARoleID", SqlDbType.Int, 4).Value = UARoleID;
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserAccountID;
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;
				comm.Parameters.Add("@IsPrimary", SqlDbType.Bit, 1).Value = Convert.ToBoolean(Convert.ToInt16(IsPrimary));
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upAddUARoleServer", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string AddUATask(string UserAccountID, string TaskID, string LogPath)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserAccountID;
				comm.Parameters.Add("@TaskID", SqlDbType.Int, 4).Value = TaskID;
				comm.Parameters.Add("@LogPath", SqlDbType.VarChar, 500).Value = LogPath.Trim();
				comm.Parameters.Add("@UATaskID", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upAddUATask", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = (retParams["@Result"].Value.ToString().ToUpper() == "S" ? retParams["@Result"].Value.ToString() + "," + retParams["@UATaskID"].Value.ToString() : Result);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string AddWOComment(string WorkOrderID, string WOComment, string UserAccountID)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@WorkOrderID", SqlDbType.Int).Value = WorkOrderID;
				comm.Parameters.Add("@WOComment", SqlDbType.VarChar, 3000).Value = WOComment.Trim();
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int).Value = UserAccountID;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upAddWOComment", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string AddWODoc(string WorkOrderID, string WorkOrderDoc)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@WorkOrderID", SqlDbType.Int, 4).Value = WorkOrderID;
				comm.Parameters.Add("@WorkOrderDoc", SqlDbType.VarChar, 100).Value = WorkOrderDoc.Trim();
				comm.Parameters.Add("@WorkOrderDocID", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upAddWODoc", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = (retParams["@Result"].Value.ToString().ToUpper() == "S" ? retParams["@Result"].Value.ToString() + "," + retParams["@WorkOrderDocID"].Value.ToString() : Result);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string AddWorkOrder(string Title, string Description, string DepartmentID, string ReceivedDate, string IsBillable, 
			string WOStatusCode, string ClientContacts, string UserAccountID, string ServiceOrder, string ClientProject,
			string DesiredCompletionDate, string RequestDate, string EstCost, string FixedCost,
			string Comment, string AssignedTos)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@Title", SqlDbType.VarChar, 300).Value = Title.Trim();
				comm.Parameters.Add("@Description", SqlDbType.VarChar, 2000).Value = Description.Trim();
				comm.Parameters.Add("@DepartmentID", SqlDbType.Int, 4).Value = DepartmentID;
				comm.Parameters.Add("@ReceivedDate", SqlDbType.SmallDateTime, 4).Value = ReceivedDate;
				comm.Parameters.Add("@IsBillable", SqlDbType.Bit, 1).Value = Convert.ToBoolean(IsBillable);
				comm.Parameters.Add("@WOStatusCode", SqlDbType.Char, 1).Value = WOStatusCode.Trim();
				comm.Parameters.Add("@ClientContacts", SqlDbType.VarChar, 1000).Value = ClientContacts.Trim();
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserAccountID;
				comm.Parameters.Add("@ServiceOrder", SqlDbType.VarChar, 200).Value = EmptyToNull(ServiceOrder);
				comm.Parameters.Add("@ClientProject", SqlDbType.VarChar, 50).Value = EmptyToNull(ClientProject);
				comm.Parameters.Add("@DesiredCompletionDate", SqlDbType.SmallDateTime, 4).Value = EmptyToNull(DesiredCompletionDate);
				comm.Parameters.Add("@RequestDate", SqlDbType.SmallDateTime, 4).Value = EmptyToNull(RequestDate);
				comm.Parameters.Add("@EstCost", SqlDbType.Money, 8).Value = EmptyToNull(EstCost);
				comm.Parameters.Add("@FixedCost", SqlDbType.Money, 8).Value = EmptyToNull(FixedCost);
				comm.Parameters.Add("@Comment", SqlDbType.VarChar, 3000).Value = EmptyToNull(Comment);
				comm.Parameters.Add("@AssignedTos", SqlDbType.VarChar, 1000).Value = EmptyToNull(AssignedTos);
				comm.Parameters.Add("@WorkOrderID", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upAddWorkOrder", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string DataTableToXML(DataTable dt, string ResultCode)
		{
			// Declarations
			StringBuilder sbXML = new StringBuilder();
			int FieldCount = 0;

			try
			{
				// Get number of fields(columns) in the DataTable
				FieldCount = dt.Columns.Count;

				// Initiate with our standard nodes
				sbXML.Append("<?xml version=\"1.0\"?><data><resultcode>" + ResultCode + "</resultcode>");

				// Loop of DataTable
				for (int i = 0; i < dt.Rows.Count; i++)
				{
					// Row node
					sbXML.Append("<row>");

					// Loop over fields
					for (int j = 0; j < FieldCount; j++)
					{
						// Node will be field name in lowercase
						sbXML.Append("<" + dt.Columns[j].ColumnName.ToLower() + ">");
						// Contents of node will be field data cleansed so we have well-formed XML
						sbXML.Append(dt.Rows[i][j].ToString().Trim().Replace("&", "&amp;").Replace(">", "&gt;").Replace("<", "&lt;").Replace("\"", "&quot;").Replace("'", "&apos;"));
						// Close the node
						sbXML.Append("</" + dt.Columns[j].ColumnName.ToLower() + ">");
					}

					// Close the node
					sbXML.Append("</row>");
				}

				// Close the node
				sbXML.Append("</data>");
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return XML
			return sbXML.ToString();
		}

		public string DeleteApplication(string ApplicationID)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@ApplicationID", SqlDbType.Int, 4).Value = ApplicationID;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upDeleteApplication", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{ FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog); }

			// Return result
			return Result;
		}

		public string DeleteAttachment(string TimeEntryAttachmentID)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@TimeEntryAttachmentID", SqlDbType.Int, 4).Value = TimeEntryAttachmentID;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upDeleteAttachment", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{ FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog); }

			// Return result
			return Result;
		}

		public string DeleteClientContact(string ClientContactID)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@ClientContactID", SqlDbType.Int, 4).Value = ClientContactID;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upDeleteClientContact", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{ FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog); }

			// Return result
			return Result;
		}

		public string DeleteContactNum(string ContactNumID, string UserAccountID)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@ContactNumID", SqlDbType.Int, 4).Value = ContactNumID;
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserAccountID;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upDeleteContactNum", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{	FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog); }

			// Return result
			return Result;
		}

		public string DeleteEmailAddr(string EmailAddressID, string UserAccountID)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@EmailAddressID", SqlDbType.Int, 4).Value = EmailAddressID;
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserAccountID;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upDeleteEmailAddr", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{ FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog); }

			// Return result
			return Result;
		}

		public string DeleteIP(string ServerID, string IPAddress)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@ServerID", SqlDbType.Int).Value = ServerID;
				comm.Parameters.Add("@IPAddress", SqlDbType.VarChar, 64).Value = IPAddress.Trim();
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upDeleteIP", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string DeleteServerRole(string ServerRoleID)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@ServerRoleID", SqlDbType.Int, 4).Value = ServerRoleID;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upDeleteServerRole", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string DeleteUARoleServer(string UARoleID, string UserAccountID, string ServerID)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@UARoleID", SqlDbType.Int, 4).Value = UARoleID;
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserAccountID;
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upDeleteUARoleServer", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string DeleteWODoc(string WorkOrderDocIDs)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@WorkOrderDocIDs", SqlDbType.VarChar, 1000).Value = WorkOrderDocIDs;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upDeleteWODoc", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string DelimitDataTableCol(DataTable dt, int ColIdx, string Delimiter)
		{
			// Declarations
			StringBuilder sbDelimited = new StringBuilder();

			try
			{
				// Loop of DataTable
				for (int i = 0; i < dt.Rows.Count; i++)
				{
					// Delimit field value
					sbDelimited.Append(dt.Rows[i][ColIdx].ToString() + Delimiter);
				}
				// Remove trailing delimiter
				sbDelimited.Remove(sbDelimited.Length - 1, 1);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return Delimited string
			return sbDelimited.ToString();
		}

		public string GetApplications(bool InclAddNew)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetApplications", System.Reflection.MethodBase.GetCurrentMethod().Name, (InclAddNew ? 1 : 0));

				// Add a record to the datable upon callers request
				if (InclAddNew) rs = rs.Replace("$applicationid0$", "-1");
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public DataTable GetAttachmentDT(string TimeEntryAttachmentID)
		{
			// Declarations
			DataTable dt = null;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@TimeEntryAttachmentID", SqlDbType.Int).Value = TimeEntryAttachmentID;

				// Get data
				dt = SqlToDT(comm.Parameters, "upGetAttachment", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return dt;
		}

		public string GetAttachments(string TimeEntryID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@TimeEntryID", SqlDbType.Int).Value = TimeEntryID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetAttachments", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public DataTable GetAttachmentsDT(string TimeEntryID)
		{
			// Declarations
			DataTable dt = null;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@TimeEntryID", SqlDbType.Int).Value = TimeEntryID;

				// Get data
				dt = SqlToDT(comm.Parameters, "upGetAttachments", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return dt;
		}

		public string GetAvailServerRoles(string ServerID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetAvailServerRoles", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetAvailWOAssignedTo(string WorkOrderID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@WorkOrderID", SqlDbType.Int, 4).Value = WorkOrderID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetAvailWOAssignedTo", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetAvailWORequestor(string WorkOrderID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@WorkOrderID", SqlDbType.Int, 4).Value = WorkOrderID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetAvailWORequestor", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetClientContacts()
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetClientContacts", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetContactNums(string UserAccountID, bool InclAddNew)
		{
			// Declarations
			string rs	= string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserAccountID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetContactNums", System.Reflection.MethodBase.GetCurrentMethod().Name, (InclAddNew ? 1 : 0));

				// Add empty record, if necessary
				if (InclAddNew)
				{
					rs = rs.Replace("$contactnumid0$", "-1");
				}
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetContactNumTypes()
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetContactNumTypes", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetDatabases(string ServerID, string SQLInstanceID)
    {
        // Declarations
			string rs = string.Empty;

        try
        {
          // Declarations
          SqlCommand comm = new SqlCommand();

          // Add parameters
          comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;
					comm.Parameters.Add("@SQLInstanceID", SqlDbType.Int, 4).Value = EmptyToNull(SQLInstanceID);

					// Get data
					rs = SqlToJSON(comm.Parameters, "upGetDatabases", System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
        catch (Exception ExceptionErr)
        {
            FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
        }

        // Return result
				return rs;
    }

		public string GetDbSrvInfo(string ServerID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetDbSrvInfo", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetDepartments()
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetDepartments", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetDeploymentStatuses()
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetDeploymentStatuses", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public DataTable GetDeploymentStatusesDT()
		{
			// Declarations
			DataTable dt = null;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				dt = SqlToDT(comm.Parameters, "upGetDeploymentStatuses", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return dt;
		}

		public string GetEmailAddresses(string UserAccountID, bool InclAddNew)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserAccountID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetEmailAddresses", System.Reflection.MethodBase.GetCurrentMethod().Name, (InclAddNew ? 1 : 0));

				// Add empty record, if necessary
				if (InclAddNew)
				{
					rs = rs.Replace("$emailaddressid0$", "-1");
				}
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetEmailAddressTypes()
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetEmailAddressTypes", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetDNSEntry(string Server)
		{
			// Declarations
			string IP = string.Empty;

			try 
			{
				IP = Dns.GetHostEntry(Server.ToString()).AddressList[0].ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog, Server);
			}

			return IP;
		}

		public string GetLatesNote(string SourceID, string NoteType)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@SourceID", SqlDbType.Int).Value = SourceID;
				comm.Parameters.Add("@NoteType", SqlDbType.Char, 1).Value = NoteType;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetLatestNote", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetLatestWOComment(string WorkOrderID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@WorkOrderID", SqlDbType.Int).Value = WorkOrderID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetLatestWOComment", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetMachineInfo(string Server)
		{
			// Declarations
			StringBuilder sbMachineInfo = new StringBuilder();
			string IP = string.Empty;

			// Retrieve IP Addresses
			IP = GetDNSEntry(Server);

			// Use IP instead of server name
			Server = IP;

			// Exit if we can't ping
			if (string.IsNullOrEmpty(IP) || !Ping(Server)) return "{\"data\":{\"resultcode\":false}}";

			try
			{
				// Declarations
				ConnectionOptions options = new ConnectionOptions();
				ManagementScope scope;
				ObjectQuery query;
				ManagementObjectSearcher searcher;
				ManagementObjectCollection queryCollection;
				ManagementObjectCollection.ManagementObjectEnumerator props;

				// Connection options
				options.Authentication = AuthenticationLevel.Call;
				options.EnablePrivileges = true;
				options.Authority = "ntlmdomain:PANYNJ";
				options.Impersonation = ImpersonationLevel.Impersonate;
				scope = new ManagementScope("\\\\" + Server + "\\root\\cimv2", options);
				scope.Connect();

				// Build JSON
				sbMachineInfo.Append("\"row\":[{\"ipaddress\":\"" + IP + "\"");

				// Query system for Operating System information      
				query = new SelectQuery("select * from Win32_OperatingSystem");
				searcher = new ManagementObjectSearcher(scope, query);
				queryCollection = searcher.Get();
				props = queryCollection.GetEnumerator();
				props.MoveNext();
				sbMachineInfo.Append(",\"os\":\"" + props.Current.Properties["Caption"].Value.ToString() + "\"");
				sbMachineInfo.Append(",\"osservicepack\":\"" + props.Current.Properties["ServicePackMajorVersion"].Value.ToString() + "\"");
				sbMachineInfo.Append(",\"osver\":\"" + props.Current.Properties["Version"].Value.ToString() + "\"");
				sbMachineInfo.Append(",\"osprodid\":\"" + props.Current.Properties["SerialNumber"].Value.ToString() + "\"");

				// Query system for Computer System information
				query = new SelectQuery("select * from Win32_ComputerSystem");
				searcher = new ManagementObjectSearcher(scope, query);
				queryCollection = searcher.Get();
				props = queryCollection.GetEnumerator();
				props.MoveNext();
				sbMachineInfo.Append(",\"manufacturer\":\"" + props.Current.Properties["Manufacturer"].Value.ToString() + "\"");
				sbMachineInfo.Append(",\"memorybytes\":\"" + props.Current.Properties["TotalPhysicalMemory"].Value.ToString() + "\"");
				sbMachineInfo.Append(",\"model\":\"" + props.Current.Properties["Model"].Value.ToString() + "\"");
				sbMachineInfo.Append(",\"processors\":\"" + props.Current.Properties["NumberOfProcessors"].Value.ToString() + "\"");

				// Query system for BIOS information
				query = new SelectQuery("select * from Win32_BIOS");
				searcher = new ManagementObjectSearcher(scope, query);
				queryCollection = searcher.Get();
				props = queryCollection.GetEnumerator();
				props.MoveNext();
				sbMachineInfo.Append(",\"serialnum\":\"" + props.Current.Properties["SerialNumber"].Value.ToString() + "\"");

				// Query system for Processor information
				query = new SelectQuery("select * from Win32_Processor");
				searcher = new ManagementObjectSearcher(scope, query);
				queryCollection = searcher.Get();
				props = queryCollection.GetEnumerator();
				props.MoveNext();
				sbMachineInfo.Append(",\"processorinfo\":\"" + props.Current.Properties["Name"].Value.ToString() + "\"");

				// Finalize JSON
				sbMachineInfo.Append("}]}}");
				sbMachineInfo.Insert(0, "{\"data\":{\"resultcode\":true,");

			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog;
				if (ExceptionErr.Message.IndexOf("The RPC server is unavailable") < 0)
					errLog = new Logger(ExceptionErr, cErrorLog);
				sbMachineInfo.Remove(0, sbMachineInfo.Length);
				sbMachineInfo.Append("{\"data\":{\"resultcode\":false}}");
			}

			// Return
			return sbMachineInfo.ToString();
}

		public string GetMachineServices(string Server)
		{
			return GetMachineServices(Server, "", "");
		}

		public string GetMachineServices(string Server, string Username, string Password)
		{
			// Declarations 
			StringBuilder sbMachineServices = new StringBuilder();
			string IP = string.Empty;
			bool AddComma = false;

			// Retrieve IP Addresses
			IP = GetDNSEntry(Server);

			// Use IP instead of server name
			Server = IP;

			// Exit if we can't ping
			if (string.IsNullOrEmpty(IP) || !Ping(Server)) return "{\"data\":{\"resultcode\":false}}";

			try
			{
				// Declarations
				bool Win2000 = false;
				DataTable dtServices;
				ManagementScope scope;
				ObjectQuery query;
				ManagementObjectSearcher searcher;
				ManagementObjectCollection queryCollection;
				ConnectionOptions options = new ConnectionOptions();

				// Get list of services
				dtServices = GetWinServices();

				// Exit if DB doesn't return list of services to check for
				if (dtServices.Rows.Count == 0) return "{\"data\":{\"resultcode\":false}}";

				// Connection options
				options.Authentication = AuthenticationLevel.Call;
				options.EnablePrivileges = true;
				options.Authority = "ntlmdomain:PANYNJ";
				options.Impersonation = ImpersonationLevel.Impersonate;
				options.Username = (Username.Trim().Length == 0 ? null : Username);
				options.Password = (Password.Trim().Length == 0 ? null : Password);
				options.Timeout = new TimeSpan(0, 0, 0, 10);

				// Make connection
				scope = new ManagementScope("\\\\" + Server + "\\root\\cimv2", options);
				scope.Connect();

				// Query system for Operating System information      
				query = new SelectQuery("select Version from Win32_OperatingSystem");
				searcher = new ManagementObjectSearcher(scope, query);
				queryCollection = searcher.Get();
				foreach (ManagementObject mo in queryCollection)
				{
					Win2000 = mo["Version"].ToString().StartsWith("5.0");
				}

				// Build JSON
				sbMachineServices.Append("\"row\":[");

				if (Win2000)
				{
					// Add column to track installation
					DataColumn inst = new DataColumn("installed", typeof(bool));
					inst.DefaultValue = false;
					dtServices.Columns.Add(inst);

					// Query system for Services information
					query = new SelectQuery("Select PathName,Name,DisplayName,Started from Win32_Service");
					searcher = new ManagementObjectSearcher(scope, query);
					queryCollection = searcher.Get();
					foreach (ManagementObject mo in queryCollection)
					{
						for (int i = 0; i < dtServices.Rows.Count; i++)
						{
							if (mo["PathName"].ToString().IndexOf(dtServices.Rows[i]["Exe"].ToString().Trim()) > 0)
							{
								sbMachineServices.Append((AddComma ? "," : "") + "{");
								sbMachineServices.Append("\"caption\":\"" + mo["Name"].ToString().Trim() + "\"");
								sbMachineServices.Append(",\"dispname\":\"" + mo["DisplayName"].ToString().Trim() + "\"");
								sbMachineServices.Append(",\"installed\":\"true\"");
								sbMachineServices.Append(",\"started\":\"" + mo["Started"].ToString().Trim() + "\"");
								dtServices.Rows[i]["installed"] = true;
								sbMachineServices.Append("}");
								AddComma = true;
								break;
							}
						}
					}
					// Add uninstalled service to datatable
					for (int i = 0; i < dtServices.Rows.Count; i++)
					{
						if (!Convert.ToBoolean(dtServices.Rows[i]["installed"].ToString()))
						{
							sbMachineServices.Append((AddComma ? "," : "") + "{");
							sbMachineServices.Append("\"caption\":\"" + dtServices.Rows[i]["DisplayName"].ToString().Trim() + "\"");
							sbMachineServices.Append(",\"dispname\":\"" + dtServices.Rows[i]["DisplayName"].ToString().Trim() + "\"");
							sbMachineServices.Append(",\"installed\":\"false\"");
							sbMachineServices.Append(",\"started\":\"false\"");
							sbMachineServices.Append("}");
							AddComma = true;
						}
					}
				}
				else
				{ 
					// Loop over services to search for
					for (int i = 0; i < dtServices.Rows.Count; i++)
					{
						// Query system for Services information
						query = new SelectQuery("Select Name,DisplayName,Started from Win32_Service where pathname like '%" + dtServices.Rows[i]["Exe"].ToString().Trim() + "%'");
						searcher = new ManagementObjectSearcher(scope, query);
						queryCollection = searcher.Get();
						if (queryCollection.Count == 0)
						{
							sbMachineServices.Append((AddComma ? "," : "") + "{");
							sbMachineServices.Append("\"caption\":\"" + dtServices.Rows[i]["DisplayName"].ToString().Trim() + "\"");
							sbMachineServices.Append(",\"dispname\":\"" + dtServices.Rows[i]["DisplayName"].ToString().Trim() + "\"");
							sbMachineServices.Append(",\"installed\":\"false\"");
							sbMachineServices.Append(",\"started\":\"false\"");
							sbMachineServices.Append("}");
							AddComma = true;
						}
						else
							foreach (ManagementObject mo in queryCollection)
							{
								sbMachineServices.Append((AddComma ? "," : "") + "{");
								sbMachineServices.Append("\"caption\":\"" + mo["Name"].ToString().Trim() + "\"");
								sbMachineServices.Append(",\"dispname\":\"" + mo["DisplayName"].ToString().Trim() + "\"");
								sbMachineServices.Append(",\"installed\":\"true\"");
								sbMachineServices.Append(",\"started\":\"" + mo["Started"].ToString().Trim() + "\"");
								sbMachineServices.Append("}");
								AddComma = true;
							}
					}
				}

				// Finalize JSON
				sbMachineServices.Append("]}}");
				sbMachineServices.Insert(0, "{\"data\":{\"resultcode\":true,");
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog;
				if (ExceptionErr.Message.IndexOf("The RPC server is unavailable") < 0)
					errLog = new Logger(ExceptionErr, cErrorLog);
				sbMachineServices.Remove(0, sbMachineServices.Length);
				sbMachineServices.Append("{\"data\":{\"resultcode\":false}}");
			}

			// Return
			return sbMachineServices.ToString();
		}

		public string GetManufacturerList()
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetManufacturerList", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetModelList()
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetModelList", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public DataTable GetNotes(string SourceID, string NoteType)
		{
			// Declarations
			DataTable dt = null;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@SourceID", SqlDbType.Int).Value = SourceID;
				comm.Parameters.Add("@NoteType", SqlDbType.Char, 1).Value = NoteType;

				// Get data
				dt = SqlToDT(comm.Parameters, "upGetNotes", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return dt;
		}

		public string GetOSList()
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetOSList", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetOSVerList()
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetOSVerList", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetPrimaryDBA(string ServerID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetPrimaryDBA", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetPrimarySA(string ServerID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetPrimarySA", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetProcessorInfoList()
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetProcessorInfoList", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public DataView GetReportData(string StoredProcedure)
		{
			// Declarations
			DataView dv = null;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				dv = new DataView(SqlToDT(comm.Parameters, StoredProcedure, System.Reflection.MethodBase.GetCurrentMethod().Name));
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return dv;
		}

		public DataTable GetReportInfo(string ReportID)
		{
			// Declarations
			DataTable dt = new DataTable();

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@ReportID", SqlDbType.SmallInt, 2).Value = ReportID;

				// Get data
				dt = SqlToDT(comm.Parameters, "upGetReportInfo", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return dt;
		}

		public string GetReports()
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetReports", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetRoles()
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetRoles", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetSAList()
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetSAList", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetServers(string Return, string Search, string DeploymentStatusID)
		{
			return GetServers(Return, Search, DeploymentStatusID, HttpContext.Current.Session["UserAccountID"].ToString());
		}

		public string GetServers(string Return, string Search, string DeploymentStatusID, string UserID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@Return", SqlDbType.Char, 1).Value = Return;
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserID;
				comm.Parameters.Add("@Search", SqlDbType.VarChar, 50).Value = Search;
				comm.Parameters.Add("@DeploymentStatusID", SqlDbType.TinyInt, 2).Value = DeploymentStatusID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetServers", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public DataTable GetServersDT(string Return, string Search, string DeploymentStatusID)
		{
			return GetServersDT(Return, Search, DeploymentStatusID, HttpContext.Current.Session["UserAccountID"].ToString());
		}

		public DataTable GetServersDT(string Return, string Search, string DeploymentStatusID, string UserID)
		{
			// Declarations
			DataTable dt = null;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@Return", SqlDbType.Char, 1).Value = Return;
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserID;
				comm.Parameters.Add("@Search", SqlDbType.VarChar, 50).Value = Search;
				comm.Parameters.Add("@DeploymentStatusID", SqlDbType.TinyInt, 2).Value = DeploymentStatusID;

				// Get data
				dt = SqlToDT(comm.Parameters, "upGetServers", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return dt;
		}

		public string GetServerDetails(string ServerID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetServerDetails", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetServerOrder(string ServerID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetServerOrder", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetServerRoles(string ServerID, bool InclAddNew)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetServerRoles", System.Reflection.MethodBase.GetCurrentMethod().Name, (InclAddNew ? 1 : 0));

				// Add empty record, if necessary
				if (InclAddNew)
				{
					rs = rs.Replace("$serverroleid0$", "-1");
				}
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetServerSupport(string ServerID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetServerSupport", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetServerTypeList()
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetServerTypeList", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public DataTable GetServerUATask(string UATaskID)
		{
			// Declarations
			DataTable dt = null;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@UATaskID", SqlDbType.Int, 4).Value = UATaskID;

				// Get data
				dt = SqlToDT(comm.Parameters, "upGetServerUATask", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return dt;
		}

		public string GetServerWarranty(string ServerID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetServerWarranty", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetSrvSQLInst(string ServerID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetSrvSQLInst", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetSrvSQLInstInfo(string ServerID, string SQLInstanceID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;
				comm.Parameters.Add("@SQLInstanceID", SqlDbType.Int, 4).Value = EmptyToNull(SQLInstanceID);

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetSrvSQLInstInfo", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetSupportTypes()
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetSupportTypes", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public DataTable GetSupportTypesDT()
		{
			// Declarations
			DataTable dt = null;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				dt = SqlToDT(comm.Parameters, "upGetSupportTypes", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return dt;
		}

		public string GetSrvsByField(string Field, string Search, string DeploymentStatusID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@Field", SqlDbType.VarChar, 100).Value = Field;
				comm.Parameters.Add("@Search", SqlDbType.VarChar, 50).Value = Search;
				comm.Parameters.Add("@DeploymentStatusID", SqlDbType.TinyInt, 2).Value = DeploymentStatusID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetSrvsByField", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public DataTable GetTask(string TaskID)
		{
			// Declarations
			DataTable dt = null;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@TaskID", SqlDbType.Int, 4).Value = TaskID;

				// Get data
				dt = SqlToDT(comm.Parameters, "upGetTask", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return dt;
		}

		public string GetTasks()
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetTasks", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetTimeTypes()
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetTimeTypes", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetWeekEntries(string UserAccountID, string WeekID, bool InclAddNew)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserAccountID;
				comm.Parameters.Add("@WeekID", SqlDbType.Int, 4).Value = WeekID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetUAWeekEntries", System.Reflection.MethodBase.GetCurrentMethod().Name, (InclAddNew ? 1 : 0));

				// Add empty record, if necessary
				if (InclAddNew)
				{
					rs = rs.Replace("$timeentryid0$", "-1");
				}
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetWeekHours(string UserAccountID, string WeekID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserAccountID;
				comm.Parameters.Add("@WeekID", SqlDbType.SmallInt, 2).Value = WeekID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetUAWeekHours", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetUsers()
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetUsers", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public DataTable GetUsersDT()
		{
			// Declarations
			DataTable dt = null;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				dt = SqlToDT(comm.Parameters, "upGetUsers", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			return dt;
		}

		public string GetUATasks(string UserAccountID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserAccountID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetUATasks", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetUAResponsibilities(string UserAccountID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserAccountID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetUAResponsibilities", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetWeeks()
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetWeeks", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetWOAssignedTo(string WorkOrderID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@WorkOrderID", SqlDbType.Int, 4).Value = WorkOrderID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetWOAssignedTo", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetWOComments(string WorkOrderID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@WorkOrderID", SqlDbType.Int, 4).Value = WorkOrderID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetWOComments", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetWODoc(string WorkOrderID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@WorkOrderID", SqlDbType.Int, 4).Value = WorkOrderID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetWODoc", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetWORequestor(string WorkOrderID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@WorkOrderID", SqlDbType.Int, 4).Value = WorkOrderID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetWORequestor", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetWorkOrder(string WorkOrderID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@WorkOrderID", SqlDbType.Int, 4).Value = WorkOrderID;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetWorkOrder", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetWorkOrders(string WOStatusCode)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@WOStatusCode", SqlDbType.Char, 1).Value = WOStatusCode;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetWorkOrders", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetWOStatusCodes()
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetWOStatusCodes", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string HidsMig(string TaskID, string Credentials, string BatchServers, string ServerList)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				BackgroundTask bt = new BackgroundTask(((System.Security.Principal.WindowsIdentity)HttpContext.Current.User.Identity));
				string ServerUATaskRC = "F";

				// Add User Account Task and check for success
				string rc = AddUATask(HttpContext.Current.Session["UserAccountID"].ToString(), TaskID, HttpContext.Current.Request.MapPath("tasklogs", "/", false));
				if (rc.Split((",").ToCharArray())[0] == "F") throw new ArgumentException("Unable to add User Account Task.");

				// Add Server list for task
				if (BatchServers.Trim().Length > 0)
				{
					StringBuilder sb = new StringBuilder();
					DataView dv = new DataView(GetServersDT(BatchServers, "%", "3"));
					dv.RowFilter = "OnPawanet=1 and IsOnline=1 and OSType='windows'";
					for (int i = 0; i < dv.Count; i++)
					{
						sb.Append(dv[i]["ServerID"] + ",");
					}
					ServerUATaskRC = AddServerUATask((sb.Length > 0 ? sb.ToString().Remove(sb.Length - 1) : ""), rc.Split((",").ToCharArray())[1]);
				}
				else ServerUATaskRC = AddServerUATask(ServerList, rc.Split((",").ToCharArray())[1]);

				// Check for success
				if (ServerUATaskRC != "S") throw new ArgumentException("Unable to add server list for task.");

				// Assign background task properties
				bt.RootDir = HttpContext.Current.Server.MapPath("\\");
				bt.ConnectionStr = cConnectionStr;
				bt.ErrorLog = cErrorLog;
				bt.Username = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower().Replace("panynj\\", "");
				bt.Password = Credentials;
				bt.UATaskID = rc.Split((",").ToCharArray())[1];

				Thread td = new Thread(bt.HidsMigration);
				td.Priority = ThreadPriority.Lowest;
				td.IsBackground = true;
				td.Start();
				Result = "S";
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}
			return Result;
		}

		public string HidsUpd(string TaskID, string Credentials, string BatchServers, string ServerList)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				BackgroundTask bt = new BackgroundTask(((System.Security.Principal.WindowsIdentity)HttpContext.Current.User.Identity));
				string ServerUATaskRC = "F";

				// Add User Account Task and check for success
				string rc = AddUATask(HttpContext.Current.Session["UserAccountID"].ToString(), TaskID, HttpContext.Current.Request.MapPath("tasklogs", "/", false));
				if (rc.Split((",").ToCharArray())[0] == "F") throw new ArgumentException("Unable to add User Account Task.");

				// Add Server list for task
				if (BatchServers.Trim().Length > 0)
				{
					StringBuilder sb = new StringBuilder();
					DataView dv = new DataView(GetServersDT(BatchServers, "%", "3"));
					dv.RowFilter = "OnPawanet=1 and IsOnline=1 and OSType='windows'";
					for (int i = 0; i < dv.Count; i++)
					{
						sb.Append(dv[i]["ServerID"] + ",");
					}
					ServerUATaskRC = AddServerUATask((sb.Length > 0 ? sb.ToString().Remove(sb.Length - 1) : ""), rc.Split((",").ToCharArray())[1]);
				}
				else ServerUATaskRC = AddServerUATask(ServerList, rc.Split((",").ToCharArray())[1]);

				// Check for success
				if (ServerUATaskRC != "S") throw new ArgumentException("Unable to add server list for task.");

				// Assign background task properties
				bt.RootDir = HttpContext.Current.Server.MapPath("\\");
				bt.ConnectionStr = cConnectionStr;
				bt.ErrorLog = cErrorLog;
				bt.Username = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower().Replace("panynj\\", "");
				bt.Password = Credentials;
				bt.UATaskID = rc.Split((",").ToCharArray())[1];

				Thread td = new Thread(bt.HidsUpdate);
				td.Priority = ThreadPriority.Lowest;
				td.IsBackground = true;
				td.Start();
				Result = "S";
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}
			return Result;
		}

		public string HtmlTD(string CellData)
		{
			return HtmlTD(CellData, "");
		}

		public string HtmlTD(string CellData, string Attributes)
		{
			return "<td " + Attributes + ">" + (CellData.Trim().Length == 0 ? "&nbsp;" : CellData) + "</td>";
		}

		public string ListDictionaryToXML(ListDictionary ld, string ResultCode)
		{
			// Declarations
			StringBuilder sbXML = new StringBuilder();

			try
			{
				// Initiate with our standard nodes
				sbXML.Append("<?xml version=\"1.0\"?><data><resultcode>" + ResultCode + "</resultcode>");

				// Row node
				sbXML.Append("<row>");

				// Loop of ListDictionary
				foreach (DictionaryEntry de in ld)
				{ 
					// Node will be field name in lowercase
					sbXML.Append("<" + de.Key.ToString().ToLower() + ">");
					// Contents of node will be field data cleansed so we have well-formed XML
					sbXML.Append(de.Value.ToString().Trim().Replace("&", "&amp;").Replace(">", "&gt;").Replace("<", "&lt;").Replace("\"", "&quot;").Replace("'", "&apos;"));
					// Close the node
					sbXML.Append("</" + de.Key.ToString().ToLower() + ">");
				}

				// Close the node
				sbXML.Append("</row>");

				// Close the node
				sbXML.Append("</data>");
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return XML
			return sbXML.ToString();
		}

		public string MaintainServerOrder(string ServerID, string OrderDate, string OrderNumber)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;
				comm.Parameters.Add("@OrderDate", SqlDbType.VarChar, 20).Value = OrderDate.Trim();
				comm.Parameters.Add("@OrderNumber", SqlDbType.VarChar, 20).Value = OrderNumber.Trim();
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upMaintainServerOrder", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string MaintainServerWarranty(string ServerID, string ExpirationDate, string Provider)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;
				comm.Parameters.Add("@ExpirationDate", SqlDbType.VarChar, 20).Value = ExpirationDate.Trim();
				comm.Parameters.Add("@Provider", SqlDbType.VarChar, 20).Value = Provider.Trim();
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upMaintainServerWarranty", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string MaintainUAResponsibility(string UserAccountID, string Tasks, string Projects)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserAccountID;
				comm.Parameters.Add("@Tasks", SqlDbType.VarChar, 4000).Value = Tasks.Trim();
				comm.Parameters.Add("@Projects", SqlDbType.VarChar, 4000).Value = Projects.Trim();
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upMaintainUAResponsibility", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string MaintainWOAssignedTo(string WorkOrderID, string UserAccounts)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@WorkOrderID", SqlDbType.Int, 4).Value = WorkOrderID;
				comm.Parameters.Add("@UserAccounts", SqlDbType.VarChar, 1000).Value = UserAccounts.Trim();
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upMaintainWOAssignedTo", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string MaintainWORequestor(string WorkOrderID, string ClientContacts)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@WorkOrderID", SqlDbType.Int, 4).Value = WorkOrderID;
				comm.Parameters.Add("@ClientContacts", SqlDbType.VarChar, 1000).Value = ClientContacts.Trim();
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upMaintainWORequestor", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public PomeroyMgmt()
		{
			try
			{
				// Check whether its a web or console app
				if (System.Web.HttpContext.Current == null)
				{
					InitializeClassVariables();
				}
				else
				{
					// Attempt to get path to xmldata
					try { cXmlDataPath = HttpContext.Current.Request.MapPath("xmldata", "/", false); }
					catch { }

					// Check session variables
					if (HttpContext.Current.Session["IsDevelopmentPlatform"] == null ||
						HttpContext.Current.Session["ConnectionStr"] == null ||
						HttpContext.Current.Session["ErrorLog"] == null)
					{
						InitializeSession();
					}

					// Retrieve platform
					try
					{
						cIsDevelopmentPlatform = Convert.ToBoolean(HttpContext.Current.Session["IsDevelopmentPlatform"].ToString().ToLower());
					}
					catch { }

					// Retrieve database connection and SMTP information
					cConnectionStr = HttpContext.Current.Session["ConnectionStr"].ToString();

					// Retrieve cErrorLog
					cErrorLog = HttpContext.Current.Session["ErrorLog"].ToString();
				}
			}

			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}
		}

		public DataView RptWorkOrders(string WOStatusCode)
		{
			// Declarations
			DataView dv = null;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@WOStatusCode", SqlDbType.Char, 1).Value = WOStatusCode;

				// Get data
				dv = new DataView(SqlToDT(comm.Parameters, "upRptWorkOrders", System.Reflection.MethodBase.GetCurrentMethod().Name));
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return dv;
		}

		public string ServersUpd(string TaskID, string Credentials, string BatchServers, string ServerList)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				BackgroundTask bt = new BackgroundTask(((System.Security.Principal.WindowsIdentity)HttpContext.Current.User.Identity));
				string ServerUATaskRC = "F";

				// Add User Account Task and check for success
				string rc = AddUATask(HttpContext.Current.Session["UserAccountID"].ToString(), TaskID, HttpContext.Current.Request.MapPath("tasklogs", "/", false));
				if (rc.Split((",").ToCharArray())[0] == "F") throw new ArgumentException("Unable to add User Account Task.");

				// Add Server list for task
				if (BatchServers.Trim().Length > 0)
				{
					StringBuilder sb = new StringBuilder();
					DataView dv = new DataView(GetServersDT(BatchServers, "%", "3"));
					dv.RowFilter = "OnPawanet=1 and IsOnline=1 and OSType='windows'";
					for (int i = 0; i < dv.Count; i++)
					{
						sb.Append(dv[i]["ServerID"] + ",");
					}
					ServerUATaskRC = AddServerUATask((sb.Length > 0 ? sb.ToString().Remove(sb.Length - 1) : ""), rc.Split((",").ToCharArray())[1]);
				}
				else ServerUATaskRC = AddServerUATask(ServerList, rc.Split((",").ToCharArray())[1]);

				// Check for success
				if (ServerUATaskRC != "S") throw new ArgumentException("Unable to add server list for task.");

				// Assign background task properties
				bt.RootDir = HttpContext.Current.Server.MapPath("\\");
				bt.ConnectionStr = cConnectionStr;
				bt.ErrorLog = cErrorLog;
				bt.Username = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower().Replace("panynj\\", "");
				bt.Password = Credentials;
				bt.UATaskID = rc.Split((",").ToCharArray())[1];

				Thread td = new Thread(bt.ServersUpdate);
				td.Priority = ThreadPriority.Lowest;
				td.IsBackground = true;
				td.Start();
				Result = "S";
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}
			return Result;
		}

		public string SetDbIsSupported(string DatabaseID, string IsSupported)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@DatabaseID", SqlDbType.Int).Value = DatabaseID;
				comm.Parameters.Add("@IsSupported", SqlDbType.Bit, 1).Value = IsSupported;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upSetDbIsSupported", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string SetOnPawanet(string ServerID, string OnPawanet)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@ServerID", SqlDbType.Int).Value = ServerID;
				comm.Parameters.Add("@OnPawanet", SqlDbType.Bit, 1).Value = OnPawanet;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upSetOnPawanet", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string SetSrvTaskSuccess(string ServerUATaskID, bool Success)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@ServerUATaskID", SqlDbType.Int).Value = ServerUATaskID;
				comm.Parameters.Add("@Success", SqlDbType.Bit, 1).Value = Success;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upSetSrvTaskSuccess", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string UpdateClientContact(string ClientContactID, string FirstName, string MiddleInitial, string LastName,
			string Phone, string Email)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@ClientContactID", SqlDbType.Int, 4).Value = ClientContactID;
				comm.Parameters.Add("@FirstName", SqlDbType.VarChar, 50).Value = FirstName.Trim();
				comm.Parameters.Add("@MiddleInitial", SqlDbType.Char, 1).Value = (MiddleInitial.Trim().Length == 0 ? (object)DBNull.Value : MiddleInitial.Trim()); 
				comm.Parameters.Add("@LastName", SqlDbType.VarChar, 80).Value = LastName.Trim();
				comm.Parameters.Add("@Phone", SqlDbType.VarChar, 20).Value = (Phone.Trim().Length == 0 ? (object)DBNull.Value : Phone.Trim());
				comm.Parameters.Add("@Email", SqlDbType.VarChar, 20).Value = (Email.Trim().Length == 0 ? (object)DBNull.Value : Email.Trim());
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upUpdateClientContact", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{ FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog); }

			// Return result
			return Result;
		}

		public string UpdateDbClCon(string DatabaseID, string ClientContactID)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@DatabaseID", SqlDbType.Int, 4).Value = DatabaseID;
				comm.Parameters.Add("@ClientContactID", SqlDbType.Int, 4).Value = ClientContactID;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upUpdateDbClCon", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string UpdateDbDept(string DatabaseID, string DepartmentID)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@DatabaseID", SqlDbType.Int, 4).Value = DatabaseID;
				comm.Parameters.Add("@DepartmentID", SqlDbType.SmallInt, 2).Value = DepartmentID;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upUpdateDbDept", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string UpdateServerHW(string ServerID, string Manufacturer, string Model, string SerialNumber,
			string ProcessorInfo, string Processors, string MemoryBytes, string IpAddress, string ServerType)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;
				comm.Parameters.Add("@Manufacturer", SqlDbType.VarChar, 50).Value = Manufacturer.Trim();
				comm.Parameters.Add("@Model", SqlDbType.VarChar, 50).Value = Model.Trim();
				comm.Parameters.Add("@SerialNumber", SqlDbType.VarChar, 60).Value = SerialNumber.Trim();
				comm.Parameters.Add("@ProcessorInfo", SqlDbType.VarChar, 50).Value = ProcessorInfo.Trim();
				comm.Parameters.Add("@Processors", SqlDbType.TinyInt, 1).Value = (Processors.Trim().Length == 0 ? (object) DBNull.Value : Processors);
				comm.Parameters.Add("@MemoryBytes", SqlDbType.BigInt, 8).Value = (MemoryBytes.Trim().Length == 0 ? (object) DBNull.Value : MemoryBytes);
				comm.Parameters.Add("@IpAddress", SqlDbType.VarChar, 1000).Value = IpAddress.Trim();
				comm.Parameters.Add("@ServerType", SqlDbType.Char, 1).Value = (ServerType.Trim().Length == 0 ? (object)DBNull.Value : ServerType.Trim().ToUpper());
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upUpdateServerHW", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string UpdateServerSW(string ServerID, string OS, string OSProductID, string OSVer, 
			string OSServicePack, string MajorApps)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;
				comm.Parameters.Add("@OS", SqlDbType.VarChar, 200).Value = OS.Trim();
				comm.Parameters.Add("@OSProductID", SqlDbType.VarChar, 32).Value = OSProductID.Trim();
				comm.Parameters.Add("@OSVer", SqlDbType.VarChar, 20).Value = OSVer.Trim();
				comm.Parameters.Add("@OSServicePack", SqlDbType.VarChar, 20).Value = OSServicePack.Trim();
				comm.Parameters.Add("@MajorApps", SqlDbType.VarChar, 100).Value = (MajorApps.Trim().ToUpper() == "NA" ? (object)DBNull.Value : MajorApps.Trim());
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upUpdateServerSW", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string UpdateSrvSuppInfo(string ServerID, string SupportType, string SupportBeginDate, string SupportEndDate)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@ServerID", SqlDbType.Int, 4).Value = ServerID;
				comm.Parameters.Add("@SupportType", SqlDbType.Char, 1).Value = SupportType.Trim();
				comm.Parameters.Add("@SupportBeginDate", SqlDbType.VarChar, 20).Value = SupportBeginDate.Trim();
				comm.Parameters.Add("@SupportEndDate", SqlDbType.VarChar, 20).Value = SupportEndDate.Trim();
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upUpdateSrvSuppInfo", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string UpdateTimeEntry(string TimeEntryID, string TimeTypeID, string Info, string Hours, string StartTime, string EndTime, string IsDeleted)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@TimeEntryID", SqlDbType.Int, 4).Value = TimeEntryID;
				comm.Parameters.Add("@TimeTypeID", SqlDbType.TinyInt, 1).Value = TimeTypeID;
				comm.Parameters.Add("@Info", SqlDbType.VarChar, 500).Value = Info.Trim();
				comm.Parameters.Add("@Hours", SqlDbType.Float).Value = Hours;
				comm.Parameters.Add("@StartTime", SqlDbType.VarChar, 10).Value = StartTime;
				comm.Parameters.Add("@EndTime", SqlDbType.VarChar, 10).Value = EndTime;
				comm.Parameters.Add("@IsDeleted", SqlDbType.Bit, 1).Value = Convert.ToBoolean(Convert.ToInt16(IsDeleted));
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upUpdateTimeEntry", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string UpdateUATaskStatus(string UATaskID, string TaskStatus)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@UATaskID", SqlDbType.Int, 4).Value = UATaskID;
				comm.Parameters.Add("@TaskStatus", SqlDbType.Char, 1).Value = TaskStatus;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upUpdateUATaskStatus", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string UpdateWOComment(string WOCommentID, string WOComment, string UserAccountID)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@WOCommentID", SqlDbType.Int, 4).Value = WOCommentID;
				comm.Parameters.Add("@WOComment", SqlDbType.VarChar, 3000).Value = WOComment.Trim();
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserAccountID;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upUpdateWOComment", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string UpdateWODetails(string WorkOrderID, string Title, string Description, string ClientProject,
			string ServiceOrder, string WOStatusCode, string DepartmentID, string UserAccountID)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@WorkOrderID", SqlDbType.Int, 4).Value = WorkOrderID;
				comm.Parameters.Add("@Title", SqlDbType.VarChar, 300).Value = Title.Trim();
				comm.Parameters.Add("@Description", SqlDbType.VarChar, 2000).Value = Description.Trim();
				comm.Parameters.Add("@DepartmentID", SqlDbType.Int, 4).Value = DepartmentID;
				comm.Parameters.Add("@ClientProject", SqlDbType.VarChar, 50).Value = ClientProject.Trim();
				comm.Parameters.Add("@ServiceOrder", SqlDbType.VarChar, 200).Value = ServiceOrder.Trim();
				comm.Parameters.Add("@WOStatusCode", SqlDbType.Char, 1).Value = WOStatusCode.Trim();
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Value = UserAccountID;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upUpdateWODetails", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

		public string UpdateWODtCost(string WorkOrderID, string DesiredCompletionDate, string CompletionDate,
			string RequestDate, string IsBillable, string EstCost, string FixedCost)
		{
			// Declarations
			string Result = "F";

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;

				// Add parameters
				comm.Parameters.Add("@WorkOrderID", SqlDbType.Int, 4).Value = WorkOrderID;
				comm.Parameters.Add("@DesiredCompletionDate", SqlDbType.SmallDateTime).Value = (DesiredCompletionDate.Trim().Length == 0 ? (object)DBNull.Value : DesiredCompletionDate.Trim());
				comm.Parameters.Add("@CompletionDate", SqlDbType.SmallDateTime).Value = (CompletionDate.Trim().Length == 0 ? (object)DBNull.Value : CompletionDate.Trim());
				comm.Parameters.Add("@RequestDate", SqlDbType.SmallDateTime).Value = (RequestDate.Trim().Length == 0 ? (object)DBNull.Value : RequestDate.Trim());
				comm.Parameters.Add("@IsBillable", SqlDbType.Bit, 1).Value = IsBillable;
				comm.Parameters.Add("@EstCost", SqlDbType.Money).Value = (EstCost.Trim().Length == 0 ? (object)DBNull.Value : EstCost.Trim());
				comm.Parameters.Add("@FixedCost", SqlDbType.Money).Value = (FixedCost.Trim().Length == 0 ? (object)DBNull.Value : FixedCost.Trim());
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upUpdateWODtCost", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// Assign to variable
				Result = retParams["@Result"].Value.ToString();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

	#endregion

	#region Private Functions

		private object EmptyToNull(string Input)
		{
			return (Input.Trim().Length == 0 ? (object)DBNull.Value : Input.Trim());
		}

		private DataTable GetWinServices()
		{
			// Declarations
			DataTable dt = new DataTable();

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Get data
				dt = SqlToDT(comm.Parameters, "upGetWinServices", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return dt;
		}

		private void InitializeClassVariables()
		{
			// Declarations
			AppSettingsReader AppSetReader = new AppSettingsReader();

			// Attempt to get path to xmldata
			try { cXmlDataPath = System.Reflection.Assembly.GetEntryAssembly().Location.Substring(0, System.Reflection.Assembly.GetEntryAssembly().Location.LastIndexOf("\\") + 1) + "xmldata"; }
			catch { }

			// Retrieve platform
			try
			{
				cIsDevelopmentPlatform = (bool)AppSetReader.GetValue("Development", typeof(bool));
			}
			catch { }

			// Retrieve database connection and SMTP information
			if (cIsDevelopmentPlatform)
			{
				cConnectionStr = (string)AppSetReader.GetValue("DevDB", typeof(string));
			}
			else
			{
				cConnectionStr = (string)AppSetReader.GetValue("DB", typeof(string));
			}

			// Retrieve cErrorLog
			try { cErrorLog = (string)AppSetReader.GetValue("cErrorLog", typeof(string)); }
			catch
			{	try { cErrorLog = HttpContext.Current.Request.MapPath("logfiles", "/", false) + "\\cErrorLog.xml"; }
				catch
				{ try	{	cErrorLog = System.Reflection.Assembly.GetEntryAssembly().Location.Substring(0, System.Reflection.Assembly.GetEntryAssembly().Location.LastIndexOf("\\") + 1) + "cErrorLog.xml";	}
					catch
					{	cErrorLog = string.Empty; }
				}
			}
		}

		private void InitializeSession()
		{
			// Declarations
			AppSettingsReader AppSetReader = new AppSettingsReader();

			// Retrieve platform
			try
			{
				HttpContext.Current.Session["IsDevelopmentPlatform"] = (bool)AppSetReader.GetValue("Development", typeof(bool));
			}
			catch { }

			// Retrieve database connection
			if (Convert.ToBoolean(HttpContext.Current.Session["IsDevelopmentPlatform"].ToString().ToLower()))
			{
				HttpContext.Current.Session["ConnectionStr"] =
					(string)AppSetReader.GetValue("DevDB", typeof(string));
			}
			else
			{
				HttpContext.Current.Session["ConnectionStr"] =
					(string)AppSetReader.GetValue("DB", typeof(string));
			}

			// Verify user
			if (!VerifyUser())
			{
				HttpContext.Current.Session.Abandon();
			}

			// Retrieve ErrorLog
			try { HttpContext.Current.Session["ErrorLog"] = (string)AppSetReader.GetValue("ErrorLog", typeof(string)); }
			catch
			{ try { HttpContext.Current.Session["ErrorLog"] = HttpContext.Current.Request.MapPath("logfiles", "/", false) + "\\cErrorLog.xml"; }
				catch
				{ try { HttpContext.Current.Session["ErrorLog"] = System.Reflection.Assembly.GetEntryAssembly().Location.Substring(0, System.Reflection.Assembly.GetEntryAssembly().Location.LastIndexOf("\\") + 1); }
					catch
					{	HttpContext.Current.Session["ErrorLog"] = string.Empty; }
				}
			}
		}

		private bool Ping(string Server)
		{
			try
			{
				// Declarations
				Ping pingSender = new Ping();
				PingOptions options = new PingOptions();

				// Set options
				options.DontFragment = true;

				// Create a buffer of 32 bytes of data to be transmitted.
				string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
				byte[] buffer = Encoding.ASCII.GetBytes(data);
				int timeout = 120;
				PingReply reply = pingSender.Send(Server, timeout, buffer, options);
				return (reply.Status == IPStatus.Success ? true : false);

			}
			catch {return false;}
		}

		private string ReaderToJSON(SqlDataReader dr, int EmptyRecords)
		{
			// Declarations
			StringBuilder sbJSON = new StringBuilder();
			int FieldCount = 0;
			bool AddComma = false;
			string varID = "$";

			try
			{
				// Get number of fields(columns) in the DataReader
				FieldCount = dr.FieldCount;

				// Initiate with our standard member object
				sbJSON.Append("{\"data\":{");

				// Add resultcode pair
				sbJSON.Append("\"resultcode\":true,");

				// Add row array
				sbJSON.Append("\"row\":[");

				// Loop of Datareader
				while (dr.Read())
				{
					// Initiate member object
					sbJSON.Append((AddComma ? "," : "") + "{");

					// Loop over fields
					for (int i = 0; i < FieldCount; i++)
					{
						// Pair string will be field name in lowercase
						sbJSON.Append("\"" + dr.GetName(i).ToString().Trim().ToLower() + "\":");
						// Pair value will be field data cleansed so we have well-formed JSON
						sbJSON.Append("\"" + dr[i].ToString().Trim().Replace("\"", "\\\"").Replace("\\", "\\\\").Replace("\b", "\\b").Replace("\b", "\\b").Replace("\t", "\\t").Replace("\n", "\\n").Replace("\f", "\\f").Replace("\r", "\\r")
							+ "\"" + (i < FieldCount - 1 ? "," : ""));
					}

					// Close object
					sbJSON.Append("}");

					// Reset flag
					AddComma = true;
				}

				// Add empty records
				for (int y = 0; y < EmptyRecords; y++)
				{
					// Initiate member object
					sbJSON.Append((AddComma ? "," : "") + "{");

					// Loop over fields
					for (int i = 0; i < FieldCount; i++)
					{
						// Pair string will be field name in lowercase
						sbJSON.Append("\"" + dr.GetName(i).ToString().Trim().ToLower() + "\":");
						// Pair value will be field name plus counter enclosed in varID
						sbJSON.Append("\"" + varID + dr.GetName(i).ToString().Trim().ToLower() + y.ToString() + varID + "\"" +
							(i < FieldCount - 1 ? "," : ""));
					}

					// Close object
					sbJSON.Append("}");

					// Reset flag
					AddComma = true;
				}

				// Close Array and objects
				sbJSON.Append("]}}");

				// Close Datareader
				dr.Close();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
				if (!dr.IsClosed) dr.Close();
				sbJSON.Remove(0, sbJSON.Length);
				sbJSON.Append("{\"data\":{\"resultcode\":false}}");
			}

			// Return XML
			return sbJSON.ToString();
		}

		private SqlParameterCollection ReturnSqlParams(SqlParameterCollection SqlParams, string StoredProcedure, string CallingFunc)
		{
			// Declarations
			SqlConnection conn = new SqlConnection(cConnectionStr);
			SqlCommand comm = new SqlCommand(StoredProcedure, conn);

			try
			{
				// Set command type
				comm.CommandType = CommandType.StoredProcedure;

				// Add parameters
				for (int i = 0; i < SqlParams.Count; i++)
					if (SqlParams[i].Direction == ParameterDirection.Output)
						comm.Parameters.Add(SqlParams[i].ParameterName.ToString(), SqlParams[i].SqlDbType, SqlParams[i].Size).Direction = ParameterDirection.Output;
					else
						comm.Parameters.Add(SqlParams[i].ParameterName.ToString(), SqlParams[i].SqlDbType, SqlParams[i].Size).Value = SqlParams[i].Value;

				// Open Connection and execute
				comm.Connection.Open();
				comm.ExecuteNonQuery();

				// Close connection
				conn.Close();
			}
			catch (Exception ExceptionErr)
			{
				comm.Parameters["@Result"].Value = (comm.Parameters["@Result"].Value == null ? "F" : comm.Parameters["@Result"].Value);
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog, "Called from: " + CallingFunc);
				if (conn.State != ConnectionState.Closed) conn.Close();
			}

			// Return
			return comm.Parameters;
		}

		private DataTable SqlToDT(SqlParameterCollection SqlParams, string StoredProcedure, string CallingFunc)
		{
			// Declarations
			DataSet ds = new DataSet();
			SqlConnection conn = new SqlConnection(cConnectionStr);

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand(StoredProcedure, conn);
				SqlDataAdapter da = new SqlDataAdapter();

				// Set command type
				comm.CommandType = CommandType.StoredProcedure;

				// Add parameters
				for (int i = 0; i < SqlParams.Count; i++)
					comm.Parameters.Add(SqlParams[i].ParameterName.ToString(), SqlParams[i].SqlDbType, SqlParams[i].Size).Value = SqlParams[i].Value;

				// Assign SQL command to Adapter
				da.SelectCommand = comm;

				// Open Connection
				comm.Connection.Open();

				// Fill DataSet
				da.Fill(ds);

				// Close connection
				conn.Close();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog, "Called from: " + CallingFunc);
				if (conn.State != ConnectionState.Closed) conn.Close();
			}

			// Return result
			return ds.Tables[0];
		}

		private string SqlToJSON(SqlParameterCollection SqlParams, string StoredProcedure, string CallingFunc)
		{
			return SqlToJSON(SqlParams, StoredProcedure, CallingFunc, 0);
		}

		private string SqlToJSON(SqlParameterCollection SqlParams, string StoredProcedure, string CallingFunc, int EmptyRecords)
		{
			// Declarations
			string JSON = string.Empty;
			SqlConnection conn = new SqlConnection(cConnectionStr);

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand(StoredProcedure, conn);

				// Set command type
				comm.CommandType = CommandType.StoredProcedure;

				// Add parameters
				for (int i = 0; i < SqlParams.Count; i++)
					comm.Parameters.Add(SqlParams[i].ParameterName.ToString(), SqlParams[i].SqlDbType, SqlParams[i].Size).Value = SqlParams[i].Value;

				// Open Connection
				comm.Connection.Open();

				// Assign to datareader
				JSON = ReaderToJSON(comm.ExecuteReader(), EmptyRecords);

				// Close connection
				conn.Close();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog, "Called from: " + CallingFunc);
				if (conn.State != ConnectionState.Closed) conn.Close();
			}

			// Return result
			return JSON;
		}

		private string ToTitleCase(string Text)
		{
			return System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Text);
		}

		private Boolean VerifyUser()
		{
			// Declarations
			Boolean Result = false;

			try
			{
				// Declarations
				DirectoryEntry de = new DirectoryEntry("WinNT://" + HttpContext.Current.User.Identity.Name.Replace("\\", "/").ToString());
				SqlCommand comm = new SqlCommand();
				SqlParameterCollection retParams = null;
				String FirstName, LastName;

				// Initialize class variable
				cConnectionStr = HttpContext.Current.Session["ConnectionStr"].ToString();

				// Parse First and Last Name
				if (de.Properties["FullName"].Value.ToString().IndexOf(",") > 0)
				{
					FirstName = de.Properties["FullName"].Value.ToString().Split(",".ToCharArray())[1].Trim();
					LastName = de.Properties["FullName"].Value.ToString().Split(",".ToCharArray())[0].Trim();
				}
				else
				{
					FirstName = de.Properties["FullName"].Value.ToString().Split(" ".ToCharArray())[1].Trim();
					LastName = de.Properties["FullName"].Value.ToString().Split(" ".ToCharArray())[0].Trim();
				}

				// Get user's group to assign access
				for (int i = 0; i < System.Web.HttpContext.Current.Request.LogonUserIdentity.Groups.Count; i++)
				{
					if (System.Web.HttpContext.Current.Request.LogonUserIdentity.Groups[i].Translate(typeof(System.Security.Principal.NTAccount)).ToString().ToUpper().Contains("PM_FULL"))
					{
						HttpContext.Current.Session["View"] = "FULL";
						break;
					}
					if (System.Web.HttpContext.Current.Request.LogonUserIdentity.Groups[i].Translate(typeof(System.Security.Principal.NTAccount)).ToString().ToUpper().Contains("PM_WO"))
					{
						HttpContext.Current.Session["View"] = "WO";
					}
				}

				// Add parameters
				comm.Parameters.Add("@WindowsUser", SqlDbType.VarChar, 50).Value = HttpContext.Current.User.Identity.Name.ToString();
				comm.Parameters.Add("@FirstName", SqlDbType.VarChar, 50).Value = FirstName;
				comm.Parameters.Add("@LastName", SqlDbType.VarChar, 50).Value = LastName;
				comm.Parameters.Add("@UserAccountID", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
				comm.Parameters.Add("@Result", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

				// Execute sql
				retParams = ReturnSqlParams(comm.Parameters, "upVerifyUser", System.Reflection.MethodBase.GetCurrentMethod().Name);

				// If user was verified, assign return and session variables
				if (retParams["@Result"].Value.ToString().ToUpper() == "S")
				{
					Result = true;
					HttpContext.Current.Session["UserAccountID"] =
						retParams["@UserAccountID"].Value.ToString();
					HttpContext.Current.Session["FullName"] = FirstName + " " + LastName;
				}
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return Result;
		}

	#endregion

	}
}
