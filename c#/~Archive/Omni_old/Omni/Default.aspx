﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Omni._Default" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Omni</title>
    <script src="http://yui.yahooapis.com/3.14.1/build/yui/yui-min.js"></script>
    <script>
        YUI().use("datasource-io", "datasource-jsonschema", function (Y) {
            var myDataSource = new Y.DataSource.IO({ source: "./srvexec.aspx" }),
                myCallback = {
                    success: function (e) {
                        alert(e.response.results.length);
                    },
                    failure: function (e) {
                        alert("Could not retrieve data: " + e.error.message);
                    }
                };

            myDataSource.plug(Y.Plugin.DataSourceJSONSchema, {
                schema: {
                    resultListLocator: "ResultSet.Result",
                    resultFields: ["Title", "Summary", "Url", "Size"]
                }
            });

            // This request string will get appended to the URI
            myDataSource.sendRequest({
                callback: myCallback
            });
        });
    </script>
</head>
<body id="main" runat="server" class="yui-skin-sam">
	<div id="container">
		<div id="content">
			<div class="floatleft">
				<div id="user" class="floatright" style="text-align:right;" runat="server"></div>
			</div>
			<br />
			<br />
			<div id="panels">
				<div id="servers" runat="server" class="collapsepanel" visible="false">
				</div>
				<div id="timeentry" runat="server" class="collapsepanel" visible="false">
				</div>			
				<div id="uresp" runat="server" class="collapsepanel" visible="false">
				</div>			
				<div id="maint" runat="server" class="collapsepanel" visible="false">
				</div>			
				<div id="wrkord" runat="server" class="collapsepanel" visible="false">
				</div>			
			</div>
		</div>
	</div>
</body>
</html>
