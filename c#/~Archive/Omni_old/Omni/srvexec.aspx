﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="srvexec.aspx.cs" Inherits="Omni.srvexec" %>

{
    "ResultSet": {
        "Result":[
            {
                "Title":"Madonna",
                "Summary":"Official site of pop diva Madonna, with news, music, media, and fan club.",
                "Url":"http:\/\/www.madonna.com\/",
                "Size":"145030"},
            {
                "Title":"Madonna - MySpace",
                "Summary":"Madonna MySpace page features news, blog, music downloads, desktops, wallpapers, and more.",
                "Url":"http:\/\/www.myspace.com\/madonna",
                "Size":"110365"},
            {
                "Title":"YouTube - madonna's Channel",
                "Summary":"The Official Madonna YouTube Channel. Want to Subscribe? ... http:\/\/www.youtube.com\/Madonna. Sharing Options There are 3 ways to share this channel.",
                "Url":"http:\/\/youtube.com\/madonna",
                "Size":"49955"}        
        ]
    }
}

