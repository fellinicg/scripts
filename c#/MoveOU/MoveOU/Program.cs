﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Novell.Directory.Ldap;
using Novell.Directory.Ldap.Utilclass;

namespace MoveOU
{
    class Program
    {
        static void Main(string[] args)
        {

            if ( args.Length != 6)
		    {
			    Console.WriteLine("Usage:   mono Search <host name> <ldap port>  <login dn>" + " <password> <search base>" + " <search filter>");
			    Console.WriteLine("Example: mono Search Acme.com 389"  + " \"cn=admin,o=Acme\"" + " secret \"ou=sales,o=Acme\"" + "         \"(objectclass=*)\"");
			    return;
            }

	        string ldapHost = args[0];
		    int ldapPort = System.Convert.ToInt32(args[1]);
	        String loginDN  = args[2];
	        String password = args[3];
            String searchBase = args[4];
	        String searchFilter = args[5];

            try
            {
           	    LdapConnection conn= new LdapConnection();
			    Console.WriteLine("Connecting to:" + ldapHost);
                conn.Connect(ldapHost,ldapPort);
                //conn.Bind(loginDN,password);
			    LdapSearchResults lsc=conn.Search(	searchBase,
												    LdapConnection.SCOPE_SUB,
												    searchFilter,
												    null,
												    false);

			    while (lsc.hasMore())
			    {
				    LdapEntry nextEntry = null;
				    try 
				    {
					    nextEntry = lsc.next();
				    }
				    catch(LdapException e) 
				    {
					    Console.WriteLine("Error: " + e.LdapErrorMessage);
					    // Exception is thrown, go for next entry
				    continue;
				    }
				    Console.WriteLine("\n" + nextEntry.DN);
				    LdapAttributeSet attributeSet = nextEntry.getAttributeSet();
				    System.Collections.IEnumerator ienum=attributeSet.GetEnumerator();
				    while(ienum.MoveNext())
				    {
					    LdapAttribute attribute=(LdapAttribute)ienum.Current;
       					    string attributeName = attribute.Name;
					    string attributeVal = attribute.StringValue;
					    if(!Base64.isLDIFSafe(attributeVal))
					    {
						    byte[] tbyte=SupportClass.ToByteArray(attributeVal);
						    attributeVal=Base64.encode(SupportClass.ToSByteArray(tbyte));
					    }
	       				            Console.WriteLine( attributeName + "value:" + attributeVal);
				    }
			    }
			    conn.Disconnect();
		    }
            catch(LdapException e)
            {
			    Console.WriteLine("Error:" + e.LdapErrorMessage);
                return;
            }
            catch(Exception e)
            {
                Console.WriteLine("Error:" + e.Message);
                return;
            }

        }
    }
}
