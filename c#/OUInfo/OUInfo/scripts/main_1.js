/// <reference path="x.js" />
/// <reference path="common.js" />
var exec_url = "srvexec.aspx";

// UNLOCKMIGUSER
$(function() {
    $('a.unlockmig').on('click',function (evt) {
        evt.preventDefault();
        $.post("srvexec.aspx?a=unlockmig", function (result) {
            if (result.toLowerCase() == "true") alert("Account Unlocked!");
            else alert("Unable to Unlock Account!");
        });
        return void (0);
    });
});

//function unlockmig() {
//    //alert("shit");
//    $.get("srvexec.aspx?a=unlockmig", function (result) {
//        alert(result);
//    });
//    $(function () {
//        $('#unlockmig').click(function () {
//            alert('hello');
//            return false;
//        });
//    });
////  $.get("srvexec.aspx?a=unlockmig", function (result) { alert(result); });
//}

// SEARCH
function search(txt) {
    var oTable = $('#srchresults').dataTable({
        "sDom": 'Rlfrtip',
        "iDisplayLength": 50,
        "bProcessing": true,
        //"bServerSide": true,
        "bDestroy": true,
        "sAjaxSource": "srvexec.aspx?a=search&s=" + txt,
        "sAjaxDataProp": "data",
        "aoColumns": [
            { "mData": "username" },
            { "mData": "firstname" },
            { "mData": "lastname" },
            { "mData": "ou" },
            { "mData": "dn" }
        ]
    });
    return void (0);
}
function search2(txt) {

    function fnFormatDetails(oTable, nTr, id) {
        var sOut = "";

        $.ajax({
            url: "srvexec.aspx?a=getuser&s=" + id,
            async: false,
            dataType: 'json',
            success: function (json) {
                sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;"><tr>';
                //sOut += '<th>Email</th><th>Office</th><th>Ofc Phone</th><th>Cell Phone</th><th>Status</th><th>Enabled</th></tr>';
                sOut += '<tr>';
                sOut += '<td><a href="mailto:' + json.data[0].email + '">' + json.data[0].email + '</td>';
                sOut += '<td>' + json.data[0].office + '</td>';
                sOut += '<td>' + json.data[0].officephone + '</td>';
                sOut += '<td>' + json.data[0].mobilephone + '</td>';
                sOut += '<td><img src="../images/padlock_open.png"></td>'; // + if (toBool(json.data[0].islockedout) + '</td>';
                sOut += '</tr></table>';
            }
        });
        return sOut;
    }

    var oTable = $('#srchresults').dataTable({
        "sDom": 'Rlfrtip',
        "aLengthMenu": [[50, 75, 100, -1], [50, 75, 100, "All"]],
        "iDisplayLength": 50,
        "bProcessing": true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "bDestroy": true,
        "sAjaxSource": "srvexec.aspx?a=search&s=" + txt,
        "sAjaxDataProp": "data",
        "aoColumns": [
            {
                "mData": null, "bSortable": false, "mRender": function (data, type, full) {
                    return '<a href=javascript:void(0)><img id="' + full.aduserid + ',' + full.username + '" src="../images/details_open.png"></a>';
                }
            },
            { "mData": "username" },
            { "mData": "firstname" },
            { "mData": "lastname" },
            { "mData": "ou" },
            { "mData": "dn" }
        ],
        "aaSorting": [[2, 'asc']]
    });

    $('#srchresults tbody td img').live('click', function () {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr)) {
            /* This row is already open - close it */
            this.src = "../images/details_open.png";
            oTable.fnClose(nTr);
        }
        else {
            /* Open this row */
            this.src = "../images/details_close.png";
            oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr, this.id), 'details');
        }
    });

    return void (0);
}
function toBool(src) {
    switch (src.toLowerCase()) {
        case "true": case "yes": case "1": return true;
        case "false": case "no": case "0": case null: return false;
        default: return Boolean(string);
    }
}