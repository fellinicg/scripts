﻿// FORMATPH
function formatph(ph) {return ph.toString().replace(/(\d{3})(\d{3})(\d{4})/gi,"$1-$2-$3");}
// ISVALIDEMAIL
function isvalidemail(addr){
	var regex = /^[\w-\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,3}$/
	return (addr.toString().search(regex)==-1 ? false : true);}
// DECIMALONLY
function decimalonly(obj) {
	return ((event.keyCode == 46) || (event.keyCode > 47 && event.keyCode < 58));}
// PHNUMREST
function phnumrest(obj) {
	return (!(obj.value.length == 0 && event.keyCode == 48) && (event.keyCode > 47 && event.keyCode < 58));}
// VALIDATEIP
function validateip(ip) {
	var ary = ip.split("."); 
	if (ary.length != 4) return false;
	for (var i in ary) if (!ary[i].match(/^\d{1,3}$/) || (Number(ary[i]) > 255)) return false;
	return true;}
// CLEARLIST
function clearlist(lst, floor) {
	var i;
	if (floor == null) floor = 0;
	for (i=xGetElementById(lst).length;i!=floor;i--) xGetElementById(lst).remove(i);}
// CLEAROPTIONS
function clearoptions(id) {
	var selectObj = xGetElementById(id);
	var selectParentNode = selectObj.parentNode;
	var newSelectObj = selectObj.cloneNode(false);
	selectParentNode.replaceChild(newSelectObj, selectObj);
	return newSelectObj;}
// ADDOPTION
function addOption(lst, value, text) {
	var ele = document.createElement('option');
	ele.text = text;
	ele.value = value;
	ele.title = text;
	var x = xGetElementById(lst);
	try	{x.add(ele, null);}
	catch(ex)	{x.add(ele);}}
// MOVEOPTIONS
function moveoptions(from, to) {
	var nosel = true;
	var lstfrom = xGetElementById(from);
	var lstto = xGetElementById(to);
	for (var i = lstfrom.length - 1; i > -1; i--) {
		if (lstfrom[i].selected) {
			addOption(lstto.id, lstfrom[i].value, lstfrom[i].text);
			lstfrom.remove(i);
			nosel = false;
		}
	}
	if (!nosel) sortoptions(lstto);
	return void (0);
}
// SORTOPTIONS
function sortoptions(lst) {
	var curr = new Array();
	for (var i = 0; i < lst.length; i++) curr[i] = lst[i].text + ":" + lst[i].value;
	var sorted = curr.sort();
	clearoptions(lst.id);
	for (var i = 0; i < sorted.length; i++) {
		addOption(lst.id, sorted[i].split(":")[1], sorted[i].split(":")[0]);
	}
}
// SHOWMESSAGEBOX
function showmessagebox (messageTitle, messageContent, showClose, messageWidth, maintainPopup) {
	if (eval(xGetElementById("content")) != null)
		try {if (parseFloat(navigator.appVersion.split("MSIE")[1]) < 7) xGetElementById("content").style.display = "none";}
		catch (e) {}
	if (eval(messageWidth) != null) {xGetElementById("MessageBoxDiv").style.width = messageWidth;xGetElementById("MessageBoxTxt").style.width = messageWidth;}
	else {xGetElementById("MessageBoxDiv").style.width = "300px";	xGetElementById("MessageBoxTxt").style.width = "300px";}
	xGetElementById("MessageBoxTrans").style.height = (xScrollTop() + xClientHeight()) + "px";
	xGetElementById("MessageBoxTrans").style.width = (xScrollLeft() + xClientWidth()) + "px";
	xGetElementById("MessageBoxTitle").innerHTML = messageTitle;
	if (xTrim(messageTitle).length > 0) xGetElementById('MessageBoxTitle').style.display = "inline";
	else xGetElementById("MessageBoxTitle").style.display = "none";
	xGetElementById("MessageBoxTxt").innerHTML = messageContent;
	xGetElementById("MessageBoxTxt").style.display = "inline";
	xGetElementById("MessageBox").style.display = "inline";
	if (eval(maintainPopup) == false) xGetElementById("PopupHolder").style.innerHTML = "";
	xGetElementById("PopupHolder").style.display = "none";
	if (showClose) xGetElementById("MessageBoxClose").style.display = "inline";
	else xGetElementById("MessageBoxClose").style.display = "none";
	centerMsgBox();
	document.body.style.overflow = "hidden";
	return void(0);}
// SHOWPOPUP
function showpopup (popupContent, popupWidth, showClose) {
	if (eval(xGetElementById("content")) != null) try {if (parseFloat(navigator.appVersion.split("MSIE")[1]) < 7)	xGetElementById("content").style.display = "none";}
		catch (e) {}
	xGetElementById("MessageBoxTrans").style.height = (xScrollTop() + xClientHeight()) + "px";
	xGetElementById("MessageBoxTrans").style.width = (xScrollLeft() + xClientWidth()) + "px";
	xGetElementById("MessageBoxTxt").innerHTML = "";
	xGetElementById("MessageBoxTxt").style.display = "none";
	xGetElementById("MessageBoxDiv").style.width = popupWidth;
	xGetElementById("MessageBoxTitle").style.display = "none";
	xGetElementById("PopupHolder").innerHTML = popupContent;
	xGetElementById("PopupHolder").style.width = popupWidth;
	xGetElementById("PopupHolder").style.display = "inline";
	xGetElementById("MessageBox").style.display = "inline";
	if (eval(showClose) == true) xGetElementById("MessageBoxClose").style.display = "inline";
	else xGetElementById("MessageBoxClose").style.display = "none";
	centerMsgBox();
	document.body.style.overflow = "hidden";
	return void(0);}
// HIDEMESSAGEBOX
function hidemessagebox (override) {
	if (eval(override) != true && xGetElementById("PopupHolder").innerHTML.length > 0 && xGetElementById("PopupHolder").style.display == "none") {
		showpopup(xGetElementById("PopupHolder").innerHTML, xGetElementById("PopupHolder").style.width, false);}
	else {
		xGetElementById("MessageBox").style.display = "none";
		xGetElementById("PopupHolder").innerHTML = "";
		xGetElementById("PopupHolder").style.display = "none";
		if (eval(xGetElementById("content")) != null) xGetElementById("content").style.display = "inline";}
	document.body.style.overflow = "visible";
	xGetElementById("MessageBoxDiv").style.backgroundColor = "#ffffff";		
	return void(0);}
// DISPLAYGENERICERROR
function displaygenericerror () {showmessagebox("Error", "An unknown error occured.<br/><br/>Please try again!!!", true);}
// XGETNODEVALUE
function xGetNodeValue(parent, tagName) {
	var node = parent.getElementsByTagName(tagName)[0];
	return (node && node.firstChild) ? node.firstChild.nodeValue : "";}
// XHCONN
function XHConn() {
	var xmlhttp, bComplete = false;
	try {xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");}
	catch (e) {try {xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");}
	catch (e) {try {xmlhttp = new XMLHttpRequest();}
	catch (e) { xmlhttp = false; }}}
	if (!xmlhttp) {alert("XMLHTTP not available. Try a newer/better browser."); return null;}
	this.connect = function(sURL, sMethod, sVars, fnDone) {
		if (!xmlhttp) return false;
		bComplete = false;
		sMethod = sMethod.toUpperCase();
		try {
			if (sMethod == "GET") {
				if (xTrim(sVars).length > 0) sVars += "&dummy=" + Math.round(10000 * Math.random());
				else sVars = "dummy=" + Math.round(10000 * Math.random());
				xmlhttp.open(sMethod, sURL + "?" + sVars, true);
				sVars = "";
			}
			else {
				xmlhttp.open(sMethod, sURL, true);
				xmlhttp.setRequestHeader("Method", "POST " + sURL + " HTTP/1.1");
				xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			}
			xmlhttp.onreadystatechange = function() { if (xmlhttp.readyState == 4 && !bComplete) { bComplete = true; fnDone(xmlhttp); } };
			xmlhttp.send(sVars);
		}
		catch (z) { return false; }
		return true;
	};
	return this;}
// XTRIM
function xTrim(s) {return s.replace(/^\s+|\s+$/g, '');}
// FORMATCURRENCY
function formatCurrency(num) {
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num)) num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10) cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++) 
		num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3));
	return (((sign)?'':'-') + '$' + num + '.' + cents);}
// XFIRSTCHILD
function xFirstChild(e,t) {
	e = xGetElementById(e);
	var c = e ? e.firstChild : null;
	while (c) {
		if (c.nodeType == 1 && (!t || c.nodeName.toLowerCase() == t.toLowerCase())){break;}
		c = c.nextSibling;}
	return c;}
// CENTERMSGBOX
function centerMsgBox() {
	xGetElementById('MessageBoxDiv').style.top = (xScrollTop() + (xClientHeight()) / 2) - (xHeight('MessageBoxDiv') / 2) + 'px';
	xGetElementById('MessageBoxDiv').style.left = (xClientWidth() / 2) - (xWidth('MessageBoxDiv') / 2) + 'px';}
// URLENCODE
function URLEncode (clearString) {
	var output = '';
	var x = 0;
	clearString = clearString.toString();
	var regex = /(^[a-zA-Z0-9_.]*)/;
	while (x < clearString.length) {
		var match = regex.exec(clearString.substr(x));
		if (match != null && match.length > 1 && match[1] != '') {
  		output += match[1];
			x += match[1].length;}
		else {
			if (clearString[x] == ' ') output += '+';
			else {
				var charCode = clearString.charCodeAt(x);
				var hexVal = charCode.toString(16);
				output += '%' + ( hexVal.length < 2 ? '0' : '' ) + hexVal.toUpperCase();}
			x++;}}
	return output;}
// URLDECODE
function URLDecode (encodedString) {
	var output = encodedString;
	var binVal, thisString;
	var myregexp = /(%[^%]{2})/;
	while ((match = myregexp.exec(output)) != null && match.length > 1 && match[1] != '') {
		binVal = parseInt(match[1].substr(1),16);
		thisString = String.fromCharCode(binVal);
		output = output.replace(match[1], thisString);}
	return output;}
// XDISP
function xDisp(e, bShow) {
	if(!(e=xGetElementById(e))) return null;
	if(e.style && xDef(e.style.display)) {
		if (xDef(bShow)) e.style.display = bShow ? 'inline' : 'none';
		return e.style.display;}
	return null;}
// REPLACEALL
function ReplaceAll(Source, Replace, With) {
  var temp = Source;
  var index = temp.indexOf(Replace);
  while(index != -1) {
    temp = temp.replace(Replace, With);
    index = temp.indexOf(Replace);}
  return temp;}
// TOBOOL
function toBool(src) {
	switch(src.toLowerCase()) {
		case "true": case "yes": case "1": return true;
		case "false": case "no": case "0": case null: return false;
		default: return Boolean(string);
	} 
}
// ISBLANK
function isblank (s) {
	return String(s).search(/\S/) == -1
}
// ISCURRENCY
function iscurrency (s) {
	return currencytodecimal(String(s)).search(/^\s*(\+|-)?((\d+(\.\d\d)?)|(\.\d\d))\s*$/) != -1
}
// CURRENCYTODECIMAL
function currencytodecimal (s) {
	return String(s).replace(/,/gi, "").replace(/\$/gi, "")
}
// TEXTCT
function textct(field, maxlimit) {
	if (field.value.length > maxlimit) {
		field.value = field.value.substring(0, maxlimit);
		alert(maxlimit + " character limit reached!");
	}
	else xGetElementById(field.id.toString() + "ct").innerHTML = maxlimit - field.value.length;
	return void (0);
}