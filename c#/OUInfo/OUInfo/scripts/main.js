/// <reference path="x.js" />
/// <reference path="common.js" />
var exec_url = "srvexec.aspx";

// UNLOCKMIGUSER
$(function() {
    $('a.unlockmig').on('click',function (evt) {
        evt.preventDefault();
        $.post("srvexec.aspx?a=unlockmig", function (result) {
            if (result.toLowerCase() == "true") alert("Account Unlocked!");
            else alert("Unable to Unlock Account!");
        });
        return void (0);
    });
});

//function unlockmig() {
//    $.get("srvexec.aspx?a=unlockmig", function (result) {
//        alert(result);
//    });
//    $(function () {
//        $('#unlockmig').click(function () {
//            alert('hello');
//            return false;
//        });
//    });
////  $.get("srvexec.aspx?a=unlockmig", function (result) { alert(result); });
//}

// SEARCH
function search1(txt) {
    $('#srchresults').dataTable({
        "dom": 'Rlfrtip',
        "pageLength": 50,
        "processing": true,
        //"bServerSide": true,
        "destroy": true,
        "ajax": {
            "url": "srvexec.aspx?a=search&s=" + txt,
            "type": "POST"
        },
        "columns": [
            { "data": "username" },
            { "data": "firstname" },
            { "data": "lastname" },
            { "data": "ou" },
            { "data": "dn" }
        ]
    });
    return void (0);
}

function search(txt) {
    var table;
    var url = "srvexec.aspx?a=search&s=" + txt;

    function format(d) {

        var sOut = "";

        $.ajax({
            url: "srvexec.aspx?a=getuser&s=" + d.aduserid + "," + d.username,
            async: false,
            dataType: 'json',
            success: function (json) {
                sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;"><tr>';
                //sOut += '<th>Email</th><th>Office</th><th>Ofc Phone</th><th>Cell Phone</th><th>Status</th><th>Enabled</th></tr>';
                sOut += '<tr>';
                sOut += '<td><a href="mailto:' + json.data[0].email + '">' + json.data[0].email + '</td>';
                sOut += '<td>' + json.data[0].office + '</td>';
                sOut += '<td>' + json.data[0].officephone + '</td>';
                sOut += '<td>' + json.data[0].mobilephone + '</td>';
                sOut += '<td><img src="../images/padlock_open.png"></td>'; // + if (toBool(json.data[0].islockedout) + '</td>';
                sOut += '</tr></table>';
            }
        });

        $("#dialog").html(sOut);
        $("#dialog").dialog({ modal: true, title: "Dialog Title", height: "auto", width: "auto" });
        //$("#dialog").dialog({ modal: true, title: "Dialog Title", buttons: [{ text: "Ok", click: function () { $(this).dialog("close"); } }] });
        //        $("#dialog").dialog("open");
        return;
    }

    if ($.fn.dataTable.isDataTable('#srchresults')) {
        table = $('#srchresults').DataTable().ajax.url(url).load();
        //$('#srchresults').DataTable().destroy();
    }
    else {
        table = $('#srchresults').DataTable({
            "dom": 'Rlfrtip',
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "All"]],
            "displaylength": 50,
            "processing": true,
            "pagingType": "full_numbers",
            //"serverSide": true,
            //"destroy": true,
            "ajax": {
                "url": url
                //,"type": "POST"
            },
            "columns": [
                {
                    "class": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                { "data": "username" },
                { "data": "firstname" },
                { "data": "lastname" },
                { "data": "ou" },
                { "data": "dn" }
            ],
            "order": [[2, 'asc']]
        });
    }

    $('#srchresults tbody').on('click', 'td.details-control', function () {
        var row = table.row($(this).closest('tr'));
        format(row.data())
        return;
    } );

    return void (0);
}

function toBool(src) {
    switch (src.toLowerCase()) {
        case "true": case "yes": case "1": return true;
        case "false": case "no": case "0": case null: return false;
        default: return Boolean(string);
    }
}
