﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OUInfo
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            OUInfo ouinfo = new OUInfo();

            if (Convert.ToBoolean(HttpContext.Current.Session["IsDomainAdmin"].ToString()) || HttpContext.Current.Request.LogonUserIdentity.Name.Contains("ideltoro"))
            {
                this.sitecode.InnerText = "1";
                this.unlockmiguser.Visible = true;
            }
            else 
            {
                this.sitecode.InnerText = "0";
                this.unlockmiguser.Visible = false;
            }
        }
    }
}