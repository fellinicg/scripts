﻿using FCGLogger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OUInfo
{
    public partial class srvexec : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Declarations
                string action = (HttpContext.Current.Request.Params["a"] == null ? "" : HttpContext.Current.Request.Params["a"].ToString());

                // Set all responses to text
                HttpContext.Current.Response.ContentType = "text/plain";

                // Execute the action
                HttpContext.Current.Response.Write(ProcessRequest(action));
            }
            catch (Exception ExceptionErr)
            {
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, HttpContext.Current.Session["ErrorLog"].ToString());
            }
        }

        private string ProcessRequest(string func)
        {
            // Declarations
            OUInfo ouinfo = new OUInfo();
            string Resp = string.Empty;

            try
            {
                switch (func)
                {
                    case "getuser":
                        Resp = ouinfo.GetUser(Context.Request.Params["s"].ToString());
                        break;
                    case "search":
                        Resp = ouinfo.SearchOU(Context.Request.Params["s"].ToString());
                        break;
                    case "unlockmig":
                        Resp = ouinfo.UnlockMiguser();
                        break;
                    default:
                        break;
                }

            }
            catch (Exception ExceptionErr)
            {
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, HttpContext.Current.Session["ErrorLog"].ToString());
            }

            // Return data
            return Resp;
        }
    }
}