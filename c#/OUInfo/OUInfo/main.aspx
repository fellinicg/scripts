﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="main.aspx.cs" Inherits="OUInfo.main" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Search OU</title>
    <link type="text/css" href="/css/page.css" media="all" rel="stylesheet" />
    <link type="text/css" href="/css/datatable.css" media="all" rel="stylesheet" />
	<script type="text/javascript" src="scripts/jquery.js"></script>
	<script type="text/javascript" src="scripts/datatables.js"></script>
	<script type="text/javascript" src="scripts/colreorder.js"></script>
	<script type="text/javascript" src="scripts/main.js"></script>
</head>
<body>
    <div id="container">
        Username or First Name or Last Name: 
			        <input id="txtsearch" type="text" onkeyup="javascript:if(event.keyCode==13) search(this.value);" />
			        <input type="button" value="Search" onclick="search(txtsearch.value)" />    

		<div>
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="srchresults">
	            <thead>
		            <tr>
			            <th width="10%">Username</th>
			            <th width="10%">First Name</th>
			            <th width="10%">Last Name</th>
			            <th width="40%">ActiveDirectory OU</th>
			            <th width="30%">Novell DN</th>
		            </tr>
	            </thead>
	            <tbody>
	            </tbody>
<%--	            <tfoot>
		            <tr>
			            <th>Username</th>
			            <th>First Name</th>
			            <th>Last Name</th>
			            <th>ActiveDirectory OU</th>
			            <th>Novell DN</th>
		            </tr>
	            </tfoot>--%>
            </table>
		</div>

    </div>
</body>
</html>