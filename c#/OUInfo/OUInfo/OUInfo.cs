﻿using FCGLogger;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using System.DirectoryServices.AccountManagement;
using System.Text;
using System.Web;

namespace OUInfo
{
    public class OUInfo
    {

        #region Declarations

        private string ConnectionString = string.Empty;
        private string ErrorLog = string.Empty;
        private bool IsDomainAdmin = false;
        private string Log = string.Empty;
        private string XmlDataPath = string.Empty;

        #endregion

        #region Public Functions

        public string GetUser(string User)
        {

            // Declarations
            string rs = string.Empty;
            int islocked = Convert.ToInt32(AccountLockedOut(User.Split((",").ToCharArray())[1]));

            try
            {
                // Declarations
                SqlCommand comm = new SqlCommand();

                // Add parameters
                comm.Parameters.Add("@ADUserID", SqlDbType.Int, 4).Value = User.Split((",").ToCharArray())[0];
                comm.Parameters.Add("@IsLockedOut", SqlDbType.Int, 4).Value = islocked;

                // Get data
                rs = SqlToJSON(comm.Parameters, "upGetUser", System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ExceptionErr)
            {
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, ErrorLog);
            }

            // Return result
            return rs;
        }

        public OUInfo()
        {
            try
            {
                // Check whether its a web or console app
                if (System.Web.HttpContext.Current == null)
                {
                    InitializeClassVariables();
                }
                else
                {
                    // Attempt to get path to xmldata
                    try { XmlDataPath = HttpContext.Current.Request.MapPath("xmldata", "/", false); }
                    catch { }

                    // Check session variables
                    if (HttpContext.Current.Session["ConnectionString"] == null ||
                        HttpContext.Current.Session["ErrorLog"] == null ||
                        HttpContext.Current.Session["Log"] == null ||
                        HttpContext.Current.Session["IsDomainAdmin"] == null)
                    {
                        InitializeSession();
                    }
                    else
                    {
                        ConnectionString = HttpContext.Current.Session["ConnectionString"].ToString();
                        ErrorLog = (HttpContext.Current.Session["ErrorLog"]).ToString();
                        Log = (HttpContext.Current.Session["Log"]).ToString();
                        IsDomainAdmin =  Convert.ToBoolean(HttpContext.Current.Session["IsDomainAdmin"].ToString());
                    }
                }
            }

            catch (Exception ExceptionErr)
            {
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, ErrorLog);
            }
        }

        public string SearchOU(string Search)
        {
            // Declarations
            string rs = string.Empty;

            try
            {
                // Declarations
                SqlCommand comm = new SqlCommand();

                // Add parameters
                comm.Parameters.Add("@Search", SqlDbType.VarChar, 50).Value = Search;

                // Get data
                rs = SqlToJSON(comm.Parameters, "dbo.upSearchOU", System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ExceptionErr)
            {
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, ErrorLog);
            }

            // Return result
            return rs;
        }

        public string UnlockMiguser()
        {
            // Declarations
            string ret = "false";

            try
            {
                // Execute if current user is a domain admin or Ivan
                if (IsDomainAdmin || System.Web.HttpContext.Current.Request.LogonUserIdentity.Name.Contains("ideltoro")) ret = UnlockUser("miguser").ToString();
            }
            catch (Exception ExceptionErr)
            {
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, ErrorLog);
            }

            return ret;
        }

        #endregion

        #region Private Functions

        private bool AccountLockedOut(string User)
        {
            // Declarations
            bool ret = false;

            try
            {
                PrincipalContext oPrincipalContext = null;
                UserPrincipal oUserPrincipal = null;
                Domain domain = Domain.GetCurrentDomain();
                foreach (DomainController dc in domain.DomainControllers)
                {
                    oPrincipalContext = new PrincipalContext(ContextType.Domain, dc.Name);
                    oUserPrincipal = UserPrincipal.FindByIdentity(oPrincipalContext, User);
                    if (oUserPrincipal.IsAccountLockedOut()) return true;
                }
            }
            catch (Exception ExceptionErr)
            {
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, ErrorLog);
            }

            return ret;
        }

        private void InitializeClassVariables()
        {
            // Declarations
            AppSettingsReader AppSetReader = new AppSettingsReader();

            // Attempt to get path to xmldata
            try { XmlDataPath = System.Reflection.Assembly.GetEntryAssembly().Location.Substring(0, System.Reflection.Assembly.GetEntryAssembly().Location.LastIndexOf("\\") + 1) + "xmldata"; }
            catch { }

            // Retrieve database connection
            ConnectionString = (string)AppSetReader.GetValue("DB", typeof(string));

            // Retrieve ErrorLog
            try { ErrorLog = (string)AppSetReader.GetValue("ErrorLog", typeof(string)); }
            catch
            {
                try { ErrorLog = HttpContext.Current.Request.MapPath("logfiles", "/", false) + "\\ErrorLog.xml"; }
                catch
                {
                    try { ErrorLog = System.Reflection.Assembly.GetEntryAssembly().Location.Substring(0, System.Reflection.Assembly.GetEntryAssembly().Location.LastIndexOf("\\") + 1) + "ErrorLog.xml"; }
                    catch
                    { ErrorLog = string.Empty; }
                }
            }

            // Retrieve Log
            try { Log = (string)AppSetReader.GetValue("Log", typeof(string)); }
            catch
            {
                try { Log = HttpContext.Current.Request.MapPath("logfiles", "/", false) + "\\Log.xml"; }
                catch
                {
                    try { Log = System.Reflection.Assembly.GetEntryAssembly().Location.Substring(0, System.Reflection.Assembly.GetEntryAssembly().Location.LastIndexOf("\\") + 1) + "Log.xml"; }
                    catch
                    { Log = string.Empty; }
                }
            }

            // Set domain admin status
            IsDomainAdmin = IsDomainAdministrator();
            //if (System.Web.HttpContext.Current.Request.LogonUserIdentity.Name.Contains("ideltoro") && !IsDomainAdmin) IsDomainAdmin = true;

        }

        private void InitializeSession()
        {
            // Declarations
            AppSettingsReader AppSetReader = new AppSettingsReader();

            // Retrieve database connection
            ConnectionString = (HttpContext.Current.Session["ConnectionString"] = (string)AppSetReader.GetValue("DB", typeof(string))).ToString();

            // Retrieve ErrorLog
            try { HttpContext.Current.Session["ErrorLog"] = (string)AppSetReader.GetValue("ErrorLog", typeof(string)); }
            catch
            {
                try { HttpContext.Current.Session["ErrorLog"] = HttpContext.Current.Request.MapPath("logfiles", "/", false) + "\\ErrorLog.xml"; }
                catch
                {
                    try { HttpContext.Current.Session["ErrorLog"] = System.Reflection.Assembly.GetEntryAssembly().Location.Substring(0, System.Reflection.Assembly.GetEntryAssembly().Location.LastIndexOf("\\") + 1); }
                    catch
                    { HttpContext.Current.Session["ErrorLog"] = string.Empty; }
                }
            }
            ErrorLog = (HttpContext.Current.Session["ErrorLog"]).ToString();

            // Retrieve Log
            try { HttpContext.Current.Session["Log"] = (string)AppSetReader.GetValue("Log", typeof(string)); }
            catch
            {
                try { HttpContext.Current.Session["Log"] = HttpContext.Current.Request.MapPath("logfiles", "/", false) + "\\Log.xml"; }
                catch
                {
                    try { HttpContext.Current.Session["Log"] = System.Reflection.Assembly.GetEntryAssembly().Location.Substring(0, System.Reflection.Assembly.GetEntryAssembly().Location.LastIndexOf("\\") + 1) + "Log.xml"; }
                    catch
                    { HttpContext.Current.Session["Log"] = string.Empty; }
                }
            }
            Log = (HttpContext.Current.Session["Log"]).ToString();

            // Set domain admin status
            HttpContext.Current.Session["IsDomainAdmin"] =  (IsDomainAdmin = IsDomainAdministrator()).ToString();
           
        }

        private bool IsDomainAdministrator()
        {
            // Declarations
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain); 

            // Return
            return System.Web.HttpContext.Current.Request.LogonUserIdentity.Groups.Contains(GroupPrincipal.FindByIdentity(ctx, "Domain Admins").Sid);
        }

        private string SqlToJSON(SqlParameterCollection SqlParams, string StoredProcedure, string CallingFunc)
        {
            return SqlToJSON(SqlParams, StoredProcedure, CallingFunc, 0);
        }

        private string SqlToJSON(SqlParameterCollection SqlParams, string StoredProcedure, string CallingFunc, int EmptyRecords)
        {
            // Declarations
            string JSON = string.Empty;
            SqlConnection conn = new SqlConnection(ConnectionString);

            try
            {
                // Declarations
                SqlCommand comm = new SqlCommand(StoredProcedure, conn);

                // Set command type
                comm.CommandType = CommandType.StoredProcedure;

                // Add parameters
                for (int i = 0; i < SqlParams.Count; i++)
                    comm.Parameters.Add(SqlParams[i].ParameterName.ToString(), SqlParams[i].SqlDbType, SqlParams[i].Size).Value = SqlParams[i].Value;

                // Open Connection
                comm.Connection.Open();

                // Assign to datareader
                JSON = ReaderToJSON(comm.ExecuteReader(), EmptyRecords);

                // Close connection
                conn.Close();
            }
            catch (Exception ExceptionErr)
            {
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, ErrorLog, "Called from: " + CallingFunc);
                if (conn.State != ConnectionState.Closed) conn.Close();
            }

            // Return result
            return JSON;
        }

        private string ReaderToJSON(SqlDataReader dr, int EmptyRecords)
        {
            // Declarations
            StringBuilder sbJSON = new StringBuilder();
            int FieldCount = 0;
            bool AddComma = false;
            string varID = "$";

            try
            {
                // Get number of fields(columns) in the DataReader
                FieldCount = dr.FieldCount;

                // Initiate with our standard data object
                sbJSON.Append("{\"data\":[");

                // Loop of Datareader
                while (dr.Read())
                {
                    // Initiate row
                    sbJSON.Append((AddComma ? "," : "") + "{");

                    // Loop over fields
                    for (int i = 0; i < FieldCount; i++)
                    {
                        // Pair string will be field name in lowercase
                        sbJSON.Append("\"" + dr.GetName(i).ToString().Trim().ToLower() + "\":");
                        // Pair value will be field data cleansed so we have well-formed JSON
                        sbJSON.Append("\"" + dr[i].ToString().Trim().Replace("\"", "\\\"").Replace("\\", "\\\\").Replace("\b", "\\b").Replace("\b", "\\b").Replace("\t", "\\t").Replace("\n", "\\n").Replace("\f", "\\f").Replace("\r", "\\r")
                            + "\"" + (i < FieldCount - 1 ? "," : ""));
                    }

                    // Close object
                    sbJSON.Append("}");

                    // Reset flag
                    AddComma = true;
                }

                // Add empty records
                for (int y = 0; y < EmptyRecords; y++)
                {
                    // Initiate member object
                    sbJSON.Append((AddComma ? "," : "") + "{");

                    // Loop over fields
                    for (int i = 0; i < FieldCount; i++)
                    {
                        // Pair string will be field name in lowercase
                        sbJSON.Append("\"" + dr.GetName(i).ToString().Trim().ToLower() + "\":");
                        // Pair value will be field name plus counter enclosed in varID
                        sbJSON.Append("\"" + varID + dr.GetName(i).ToString().Trim().ToLower() + y.ToString() + varID + "\"" +
                            (i < FieldCount - 1 ? "," : ""));
                    }

                    // Close object
                    sbJSON.Append("}");

                    // Reset flag
                    AddComma = true;
                }

                // Close Array and objects
                sbJSON.Append("]}");

                // Close Datareader
                dr.Close();
            }
            catch (Exception ExceptionErr)
            {
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, ErrorLog);
                if (!dr.IsClosed) dr.Close();
                sbJSON.Remove(0, sbJSON.Length);
                sbJSON.Append("{\"data\":{\"resultcode\":false}}");
            }

            // Return XML
            return sbJSON.ToString();
        }

        private bool UnlockUser(string User)
        {
            // Declarations
            bool ret = false;

            try
            {
                PrincipalContext oPrincipalContext = null;
                UserPrincipal oUserPrincipal = null;
                Domain domain = Domain.GetCurrentDomain();
                foreach (DomainController dc in domain.DomainControllers)
                {
                    oPrincipalContext = new PrincipalContext(ContextType.Domain, dc.Name);
                    oUserPrincipal = UserPrincipal.FindByIdentity(oPrincipalContext, User);
                    oUserPrincipal.UnlockAccount();
                }
                ret = !oUserPrincipal.IsAccountLockedOut();
            }
            catch (Exception ExceptionErr)
            {
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, ErrorLog);
            }

            return ret;
        }

        #endregion

    }
}