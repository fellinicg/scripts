/// <reference path="x.js" />
/// <reference path="common.js" />
var exec_url = "srvexec.aspx";

// UNLOCKMIGUSER
$(function() {
    $('a.unlockmig').on('click',function (evt) {
        evt.preventDefault();
        $.post("srvexec.aspx?a=unlockmig", function (result) {
            if (result.toLowerCase() == "true") alert("Account Unlocked!");
            else alert("Unable to Unlock Account!");
        });
        return void (0);
    });
});

//function unlockmig() {
//    //alert("shit");
//    $.get("srvexec.aspx?a=unlockmig", function (result) {
//        alert(result);
//    });
//    $(function () {
//        $('#unlockmig').click(function () {
//            alert('hello');
//            return false;
//        });
//    });
////  $.get("srvexec.aspx?a=unlockmig", function (result) { alert(result); });
//}

// SEARCH
function search(txt) {
    var oTable = $('#srchresults').dataTable({
        "sDom": 'Rlfrtip',
        "iDisplayLength": 50,
        "bProcessing": true,
        //"bServerSide": true,
        "bDestroy": true,
        "sAjaxSource": "srvexec.aspx?a=search&s=" + txt,
        "sAjaxDataProp": "data",
        "aoColumns": [
            { "mData": "username" },
            { "mData": "firstname" },
            { "mData": "lastname" },
            { "mData": "ou" },
            { "mData": "dn" }
        ]
    });
    return void (0);
}
function search1() {

    function fnFormatDetails(oTable, nTr, id) {
        var sOut = "";
        $.ajax({
            url: "srvexec.aspx?a=search&s=" + id,
            async: false,
            dataType: 'json',
            success: function (json) {
                sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
                sOut += '<tr><td>Department Name:</td><td>' + json.data[0].departmentname + '</td></tr>';
                sOut += '<tr><td>Alloy ID:</td><td>' + json.data[0].alloyid + '</td></tr>';
                sOut += '<tr><td>Even more info:</td><td>And any further details here</td></tr>';
                sOut += '</table>';
            }
        });
        return sOut;
    }

    var oTable = $('#servers2').dataTable({
        "sDom": 'Rlfrtip',
        "aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
        "iDisplayLength": 5,
        "bProcessing": true,
        "sPaginationType": "full_numbers",
        //"bServerSide": true,
        "sAjaxSource": "srvexec.aspx?a=getsrvs",
        "sAjaxDataProp": "data",
        "aoColumns": [
            {
                "mData": null, "bSortable": false, "mRender": function (data, type, full) {
                    return '<a href=javascript:void(0)><img id="' + full.serverid + '" src="../images/details_open.png"></a>';
                }
            },
            { "mData": "name" },
            { "mData": "type" },
            { "mData": "serialnum" },
            { "mData": "status" }
        ],
        "aaSorting": [[1, 'asc']]
    });

    $('#servers2 tbody td img').live('click', function () {
        alert(this.id);
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr)) {
            /* This row is already open - close it */
            this.src = "../images/details_open.png";
            oTable.fnClose(nTr);
        }
        else {
            /* Open this row */
            this.src = "../images/details_close.png";
            oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr, this.id), 'details');
        }
    });

    return void (0);
}
