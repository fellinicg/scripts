﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="OUInfo._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Search OU</title>
    <link type="text/css" href="css/page.css" media="all" rel="stylesheet" />
    <link type="text/css" href="css/datatable.css" media="all" rel="stylesheet" />
    <link type="text/css" href="css/jquery-ui.css" media="all" rel="stylesheet" />
	<%--<script type="text/javascript" src="scripts/jquery_1_8_2.js"></script>--%>
    <script type="text/javascript" src="scripts/jquery_1_11_1.js"></script>
	<%--<script type="text/javascript" src="scripts/datatables_1_9_4.js"></script>--%>
    <script type="text/javascript" src="scripts/datatables_1_10_1.js"></script>
    <script type="text/javascript" src="scripts/colreorder_1_1_2.js"></script>
	<script type="text/javascript" src="scripts/main.js"></script>
    <script type="text/javascript" src="scripts/jquery-ui.js"></script>
    <%--<script>$("#dialog").dialog({ autoOpen: false });</script>--%>
</head>
<body>
    <div id="dialog"></div>
    <div id="sitecode" runat="server" style="display:none"></div>
    <div id="unlockmiguser" runat="server" visible="false"><a class="unlockmig" href="javascript:void(0);">Unlock MigUser</a><br /></div>
    <div id="container">
        Username or First Name or Last Name: 
			        <input id="txtsearch" type="text" onkeyup="javascript:if(event.keyCode==13) search(this.value);" value="tfellini"/>
			        <input type="button" value="Search" onclick="search(txtsearch.value)" />    

		<div>
            <table width="100%" cellspacing="0" class="display compact" id="srchresults">
	            <thead>
		            <tr>
                        <th></th>
			            <th>Username</th>
			            <th>First Name</th>
			            <th>Last Name</th>
			            <th>ActiveDirectory OU</th>
			            <th>Novell DN</th>
                        <%--<th width="20px"></th>--%>
                        <%--<th width="10%">Username</th>
			            <th width="10%">First Name</th>
			            <th width="10%">Last Name</th>
			            <th width="40%">ActiveDirectory OU</th>
			            <th width="30%">Novell DN</th>--%>
		            </tr>
	            </thead>
	            <tbody>
	            </tbody>
<%--	            <tfoot>
		            <tr>
			            <th>Username</th>
			            <th>First Name</th>
			            <th>Last Name</th>
			            <th>ActiveDirectory OU</th>
			            <th>Novell DN</th>
		            </tr>
	            </tfoot>--%>
            </table>
		</div>

    </div>
</body>
</html>