using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Threading;

namespace FCGErrorLogger
{
	public class Logger
	{
		public Logger(Exception ErrException, string LogFile)
		{
			// Call function
			LogError(ErrException, LogFile, "");
		}

		public Logger(Exception ErrException, string LogFile, string AdditionalInfo)
		{
			// Call function
			LogError(ErrException, LogFile, AdditionalInfo);
		}

		private void LogError(Exception ErrException, string LogFile, string AdditionalInfo)
		{
			// Declarations
			Mutex ErrMutex = new Mutex(true);

			try
			{
				// Check to see if file exists
				if (!File.Exists(LogFile))
				{
					XmlTextWriter Writer = new XmlTextWriter(LogFile, null);
					Writer.WriteStartDocument();
					Writer.WriteStartElement("Errors");
					Writer.WriteEndElement();
					Writer.WriteEndDocument();
					Writer.Close();
				}

				// Add a mutex to avoid multiple user accessing the xmlfile for write.
				ErrMutex.WaitOne();

				// Create an XmlDocument
				XmlDocument ErrXmlDocument = new XmlDocument();
				ErrXmlDocument.Load(LogFile);

				// Create Error node
				XmlElement ErrXmlElement = ErrXmlDocument.CreateElement("Error");
				ErrXmlDocument.DocumentElement.AppendChild(ErrXmlElement);

				// Create Date node
				ErrXmlDocument.DocumentElement.LastChild.AppendChild(ErrXmlDocument.CreateElement("Date"));
				ErrXmlDocument.DocumentElement.LastChild.LastChild.InnerText = DateTime.Now.ToString();

				// Create Message node
				ErrXmlDocument.DocumentElement.LastChild.AppendChild(ErrXmlDocument.CreateElement("Message"));
				ErrXmlDocument.DocumentElement.LastChild.LastChild.InnerText = ErrException.Message;
				ErrXmlDocument.DocumentElement.LastChild.LastChild.Normalize();

				// Create Type node
				ErrXmlDocument.DocumentElement.LastChild.AppendChild(ErrXmlDocument.CreateElement("Type"));
				ErrXmlDocument.DocumentElement.LastChild.LastChild.InnerText = ErrException.GetType().FullName;

				// Create Source node
				ErrXmlDocument.DocumentElement.LastChild.AppendChild(ErrXmlDocument.CreateElement("Source"));
				ErrXmlDocument.DocumentElement.LastChild.LastChild.InnerText = ErrException.Source;

				// Create StackTrace
				ErrXmlDocument.DocumentElement.LastChild.AppendChild(ErrXmlDocument.CreateElement("StackTrace"));
				ErrXmlDocument.DocumentElement.LastChild.LastChild.InnerText = ErrException.StackTrace;

				// Create AdditionalInfo node
				if (AdditionalInfo.Trim().Length > 0)
				{
					ErrXmlDocument.DocumentElement.LastChild.AppendChild(ErrXmlDocument.CreateElement("AdditionalInfo"));
					ErrXmlDocument.DocumentElement.LastChild.LastChild.InnerText = AdditionalInfo;
					ErrXmlDocument.DocumentElement.LastChild.LastChild.Normalize();
				}

				// Save document
				ErrXmlDocument.Save(LogFile);

			}
			catch
			{
			}
			finally
			{
				// Release the mutex
				if (ErrMutex != null)
					ErrMutex.ReleaseMutex();
			}
		}
	}
}
