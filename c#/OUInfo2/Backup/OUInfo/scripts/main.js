﻿/// <reference path="common.js" />
/// <reference path="x.js" />

function search() {
	toggle('divSearchResults', 'divMachineDetails')
	var s = xGetElementById("txtsearch");
	if (!s.value) {
		alert("Machine name required!");
		s.focus()
		return;
	}
	var formdata = "a=search&s=" + s.value;
	var myConn = new XHConn();
	myConn.connect(exec_url, "GET", formdata, fnWhenDone);
		var tb = xGetElementById("tbResults");
		tb.innerText = "";
		var r = tb.insertRow(0);
		r.insertCell(0).innerHTML = "<center><b>Name</b></center>";
		r.insertCell(1).innerHTML = "<center><b>Major Application</b></center>";
		var oJSON = YAHOO.lang.JSON.parse(JSON.responseText);
		if (oJSON != null && oJSON.data.resultcode) {
			for (x = 0; x < oJSON.data.row.length; x++) {
				r = tb.insertRow(tb.rows.length);
				r.insertCell(0).innerHTML = '<a href="javascript:void(0);" onclick="javascript:getmch(\'' + oJSON.data.row[x].id + '\')">' + oJSON.data.row[x].name + '</a>';
				r.insertCell(1).innerHTML = htmlencode(oJSON.data.row[x].udf_majorapplications);
			}
		}
		else {
			r = tb.insertRow(tb.rows.length);
			var c = r.insertCell(0);
			c.setAttribute("colspan", "2");
			c.innerHTML = "No machines found.";
		}
	}
	return void (0);
}

function getmch(m) {
	var tb = xGetElementById("tbDetails");
	var formdata = "a=getmchdetail&m=" + m;
	tb.innerText = "";
	var myConn = new XHConn();
	myConn.connect(exec_url, "GET", formdata, fnWhenDone);
	function fnWhenDone(JSON) {
		var oJSON = YAHOO.lang.JSON.parse(JSON.responseText);
		if (oJSON != null && oJSON.data.resultcode) {
			for (x = 0; x < oJSON.data.row.length; x++) {
				for (var key in oJSON.data.row[x]) {
					if (key.indexOf("id") == -1) {
						var r = tb.insertRow(tb.rows.length);
						r.insertCell(0).innerHTML = "<b>" + key.replaceAll("_", " ").toProperCase() + "</b>";
						r.insertCell(1).innerHTML = htmlencode(oJSON.data.row[x][key]);
					}
				}
			}
		}
		else {
			var r = tb.insertRow(tb.rows.length);
			var c = r.insertCell(0);
			c.setAttribute("colspan", "2");
			c.innerHTML = "No details found.";
		}
	}
	toggle('divMachineDetails', 'divSearchResults');
	return void (0);
}
function toggle(show, hide) {
	xDisp(show, true);
	xDisp(hide, false);
}