﻿using FCGErrorLogger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ServerInfo
{
	public partial class srvexec : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				// Declarations
				string action = (Context.Request.Params["a"] == null ? "" : Context.Request.Params["a"].ToString());

				// Set all responses to XML
				Context.Response.ContentType = "text/plain";

				// Execute the action
				Context.Response.Write(ProcessRequest(action));
				Context.Response.Flush();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, HttpContext.Current.Session["ErrorLog"].ToString());
			}
		}

		private string ProcessRequest(string func)
		{
			// Declarations
			serverinfo csSI = new serverinfo();
			string Resp = string.Empty;

			try
			{
				switch (func)
				{
					case "getmchdetail":
						Resp = csSI.GetMachineDetails(Context.Request.Params["m"].ToString());
						break;

					case "search":
						Resp = csSI.GetMachines(Context.Request.Params["s"].ToString());
						break;

					default:
						break;
				}

			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, HttpContext.Current.Session["ErrorLog"].ToString());
			}

			// Return data
			return Resp;
		}
	}
}
