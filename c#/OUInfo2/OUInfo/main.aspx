﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="main.aspx.cs" Inherits="OUInfo._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
  <title>Machine Info</title>
	<script type="text/javascript" src="/scripts/common.js"></script>
	<script type="text/javascript" src="/scripts/main.js"></script>
	<script type="text/javascript" src="/scripts/x.js"></script>
	<script type="text/javascript" src="/scripts/yahoo-dom-event.js"></script>
	<script type="text/javascript" src="/scripts/json-min.js"></script>
</head>
<body>
    <div id="divSearch">
			Username or First Name or Last Name: 
			<input id="txtsearch" type="text" onkeyup="javascript:if(event.keyCode==13) search();" />
			<input type="button" value="Search" onclick="search()" />
    </div>
    <br />
    <div id="divSearchResults">
			<table id="tbResults" border="1">
			</table>
    </div>
    <div id="divMachineDetails" style="display:none">
			<a href="javascript:void(0);" onclick="toggle('divSearchResults', 'divMachineDetails')"><< Return</a>
			<table id="tbDetails" border="1">
			</table>
    </div>
</body>
</html>
