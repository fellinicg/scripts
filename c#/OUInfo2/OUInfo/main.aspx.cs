﻿using FCGErrorLogger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OUInfo
{
	public partial class _Default : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				// Declarations
				OUInfo csSI = new OUInfo();

				// Add OnLoad event to body tag
//				this.pmbody.Attributes.Add("OnLoad", "pageload(" + Session["UserAccountID"].ToString() + ");");
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, HttpContext.Current.Session["ErrorLog"].ToString());
			}
		}
	}
}
