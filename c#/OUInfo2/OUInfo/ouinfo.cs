﻿using FCGErrorLogger;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;

namespace OUInfo
{
	public class OUInfo
	{
	#region Declarations

		private bool cIsDevelopmentPlatform = false;
		private string cConnectionStr = string.Empty;
		private string cXmlDataPath = string.Empty;
		private string cErrorLog = string.Empty;

	#endregion

		public string GetMachines(string Search)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@Search", SqlDbType.VarChar, 50).Value = Search;

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetMachines", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		public string GetMachineDetails(string ID)
		{
			// Declarations
			string rs = string.Empty;

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand();

				// Add parameters
				comm.Parameters.Add("@ID", SqlDbType.UniqueIdentifier).Value = new Guid(ID);

				// Get data
				rs = SqlToJSON(comm.Parameters, "upGetMachineDetails", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}

			// Return result
			return rs;
		}

		private void InitializeClassVariables()
		{
			// Declarations
			AppSettingsReader AppSetReader = new AppSettingsReader();

			// Attempt to get path to xmldata
			try { cXmlDataPath = System.Reflection.Assembly.GetEntryAssembly().Location.Substring(0, System.Reflection.Assembly.GetEntryAssembly().Location.LastIndexOf("\\") + 1) + "xmldata"; }
			catch { }

			// Retrieve platform
			try
			{
				cIsDevelopmentPlatform = (bool)AppSetReader.GetValue("Development", typeof(bool));
			}
			catch { }

			// Retrieve database connection and SMTP information
			if (cIsDevelopmentPlatform)
			{
				cConnectionStr = (string)AppSetReader.GetValue("DevDB", typeof(string));
			}
			else
			{
				cConnectionStr = (string)AppSetReader.GetValue("DB", typeof(string));
			}

			// Retrieve cErrorLog
			try { cErrorLog = (string)AppSetReader.GetValue("cErrorLog", typeof(string)); }
			catch
			{
				try { cErrorLog = HttpContext.Current.Request.MapPath("logs", "/", false) + "\\cErrorLog.xml"; }
				catch
				{
					try { cErrorLog = System.Reflection.Assembly.GetEntryAssembly().Location.Substring(0, System.Reflection.Assembly.GetEntryAssembly().Location.LastIndexOf("\\") + 1) + "cErrorLog.xml"; }
					catch
					{ cErrorLog = string.Empty; }
				}
			}
		}

		private void InitializeSession()
		{
			// Declarations
			AppSettingsReader AppSetReader = new AppSettingsReader();

			// Retrieve platform
			try
			{
				HttpContext.Current.Session["IsDevelopmentPlatform"] = (bool)AppSetReader.GetValue("Development", typeof(bool));
			}
			catch { }

			// Retrieve database connection
			if (Convert.ToBoolean(HttpContext.Current.Session["IsDevelopmentPlatform"].ToString().ToLower()))
			{
				HttpContext.Current.Session["ConnectionStr"] =
					(string)AppSetReader.GetValue("DevDB", typeof(string));
			}
			else
			{
				HttpContext.Current.Session["ConnectionStr"] =
					(string)AppSetReader.GetValue("DB", typeof(string));
			}

			// Retrieve ErrorLog
			try { HttpContext.Current.Session["ErrorLog"] = (string)AppSetReader.GetValue("ErrorLog", typeof(string)); }
			catch
			{
				try { HttpContext.Current.Session["ErrorLog"] = HttpContext.Current.Request.MapPath("logs", "/", false) + "\\cErrorLog.xml"; }
				catch
				{
					try { HttpContext.Current.Session["ErrorLog"] = System.Reflection.Assembly.GetEntryAssembly().Location.Substring(0, System.Reflection.Assembly.GetEntryAssembly().Location.LastIndexOf("\\") + 1); }
					catch
					{ HttpContext.Current.Session["ErrorLog"] = string.Empty; }
				}
			}
		}

		public OUInfo()
		{
			try
			{
				// Check whether its a web or console app
				if (System.Web.HttpContext.Current == null)
				{
					InitializeClassVariables();
				}
				else
				{
					// Attempt to get path to xmldata
					try { cXmlDataPath = HttpContext.Current.Request.MapPath("xmldata", "/", false); }
					catch { }

					// Check session variables
					if (HttpContext.Current.Session["IsDevelopmentPlatform"] == null ||
						HttpContext.Current.Session["ConnectionStr"] == null ||
						HttpContext.Current.Session["ErrorLog"] == null)
					{
						InitializeSession();
					}

					// Retrieve platform
					try
					{
						cIsDevelopmentPlatform = Convert.ToBoolean(HttpContext.Current.Session["IsDevelopmentPlatform"].ToString().ToLower());
					}
					catch { }

					// Retrieve database connection and SMTP information
					cConnectionStr = HttpContext.Current.Session["ConnectionStr"].ToString();

					// Retrieve cErrorLog
					cErrorLog = HttpContext.Current.Session["ErrorLog"].ToString();
				}
			}

			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
			}
		}

		private string SqlToJSON(SqlParameterCollection SqlParams, string StoredProcedure, string CallingFunc)
		{
			return SqlToJSON(SqlParams, StoredProcedure, CallingFunc, 0);
		}

		private string SqlToJSON(SqlParameterCollection SqlParams, string StoredProcedure, string CallingFunc, int EmptyRecords)
		{
			// Declarations
			string JSON = string.Empty;
			SqlConnection conn = new SqlConnection(cConnectionStr);

			try
			{
				// Declarations
				SqlCommand comm = new SqlCommand(StoredProcedure, conn);

				// Set command type
				comm.CommandType = CommandType.StoredProcedure;

				// Add parameters
				for (int i = 0; i < SqlParams.Count; i++)
					comm.Parameters.Add(SqlParams[i].ParameterName.ToString(), SqlParams[i].SqlDbType, SqlParams[i].Size).Value = SqlParams[i].Value;

				// Open Connection
				comm.Connection.Open();

				// Assign to datareader
				JSON = ReaderToJSON(comm.ExecuteReader(), EmptyRecords);

				// Close connection
				conn.Close();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog, "Called from: " + CallingFunc);
				if (conn.State != ConnectionState.Closed) conn.Close();
			}

			// Return result
			return JSON;
		}

		private string ReaderToJSON(SqlDataReader dr, int EmptyRecords)
		{
			// Declarations
			StringBuilder sbJSON = new StringBuilder();
			int FieldCount = 0;
			bool AddComma = false;
			string varID = "$";

			try
			{
				// Get number of fields(columns) in the DataReader
				FieldCount = dr.FieldCount;

				// Initiate with our standard member object
				sbJSON.Append("{\"data\":{");

				// Add resultcode pair
				sbJSON.Append("\"resultcode\":true,");

				// Add row array
				sbJSON.Append("\"row\":[");

				// Loop of Datareader
				while (dr.Read())
				{
					// Initiate member object
					sbJSON.Append((AddComma ? "," : "") + "{");

					// Loop over fields
					for (int i = 0; i < FieldCount; i++)
					{
						// Pair string will be field name in lowercase
						sbJSON.Append("\"" + dr.GetName(i).ToString().Trim().ToLower() + "\":");
						// Pair value will be field data cleansed so we have well-formed JSON
						sbJSON.Append("\"" + dr[i].ToString().Trim().Replace("\"", "\\\"").Replace("\\", "\\\\").Replace("\b", "\\b").Replace("\b", "\\b").Replace("\t", "\\t").Replace("\n", "\\n").Replace("\f", "\\f").Replace("\r", "\\r")
							+ "\"" + (i < FieldCount - 1 ? "," : ""));
					}

					// Close object
					sbJSON.Append("}");

					// Reset flag
					AddComma = true;
				}

				// Add empty records
				for (int y = 0; y < EmptyRecords; y++)
				{
					// Initiate member object
					sbJSON.Append((AddComma ? "," : "") + "{");

					// Loop over fields
					for (int i = 0; i < FieldCount; i++)
					{
						// Pair string will be field name in lowercase
						sbJSON.Append("\"" + dr.GetName(i).ToString().Trim().ToLower() + "\":");
						// Pair value will be field name plus counter enclosed in varID
						sbJSON.Append("\"" + varID + dr.GetName(i).ToString().Trim().ToLower() + y.ToString() + varID + "\"" +
							(i < FieldCount - 1 ? "," : ""));
					}

					// Close object
					sbJSON.Append("}");

					// Reset flag
					AddComma = true;
				}

				// Close Array and objects
				sbJSON.Append("]}}");

				// Close Datareader
				dr.Close();
			}
			catch (Exception ExceptionErr)
			{
				FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
				if (!dr.IsClosed) dr.Close();
				sbJSON.Remove(0, sbJSON.Length);
				sbJSON.Append("{\"data\":{\"resultcode\":false}}");
			}

			// Return XML
			return sbJSON.ToString();
		}

        public string SearchOU(string Search)
        {
            // Declarations  'todd'
            string rs = string.Empty;

            try
            {
                // Declarations
                SqlCommand comm = new SqlCommand();

                // Add parameters
                comm.Parameters.Add("@Search", SqlDbType.VarChar, 50).Value = Search;

                // Get data
                rs = SqlToJSON(comm.Parameters, "dbo.upSearchOU", System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ExceptionErr)
            {
                FCGErrorLogger.Logger errLog = new Logger(ExceptionErr, cErrorLog);
            }

            // Return result
            return rs;
        }


    }
}
