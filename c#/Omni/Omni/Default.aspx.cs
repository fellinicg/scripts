﻿using FCGLogger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Omni
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Declarations
                Omni omni = new Omni();

                // Set user's full name
//                this.user.InnerHtml = "Welcome, " + Session["FullName"].ToString();

                // Set user's view
                //switch (Session["View"].ToString())
                //{
                //    case "FULL":
                //        this.servers.Visible = this.timeentry.Visible = this.uresp.Visible = this.maint.Visible = true;
                //        this.servers.Visible = this.timeentry.Visible = this.uresp.Visible = this.maint.Visible = this.wrkord.Visible = true;
                //        break;
                //    case "WO":
                //        this.wrkord.Visible = true;
                //        break;
                //    default:
                //        break;
                //}

                // Add OnLoad event to body tag
                //this.pmbody.Attributes.Add("OnLoad", "pageload(" + Session["UserAccountID"].ToString() + ");");
            }
            catch (Exception ExceptionErr)
            {
                FCGLogger.Write errLog = new FCGLogger.Write(ExceptionErr, HttpContext.Current.Session["ErrorLog"].ToString());
            }
        }
    }
}