﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Omni.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Omni</title>
    <link type="text/css" href="/css/page.css" media="all" rel="stylesheet" />
    <link type="text/css" href="/css/datatable.css" media="all" rel="stylesheet" />
	<script type="text/javascript" src="scripts/jquery.js"></script>
	<script type="text/javascript" src="scripts/datatables.js"></script>
	<script type="text/javascript" src="scripts/colreorder.js"></script>
	<script type="text/javascript" src="scripts/main.js"></script>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
		    getsrvs();
		});
	</script>
</head>
<body id="DefaultBody">
    <div id="container">
		<div class="full_width big">
			OMNI
		</div>
		<h1>Servers</h1>
			<div>
                <table cellpadding="0" cellspacing="0" border="0" class="display" id="servers">
	                <thead>
		                <tr>
			                <th width="20%">Name</th>
			                <th width="10%">Type</th>
			                <th width="40%">Serial #</th>
			                <th width="15%">Status</th>
		                </tr>
	                </thead>
	                <tbody>
	                </tbody>
	                <tfoot>
		                <tr>
			                <th>Name</th>
			                <th>Type</th>
			                <th>Serial #</th>
			                <th>Status</th>
		                </tr>
	                </tfoot>
                </table>
			</div>
			<div class="spacer"></div>
		<h1>More Servers</h1>
			<div>
                <table cellpadding="0" cellspacing="0" border="0" class="display" id="servers2">
	                <thead>
		                <tr>
                            <th>&nbsp;</th>
			                <th width="20%">Name</th>
			                <th width="10%">Type</th>
			                <th width="40%">Serial #</th>
			                <th width="15%">Status</th>
		                </tr>
	                </thead>
	                <tbody>
	                </tbody>
                </table>
			</div>
			<div class="spacer"></div>
    </div>
</body>
</html>
